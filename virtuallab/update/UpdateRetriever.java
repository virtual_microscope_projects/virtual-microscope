/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.update;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import virtuallab.Specimen;
import virtuallab.OfflineSpecimen;
import virtuallab.gui.controls.annotation.SpecimenAnnotationFile;
import virtuallab.util.Constants;

import virtuallab.Log;

/**
 * This class analyses the RSS feed and finds out the new specimens available.
 * The RSS feed includes all the specimens available from the virtual lab data
 * download page. Specimens that are not present on the host machine's
 * "specimens" folder but are present on the virtual lab data download page are
 * considered new and this class only restricits itself to dealing with such
 * specimens.
 *
 */
public class UpdateRetriever {

    private static final Log log = new Log(UpdateRetriever.class.getName());

    /**
     * A tag that we expect to find in the RSS feed. The parent tag that holds
     * all the specimens under <item> tags.
     */
    private final String XML_TAG_CHANNEL = "channel";
    /**
     * A tag that we expect to find in the RSS feed. The parent tag that
     * delimits each specimen
     */
    private final String XML_TAG_ITEM = "item";
    /**
     * A tag that we expect to find in the RSS feed. The name of the specimen
     */
    private final String XML_TAG_TITLE = "title";
    /**
     * A tag that we expect to find in the RSS feed. The URL from which the
     * specimen can be downloaded
     */
    private final String XML_TAG_LINK = "link";
    /**
     * A tag that we expect to find in the RSS feed. The HTML long description
     * of the speicmen
     */
    private final String XML_TAG_DESCRIPTION = "virtuallab:description";

    /**
     * This methods takes in the description text and gets rid of the specimen
     * IMG hyperlink. This is required because we show the image thumbnail
     * separately in our detail view and we do not want the thumbnail to show up
     * inside the long description. If the Image tag is not found we return the
     * original raw text.
     *
     * @param rawDescription
     * @deprecated
     * @return
     *
    private String formatDescriptionText(String rawDescription) {
    int startOfImageTag = rawDescription.indexOf(IMAGE_TAG_START_DELIMITER);
    int endOfImageTag = rawDescription.indexOf(IMAGE_TAG_END_DELIMITER);
    if (startOfImageTag < 0 || endOfImageTag < 0)
    return rawDescription;
    return rawDescription.substring(endOfImageTag
    + (IMAGE_TAG_END_DELIMITER.length()), rawDescription.length());
    }*/
    /**
     * A tag that we expect to find in the RSS feed. The unique specimen Id. We
     * shall use this informaton to figure out which specimens are new
     */
    private final String XML_TAG_SPECIMEN_ID = "virtuallab:specimenid";
    /**
     * A tag that we expect to find in the RSS feed. Depicts the XML version of
     * the specimen.xml file for this specimen located in the respective
     * specimen jar file
     */
    private final String XML_TAG_SPECIMEN_XML_VERSION = "virtuallab:xmlversion";
    /**
     * Holds the thumbnail URL from where to download the thumbnail
     */
    private final String XML_TAG_THUMBNAIL_URL = "virtuallab:thumbnail";
    /**
     * Holds the size of the specimen jar file
     */
    private final String XML_TAG_SPECIMEN_SIZE = "virtuallab:jar_bytes";
    /**
     * Holds the size of the long name of the instrument
     */
    private final String XML_TAG_INSTRUMENT_NAME = "virtuallab:instr_name";
    private final String XML_TAG_SPECIMEN_STATS = "virtuallab:stats";

    /**
     * holds the short name of the instrument.
     */
    private final String XML_TAG_INSTRUMENT_ID = "virtuallab:instr_id";
    private final String IMAGE_EXTENSION = "png";
    /**
     * The local file to which the rss feed will be saved to and will be read
     * back if we looses internet connectivity
     */
    private final String UPDATE_FILE_NAME = "updates.xml";

    public UpdateRetriever() {
    }

    /**
     * Determines if the host machine has internet connectivity or not.
     *
     * @return true if connected to internet false otherwise.
     */
    public static boolean isConnectedToInternet() {
        try {
            InetAddress.getByName("virtual.itg.uiuc.edu");
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * A tag that we expect to find in the RSS feed. The date when we published
     * this specimen
     *
     private final String XML_TAG_PUBLISH_DATE = "pubDate";*/

    /**
     * This method retrieves the RSS feed as an XML file from the given URL.
     *
     * @param rssFeedURL
     * @return The RSS Feed stuffed into a StringBuffer.
     */
    private StringBuffer retrieveRSSFeed(String rssFeedURL) {
        byte[] rssFeedBuffer = new byte[2048];
        StringBuffer rssFeedString = new StringBuffer(2048);
        try {
            URL url = new URL(rssFeedURL);
            InputStream in = url.openStream();
            int len;
            while ((len = in.read(rssFeedBuffer)) > 0) {
                for (int i = 0; i < len; i++) {
                    rssFeedString.append((char) rssFeedBuffer[i]);
                }
            }
            in.close();
        } catch (Exception e) {
            rssFeedString = null;
        }
        return rssFeedString;
    }

    /**
     * This method downloads the remote file to the local directory.
     *
     * @param fileToBeDownloadedURI        e.g. http://virtual.itg.uiuc.edu/data/annotations/1155375028342.xml
     * @param destinationFileCanonicalName e.g. ./annotations/1155375028342.xml
     * @return
     */
    public boolean downloadFile(String fileToBeDownloadedURI,
                                String destinationFileCanonicalName) {
        URL url;
        URLConnection connection;
        DataInputStream downloadFileStream = null;
        FileOutputStream os = null;
        try {
            long millis = System.currentTimeMillis();

            url = new URL(fileToBeDownloadedURI);

            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setUseCaches(false);

            downloadFileStream = new DataInputStream(connection.getInputStream());
            File file = new File(destinationFileCanonicalName);
            os = new FileOutputStream(file);
            int bufferSize = 4 * 1024;
            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            while ((bytesRead = downloadFileStream.read(buffer)) >= 0) {
                os.write(buffer, 0, bytesRead);
            }
            millis = System.currentTimeMillis() - millis;
        } catch (MalformedURLException mue) {
            return false;
        } catch (IOException ioe) {
            return false;
        } catch (Exception ex) {
            log.error(ex.getMessage());
        } finally {
            try {
                if (downloadFileStream != null)
                    downloadFileStream.close();
                if (os != null)
                    os.close();
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }

        }
        return true;
    }

    /**
     * This method takes in an Image URL and returns a bufferedimage loaded from
     * the specified URL. If the download was unsuccessful it returns null
     *
     * @param imageURL the URL of the image that will be downloaded.
     * @return the downloaded image or null if the download was unsuccessful.
     */
    private void downloadSpecimenThumbnail(URL downloadURL, String localDiskPath)
            throws Exception {
        File localImage = new File(localDiskPath);
        if (!localImage.exists()) {
            BufferedImage thumbnail = ImageIO.read(downloadURL);
            ImageIO.write(thumbnail, IMAGE_EXTENSION, new File(localDiskPath));
        }
    }

    /**
     * This method takes in an XML item node and populates a Specimen object out
     * of the contents of the node.
     *
     * @return The populated Specimen Object.
     */
    private OfflineSpecimen readSpecimen(Node itemNode) {
        OfflineSpecimen specimen = null;
        String specimenName = "";
        try {
            if ((itemNode.getNodeName().equals(XML_TAG_ITEM)) && (itemNode.getNodeType() == Node.ELEMENT_NODE)) {
                specimen = new OfflineSpecimen("");
                NodeList list = null;
                specimen.initializeSampleInfo();
                specimen.initializeInstrumentInfo();

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_TITLE);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getSampleInfo().setNamedParam(Constants.XMLTags.NAME, ((Text) list.item(0)).getData());
                    specimenName = ((Text) list.item(0)).getData();
                }
                list = ((Element) itemNode).getElementsByTagName(XML_TAG_LINK);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    /** will be used to download the file */
                    specimen.setJarFilePath(((Text) list.item(0)).getData());
                    /**
                     * will be used later if the specimen gets downloaded to
                     * local machine and then gets removed from the online list -
                     * since we shall then have to reset the jarfile path to the
                     * download url
                     */
                    specimen.getSampleInfo().setNamedParam(Constants.SPECIMEN_DOWNLOAD_URL, specimen.getJarFilePath());
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_DESCRIPTION);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getSampleInfo().setNamedParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION, ((Text) list.item(0)).getData());
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_SPECIMEN_ID);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getSampleInfo().setNamedParam(Constants.XMLTags.UNIQUE_NAME, ((Text) list.item(0)).getData());
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_SPECIMEN_STATS);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getSampleInfo().setNamedParam(Constants.XMLTags.SAMPLE_STATS, ((Text) list.item(0)).getData());
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_SPECIMEN_XML_VERSION);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    double xmlVersion = 0.0;
                    try {
                        xmlVersion = Double.parseDouble(((Text) list.item(0)).getData());
                    } catch (Exception ex) {
                        xmlVersion = 3.3;
                    }
                    specimen.setXMLVersion(xmlVersion);
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_SPECIMEN_SIZE);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.setJarFileSize(Integer.parseInt(((Text) list.item(0)).getData()));
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_INSTRUMENT_ID);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getInstrumentInfo().setNamedParam(Constants.XMLTags.TYPE, ((Text) list.item(0)).getData());
                }
                list = ((Element) itemNode).getElementsByTagName(XML_TAG_INSTRUMENT_NAME);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    specimen.getInstrumentInfo().setNamedParam(Constants.XMLTags.LONG_NAME, ((Text) list.item(0)).getData());
                }

                list = ((Element) itemNode).getElementsByTagName(XML_TAG_THUMBNAIL_URL);
                if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    try {
                        File dir = new File(Constants.VIRTUAL_EXECUTABLE_LOCATION + Constants.PATH_SEPARATOR + "temp");
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        downloadSpecimenThumbnail(new URL(((Text) list.item(0)).getData()),
                                Constants.VIRTUAL_EXECUTABLE_LOCATION + Constants.PATH_SEPARATOR + "temp" + Constants.PATH_SEPARATOR + specimen.getUniqueName() + "." + IMAGE_EXTENSION);
                        specimen.getSampleInfo().setNamedParam(Constants.XMLTags.THUMBNAIL,
                                Constants.VIRTUAL_EXECUTABLE_LOCATION + Constants.PATH_SEPARATOR + "temp" + Constants.PATH_SEPARATOR + specimen.getUniqueName() + "." + IMAGE_EXTENSION);
                    } catch (Exception ex) {
                        log.error(ex.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            log.error("Error reading " + specimenName + " !");
            log.error(ex.getMessage());
            specimen = null;
        }
        return specimen;
    }

    /**
     * This method parses the RSS Feed raw text (which is an XML) and extracts
     * the information pertaining to each specimen. It puts this information in
     * a collection as Specimen Objects.
     *
     * @throws Exception
     */
    private LinkedHashMap parseRSSFeed(String rssFeedURI) {
        LinkedHashMap allSpecimens = new LinkedHashMap();
        try {
            // System.out.print(rawRssContents);
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = builder.parse(rssFeedURI);
            NodeList list = doc.getElementsByTagName(XML_TAG_CHANNEL);
            Specimen specimen = null;
            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                list = list.item(0).getChildNodes();
                int channelNodesCount = list.getLength();
                for (int itemIndex = 0; itemIndex < channelNodesCount; itemIndex++) {
                    if ((specimen = readSpecimen(list.item(itemIndex))) != null) {
                        allSpecimens.put(specimen.getUniqueName(), specimen);
                    }
                }
            }
        } catch (Exception e) {
            allSpecimens = new LinkedHashMap();
        }
        return allSpecimens;
    }

    /**
     * This method acts as a mediator executing required methods to obtain a
     * list of all the specimens available at the virtual lab data web site.
     */
    public LinkedHashMap getAllSpecimens() throws Exception {
        LinkedHashMap specimens = null;
        String updateURL = Constants.VIRTUAL_EXECUTABLE_LOCATION
                + Constants.PATH_SEPARATOR + "temp" + Constants.PATH_SEPARATOR
                + UPDATE_FILE_NAME;
        File tempDir = new File(Constants.VIRTUAL_EXECUTABLE_LOCATION
                + Constants.PATH_SEPARATOR + "temp");
        if (!tempDir.exists())
            tempDir.mkdirs();
        if (isConnectedToInternet()) {
            // copy the remote file locally. So that we can use this as
            // reference when there
            // is not connectivity.
            FileOutputStream updateFile = new FileOutputStream(updateURL);
            StringBuffer rssFeed = retrieveRSSFeed(Constants.SPECIMEN_RSS_FEED_URL);
            if (rssFeed != null) {
                // rssFeed would be null in case when the internet connection
                // goes down while we are dowloading the rss feed.
                updateFile.write(rssFeed.toString().getBytes());
            }
            updateFile.close();
        }
        specimens = parseRSSFeed(updateURL);
        Iterator itr = specimens.values().iterator();
        int i = 0;
        while (itr.hasNext()) {
            OfflineSpecimen specimen = (OfflineSpecimen) itr.next();
        }
        return specimens;
    }

    /**
     * This method acts as a mediator executing required methods to obtain a
     * list of all the annotations available at the virtual lab server.
     *
     * @param annotationDirectory the folder where the annotations are saved.
     */
    public ArrayList getAllAnnotations(String annotationDirectory) throws Exception {
        ArrayList annotations = retrieveAnnotations(annotationDirectory);
        int i = 0;
        for (i = 0; i < annotations.size(); i++) {
            SpecimenAnnotationFile annotation = (SpecimenAnnotationFile) annotations.get(i);
        }
        return annotations;
    }

    /**
     * Given the xml contents, this method parses them and creates SpecimenAnnotationFile
     * objects corresponding to each annotation found in the given xml. It then validates these
     * annotations against what is available in the annotation directory, any matches found are
     * ignored and only the annotations that aren't found to be present in the annotations
     * directory are retained.
     *
     * @param annotationDirectory the path where the annotations will be saved.
     * @return the map containing the new specimenannotationfiles that should be downloaded and
     * put in the annotation folder.
     */
    private ArrayList retrieveAnnotations(String annotationDirectory) {
        ArrayList annotations = new ArrayList();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = builder.parse(Constants.ANNOTATION_RSS_FEED_URL);

            NodeList list = doc.getElementsByTagName(XML_TAG_CHANNEL);
            SpecimenAnnotationFile annotation = null;
            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                list = list.item(0).getChildNodes();
                int channelNodesCount = list.getLength();
                for (int itemIndex = 0; itemIndex < channelNodesCount; itemIndex++) {
                    if ((annotation = readAnnotation(list.item(itemIndex), annotationDirectory)) != null) {
                        annotations.add(annotation);
                    }
                }
            }
        } catch (Exception e) {
            log.error("Could not load annotations due to the following error " + e.getMessage());
            annotations = new ArrayList();
        }
        return annotations;
    }

    /**
     * Takes in an xml tag node and extracts the information pertaining to the
     * corresponding Annotation.
     *
     * @param itemNode
     * @param annotationDirectory the folder where the annotations will be saved.
     * @return The specimen annotation file containing the Annotation
     * information.
     */
    private SpecimenAnnotationFile readAnnotation(Node itemNode, String annotationDirectory) {
        try {
            if ((itemNode.getNodeName().equals(XML_TAG_ITEM))
                    && (itemNode.getNodeType() == Node.ELEMENT_NODE)) {
                NodeList list = null;
                String annotationFileName = null;
                String url = null;
                File annotationFile = null;
                list = ((Element) itemNode).getElementsByTagName(XML_TAG_TITLE);
                /** annotation File Name * */
                if (list != null && list.getLength() == 1
                        && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    annotationFileName = ((Text) list.item(0)).getData();
                    annotationFile = new File(annotationDirectory
                            + Constants.PATH_SEPARATOR + annotationFileName);
                    if (annotationFile.exists())
                        // the annotation already exists locally.
                        return null;
                }
                /** annotation URL * */
                list = ((Element) itemNode).getElementsByTagName(XML_TAG_LINK);
                if (list != null && list.getLength() == 1
                        && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                    list = list.item(0).getChildNodes();
                    /** will be used to download the file */
                    url = ((Text) list.item(0)).getData();
                }
                if (!downloadFile(url, annotationFile.getCanonicalPath()))
                    return null;
                SpecimenAnnotationFile annotation = new SpecimenAnnotationFile(
                        annotationFile);
                return annotation;
            }
        } catch (Exception ex) {
            return null;
        }
        return null;
    }
}
