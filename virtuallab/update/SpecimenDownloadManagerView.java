/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.update;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.net.URL;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;
import javax.swing.SwingUtilities;

import virtuallab.Configuration;
import virtuallab.OfflineSpecimen;
import virtuallab.gui.SpecimenButton;
import virtuallab.util.Constants;
import virtuallab.util.Locale;
import virtuallab.util.Utility;

import virtuallab.Log;

/**
 * This class is the view for the SpecimenDownloadManager. It contains the download
 * progress bar for each download.
 *
 */
public class SpecimenDownloadManagerView extends JFrame implements ActionListener, MouseMotionListener, MouseListener, WindowListener {

    private static final Log log = new Log(SpecimenDownloadManagerView.class.getName());

    private static final long serialVersionUID = -1786208628952734726L;
    private final int PROGRESS_BAR_HEIGHT = 15;
    private final int PROGRESS_BAR_WIDTH = 230;
    private final Dimension SPECIMEN_THUMBNAIL_DIMENSION = new Dimension(50, 50);
    private final float BYTES_PER_MEGABYTE = 1048576;
    private final String DOWNLOAD_PROGRESS_DESCRIPTION = "DOWNLOAD_PROGRESS_DESCRIPTION";
    private final String DOWNLOAD_PROGRESS_VIEW = "DOWNLOAD_PROGRESS_VIEW";
    private final int DIALOG_WIDTH = 377;//380;
    private final int DIALOG_HEIGHT = 530;//595;
    private final int DOWNLOAD_PANEL_HEIGHT = 80;
    private final int DOWNLOAD_PANEL_WIDTH = 350;
    private final String CLEANUP_COMMAND = "Cleanup";
    private SpecimenDownloadManager m_model;
    /**
     * The panel that contains ALL the download progress bars
     */
    private JPanel m_container;
    private JScrollPane m_specimenScrollPane;
    private HashMap m_progressBars = new HashMap();
    private Cursor m_originalCursor;
    /**
     * @deprecated
     */
    private JButton m_cleanupButton;
    /**
     * The component that acts as vertical glue for the specimen download
     * panel. This is required to make sure that the the specimen download
     * panels always aligh at the top of the frame. Every time a new specimen
     * is downloaded the corresponding progress bar and the panel gets added
     * along with this footer glue. when another specimen is downloaded we
     * first remove this glue and then add the new progress bar panel and then
     * insert the glue back in. This trick helps us make our download window
     * behave exactly like FireFox's downalod window.
     */
    private Component m_containerFooterGlue;

    public SpecimenDownloadManagerView(SpecimenDownloadManager model) {
        super(Locale.DownloadManager(Configuration.getApplicationLanguage()));
        try {
            URL url = getClass().getResource(Constants.APPLICATION_ICON);
            Image image = ImageIO.read(url);
            this.setIconImage(image);
        } catch (Exception e) {
            log.error("Could not find the application icon:" + Constants.APPLICATION_ICON);
            log.error(e.getMessage());
        }

        m_model = model;
        setPreferredSize(new Dimension(DIALOG_WIDTH, DIALOG_HEIGHT));

        m_container = new JPanel();
        m_container.setLayout(new BoxLayout(m_container, BoxLayout.PAGE_AXIS));
        m_container.setBackground(Color.WHITE);

        m_specimenScrollPane = new JScrollPane(m_container);
        m_specimenScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        m_specimenScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        m_specimenScrollPane.setPreferredSize(new Dimension(DOWNLOAD_PANEL_WIDTH, 6 * DOWNLOAD_PANEL_HEIGHT));

        SpringLayout layout = new SpringLayout();
        setLayout(layout);
        layout.putConstraint(SpringLayout.WEST, m_specimenScrollPane, 12, SpringLayout.WEST, this);
        layout.putConstraint(SpringLayout.NORTH, m_specimenScrollPane, 10, SpringLayout.NORTH, this);
        getContentPane().add(m_specimenScrollPane);

        pack();
        setResizable(false);

        setVisible(true);
    }

    /**
     * This method reinitializes an existing download progress panel that has been cancelled before.
     * If the specified progress panel does not exist it returns false.
     *
     * @return true if the a progress panel by the given name already exists false otherwise.
     */
    private boolean rejuvenateCancelledProgressBar(String specimenUniqueName) {
        if (m_progressBars.get(specimenUniqueName) != null) {
            // progress bar already exists. This will happen when a cancelled download
            // is being restarted.
            JPanel progressBarPanel = (JPanel) m_progressBars.get(specimenUniqueName);
            for (int i = 0; i < progressBarPanel.getComponentCount(); i++) {
                Component progressPanelComponent = progressBarPanel.getComponent(i);
                if (progressPanelComponent.getName() != null) {
                    if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_DESCRIPTION)) {
                        JLabel cancelLabel = (JLabel) progressPanelComponent;
                        cancelLabel.setText("");
                        cancelLabel.setForeground(Color.BLACK);
                    } else if (progressPanelComponent instanceof JLabel && progressPanelComponent.getName().equals(progressBarPanel.getName())) {
                        JLabel cancelLabel = (JLabel) progressPanelComponent;
                        cancelLabel.setText("<html><u><b><font color=blue>"+Locale.Cancel(Configuration.getApplicationLanguage())+"</font></b></u></html>");
                    }
                }
            }
            return true;
        }
        return false;
    }


    public void windowOpened(WindowEvent we) {
    }

    public void windowIconified(WindowEvent we) {
    }

    public void windowDeiconified(WindowEvent we) {
    }

    public void windowActivated(WindowEvent we) {
    }

    public void windowDeactivated(WindowEvent we) {
    }

    public void windowClosed(WindowEvent we) {
    }

    /**
     * This method gets called when the graph window is being closed by pressing the top
     * right "Cross" button. We shall delete the respective Annotation and the JFrame.
     */
    public void windowClosing(WindowEvent we) {
        //m_model.cancelAllDownloads(this);
    }


    /**
     * This method adds a new progress bar to the download manager window. In addition
     * to the progress bar a specimen thumbnail and accompanying labels are also added.
     *
     * @param specimenBeingDownloaded
     */
    public void addProgressBar(OfflineSpecimen specimenBeingDownloaded) {
        JPanel progressBarPanel = new JPanel();
        JProgressBar downloadProgressBar = new JProgressBar();
        JLabel specimenThumbnailLabel;
        JLabel specimenName = new JLabel();
        JLabel downloadProgressDescription = new JLabel();
        JLabel cancel = new JLabel("<html><u><b><font color=blue>"+ Locale.Cancel(Configuration.getApplicationLanguage())+"</font></b></u></html>");
        String progressBarName = specimenBeingDownloaded.getUniqueName();
        if (rejuvenateCancelledProgressBar(progressBarName))
            return;
        try {

            progressBarPanel.setName(progressBarName);
            progressBarPanel.setBorder(BorderFactory.createEtchedBorder(1));
            progressBarPanel.setPreferredSize(new Dimension(DOWNLOAD_PANEL_WIDTH, DOWNLOAD_PANEL_HEIGHT));
            progressBarPanel.setMaximumSize(new Dimension(DOWNLOAD_PANEL_WIDTH, DOWNLOAD_PANEL_HEIGHT));
            progressBarPanel.setBackground(Color.WHITE);

            SpecimenButton curBttn = new SpecimenButton(specimenBeingDownloaded.getUniqueName());
            ImageIcon specimenThumbnail = new ImageIcon(curBttn.getThumbnailImage(specimenBeingDownloaded, SPECIMEN_THUMBNAIL_DIMENSION));
            specimenThumbnailLabel = new JLabel(specimenThumbnail);
            specimenThumbnailLabel.setMinimumSize(SPECIMEN_THUMBNAIL_DIMENSION);
            Font labelFont = new Font("Verdana", Font.PLAIN, 9);
            specimenName.setFont(labelFont);
            specimenName.setText("<html><b>"+Locale.Downloading(Configuration.getApplicationLanguage())+":</b> " + specimenBeingDownloaded.getNamedParam(Constants.XMLTags.NAME));

            downloadProgressDescription.setFont(labelFont);
            downloadProgressDescription.setText("0 "+Locale.Of(Configuration.getApplicationLanguage())+" " + Utility.formatDouble((double) specimenBeingDownloaded.getJarFileSize() / BYTES_PER_MEGABYTE, 1) + " MB");
            downloadProgressDescription.setName(DOWNLOAD_PROGRESS_DESCRIPTION);

            downloadProgressBar.setName(DOWNLOAD_PROGRESS_VIEW);
            downloadProgressBar.setPreferredSize(new Dimension(PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT));
            downloadProgressBar.setMinimumSize(new Dimension(PROGRESS_BAR_WIDTH, PROGRESS_BAR_HEIGHT));
            downloadProgressBar.setMinimum(0);
            downloadProgressBar.setMaximum(100);
            downloadProgressBar.setStringPainted(true);

            cancel.setFont(labelFont);
            cancel.setName(specimenBeingDownloaded.getUniqueName());
            // the event listeners are added to handle the cancellation of a download.
            cancel.addMouseMotionListener(this);
            cancel.addMouseListener(this);

            addComponentsToProgressPanel(progressBarPanel, specimenThumbnailLabel, specimenName, downloadProgressBar, downloadProgressDescription, cancel);
            if (m_containerFooterGlue != null) // will be null first time around.
                m_container.remove(m_containerFooterGlue);
            m_container.add(progressBarPanel);
            m_containerFooterGlue = Box.createVerticalGlue();
            m_container.add(m_containerFooterGlue);
            m_progressBars.put(specimenBeingDownloaded.getUniqueName(), progressBarPanel);
            validate();
            m_specimenScrollPane.getVerticalScrollBar().setValue((int) DOWNLOAD_PANEL_HEIGHT * m_progressBars.size());
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    /**
     * This method laysout the components inside the ProgressBarPanel.
     *
     * @param container     the progress bar panel in which the components will be laid out
     * @param leftIconLabel the specimen icon thumbnail
     * @param topLabel      the specimen name label
     * @param centerBar     the download progress bar.
     * @param bottomLabel   the bottom textual description of the download progress
     * @return the fully populated progress bar panel.
     */
    private JPanel addComponentsToProgressPanel(JPanel container, JLabel leftIconLabel, JLabel topLabel, JProgressBar centerBar, JLabel bottomLabel, JLabel bottomRight) {
        container.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridheight = 3;
        constraints.insets = new Insets(0, 0, 0, 10);
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        container.add(leftIconLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;

        constraints.ipady = 10;
        container.add(topLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.gridy = 1;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        container.add(centerBar, constraints);

        constraints = new GridBagConstraints();
        constraints.gridy = 1;
        constraints.gridwidth = 3;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        JLabel rightInvisiblePad = new JLabel();
        rightInvisiblePad.setPreferredSize(new Dimension(15, 2));
        container.add(rightInvisiblePad, constraints);

        constraints = new GridBagConstraints();
        constraints.gridy = 2;
        constraints.gridx = 1;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        constraints.ipady = 5;
        container.add(bottomLabel, constraints);

        constraints = new GridBagConstraints();
        constraints.gridy = 2;
        constraints.gridx = 2;
        constraints.anchor = GridBagConstraints.FIRST_LINE_END;
        constraints.ipady = 5;

        container.add(bottomRight, constraints);

        return container;
    }

    public void actionPerformed(ActionEvent ae) {
        if (ae.getActionCommand().equals(CLEANUP_COMMAND)) {
            m_model.cleanUp();
        }
    }

    /**
     * This method updates the download progress of the specified specimen. It
     * performs its updates in a Swing thread thus relinquishing the caller
     * from managing their own GUI update threads. For this to work properly
     * the caller must not itself be running in a Swing thread - the caller must
     * be running in a non-Swing worker thread, else the progress bar update will
     * not be reflected.
     * Both the progress bar status and the textual description are updated to reflect
     * the download progress.
     *
     * @param specimenID
     * @param downloadedBytes
     * @param totalBytes
     */
    public void updateProgressStatus(final String specimenID, final int downloadedBytes, final int totalBytes) {
        if (m_progressBars.get(specimenID) != null) {
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    JPanel progressPanel = (JPanel) m_progressBars.get(specimenID);
                    for (int i = 0; i < progressPanel.getComponentCount(); i++) {
                        Component progressPanelComponent = progressPanel.getComponent(i);
                        int percentageCompleted = (int) ((float) downloadedBytes / (float) totalBytes * (float) 100);
                        if (progressPanelComponent.getName() != null) {
                            if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_VIEW)) {
                                ((JProgressBar) progressPanelComponent).setValue(percentageCompleted);
                            } else if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_DESCRIPTION)) {
                                if (percentageCompleted < 100) {
                                    ((JLabel) progressPanelComponent).setText(Utility.formatDouble((double) downloadedBytes / BYTES_PER_MEGABYTE, 1) +
                                            Constants.SPACE+ Locale.Of(Configuration.getApplicationLanguage())+Constants.SPACE+
                                            Utility.formatDouble((double) totalBytes / BYTES_PER_MEGABYTE, 1) + " MB");
                                } else {
                                    ((JLabel) progressPanelComponent).setText(Locale.Completed(Configuration.getApplicationLanguage()));
                                    ((JLabel) progressPanelComponent).setForeground(Color.BLUE);
                                }
                            } else if (progressPanelComponent instanceof JLabel &&
                                    progressPanelComponent.getName().equals(specimenID)) {
                                if (percentageCompleted >= 100)
                                    ((JLabel) progressPanelComponent).setText("");
                            }
                        }
                    }
                }
            };
            SwingUtilities.invokeLater(updateAComponent);
        }
    }

    /**
     * This method handles the download cancellation.
     *
     * @param progressBarPanelName
     */
    public void cancelDownload(final String progressBarPanelName) {
        if (m_progressBars.get(progressBarPanelName) != null) {
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    JPanel progressPanel = (JPanel) m_progressBars.get(progressBarPanelName);
                    for (int i = 0; i < progressPanel.getComponentCount(); i++) {
                        Component progressPanelComponent = progressPanel.getComponent(i);
                        if (progressPanelComponent.getName() != null) {
                            if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_VIEW)) {
                                ((JProgressBar) progressPanelComponent).setValue(0);
                            } else if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_DESCRIPTION)) {
                                JLabel cancelLabel = (JLabel) progressPanelComponent;
                                cancelLabel.setText(Locale.Cancelled(Configuration.getApplicationLanguage()));
                                cancelLabel.setForeground(Color.RED);
                            } else if (progressPanelComponent instanceof JLabel && progressPanelComponent.getName().equals(progressBarPanelName)) {
                                JLabel cancelLabel = (JLabel) progressPanelComponent;
                                cancelLabel.setText("");
                            }
                        }
                    }
                }
            };
            SwingUtilities.invokeLater(updateAComponent);
        }

    }

    /**
     * This method handles the download cancellation.
     *
     * @param progressBarPanelName
     */
    public void removeSpecimenDownloadPanel(final String progressBarPanelName) {
        if (m_progressBars.get(progressBarPanelName) != null) {
            m_container.remove((JPanel) m_progressBars.get(progressBarPanelName));
            m_progressBars.remove(progressBarPanelName);
            m_container.validate();
            validate();
        }
    }

    /**
     * This method shows any errors that may have occurred during downloading.
     *
     * @param progressBarPanelName the progressBar to which the errorMessage belong.
     * @param errorMessage,        the message to show on the panel.
     */
    public void reportErrorWhileDownloading(final String progressBarPanelName, final String errorMessage) {
        if (m_progressBars.get(progressBarPanelName) != null) {
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    JPanel progressPanel = (JPanel) m_progressBars.get(progressBarPanelName);
                    for (int i = 0; i < progressPanel.getComponentCount(); i++) {
                        String message = errorMessage;
                        Component progressPanelComponent = progressPanel.getComponent(i);
                        if (progressPanelComponent.getName() != null) {
                            if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_VIEW)) {
                                ((JProgressBar) progressPanelComponent).setValue(0);
                            } else if (progressPanelComponent.getName().equals(DOWNLOAD_PROGRESS_DESCRIPTION)) {
                                JLabel cancelLabel = (JLabel) progressPanelComponent;
                                if (message == null || message.length() == 0) {
                                    message = Locale.UnkownErrorWhileDownloading(Configuration.getApplicationLanguage());
                                }
                                cancelLabel.setText(Locale.Error(Configuration.getApplicationLanguage())+":" + message);
                                cancelLabel.setForeground(Color.RED);
                            } else if (progressPanelComponent instanceof JLabel &&
                                    progressPanelComponent.getName().equals(progressBarPanelName)) {
                                // it is the cancel link
                                JLabel cancelLabel = (JLabel) progressPanelComponent;
                                cancelLabel.setText("");
                            }
                        }
                    }
                }
            };
            SwingUtilities.invokeLater(updateAComponent);
        }

    }

    public void mouseExited(MouseEvent me) {
        if (me.getSource() instanceof JLabel) {
            ((JLabel) me.getSource()).setCursor(m_originalCursor);
        }
    }

    public void mouseEntered(MouseEvent me) {
        if (me.getSource() instanceof JLabel) {
            if (((JLabel) me.getSource()).getText().equals("")) {
                ((JLabel) me.getSource()).removeMouseListener(this);
            } else {
                m_originalCursor = getCursor();
                ((JLabel) me.getSource()).setCursor(new Cursor(Cursor.HAND_CURSOR));
            }
        }
    }

    public void mouseReleased(MouseEvent me) {
    }

    public void mouseMoved(MouseEvent me) {
    }

    public void mousePressed(MouseEvent me) {
        if (me.getSource() instanceof JLabel) {
            JLabel cancel = (JLabel) me.getSource();
            if (cancel.getName() != null) {
                if (cancel.getText().equals("")) {
                    cancel.removeMouseListener(this);
                } else {
                    m_model.cancelDownload(cancel.getName(), true);
                }
            }
        }
    }

    public void mouseClicked(MouseEvent me) {
    }

    public void mouseDragged(MouseEvent me) {
    }
}
