/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.update;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JOptionPane;

import virtuallab.Configuration;
import virtuallab.OfflineSpecimen;
import virtuallab.VirtualMicroscope;
import virtuallab.util.Constants;

import virtuallab.Log;
import virtuallab.util.Locale;
import virtuallab.util.TranslatedOptionPane;

/**
 * This class manages the downloading of specimens. It creates a new thread for each
 * specimen download and shows the download progress in the associated view.
 *
 */
public class SpecimenDownloadManager {

    private static final Log log = new Log(SpecimenDownloadManager.class.getName());

    /**
     * Virtuallab's configuration
     */
    private Configuration m_config;
    /**
     * The view for the download manager
     */
    private SpecimenDownloadManagerView m_view;

    /**
     * contains reference to all the download threads that have been initiated
     */
    private HashMap m_downloadThreads = new HashMap(3);

    private VirtualMicroscope m_parentContainer;

    public SpecimenDownloadManager(Configuration config, VirtualMicroscope parentContainer) {
        m_config = config;
        m_parentContainer = parentContainer;
    }

    private HashMap<String, Boolean> m_isSleeping = new HashMap<String, Boolean>();
    private HashMap<String, Boolean> m_shouldStop = new HashMap<String, Boolean>();

    /**
     * This method downloads the specified specimen jar file to the
     * destination file. If the destination file is null the file will
     * be downloaded to the specimens directory under the same name as
     * the remote secimen jar file.
     *
     * @param specimenToDownload the specimen that is to be downloaded
     * @return the local path tot which the file was downloaded. if download was
     * unsuccessful returns null.
     */

    private String downloadSpecimenJar(final OfflineSpecimen specimenToDownload) {
        URL url;
        URLConnection connection;
        DataInputStream downloadFileStream = null;
        FileOutputStream os = null;
        String destinationFile = null;
        final String specimenRemotePath = specimenToDownload.getJarFilePath();
        final int specimenJarFileSize = specimenToDownload.getJarFileSize();
        String threadName = specimenToDownload.getUniqueName();

        try {
            long millis = System.currentTimeMillis();
            url = new URL(specimenRemotePath);
            destinationFile = url.getFile();
            destinationFile = destinationFile.substring(destinationFile.lastIndexOf('/') + 1, destinationFile.length());
            destinationFile = m_config.getSpecimenDir() + Constants.PATH_SEPARATOR + destinationFile;

            connection = url.openConnection();
            connection.setDoInput(true);
            connection.setUseCaches(false);

            downloadFileStream = new DataInputStream(connection.getInputStream());
            File file = new File(destinationFile);
            os = new FileOutputStream(file);
            int bufferSize = 4 * 1024;
            byte[] buffer = new byte[bufferSize];
            int bytesRead;
            int bytesDownloadedSoFar = 0;
            while ((bytesRead = downloadFileStream.read(buffer)) >= 0) {
                while (m_isSleeping.get(threadName)) {
                    Thread.currentThread().sleep(1000);
                    if (m_shouldStop.get(threadName))
                        throw new Exception("Cancel download requested by user!");
                }
                if (m_shouldStop.get(threadName))
                    throw new Exception("Cancel download requested by user!");
                bytesDownloadedSoFar += bytesRead;
                os.write(buffer, 0, bytesRead);
                m_view.updateProgressStatus(threadName, bytesDownloadedSoFar, specimenJarFileSize);
            }
            millis = System.currentTimeMillis() - millis;
            specimenToDownload.setJarFilePath(destinationFile);
        } catch (Exception ex) {
            if (!m_shouldStop.get(threadName)) {
                m_view.reportErrorWhileDownloading(specimenToDownload.getUniqueName(), "Error while downloading specimen.");
                log.error("Error while downloading specimen " + threadName);
                log.error(ex.getMessage());
            }
            destinationFile = null;
        } finally {
            try {
                if (downloadFileStream != null)
                    downloadFileStream.close();
                if (os != null)
                    os.close();
            } catch (Exception ex) {
                log.error(ex.getMessage());
            }
        }
        return destinationFile;
    }

    /**
     * copies the remote specimen jar file to the host machine. The remote
     * file is downloaded into the specimens directory. This method is asynchronous
     * It returns immediately.
     *
     * @param specimenToDownload
     */
    public void downloadSpecimen(final OfflineSpecimen specimenToDownload) {
        m_parentContainer.setSpecimenDownloadButtonsEnabled(specimenToDownload.getUniqueName(), false);
        try {
            if (!UpdateRetriever.isConnectedToInternet()) {
                JOptionPane.showMessageDialog(m_view,
                        Locale.NoInternetConnection(Configuration.getApplicationLanguage())+". "+
                        Locale.ResumeAfterCoonectingToInternet(Configuration.getApplicationLanguage())+".",
                        Locale.ConnectivityError(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                m_parentContainer.setSpecimenDownloadButtonsEnabled(specimenToDownload.getUniqueName(), true);
                return;
            }
            if (m_view == null)
                m_view = new SpecimenDownloadManagerView(this);
            if (!m_view.isVisible())
                m_view.setVisible(true);
            if (isSpecimenBeingDownloaded(specimenToDownload.getUniqueName())) {
                // TODO: With the new disable download button this if() will never be
                // executed. Remove it when the new logic is finalized.
                m_view.toFront();
                return;
            }
        } catch (Exception ex) {
            m_view.setTitle(Locale.ErrorAtDownloadInitialization(Configuration.getApplicationLanguage()));
            log.error(Locale.ErrorAtDownloadInitialization(Locale.LANGUAGE.EN));
            log.error(ex.getMessage());
            m_parentContainer.setSpecimenDownloadButtonsEnabled(specimenToDownload.getUniqueName(), true);
            return;
        }
        Runnable downloadThread = new Runnable() {
            // start download in a separate thread.
            public void run() {
                m_view.addProgressBar(specimenToDownload);
                if (downloadSpecimenJar(specimenToDownload) != null)
                    m_parentContainer.moveOfflineSpecimenToOnlinePanel(specimenToDownload);
                else {
                    // error occurred so we reenable the download button.
                    m_parentContainer.setSpecimenDownloadButtonsEnabled(specimenToDownload.getUniqueName(), true);
                }
            }
        };
        Thread refreshThread = new Thread(downloadThread, specimenToDownload.getUniqueName());
        m_isSleeping.put(specimenToDownload.getUniqueName(), false);
        m_shouldStop.put(specimenToDownload.getUniqueName(), false);
        m_downloadThreads.put(specimenToDownload.getUniqueName(), refreshThread);
        refreshThread.start();
    }

    /**
     * Returns true if the specimen is either being downloaded or has already been downloaded
     *
     * @return
     */
    public boolean isSpecimenBeingDownloaded(String specimenID) {
        if (m_downloadThreads.get(specimenID) != null) {
            return (((Thread) m_downloadThreads.get(specimenID)).isAlive());
        } else
            return false;
    }

    /**
     * This method cancels the download. Since the download is being done in a separate
     * thread this method will interrupt the thread and will stop it as required.
     *
     * @param threadName
     */
    public void cancelDownload(String threadName, boolean confirmationRequired) {
        Thread threadToStop = (Thread) m_downloadThreads.get(threadName);
        if (threadToStop != null) {
            if (confirmationRequired) {
                m_isSleeping.put(threadName, true);
                if (JOptionPane.YES_OPTION == new TranslatedOptionPane().yesNo(m_view,
                            Locale.CancelDownloadConfirmation(Configuration.getApplicationLanguage())+" ?",
                            Locale.ConfirmDownloadCancellation(Configuration.getApplicationLanguage()))) {
                    m_shouldStop.put(threadName, true);
                    m_isSleeping.put(threadName, false);
                    m_view.cancelDownload(threadName);
                    m_parentContainer.setSpecimenDownloadButtonsEnabled(threadName, true);
                } else
                    m_isSleeping.put(threadName, false);

            } else // no confirmation required.
                m_shouldStop.put(threadName, true);


        }
    }

    /**
     * This method cancels all in-progress dowanlods. This is usuall called
     * when the application is exiting, this method guarantees that we do not
     * leave any zombie threads behind.
     */
    public void cancelAllDownloads() {
        ArrayList threadsToRemove = new ArrayList();
        Iterator it = m_downloadThreads.keySet().iterator();
        while (it.hasNext()) {
            String specimenUniqueName = (String) it.next();
            cancelDownload(specimenUniqueName, false);
            threadsToRemove.add(specimenUniqueName);
        }
        removeThreads(threadsToRemove);
    }

    /**
     * This method gets rid of cancelled and completed downloads. This is invoked when
     * the user presses the clean up button
     *
     * @deprecated The clean up button has been taken off the download manager window as
     * the user tests showed that it confused the users.
     */
    public void cleanUp() {
        ArrayList threadsToRemove = new ArrayList();
        Iterator it = m_downloadThreads.keySet().iterator();
        while (it.hasNext()) {
            String specimenUniqueName = (String) it.next();
            Thread downloadThread = (Thread) m_downloadThreads.get(specimenUniqueName);
            if (!downloadThread.isAlive()) {
                m_view.removeSpecimenDownloadPanel(specimenUniqueName);
                threadsToRemove.add(specimenUniqueName);
                downloadThread = null;
            }
        }
        removeThreads(threadsToRemove);
    }

    /**
     * This method takes in the list of the thread names that need to be removed and
     * removes them from th einternal data structure. Note that the method is synchronized
     * so that while we are removing the thread new download threads may be started withou
     * raising concurrency issues.
     *
     * @param threadsToRemove
     */
    private synchronized void removeThreads(ArrayList threadsToRemove) {
        for (int i = 0; i < threadsToRemove.size(); i++) {
            m_downloadThreads.remove((String) threadsToRemove.get(i));
        }
    }
}
