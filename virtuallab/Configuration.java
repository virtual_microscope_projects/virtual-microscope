/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.io.FileOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import virtuallab.util.Constants;
import virtuallab.util.Locale;

//$Id: Configuration.java,v 1.20 2006/09/05 01:08:47 manzoor2 Exp $**
/* Saves, retrieves, and validates configuration information.
 */
public class Configuration {

    private static final String APPLICATION_DIR = "./VirtualMicroscope";
    /**
     * Default specimen directory.
     */
    private static final String DEF_SPECIMEN_DIR = getApplicationFolder() + "/specimens";
    /**
     * Default annotations directory.
     */
    private static final String DEF_ANNOTAT_DIR = getApplicationFolder() + "/annotations";

    /**
     * The default configuration version
     */
    private static final double DEF_CONFIGURATION_VERSION = Double.parseDouble(Version.getMajorVersion() + "." + Version.getMinorVersion());
    /**
     * Default configuration file name.
     */
    private static final String DEF_CONFIG = getApplicationFolder() + "/VirtualLabConfig.xml";

    public static String getApplicationFolder() {
        Path path = Paths.get(APPLICATION_DIR);
        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                return "";
            }
        }

        return APPLICATION_DIR;
    }
    /**
     * Default HELP URL
     */
    private static final String DEF_HELP_URL = "https://gitlab.com/virtual_microscope_projects/virtual-microscope/wikis/home";
    /**
     * Default Navigation Speed
     */
    private static final String DEF_NAVIGATION_SPEED = "5";
    private static final OverlayElement[] DEF_OVERLAY_ELEMENTS = {
            new OverlayElement("Ca", "Ca", "Ca (Ka)", 3.691f),
            new OverlayElement("Mg", "Mg", "Mg (Ka)", 1.254f),
            new OverlayElement("K", "K", "K (Ka)", 3.313f),
            new OverlayElement("C", "C", "C (Ka)", 0.282f),
            new OverlayElement("Al", "Al", "Al (Ka)", 1.486f),
            new OverlayElement("Fe", "Fe", "Fe (Ka)", 6.398f),
            new OverlayElement("O", "O", "O (Ka)", 0.525f),
            new OverlayElement("Pd", "Pd", "Pd (Ka)", 2.013f),
            new OverlayElement("Si", "Si", "Si (Ka)", 1.739f),
            new OverlayElement("Na", "Na", "Na (Ka)", 1.040f),
            new OverlayElement("Au", "Au", "Au (Ma)", 2.120f),
            new OverlayElement("Cl", "Cl", "Cl (Ka)", 2.622f),
            new OverlayElement("Ba", "Ba", "Ba (La)", 4.465f),
            new OverlayElement("Cr", "Cr", "Cr (Ka)", 5.414f),
            new OverlayElement("S", "S", "S (Ka)", 2.307f),
            new OverlayElement("Ni", "Ni", "Ni (Ka)", 7.477f)
    };
    /**
     * Default set of plugins.
     */
    private static final String[][] DEF_CONTROLS = {
            {Constants.Controls.ANNOTATION, "virtuallab.gui.controls.annotation.AnnotationControl"},
            {Constants.Controls.DETECTOR, "virtuallab.gui.DetectorMenu"},
            {Constants.Controls.EDS, "virtuallab.gui.controls.eds.EDSMenu"},
            {Constants.Controls.COLORMAP, "virtuallab.gui.controls.afm.ColormapMenu"},
            {Constants.Controls.MAGNIFICATION, "virtuallab.gui.TextMenu"},
            {Constants.Controls.FOCUS, "virtuallab.gui.SliderMenu"},
            {Constants.Controls.BRIGHTNESS, "virtuallab.gui.AdjustmentControlBase"},
            {Constants.Controls.CONTRAST, "virtuallab.gui.AdjustmentControlBase"},
            {Constants.Controls.NAVIGATOR, "virtuallab.gui.NavigationThumbnail"},
            {Constants.Controls.HEIGHT, "virtuallab.gui.controls.measure.HeightMeasure"},
            {Constants.Controls.DISTANCE, "virtuallab.gui.controls.measure.DistanceMeasure"}};


    private static final String XML_TAG_VLAB_CONFIG = "VirtualLabConfig";
    private static final String XML_TAG_SPECIMEN_DIR = "SpecimenDirectory";
    private static final String XML_TAG_CONFIGURATION_VERSION = "ConfigurationVersion";
    private static final String XML_TAG_ANNOT_DIR = "AnnotDirectory";

    private static final String XML_TAG_HELP_URL = "HelpURL";
    private static final String XML_TAG_JDK_CHECK = "JdkCheck";
    private static final String XML_TAG_NAV_SPEED = "NavigationSpeed";
    private static final String XML_ATTRIB_CHECK = "Check";
    private static final String XML_ATTRIB_SPEED = "Speed";
    private static final String XML_ATTRIB_LANGUAGE = "Language";
    private static final String XML_ATTRIB_VERSION = "version";
    private static final String XML_ATTRIB_PATH = "Path";
    private static final String XML_TAG_CONTROLS = "Controls";
    private static final String XML_TAG_CONTROL = "Control";
    private static final String XML_ATTRIB_CONTROL_NAME = "Name";
    private static final String XML_ATTRIB_CONTROL_CLASSNAME = "ClassName";
    private static final String XML_TAG_OVERLAY_ELEMENTS = "OverlayElements";
    private static final String XML_TAG_ELEMENT = "Element";
    private static final String XML_ATTRIB_ELEMENT_ID = "id";
    private static final String XML_ATTRIB_ELEMENT_SHORT_NAME = "ShortName";
    private static final String XML_ATTRIB_ELEMENT_LONG_NAME = "LongName";
    private static final String XML_ATTRIB_ELEMENT_MAIN_PEAK = "MainPeak";
    /**
     * JDK Check Flag
     */
    protected String m_jdkCheck;
    /**
     * Navigation Speed.
     */
    protected String m_navigationSpeed;
    /**
     * Help URL
     */
    protected String m_helpURL;
    /**
     * Specimen directory.
     */
    protected String m_specimenDir;
    /**
     * The configuration file version
     */
    protected double m_configurationVersion;
    /**
     * Annotations directory.
     */
    protected String m_annotDir;
    /**
     * Name of the config file.
     */
    protected String m_configFileName;
    /**
     * List of class names implementing controls.
     */
    protected HashMap m_controlNames;
    /**
     * List of overlay Elements..
     */
    protected HashMap m_overlayElements = new HashMap();
    /**
     * Fix problems if possible.
     */
    protected boolean m_bFixIfPossible;
    /**
     * Dirty flag.
     */
    protected boolean m_bDirty;

    private static Locale.LANGUAGE m_applicationLanguage = Locale.LANGUAGE.EN;

    public static Locale.LANGUAGE getApplicationLanguage() {
        return m_applicationLanguage;
    }

    public static void setApplicationLanguage(String text) {

        if(text.equals(Locale.LANGUAGE.RO.toString())) {
            setApplicationLanguage(Locale.LANGUAGE.RO);
            return;
        }
        else if(text.equals(Locale.LANGUAGE.ES.toString())) {
            setApplicationLanguage(Locale.LANGUAGE.ES);
            return;
        }
        else if(text.equals(Locale.LANGUAGE.IT.toString())) {
            setApplicationLanguage(Locale.LANGUAGE.IT);
            return;
        }

        setApplicationLanguage(Locale.LANGUAGE.EN);
    }

    public static void setApplicationLanguage(Locale.LANGUAGE lang) {
        m_applicationLanguage = lang;
    }

    /**
     * Represents the 'yes' value through out the application. used to compare some configuration
     * values settings visible through Tools->Configure option
     */
    public static String OptionYes(Locale.LANGUAGE lang) { return Locale.Yes(lang); }
    /**
     * Represents the 'no' value through out the application. used to compare some configuration
     * values settings visible through Tools->Configure option
     */
    public static String OptionNo(Locale.LANGUAGE lang) { return Locale.No(lang); }

    /**
     * Default constructor.
     */
    public Configuration() {
        this(DEF_CONFIG, true);
    }

    /**
     * @param configFileName name of configuration file
     * @param fixIfPossible  fix problems if possible
     */
    public Configuration(String configFileName, boolean fixIfPossible) {
        m_bFixIfPossible = fixIfPossible;

        // Try to read out of the config file
        m_configFileName = configFileName;

        m_controlNames = new HashMap();

        // If had troubles unpersisting config - persist the default one
        if (!readConfig()) {
            writeConfig();
        }
    }

    /**
     * @return specimen directory
     */
    public String getSpecimenDir() {
        return m_specimenDir;
    }

    /**
     * @param specimenDir specimen directory string
     */
    public void setSpecimenDir(String specimenDir) {
        if (!m_specimenDir.equals(specimenDir)) {
            m_bDirty = true;
            m_specimenDir = specimenDir;
        }
    }

    /**
     * @return annotations directory
     */
    public String getAnnotDir() {
        return m_annotDir;
    }

    /**
     * @param specimenDir annotation directory string
     */
    public void setAnnotDir(String annotDir) {
        if (!m_annotDir.equals(annotDir)) {
            m_bDirty = true;
            m_annotDir = annotDir;
        }
    }

    public String getNavigationSpeed() {
        return m_navigationSpeed;
    }

    public void setNavigationSpeed(String str) {
        m_navigationSpeed = str;
        m_bDirty = true;
    }

    public int getNavigationSpeedAsInt() {
        return Integer.parseInt(m_navigationSpeed);
    }

    /**
     * @return true if JDK needs to be checked false otherwise.
     */
    public boolean shouldCheckJdk() {
        if (m_jdkCheck.equalsIgnoreCase(OptionYes(getApplicationLanguage())))
            return true;
        else
            return false;
    }

    /**
     * set the Jdk check flag (yes/no)
     */
    public void setCheckJdk(String flag) {
        if (!m_jdkCheck.equalsIgnoreCase(flag))
            m_bDirty = true;
        m_jdkCheck = flag;
    }

    /**
     * set the Jdk check flag (true/false)
     */
    public void setCheckJdk(boolean flag) {
        if (flag && !m_jdkCheck.equalsIgnoreCase(OptionYes(getApplicationLanguage()))) {
            m_jdkCheck = OptionYes(getApplicationLanguage());
            m_bDirty = true;
        } else if (m_jdkCheck.equalsIgnoreCase(OptionYes(getApplicationLanguage()))) {
            m_jdkCheck = OptionNo(getApplicationLanguage());
            m_bDirty = true;
        }
    }

    /**
     * @param controlName unique, case-sensitive control name.
     * @return control class name or null if not registered.
     */
    public String getControlClassName(String controlName) {
        return (String) m_controlNames.get(controlName);
    }

    /**
     * @return the dirty flag
     */
    public boolean isDirty() {
        return m_bDirty;
    }

    /**
     * Validates the configuration file.
     *
     * @return true if the config is valid
     */
    public boolean validateConfig() {
        // Check if directories exist
        File fileS = new File(m_specimenDir);
        File fileA = new File(m_annotDir);

        if (m_bFixIfPossible) {
            if (!fileS.exists()) {
                fileS.mkdirs();
            }

            if (!fileA.exists()) {
                fileA.mkdirs();
            }
        }

        // Check if all controls are indeed valid classes
        boolean bClassesValid = true;
        Iterator iter = m_controlNames.keySet().iterator();

        try {
            while (iter.hasNext()) {  // Attempt to load each class
                Class.forName((String) m_controlNames.get((String) iter.next()));
            }
        } catch (ClassNotFoundException e) {
            bClassesValid = false;
        }

        return bClassesValid && fileS.exists() && fileS.isDirectory() && fileA.exists() && fileA.isDirectory();
    }

    /**
     * Unpersists the configuration file.
     *
     * @return true iff file was read
     */
    protected boolean readConfig() {

        boolean success = true;

        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(m_configFileName);
            Node root = doc.getElementsByTagName(XML_TAG_VLAB_CONFIG).item(0);
            Element elem;
            NodeList list;
            String ctrlName;
            String ctrlClassName;

            try { // Get the config fileversion.
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_CONFIGURATION_VERSION).item(0);
                m_configurationVersion = Double.parseDouble(elem.getAttribute(XML_ATTRIB_VERSION));
            } catch (Exception e) {
                success = false;
                m_configurationVersion = DEF_CONFIGURATION_VERSION;
            }
            if (m_configurationVersion < DEF_CONFIGURATION_VERSION) {
                throw new Exception("An older version of VirtuallabConfig.xml was found. The file will be replaced with the new version.");
            }

            try { // Get the specimen directory
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_SPECIMEN_DIR).item(0);
                m_specimenDir = elem.getAttribute(XML_ATTRIB_PATH);
            } catch (Exception e) {
                success = false;
                m_specimenDir = DEF_SPECIMEN_DIR;
            }

            try { // Get the annotations directory
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_ANNOT_DIR).item(0);
                m_annotDir = elem.getAttribute(XML_ATTRIB_PATH);
            } catch (Exception e) {
                success = false;
                m_annotDir = DEF_ANNOTAT_DIR;
            }

            try { // Get the Help URL
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_HELP_URL).item(0);
                m_helpURL = elem.getAttribute(XML_ATTRIB_PATH).trim();
            } catch (Exception e) {
                success = false;
                m_helpURL = DEF_HELP_URL;
            }

            try { // Get the Jdk Check information
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_JDK_CHECK).item(0);
                m_jdkCheck = elem.getAttribute(XML_ATTRIB_CHECK).trim();
            } catch (Exception e) {
                success = false;
                m_jdkCheck = OptionYes(Configuration.getApplicationLanguage());
            }

            try { // Get the Navigation speed information
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_NAV_SPEED).item(0);
                m_navigationSpeed = elem.getAttribute(XML_ATTRIB_SPEED).trim();
            } catch (Exception e) {
                success = false;
                m_navigationSpeed = DEF_NAVIGATION_SPEED;
            }

            try { // Get the Navigation speed information
                elem = (Element) ((Element) root).getElementsByTagName(XML_ATTRIB_LANGUAGE).item(0);
                setApplicationLanguage(elem.getAttribute(XML_ATTRIB_LANGUAGE).trim());
            } catch (Exception e) {
                success = false;
                setApplicationLanguage(Locale.LANGUAGE.EN);
            }

            try { // Read control class names
                m_controlNames.clear();
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_CONTROLS).item(0);
                list = elem.getElementsByTagName(XML_TAG_CONTROL);

                for (int ctrlNum = 0; ctrlNum < list.getLength(); ctrlNum++) {
                    // Get the next control node
                    elem = (Element) list.item(ctrlNum);

                    ctrlName = elem.getAttribute(XML_ATTRIB_CONTROL_NAME);
                    ctrlClassName = elem.getAttribute(XML_ATTRIB_CONTROL_CLASSNAME);
                    m_controlNames.put(ctrlName, ctrlClassName);
                }
            } catch (Exception e) {
                success = false;

                for (int ctrlNum = 0; ctrlNum < DEF_CONTROLS.length; ctrlNum++) {
                    m_controlNames.put(DEF_CONTROLS[ctrlNum][0], DEF_CONTROLS[ctrlNum][1]);
                }
            }


            try { // Read Overlay Elements names
                m_overlayElements.clear();
                elem = (Element) ((Element) root).getElementsByTagName(XML_TAG_OVERLAY_ELEMENTS).item(0);
                list = elem.getElementsByTagName(XML_TAG_ELEMENT);

                for (int ctrlNum = 0; ctrlNum < list.getLength(); ctrlNum++) {
                    OverlayElement oe = new OverlayElement();
                    // Get the next element.
                    elem = (Element) list.item(ctrlNum);

                    oe.m_id = elem.getAttribute(XML_ATTRIB_ELEMENT_ID);
                    oe.m_shortName = elem.getAttribute(XML_ATTRIB_ELEMENT_SHORT_NAME);
                    oe.m_longName = elem.getAttribute(XML_ATTRIB_ELEMENT_LONG_NAME);
                    oe.m_energyLevel = new Float(elem.getAttribute(XML_ATTRIB_ELEMENT_MAIN_PEAK)).floatValue();
                    m_overlayElements.put(oe.m_id, oe);
                }
            } catch (Exception e) {
                success = false;
                for (int i = 0; i < DEF_OVERLAY_ELEMENTS.length; i++) {
                    m_overlayElements.put(DEF_OVERLAY_ELEMENTS[i].m_id, DEF_OVERLAY_ELEMENTS[i]);
                }
            }
        } catch (Exception e) {
            success = false;
            m_specimenDir = DEF_SPECIMEN_DIR;
            m_annotDir = DEF_ANNOTAT_DIR;
            m_helpURL = DEF_HELP_URL;
            m_jdkCheck = OptionYes(Configuration.getApplicationLanguage());
            m_navigationSpeed = DEF_NAVIGATION_SPEED;
            m_configurationVersion = DEF_CONFIGURATION_VERSION;
            for (int ctrlNum = 0; ctrlNum < DEF_CONTROLS.length; ctrlNum++) {
                m_controlNames.put(DEF_CONTROLS[ctrlNum][0], DEF_CONTROLS[ctrlNum][1]);
            }
            for (int i = 0; i < DEF_OVERLAY_ELEMENTS.length; i++) {
                m_overlayElements.put(DEF_OVERLAY_ELEMENTS[i].m_id, DEF_OVERLAY_ELEMENTS[i]);
            }
        }

        m_bDirty = !success;

        return success;
    }

    /**
     * Writes out config info.
     *
     * @return true iff write was successful
     */
    public boolean writeConfig() {
        boolean success = true;

        try {
            // Create document
            DocumentBuilder builder = (DocumentBuilderFactory.newInstance()).newDocumentBuilder();
            Document doc = builder.newDocument();
            Node root = doc.createElement(XML_TAG_VLAB_CONFIG);

            // Populate it with xml version.
            Element elem = doc.createElement(XML_TAG_CONFIGURATION_VERSION);
            elem.setAttribute(XML_ATTRIB_VERSION, Double.toString(m_configurationVersion));
            root.appendChild(elem);

            // And populate it with specimen directory
            elem = doc.createElement(XML_TAG_SPECIMEN_DIR);
            elem.setAttribute(XML_ATTRIB_PATH, m_specimenDir);
            root.appendChild(elem);

            // And annotations directory
            elem = doc.createElement(XML_TAG_ANNOT_DIR);
            elem.setAttribute(XML_ATTRIB_PATH, m_annotDir);
            root.appendChild(elem);

            // And Help URL
            elem = doc.createElement(XML_TAG_HELP_URL);
            elem.setAttribute(XML_ATTRIB_PATH, m_helpURL);
            root.appendChild(elem);

            // And JDK check flag
            elem = doc.createElement(XML_TAG_JDK_CHECK);
            elem.setAttribute(XML_ATTRIB_CHECK, m_jdkCheck);
            root.appendChild(elem);

            // And Navigation Speeed
            elem = doc.createElement(XML_TAG_NAV_SPEED);
            elem.setAttribute(XML_ATTRIB_SPEED, m_navigationSpeed);
            root.appendChild(elem);

            elem = doc.createElement(XML_ATTRIB_LANGUAGE);
            elem.setAttribute(XML_ATTRIB_LANGUAGE, getApplicationLanguage().toString());
            root.appendChild(elem);

            // And control names
            elem = doc.createElement(XML_TAG_CONTROLS);
            Element innerElem;
            String ctrlName;
            Iterator iter = m_controlNames.keySet().iterator();

            while (iter.hasNext()) {
                ctrlName = (String) iter.next();
                innerElem = doc.createElement(XML_TAG_CONTROL);
                innerElem.setAttribute(XML_ATTRIB_CONTROL_NAME, ctrlName);
                innerElem.setAttribute(XML_ATTRIB_CONTROL_CLASSNAME, (String) m_controlNames.get(ctrlName));

                elem.appendChild(innerElem);
            }
            root.appendChild(elem);

            //doc.appendChild(root);

            // And overlay element information
            elem = doc.createElement(XML_TAG_OVERLAY_ELEMENTS);
            for (int i = 0; i < DEF_OVERLAY_ELEMENTS.length; i++) {
                OverlayElement oe = (OverlayElement) DEF_OVERLAY_ELEMENTS[i];
                innerElem = doc.createElement(XML_TAG_ELEMENT);
                innerElem.setAttribute(XML_ATTRIB_ELEMENT_ID, oe.m_id);
                innerElem.setAttribute(XML_ATTRIB_ELEMENT_SHORT_NAME, oe.m_shortName);
                innerElem.setAttribute(XML_ATTRIB_ELEMENT_LONG_NAME, oe.m_longName);
                innerElem.setAttribute(XML_ATTRIB_ELEMENT_MAIN_PEAK, Float.toString(oe.m_energyLevel));
                elem.appendChild(innerElem);
            }
            root.appendChild(elem);
            doc.appendChild(root);

            // Now, persist in into file with a given name
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult result = new StreamResult(new FileOutputStream(m_configFileName));
            transformer.transform(new DOMSource(doc), result);

            m_bDirty = false;
        } catch (Exception e) {
            success = false;
        }

        return success;
    }

    public OverlayElement getOverlayElement(String elementName) {
        if (m_overlayElements.get(elementName) == null)
            return null;
        else
            return (OverlayElement) m_overlayElements.get(elementName);
    }

}
