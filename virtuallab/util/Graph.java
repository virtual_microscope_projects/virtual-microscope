/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.util;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Paint;
import java.awt.geom.Point2D;
import java.util.Vector;
import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.TextAnchor;

import virtuallab.OverlayElement;
/**
 * This class creates the graphs. It uses a freeware graphics package to create
 * the required graphs.
 *
 */
public class Graph {
    /**
     * Simple line graph with single data series
     */
    public final static short LINE_GRAPH = 0;
    /**
     * Vertical Bar graph
     */
    public final static short AREA_LINE_GRAPH = 1;
    /**
     * Line grah with multiple data series
     */
    public final static short MULTI_LINE_GRAPH = 2;
    /**
     * The data set that will be plotted
     */
    public Vector m_dataSet;
    /**
     * The panel in which the graph image will be placed
     */
    public JPanel m_panel;
    /**
     * The default graph label
     */
    private String m_graphLabel = "Virtual Miscroscope X-ray Spectra";
    /**
     * The default X-Axis label
     */
    private String m_XLabel = "Energy (KeV)";
    /**
     * The default Y-Axis label
     */
    private String m_YLabel = "X-ray count";
    /**
     * ALL the overlay elements in the EDS menu
     */
    private OverlayElementCheckbox m_overlayElements[];

    public Graph(String string, int width, int height, Vector data, OverlayElementCheckbox overlayElements[]) {
        m_dataSet = data;
        m_overlayElements = overlayElements;
        m_panel = createLineGraphPanel();
    }

    public Graph(int width, int height, Vector data, String graphLabel, String xlabel, String ylabel, short type, OverlayElementCheckbox overlayElements[]) {
        m_overlayElements = overlayElements;
        m_dataSet = data;
        if (type == LINE_GRAPH)
            drawLineGraph(width, height, data, graphLabel, xlabel, ylabel);
        else if (type == AREA_LINE_GRAPH)
            drawAreaLineGraph(width, height, data, graphLabel, xlabel, ylabel);
        else if (type == MULTI_LINE_GRAPH)
            drawMultiLineGraph(width, height, data, graphLabel, xlabel, ylabel);
    }

    /**
     * This method handles the drawing of the line graph with multiple data series.
     *
     * @param width
     * @param height
     * @param data       The data to be plotted
     * @param graphLabel The text that appears at the top of the graph.
     * @param xlabel     The X-Axis label
     * @param ylabel     the Y-Axis label.
     */
    private void drawMultiLineGraph(int width, int height, Vector data, String graphLabel, String xlabel, String ylabel) {
        if (graphLabel != null)
            m_graphLabel = graphLabel;
        if (xlabel != null)
            m_XLabel = xlabel;
        if (ylabel != null)
            m_YLabel = ylabel;
        m_panel = createMultiLineGraphPanel();
        m_panel.setPreferredSize(new Dimension(width, height));
    }

    /**
     * This method handles the drawing of the line graph.
     *
     * @param string     The name of the Panel in which the graph will be shown.
     * @param width
     * @param height
     * @param data       The data to be plotted
     * @param graphLabel The text that appears at the top of the graph.
     * @param xlabel     The X-Axis label
     * @param ylabel     the Y-Axis label.
     */
    private void drawLineGraph(int width, int height, Vector data, String graphLabel, String xlabel, String ylabel) {
        if (graphLabel != null)
            m_graphLabel = graphLabel;
        if (xlabel != null)
            m_XLabel = xlabel;
        if (ylabel != null)
            m_YLabel = ylabel;
        m_panel = createLineGraphPanel();
        m_panel.setPreferredSize(new Dimension(width, height));
    }

    /**
     * This method handles the drawing of the line graph.
     *
     * @param width
     * @param height
     * @param data       The data to be plotted
     * @param graphLabel The text that appears at the top of the graph.
     * @param xlabel     The X-Axis label
     * @param ylabel     the Y-Axis label.
     */
    private void drawAreaLineGraph(int width, int height, Vector data, String graphLabel, String xlabel, String ylabel) {
        if (graphLabel != null)
            m_graphLabel = graphLabel;
        if (xlabel != null)
            m_XLabel = xlabel;
        if (ylabel != null)
            m_YLabel = ylabel;
        m_panel = createAreaLineGraphPanel();
        m_panel.setPreferredSize(new Dimension(width, height));
    }

    /**
     * This method converts the input data into a format which is understandable to the
     * chart library.
     *
     * @return The created category data set which can be passed to the chart plotter method to plot the actual graph.
     */
    private XYSeriesCollection createLineGraphDataset() {
        XYSeriesCollection xyseriescollection = new XYSeriesCollection();
        XYSeries xyseries = new XYSeries("Consolidated Data", true, false);
        for (int i = 0; i < m_dataSet.size(); i++) {
//			Point2D.Double point = (Point2D.Double)m_dataSet.get(i);			
            Point2D.Float point = (Point2D.Float) m_dataSet.get(i);
            xyseries.add(point.getX(), point.getY());
        }
        xyseriescollection.addSeries(xyseries);
        return xyseriescollection;
    }

    /**
     * This method converts the input data into a format which is understandable to the
     * chart library.
     *
     * @return The created category data set which can be passed to the chart plotter method to plot the actual graph.
     */
    private XYSeriesCollection createAreaLineGraphDataset() {
        XYSeriesCollection xyseriescollection = new XYSeriesCollection();
        XYSeries xyseries = new XYSeries("Consolidated Data", true, false);
        for (int i = 0; i < m_dataSet.size(); i++) {
            Point2D.Float point = (Point2D.Float) m_dataSet.get(i);
            xyseries.add(point.getX(), point.getY());
        }
        xyseriescollection.addSeries(xyseries);
        return xyseriescollection;
    }

    /**
     * This method creates the actual line graph.
     */
    private JFreeChart createAreaLineGraph(XYDataset xydataset) {
        boolean legend = false;
        boolean tooltip = true;
        boolean urls = false;
        JFreeChart jfreechart = ChartFactory.createXYLineChart(null, m_XLabel, m_YLabel, xydataset, PlotOrientation.VERTICAL, legend, tooltip, urls);
        jfreechart.addSubtitle(new TextTitle(m_graphLabel));

        XYPlot xyplot = (XYPlot) jfreechart.getXYPlot();
        xyplot.setBackgroundPaint(Color.white);

        XYLineAndShapeRenderer lineandshaperenderer = (XYLineAndShapeRenderer) xyplot.getRenderer();
        lineandshaperenderer.setBaseLinesVisible(false);
        lineandshaperenderer.setDrawOutlines(true);
        lineandshaperenderer.setUseFillPaint(true);
        lineandshaperenderer.setBaseFillPaint(Color.white);

        drawAnnotations(xyplot);
        return jfreechart;
    }


    /**
     * This method creates the actual line graph.
     */
    private JFreeChart createLineGraph(XYDataset xydataset) {
        boolean legend = false;
        boolean tooltip = true;
        boolean urls = false;
        JFreeChart jfreechart = ChartFactory.createXYLineChart(null, m_XLabel, m_YLabel, xydataset, PlotOrientation.VERTICAL, legend, tooltip, urls);
        jfreechart.addSubtitle(new TextTitle(m_graphLabel));

        XYPlot xyplot = (XYPlot) jfreechart.getXYPlot();

        xyplot.setBackgroundPaint(Color.white);
        XYLineAndShapeRenderer lineandshaperenderer = (XYLineAndShapeRenderer) xyplot.getRenderer();
        lineandshaperenderer.setBaseLinesVisible(false);
        lineandshaperenderer.setDrawOutlines(true);
        lineandshaperenderer.setUseFillPaint(true);
        lineandshaperenderer.setBaseFillPaint(Color.white);

        NumberAxis numberaxisRange = (NumberAxis) xyplot.getRangeAxis();
        numberaxisRange.setAutoRangeIncludesZero(false);
        numberaxisRange.setRange(0.0, 1.0);
        drawAnnotations(xyplot);
        return jfreechart;
    }

    /**
     * This method draws annotations on the graph. The annotations correspond to the
     * peak in the graph.
     *
     * @param xyplot
     */
    private void drawAnnotations(XYPlot xyplot) {
        // Draw annotation corresponding to the peak of each element.
        Font font = new Font("SansSerif", 0, 9);
        /**
         * The Y-Axis value plotted on the graph is consolidated in the sense that
         * each overlay element X-ray count contribute towards that value.
         * The annotation needs to be placed at the peak. Since we know the X-axis
         * value corresponding to the peaks (these are the respective energy levels
         * of the elements) we shall dive into the data set and pull out the correponding
         * peak Y-Axis value and use this to properly place the annotation. The code below
         * achieves this.
         */
        for (int i = 0; i < m_overlayElements.length; i++) {
            boolean wasLess = true;
            for (int j = 0; j < m_dataSet.size(); j++) {
                Point2D point = (Point2D) m_dataSet.get(j);
                float x = (float) point.getX();
                float y = (float) point.getY();
                if (wasLess && x >= m_overlayElements[i].m_overlayElement.m_energyLevel) {
                    //if (x == m_overlayElements[i].m_overlayElement.m_energyLevel){
                    //System.out.println(x + " " + y);
                    // drawing the annotation at a location that makes it easy to read.
                    y += (float) xyplot.getRangeAxis().getRange().getUpperBound() / 40;
                    XYTextAnnotation xytextannotation
                            = new XYTextAnnotation(m_overlayElements[i].m_overlayElement.m_longName,
                            x, y);
                    Paint paint = new GradientPaint(0.0F, 0.0F,
                            m_overlayElements[i].m_selectedColor, 0.0F,
                            0.0F, m_overlayElements[i].m_selectedColor);
                    xytextannotation.setFont(font);
                    xytextannotation.setPaint(paint);
                    xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
                    xyplot.addAnnotation(xytextannotation);
                    break;
                }
            }
        }

    }

    /**
     * This method calls the appropriate library methods to get the chart plotted.
     *
     * @return
     */
    public JPanel createLineGraphPanel() {
        JFreeChart jfreechart = createLineGraph(createLineGraphDataset());
        return new ChartPanel(jfreechart);
    }

    /**
     * This method calls the appropriate library methods to get the chart plotted.
     *
     * @return
     */
    public JPanel createAreaLineGraphPanel() {
        JFreeChart jfreechart = createAreaLineGraph(createAreaLineGraphDataset());
        return new ChartPanel(jfreechart);
    }

    /**
     * This method creates data series along with their respective data sets for the
     * Multi Line graph.
     *
     * @return The XYSeries with their accompanying data sets.
     */
    private XYSeriesCollection createMultiLineDataset() {
        XYSeriesCollection xyseriescollection = new XYSeriesCollection();
        for (int i = 0; i < m_dataSet.size(); i++) {
            OverlayElement oe = (OverlayElement) m_dataSet.get(i);
            XYSeries xyseries = new XYSeries(oe.m_longName, true, false);
            Vector vt = oe.getLineGraphDataSeries();
            for (int j = 0; j < vt.size(); j++) {
                Point2D pt = (Point2D) vt.get(j);
                xyseries.add(pt.getX(), pt.getY());
            }
            xyseriescollection.addSeries(xyseries);
        }
        return xyseriescollection;
    }

    /**
     * This method creates the Multi Line plot.
     *
     * @param xydataset
     * @return
     */
    private JFreeChart createMultiLineGraph(XYDataset xydataset) {
        JFreeChart jfreechart
                = ChartFactory.createXYLineChart(null, m_XLabel, m_YLabel,
                xydataset,
                PlotOrientation.VERTICAL, true,
                true, false);
        TextTitle texttitle = new TextTitle(m_graphLabel,
                new Font("SansSerif", 1, 14));
        jfreechart.addSubtitle(texttitle);


        XYPlot xyplot = jfreechart.getXYPlot();
        xyplot.setBackgroundPaint(Color.white);
        //xyplot.setRangeGridlinePaint(Color.white);

        XYLineAndShapeRenderer xylineandshaperenderer
                = new XYLineAndShapeRenderer();
        for (int i = 0; i < m_dataSet.size(); i++) {
            OverlayElementCheckbox chbx = ((OverlayElement) m_dataSet.get(i)).m_parentCheckbox;
            Paint paint
                    = new GradientPaint(0.0F, 0.0F, chbx.m_selectedColor, 0.0F, 0.0F, chbx.m_selectedColor);
            xylineandshaperenderer.setSeriesPaint(i, paint);
            xylineandshaperenderer.setSeriesShapesVisible(i, false);

        }
        //xylineandshaperenderer.setSeriesLinesVisible(0,true);
        //xylineandshaperenderer.setSeriesShapesVisible(1,true);
//    	xylineandshaperenderer.setSeriesFillPaint(0,paint);
//    	xylineandshaperenderer.setBaseFillPaint(paint);
//    	xylineandshaperenderer.setSeriesOutlinePaint(0,paint);
        xyplot.setRenderer(xylineandshaperenderer);

        NumberAxis numberaxis = (NumberAxis) xyplot.getDomainAxis();
        //numberaxis.setUpperMargin(0.12);
        numberaxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        //NumberAxis numberaxisRange = (NumberAxis) xyplot.getRangeAxis();
        //numberaxisRange.setAutoRangeIncludesZero(false);


        //XYTextAnnotation xytextannotation = new XYTextAnnotation("5th", 10, 0.2);
        //xytextannotation.setFont(font);
        //xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
        //xyplot.addAnnotation(xytextannotation);
    	/*
    	xytextannotation = new XYTextAnnotation("10th", 36.5, 12.493);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("25th", 36.5, 13.313);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("50th", 36.5, 14.33);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("75th", 36.5, 15.478);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("90th", 36.5, 16.642);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("95th", 36.5, 17.408);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	xytextannotation = new XYTextAnnotation("97th", 36.5, 17.936);
    	xytextannotation.setFont(font);
    	xytextannotation.setTextAnchor(TextAnchor.HALF_ASCENT_LEFT);
    	xyplot.addAnnotation(xytextannotation);
    	*/
        return jfreechart;
    }

    /**
     * This method calls the multi line related methods to get the graph plotted.
     *
     * @return The JPanel that contains the graph image.
     */
    public JPanel createMultiLineGraphPanel() {
        JFreeChart jfreechart = createMultiLineGraph(createMultiLineDataset());
        return new ChartPanel(jfreechart);
    }

    /**
     * This class is used by the Bar Graph to give a vertical color shade affect to the bars.
     *
     * @author Kashif Manzoor
     */
    class CustomBarRenderer extends BarRenderer {
        /**
         *
         */
        private static final long serialVersionUID = -1586975003579539764L;
        private Paint[] colors;

        public CustomBarRenderer(Paint[] paints) {
            colors = paints;
        }

        public Paint getItemPaint(int i, int i_0_) {
            return colors[i_0_ % colors.length];
        }
    }


    /**
     * The main method used for testing and debugging.
     *
     public static void main(String[] strings) {
     }*/
}
