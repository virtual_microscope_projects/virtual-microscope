/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.util;


import java.awt.Graphics;
import java.awt.FontMetrics;
import java.awt.Dimension;
import java.util.Vector;
import java.util.Enumeration;

import javax.swing.JLabel;

/**
 * VWrappingLabel is based on Symantec's class WrappingLabel; however,
 * this class can format the text vertically, too.
 * It also wraps text at newlines embedded in the label's text.
 *
 */


public class VWrappingLabel extends JLabel {
    //--------------------------------------------------
    // constants
    //--------------------------------------------------

    //--------------------------------------------------
    // class variables
    //--------------------------------------------------


    /**
     *
     */
    private static final long serialVersionUID = 8629970155207606711L;
    //--------------------------------------------------
    // member variables
    //--------------------------------------------------
    protected String text;
    protected float m_nHAlign;
    protected float m_nVAlign;
    protected int baseline;
    protected FontMetrics fm;


    //--------------------------------------------------
    // constructors
    //--------------------------------------------------

    public VWrappingLabel() {
        this("");
    }

    public VWrappingLabel(String s) {
        this(s, JLabel.LEFT_ALIGNMENT, JLabel.CENTER_ALIGNMENT);
    }

    public VWrappingLabel(String s, float nHorizontal, float nVertical) {
        setText(s);
        setHAlignStyle(nHorizontal);
        setVAlignStyle(nVertical);
    }


    //--------------------------------------------------
    // accessor members
    //--------------------------------------------------

    public float getHAlignStyle() {
        return m_nHAlign;
    }

    public void setHAlignStyle(float a) {
        m_nHAlign = a;
        invalidate();
    }

    public float getVAlignStyle() {
        return m_nVAlign;
    }

    public void setVAlignStyle(float a) {
        m_nVAlign = a;
        invalidate();
    }

    public String getText() {
        return text;
    }

    public void setText(String s) {
        text = s;
        repaint();
    }


    //--------------------------------------------------
    // member methods
    //--------------------------------------------------

    public String paramString() {
        return "";
    }

    public void paint(Graphics g) {
        if (text != null) {
            Dimension d;
            int currentY = 0;
            Vector lines;

            // Set up some class variables
            fm = getFontMetrics(getFont());
            baseline = fm.getMaxAscent();

            // Get the maximum height and width of the current control
            d = getSize();

            lines = breakIntoLines(text, d.width);

            //if (m_nVAlign == V_ALIGN_CENTER)
            if (m_nVAlign == JLabel.CENTER_ALIGNMENT) {
                int center = (d.height / 2);
                currentY = center - ((lines.size() / 2) * fm.getHeight());
            }
            //else if (m_nVAlign == V_ALIGN_BOTTOM)
            else if (m_nVAlign == JLabel.BOTTOM_ALIGNMENT) {
                currentY = d.height - (lines.size() * fm.getHeight());
            }

            ellipsisNeeded(d.height, d.width, fm, lines);
            // now we have broken into substrings, print them
            Enumeration elements = lines.elements();
            while (elements.hasMoreElements()) {
                drawAlignedString(g,
                        (String) (elements.nextElement()),
                        0, currentY, d.width);
                currentY += fm.getHeight();
            }

            // We're done with the font metrics...
            fm = null;
        }
    }

    /**
     * This method sees if the label text spils over the label height. If it does it
     * postfixes the last visible line with '...' and removes the rest of the lines.
     *
     * @param labelHeight
     * @param labelWidth
     * @param fontMetric
     * @param lines
     * @return
     */
    private Vector ellipsisNeeded(int labelHeight, int labelWidth, FontMetrics fontMetric, Vector lines) {
        // do we need ellipsis ?
        int fontHeight = fontMetric.getHeight();
        int possibleLines = labelHeight / fontHeight;
        if (possibleLines >= lines.size())
            return lines; // no we don't.

        // We need ellipsis in the last visible line.
        String lastLine = (String) lines.get(possibleLines - 1);
        String newLine = "";
        int lineWidth = fontMetric.stringWidth(lastLine);
        // decide where should we put the ellipsis.
        if (lineWidth + fontMetric.stringWidth("...") < labelWidth) {
            newLine = lastLine + "...";
        } else if (lineWidth + fontMetric.stringWidth("..") < labelWidth) {
            newLine = lastLine.substring(0, lastLine.length() - 1) + "...";
        } else if (lineWidth + fontMetric.stringWidth(".") < labelWidth) {
            newLine = lastLine.substring(0, lastLine.length() - 2) + "...";
        } else
            newLine = lastLine.substring(0, lastLine.length() - 3) + "...";
        lines.insertElementAt(newLine, possibleLines - 1);
        // remove the rest of the label text as it will not be visible anyways.
        for (int i = possibleLines; i < lines.size(); ) {
            lines.removeElementAt(i);
        }
        return lines;

    }

    protected Vector breakIntoLines(String s, int width) {
        int fromIndex = 0;
        int pos = 0;
        int bestpos;
        String largestString;
        Vector lines = new Vector();

        // while we haven't run past the end of the string...
        while (fromIndex != -1) {
            // Automatically skip any spaces at the beginning of the line
            while (fromIndex < text.length()
                    && text.charAt(fromIndex) == ' ') {
                ++fromIndex;
                // If we hit the end of line
                // while skipping spaces, we're done.
                if (fromIndex >= text.length()) break;
            }

            // fromIndex represents the beginning of the line
            pos = fromIndex;
            bestpos = -1;
            largestString = null;

            while (pos >= fromIndex) {
                boolean bHardNewline = false;
                int newlinePos = text.indexOf('\n', pos);
                int spacePos = text.indexOf(' ', pos);

                if (newlinePos != -1 &&    // there is a newline and either
                        ((spacePos == -1) ||  // 1. there is no space, or
                                (spacePos != -1 &&
                                        newlinePos < spacePos)))
                // 2. the newline is first
                {
                    pos = newlinePos;
                    bHardNewline = true;
                } else {
                    pos = spacePos;
                    bHardNewline = false;
                }

                // Couldn't find another space?
                if (pos == -1) {
                    s = text.substring(fromIndex);
                } else {
                    s = text.substring(fromIndex, pos);
                }

                // If the string fits, keep track of it.
                if (fm.stringWidth(s) < width) {
                    largestString = s;
                    bestpos = pos;

                    // If we've hit the end of the
                    // string or a newline, use it.
                    if (bHardNewline)
                        bestpos++;
                    if (pos == -1 || bHardNewline) break;
                } else {
                    break;
                }

                ++pos;
            }

            if (largestString == null) {
                // Couldn't wrap at a space, so find the largest line
                // that fits and print that.  Note that this will be
                // slightly off -- the width of a string will not necessarily
                // be the sum of the width of its characters, due to kerning.
                int totalWidth = 0;
                int oneCharWidth = 0;

                pos = fromIndex;

                while (pos < text.length()) {
                    oneCharWidth = fm.charWidth(text.charAt(pos));
                    if ((totalWidth + oneCharWidth) >= width) break;
                    totalWidth += oneCharWidth;
                    ++pos;
                }

                lines.addElement(text.substring(fromIndex, pos));
                fromIndex = pos;
            } else {
                lines.addElement(largestString);
                fromIndex = bestpos;
            }
        }

        return lines;
    }


    protected void drawAlignedString(Graphics g,
                                     String s, int x, int y, int width) {
        int drawx;
        int drawy;

        drawx = x;
        drawy = y + baseline;

        if (m_nHAlign != JLabel.LEFT_ALIGNMENT) {
            int sw;

            sw = fm.stringWidth(s);

            if (m_nHAlign == JLabel.CENTER_ALIGNMENT) {
                drawx += (width - sw) / 2;
            } else if (m_nHAlign == JLabel.RIGHT_ALIGNMENT) {
                drawx = drawx + width - sw;
            }
        }

        g.drawString(s, drawx, drawy);
    }
/*
    public static void main(String[] args) {
    	VWrappingLabel label = new VWrappingLabel();
    	label.setText("Hello world my name is kashif manzoor what is your name");
    	label.setForeground(Color.WHITE);
    	label.setBackground(Color.BLACK);
    	label.setHAlignStyle(JLabel.CENTER_ALIGNMENT);
    	label.setVisible(true);
    	JFrame frame = new JFrame("Hello");
    	JPanel panel = new JPanel();
    	panel.setBorder(BorderFactory.createRaisedBevelBorder());
    	panel.setPreferredSize(new Dimension(100,100));
    	panel.add(label);
    	frame.getContentPane().add(label);
    	label.setSize(new Dimension(20,50));
    	frame.pack();
    	frame.setVisible(true);
    }
*/
}