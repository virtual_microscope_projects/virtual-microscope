/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */

package virtuallab.util;

public class Locale {

    public enum LANGUAGE { EN, RO, ES, IT }

    public static String About(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Despre";
            case ES:
                return "Acerca de";
            case IT:
                return "Di";
            default:
                return "About";
        }
    }

    public static String Specimen(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Specimen";
            case ES:
                return "Muestra";
            case IT:
                return "Specimen";
            default:
                return "Specimen";
        }
    }

    public static String MissingAppIcon(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Icoana aplicației nu poate fi găsită";
            case ES:
                return "No se pudo encontrar el icono de la aplicación";
            case IT:
                return "Impossibile trovare l'icona dell'applicazione";
            default:
                return "Could not find the application icon";
        }
    }

    public static String Close(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Închide";
            case ES:
                return "Cerrar";
            case IT:
                return "Chiudi";
            default:
                return "Close";
        }
    }

    public static String Version(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Versiune";
            case ES:
                return "Versión";
            case IT:
                return "Versione";
            default:
                return "Version";
        }
    }

    public static String Icon(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Icoană";
            case ES:
                return "Icono";
            case IT:
                return "Icona";
            default:
                return "Icon";
        }
    }

    public static String Detail(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Detaliu";
            case ES:
                return "Detalle";
            case IT:
                return "Dettaglio";
            default:
                return "Detail";
        }
    }

    public static String Choices(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Opțiuni";
            case ES:
                return "Opciones";
            case IT:
                return "Opzioni";
            default:
                return "Choices";
        }
    }

    public static String Adjust(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Modifică";
            case ES:
                return "Ajustar";
            case IT:
                return "Modifica";
            default:
                return "Adjust";
        }
    }

    public static String UnsupprtedImmageType(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imagine cu format necunoscut";
            case ES:
                return "Tipo de imagen no compatible";
            case IT:
                return "Immagine con formato sconosciuto";
            default:
                return "Unsupported image type";
        }
    }

    public static String InvalidSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție invalidă";
            case ES:
                return "Selección invalida";
            case IT:
                return "Selezione non valida";
            default:
                return "Invalid selection";
        }
    }

    public static String Error(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare";
            case ES:
                return "Error";
            case IT:
                return "Errore";
            default:
                return "Error";
        }
    }

    public static String SelectionBoundaryError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Zona selectată depășește marginea imaginii";
            case ES:
                return "Su selección se encuentra más allá del límite de la imagen";
            case IT:
                return "L'area selezionata supera il bordo dell'immagine";
            default:
                return "Your selection lies beyond the image boundary";
        }
    }

    public static String SelectionBoundaryErrorSupport(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Zona selectată nu are voie să depășească limitele imaginii";
            case ES:
                return "Por favor restringe tu selección a los límites de la imagen";
            case IT:
                return "L'area selezionata non deve superare i limiti dell'immagine";
            default:
                return "Please restrict your selection to the image boundaries";
        }
    }

    public static String Information(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Informație";
            case ES:
                return "Información";
            case IT:
                return "Informazioni";
            default:
                return "Information";
        }
    }

    public static String VizualizationOfArea(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Vizualizare 3D a zonei";
            case ES:
                return "Visualización 3D del área seleccionada";
            case IT:
                return "Vista 3D dell'area";
            default:
                return "3D Visualization of selected area";
        }
    }

    public static String ErrorDisplaying3DImmage(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "A apărut o eroare la afișarea imaginii 3D";
            case ES:
                return "Ocurrió un error inesperado al mostrar la imagen 3D";
            case IT:
                return "Si è verificato un errore durante la visualizzazione dell'immagine 3D";
            default:
                return "An unexpected error occurred while displaying the 3D image";
        }
    }

    public static String TryAgain(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Reîncearcă";
            case ES:
                return "Inténtalo de nuevo";
            case IT:
                return "Riprova";
            default:
                return "Please try again";
        }
    }

    public static String IfErrorPersists(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Dacă eroarea persistă trimite un mail către";
            case ES:
                return "Si el error persiste, por favor envíe un correo electrónico a";
            case IT:
                return "Se l'errore persiste, manda una mail a";
            default:
                return "If the error persists please send an email to";
        }
    }

    public static String ShapeIsNull(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Forma nu poate fi null";
            case ES:
                return "La forma no puede ser nula";
            case IT:
                return "La forma non può essere nulla";
            default:
                return "Shape cannot be null";
        }
    }

    public static String AnnotationNotFound(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nota nu poate fi găsită";
            case ES:
                return "No se puede establecer la ubicación de la anotación";
            case IT:
                return "La nota non può essere trovata";
            default:
                return "Unable to establish location of annotation";
        }
    }

    public static String Annotation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nota";
            case ES:
                return "Anotación";
            case IT:
                return "Nota";
            default:
                return "Annotation";
        }
    }

    public static String Delete(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Șterge";
            case ES:
                return "Borrar";
            case IT:
                return "Elimina";
            default:
                return "Delete";
        }
    }

    public static String AnnotationControlPanel(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Panou control note";
            case ES:
                return "Panel de control de anotación";
            case IT:
                return "Pannello  di controllo";
            default:
                return "Annotation Control Panel";
        }
    }

    public static String AnnotationControlDescription(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Conține controalele folosite la desenarea notelor";
            case ES:
                return "Contiene todos los controles utilizados para dibujar anotaciones";
            case IT:
                return "Contiene i controlli usati per disegnare le note";
            default:
                return "Contains all the controls used to draw annotations";
        }
    }

    public static String NullMicroscopeView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imaginea, specimenul sau numele sunt nule";
            case ES:
                return "Microscopio nulo, espécimen o nombre";
            case IT:
                return "L'immagine, il campione o il nome non sono validi";
            default:
                return "Null microscope view, specimen, or name";
        }
    }

    public static String Save(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Salvează";
            case ES:
                return "Guardar";
            case IT:
                return "Salva";
            default:
                return "Save";
        }
    }

    public static String SaveAs(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Salvează ca";
            case ES:
                return "Guardar como";
            case IT:
                return "Salva come";
            default:
                return "Save as";
        }
    }

    public static String ErrorSaving(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la salvarea";
            case ES:
                return "Error guardando";
            case IT:
                return "Si è verificato un errore durante il salvataggio";
            default:
                return "Error saving";
        }
    }

    public static String AnnotationFile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierului cu nota";
            case ES:
                return "Archivo de anotación";
            case IT:
                return "Il file con la nota";
            default:
                return "Annotation file";
        }
    }


    public static String TheAnnotationFile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierul cu nota";
            case ES:
                return "El archivo de anotaciones";
            case IT:
                return "Il file delle note";
            default:
                return "The annotation file";
        }
    }

    public static String PleaseEnter(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Introdu o";
            case ES:
                return "Por favor ingrese un";
            case IT:
                return "Inseriscilo";
            default:
                return "Please enter a";
        }
    }

    public static String ShortDescription(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Scurtă descriere";
            case ES:
                return "Breve descripción";
            case IT:
                return "Breve descrizione";
            default:
                return "Short description";
        }
    }

    public static String InvalidShortDescription(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descriere invalidă";
            case ES:
                return "Descripción breve no válida";
            case IT:
                return "Descrizione non valida";
            default:
                return "Invalid short description";
        }
    }

    public static String NotSaved(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu va fi salvată";
            case ES:
                return "No será salvo";
            case IT:
                return "Non sarà salvato";
            default:
                return "Will not be saved";
        }
    }

    public static String ConfirmSavingAnnotation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Vrei să salvezi schimbările la notă";
            case ES:
                return "¿Quieres guardar los cambios de anotación?";
            case IT:
                return "Vuoi salvare le modifiche alla nota";
            default:
                return "Do you want to save annotation changes";
        }
    }

    public static String Importing(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "La importarea";
            case ES:
                return "Importador";
            case IT:
                return "Durante l'importazione";
            default:
                return "Importing";
        }
    }

    public static String InvalidAnnotationFile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Formatul fișierului nu este corect sau nota conținută nu se aplică specimenului";
            case ES:
                return "El archivo es un archivo de anotación no válido o no se aplica a la muestra cargada";
            case IT:
                return "Il formato del file non è corretto o la nota contenuta non si applica al campione";
            default:
                return "The file is either an invalid annotation file or it does not apply to the loaded specimen";
        }
    }

    public static String Opening(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "La deschiderea";
            case ES:
                return "Apertura";
            case IT:
                return "All'apertura";
            default:
                return "Opening";
        }
    }

    public static String To(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "În";
            case ES:
                return "A";
            case IT:
                return "In";
            default:
                return "To";
        }
    }

    public static String CantCloseInputStream(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu poate fi închis fișierul sursă";
            case ES:
                return "No se puede cerrar la secuencia de entrada de archivo para el archivo";
            case IT:
                return "Il file sorgente non può essere chiuso";
            default:
                return "Unable to close file input stream for file";
        }
    }

    public static String CantCloseOutputStream(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu poate fi închis fișierul destinație";
            case ES:
                return "No se puede cerrar la secuencia de salida de archivo para el archivo";
            case IT:
                return "Il file di destinazione non può essere chiuso";
            default:
                return "Unable to close file output stream for file";
        }
    }

    public static String NoAnnotationsToExport(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu există note care să fie exportate";
            case ES:
                return "No hay anotaciones para exportar";
            case IT:
                return "Non ci sono note da esportare";
            default:
                return "There are no annotations to export";
        }
    }

    public static String LoadOrDrawAnnotations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Trebuie să încarci sau să desenezi note ca exportul să meargă";
            case ES:
                return "Debes dibujar o cargar algunas anotaciones antes de exportar";
            case IT:
                return "Devi caricare o disegnare note per fare andare l'esportazione";
            default:
                return "You must draw or load some annotations before exporting";
        }
    }

    public static String Deleting(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "La ștergerea";
            case ES:
                return "Borrando";
            case IT:
                return "Eliminazione";
            default:
                return "Deleting";
        }
    }

    public static String IsWriteProtected(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Este protejat împotriva scrierii";
            case ES:
                return "Está protegido contra escritura";
            case IT:
                return "È protetto da scrittura";
            default:
                return "Is write protected";
        }
    }

    public static String ApplicableAnnotations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Note aplicabile specimenului încărcat";
            case ES:
                return "Anotaciones aplicables al espécimen cargado";
            case IT:
                return "Annotazioni applicabili al campione caricato";
            default:
                return "Annotations applicable to loaded specimen";
        }
    }

    public static String LoadSpecimenAnnotations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Încarcă notele specimenului";
            case ES:
                return "Cargar anotaciones de especímenes";
            case IT:
                return "Carica annotazioni del  campione";
            default:
                return "Load specimen anotations";
        }
    }

    public static String CantLoadAnnotations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu s-au putut încărca adnotări din";
            case ES:
                return "No se pudieron cargar las anotaciones de";
            case IT:
                return "Impossibile caricare annotazioni da";
            default:
                return "Could not load annotations from";
        }
    }

    public static String ChooseColor(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Alege culoare pentru";
            case ES:
                return "Alegeți culoarea pentru";
            case IT:
                return "Scegli il colore per";
            default:
                return "Choose color for ";
        }
    }

    public static String RED(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "ROȘU";
            case ES:
                return "ROJO";
            case IT:
                return "ROSSO";
            default:
                return "RED";
        }
    }

    public static String GREEN(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "VERDE";
            case ES:
                return "VERDE";
            case IT:
                return "VERDE";
            default:
                return "GREEN";
        }
    }

    public static String BLUE(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "ALBASTRU";
            case ES:
                return "AZUL";
            case IT:
                return "BLU";
            default:
                return "BLUE";
        }
    }

    public static String Reset(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Resetează";
            case ES:
                return "Reiniciar";
            case IT:
                return "Reset";
            default:
                return "Reset";
        }
    }

    public static String Cancel(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Anulează";
            case ES:
                return "Cancelar";
            case IT:
                return "Annulla";
            default:
                return "Cancel";
        }
    }

    public static String SetAnnotationColor(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Setează culoarea notei";
            case ES:
                return "Establecer color de anotación";
            case IT:
                return "Imposta il colore della nota";
            default:
                return "Set annotation color";
        }
    }

    public static String Annotations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Note";
            case ES:
                return "Anotaciones";
            case IT:
                return "Note";
            default:
                return "Annotations";
        }
    }

    public static String Select3DArea(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selectează zona de imagine care va fi afișată în 3D";
            case ES:
                return "Seleccione el área de imagen AFM para ver en 3D";
            case IT:
                return "Seleziona l'area dell'immagine da visualizzare in 3D";
            default:
                return "Select the AFM image area to view in 3D";
        }
    }

    public static String RightClick(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Click dreapta";
            case ES:
                return "Click derecho";
            case IT:
                return "Click destro";
            default:
                return "Right click";
        }
    }

    public static String LeftClick(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Click stânga";
            case ES:
                return "Click izquierdo";
            case IT:
                return "Click sinistra";
            default:
                return "Left click";
        }
    }

    public static String ForPersistentMode(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Pentru a activa modul persistent";
            case ES:
                return "Para modo persistente";
            case IT:
                return "Per abilitare la modalità persistente";
            default:
                return "For persistent mode";
        }
    }

    public static String ForVolatileMode(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Pentru a activa modul volatil";
            case ES:
                return "Para modo volátil";
            case IT:
                return "Per abilitare la modalità volatile";
            default:
                return "For volatile mode";
        }
    }

    public static String AvailableColormaps(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Culori disponibile";
            case ES:
                return "Mapas de colores disponibles";
            case IT:
                return "Colori disponibili";
            default:
                return "Available colormaps";
        }
    }

    public static String EditConfiguration(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Editează configurația";
            case ES:
                return "Editar configuración";
            case IT:
                return "Modifica la configurazione";
            default:
                return "Edit configuration";
        }
    }

    public static String NoConfigurationSpecified(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nici un obiect de configurație specificat";
            case ES:
                return "Ningún objeto de configuración especificado";
            case IT:
                return "Nessun oggetto di configurazione specificato";
            default:
                return "No configuration object specified";
        }
    }

    public static String Apply(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Aplică";
            case ES:
                return "Aplicar";
            case IT:
                return "Applicare";
            default:
                return "Apply";
        }
    }

    public static String InvalidConfiguration(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Configurație invalidă";
            case ES:
                return "Configuración inválida";
            case IT:
                return "Configurazione non valida";
            default:
                return "Invalid Configuration";
        }
    }

    public static String SpecimenDirectory(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Director cu specimene";
            case ES:
                return "Directorio de muestras";
            case IT:
                return "Cartella dei campioni";
            default:
                return "Specimen directory";
        }
    }

    public static String TheSpecimensDirectory(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Directorul de unde se citesc specimenele";
            case ES:
                return "El directorio desde donde se leen las especificaciones";
            case IT:
                return "La cartella da dove vengono letti gli esemplari";
            default:
                return "The directory from where the speciemns are read";
        }
    }

    public static String AnnotationsDirectory(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Directorul cu note";
            case ES:
                return "Directorio de anotaciones";
            case IT:
                return "Cartella delle annotazioni";
            default:
                return "Annotations directory";
        }
    }

    public static String TheAnnotationDirectory(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Directorul unde sunt stocate notele";
            case ES:
                return "El directorio donde se almacenan las anotaciones";
            case IT:
                return "La cartella in cui sono archiviate le annotazioni";
            default:
                return "The directory where the annotations are stored";
        }
    }

    public static String JDKcheck(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Verificare JDK";
            case ES:
                return "Realizar la comprobación de JDK";
            case IT:
                return "Eseguire il controllo JDK";
            default:
                return "Perform JDK check";
        }
    }

    public static String JDKcheckBox(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Bifa care indică dacă compatibilitatea cu JDK trebuie verificată";
            case ES:
                return "La bandera que determina si la aplicación debe verificar la compatibilidad con JDK";
            case IT:
                return "Il flag che determina se l'applicazione deve verificare la compatibilità con JDK";
            default:
                return "The flag that determines if the application should check for JDK compatibility";
        }
    }

    public static String SpecimenScrollSpeed(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Viteza de navigare în specimen";
            case ES:
                return "Velocidad de desplazamiento de la muestra";
            case IT:
                return "Velocità di scorrimento del campione";
            default:
                return "Specimen Scroll Speed";
        }
    }

    public static String SpecimenScrollSpeedTip(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Viteza cu care speciemnul se va deplasa când utilizatortul va naviga folosing săgețile";
            case ES:
                return "La velocidad con la que se desplazará la muestra cuando el usuario navega utilizando las teclas de flecha";
            case IT:
                return "La velocità con cui il campione verrà scostato quando l'utente naviga utilizzando i tasti freccia";
            default:
                return "The speed with which the specimen will be scrolled when the user navigates using the arrow keys";
        }
    }

    public static String CreditsToOriginalDevelopers(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Credit oferit dezvoltatorilor inițiali";
            case ES:
                return "Créditos a los desarrolladores originales";
            case IT:
                return "Crediti per gli sviluppatori originali";
            default:
                return "Credits to the original developers";
        }
    }

    public static String ThisIsAFork(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Această aplicatie este derivată din proiectul original Virtual Microscope, dezvoltat de Imaging Technology Group, Beckman for Advanced Science and Technology, la universitatea din Illinois la Urbana-Champaign.";
            case ES:
                return "Este trabajo es una bifurcación del Microscopio Virtual original, un proyecto del Grupo de Tecnología de Imagenología, Instituto Beckman para Ciencia y Tecnología Avanzadas, en la Universidad de Illinois en Urbana-Champaign.";
            case IT:
                return "Questo lavoro è un fork del microscopio virtuale originale, un progetto del Imaging Technology Group, Beckman Institute for Advanced Science and Technology, presso l'Università dell'Illinois a Urbana-Champaign.";
            default:
                return "This work is a fork of the original Virtual Microscope, a project of the Imaging Technology Group, Beckman Institute for Advanced Science and Technology, at the University of Illinois at Urbana-Champaign.";
        }
    }

    public static String SupporitingMaterials(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Virtual Microscope și materialele adiacente sunt componente ale proiectului Virtual Laboratory finanțat de NASA";
            case ES:
                return "El microscopio virtual y sus materiales de apoyo son componentes del Proyecto de Laboratorio Virtual financiado por la NASA.";
            case IT:
                return "Il microscopio virtuale ei suoi materiali di supporto sono componenti del progetto di laboratorio virtuale finanziato dalla NASA";
            default:
                return "The Virtual Microscope and its supporting materials are components of the Virtual Laboratory Project funded by NASA.";
        }
    }

    public static String MoreInformation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Mai multe informații despre Virtual Microscope sunt disponibile la http://virtual.itg.uiuc.edu.";
            case ES:
                return "Más información sobre el microscopio virtual está disponible en http://virtual.itg.uiuc.edu.";
            case IT:
                return "Ulteriori informazioni sul microscopio virtuale sono disponibili su http://virtual.itg.uiuc.edu.";
            default:
                return "More information about the Virtual Microscope is available at http://virtual.itg.uiuc.edu.";
        }
    }

    public static String CreditsToContributors(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Credit oferit contributorilor";
            case ES:
                return "Créditos a los contribuyentes";
            case IT:
                return "Crediti per i contributori";
            default:
                return "Credits to contributors";
        }
    }

    public static String CreditForLogo(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Traducerea și emblema aplicației sunt disponibile datorită eforturilor echipei Academix. Pagină web:";
            case ES:
                return "La traducción y el logotipo están disponibles gracias a los esfuerzos del equipo de Academix. Sitio web:";
            case IT:
                return "La traduzione e il logo del software sono disponibili grazie agli sforzi del team Academix. Sito web:";
            default:
                return "The translation and logo are available thanks to Academix team efforts. Website:";
        }
    }

    public static String Yes(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Da";
            case ES:
                return "Sí";
            case IT:
                return "Si";
            default:
                return "Yes";
        }
    }

    public static String No(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu";
            case ES:
                return "No";
            case IT:
                return "No";
            default:
                return "No";
        }
    }

    public static String OpenInSameWindow(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Deschide în aceeași fereastră";
            case ES:
                return "Abrir en la misma ventana";
            case IT:
                return "Apri nella stessa finestra";
            default:
                return "Open in same window";
        }
    }

    public static String AppendToSameWindow(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Adauga la aceeași fereastră";
            case ES:
                return "Anexar a la misma ventana";
            case IT:
                return "Aggiungi alla stessa finestra";
            default:
                return "Append to same window";
        }
    }

    public static String MySpecimenCollection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Colecția mea de specimene";
            case ES:
                return "Mi coleccion de muestras";
            case IT:
                return "La mia collezione di campioni";
            default:
                return "My specimen collection";
        }
    }

    public static String ClickToView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Apasă pentru a vizualiza";
            case ES:
                return "Click para ver";
            case IT:
                return "Clicca per vedere";
            default:
                return "Click to view";
        }
    }

    public static String NoSpecimensDownloaded(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nici un specimen nu a fost descărcat";
            case ES:
                return "Todavía no se han descargado ejemplares";
            case IT:
                return "Nessun esemplare è stato ancora scaricato";
            default:
                return "No specimens have been downloaded yet";
        }
    }

    public static String GetMoreSpecimens(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descarcă mai multe specimene";
            case ES:
                return "Consigue más ejemplares";
            case IT:
                return "Ottieni più campioni";
            default:
                return "Get more specimens";
        }
    }

    public static String ClickToDownload(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Apasă pentru a descărca";
            case ES:
                return "Haga clic para descargar";
            case IT:
                return "Clicca per scaricare";
            default:
                return "Click to download";
        }
    }

    public static String NoMoreSpecimensAvailable(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu mai sunt specimene disponibile";
            case ES:
                return "No hay más ejemplares disponibles";
            case IT:
                return "Non più campioni disponibili";
            default:
                return "No more specimens available";
        }
    }

    public static String NoDescriptionFound(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descrierea nu poate fi găsită";
            case ES:
                return "No se ha encontrado ninguna descripción";
            case IT:
                return "La descrizione non può essere trovata";
            default:
                return "No description found";
        }
    }

    public static String DownloadSize(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Mărimea fișierului de descărcat";
            case ES:
                return "Tamaño descarga";
            case IT:
                return "Dimensione del file da scaricare";
            default:
                return "Download Size";
        }
    }

    public static String Toolbar(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unelte";
            case ES:
                return "Barra de herramientas";
            case IT:
                return "Toolbar";
            default:
                return "Toolbar";
        }
    }

    public static String DistanceMeasure(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Măsurare distanță";
            case ES:
                return "Medida de distancia";
            case IT:
                return "Misura la distanza";
            default:
                return "Distance measure";
        }
    }

    public static String DeleteDistanceMeasure(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Șterge măsurătoarea de distanță";
            case ES:
                return "Eliminar medida de distancia";
            case IT:
                return "Rimuove il misuratore di distanza";
            default:
                return "Delete distance measure";
        }
    }

    public static String BaseImage(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imagine de bază";
            case ES:
                return "Imagen base";
            case IT:
                return "Immagine di base";
            default:
                return "Base image";
        }
    }

    public static String SelectBaseImage(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selectează imaginea de bază";
            case ES:
                return "Seleccionar imagen base";
            case IT:
                return "Seleziona l'immagine di base";
            default:
                return "Select base image";
        }
    }

    public static String DetectorIncompatibleWithSpecimen(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Funcția nouă a detectorului nu este compatibilă cu specimenul în format vechi";
            case ES:
                return "La nueva funcionalidad del detector NO es compatible con la muestra antigua";
            case IT:
                return "La nuova funzione del rilevatore non è compatibile con il vecchio campione";
            default:
                return "The detector's new functionality is NOT compatible with the old specimen";
        }
    }

    public static String Opacity(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Opacitate";
            case ES:
                return "Opacidad";
            case IT:
                return "Opacità";
            default:
                return "Opacity";
        }
    }

    public static String OpacitySlider(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Mută cursorul pentru a ajusta opacitatea";
            case ES:
                return "Mueve el deslizador para ajustar la opacidad";
            case IT:
                return "Muovi il cursore per regolare l'opacità";
            default:
                return "Move the slider to adjust the opacity";
        }
    }

    public static String ComparativeAnalysis(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Analiză comparativă";
            case ES:
                return "Análisis comparativo";
            case IT:
                return "Analisi comparativa";
            default:
                return "Comparative analysis";
        }
    }

    public static String Elements(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Elemente";
            case ES:
                return "Elementos";
            case IT:
                return "Elementi";
            default:
                return "Elements";
        }
    }

    public static String PointSelectionTool(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unealtă de selecție tip punct";
            case ES:
                return "Herramienta de selección de puntos";
            case IT:
                return "Strumento di selezione dei punti";
            default:
                return "Point selection tool";
        }
    }

    public static String AreaSelectionTool(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unealtă de selecție tip zonă";
            case ES:
                return "Herramienta de selección de área";
            case IT:
                return "Strumento di selezione delle zone";
            default:
                return "Area selection tool";
        }
    }

    public static String LineSelectionTool(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unealtă de selecție tip linie";
            case ES:
                return "Línea de selección de área";
            case IT:
                return "Strumento di selezione delle linie";
            default:
                return "Area selection line";
        }
    }

    public static String HeightMeasure(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Măsurare înălțime";
            case ES:
                return "Medida de altura";
            case IT:
                return "Misura l'altezza";
            default:
                return "Height measure";
        }
    }

    public static String Height(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Înălțime";
            case ES:
                return "Altura";
            case IT:
                return "Altezza";
            default:
                return "Height";
        }
    }

    public static String DeleteHeightMeasure(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Șterge măsurătoarea de înălțime";
            case ES:
                return "Eliminar medida de altura";
            case IT:
                return "Elimina la misurazione dell'altezza";
            default:
                return "Delete height measure";
        }
    }

    public static String MissingHeightScalingInFile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu a fost găsită informație de scalare a înălțimii în fișierul de specimen";
            case ES:
                return "No se encontró información de escala de altura en el archivo de muestra";
            case IT:
                return "Nessuna informazione di ridimensionamento dell'altezza è stata trovata nel file campione";
            default:
                return "No height scaling information found in specimen file";
        }
    }

    public static String Instrument(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Instrument";
            case ES:
                return "Instrumento";
            case IT:
                return "Strumento";
            default:
                return "Instrument";
        }
    }

    public static String SpecimenThumbnail(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imaginea în miniatură a specimenului";
            case ES:
                return "Miniatura de muestra";
            case IT:
                return "L'immagine in miniatura dell'esemplare";
            default:
                return "Specimen thumbnail";
        }
    }

    public static String MissingThumbnail(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imaginea în miniatură nu a fost găsită";
            case ES:
                return "No se ha encontrado una miniatura";
            case IT:
                return "Immagine di anteprima non trovata";
            default:
                return "No thumbnail found";
        }
    }

    public static String ErrorOpeningBrowser(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare la deschiderea navigatorului de internet";
            case ES:
                return "Error al abrir el navegador";
            case IT:
                return "Errore durante l'apertura del browser Internet";
            default:
                return "Error opening browser";
        }
    }

    public static String ConfigurationError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Posibil să nu fie configurat corect";
            case ES:
                return "Puede que no esté configurado correctamente";
            case IT:
                return "Potrebbe non essere configurato correttamente";
            default:
                return "It may not be configured properly";
        }
    }

    public static String CouldNotLocate(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu se poate localiza";
            case ES:
                return "No se pudo localizar";
            case IT:
                return "Impossibile localizzare";
            default:
                return "Could not locate";
        }
    }

    public static String Folder(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Director";
            case ES:
                return "Carpeta";
            case IT:
                return "Cartella";
            default:
                return "Folder";
        }
    }

    public static String VirtuallabExecutableIsPresent(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Verifică că sub directorul este prezent și conține fișierul VirtualLab.jar";
            case ES:
                return "Asegúrese de que la subcarpeta esté presente y que contenga el archivo VirtualLab.jar";
            case IT:
                return "Verificare che la directory sia presente e contenga il file VirtualLab.jar";
            default:
                return "Make sure the sub-folder is present and that it contains the file VirtualLab.jar";
        }
    }

    public static String VirtuallabExecutableNotFound(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Executabilul VirtualLab.jar nu poate fi găsit în directorul indicat";
            case ES:
                return "No se pudo localizar el archivo VirtualLab.jar en el directorio designado";
            case IT:
                return "Impossibile trovare VirtualLab.jar eseguibile nella directory specificata";
            default:
                return "Could not locate the VirtualLab.jar file in the designated directory";
        }
    }

    public static String DownloadTheVirtualLabExecutable(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Poți descărca ultima versiune a aplicației Virtual Microscope de la locația";
            case ES:
                return "Por favor descargue la última aplicación de Microscopio Virtual de";
            case IT:
                return "Puoi scaricare l'ultima versione di Virtual Microscope dalla tua posizione";
            default:
                return "Please download the latest Virtual Microscope application from";
        }
    }
    public static String Warning(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Avertizare";
            case ES:
                return "Advertencia";
            case IT:
                return "Avvertimento";
            default:
                return "Warning";
        }
    }

    public static String IfTheProblemPersists(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Dacă problema persistă poți să ne contactezi la";
            case ES:
                return "Si el problema persiste, por favor contáctenos en";
            case IT:
                return "Se il problema persiste, puoi contattarci all'indirizzo";
            default:
                return "If the problem persists please contact us at";
        }
    }

    public static String ErrorWhileSearchingForTheVirtuallab(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Următoarea eroare a apărut în timp ce se căuta executabilul Virtuallab, în directorul curent,";
            case ES:
                return "Se produjo el siguiente error al intentar localizar el archivo ejecutable de VirtualLab, en el directorio actual,";
            case IT:
                return "Si è verificato il seguente errore durante la ricerca dell'eseguibile Virtuallab nella directory corrente,";
            default:
                return "The following error occurred while trying to locate the VirtualLab executable file, in the current directory,";
        }
    }

    public static String LicenseVirtualMicroscope(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Licența Virtual Microscope";
            case ES:
                return "Licencia de Microscopio virtual";
            case IT:
                return "Licenza Virtual Microscope";
            default:
                return "License Virtual Microscope";
        }
    }

    public static String SetLineWidth(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Setează grosimea liniei";
            case ES:
                return "Establecer ancho de línea";
            case IT:
                return "Imposta lo spessore della linea";
            default:
                return "Set line width";
        }
    }

    public static String OnePixelWideLine(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Line de un pixel grosime";
            case ES:
                return "Línea de un píxel de ancho";
            case IT:
                return "Linea di spessore di un pixel";
            default:
                return "One pixel wide line";
        }
    }

    public static String OnePixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție cu linie de grosime un pixel";
            case ES:
                return "Selección de ancho de línea de un píxel";
            case IT:
                return "Selezione con una linea di spessore di un pixel";
            default:
                return "One pixel line width selection";
        }
    }

    public static String UsedForOnePixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se folosește pentru a seta grosimea liniei la un pixel";
            case ES:
                return "Se utiliza para establecer el ancho de línea en un píxel";
            case IT:
                return "È usato per impostare lo spessore della linea su un pixel";
            default:
                return "It is used to set the line width to one pixel";
        }
    }

    public static String TwoPixelWideLine(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Line de doi pixeli grosime";
            case ES:
                return "Línea de dos píxeles de ancho";
            case IT:
                return "Linea di due pixel di spessore";
            default:
                return "Two pixel wide line";
        }
    }

    public static String TwoPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție cu linie de grosime doi pixeli";
            case ES:
                return "Selección de ancho de línea de dos píxeles";
            case IT:
                return "Selezione con una linea spessa due pixel";
            default:
                return "Two pixel line width selection";
        }
    }

    public static String UsedForTwoPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se folosește pentru a seta grosimea liniei la doi pixeli";
            case ES:
                return "Se utiliza para establecer el ancho de línea en dos píxeles";
            case IT:
                return "È usato per impostare lo spessore della linea a due pixel";
            default:
                return "It is used to set the line width to two pixels";
        }
    }

    public static String FourPixelWideLine(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Line de patru pixeli grosime";
            case ES:
                return "Línea ancha de cuatro píxeles";
            case IT:
                return "Linea di quattro pixel di spessore";
            default:
                return "Four pixel wide line";
        }
    }

    public static String FourPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție cu linie de grosime patru pixeli";
            case ES:
                return "Selección de ancho de línea de cuatro píxeles";
            case IT:
                return "Selezione con una linea di spessore di quattro pixel";
            default:
                return "Four pixel line width selection";
        }
    }

    public static String UsedForFourPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se folosește pentru a seta grosimea liniei la patru pixeli";
            case ES:
                return "Se utiliza para establecer el ancho de línea a cuatro píxeles";
            case IT:
                return "È usato per impostare la larghezza della linea su quattro pixel";
            default:
                return "It is used to set the line width to four pixels";
        }
    }

    public static String SixPixelWideLine(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Line de șase pixeli grosime";
            case ES:
                return "Línea de seis píxeles de ancho";
            case IT:
                return "Linea di sei pixel di spessore";
            default:
                return "Six pixel wide line";
        }
    }

    public static String SixPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție cu linie de grosime șase pixeli";
            case ES:
                return "Selección de ancho de línea de seis píxeles";
            case IT:
                return "Selezione linea a sei pixel";
            default:
                return "Six pixel line width selection";
        }
    }

    public static String UsedForSixPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se folosește pentru a seta grosimea liniei la șase pixeli";
            case ES:
                return "Se utiliza para establecer el ancho de línea a seis píxeles";
            case IT:
                return "È usato per impostare la larghezza della linea su sei pixel";
            default:
                return "It is used to set the line width to six pixels";
        }
    }

    public static String EightPixelWideLine(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Line de opt pixeli grosime";
            case ES:
                return "Línea de ocho píxeles de ancho";
            case IT:
                return "Linea di otto pixel di spessore";
            default:
                return "Eight pixel wide line";
        }
    }

    public static String EightPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecție cu linie de grosime opt pixeli";
            case ES:
                return "Selección de ancho de línea de ocho píxeles";
            case IT:
                return "Selezione con una linea di spessore di otto pixel";
            default:
                return "Eight pixel line width selection";
        }
    }

    public static String UsedForEightPixelLineWidthSelection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se folosește pentru a seta grosimea liniei la opt pixeli";
            case ES:
                return "Se utiliza para establecer el ancho de línea a ocho píxeles";
            case IT:
                return "È usato per impostare lo spessore della linea su otto pixel";
            default:
                return "It is used to set the line width to eight pixels";
        }
    }

    public static String CouldntCreateLog(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierul de înregistrare nu poate fi creat";
            case ES:
                return "No se pudo crear el archivo de registro";
            case IT:
                return "Il file di registrazione non può essere creato";
            default:
                return "Could not create log file";
        }
    }

    public static String DescriptirMiscroscopeOrNameAreNull(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descrierea controlului, microscopul sau numele sunt nule";
            case ES:
                return "Descriptor de control nulo, microscopio o nombre";
            case IT:
                return "Descrizione del controllo, microscopio o nome non validi";
            default:
                return "Null control descriptor, microscope, or name";
        }
    }

    public static String InvalidTilePath(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Calea către imagine este invalidă";
            case ES:
                return "Ruta de título no válida";
            case IT:
                return "Il percorso dell'immagine non è valido";
            default:
                return "Invalid title path";
        }
    }

    public static String CouldntLocateFileSeparator(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nici un separator de fișier nu poate fi localizat";
            case ES:
                return "No se pudo localizar ningún separador de archivos";
            case IT:
                return "Nessun separatore di file può essere localizzato";
            default:
                return "Could not locate any file separator";
        }
    }

    public static String CouldntLocateTrailing(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Elementul nu poate fi localizat";
            case ES:
                return "No se pudo localizar el rastro";
            case IT:
                return "L'articolo non può essere localizzato";
            default:
                return "Could not locate the trailing";
        }
    }

    public static String RightClickToChangeColor(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Click dreapta pentru a schimba culoarea la";
            case ES:
                return "Haga clic derecho para cambiar el color de";
            case IT:
                return "Fare clic con il tasto destro per cambiare colore in";
            default:
                return "Right click to change color for";
        }
    }

    public static String LeftClickToToggleElement(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Click stânga pentru a alege alt element";
            case ES:
                return "Clic izquierdo para alternar selección de elementos";
            case IT:
                return "Click sinistra per scegliere un altro oggetto";
            default:
                return "Left click to toggle element selection";
        }
    }

    public static String ProblemReport(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Raportează o problemă";
            case ES:
                return "Reportar un problema";
            case IT:
                return "Segnala un problema";
            default:
                return "Report a problem";
        }
    }

    public static String OpenEmailClient(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Deschide aplicația de email instalată";
            case ES:
                return "Abrir la aplicación de correo electrónico instalada";
            case IT:
                return "Apre l'applicazione di posta elettronica installata";
            default:
                return "Open installed email application";
        }
    }

    public static String Write(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Scrie";
            case ES:
                return "Escribir";
            case IT:
                return "Scrivi";
            default:
                return "Write";
        }
    }

    public static String IssueTitle(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Scrie titlul erorii";
            case ES:
                return "Escribe el título del error";
            case IT:
                return "Scrivi il titolo dell'errore";
            default:
                return "Write an issue title";
        }
    }

    public static String IssueDescription(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descrie eroarea";
            case ES:
                return "Describa el problema";
            case IT:
                return "Descrive l'errore";
            default:
                return "Describe the issue";
        }
    }

    public static String ErrorOpeningEmail(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la deschiderea aplicației de email";
            case ES:
                return "Error al abrir la aplicación de correo electrónico";
            case IT:
                return "Si è verificato un errore durante l'apertura dell'applicazione di posta elettronica";
            default:
                return "Error opening email application";
        }
    }

    public static String ReportIssues(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroarea poate fi raportată prin mail către";
            case ES:
                return "Por favor informe problemas enviando un correo electrónico a";
            case IT:
                return "L'errore può essere segnalato per posta a";
            default:
                return "Please report issues by sending an email to";
        }
    }

    public static String Unload(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descarcă";
            case ES:
                return "Descargar";
            case IT:
                return "Scarica";
            default:
                return "Unload";
        }
    }

    public static String FileName(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nume fișier";
            case ES:
                return "Nombre del archivo";
            case IT:
                return "Nome del file";
            default:
                return "File name";
        }
    }

    public static String TheSpecimenFile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierul cu specimen";
            case ES:
                return "El archivo de especimen";
            case IT:
                return "File con il campione";
            default:
                return "The specimen file";
        }
    }

    public static String IsInvalid(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Este invalid";
            case ES:
                return "No es válido";
            case IT:
                return "Non è valido";
            default:
                return "Is invalid";
        }
    }

    public static String MissingManifestInSpecimenArchive(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Arhiva cu specimen nu conține un manifest";
            case ES:
                return "El archivo del espécimen no contiene manifiesto";
            case IT:
                return "L'archivio campioni non contiene un manifest";
            default:
                return "Specimen archive contains no manifest";
        }
    }

    public static String MissingDefinitionInSpecimenArchive(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Arhiva cu specimen nu conține un fișier tip definiție";
            case ES:
                return "Falta el archivo de definición de la muestra en el archivo";
            case IT:
                return "L'archivio campioni non contiene un file di definizione";
            default:
                return "Specimen definition file missing from archive";
        }
    }

    public static String InputStreamForSpecimenDefinition(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierul de definire a specimenului nu poate fi deschis";
            case ES:
                return "No se puede adquirir el flujo de entrada para la definición de la muestra";
            case IT:
                return "Il file di definizione del campione non può essere aperto";
            default:
                return "Unable to acquire input stream for specimen definition";
        }
    }

    public static String ErrorReadingSpecimenDefinition(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la citirea definiției specimenului";
            case ES:
                return "Error al intentar leer la definición de la muestra";
            case IT:
                return "Si è verificato un errore durante la lettura della definizione del campione";
            default:
                return "Error attempting to read specimen definition";
        }
    }

    public static String ErrorOpenningSpecimenDefinition(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la deschiderea fișierului de definiție a specimenului";
            case ES:
                return "Error al intentar abrir la muestra";
            case IT:
                return "Si è verificato un errore durante l'apertura del file di definizione del campione";
            default:
                return "Error while attempting to open specimen";
        }
    }

    public static String RequestedEntry(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Elementul cerut";
            case ES:
                return "Entrada solicitada";
            case IT:
                return "Elemento richiesto";
            default:
                return "Requested entry";
        }
    }

    public static String DoesntExist(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu există";
            case ES:
                return "No existe";
            case IT:
                return "Non esiste";
            default:
                return "Does not exist";
        }
    }

    public static String InputStream(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișierul nu poate fi deschis";
            case ES:
                return "No se puede adquirir el flujo de entrada";
            case IT:
                return "Il file non può essere aperto";
            default:
                return "Unable to acquire input stream";
        }
    }

    public static String ErrorInSpecimenXML(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la validarea definiției XML a specimenului";
            case ES:
                return "Error al validar el archivo XML de la muestra";
            case IT:
                return "Si è verificato un errore durante la convalida della definizione del campione XML";
            default:
                return "Error validating specimen XML file";
        }
    }

    public static String ErrorInXMLparser(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la crearea cititorului XML";
            case ES:
                return "Error al crear el analizador XML";
            case IT:
                return "Si è verificato un errore durante la creazione del lettore XML";
            default:
                return "Error creating XML parser";
        }
    }

    public static String ErrorWehnParsingXML(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la interpretarea definiției XML a specimenului";
            case ES:
                return "Error al analizar el archivo XML de la muestra";
            case IT:
                return "Si è verificato un errore durante l'interpretazione della definizione del campione XML";
            default:
                return "Error parsing specimen XML file";
        }
    }

    public static String IOErrorWhileParsingXML(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare de citire și scriere la parsarea definiției XML a specimenului";
            case ES:
                return "Error de IO al analizar el archivo XML de la muestra";
            case IT:
                return "Leggere e scrivere errori durante l'analisi della definizione del campione XML";
            default:
                return "IO error while parsing specimen XML file";
        }
    }

    public static String TheXMLrevision(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Versiunea fișierului XML";
            case ES:
                return "La revisión XML";
            case IT:
                return "Versione del file XML";
            default:
                return "The XML Revision";
        }
    }

    public static String AllowsOne(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Permite doar o";
            case ES:
                return "Permite exactamente una";
            case IT:
                return "Consentire solo uno";
            default:
                return "Allows exactly one";
        }
    }

    public static String TagInXMLfile(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Etichetă să fie prezent in fișierul XML a specimenului";
            case ES:
                return "Etiqueta para estar presente en el archivo XML de la muestra";
            case IT:
                return "Etichetta presente nel file XML esemplare";
            default:
                return "Tag to be present in the specimen XML file";
        }
    }

    public static String SpecimenCantBeLoaded(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Specimenul nu poate fi încărcat";
            case ES:
                return "El espécimen no puede ser cargado";
            case IT:
                return "Il campione non può essere caricato";
            default:
                return "The specimen can not be loaded";
        }
    }

    public static String MalformedXML(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișier XML cu erori";
            case ES:
                return "XML malformado";
            case IT:
                return "File XML con errori";
            default:
                return "Malformed XML";
        }
    }

    public static String ThereShouldBeOnlyOne(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Ar trebui sa fie un singur";
            case ES:
                return "Debe haber exactamente uno";
            case IT:
                return "Dovrebbe essere uno";
            default:
                return "There should be exactly one";
        }
    }

    public static String InXML(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "În XML";
            case ES:
                return "En el XML";
            case IT:
                return "In XML";
            default:
                return "In the XML";
        }
    }

    public static String NullParameterForSortControlStates(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Funcția sortControlStates nu suportă paramterii tip null";
            case ES:
                return "Nulo pasado a sortControlStates ()";
            case IT:
                return "La funzione sortControlStates non supporta i parametri null";
            default:
                return "Null passed to sortControlStates()";
        }
    }

    public static String Using(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Folosind";
            case ES:
                return "Utilizando";
            case IT:
                return "Utilizzando";
            default:
                return "Using";
        }
    }

    public static String Download(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descarcă";
            case ES:
                return "Descargar";
            case IT:
                return "Scarica";
            default:
                return "Download";
        }
    }

    public static String NoThumbnail(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nici o imagine în miniatură";
            case ES:
                return "Sin miniatura";
            case IT:
                return "Nessuna immagine in miniatura";
            default:
                return "No Thumbnail";
        }
    }

    public static String UserCancelDownload(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Utilizatorul a anulat descărcarea";
            case ES:
                return "Cancelar descarga solicitada por el usuario";
            case IT:
                return "L'utente ha annullato il download";
            default:
                return "Cancel download requested by user";
        }
    }

    public static String NoInternetConnection(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Conexiunea la internet lipsește";
            case ES:
                return "No estas conectado a internet";
            case IT:
                return "Manca la connessione Internet";
            default:
                return "You are not connected to the internet";
        }
    }

    public static String ResumeAfterCoonectingToInternet(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Încearcă din nou după ce conexiunea la internet devine disponibilă";
            case ES:
                return "Asegúrese de estar conectado a Internet y vuelva a intentarlo";
            case IT:
                return "Riprovare una volta che la connessione Internet diventa disponibile";
            default:
                return "Please make sure you are connected to the internet and then try again";
        }
    }

    public static String ConnectivityError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare de conectare";
            case ES:
                return "Error de conectividad";
            case IT:
                return "Errore di connessione";
            default:
                return "Connectivity Error";
        }
    }

    public static String ErrorAtDownloadInitialization(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la inițializarea descărcării";
            case ES:
                return "Se produjo un error al iniciar la descarga";
            case IT:
                return "Si è verificato un errore durante l'inizializzazione del download";
            default:
                return "Error occurred while initiating download";
        }
    }

    public static String CancelDownloadConfirmation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Dorești sa oprești descărcarea";
            case ES:
                return "¿Seguro que quieres cancelar la descarga?";
            case IT:
                return "Vuoi smettere di scaricare";
            default:
                return "Are you sure you want to cancel the download";
        }
    }

    public static String ConfirmDownloadCancellation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Confirmă anularea descărcării";
            case ES:
                return "Confirmar cancelación de descarga";
            case IT:
                return "Conferma la cancellazione del download";
            default:
                return "Confirm download cancellation";
        }
    }

    public static String DownloadManager(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Manager descărcare";
            case ES:
                return "Gestor de descargas";
            case IT:
                return "Download manager";
            default:
                return "Download manager";
        }
    }

    public static String Downloading(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se descarcă";
            case ES:
                return "Descargando";
            case IT:
                return "Scaricando";
            default:
                return "Downloading";
        }
    }

    public static String Of(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Din";
            case ES:
                return "De";
            case IT:
                return "Di";
            default:
                return "Of";
        }
    }

    public static String Completed(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Finalizată";
            case ES:
                return "Terminado";
            case IT:
                return "Completato";
            default:
                return "Completed";
        }
    }

    public static String Cancelled(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Anulată";
            case ES:
                return "Cancelado";
            case IT:
                return "Cancellato";
            default:
                return "Cancelled";
        }
    }

    public static String UnkownErrorWhileDownloading(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare necunoscută apărută în timpul descărcării";
            case ES:
                return "Error desconocido durante la descarga";
            case IT:
                return "Errore sconosciuto durante lo scaricamento";
            default:
                return "Unknown error occurred while downloading";
        }
    }

    public static String LoadingSpecimenFiles(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se încarcă specimenele";
            case ES:
                return "Cargando archivos de muestras";
            case IT:
                return "Caricamento dei file dei campioni";
            default:
                return "Loading specimen files";
        }
    }

    public static String SpecimenDirectoryNameError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Numele directorului cu specimene este null sau gol";
            case ES:
                return "El nombre del directorio de la muestra es nulo o está vacío";
            case IT:
                return "Il nome della directory del campione è nullo o vuoto";
            default:
                return "Specimen directory name is null or empty";
        }
    }

    public static String SpecimenDirectoryDoesntExist(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Directorul cu specimene indicat nu există";
            case ES:
                return "El directorio del espécimen especificado no existe";
            case IT:
                return "La directory con i campioni indicati non esiste";
            default:
                return "The specified specimen directory does not exist";
        }
    }

    public static String BuildOn(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Generat în";
            case ES:
                return "Construida sobre";
            case IT:
                return "Generato in";
            default:
                return "Built on";
        }
    }

    public static String LoadingOfflineSpecimens(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se încarcă specimenele salvate local";
            case ES:
                return "Cargando muestras fuera de línea";
            case IT:
                return "Caricamento di campioni salvati localmente";
            default:
                return "Loading offline specimens";
        }
    }

    public static String PleaseWait(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Te rugam să aștepți";
            case ES:
                return "Por favor espera";
            case IT:
                return "Per favore aspetta";
            default:
                return "Please wait";
        }
    }

    public static String LoadingThumbnails(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se încarcă imaginile în miniatură";
            case ES:
                return "Cargando miniaturas";
            case IT:
                return "Caricamento miniature";
            default:
                return "Loading thumbnails";
        }
    }

    public static String CantCreateSpecimenSet(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Setul de specimene nu poate fi creat";
            case ES:
                return "No se puede crear el conjunto de muestras";
            case IT:
                return "La serie di esemplari non può essere creata";
            default:
                return "Unable to create specimen set";
        }
    }

    public static String Decrease(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Decrementează";
            case ES:
                return "Disminución";
            case IT:
                return "Decrementato";
            default:
                return "Decrease";
        }
    }

    public static String Increase(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Incrementează";
            case ES:
                return "Incrementar";
            case IT:
                return "Incrementi";
            default:
                return "Increase";
        }
    }

    public static String SelectAnnotationTool(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selectează intrumentul de marcaj a notei";
            case ES:
                return "Seleccione la herramienta de anotación";
            case IT:
                return "Seleziona l'indicatore di nota";
            default:
                return "Select Annotation Tool";
        }
    }


    public static String ClickToSeeOtherTools(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Apăsați pentru a vedea unelte adiționale";
            case ES:
                return "Haga clic para ver herramientas adicionales";
            case IT:
                return "Tocca per visualizzare strumenti aggiuntivi";
            default:
                return "Click to see additional tools";
        }
    }

    public static String Rectangle(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Dreptunghi";
            case ES:
                return "Rectángulo";
            case IT:
                return "Rettangolo";
            default:
                return "Rectangle";
        }
    }

    public static String Line(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Linie";
            case ES:
                return "Línea";
            case IT:
                return "Linea";
            default:
                return "Line";
        }
    }

    public static String Ellipse(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Elipsă";
            case ES:
                return "Elipse";
            case IT:
                return "Ellisse";
            default:
                return "Ellipse";
        }
    }

    public static String Freeform(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Formă nedefinită";
            case ES:
                return "Forma libre";
            case IT:
                return "Forma indefinita";
            default:
                return "Freeform";
        }
    }

    public static String Text(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Text";
            case ES:
                return "Texto";
            case IT:
                return "Testo";
            default:
                return "Text";
        }
    }

    public static String ErrorReading(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare la citirea";
            case ES:
                return "Error de lectura";
            case IT:
                return "Errore di lettura";
            default:
                return "Error reading";
        }
    }

    public static String CouldntLoadAnnotationsBecause(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Notele nu pot fi încărcate din cauze erorii";
            case ES:
                return "No se pudieron cargar las anotaciones debido al siguiente error";
            case IT:
                return "Le note non possono essere caricate a causa di errori";
            default:
                return "Could not load annotations due to the following error";
        }
    }

    public static String AnnotationsFrom(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Notele din";
            case ES:
                return "Anotaciones de";
            case IT:
                return "Note da";
            default:
                return "Annotations from";
        }
    }

    public static String CouldntBeLoaded(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nu pot fi încărcate";
            case ES:
                return "No pudo ser cargado";
            case IT:
                return "Non può essere caricato";
            default:
                return "Could not be loaded";
        }
    }

    public static String InstallationTutorial(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Tutorial de instalare";
            case ES:
                return "Tutorial de instalación";
            case IT:
                return "Tutorial d'installazione";
            default:
                return "Installation Tutorial";
        }
    }

    public static String RunningJDKversion(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Rulați JDK versiunea";
            case ES:
                return "Usted está ejecutando la versión JDK";
            case IT:
                return "Esegui la versione di JDK";
            default:
                return "You are running JDK version";
        }
    }

    public static String JDKrecommandation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Este recomandat să folosești Virtual Microscope cu";
            case ES:
                return "Se recomienda encarecidamente que ejecute Virtual Microscope con";
            case IT:
                return "Si consiglia di utilizzare il Microscopio Virtuale con";
            default:
                return "It is highly recommended that you run Virtual Microscope with";
        }
    }

    public static String OrAbove(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Sau o versiune mai recentă";
            case ES:
                return "O arriba";
            case IT:
                return "O una versione più recente";
            default:
                return "Or above";
        }
    }

    public static String JDKlimitations(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unele funcții nu vor merge corect cu JDK";
            case ES:
                return "Algunas de las características no funcionarán correctamente con JDK";
            case IT:
                return "Alcune funzionalità non funzioneranno correttamente con JDK";
            default:
                return "Some of the features will not work properly with JDK";
        }
    }

    public static String BugReportRequest(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Dacă consideri că aceasta este o eroare te rog să o raportezi prin email către";
            case ES:
                return "Si cree que esto es un error, envíe un correo electrónico con la descripción del problema a";
            case IT:
                return "Se ritieni che si tratti di un errore, segnalalo via email a";
            default:
                return "If you think this is a bug please send an email with the problem description to";
        }
    }

    public static String DisableJDKwarning(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Notă: poți deactiva acest avertisment din meniul Unelte->Configurați";
            case ES:
                return "Nota: puede desactivar esta advertencia a través de Herramientas-> Configurar opción de menú";
            case IT:
                return "Nota: puoi disabilitare questo avviso dal menu Strumenti-> Configura";
            default:
                return "Note: You can disable this warning through Tools->Configure menu option";
        }
    }

    public static String JavaVersionWarning(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Avertisment versiune java";
            case ES:
                return "Advertencia de la versión de Java";
            case IT:
                return "Attenzione versione java";
            default:
                return "Java version warning";
        }
    }

    public static String FatalErrorAtLaunch(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărtuă în timpul ponirii aplicatiei";
            case ES:
                return "Se produjo un error fatal al iniciar Virtual Lab";
            case IT:
                return "Si è verificato un errore durante il posizionamento dell'applicazione";
            default:
                return "A fatal error occurred while launching Virtual Lab";
        }
    }

    public static String FatalError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare fatală";
            case ES:
                return "Error fatal";
            case IT:
                return "Errore fatale";
            default:
                return "Fatal error";
        }
    }

    public static String ErrorStarting(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la pornirea";
            case ES:
                return "Error al iniciar";
            case IT:
                return "Si è verificato un errore all'avvio";
            default:
                return "Error starting";
        }
    }

    public static String ExitVirtualMicroscope(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Închide Virtual Microscope";
            case ES:
                return "Salir del microscopio virtual";
            case IT:
                return "Chiudi Virtual Microscope";
            default:
                return "Exit Virtual Microscope";
        }
    }

    public static String ConfirmExit(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Confirmă ieșirea";
            case ES:
                return "Confirmar salida";
            case IT:
                return "Conferma l'uscita";
            default:
                return "Confirm exit";
        }
    }

    public static String SwitchToDetailView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Schimbă la modul de vizualizare detaliat";
            case ES:
                return "Cambiar a vista de detalle";
            case IT:
                return "Passa alla modalità di visualizzazione dettagliata";
            default:
                return "Switch to detail view";
        }
    }

    public static String SwitchToIconView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Schimbă la modul de vizualizare icoană";
            case ES:
                return "Cambiar a la vista de iconos";
            case IT:
                return "Passa alla modalità di visualizzazione delle icone";
            default:
                return "Switch to icon view";
        }
    }

    public static String ToggleBetweenIconDetailView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Alternează între modul de vizualizare icoană și detaliat";
            case ES:
                return "Alternar entre icono / Vista detallada";
            case IT:
                return "Alternare tra la visualizzazione di icone e la modalità di dettaglio";
            default:
                return "Toggle between Icon/Detail view";
        }
    }

    public static String File(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Fișier";
            case ES:
                return "Fichero";
            case IT:
                return "File";
            default:
                return "File";
        }
    }

    public static String Help(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Ajutor";
            case ES:
                return "Ayuda";
            case IT:
                return "Aiuto";
            default:
                return "Help";
        }
    }

    public static String Import(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Importă";
            case ES:
                return "Importar";
            case IT:
                return "Importazione";
            default:
                return "Import";
        }
    }

    public static String Export(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Exportă";
            case ES:
                return "Exportar";
            case IT:
                return "Esportazione";
            default:
                return "Export";
        }
    }

    public static String Print(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imprimă";
            case ES:
                return "Imprimir";
            case IT:
                return "Stampa";
            default:
                return "Print";
        }
    }

    public static String Exit(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Ieșire";
            case ES:
                return "Salida";
            case IT:
                return "Uscire";
            default:
                return "Exit";
        }
    }

    public static String ReportIssue(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Raportează o problemă";
            case ES:
                return "Reportar un problema";
            case IT:
                return "Segnala un problema";
            default:
                return "Report an issue";
        }
    }

    public static String License(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Licența";
            case ES:
                return "Licencia";
            case IT:
                return "Licenza";
            default:
                return "License";
        }
    }

    public static String Configure(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Configurare";
            case ES:
                return "Configurar";
            case IT:
                return "Configurazione";
            default:
                return "Configure";
        }
    }

    public static String SpecimenInformation(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Informații specimen";
            case ES:
                return "Información de la muestra";
            case IT:
                return "Informazioni sui campioni";
            default:
                return "Specimen Information";
        }
    }

    public static String SampleName(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nume specimen";
            case ES:
                return "Nombre del espécimen";
            case IT:
                return "Nome campione";
            default:
                return "Specimen Name";
        }
    }

    public static String CollectedOn(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Colectat la data de";
            case ES:
                return "Recogido en";
            case IT:
                return "Raccolti in";
            default:
                return "Collected on";
        }
    }

    public static String Description(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Descriere";
            case ES:
                return "Descripción";
            case IT:
                return "Descrizione";
            default:
                return "Description";
        }
    }

    public static String EquipmentName(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Nume echipament";
            case ES:
                return "Nombre del equipo";
            case IT:
                return "Nome dell'equipaggiamento";
            default:
                return "Equipment name";
        }
    }

    public static String Model(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Model";
            case ES:
                return "Modelo";
            case IT:
                return "Modello";
            default:
                return "Model";
        }
    }

    public static String View(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Vizualizare";
            case ES:
                return "Vista";
            case IT:
                return "Visualizzare";
            default:
                return "View";
        }
    }

    public static String Tools(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Unelte";
            case ES:
                return "Herramientas";
            case IT:
                return "Strumenti";
            default:
                return "Tools";
        }
    }

    public static String Refresh(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Reâmprosprătează";
            case ES:
                return "Refrescar";
            case IT:
                return "Rinfrescare";
            default:
                return "Refresh";
        }
    }

    public static String ErrorReadingSpecimens(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la citirea specimenelor";
            case ES:
                return "Error en la lectura de muestras";
            case IT:
                return "Errore durante la lettura dei campioni";
            default:
                return "Error reading specimens";
        }
    }

    public static String PrintingCompletedSuccessfully(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imprimarea a fost finalizată cu success";
            case ES:
                return "Impresión completada exitosamente";
            case IT:
                return "La stampa è stata completata con successo";
            default:
                return "Printing completed successfully";
        }
    }

    public static String PrintingFailed(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imprimarea a eșuat";
            case ES:
                return "Impresión fallida";
            case IT:
                return "Stampa fallita";
            default:
                return "Printing failed";
        }
    }

    public static String ErrorDisplayingTheView(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Eroare apărută la afișarea în modul solicitat";
            case ES:
                return "Error al visualizar la vista solicitada";
            case IT:
                return "Errore durante la visualizzazione come richiesto";
            default:
                return "Error displaying the requested view";
        }
    }

    public static String Loading(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Se încarcă";
            case ES:
                return "Cargando";
            case IT:
                return "Sta caricando";
            default:
                return "Loading";
        }
    }

    public static String SpecimenWasManualyRemoved(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Specimenul selectat a fost șters manual din folderul de specimene";
            case ES:
                return "La muestra seleccionada se ha eliminado manualmente de la carpeta de muestras";
            case IT:
                return "Il campione selezionato è stato eliminato manualmente dalla cartella dei campioni";
            default:
                return "The selected specimen has been manually removed from the specimen folder";
        }
    }

    public static String SpecimenNotFoundError(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Specimenul nu a fost găsit";
            case ES:
                return "Espécimen no encontrado";
            case IT:
                return "Campione non trovato";
            default:
                return "Specimen not found";
        }
    }

    public static String Magnification(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Lupă";
            case ES:
                return "Aumento";
            case IT:
                return "Lente d'ingrandimento";
            default:
                return "Magnification";
        }
    }

    public static String Brightness(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Luminozitate";
            case ES:
                return "Brillo";
            case IT:
                return "Luminosità";
            default:
                return "Brightness";
        }
    }

    public static String Contrast(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Contrast";
            case ES:
                return "Contraste";
            case IT:
                return "Contrasto";
            default:
                return "Contrast";
        }
    }

    public static String Navigator(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Navigator";
            case ES:
                return "Navegador";
            case IT:
                return "Navigatore";
            default:
                return "Navigator";
        }
    }

    public static String PrinterNotFound(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Imprimanta nu a fost găsită";
            case ES:
                return "La impresora no fue encontrada";
            case IT:
                return "Stampante non trovata";
            default:
                return "Printer was not found";
        }
    }

    public static String SelectPrinter(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Selecteează imprimanta";
            case ES:
                return "Seleccione impresora";
            case IT:
                return "Seleziona la stampante";
            default:
                return "Select printer";
        }
    }

    public static String InterfaceLanguage(LANGUAGE lang) {
        switch (lang) {
            case RO:
                return "Limbă interfață (aplicația va fi repornită!!)";
            case ES:
                return "Idioma de la interfaz (la aplicación se reiniciará)";
            case IT:
                return "Lingua dell'interfaccia (l'applicazione verrà riavviata!!)";
            default:
                return "Interface language (the application will be restarted!!)";
        }
    }

    public static String translateSideMenuTitles (String sideMenuTitle) {

        if (null==sideMenuTitle)
            return sideMenuTitle;

        if (sideMenuTitle.equals(Navigator(LANGUAGE.EN)))
            return Navigator(virtuallab.Configuration.getApplicationLanguage());

        if (sideMenuTitle.equals(Contrast(LANGUAGE.EN)))
            return Contrast(virtuallab.Configuration.getApplicationLanguage());

        if (sideMenuTitle.equals(Brightness(LANGUAGE.EN)))
            return Brightness(virtuallab.Configuration.getApplicationLanguage());

        if (sideMenuTitle.equals(Magnification(LANGUAGE.EN)))
            return Magnification(virtuallab.Configuration.getApplicationLanguage());

        if (sideMenuTitle.equals(Annotation(LANGUAGE.EN)))
            return Annotation(virtuallab.Configuration.getApplicationLanguage());

        return sideMenuTitle;
    }
}