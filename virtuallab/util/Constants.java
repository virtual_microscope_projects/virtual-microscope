/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.util;

import java.awt.Color;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.Random;
import java.util.Vector;
/**
 * This class contains the constants used throughout application. The constant
 * class provides ease of maintenance by grouping these constants in one place.
 *
 */
public class Constants {

    public static final String APPLICATION_NAME = "Virtual Microscope";

    public static final String ACADEMIX_HOME_PAGE = "https://academixproject.com";

    public static final String NO_ACTIVE_TOOL = "NO_ACTIVE_TOOL";

    public static final String ANNOTATION_PEN_TOOL = "ANNOTATION_PEN_TOOL";

    public static final String ANNOTATION_LINE_TOOL = "ANNOTATION_LINE_TOOL";

    public static final String ANNOTATION_RECTANGLE_TOOL = "ANNOTATION_RECTANGLE_TOOL";

    public static final String ANNOTATION_ROUNDEDRECTANGLE_TOOL = "ANNOTATION_ROUNDEDRECTANGLE_TOOL";

    public static final String ANNOTATION_ELLIPSE_TOOL = "ANNOTATION_ELLIPSE_TOOL";

    public static final String ANNOTATION_TEXT_TOOL = "ANNOTATION_TEXT_TOOL";

    public static final String SPACE = " ";

    /**
     * The folder that contains the colormaps. The colormaps are used in
     * AFM specimens. All image files contained in this folder will appear
     * in the colormap combobox in the AFM tool side panel.
     *
     * @see ColormapMenu#createAFMPanel()
     */
    public static final String COLORMAPS_LOCATION = "colormaps/";
    /**
     * The key against which the SPecimen download URL will be stored. We use this
     * to safe keep the download URL so that we can use this information at the time
     * when the online specimen gets deleted and we need tomove it to offline specimen.
     * At that time we need to point the m_jarFilePath to the download URL instead of the
     * ,now obsolete, local machine path of the jar file.
     */
    public static final String SPECIMEN_DOWNLOAD_URL = "SPECIMEN_DOWNLOAD_URL";
    /**
     * The new line character.
     */
    public static final String NEW_LINE = System.getProperty("line.separator");

    /**
     * The character that is used while concatenating two folder names. If it is
     * window like backslash "\" we change it to forward slash. This is because while
     * reading from the jar file the backslash does not work. This force change is
     * harmless since windows identify both backslash and forward slash as file
     * separator.
     */
    public static final String PATH_SEPARATOR = (System.getProperty("file.separator").equals("\\") ? "/" : System.getProperty("file.separator"));

    public static final String RESOURCE_FOLDER = PATH_SEPARATOR + "virtuallab" + PATH_SEPARATOR + "gui" + PATH_SEPARATOR + "res" + PATH_SEPARATOR;

    /**
     * Preferred dimension of the main frame.
     */
    public static final Dimension VIEWER_PREFERRED_DIM = new Dimension(800, 620);

    public static final String SUPPORT_EMAIL_ADDRESS = "incoming+virtual_microscope_projects/virtual-microscope@incoming.gitlab.com";

    /**
     * The URL of the RSS feed that contains the information of the
     * specimens available on the virtual lab data web site"
     */
    public static final String SPECIMEN_RSS_FEED_URL = "http://virtual.itg.uiuc.edu/data/updates.feed.php";

    /**
     * The URL of the RSS feed that contains the information of the
     * annotations available on the virtual lab server"
     */
    public static final String ANNOTATION_RSS_FEED_URL = "http://virtual.itg.uiuc.edu/data/annotations.feed.php";

    /**
     * Holds the name of the file that contains the noise data for the EDS overlay elements. Used
     * while creating the point graphs.
     */
    public static final String NOISE_DATA_FILE_NAME = "bremstrahlung";
    /**
     * The extension of the element graph data files.
     */
    public static final String ELEMENT_GRAPH_DATA_FILE_EXTENSION = ".csv";

    /**
     * holds the lower limit of th energy level. This corresponds to the first X-axis (KeV)value of the
     * X-ray count value given in the element csv file.
     */
    public final static float ENERGY_LEVEL_LOWER_LIMIT = 0;
    /**
     * holds the lower limit of th energy level. This corresponds to the last X-axis (KeV)value of the
     * X-ray count value given in the element csv file.
     */
    public final static float ENERGY_LEVEL_UPPER_LIMIT = 10;


    /**
     * The location where the launch.jar will look for virtuallab executable
     * jar file.
     */
    public static final String VIRTUAL_EXECUTABLE_LOCATION = virtuallab.Configuration.getApplicationFolder() + "/resources";

    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short NONE = 0;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short NORTH = 1;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short EAST = 2;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short WEST = 3;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short SOUTH = 4;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short NORTHEAST = 5;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short NORTHWEST = 6;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short SOUTHEAST = 7;
    /**
     * Holds the scrolling direction outcome. Used to represent the direction in which the base image
     * will scroll in relation to pressing the mouse and/or the navigation arrow keys.
     */
    public static final short SOUTHWEST = 8;
    /**
     * The prefix for the label of the graph.
     */
    public final static String POINT_LABEL_PREFIX = "P";
    /**
     * The prefix for the Area label of the graph.
     */
    public final static String AREA_LABEL_PREFIX = "A";
    /**
     * The prefix for the line annotations
     */
    public final static String LINE_LABEL_PREFIX = "L";
    /**
     * The name of the toggle button for point tool
     */
    public final static String POINT_TOOL = "PointTool";
    /**
     * The name of the toggle button for Area tool
     */
    public final static String AREA_TOOL = "AreaTool";
    /**
     * The name of the toggle button for Line tool
     */
    public final static String LINE_TOOL = "LineTool";
    /**
     * constant representing the EDS graph mode
     */
    public final static short NEW_WINDOW_MODE = 0;
    /**
     * constant representing the EDS graph mode
     */
    public final static short EXISTING_WINDOW_MODE = 1;
    /**
     * constant representing the EDS graph mode
     */
    public final static short APPEND_WINDOW_MODE = 2;
    /**
     * the relative height of the append window in relation to the single graph window
     */
    public final static float APPEND_WINDOW_HEIGHT_FRACTION = 2.40f;
    /**
     * the relative height of the append window in relation to the single graph window
     */
    public final static float APPEND_WINDOW_WIDTH_FRACTION = 1.0f;
    /**
     * The height of the panel that will contain the graph
     */
    public final static int GRAPH_PANEL_HEIGHT = 300;
    /**
     * The width of the panel that will contain the graph
     */
    public final static int GRAPH_PANEL_WIDTH = 450;
    /**
     * This holds the attribute name of the "Specimen" tag. The value of this parameter
     * will hold the XML version.
     */
    public static final String VERSION_TAG = "XMLversion";
    public static final String JAXP_SCHEMA_LANGUAGE = "http://java.sun.com/xml/jaxp/properties/schemaLanguage";
    public static final String W3C_XML_SCHEMA = "http://www.w3.org/2001/XMLSchema";
    /**
     * The folder where the .csv files are kept inside the executable jar.
     */
    public static final String ELEMENT_GRAPH_DATA_FILE_LOCATION = RESOURCE_FOLDER + "csv" + PATH_SEPARATOR;
    /**
     * This parameter is used to identify when, during our XML parsing, have we encountered
     * the last element. This is the element whose corresponding folder will contain the image files.
     */
    public static final String LEAF_NODE = "MAGNIFICATION";
    public static final String APPLICATION_ICON = Resources.LOGO;
    /**
     * Splash pause time (in milliseconds).
     */
    public static final String SPECIMEN_FILE_NAME = "specimen.xml";
    /**
     * Holds how many tiles would be preloaded. Used to decide the preloading during the
     * base image cache operations.
     */
    public static int PRELOADING_THRESHOLD = 9;
    /**
     * Holds the width of the progress indicator bar that shows up while loading the image thumbnails.
     */
    public static int PROGRESS_BAR_WIDTH = 450;
    /**
     * Holds the height of the progress indicator bar that shows up while loading the image thumbnails.
     */
    public static int PROGRESS_BAR_HEIGHT = 15;
    /**
     * Holds the width of the window that shows up while loading the image thumbnails.
     */
    public static int LOADING_WINDOW_WIDTH = 500;
    /**
     * Holds the height of the window that shows up while loading the image thumbnails.
     */
    public static int LOADING_WINDOW_HEIGHT = 100;
    /**
     * The JDK version of the JVM.
     */
    public static float JDK_VERSION;

    /**
     * The Full License text
     */
    public static String LICENSE = "<html>Copyright (c) 2018-2019 Viorel Bota<br>" +
            "<br>" +
            "Permission is hereby granted, free of charge, to any person obtaining a copy<br>" +
            "of this software and associated documentation files (the \"Virtual Microscope Software\"),<br>" +
            "to deal in the Virtual Microscope Software without restriction, including without limitation the rights<br>" +
            "to use, copy, modify, merge, publish, distribute, sublicense, and/or sell<br>" +
            "copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is<br>" +
            "furnished to do so, subject to the following conditions:<br>" +
            "<br>" +
            "The above copyright notice and this permission notice shall be included in all<br>" +
            "copies or substantial portions of the Virtual Microscope Software.<br>" +
            "<br>" +
            "Redistributions in binary form must implement the original license conditions <br>" +
            "as written in the NOTES Section 1 from below.<br>" +
            "<br>" +
            "Redistributions of source code must implement the original license conditions <br>" +
            "as written in the NOTES Section 1 from below.<br>" +
            "<br>" +
            "THE Virtual Microscope Software IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR<br>" +
            "IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,<br>" +
            "FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE<br>" +
            "AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER<br>" +
            "LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,<br>" +
            "OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE<br>" +
            "Virtual Microscope Software.<br>" +
            "<br>" +
            "NOTES:<br>" +
            "Section 1. This project is a fork. The original license was:<br>" +
            " University of Illinois/NCSA Open Source License<br>" +
            "<br>" +
            " Copyright (c) 2005, Imaging Technology Group, All rights reserved.<br>" +
            "<br>" +
            " Developed by:<br>" +
            " Imaging Technology Group<br>" +
            " Beckman Institute for Advanced Science and Technology<br>" +
            " University of Illinois at Urbana-Champaign<br>" +
            "<br>" +
            " http://virtual.itg.uiuc.edu<br>" +
            " virtual@itg.uiuc.edu<br>" +
            "<br>" +
            " Permission is hereby granted, free of charge, to any person obtaining a<br>" +
            " copy of this software and associated documentation files (the<br>" +
            " \"Software\"), to deal with the Software without restriction, including<br>" +
            " without limitation the rights to use, copy, modify, merge, publish,<br>" +
            " distribute, sublicense, and/or sell copies of the Software, and to<br>" +
            " permit persons to whom the Software is furnished to do so, subject to<br>" +
            " the following conditions:<br>" +
            "<br>" +
            " Redistributions of source code must retain the above copyright notice,<br>" +
            " this list of conditions and the following disclaimers.<br>" +
            "<br>" +
            " Redistributions in binary form must reproduce the above copyright<br>" +
            " notice, this list of conditions and the following disclaimers in the<br>" +
            " documentation and/or other materials provided with the distribution.<br>" +
            "<br>" +
            " Neither the names of the Imaging Technology Group, Beckman Institute<br>" +
            " for Advanced Science and Technology, University of Illinois at<br>" +
            " Urbana-Champaign, nor the names of its contributors may be used to<br>" +
            " endorse or promote products derived from this Software without<br>" +
            " specific prior written permission.<br>" +
            "<br>" +
            " THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND, EXPRESS<br>" +
            " OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF<br>" +
            " MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.<br>" +
            " IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR<br>" +
            " ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,<br>" +
            " TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE<br>" +
            " SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.<br>" +
            "<br>" +
            "Section 2. Third party libraries:<br>" +
            "The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.<br>"+ "" +
            "You can find the license for them here:<br>" +
            "https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses<br>" +
            "<br>" +
            "Instructions on how to replace the jfreechart and jcommon with another version can be found here:<br>" +
            "https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses<br>" +
            "<br>" +
            "The versions of the source code and binaries<br>" +
            "for jfreechart and jcommon used to build the application can be found here:<br>" +
            "https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses";

    /**
     * This hashmap contains the location for the default instrument
     * thumbnail.
     */
    private static HashMap instruments = new HashMap();
    private static Vector m_overlayColorMap = new Vector();

    /**
     * This class encapsulates the default image logic for the instruments.
     *
     * @author Kashif Manzoor
     */
    public static class Instruments {
        public static final String EM = "EM";
        public static final String SEM = "SEM";
        public static final String LM = "LM";

        static {
            instruments.put(Constants.Instruments.EM, Resources.ESEM);
            instruments.put(Constants.Instruments.SEM, Resources.ESEM);
            instruments.put(Constants.Instruments.LM, Resources.LM);
        }

        /**
         * This method takes in the instrument TYPE and returns the thumbnail
         * image for the instrument.
         *
         * @return the path of the thumbnail
         */
        public static String getInstrumentThumbnail(String instrumentType) {
            return (String) instruments.get(instrumentType);
        }
    }

    /**
     * This class contains the default color logic for the overlay elements.
     * We keep a collection of predefined colors. As the request for defaultColor
     * is received we pick a color from this collection. Once we run out of
     * predefined colors we return randomly generated colors.
     *
     * @author Kashif Manzoor
     */
    public static class OverlayElementColor {
        public static int m_pointer = 0;

        static {
            m_overlayColorMap.add(new Color(243, 0, 0));
            m_overlayColorMap.add(new Color(248, 232, 0));
            m_overlayColorMap.add(new Color(64, 246, 5));
            m_overlayColorMap.add(new Color(35, 167, 248));
            m_overlayColorMap.add(new Color(240, 34, 200));
            m_overlayColorMap.add(new Color(0, 0, 255));
            m_overlayColorMap.add(new Color(128, 128, 0));
            m_overlayColorMap.add(new Color(236, 118, 0));
        }

        public static Color getDefaultColor(String element) {
            Color clr = null;
            if (m_pointer >= m_overlayColorMap.size()) {
                // Randomly pick any color.
                Random rand = new Random();
                // Random integers
                clr = new Color(rand.nextInt(256), rand.nextInt(256), rand.nextInt(256));
            } else {
                //pick from the list of predefined colors.
                clr = (Color) m_overlayColorMap.get(m_pointer++);
            }
            return clr;
        }
    }


    /**
     * This class contains the relative paths to the respirces.
     *
     * @author Kashif Manzoor
     * TODO: Move all the hardcoded resource paths into this class.
     */
    public static class Resources {
        /**
         * Relative path of ITg Logo, shown in the information dialog box.
         */
        public static final String LOGO = RESOURCE_FOLDER + "logo.png";
        public static final String ONE_PIXEL_LINE = RESOURCE_FOLDER + "onepixelline.gif";
        public static final String TWO_PIXEL_LINE = RESOURCE_FOLDER + "twopixelline.gif";
        public static final String FOUR_PIXEL_LINE = RESOURCE_FOLDER + "fourpixelline.gif";
        public static final String SIX_PIXEL_LINE = RESOURCE_FOLDER + "sixpixelline.gif";
        public static final String EIGHT_PIXEL_LINE = RESOURCE_FOLDER + "eightpixelline.gif";

        public static final String UNSELECTED_RECTANGLE = RESOURCE_FOLDER + "rect-unselected.gif";
        public static final String SELECTED_RECTANGLE = RESOURCE_FOLDER + "rect-selected.gif";
        public static final String UNSELECTED_ESD_POINT = RESOURCE_FOLDER + "EDS-point-unselected.gif";
        public static final String SELECTED_ESD_POINT = RESOURCE_FOLDER + "EDS-point-selected.gif";
        public static final String UNSELECTED_ESD_AREA = RESOURCE_FOLDER + "EDS-area-unselected.gif";
        public static final String SELECTED_ESD_AREA = RESOURCE_FOLDER + "EDS-area-selected.gif";
        public static final String UNSELECTED_ESD_LINE = RESOURCE_FOLDER + "EDS-line-unselected.gif";
        public static final String SELECTED_ESD_LINE = RESOURCE_FOLDER + "EDS-line-selected.gif";
        public static final String BULLET = RESOURCE_FOLDER + "bullet.gif";
        public static final String UNSELECTED_BULLET = RESOURCE_FOLDER + "unselected_menu.gif";
        public static final String UNSELECTED_LINE = RESOURCE_FOLDER + "line-unselected.gif";
        public static final String UNSELECTED_ELIPSE = RESOURCE_FOLDER + "ellipse-unselected.gif";
        public static final String UNSELECTED_PEN = RESOURCE_FOLDER + "pencil-unselected.gif";
        public static final String UNSELECTED_TEXT = RESOURCE_FOLDER + "text-unselected.gif";

        public static final String ESEM = RESOURCE_FOLDER + "esem.gif";
        public static final String LM = RESOURCE_FOLDER + "lm.gif";
        public static final String ICONVIEW = RESOURCE_FOLDER + "iconview.png";
        public static final String DETAILSVIEW = RESOURCE_FOLDER + "detailsview.png";
    }

    /**
     * This class contains the XML tags that are used in the specimen.xml file
     *
     * @author Kashif Manzoor
     */
    public static class XMLTags {
        /**
         * The tag that contains the EDS noise information. this noise will be added to the
         * EDS spectra to give it a realistic appeal.
         */
        public final static String BREMSSTRAHLUNG = "Bremsstrahlung";
        /**
         * The EDS Element tage. The overlay elements are enclosed in this tag.
         */
        public final static String ELEMENT = "Element";
        /**
         * The path of the EDs overlay map .bmp files.
         */
        public final static String PATH = "path";
        /**
         * The EDS scale. The pixel values will be multiplied by this scaling factor to adjust their
         * Xraw -ray counts
         */
        public final static String SCALE = "scale";
        /**
         * The pixel size of the overlay elements map. This is used to do scale conversions between the
         * overlay map and the base image.
         */
        public final static String PXSIZE = "pxsize";


        /**
         * model of the instrument
         */

        public final static String MODEL = "Model";
        /**
         * The Date when the sample was collected
         */
        public final static String COLLECTION_DATE = "CollectionDate";

        /**
         * The "Name" tag that holds the name of the specimen
         */
        public final static String NAME = "Name";
        /**
         * The LongName tag that holds the information to be shown
         * on the title bar when the speciman is loaded.
         */
        public final static String LONG_NAME = "LongName";
        /**
         * DisplayString attribute
         */
        public final static String DISPLAY_STRING = "DisplayString";
        /**
         * The long description of the sample. This is read out of the RSS Feed, and is not
         * included in the specimen.xml
         */
        public final static String SAMPLE_LONG_DESCRIPTION = "description";

        /**
         * Unique Name for the image
         */
        public final static String UNIQUE_NAME = "SpecimenID";

        public final static String SAMPLE_STATS = "SAMPLE_STATS";

        /**
         * The Collection Date tag
         */
        public final static String COLLECTION_NAME = "CollectionDate";
        /**
         * The Preparer Tag
         */
        public final static String PREPARER = "Preparer";
        /**
         * The specimen thumbnail shown when the application is loaded.
         */
        public final static String THUMBNAIL = "Thumbnail";

        /**
         * The thumbnail used for the navigation functionality.
         */
        public final static String NAVIGATION_THUMBNAIL = "NavigationThumbnail";

        /**
         * The "FolderName" attribute of the "Level" nodes
         */
        public final static String FOLDER_NAME = "FolderName";

        /**
         * The "href" attribute
         */
        public final static String HREF = "href";

        /**
         * The "TileSize" tag
         */

        public final static String TILE_SIZE = "TileSize";

        /**
         * The "PixelSize" tag
         */

        public final static String PIXEL_SIZE = "PixelSize";

        /**
         * The "ImageSize" tag
         */
        public final static String IMAGE_SIZE = "ImageSize";

        /**
         * The width attribute
         */

        public final static String WIDTH = "width";

        /**
         * The height attribute
         */

        public final static String HEIGHT = "height";


        /**
         * The id Attribute
         */
        public final static String ID = "id";
        /**
         * The ControlDescriptor tag
         */
        public final static String CONTROL_DESCRIPTORS = "ControlDescriptors";
        /**
         * The Control Tag
         */
        public final static String CONTROL = "Control";
//		  /**
//		   * The Level tag.
//		   */
//		  final static String  LEVEL = "Control";

        /**
         * InfoRef Tag
         */
        public final static String INFO_REF = "InfoRef";

        /**
         * Type of instrument
         */
        public final static String TYPE = "Type";

        /**
         * The parent Specimen tag
         */
        public static final String SPECIMEN = "Specimen";
        /**
         * The METADATA" tag
         */
        public static final String METADATA = "MetaData";
        /**
         * The Instrument Tag
         */
        public static final String INSTRUMENT = "Instrument";
        /**
         * The Specimen Collection Tag
         */
        public static final String SAMPLE_INFO = "SampleInfo";


    }

    /**
     * The class that groups the control names.
     *
     * @author Kashif Manzoor
     * @see virtuallab.Configuration
     */
    public class Controls {
        public static final String ANNOTATION = "ANNOTATION";
        public static final String DETECTOR = "DETECTOR";
        public static final String EDS = "EDS";
        public static final String AFM = "AFM";
        public static final String COLORMAP = "COLORMAP";
        public static final String MAGNIFICATION = "MAGNIFICATION";
        public static final String FOCUS = "FOCUS";
        public static final String BRIGHTNESS = "BRIGHTNESS";
        public static final String CONTRAST = "CONTRAST";
        public static final String NAVIGATOR = "NAVIGATOR";
        public static final String DISTANCE = "DISTANCE";
        public static final String HEIGHT = "HEIGHT";

        public static final String NAVIGATOR_DISPLAY_STRING = "Navigator";
        /**
         * The default control, that will be shown if the loaded
         * control does not have any associated class defined in the
         * VirtualMicroscope.xml file.
         */
        public static final String DEFAULT_CONTROL = "virtuallab.gui.TextMenu";
        /**
         * The label of the Brightness control.
         */
        public static final String BRIGHTNESS_DISPLAY_STRING = "Brightness";
        /**
         * The label of the contrast control
         */
        public static final String CONTRAST_DISPLAY_STRING = "Contrast";
        /**
         * The label of the Annotation control
         */
        public static final String ANNOTATION_DISPLAY_STRING = "Annotation";

        public static final String NONE_DISPLAY_STRING = "None";

    }

}

