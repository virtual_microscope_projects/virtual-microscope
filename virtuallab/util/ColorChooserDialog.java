/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.util;

import java.awt.Color;
import java.awt.Dimension;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SpringLayout;


import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import virtuallab.Configuration;
import virtuallab.Log;
/**
 * This class implementes the color chooser dialog box. The java default color chooser
 * was not suitable since the HSB tab of the default chooser does not allow the color to
 * be selected by clicking on the vertical color bar. Additionally the default preview
 * is too crowded. This dialog box is completely autonomous and can be plugged in
 * in any situation.
 *
 */

public class ColorChooserDialog extends JDialog implements ActionListener, ChangeListener, MouseListener, MouseMotionListener {

    private static final Log log = new Log(ColorChooserDialog.class.getName());

    /**
     *
     */
    private static final long serialVersionUID = 9014090306821618947L;
    public Color m_chosenColor = null;
    public Color m_defaultColor = null;
    JButton m_previewbttn = new JButton();
    JPanel m_previewpanel = null;
    JPanel m_rgbPanel = null;
    JPanel m_chooserPanel = null;
    JLabel m_shadeLabel = null;
    JTextField m_red = new JTextField();
    JTextField m_green = new JTextField();
    JTextField m_blue = new JTextField();
    float m_imageBarWidth = 20;
    float m_imageBarHeight = (float) (359 * 0.55);
    int DIALOG_WIDTH = 345;
    int DIALOG_HEIGHT = 410;
    JSlider m_slider = new JSlider();

    /**
     * The constructor
     *
     * @param owner        the window that will contain this dialog
     * @param text         the postfix of the dialog box title
     * @param defaultColor the default color. If null is specified we use BLACK.
     */
    public ColorChooserDialog(JFrame owner, String text, Color defaultColor) {
        super(owner, Locale.ChooseColor(Configuration.getApplicationLanguage())+" '" + text + "'", true);
        if (defaultColor != null)
            m_defaultColor = defaultColor;
        else
            m_defaultColor = Color.BLACK;
        m_chosenColor = m_defaultColor;
        m_slider.setOrientation(JSlider.VERTICAL);
        m_slider.setPreferredSize(new Dimension(20, (int) m_imageBarHeight + 10));
        m_slider.setMinimum(0);
        m_slider.setMaximum(100);
        m_slider.addChangeListener(this);
        m_slider.setInverted(true);
        float hsb[] = new float[3];
        Color.RGBtoHSB(m_defaultColor.getRed(),
                m_defaultColor.getGreen(),
                m_defaultColor.getBlue(), hsb);
        m_slider.setValue((int) (hsb[0] * 100));

        m_red.setEnabled(false);
        m_red.setPreferredSize(new Dimension(40, 20));
        m_red.setHorizontalAlignment(JTextField.RIGHT);
        m_red.setText(Integer.toString(m_defaultColor.getRed()));

        m_green.setEnabled(false);
        m_green.setPreferredSize(new Dimension(40, 20));
        m_green.setHorizontalAlignment(JTextField.RIGHT);
        m_green.setText(Integer.toString(m_defaultColor.getGreen()));

        m_blue.setEnabled(false);
        m_blue.setPreferredSize(new Dimension(40, 20));
        m_blue.setHorizontalAlignment(JTextField.RIGHT);
        m_blue.setText(Integer.toString(m_defaultColor.getBlue()));

        getContentPane().setLayout(new GridBagLayout());
        placePanels(drawPreviewPanel(), drawRGBPanel(), drawChooserPanel());
        setResizable(false);
        pack();
    }

    /**
     * This method draws the preview panel that contains the rectangle which shows
     * the selected color.
     *
     * @return the preview JPanel
     */
    private JPanel drawPreviewPanel() {
        GridBagConstraints c = new GridBagConstraints();
        m_previewpanel = new JPanel();
        m_previewpanel.setLayout(new GridBagLayout());

        ////////// The vertical color chooser bar ///////
        float hue = (float) m_slider.getValue() / 100;
        updateShade(hue);
        ShadeChooser mouseEventHandler = new ShadeChooser();
        m_shadeLabel.addMouseListener(mouseEventHandler);
        m_shadeLabel.addMouseMotionListener(mouseEventHandler);

        c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        m_previewpanel.add(m_shadeLabel);
        //preview.setBorder(BorderFactory.createEtchedBorder());
        return m_previewpanel;

    }

    /**
     * This method draws the text boxes that shows the RGB values
     * of the selected color.
     *
     * @return the JPanel containing the RGB text boxes.
     */
    private JPanel drawRGBPanel() {
        m_rgbPanel = new JPanel();
        m_rgbPanel.setPreferredSize(new Dimension(200, 85));
        SpringLayout layout = new SpringLayout();
        m_rgbPanel.setLayout(layout);
        m_rgbPanel.setBorder(BorderFactory.createEtchedBorder());

        m_previewbttn = new JButton("   ");
        m_previewbttn.setEnabled(false);
        m_previewbttn.setPreferredSize(new Dimension(80, 70));
        m_previewbttn.setBackground(m_defaultColor);
        m_rgbPanel.add(m_previewbttn);
        layout.putConstraint(SpringLayout.NORTH, m_previewbttn, 5, SpringLayout.NORTH, m_rgbPanel);
        layout.putConstraint(SpringLayout.WEST, m_previewbttn, 5, SpringLayout.WEST, m_rgbPanel);

        ////////////// RED //////////////
        JLabel red = new JLabel("RED ", JLabel.TRAILING);
        m_rgbPanel.add(red);
        red.setLabelFor(m_red);
        m_rgbPanel.add(m_red);
        layout.putConstraint(SpringLayout.NORTH, m_red, 5, SpringLayout.NORTH, m_rgbPanel);
        layout.putConstraint(SpringLayout.SOUTH, red, 0, SpringLayout.SOUTH, m_red);
        layout.putConstraint(SpringLayout.EAST, m_red, -5, SpringLayout.EAST, m_rgbPanel);
        layout.putConstraint(SpringLayout.EAST, red, -5, SpringLayout.WEST, m_red);

        ////////////// GREEN //////////////
        JLabel green = new JLabel("GREEN ", JLabel.TRAILING);
        m_rgbPanel.add(green);
        green.setLabelFor(m_green);
        m_rgbPanel.add(m_green);
        layout.putConstraint(SpringLayout.NORTH, m_green, 5, SpringLayout.SOUTH, m_red);
        layout.putConstraint(SpringLayout.SOUTH, green, 0, SpringLayout.SOUTH, m_green);
        layout.putConstraint(SpringLayout.EAST, m_green, -5, SpringLayout.EAST, m_rgbPanel);
        layout.putConstraint(SpringLayout.EAST, green, -5, SpringLayout.WEST, m_green);

        ////////////// BLUE //////////////
        JLabel blue = new JLabel("BLUE ", JLabel.TRAILING);
        m_rgbPanel.add(blue);
        blue.setLabelFor(m_blue);
        m_rgbPanel.add(m_blue);
        layout.putConstraint(SpringLayout.NORTH, m_blue, 5, SpringLayout.SOUTH, m_green);
        layout.putConstraint(SpringLayout.SOUTH, blue, 0, SpringLayout.SOUTH, m_blue);
        layout.putConstraint(SpringLayout.EAST, m_blue, -5, SpringLayout.EAST, m_rgbPanel);
        layout.putConstraint(SpringLayout.EAST, blue, -5, SpringLayout.WEST, m_blue);


        return m_rgbPanel;
    }

    private JPanel drawChooserPanel() {
        m_chooserPanel = new JPanel();
        GridBagConstraints c = new GridBagConstraints();

        // //////// The vertical color chooser bar ///////
        float hueStep = 0.0f;
        BufferedImage colorImage = new BufferedImage((int) m_imageBarWidth,(int) m_imageBarHeight, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < colorImage.getHeight(); i++) {
            hueStep += (float) 360 / m_imageBarHeight;
            float sat = 1.0f;
            float bri = 1.0f;
            for (int j = 0; j < colorImage.getWidth(); j++) {
                colorImage.setRGB(j, i, Color.HSBtoRGB((float) hueStep / 360,
                        sat, bri));
            }
        }
        Icon icon = new ImageIcon(colorImage);
        JLabel label = new JLabel(icon);
        label.addMouseListener(this);
        label.addMouseMotionListener(this);
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.EAST;
        c.insets = new Insets(0, 20, 0, 0);
        c.gridx = 0;
        c.gridy = 0;
        // slider.setBorder(BorderFactory.createEtchedBorder());
        m_chooserPanel.add(m_slider, c);
        // /////// The accompanying slider //////////
        c = new GridBagConstraints();
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 1;
        c.gridy = 0;
        // label.setBorder(BorderFactory.createEtchedBorder());
        m_chooserPanel.add(label, c);
        return m_chooserPanel;

    }

    private void placePanels(JPanel previewPanel, JPanel rgbPanel, JPanel chooserPanel) {
        GridBagConstraints c = new GridBagConstraints();
        JPanel bttnPanel = new JPanel();
        bttnPanel.setLayout(new GridBagLayout());
        bttnPanel.setBorder(BorderFactory.createTitledBorder(""));
        JPanel colorPanel = new JPanel();
        colorPanel.setBorder(BorderFactory.createTitledBorder(""));
        colorPanel.setLayout(new GridBagLayout());
        ////////////// The color panel /////////
        GridBagLayout colorLayout = new GridBagLayout();
        colorPanel.setLayout(colorLayout);
        c = new GridBagConstraints();
        c.insets = new Insets(10, 10, 0, 0);
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        colorPanel.add(previewPanel, c);

        c = new GridBagConstraints();
        c.insets = new Insets(0, 20, 0, 10);
        c.anchor = GridBagConstraints.EAST;
        c.gridx = 1;
        c.gridy = 0;
        c.fill = GridBagConstraints.VERTICAL;
        c.gridheight = 2;
        colorPanel.add(chooserPanel, c);

        c = new GridBagConstraints();
        c.insets = new Insets(0, 10, 0, 0);
        c.anchor = GridBagConstraints.WEST;
        c.gridx = 0;
        c.gridy = 1;
        colorPanel.add(rgbPanel, c);
        ///////////// The buttons panel /////////
        c = new GridBagConstraints();
        c.insets = new Insets(3, 22, 3, 15);
        c.gridx = 0;
        c.gridy = 0;
        JButton ok = new JButton("Ok");
        ok.addActionListener(this);
        bttnPanel.add(ok, c);
        c = new GridBagConstraints();
        c.insets = new Insets(3, 15, 3, 15);
        c.gridx = 1;
        c.gridy = 0;
        JButton reset = new JButton(Locale.Reset(Configuration.getApplicationLanguage()));
        reset.addActionListener(this);
        bttnPanel.add(reset, c);
        c = new GridBagConstraints();
        c.insets = new Insets(3, 15, 3, 22);
        c.gridx = 2;
        c.gridy = 0;
        JButton cancel = new JButton(Locale.Cancel(Configuration.getApplicationLanguage()));
        cancel.addActionListener(this);
        bttnPanel.add(cancel, c);

        ///////////// Laying out the two container panels //////
        c = new GridBagConstraints();
        c.insets = new Insets(5, 10, 5, 10);
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 0;
        getContentPane().add(colorPanel, c);
        /////////////////////
        c = new GridBagConstraints();
        c.insets = new Insets(5, 10, 5, 10);
        c.anchor = GridBagConstraints.CENTER;
        c.gridx = 0;
        c.gridy = 1;
        getContentPane().add(bttnPanel, c);
    }

    public void stateChanged(ChangeEvent ce) {
        float hue = (float) ((JSlider) ce.getSource()).getValue() / 100;
        float hsb[] = getHSB(m_chosenColor);
        hsb[0] = hue;
        Color cl = new Color(Color.HSBtoRGB(hue, hsb[1], hsb[2]));
        m_previewbttn.setBackground(cl);
        m_red.setText("" + cl.getRed());
        m_green.setText("" + cl.getGreen());
        m_blue.setText("" + cl.getBlue());
        m_chosenColor = cl;
        updateShade(hsb);
    }

    public void mouseEntered(MouseEvent ev) {
    }

    public void mouseExited(MouseEvent ev) {
    }

    public void mouseReleased(MouseEvent ev) {
    }

    public void mousePressed(MouseEvent ev) {
    }

    public void mouseDragged(MouseEvent e) {
        if (e.getX() < 0 || e.getX() > 20)
            // outside the limits of the vertical color strip
            return;
        mouseClicked(e);
    }

    public void mouseMoved(MouseEvent arg0) {
    }


    /**
     * This method handles mouse clicks over the image chooser bar i.e. the
     * vertical rainbow bar.
     */
    public void mouseClicked(MouseEvent e) {
        // color chooser vertical bar was clicked on.
        float value = e.getY() / m_imageBarHeight;
        if (value > .992)
            //this ensures that when the user clicks at the very bottom we get a pure RED.
            value = 1;
        float hsb[] = getHSB(m_chosenColor);
        m_slider.setValue((int) (value * 100));
        Color cl = new Color(Color.HSBtoRGB(value, hsb[1], hsb[2]));
        hsb[0] = value;
        m_previewbttn.setBackground(cl);
        m_red.setText("" + cl.getRed());
        m_green.setText("" + cl.getGreen());
        m_blue.setText("" + cl.getBlue());
        m_chosenColor = cl;
        updateShade(hsb);
    }

    /**
     * This method handles the button clicks in the chooser dialog box.
     */
    public void actionPerformed(ActionEvent e) {
        // One of the dialog buttons have been pressed.
        if (e.getActionCommand().equalsIgnoreCase("Cancel")) {
            m_chosenColor = null;
            dispose();
        } else if (e.getActionCommand().equalsIgnoreCase("Reset")) {
            m_chosenColor = m_defaultColor;
            float[] hsb = getHSB(m_defaultColor);
            m_slider.setValue((int) (hsb[0] * 100));
            m_red.setText(formatInteger(m_defaultColor.getRed(), 8, ' '));
            m_green.setText(formatInteger(m_defaultColor.getGreen(), 8, ' '));
            m_blue.setText(formatInteger(m_defaultColor.getBlue(), 8, ' '));
            updateShade(hsb);
            m_previewbttn.setBackground(m_chosenColor);
        } else
            dispose();
    }

    /**
     * This method takes in an integer and the desired length and outputs
     * the string that has the same length as determined by the parameter
     * size. If the integer has less digits than what 'size' requires the method
     * left pads the integer with the 'padChar'.
     *
     * @param value
     * @param size
     * @return
     */
    private String formatInteger(int value, int size, char padChar) {
        String ret = "" + value;
        try {
            int length = ret.length();
            if (length < size) {
                for (int i = 0; i < (size - length); i++) {
                    ret = padChar + ret;
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
            ret = "";
        }
        return ret;
    }

    /**
     * This method is called when the
     * <ul><li>the shade image is to be redrawn
     * which is usually required when the user changes the hue.</li>
     * <li>the shade selector indicator white rectangle is to be redrawn.
     * Which will happen if the user selects a shade by dragging or
     * clicking the mouse</li></ul>
     * This method takes in the hue (which is the selected value in the vertical
     * rainbow color strip). It gets the sat and bri values from the selected
     * color.
     *
     * @param hue the selected Hue
     */
    private void updateShade(float hue) {
        /// Drawing a white rectanhle depicting the selected shade ////
        float hsb[] = new float[3];
        Color.RGBtoHSB(m_chosenColor.getRed(),
                m_chosenColor.getGreen(),
                m_chosenColor.getBlue(), hsb);
        updateShade(hsb);
    }

    /**
     * This method is called when the
     * <ul><li>the shade image is to be redrawn
     * which is usually required when the user changes the hue.</li>
     * <li>the shade selector indicator white rectangle is to be redrawn.
     * Which will happen if the user selects a shade by dragging or
     * clicking the mouse</li></ul>
     *
     * @param hue the selected Hue
     */
    private void updateShade(float[] hsb) {
        BufferedImage colorImage = drawShadeImage(hsb[0]);

        /// Drawing a white rectanhle depicting the selected shade ////
        drawShadeSelectorRectangle(colorImage, hsb);

        // simple invalidate() wouldn't work we have to remove the
        // icon and then reinsert it into the label.
        Icon icon = new ImageIcon(colorImage);
        if (m_shadeLabel != null) {
            m_shadeLabel.removeAll();
            m_shadeLabel.setIcon(icon);
        } else
            m_shadeLabel = new JLabel(icon);
    }

    /**
     * @param colorImage
     * @param hsb
     */
    private void drawShadeSelectorRectangle(BufferedImage colorImage, float[] hsb) {
        int x = (int) (200 - (hsb[1] * 200));
        int y = (int) (200 - (hsb[2] * 200));

        if (x > 193) x = 193;
        if (y > 193) y = 193;

        int white = Color.HSBtoRGB(0.0f, 0.0f, 1.0f);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x, y, white);

        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        x = (int) (200 - (hsb[1] * 200));
        y = (int) (200 - (hsb[2] * 200));
        if (x > 193) x = 193;
        if (y > 193) y = 193;
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y++, white);
        colorImage.setRGB(x, y, white);

        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
        colorImage.setRGB(x++, y, white);
    }

    /**
     * @param hue
     * @return
     */
    private BufferedImage drawShadeImage(float hue) {
        /////// Redraw the Shade Card ///
        float sat = 100.0f;
        float bri = 100.0f;
        BufferedImage colorImage = new BufferedImage(
                200, 200, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < colorImage.getHeight(); i++) {
            sat = 100.0f;
            if (i % 2 == 0) bri--;
            for (int j = 0; j < colorImage.getWidth(); j++) {
                if (j % 2 == 0) sat--;
                colorImage.setRGB(j, i, Color.HSBtoRGB(hue, sat / 100, bri / 100));
            }
        }
        return colorImage;
    }

    /**
     * This method gets the HSB values from the color.
     *
     * @param clr
     * @return
     */
    private float[] getHSB(Color clr) {
        float hsb[] = new float[3];
        Color.RGBtoHSB(clr.getRed(), clr.getGreen(), clr.getBlue(), hsb);
        return hsb;

    }

    class ShadeChooser implements MouseListener, MouseMotionListener {
        public void mouseDragged(MouseEvent e) {
            // color chade chooser Box was clicked on.

            int x = e.getX();
            int y = e.getY();
            if (x < 0) return;//x = 0;
            if (y < 0) return;//y =0;
            if (x > 198) return;//x =198;
            if (y > 198) return;//y=198;
            handleColorSelectionChanged(x, y);
//TODO remove unsused code section:
//			float sat = (float)(200-x)/200;
//			float bri = (float)(200-y)/200;
//			Color cl = new Color(Color.HSBtoRGB((float)m_slider.getValue()/100, sat, bri));
//			m_previewbttn.setBackground(cl);
//			m_red.setText("" + cl.getRed());
//			m_green.setText("" + cl.getGreen());
//			m_blue.setText("" + cl.getBlue());
//			m_chosenColor = cl;
//			updateShade((float)m_slider.getValue()/100);
        }

        public void mouseMoved(MouseEvent arg0) {
            // TODO Auto-generated method stub

        }

        public void mouseEntered(MouseEvent ev) {
            //TODO remove unused event mouseEntered
        }

        public void mouseExited(MouseEvent ev) {
            //TODO remove unused event mouseExited
        }

        public void mouseReleased(MouseEvent ev) {
            //TODO remove unused event mouseReleased
        }

        public void mousePressed(MouseEvent ev) {
            //TODO remove unused event mousePressed
        }

        /**
         * This method handles mouse clicks over the image chooser bar i.e. the
         * vertical rainbow bar.
         */
        public void mouseClicked(MouseEvent e) {
            // color chade chooser Box was clicked on.
            handleColorSelectionChanged(e.getX(), e.getY());
        }

        /**
         * @param mousePositionX
         * @param mousePositionY         *
         */
        private void handleColorSelectionChanged(int mousePositionX, int mousePositionY) {
            float sat = (float) (200 - mousePositionX) / 200;
            float bri = (float) (200 - mousePositionY) / 200;

            Color cl = new Color(Color.HSBtoRGB((float) m_slider.getValue() / 100, sat, bri));
            m_previewbttn.setBackground(cl);
            m_red.setText("" + cl.getRed());
            m_green.setText("" + cl.getGreen());
            m_blue.setText("" + cl.getBlue());
            m_chosenColor = cl;
            updateShade((float) m_slider.getValue() / 100);
        }
    }
}
