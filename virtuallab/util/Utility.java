/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */

/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 **
 * University of Illinois/NCSA Open Source License Copyright (c) 2005, Imaging
 * Technology Group, All rights reserved. Developed by: Imaging Technology Group
 * Beckman Institute for Advanced Science and Technology University of Illinois
 * at Urbana-Champaign http://virtual.itg.uiuc.edu virtual@itg.uiuc.edu
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * with the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: Redistributions of
 * source code must retain the above copyright notice, this list of conditions
 * and the following disclaimers. Redistributions in binary form must reproduce
 * the above copyright notice, this list of conditions and the following
 * disclaimers in the documentation and/or other materials provided with the
 * distribution. Neither the names of the Imaging Technology Group, Beckman
 * Institute for Advanced Science and Technology, University of Illinois at
 * Urbana-Champaign, nor the names of its contributors may be used to endorse or
 * promote products derived from this Software without specific prior written
 * permission. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS WITH THE SOFTWARE.
 */

package virtuallab.util;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.MemoryImageSource;
import java.awt.image.WritableRaster;
import java.io.*;
import java.text.DecimalFormat;
import java.util.Vector;

import virtuallab.Log;

/**
 * The utility class that hosts the utility method used across the application.
 *
 */
public class Utility {

    private static final Log log = new Log(Utility.class.getName());

    /**
     * This method reads in a record file that has each record on a line by
     * itself, and each of the field of the record is separated by a delimiter
     * character/string. The output is a two dimensional vector containing the
     * records and their respective fields.
     *
     * @param fileName       the name of the file to read in.
     * @param fieldSeparator the character that separates the fields of a record.
     * @return Vector. Each item in the vector represent a single record in the
     * file. Each element of the vector is itself a vector that contains
     * Points2D.Double. the x value of which represent the field number
     * and the y value represent the data value of the field. Returns
     * null if the file does not exist or if there is some error reading
     * the file.
     */
    public static Vector readRecordFile(String fieldSeparator, String fileName) {
        Vector vt = new Vector();
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String str;
            // String aString = "";
            while ((str = in.readLine()) != null) {
                Vector record = new Vector();
                int x = 1;
                while (str.indexOf(fieldSeparator) > 0) {
                    int index = str.indexOf(fieldSeparator);
                    if (index > 0) {
                        // a new field discovered.
                        String str1 = str.substring(0, index);
                        Point2D.Float pt = new Point2D.Float();
                        pt.y = Float.parseFloat(str1);
                        pt.x = x++;
                        record.add(pt);
                        // aString = aString + pt.y + '\n';
                    }
                    str = str.substring(index + 1, str.length());
                }
                Point2D.Float pt = new Point2D.Float();
                pt.y = Float.parseFloat(str);
                pt.x = x;
                record.add(pt);
                vt.add(record);
            }
            in.close();
        } catch (IOException e) {
            vt = null;
        }
        return vt;
    }

    /**
     * The polymorphic version of the readRecordFile(). Does the same thing
     * except that it takes in a fileStream. Useful when reading a file included
     * in the jar file.
     *
     * @param fieldSeparator
     * @param fileStream
     * @return
     */
    public static Vector readRecordFile(String fieldSeparator, InputStream fileStream) {
        Vector vt = new Vector();
        try {
            InputStreamReader inStream = new InputStreamReader(fileStream);
            BufferedReader in = new BufferedReader(inStream);
            String str;
            while ((str = in.readLine()) != null) {
                Vector record = new Vector();
                int x = 1;
                while (str.indexOf(fieldSeparator) > 0) {
                    int index = str.indexOf(fieldSeparator);
                    if (index > 0) {
                        // a new field discovered.
                        String str1 = str.substring(0, index);
                        Point2D.Float pt = new Point2D.Float();
                        pt.y = Float.parseFloat(str1);
                        pt.x = x++;
                        record.add(pt);
                    }
                    str = str.substring(index + 1, str.length());
                }
                Point2D.Float pt = new Point2D.Float();
                pt.y = Float.parseFloat(str);
                pt.x = x;
                record.add(pt);
                vt.add(record);
            }
            in.close();
        } catch (IOException e) {
            vt = null;
        }
        return vt;
    }

    /**
     * This method takes in an integer and the desired length and outputs the
     * string that has the same length as determined by the parameter size. If
     * the integer has less digits than what 'size' requires the method left
     * pads the integer with the 'padChar'.
     *
     * @param value
     * @param size
     * @return
     */
    public static String formatInteger(int value, short size, char padChar) {
        String ret = "" + value;
        try {
            int length = ret.length();
            if (length < size) {
                for (int i = 0; i < (size - length); i++) {
                    ret = padChar + ret;
                }
            }
        } catch (Exception ex) {
            ret = "";
        }
        return ret;
    }

    /**
     * This method formats the input number to the desired decimal places and
     * returns the string version of the rounded off number.
     *
     * @return the rounded off number
     */
    public static String formatDouble(double numberToFormat, int decimalPlaces) {
        String fomatString = "#,###,###,##0.";
        for (int i = 0; i < decimalPlaces; i++) {
            fomatString += "0";
        }
        DecimalFormat decimalFormat = new DecimalFormat(fomatString);
        String numberAsString = decimalFormat.format(numberToFormat);
        numberAsString = numberAsString.replaceAll(",", ".");

        return numberAsString;
    }

    /**
     * This method creates a rectangular cursor with the given widht and height.
     * The method can also draw a cross hair joining the midpoints of the four
     * sides of the rectangle. Note that if the given width and height is
     * different than what the system can support then the cursor's dimensions
     * will be set to the ones that closely matches the system supported cursor
     * size. The caller may first call
     * Toolkit.getDefaultToolkit().getBestCursorSize(curWidth, curHeight) to
     * find out the cursor dimensions that the system will use instead of the
     * ones passed in to this function.
     *
     * @param isCrossHairRequired if set to true the method will draw a cross hair inside the
     *                            rectangle
     * @param curWidth            the required cursor width.
     * @param curHeight           the required cursor height.
     * @return The rectangular cursor.
     */
    public static Cursor createRectangularCursor(int curWidth, int curHeight, boolean isCrossHairRequired) {
        int pix[] = new int[curWidth * curHeight];

        for (int y = 0; y < (curHeight * curHeight); y++) {
            pix[y] = 0; // all points transparent
        }
        int curCol = Color.WHITE.getRGB();
        int index = 0;
        // Drawing the top line of the rectangular.
        for (int x = 0; x < curWidth; x++) {
            pix[x] = curCol;
        }
        // Drawing the bottom of the rectangle
        index = (curWidth) * (curHeight - 1);
        for (int x = 0; x < curWidth; x++, index++) {
            pix[index] = curCol;
        }
        // Drawing the left of the rectangle
        index = 0;
        for (int x = 0; x < curHeight; x++) {
            pix[index] = curCol;
            index += curWidth;
        }
        // Drawing the right of the rectangle
        index = curWidth - 1;
        for (int x = 0; x < curHeight; x++) {
            pix[index] = curCol;
            index += curWidth;
        }
        if (isCrossHairRequired) {
            // Drawing the cross hair.
            // the vertical line
            index = curWidth / 2;
            for (int x = 0; x < curHeight; x++) {
                pix[index] = curCol;
                index += curWidth;
            }
            // the horizontal line
            index = (curWidth) * ((curHeight - 1) / 2);
            for (int x = 0; x < curWidth; x++, index++) {
                pix[index] = curCol;
            }
        }
        Image img = Toolkit.getDefaultToolkit().createImage(
                new MemoryImageSource(curWidth, curHeight, pix, 0, curWidth));
        Cursor curRect = Toolkit.getDefaultToolkit().createCustomCursor(img,
                new Point(0, 0), "rect");
        return curRect;
    }

    /**
     * This method creates a circular cursor with a dot in the center.
     *
     * @param img
     * @return
     */
    public static Cursor createPointCursor() {
        int curWidth = 32;
        int curHeight = 32;

        int pix[] = new int[curWidth * curHeight];
        for (int y = 0; y <= curHeight; y++)
            for (int x = 0; x <= curWidth; x++)
                pix[y + x] = 0; // all points transparent

        // black circle - outside
        int curCol = Color.black.getRGB();
        int yscale = 10;
        int xscale = 10;
        for (int x = 2; x <= 8; x++)
            pix[x] = curCol; // up
        for (int x = 2; x <= 8; x++)
            pix[(yscale * curWidth) + x] = curCol; // bottom
        for (int y = 2; y <= 8; y++)
            pix[curWidth * y] = curCol; // left
        for (int y = 2; y <= 8; y++)
            pix[(curWidth * y) + yscale] = curCol; // right
        pix[1 + curWidth] = curCol;
        pix[yscale + curWidth - 1] = curCol;
        pix[1 + (curWidth * (yscale - 1))] = curCol;
        pix[(curWidth * (yscale - 1)) + yscale - 1] = curCol;

        // white circle - inside
        curCol = Color.white.getRGB();
        yscale = yscale - 1;
        xscale = xscale - 1;
        for (int x = 3; x <= 7; x++)
            pix[x + curWidth] = curCol; // up
        for (int x = 3; x <= 7; x++)
            pix[(yscale * curWidth) + x] = curCol; // bottom
        for (int y = 3; y <= 7; y++)
            pix[curWidth * y + 1] = curCol; // left
        for (int y = 3; y <= 7; y++)
            pix[(curWidth * y) + yscale] = curCol; // right
        pix[2 + curWidth + curWidth] = curCol;
        pix[yscale + curWidth + curWidth - 1] = curCol;
        pix[1 + (curWidth * (yscale - 1)) + 1] = curCol;
        pix[(curWidth * (yscale - 1)) + yscale - 1] = curCol;

        Image img = Toolkit.getDefaultToolkit().createImage(
                new MemoryImageSource(curWidth, curHeight, pix, 0, curWidth));
        Cursor curCircle = Toolkit.getDefaultToolkit().createCustomCursor(img,
                new Point(5, 5), "circle");
        return curCircle;
    }

    /**
     * This method returns the default configuration that is suitable to the
     * current hardware specification. This information can then be used to
     * create a bufferedimage which is perfectly suited to the hardware and
     * system configuration.
     *
     * @return
     */
    public static GraphicsConfiguration getDefaultConfiguration() {
        GraphicsEnvironment ge = GraphicsEnvironment
                .getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }

    /**
     * Returns a buffered image which is better suited to the host machine's
     * hardware.
     *
     * @param image The original buffered image
     * @param gc    the graphics configuration returned from the
     *              getDefaultConfiguration method.
     * @return the bufferedimage that is wellsuited to the host machine.
     */
    public static BufferedImage toCompatibleImage(BufferedImage image, GraphicsConfiguration gc) {
        if (gc == null)
            gc = getDefaultConfiguration();
        int w = image.getWidth();
        int h = image.getHeight();
        int transparency = image.getColorModel().getTransparency();
        BufferedImage result = gc.createCompatibleImage(w, h, transparency);
        Graphics2D g2 = result.createGraphics();
        g2.drawRenderedImage(image, null);
        g2.dispose();
        return result;
    }

    /**
     * This method create a copy of the buffered image which is much smoother.
     */
    public static BufferedImage copy(BufferedImage source, BufferedImage target) {
        Graphics2D g2 = target.createGraphics();
        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,  RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR);
        double scalex = (double) target.getWidth() / source.getWidth();
        double scaley = (double) target.getHeight() / source.getHeight();
        AffineTransform xform = AffineTransform.getScaleInstance(scalex, scaley);
        g2.drawRenderedImage(source, xform);
        g2.dispose();
        return target;
    }

    /**
     * returns a scaled up version of the original image. this method is
     * ineffective if we want a scaled down version. for scaling down use
     * getscaledinstance() instead.
     *
     * @param image
     * @param width
     * @param height
     * @param gc
     * @return
     */
    public static BufferedImage getScaledInstance(BufferedImage image, int width, int height, GraphicsConfiguration gc) {
        if (gc == null)
            gc = getDefaultConfiguration();
        int transparency = image.getColorModel().getTransparency();
        return copy(image, gc
                .createCompatibleImage(width, height, transparency));
    }

    /**
     * returns a scaled up version of the original image. this method is
     * ineffective if we want a scaled down version. for scaling down use
     * getscaledinstance() instead.
     *
     * @param image
     * @param width
     * @param height
     * @return
     */
    public static BufferedImage getScaledInstance(BufferedImage image, int width, int height) {
        GraphicsConfiguration gc = getDefaultConfiguration();
        int transparency = image.getColorModel().getTransparency();
        return copy(image, gc
                .createCompatibleImage(width, height, transparency));
    }

    /**
     * This method gets a scaled up version with the specified color mode.
     *
     * @param image
     * @param width
     * @param height
     * @param cm
     * @return
     */
    public static BufferedImage getScaledInstance(BufferedImage image, int width, int height, ColorModel cm) {
        WritableRaster raster = cm
                .createCompatibleWritableRaster(width, height);
        boolean isRasterPremultiplied = cm.isAlphaPremultiplied();
        return copy(image, new BufferedImage(cm, raster, isRasterPremultiplied,
                null));
    }

    /*
     * Arguments: sdir and sfile are the result of the FileDialog()
     * getDirectory() and getFile() methods. Returns: Image Object, be sure to
     * check for (Image)null !!!!
     */
    public static BufferedImage loadbitmap(String url) {
        BufferedImage image;
        try {
            FileInputStream fs = new FileInputStream(url);
            int bflen = 14; // 14 byte BITMAPFILEHEADER
            byte bf[] = new byte[bflen];
            fs.read(bf, 0, bflen);
            int bilen = 40; // 40-byte BITMAPINFOHEADER
            byte bi[] = new byte[bilen];
            fs.read(bi, 0, bilen);

            // Interperet data.
            int nsize = (((int) bf[5] & 0xff) << 24) | (((int) bf[4] & 0xff) << 16) | (((int) bf[3] & 0xff) << 8) | (int) bf[2] & 0xff;

            int nbisize = (((int) bi[3] & 0xff) << 24) | (((int) bi[2] & 0xff) << 16) | (((int) bi[1] & 0xff) << 8) | (int) bi[0] & 0xff;

            int nwidth = (((int) bi[7] & 0xff) << 24) | (((int) bi[6] & 0xff) << 16) | (((int) bi[5] & 0xff) << 8) | (int) bi[4] & 0xff;

            int nheight = (((int) bi[11] & 0xff) << 24) | (((int) bi[10] & 0xff) << 16) | (((int) bi[9] & 0xff) << 8) | (int) bi[8] & 0xff;

            int nplanes = (((int) bi[13] & 0xff) << 8) | (int) bi[12] & 0xff;

            int nbitcount = (((int) bi[15] & 0xff) << 8) | (int) bi[14] & 0xff;

            // Look for non-zero values to indicate compression
            int ncompression = (((int) bi[19]) << 24) | (((int) bi[18]) << 16) | (((int) bi[17]) << 8) | (int) bi[16];

            int nsizeimage = (((int) bi[23] & 0xff) << 24) | (((int) bi[22] & 0xff) << 16) | (((int) bi[21] & 0xff) << 8) | (int) bi[20] & 0xff;

            int nxpm = (((int) bi[27] & 0xff) << 24) | (((int) bi[26] & 0xff) << 16) | (((int) bi[25] & 0xff) << 8) | (int) bi[24] & 0xff;

            int nypm = (((int) bi[31] & 0xff) << 24) | (((int) bi[30] & 0xff) << 16) | (((int) bi[29] & 0xff) << 8) | (int) bi[28] & 0xff;

            int nclrused = (((int) bi[35] & 0xff) << 24) | (((int) bi[34] & 0xff) << 16) | (((int) bi[33] & 0xff) << 8) | (int) bi[32] & 0xff;

            int nclrimp = (((int) bi[39] & 0xff) << 24) | (((int) bi[38] & 0xff) << 16) | (((int) bi[37] & 0xff) << 8) | (int) bi[36] & 0xff;

            if (nbitcount == 24) {
                // No Palatte data for 24-bit format but scan lines are
                // padded out to even 4-byte boundaries.
                int npad = (nsizeimage / nheight) - nwidth * 3;
                int ndata[] = new int[nheight * nwidth];
                byte brgb[] = new byte[(nwidth + npad) * 3 * nheight];
                fs.read(brgb, 0, (nwidth + npad) * 3 * nheight);
                int nindex = 0;
                image = new BufferedImage(nwidth, nheight, BufferedImage.TYPE_INT_RGB);
                for (int j = 0; j < nheight; j++) {
                    for (int i = 0; i < nwidth; i++) {
                        ndata[nwidth * (nheight - j - 1) + i] = (255 & 0xff) << 24
                                | (((int) brgb[nindex + 2] & 0xff) << 16)
                                | (((int) brgb[nindex + 1] & 0xff) << 8)
                                | (int) brgb[nindex] & 0xff;
                        nindex += 3;
                    }
                    nindex += npad;
                }

                image = new BufferedImage(nwidth, nheight, BufferedImage.TYPE_INT_RGB);
                image.setRGB(0, 0, nwidth, nheight, ndata, 0, nwidth);

            } else if (nbitcount == 8) {
                // Have to determine the number of colors, the clrsused
                // parameter is dominant if it is greater than zero. If
                // zero, calculate colors based on bitsperpixel.
                int nNumColors = 0;
                if (nclrused > 0) {
                    nNumColors = nclrused;
                } else {
                    nNumColors = (1 & 0xff) << nbitcount;
                }

                // Some bitmaps do not have the sizeimage field calculated
                // Ferret out these cases and fix 'em.
                if (nsizeimage == 0) {
                    nsizeimage = ((((nwidth * nbitcount) + 31) & ~31) >> 3);
                    nsizeimage *= nheight;
                }

                // Read the palatte colors.
                int npalette[] = new int[nNumColors];
                byte bpalette[] = new byte[nNumColors * 4];
                fs.read(bpalette, 0, nNumColors * 4);
                int nindex8 = 0;
                for (int n = 0; n < nNumColors; n++) {
                    npalette[n] = (255 & 0xff) << 24 | (((int) bpalette[nindex8 + 2] & 0xff) << 16) | (((int) bpalette[nindex8 + 1] & 0xff) << 8) | (int) bpalette[nindex8] & 0xff;
                    nindex8 += 4;
                }

                // Read the image data (actually indices into the palette)
                // Scan lines are still padded out to even 4-byte
                // boundaries.
                int npad8 = (nsizeimage / nheight) - nwidth;

                int ndata8[] = new int[nwidth * nheight];
                byte bdata[] = new byte[(nwidth + npad8) * nheight];
                fs.read(bdata, 0, (nwidth + npad8) * nheight);
                nindex8 = 0;
                for (int j8 = 0; j8 < nheight; j8++) {
                    for (int i8 = 0; i8 < nwidth; i8++) {
                        ndata8[nwidth * (nheight - j8 - 1) + i8] = npalette[((int) bdata[nindex8] & 0xff)];
                        nindex8++;
                    }
                    nindex8 += npad8;
                }

                image = new BufferedImage(nwidth, nheight, BufferedImage.TYPE_INT_RGB);
                image.setRGB(0, 0, nwidth, nheight, ndata8, 0, nwidth);

            } else {
                log.error("Not a 24-bit or 8-bit Windows Bitmap, aborting...");
                image = null;
            }

            fs.close();
            return image;
        } catch (Exception e) {
            log.error("Caught exception in load bitmap!");
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * Polymorphic version of the loadbitmap() method but it takes in the
     * InputStream instead of the file location.
     *
     * @param fs
     * @return
     */
    public static BufferedImage loadbitmap(InputStream is) {
        BufferedImage image;
        BufferedInputStream fs = new BufferedInputStream(is);
        try {
            int bflen = 14; // 14 byte BITMAPFILEHEADER
            byte bf[] = new byte[bflen];
            fs.read(bf, 0, bflen);
            int bilen = 40; // 40-byte BITMAPINFOHEADER
            byte bi[] = new byte[bilen];
            fs.read(bi, 0, bilen);

            // Interperet data.
//			int nsize = (((int) bf[5] & 0xff) << 24)
//					| (((int) bf[4] & 0xff) << 16)
//					| (((int) bf[3] & 0xff) << 8) | (int) bf[2] & 0xff;
            // System.out.println("File type is :" + (char) bf[0] + (char)
            // bf[1]);
            // System.out.println("Size of file is :" + nsize);

//			int nbisize = (((int) bi[3] & 0xff) << 24)
//					| (((int) bi[2] & 0xff) << 16)
//					| (((int) bi[1] & 0xff) << 8) | (int) bi[0] & 0xff;
            // System.out.println("Size of bitmapinfoheader is :" + nbisize);

            int nwidth = (((int) bi[7] & 0xff) << 24)
                    | (((int) bi[6] & 0xff) << 16)
                    | (((int) bi[5] & 0xff) << 8) | (int) bi[4] & 0xff;
            // System.out.println("Width is :" + nwidth);

            int nheight = (((int) bi[11] & 0xff) << 24)
                    | (((int) bi[10] & 0xff) << 16)
                    | (((int) bi[9] & 0xff) << 8) | (int) bi[8] & 0xff;
            // System.out.println("Height is :" + nheight);

//			int nplanes = (((int) bi[13] & 0xff) << 8) | (int) bi[12] & 0xff;
            // System.out.println("Planes is :" + nplanes);

            int nbitcount = (((int) bi[15] & 0xff) << 8) | (int) bi[14] & 0xff;
            // System.out.println("BitCount is :" + nbitcount);

            // Look for non-zero values to indicate compression
//			int ncompression = (((int) bi[19]) << 24) | (((int) bi[18]) << 16)
//					| (((int) bi[17]) << 8) | (int) bi[16];
            // System.out.println("Compression is :" + ncompression);

            int nsizeimage = (((int) bi[23] & 0xff) << 24)
                    | (((int) bi[22] & 0xff) << 16)
                    | (((int) bi[21] & 0xff) << 8) | (int) bi[20] & 0xff;
            // System.out.println("SizeImage is :" + nsizeimage);

//			int nxpm = (((int) bi[27] & 0xff) << 24)
//					| (((int) bi[26] & 0xff) << 16)
//					| (((int) bi[25] & 0xff) << 8) | (int) bi[24] & 0xff;
            // System.out.println("X-Pixels per meter is :" + nxpm);

//			int nypm = (((int) bi[31] & 0xff) << 24)
//					| (((int) bi[30] & 0xff) << 16)
//					| (((int) bi[29] & 0xff) << 8) | (int) bi[28] & 0xff;
            // System.out.println("Y-Pixels per meter is :" + nypm);

            int nclrused = (((int) bi[35] & 0xff) << 24)
                    | (((int) bi[34] & 0xff) << 16)
                    | (((int) bi[33] & 0xff) << 8) | (int) bi[32] & 0xff;
            // System.out.println("Colors used are :" + nclrused);

//			int nclrimp = (((int) bi[39] & 0xff) << 24)
//					| (((int) bi[38] & 0xff) << 16)
//					| (((int) bi[37] & 0xff) << 8) | (int) bi[36] & 0xff;
            // System.out.println("Colors important are :" + nclrimp);

            if (nbitcount == 24) {
                // No Palatte data for 24-bit format but scan lines are
                // padded out to even 4-byte boundaries.
                int npad = (nsizeimage / nheight) - nwidth * 3;
                int ndata[] = new int[nheight * nwidth];
                byte brgb[] = new byte[(nwidth + npad) * 3 * nheight];
                fs.read(brgb, 0, (nwidth + npad) * 3 * nheight);
                int nindex = 0;
                image = new BufferedImage(nwidth, nheight,
                        BufferedImage.TYPE_INT_RGB);
                for (int j = 0; j < nheight; j++) {
                    for (int i = 0; i < nwidth; i++) {
                        ndata[nwidth * (nheight - j - 1) + i] = (255 & 0xff) << 24
                                | (((int) brgb[nindex + 2] & 0xff) << 16)
                                | (((int) brgb[nindex + 1] & 0xff) << 8)
                                | (int) brgb[nindex] & 0xff;
                        nindex += 3;
                    }
                    nindex += npad;
                }

                image = new BufferedImage(nwidth, nheight,
                        BufferedImage.TYPE_INT_RGB);
                image.setRGB(0, 0, nwidth, nheight, ndata, 0, nwidth);
                // image = toBufferedImage(createImage( new MemoryImageSource
                // (nwidth, nheight,ndata, 0, nwidth)));
            } else if (nbitcount == 8) {
                // Have to determine the number of colors, the clrsused
                // parameter is dominant if it is greater than zero. If
                // zero, calculate colors based on bitsperpixel.
                int nNumColors = 0;
                if (nclrused > 0) {
                    nNumColors = nclrused;
                } else {
                    nNumColors = (1 & 0xff) << nbitcount;
                }
                // System.out.println("The number of Colors is" + nNumColors);

                // Some bitmaps do not have the sizeimage field calculated
                // Ferret out these cases and fix 'em.
                if (nsizeimage == 0) {
                    nsizeimage = ((((nwidth * nbitcount) + 31) & ~31) >> 3);
                    nsizeimage *= nheight;
                    // System.out.println("nsizeimage (backup) is" +
                    // nsizeimage);
                }

                // Read the palatte colors.
                int npalette[] = new int[nNumColors];
                byte bpalette[] = new byte[nNumColors * 4];
                fs.read(bpalette, 0, nNumColors * 4);
                int nindex8 = 0;
                for (int n = 0; n < nNumColors; n++) {
                    npalette[n] = (255 & 0xff) << 24
                            | (((int) bpalette[nindex8 + 2] & 0xff) << 16)
                            | (((int) bpalette[nindex8 + 1] & 0xff) << 8)
                            | (int) bpalette[nindex8] & 0xff;
                    nindex8 += 4;
                }

                // Read the image data (actually indices into the palette)
                // Scan lines are still padded out to even 4-byte
                // boundaries.
                int npad8 = (nsizeimage / nheight) - nwidth;

                int ndata8[] = new int[nwidth * nheight];
                byte bdata[] = new byte[(nwidth + npad8) * nheight];
                fs.read(bdata, 0, (nwidth + npad8) * nheight);
                nindex8 = 0;
                for (int j8 = 0; j8 < nheight; j8++) {
                    for (int i8 = 0; i8 < nwidth; i8++) {
                        ndata8[nwidth * (nheight - j8 - 1) + i8] = npalette[((int) bdata[nindex8] & 0xff)];
                        nindex8++;
                    }
                    nindex8 += npad8;
                }

                image = new BufferedImage(nwidth, nheight, BufferedImage.TYPE_INT_RGB);
                image.setRGB(0, 0, nwidth, nheight, ndata8, 0, nwidth);
            } else {
                log.error("Not a 24-bit or 8-bit Windows Bitmap, aborting...");
                image = null;
            }
            fs.close();
            return image;
        } catch (Exception e) {
            log.error("Caught exception in load bitmap!");
            log.error(e.getMessage());
        }
        return null;
    }

    /**
     * Returns the number of lines needed to show the specified Text in the
     * given label. This is useful when we need to set the Label height based on
     * the text that needs to be shown in the label. Currently this is being
     * used in determining the height of the specimen details panel in the
     * DetailChooserView.
     *
     * @param labelText
     * @param labelWidth
     * @param labelFontMetric
     * @return Vector each of its elements contain a line of the text.
     */
    public static Vector splitTextIntoLines(String labelText, int labelWidth, FontMetrics labelFontMetric) {
        int fromIndex = 0;
        int pos = 0;
        int bestpos;
        String newString;
        String largestString;
        Vector lines = new Vector();

        if (labelText == null || labelText.length() == 0)
            return lines;
        labelText = stripOffHTMLTags(labelText);
        newString = labelText;
        // while we haven't run past the end of the string...
        while (fromIndex != -1) {
            // Automatically skip any spaces at the beginning of the line
            while (fromIndex < labelText.length()
                    && labelText.charAt(fromIndex) == ' ') {
                ++fromIndex;
                // If we hit the end of line
                // while skipping spaces, we're done.
                if (fromIndex >= labelText.length())
                    break;
            }

            // fromIndex represents the beginning of the line
            pos = fromIndex;
            bestpos = -1;
            largestString = null;

            while (pos >= fromIndex) {
                boolean bHardNewline = false;
                int newlinePos = labelText.indexOf('\n', pos);
                int spacePos = labelText.indexOf(' ', pos);

                if (newlinePos != -1 && // there is a newline and either
                        ((spacePos == -1) || // 1. there is no space, or
                                (spacePos != -1 && newlinePos < spacePos)))
                // 2. the newline is first
                {
                    pos = newlinePos;
                    bHardNewline = true;
                } else {
                    pos = spacePos;
                    bHardNewline = false;
                }

                // Couldn't find another space?
                if (pos == -1) {
                    newString = labelText.substring(fromIndex);
                } else {
                    newString = labelText.substring(fromIndex, pos);
                }

                // If the string fits, keep track of it.
                if (labelFontMetric.stringWidth(newString) < labelWidth) {
                    largestString = newString;
                    bestpos = pos;

                    // If we've hit the end of the
                    // string or a newline, use it.
                    if (bHardNewline)
                        bestpos++;
                    if (pos == -1 || bHardNewline)
                        break;
                } else {
                    break;
                }

                ++pos;
            }

            if (largestString == null) {
                // Couldn't wrap at a space, so find the largest line
                // that fits and print that. Note that this will be
                // slightly off -- the width of a string will not necessarily
                // be the sum of the width of its characters, due to kerning.
                int totalWidth = 0;
                int oneCharWidth = 0;

                pos = fromIndex;

                while (pos < labelText.length()) {
                    oneCharWidth = labelFontMetric.charWidth(labelText
                            .charAt(pos));
                    if ((totalWidth + oneCharWidth) >= labelWidth)
                        break;
                    totalWidth += oneCharWidth;
                    ++pos;
                }

                lines.addElement(labelText.substring(fromIndex, pos));
                fromIndex = pos;
            } else {
                lines.addElement(largestString);
                fromIndex = bestpos;
            }
        }
        return lines;
    }

    /**
     * This method strips off the HTML tags from the input string
     *
     * @return th string without any HTML tags
     */
    public static String stripOffHTMLTags(String htmlString) {
        String rawText = null;
        if (htmlString != null) {
            rawText = htmlString.replaceAll("\\&lt;.*?\\&gt;", "");
            rawText = rawText.replaceAll("\\<.*?\\>", "");
        }
        return rawText;
    }


    /**
     * This method takes in an image and resizes it to the specified dimensions.
     *
     * @param inputImage         original image to be resized
     * @param widthScale         the width resizing factor
     * @param heightScale        the height resizing factor
     * @param transformationType e.g. AffineTransformOp.TYPE_NEAREST_NEIGHBOR or
     *                           AffineTransformOp.TYPE_BILINEAR or AffineTransformOp.TYPE_BICUBIC
     * @return resized BufferedImage.
     */
    public static BufferedImage resizeImage(BufferedImage inputImage, double widthScale, double heightScale, int transformationType) {
        AffineTransform tx = new AffineTransform();
        double xsc = widthScale;
        double ysc = heightScale;
        // System.out.println(" W==> " + inputImage.getWidth() * widthScale);
        tx.scale(xsc, ysc);
        // AffineTransformOp op = new AffineTransformOp(tx,
        // AffineTransformOp.TYPE_BILINEAR);
        AffineTransformOp op = new AffineTransformOp(tx,
                AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        // inputImage = op.filter(inputImage, null);
        // return inputImage;
        return op.filter(inputImage, null);
    }

    /**
     * This method removes the HTML <a></a> hyperlinks from the input string
     *
     * @param longDescription
     * @return
     */
    public static String removeLinks(String inputHTMLText) {
        int startLookingFrom = 0;
        try {
            while (true) {
                int startOfLink = inputHTMLText.indexOf("<a", startLookingFrom);
                if (startOfLink < 0)
                    break;
                int endOfLink = inputHTMLText.indexOf(">", startOfLink + 1);
                inputHTMLText = inputHTMLText.replaceAll(inputHTMLText.substring(startOfLink, endOfLink + 1), "");
                startLookingFrom = endOfLink;
            }
            inputHTMLText = inputHTMLText.replaceAll("</a>", "");
        } catch (Exception ex) {
            return inputHTMLText;
        }
        return inputHTMLText;
    }
}
