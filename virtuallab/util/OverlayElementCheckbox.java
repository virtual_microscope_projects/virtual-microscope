/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import virtuallab.MicroscopeView;
import virtuallab.OverlayElement;
/**
 * This utility class encapsulates all the functionality of the Overlay elements checkboxes
 * that appear in the EDS control.
 *
 */
public class OverlayElementCheckbox extends JCheckBox implements MouseListener, ActionListener {
    /**
     *
     */
    private static final long serialVersionUID = -4590018263498512391L;
    /**
     * The data related to the overlay element is stored in this variable.
     */
    public OverlayElement m_overlayElement;
    /**
     * The selected color in which this element will be shown to the user on the screen.
     */
    public Color m_selectedColor;
    /**
     * The parent frame that will contain the color chooser pop-up dialog box.
     */
    private JFrame m_parent;
    private MicroscopeView m_display;

    public OverlayElementCheckbox(JFrame parent, OverlayElement element, MicroscopeView display) {
        super(element.m_shortName);
        m_display = display;
        m_overlayElement = element;
        m_overlayElement.m_parentCheckbox = this;
        m_parent = parent;
        m_selectedColor = Constants.OverlayElementColor.getDefaultColor(getText());
        element.changeColor(m_selectedColor);
        setForeground(m_selectedColor);
        setSelected(true);
        m_overlayElement.makeDirty();
        addActionListener(this);
        addMouseListener(this);
        setToolTipText("<html>"+Locale.RightClickToChangeColor(virtuallab.Configuration.getApplicationLanguage())+" "+element.m_shortName + " <br>"+Locale.LeftClickToToggleElement(virtuallab.Configuration.getApplicationLanguage())+"</html>");
    }

    public void actionPerformed(ActionEvent e) {
        m_overlayElement.makeDirty();
        m_display.validate();
        m_display.repaint();
    }

    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            showColorChooser();
        } else {
            // user checked or unchecked the element.
            m_overlayElement.makeDirty();
        }
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    /**
     * The method shows the color chooser pop-up.
     */
    public void showColorChooser() {
        ColorChooserDialog dlg = new ColorChooserDialog(m_parent, getText(), m_selectedColor);
        dlg.setVisible(true);
        if (dlg.m_chosenColor != null && (m_selectedColor.getRGB() != dlg.m_chosenColor.getRGB())) {
            m_selectedColor = dlg.m_chosenColor;
            setForeground(m_selectedColor);
            m_overlayElement.changeColor(m_selectedColor);
            m_overlayElement.makeDirty();
            //redrawImage();
            /** All the tiles are now invalid, since those tiles were created
             * using the old color. We need to clear the cache.
             */
            m_display.getEDSControl().m_overlayImageHandler.clearCache();
            m_display.paintComponent((Graphics2D) m_display.getGraphics());
        }
        // else No change in color.
    }

    /**
     *  This method redraws the overlay image using the new color. This image
     *  will be used to form the consolidated overlay image.
     *
     *
     public void redrawImage()
     {
     m_overlayElement.makeDirty();
     /*
     // convert to HSB values.
     int rgb = m_selectedColor.getRGB();
     int red   = (rgb>>16)&0xFF;
     int green = (rgb>>8)&0xFF;
     int blue  = rgb&0xFF;
     m_overlayElement.setDirty(true);
     float[] hsb = Color.RGBtoHSB(red,green,blue,null);
     for (int i=0; i<m_overlayElement.getImageWidth(); i++)
     {
     for (int j=0; j<m_overlayElement.getImageHeight(); j++)
     {
     rgb = m_overlayElement.getImageRGB(i, j);
     //convert to HSB values.
     red = (rgb>>16)&0xFF;
     green = (rgb>>8)&0xFF;
     blue = rgb&0xFF;
     float[] inputHSB = Color.RGBtoHSB(red, green, blue, null);
     float brightness = inputHSB[2];
     // what interests us is the brightness.
     //System.out.println(Integer.toHexString(rgb));
     // using the brightness of the X-Ray image we shall
     // update the brightness of the user chosen color.
     rgb = Color.HSBtoRGB(hsb[0], hsb[1], brightness);
     //				  rgb = Color.HSBtoRGB(inputHSB[0], inputHSB[1], brightness);
     m_overlayElement.setImageRGB(i, j, rgb);
     //				  System.out.println(m_overlayElement.m_mapImage.getType());

     }
     }
     }*/
}
