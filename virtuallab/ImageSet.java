/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.awt.Dimension;
import java.awt.geom.Point2D;

import virtuallab.util.Constants;

// $Id: ImageSet.java,v 1.12 2006/09/05 01:08:47 manzoor2 Exp $

public class ImageSet {


    private Dimension m_imageSize;
    private Dimension m_tileSize;
    /**
     * The value that comunicates the scaling factor for the height value.
     * This oly applies to AFM. AFM specimens convey the specimen height
     * information as brightness of a pixel. The brightness value range
     * from [0-1] these need to be converted into the appropriate scale.
     * This field provide us with the required multiplying factor.
     */
    private Double m_pixelHeightRange;
    private Point2D m_pixelSize;
    /**
     * The common base path for all the tiles in this imageset. The common base
     * path is the path that leads to the row level sub-folder. This path must
     * be prepended to the Tile.getFilePath() to get the actual location of each
     * tile.
     */
    private String m_path;
    /**
     * The chosen tiles that are being drawn/prefetched.
     */
    private Tile[] m_chosenTiles;

    public ImageSet(Specimen.ImageDescriptor idesc, String path) {
        m_imageSize = idesc.getImageSize();
        m_tileSize = idesc.getTileSize();
        m_pixelHeightRange = idesc.getPixelHeightRange();
        m_pixelSize = new Point2D.Double(idesc.getPixelSize().x, idesc.getPixelSize().y);
        m_path = path;
    }

    public Dimension getImageSize() {
        return m_imageSize;
    }

    public Dimension getTileSize() {
        return m_tileSize;
    }

    public Point2D getPixelSize() {
        return m_pixelSize;
    }

    public Double getPixelHeightRange() {
        return m_pixelHeightRange;
    }

    /**
     * The common base path for all the tiles in this imageset. The common base
     * path is the path that leads to the row level sub-folder. This path must
     * be prepended to the Tile.getFilePath() to get the actual location of each
     * tile.
     *
     * @return the common base path for all the tiles in this image set
     */
    public String getPath() {
        return m_path;
    }

    /**
     * This method returns the chosen tiles.
     *
     * @return
     */
    public Tile[] getChosenTiles() {
        return m_chosenTiles;
    }

    /**
     * This method decides which tiles will be visible to the user. It takes in
     * the orientation and sizing information and then returns the tiles that
     * will be drawn on the screen/printer by the calling function.
     *
     * @param beam
     * @return
     */

    public Tile[] chooseDisplayTiles(MicroscopeView beam, int cacheSize,
                                     short scrollDirection) {
        int numRows = 0;
        if (m_tileSize.height > 0) {
            // Total of numRows rows of the tiles would be drawn
            numRows = (int) Math.ceil((double) m_imageSize.height
                    / (double) m_tileSize.height);
        }

        int numCols = 0;
        if (m_tileSize.width > 0) {
            // total of numCols columns would be drawn.
            numCols = (int) Math.ceil((double) m_imageSize.width
                    / (double) m_tileSize.width);
        }

        if (numRows < 1 || numCols < 1) {
            return null;
        }
        // Get the first row,column and the last row,column that are viewable on
        // the screen.
        // If the whole image can be filled on the current viewable screen then
        // the first row
        // and column will be the row 0 and column 0 of the tile set, and the
        // last col and row
        // will be the maximum rows and columns for the tile. Otherwise we do
        // some calculation
        // to figure out which sets of tiles will constitute the first and the
        // last rows,columns.
        int firstCol = 0;
        if (beam.getStagePosition().x < 0) {
            // The first tile of the specimen is not visible on the screen
            // so get the first column that needs to be shown on the screen.
            firstCol = (int) Math.floor(Math.abs(beam.getStagePosition().x)
                    / m_tileSize.width);
        }// else the first column itself is visible on the screen.

        int firstRow = 0;
        if (beam.getStagePosition().y < 0) { // The first tile of the
            // specimen is not visible on
            // the screen, so
            // we shall get the first row that needs to be shown on the screen.
            firstRow = (int) Math.floor(Math.abs(beam.getStagePosition().y)
                    / m_tileSize.height);
        }

        int lastCol = numCols > 0 ? numCols - 1 : numCols;
        if (beam.getStagePosition().x + m_imageSize.width > beam.getBeamSize().width) {
            // The specimen will spill over the viewable screen from the right
            // side
            // , so we can't show the last column of the specimen, let us then
            // find
            // out the maximum column that we CAN show on the screen i.e. the
            // last
            // viewable column, anything above that will be spill over and not
            // viewable.
            lastCol -= (int) Math.floor((m_imageSize.width
                    + beam.getStagePosition().x - beam.getBeamSize().width)
                    / m_tileSize.width);
        }// else the last column can be shown on the screen so no need to do
        // anything.

        int lastRow = numRows > 0 ? numRows - 1 : numRows;
        if (beam.getStagePosition().y + m_imageSize.height > beam.getBeamSize().height) {
            // The image will spill over the screen from the bottom.
            lastRow -= (int) Math.floor((m_imageSize.height
                    + beam.getStagePosition().y - beam.getBeamSize().height)
                    / m_tileSize.height);
        }

        if (lastRow < 0 || lastCol < 0) {
            // Oops, no tile to show. Something bad has happened !
            return null;
        }
        // So far we have figured out the tiles that MUST be loaded since they
        // are
        // immediately viewable to the user. But we can do some pre-loading by
        // loading
        // some additional tiles provided our cache has some place available.
        // We shall use the image scroll direction to help us decide which tiles
        // to preload.
        m_chosenTiles = preloadTiles(firstRow, lastRow, firstCol, lastCol, numRows, numCols, cacheSize, scrollDirection, beam.getStagePosition());
        return m_chosenTiles;
    }

    /**
     * This method checks to see if we can preload some additional tiles into
     * the cache provided there is some empty slots in the cache and we have
     * some left over tiles. This method only tries to load the tiles that we
     * expect the user to see in immediate future. This decision is based on the
     * direction in which the user is currently scrolling.
     *
     * @param firstRow          The first viewable tile row
     * @param lastRow           the last viewable tile row
     * @param firstCol          the first viewable tile column
     * @param lastCol           the last viewable tile column
     * @param numRows           the total number of tile rows in the specimen.
     * @param numCols           the total number of tile columns in the specimen.
     * @param cacheSize         the cache size in slots.
     * @param scrollDirection   the direction of scrolling
     * @param beamStagePosition the center of the viewable area.
     * @return the list of tiles that should be loaded
     */
    private Tile[] preloadTiles(int firstRow, int lastRow, int firstCol, int lastCol, int numRows, int numCols, int cacheSize, short scrollDirection, Point2D.Double beamStagePosition) {
        int origFirstRow = firstRow;
        int origFirstCol = firstCol;
        int origLastRow = lastRow;
        int origLastCol = lastCol;

        int numTiles = (1 + (lastCol - firstCol)) * (1 + (lastRow - firstRow));
        int vacantSpots = cacheSize - numTiles;
        // Do we have additional tiles that we can pre-load ?. note that
        // our minimum unit of tile loading is either a complete single row
        // or a complete single column - so the vacantSpots must be at least
        // enough
        // to either load a single row or a single column. Anything less won't
        // do.
        if ((vacantSpots > numRows || vacantSpots > numCols) && (numRows * numCols) > numTiles) {
            boolean exhausted = false;
            switch (scrollDirection) {
                case Constants.NORTH:
                    // how many rows of tiles can we preload ?
                    firstRow -= vacantSpots / numCols;
                    // TODO: The logic below may be redundant since we have the
                    // (numRows * numCols) > numTiles condition guarding this
                    // switch.
                    // See if this is so and remove it if required.
                    firstRow = (firstRow < 0 ? 0 : firstRow);
                    break;
                case Constants.SOUTH:
                    // how many rows of tiles can we preload ?
                    lastRow += vacantSpots / numCols;
                    lastRow = (lastRow > (numRows - 1) ? (numRows - 1) : lastRow);
                    break;
                case Constants.WEST:
                    // how many columns of tiles can we preload ?
                    firstCol -= vacantSpots / numRows;
                    firstCol = (firstCol < 0 ? 0 : firstCol);
                    break;
                case Constants.EAST:
                    // how many columns of tiles can we preload ?
                    lastCol += vacantSpots / numRows;
                    lastCol = (lastCol > (numCols - 1) ? (numCols - 1) : lastCol);
                    break;
                case Constants.NORTHEAST:
                    // how many rows and columns of tiles can we preload ?
                    // Let us try with one row and one column first and keep going
                    // until we fill up all the vacant slots.
                    while ((firstRow > 0 || lastCol < (numCols - 1)) && !exhausted) {
                        exhausted = true;
                        if (vacantSpots / numCols > 0 && firstRow > 0) {
                            firstRow--;
                            vacantSpots -= numCols;
                            exhausted = false;
                        }
                        if (vacantSpots / numRows > 0 && lastCol < (numCols - 1) && !exhausted) {
                            lastCol++;
                            vacantSpots -= numRows;
                            exhausted = false;
                        }
                        // Managed to add one row at top and one column
                        // to the right. Let us try to add yet another set
                        // or row and column.
                    }
                    break;
                case Constants.SOUTHEAST:
                    // how many rows and columns of tiles can we preload ?
                    // Let us try with one row and one column first and keep going
                    // until we fill up all the vacant slots.
                    while ((lastRow < (numRows - 1) || lastCol < (numCols - 1)) && !exhausted) {
                        exhausted = true;
                        if (vacantSpots / numCols > 0 && lastRow < (numRows - 1)) {
                            lastRow++;
                            vacantSpots -= numCols;
                            exhausted = false;
                        }
                        if (vacantSpots / numRows > 0 && lastCol < (numCols - 1)) {
                            lastCol++;
                            vacantSpots -= numRows;
                            exhausted = false;
                        }
                        // Managed to add one row at top and one column
                        // to the right. Let us try to add yet another set
                        // or row and column.
                    }
                    break;
                case Constants.NORTHWEST:
                    // how many rows and columns of tiles can we preload ?
                    // Let us try with one row and one column first and keep going
                    // until we fill up all the vacant slots.
                    while ((firstRow > 0 || firstCol > 0) && !exhausted) {
                        exhausted = true;
                        if (vacantSpots / numCols > 0 && firstRow > 0) {
                            firstRow--;
                            vacantSpots -= numCols;
                            exhausted = false;
                        }
                        if (vacantSpots / numRows > 0 && firstCol > 0) {
                            firstCol--;
                            vacantSpots -= numRows;
                            exhausted = false;
                        }
                        // Managed to add one row at top and one column
                        // to the right. Let us try to add yet another set
                        // or row and column.
                    }
                    break;
                case Constants.SOUTHWEST:
                    // how many rows and columns of tiles can we preload ?
                    // Let us try with one row and one column first and keep going
                    // until we fill up all the vacant slots.
                    while ((lastRow < (numRows - 1) || firstCol > 0) && !exhausted) {
                        exhausted = true;
                        if (vacantSpots / numCols > 0 && lastRow < (numRows - 1)) {
                            lastRow++;
                            vacantSpots -= numCols;
                            exhausted = false;
                        }
                        if (vacantSpots / numRows > 0 && firstCol > 0) {
                            firstCol--;
                            vacantSpots -= numRows;
                            exhausted = false;
                        }
                        // Managed to add one row at top and one column
                        // to the right. Let us try to add yet another set
                        // or row and column.
                    }
                    break;
            }
            numTiles = (1 + lastCol - firstCol) * (1 + lastRow - firstRow);
        }

        // check if preloading is beneficial. There may be a case when we
        // over-preload
        // thus resulting jitter that has a large duration. This needs to be
        // avoided.
        // if we come across a situation where we have decided to pre-load large
        // tiles, we shall forceably reduce this number.

        // System.out.println("Rows :" + numRows);
        // System.out.println("Cols :" + numCols);
        int x = (lastRow - firstRow + 1);
        int y = (lastCol - firstCol + 1);

        // Try to reduce the preloading, but we must still load the mandatory
        // tiles - which are
        // the tiles that are viewable currently.
        if ((x * y) > Constants.PRELOADING_THRESHOLD) {
            // System.out.println("Using Old Algorithm");
            lastRow = origLastRow;
            firstRow = origFirstRow;
            firstCol = origFirstCol;
            lastCol = origLastCol;
            x = (lastRow - firstRow + 1);
            y = (lastCol - firstCol + 1);
            numTiles = x * y;
        }

        Tile[] retVal = new Tile[numTiles];
        int i = 0;
        for (int row = firstRow; row <= lastRow; row++) {
            for (int col = firstCol; col <= lastCol; col++) {
                Tile tile = new virtuallab.Tile(row, col, (int) (col
                        * m_tileSize.getWidth() + beamStagePosition.x), (int) (row
                        * m_tileSize.getHeight() + beamStagePosition.y));
                retVal[i++] = tile;
            }
        }
        // System.out.println("Loaded -----> " + retVal.length);
        return retVal;
    }
}