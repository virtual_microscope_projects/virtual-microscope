/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import virtuallab.util.Constants;

import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.zip.ZipException;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import virtuallab.util.Locale;


// $Id: Specimen.java,v 1.21 2006/09/05 01:08:47 manzoor2 Exp $
/**
 * This class contains all the specimen related objects. It has inner classes
 * one for each of the main logical components of a specimen e.g. the controls, the image tiles,
 * the image dimensions, etc. This class completely loads itself from the specimen.xml file.
 * <p>
 * Specimen is implemented using "Composite Design" pattern, in which the SpecimenElement acts as the
 * abstract class. Specimen acts as the container (i.e. it contains several other SpecimenElements)
 * and other subclasses of SpecimenElement act as the leaf nodes (i.e they do not contain
 * any SpecimenElement).
 * This class supports multiple versions of the specimen.xml i.e. a specimen defined using an old
 * XML Format and one described by new XML Format. It sees the specimen.xml file XMLrevision tag and
 * decides which version of the XML it is dealing with and then appropriately switches to a backward
 * compatabile mode (for revisions 1.0 or less) or to a latest version mode (for revisions >1.0).
 *
 * @extends SpecimenElement
 */

public class Specimen extends SpecimenElement {

    private static final Log log = new Log(SpecimenElement.class.getName());

    private static ArrayList individuals;
    /**
     * The URL or the local path of the specimen jar file. This is the URL from where the
     * specimen can be downloaded (for offline specimens) and the path of the jar file from
     * which the specimen has been loaded from (for Online specimens)
     */
    protected String m_specimenJarFilePath;
    /**
     * This variable holds the XML version of the specimen. It gets populated
     * from the XML "Version" tag in the secimen.xml file.
     */
    private double m_xmlVersion = 0;
    /**
     * is true if this specimen contains EDS functionality, false otherwise.
     */
    private boolean m_isEds = false;
    /**
     * is true if this specimen is an AFM specimenm, false otherwise. This
     * distinction is needed since an AFM specimen behaves differently than any
     * othe specimen in several ways.
     */
    private boolean m_isAfm = false;
    /**
     * holds the dimensions of the first image that is read from the specimen file.
     * This variale is needed for cases when we need to know
     * the aspect ratio and the image size of the image which hasn't been loaded yet.
     * For example to draw the navigation thumbnail we need to know the aspect ratio
     * of the base image so that the thumbnail's dimensions can be adjusted but
     * since the base image only gets drawn after the side controls are drawn the
     * thumbnail navigator side control can not use the loaded imageSet to find out
     * the aspect ratio; hence this variable was created to satisfy such requirements.
     */
    private Point2D.Double m_firstReadImageSize;
    /**
     * Holds the EDS noise information.
     */
    private OverlayElement m_edsNoise;
    private JarFile m_jarFile;
    private File m_jarFileInfo;
    private LinkedHashMap m_controlDescriptors = new LinkedHashMap();
    private LinkedHashMap m_controlStates = new LinkedHashMap();
    private LinkedHashMap m_instrumentDescriptors = new LinkedHashMap();
    private Information m_sampleInfo;
    private Information m_instrumentInfo;
    private LinkedHashMap m_imageDescriptors = new LinkedHashMap();
    private LinkedHashMap m_controlStateDescriptors = new LinkedHashMap();
    private Root m_root;
    private HashMap m_imageSets = new HashMap();

    /**
     * Default constructor of the class.
     */
    public Specimen(String specimenURLOrFile) {
        m_specimenJarFilePath = specimenURLOrFile;
    }

    /**
     * Specimen constructor - unpersists Specimen's definitions from jar with
     * the supplied filename.
     *
     * @param specimenJar specimen jar file
     * @throws SpecimenException
     */
    public Specimen(File specimenJar) throws SpecimenException, ZipException {
        try {
            try {
                m_jarFile = new JarFile(specimenJar);
            } catch (ZipException ze) {
                //TODO 84 ask user if it wants to delete the invalid specimen file
                //specimenJar.delete();
                throw new SpecimenException("The specimen file " + specimenJar.getCanonicalPath() + " is an invalid specimen file.");
            }

            Manifest manifest = m_jarFile.getManifest();

            if (manifest == null) {
                throw new SpecimenException("Specimen archive contains no manifest.");
            }

            String specimenDefinitionFileName = manifest.getMainAttributes().getValue("Specimen-Definition");

            if (specimenDefinitionFileName == null || specimenDefinitionFileName.equals("")) {
                specimenDefinitionFileName = Constants.SPECIMEN_FILE_NAME; // Use default name.
            }

            JarEntry specimenDefinitionEntry = m_jarFile.getJarEntry(specimenDefinitionFileName);

            if (specimenDefinitionEntry == null) {
                throw new SpecimenException("Specimen definition file missing from archive.");
            }

            try {
                InputStream specimenDefinitionXML = m_jarFile.getInputStream(specimenDefinitionEntry);

                if (specimenDefinitionXML == null) {
                    throw new SpecimenException("Unable to acquire input stream for specimen definition.");
                }
                m_specimenJarFilePath = specimenJar.getAbsolutePath();
                load(specimenDefinitionXML);
                m_jarFileInfo = specimenJar;
                m_jarFile.close();
                m_jarFile = null;
            } catch (Exception e) {
                throw new SpecimenException("Error attempting to read specimen definition.", e);
            }
        } catch (IOException e) {
            throw new SpecimenException("Error while attempting to open specimen.", e);
        }
    }

    /**
     * This method inputs a node and sees if this is the leaf node in the <Control> node hierarchy.
     * We need this information, because only the leaf node contains the image files information
     * e.g. the tile size, the total image size and the Pixel size.
     * <p>
     * This method sees if the node id is equal to Specimen.LEAF_NODE if it is it willthen see if the
     * node also contains an attribute named FolderName. This seconnd check is necessary because there
     * may be some <Control> tags with the same id as Specimen.LEAF_NODE.
     *
     * @param node the node which is suspected of being a leaf node.
     * @return true if the Node is an image leaf node, false otherwise.
     */
    static protected boolean isImageLeafNode(Node node) {
        if (node.hasAttributes()) {
            NamedNodeMap attrs = node.getAttributes();
            Node idNode = attrs.getNamedItem(Constants.XMLTags.ID);
            Node folderNode = attrs.getNamedItem(Constants.XMLTags.FOLDER_NAME);

            if (idNode != null &&
                    folderNode != null &&
                    idNode.getNodeValue().equals(Constants.LEAF_NODE)) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method determines if the input element needs to be processed inidvidually. The elements
     * like controlstates, imagedescriptors etc. require involved and speific handling; whereas
     * others like Author, Copyright, Display string etc. are just information tags that do not require
     * any specific handling.
     *
     * @param elementName
     * @return
     */
    private static boolean isHandledIndividually(String elementName) {
        if (individuals == null) {
            individuals = new ArrayList();

            // Required for the old XML format
            individuals.add("ImageDescriptor");
            individuals.add("InstrumentDescriptor");
            individuals.add("ControlDescriptor");
            individuals.add("ControlState");
            individuals.add("ControlStates");
            individuals.add("Root");
            //////////////////////////////////////

            //Required for the latest XML format.
            individuals.add(Constants.XMLTags.CONTROL_DESCRIPTORS);
            //individuals.add(Constants.XMLTags.LEVEL);
            individuals.add(Constants.XMLTags.CONTROL);
            /////////////////////////////////////////
        }

        return individuals.contains(elementName);
    }

    public boolean isAfm() {
        return m_isAfm;
    }

    /**
     * Returns the aspect ratio of the base image. The aspect ratio is
     * calculated as the image Width/ image Height.
     *
     * @return
     */
    public Point2D.Double getFirstImageDimensions() {
        return m_firstReadImageSize;
    }

    public short getSpecimenType() {
        return ONLINE_SPECIMEN;
    }

    public String getJarFilePath() {
        return m_specimenJarFilePath;
    }

    public void setJarFilePath(String pathOrUrl) {
        m_specimenJarFilePath = pathOrUrl;
    }

    /**
     * returns true if this specimen has EDS functionality, false otherwise.
     *
     * @return
     */
    public boolean isEds() {
        return m_isEds;
    }

    /**
     * Returns the EDS noise data. null if the data is not available or not
     * applicable
     *
     * @return the EDS noise data. null if the data is not available or not
     * applicable
     */
    public OverlayElement getNoiseData() {
        return m_edsNoise;
    }

    /**
     * This method returns the XML version of the specimen. The m_xmlVersion gets populated
     * from the XML "Version" tag
     */
    public double getXMLVersion() {
        return m_xmlVersion;
    }

    public void setXMLVersion(double specimenXmlVersion) {
        m_xmlVersion = specimenXmlVersion;
    }

    /**
     * This method takes in a subTree root and traverses up to get all the
     * parent nodes. The end result would be a sorted ArrayList that has
     * the root of the Specimen Data tree as its first element and the
     * root of the subtree as the last element.
     * This information is useful when drawing the controls in the sidepanel. This
     * would put all the controls in the order they appear in the tree.
     *
     * @param subTree
     * @return ArrayList containing the complete tree that includes the input subtree
     * sorted in the order or traversal.
     */

    public ArrayList getTreeOrder(ControlStates subTree) {
        ArrayList retList = new ArrayList();
        ControlStates state = subTree;
        retList.add(subTree.getId());
        while (state.m_parent != null && state.m_parent.m_id != null) {
            state = state.m_parent;
            retList.add(state.getId());
        }
        // Since we are traversing from bottom up, we shall now invert
        // the arraylist so that it is correctly ordered.
        ArrayList orderedList = new ArrayList(retList.size());
        for (int i = retList.size() - 1; i >= 0; i--) {
            orderedList.add(retList.get(i));
        }
        return orderedList;
    }

    /**
     * Returns the root state for the specimen usually it is the
     * node "DETECTOR". This method is used for the new XML format.
     *
     * @return ControlStates containing the root of the specimen Data tree.
     */

    public ControlStates getRootStates() {
        return m_root.m_states;
    }

    /**
     * This method verifies that the path for the overlay image is valid and that the
     * corresponding .bmp file exists in the specimen jar.
     */
    public void validateOverlayImage(OverlayElement oe) throws SpecimenException {
        JarEntry entry = m_jarFile.getJarEntry(oe.m_mapFilePath);
        if (entry == null) {
            throw new SpecimenException("Requested entry '" + oe.m_mapFilePath + "' does not exist.");
        }
    }

    /**
     * This method returns the requested parameter's value by extracting it
     * from the sampleInfo object. these are the value located under the
     * SampleInfo tag. This method is used for the new XML format.
     *
     * @param key
     * @return
     */
    public String getNamedSampleInfoParam(String key) {
        if (m_xmlVersion > 1.0)
            return (String) m_sampleInfo.m_params.get(key);
        else
            // old XMLformat does not support a separate <SampleInfo> tag.
            return (String) getNamedParam(key);
    }

    /**
     * This method returns the requested parameter's value by extracting it
     * from the InstrumentDescriptor object. These are the value located under the
     * Instrument tag. This method is used for the new XML format.
     *
     * @param key
     * @return
     */
    public String getNamedInstrumetParam(String key) {
        if (m_xmlVersion > 1.0)
            return (String) m_instrumentInfo.m_params.get(key);
        else
            // old XMLformat does not support a separate <InstrumentInfo> tag.
            return (String) getNamedParam(key);
    }

    /**
     * This method returns the Display string for the Specimen. This Display String
     * appears as "LongName" under the "Instrument" tag in the new XML.
     */
    public String getDisplayString() {
        if (m_xmlVersion > 1.0) // New XML
        {
            // This is a new format XML file. The new Format
            // stores the informaton as "LongName"
            return (getNamedSampleInfoParam(Constants.XMLTags.NAME));
        } else {
            return (super.getDisplayString());
        }
    }

    /**
     * This method retreives the value of the key. For new XML this value
     * may be located in SampleInfo or Instrumen tags, hence if it is a new
     * XML then we lookup the value in SampleInfo and InstrumentInfo as well.
     *
     * @param key whose value is to be looked up.
     */
    public String getNamedParam(String key) {
        String value = "";
        if (m_xmlVersion > 1.0) // New XML
        {
            value = super.getNamedParam(key);
            if (value == null) {
                value = getNamedSampleInfoParam(key);
                if (value == null) {
                    value = getNamedInstrumetParam(key);
                }
            }
            // This is a new format XML file. The new Format
            // stores the informaton as "LongName".
        } else {
            value = super.getNamedParam(key);
        }
        return value;
    }

    /**
     * This method gets a default thumbnail for the instrument.
     * If the specimen.xml does not specify a thumbnail for the instrument
     * then we look up default instrument based on the instrument type information
     * provided in the specimen.xml
     *
     * @return
     */
    public String getDefaultInstrumentThumbnail() {
        String strPath = getNamedInstrumetParam(Constants.XMLTags.THUMBNAIL);
        if (strPath == null) {
            String type = getNamedInstrumetParam(Constants.XMLTags.TYPE);
            if (type == null) {
                // this should not happen, since <TYPE/> is a mandatory tag!
                strPath = "";
            } else {
                strPath = Constants.Instruments.getInstrumentThumbnail(type);
                if (strPath == null)
                    // indicates that a type has been specified for which we have
                    // no default thumbnail.
                    strPath = "";
            }
        }
        return strPath;
    }

    /**
     * This method attempts to get the the navigation thumbnail path from the specimen.xml.
     * If it can't get the path (e.g. old specimen.xml formats do not specify the navigationthumbnail
     * information) it returns null.
     *
     * @return the path of the thumbnail.
     */
    public String getNavigationThumbnail() {
        String path = "";
        if (m_xmlVersion > 1.0) {
            path = getNamedSampleInfoParam(Constants.XMLTags.NAVIGATION_THUMBNAIL);
        } else {
            // Old specimens should not have the navigation thumbnails and we shall
            // not come to the else. However keeping this check here to make
            // sure that even if the old specimens are modifed such that only
            // their specimen.xml are changed to include <NAVIGATOR> tag wuthout
            // including the <NavigationThumbnail> tag then we shall still handle
            // this situation gracefully despite the fact that this indicate an error
            // in specimen jar creation.
            path = getNamedSampleInfoParam(Constants.XMLTags.NAVIGATION_THUMBNAIL);
            if (path == null) {
                //Navigation thumb nail not found.
                path = getNamedSampleInfoParam(Constants.XMLTags.THUMBNAIL);
                if (path == null) {
                    // Sample thumbnail not found.
                    path = getDefaultSampleThumbnail();
                }
            }

        }
        return path;
    }

    /**
     * This method gets a default specimen thumbnail. The default thumbnail is the first image
     * located in the first leaf i.e. 0/0/0/.../0_0.jpg. This method should only be called
     * when the specimen.xml does not specify a sample thumbnail image, or if the specified
     * thumbnail image could not be loaded.
     *
     * @return the path of the default fallback thumbnail, or null if some exception getting the
     * default thumbnail.
     */
    public String getDefaultSampleThumbnail() {
        String strPath = null;
        if (m_xmlVersion > 1.0) {
            // New XML format.
            ControlStates states = m_root.m_states;
            while (states.m_states != null &&
                    states.m_states.size() > 0) {
                states = (ControlStates) states.m_states.get("0");
            }
            strPath = states.m_imageDescriptor.m_id + Constants.PATH_SEPARATOR + "0" +
                    Constants.PATH_SEPARATOR + "0_0.jpg";
        } else {
            // Old XML format.
            ControlStates states = m_root.m_states;
            ControlState state = null;
            if (states.m_states != null &&
                    states.m_states.size() > 0) {
                Iterator it = states.m_states.values().iterator();
                state = (ControlState) it.next();
                strPath = state.getPathPart();
                while (state != null) {
                    if (state.m_imageDescriptor != null) {
                        // we have found the root.
                        break;
                    }
                    state = (ControlState) state.m_controlStates.m_states.values().iterator().next();
                    strPath = strPath + Constants.PATH_SEPARATOR + state.getPathPart();
                }
            }
            strPath = strPath + Constants.PATH_SEPARATOR + "0" +
                    Constants.PATH_SEPARATOR + "0_0.jpg";
        }
        return strPath;
    }

    /**
     * This method takes in the descriptor id (e.g. "FOCUS") and returns the
     * corresponding control descriptor
     *
     * @param descriptorId
     * @return
     */
    public ControlDescriptor getControlDescriptor(String descriptorId) {
        return (ControlDescriptor) m_controlDescriptors.get(descriptorId);
    }

    public Iterator getControlDescriptors() {
        return m_controlDescriptors.values().iterator();
    }

    public Iterator getInstrumentDescriptors() {
        return m_instrumentDescriptors.values().iterator();
    }

    /**
     * Returns Specimen's unique name. This is stored as
     * "UniqueName" in old XML format and as SpecimenID in
     * new XML format.
     */
    public String getUniqueName() {
        String uniqueName = "";
        if (m_xmlVersion <= 1.0) {
            uniqueName = getNamedParam("UniqueName");
        } else {
            // This is a new XML revision file which stores
            // the uniqueName as SpecimenId inside SampleInfo Tag.
            uniqueName = getNamedSampleInfoParam(Constants.XMLTags.UNIQUE_NAME);
        }
        return uniqueName;
    }

    /**
     * This methods gets the Jar input stream from which the caller can read in
     * the desired file. The caller should call close() after it is done with
     * the stream if there is a chance that the caller may be working with multiple
     * spcimens at a given time (e.g. this happens when we are loading the thumbnails
     * for ALL the specimens). Else the jar will keep consuming memory and if
     * too many spcimens are present in the specimen directory this may lead to
     * OutOfMemoryError.
     * This error manifests itself in MacOS when more than 23 specimens are present and while
     * loading the thumbnails or refreshing the thumbnails. In such cases  calling
     * close() will make sure that we close one jar file before opening another specimne
     * jar file.
     *
     * @param path
     * @return
     * @throws SpecimenException
     */
    public InputStream getInputStream(String path) throws SpecimenException {
        try {
            if (m_jarFile == null) {
                m_jarFile = new JarFile(m_jarFileInfo);
            }

            JarEntry entry = m_jarFile.getJarEntry(path);

            if (entry == null) {
                throw new SpecimenException("Requested entry '" + path + "' does not exist.");
            }

            InputStream stream = m_jarFile.getInputStream(entry);
            if (stream == null) {
                throw new SpecimenException("Unable to acquire input stream.");
            }
            return stream;
        } catch (IOException e) {
            throw new SpecimenException("Error while attempting to read specimen.", e);
        }
    }

    /**
     * This method relinquishes the jar file. Every caller to getInputStream may call
     * this method after it is done with the inputstream to conserve memory.
     * Otherwise with too many specimens we may risk getting OutOfMemoryError
     * especially on Mac OS. Note that even if the caller has not explicitly called this
     * method to close the jar file, this method still gets called implicitly when
     * the specimen is unloaded.
     */
    public void close() {
        if (m_jarFile != null) {
            try {
                m_jarFile.close();
                m_jarFile = null;
            } catch (IOException e) {
                return;
            }
        }
    }

    /**
     * This method receievs the controls corresponding to each of the GUI controls and
     * it looks at the selected values of the controls and then decides the path of
     * the image folder that should be chosen. Once determined it then sets the image path
     * and returns the populated ImageSet with the path information.
     *
     * @param states
     * @param version
     * @return the Imageset pointing to the folder location of the images to be shown.
     */
    public ImageSet chooseImageSet(Collection states, double version) {
        try {
            if (version > 1.0) {
                Collection arraylist = sortControlStates(states, version);

                // Now see if the node is in the states controls, if it is not then go down to
                // the next node stored in m_states.get("0"), if the match is found then follow the
                // node that matches the selected value.
                String strPath = "";
                ControlStates cs = null;
                Iterator iter = arraylist.iterator();
                while (iter.hasNext()) {
                    cs = (ControlStates) iter.next();
                    strPath += cs.m_folderName + Constants.PATH_SEPARATOR;
                }
                ImageSet iset = (ImageSet) m_imageSets.get(strPath);
                if (iset == null && cs != null) {
                    iset = new ImageSet(cs.m_imageDescriptor, strPath);
                    m_imageSets.put(strPath, iset);
                }
                return iset;
            }
        } catch (Exception ex) {
            return null;
        }

        //// Old XML Code below ////
        if (states == null || states.size() < 1) {
            states = getDefaultControlStates();
        } else {
            states = sortControlStates(states, version);
        }

        String path = m_root.getPathPart();
        Iterator iter = states.iterator();

        while (iter.hasNext()) {
            ControlState cs = (ControlState) iter.next();
            path += cs.getPathPart() + Constants.PATH_SEPARATOR;

            if (cs.m_imageDescriptor != null) {
                ImageSet iset = (ImageSet) m_imageSets.get(path);

                if (iset == null) {
                    iset = new ImageSet(cs.m_imageDescriptor, path);
                    m_imageSets.put(path, iset);
                }
                return iset;
            }
        }

        return null;
    }

    /**
     * This method returns the first Child of each of the "Control" node. The logic
     * behind this is that the default value of each of the control will be set to
     * the first value that appears in the XML. The tree that was constructed while parsing the
     * new format XML was created such that the first child of each tree node was stored as
     * m_states.put("0", firstChild) so now we do m_states.get("0) to get the first child (
     * and the default state).
     *
     * @param version the XML version.
     * @return Collection the ArrayList containing all the first child in the order they appear in the XML.
     * @see loadElementsNewVersion()
     */
    public Collection getDefaultControlStates(double version) {
        if (version <= 1.0) {
            return getDefaultControlStates();
        }

        ArrayList retStates = new ArrayList();
        ControlStates css = m_root.m_states;

        while (css != null) {
            css = (ControlStates) css.m_states.get("0");
            if (css == null)
                break;
            // For the new XML format the first child is always folder number
            // 0 and it is stored against this fodername key, so all we need to do
            // is to pass on the "0" hash value.
            retStates.add(css);
        }
        return retStates;
    }

    /**
     * This method gets the first controlstate tag for each of the
     * controlstates. The idea is that the default state for each of the
     * control will be the first value e.g. the default for the FOCUS control will
     * be focus 1 (or 0 depending on how it is specified in the XML), which corresponds to
     * the first controlstate tage that occurs under the controlstates parent tag.
     *
     * @return
     */
    public Collection getDefaultControlStates() {
        ArrayList retStates = new ArrayList();
        ControlStates css = m_root.m_states;

        while (css != null) {
            ControlState cs = css.getFirstState();
            retStates.add(cs);


            if (cs.m_controlStates == null)
                break;
            css = cs.m_controlStates;
        }

        return retStates;
    }

    /**
     * This method returns all the valid states for a given the control of the
     * given controlstates node. e.g. if it is a FOCUS controlstate then it will return
     * all the valid focus for the FOCUS control state. This information is used to properly
     * draw all the controls e.g. it will answer the question "How many level of magnifications
     * should the user be shown in the Magnification drop down ?".
     * <p>
     * The input that gets passed to this method is the default states of each of the control
     * that is to be shown to the user. The method will then find all the valid states for each of the control
     * whose default state is passed to it in currentStates input parameter.
     *
     * @param currentStates the information pertaining to the default state of each of the control.
     * @return
     */
    public HashMap getValidControlStates(Collection currentStates) {
        HashMap validStates = new HashMap();
        ControlStates css = m_root.m_states;

        while (css != null) {
            validStates.put(css.getControlDescriptor(), css.m_states.values());
            ControlState cs = css.getFirstState(); // set up for failed match
            Iterator iter = currentStates.iterator();

            while (iter.hasNext()) {
                ControlState match = (ControlState) iter.next();

                if (css.m_states.containsValue(match)) {
                    cs = match;
                    break;
                }
            }

            css = (cs != null) ? cs.m_controlStates : null;
        }

        return validStates;
    }

    /**
     * This method returns all the allowed states (possible values a control can take).
     * The input parameter is the list of the control's default states. The method
     * will retrieve all the allowed states for each of the entry in the currentStates collection.
     * <p>
     * This method sees the xml version and if it is an old version it will call the old function
     * else it will perform all the work itself.
     *
     * @param currentStates the information pertaining to the default state of each of the control.
     * @param version       the XML Version.
     * @return the HashMap that contains the valid states for each of the control descriptors.
     */
    public HashMap getValidControlStates(Collection currentStates, double version) {
        if (version <= 1.0)
            return getValidControlStates(currentStates);

        HashMap validStates = new HashMap();
        ControlStates css = m_root.m_states;
        while (css != null) {
            Iterator it = currentStates.iterator();
            boolean matchFound = false;
            while (it.hasNext()) {
                ControlStates defaultValue = (ControlStates) it.next();

                if (css.m_states.containsValue(defaultValue)) {
                    // The m_states match has been found. This means
                    // that all the current values in m_states form the
                    // valid values set for the control pointed to by
                    //defaultValue.
                    validStates.put(defaultValue.m_controlDescriptor, css.m_states.values());
                    matchFound = true;
                    css = defaultValue;
                    break;
                }
            }
            if (!matchFound) {
                // no point in carrying on since the hierarchy has been broken.
                // TODO: Test when would this condition occurr.
                break;
            }
        }
        return validStates;
    }

    private void load(InputStream specimenDefinition) throws SpecimenException {
        boolean validate = false;
        ParseErrorHandler errorHandler = null;

        try {
            javax.xml.parsers.DocumentBuilderFactory f = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            f.setNamespaceAware(true);
            f.setValidating(false); // TODO: true

            try {
                f.setAttribute(Constants.JAXP_SCHEMA_LANGUAGE, Constants.W3C_XML_SCHEMA);
                // TODO: validate = true;
            } catch (IllegalArgumentException e) {
                f.setNamespaceAware(false);
                f.setValidating(false);
            }

            javax.xml.parsers.DocumentBuilder b = f.newDocumentBuilder();

            if (validate) { // If validating - set an error handler
                errorHandler = new ParseErrorHandler();
                b.setErrorHandler(errorHandler);
            }
            Document doc = b.parse(specimenDefinition);

            loadVersionedElements(doc);
        } catch (SAXParseException se) {
            throw new SpecimenException("Error validating specimen XML file.", se);
        } catch (javax.xml.parsers.ParserConfigurationException e) {
            throw new SpecimenException("Error creating XML parser.", e);
        } catch (org.xml.sax.SAXException e) {
            throw new SpecimenException("Error parsing specimen XML file.", e);
        } catch (IOException e) {
            throw new SpecimenException("IO Error while parsing specimen XML file.", e);
        }
    }

    /**
     * This method checks the version of the XML and decides which XML loading/parsing method to call.
     * If it could not find a XMLrevision tag in the XML it assumes a version=1 and proceedes accordingly.
     *
     * @param doc
     * @throws SpecimenException
     */
    private void loadVersionedElements(Document doc) throws SpecimenException {
        // Gather and store all the specimen metadata
        NodeList nl = doc.getElementsByTagName(Constants.XMLTags.SPECIMEN);

        if (nl.getLength() != 1) {
            throw new SpecimenException("Specimen definition file contains more than one Specimen element.");
        }
        Node n = nl.item(0);
        if (n != null && n.hasAttributes()) {
            if (n.getAttributes().getNamedItem(Constants.VERSION_TAG) != null) {
                String vrs = n.getAttributes().getNamedItem(Constants.VERSION_TAG).getNodeValue();
                try {
                    m_xmlVersion = Double.parseDouble(vrs);
                } catch (Exception ex) {
                    // All specimen.xml version 1.1. and above must have a version tag.
                    m_xmlVersion = 1.0;
                }
            }
        }
        if (m_xmlVersion > 1.0) {
            // This is a new XML format.
            loadElementsNewFormat(doc, nl);
        } else {
            // old XML format.
            loadElements(doc, nl);
        }
    }

    /**
     * This method starts the loading process. It frst retrieves the main root tag "Specimen".
     * Then it gets all its children and goes into the loop to traverse and load each of the child.
     * Some child elements are for informational purpose e.g. Display string, Author,
     * image collection date etc. Others are more serious tags which themselves have children
     * and they encapsulates the image data location, the controls and the image tiles sizes etc.
     * <p>
     * This method handles the loading for XML revisions <=1.0. For XMLrevisions greater than 1.0 the
     * method loadElementsNewFormat() is called.
     *
     * @param doc
     * @throws SpecimenException
     * @see virtuallab.Specimen.loadElementsNewFormat
     */
    private void loadElements(Document doc, NodeList nl) throws SpecimenException {
        // Load the parent <Specimen> tag.
        loadElement(nl.item(0));

        // Gather and store all independent elements.
        nl = doc.getElementsByTagName("ImageDescriptor");

        for (int i = 0; i < nl.getLength(); i++) {

            Node n = nl.item(i);

            if (!isReference(n)) {
                ImageDescriptor desc = new ImageDescriptor(n);
                // The m_imageDescriptors hashmap contains all the
                // Image descriptors as (uniquImageDescID, ImageDescriptorObject)
                // (name,value) pairs. Each unique imagedescriptor tag will end up getting
                // loaded in the following hashmap.
                m_imageDescriptors.put(desc.getId(), desc);
            }
        }

        nl = doc.getElementsByTagName("InstrumentDescriptor");

        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);

            if (!isReference(n)) {
                InstrumentDescriptor desc = new InstrumentDescriptor(n);
                m_instrumentDescriptors.put(desc.getId(), desc);

                // add instrument controls
            }
        }

        nl = doc.getElementsByTagName("ControlDescriptor");

        for (int i = 0; i < nl.getLength(); i++) {
            Node n = nl.item(i);

            if (!isReference(n)) {
                ControlDescriptor desc = new ControlDescriptor(n);
                m_controlDescriptors.put(desc.getId(), desc);
            }
        }

        nl = doc.getElementsByTagName("ControlState");

        // TODO: Does this loop ever do anything useful ? I did
        // not see any <controlstate> tags with an "id" attribute in the specimen.xml, i could never
        // see the if condition getting executed. Need to see with other colored samples if this code ever
        // gets executed.
        for (int i = 0; i < nl.getLength(); i++) {
            // Create only those control states that are not references and that have an element id.
            // Those without an element id are created during the enclosing controlStates creation.
            Node n = nl.item(i);

            if (!isReference(n) && hasElementId(n)) {
                ControlState desc = new ControlState(n);
                m_controlStateDescriptors.put(desc.getId(), desc);
            }
        }

        nl = doc.getElementsByTagName("ControlStates");

        for (int i = 0; i < nl.getLength(); i++) {

            Node n = nl.item(i);
            if (!isReference(n)) {
                ControlStates desc = new ControlStates(n);
                m_controlStates.put(desc.getId(), desc);
            }
        }

        nl = doc.getElementsByTagName("Root");
        m_root = new Root(nl.item(0));

        // Resolve all control-state references to image descriptors and controlStates.
        Iterator iter = m_controlStateDescriptors.values().iterator();

        // In this while loop we link all the imagedescriptors tags appearing elsewhere in the
        // specimen.xml to the original occurrence of the respective imagedescriptor tags.
        // What this allows us to do is to be able to get to access the ImageDescriptor> from
        // with in the encompassing ControlState. For example if the controlstate is MAGNIFICATION then
        // we would have created a separate Specimen Object for it.This object links to an Image Descriptor
        // so we will establish this link in the while loop given below.
        while (iter.hasNext()) {
            ((ControlState) iter.next()).resolveReferences();
        }
    }

    /**
     * This method forms a tree from the XML "Control" nodes. The "Control" nodes form a hierarchy and this
     * method traverses through this hierarchy and then populates it as a tree. Each node is of type
     * ControlStates. This tree would not be a balanced tree - since the new XML format is very flexible
     * and allows for any arbitrary hierarchy of "Control" nodes. This method, in addition to creating the Tree,
     * also stuffs in some of the read in data into some general purpose datastructures e.g. the image
     * descriptors, the control descriptors.
     * The method uses recursion to create the tree. The leaf of the trees are identified by the fact that
     * these will contain ImageDescriptors objects. For non-leaf nodes the imagedescriptors are null.
     *
     * @param parent               The ControStates Object that is the immediate parent of the node that will be processed in this
     *                             particular execution of the processChild() method. This is needed so that a lead node can set itself inside the
     *                             appropriate parent variable, and non-leaf nodes can use a back pointer to their parents
     * @param controlDescriptorMap LinkedHashMap that will get populated with the controlDescriptors.
     * @param imageDescriptorMap   The LinkedHashMap that will get populated with the Leaf Nodes
     *                             of the "Control" tags hierahrchy. The leaf contains information on the tile size, image size and the pixel Size.
     * @param uniquePath           The old XML Format required each ImageDescriptor tag to have unique id. That is how
     *                             the parent node was linked to the child node, since the format was essentially flat and that was the only
     *                             way to establish some sort of the hierarchy. In the new XML Format the hierarchy is naturally embedded
     *                             into the XML as "Control" nodes. Therefore there is not need for a unique ID. But to be backward compatible we
     *                             still need to use the existing datastructures to hold our loaded data;therefore a unique id needs to be assigned
     *                             to each image node (which is the leaf "Control" node in the new XML Format). As we traverse through the tree of "Control"
     *                             we shall keep appending the "FolderName" value of each node. This way when we hit the child node we can use
     *                             this concatenated FolderNames as the unique id for the leaf node.
     * @param treeDepth            contains the depth of the parent node.
     */

    private void processChild(Node parent, LinkedHashMap imageDescriptorMap, ControlStates parentLevel, String uniquePath, short treeDepth) throws SpecimenException {
        String strPath = "";
        ControlStates levelNode = new ControlStates();
        if (parent.hasChildNodes()) {
            NodeList nl = parent.getChildNodes();
            treeDepth++;
            for (int i = 0; i < nl.getLength(); i++) {
                Node n = nl.item(i);
                if (n.getNodeType() != Node.ELEMENT_NODE) {
                    // We are only interested in ELEMENT nodes, other
                    // nodes e.g. comments, white spaces are not
                    // relevant to our processing.
                    continue;
                }

                if (isImageLeafNode(n)) {
                    // *********** populating the leaf node in the tree ********************
                    // We shall save the leaf node and then set the image descriptor in it.
                    levelNode = new ControlStates();
                    levelNode.m_parent = parentLevel;
                    levelNode.m_depth = treeDepth;
                    if (n.hasAttributes()) {
                        // First we read in all the attributes of this "Control" node.
                        NamedNodeMap m = n.getAttributes();
                        //System.out.println("**************** Attributes of " + n.getNodeName() + " *************");
                        for (int j = 0; j < m.getLength(); j++) {
                            // Get all the attributes of the tag
                            Node a = m.item(j);
                            String str = a.getNodeName();
                            String v = a.getNodeValue();
                            // System.out.println("Attribute : " + str + "=" + v);
                            if (str.equals(Constants.XMLTags.FOLDER_NAME)) {
                                // Concatenating the foldername with the parent folder name, and then
                                // passing on to the child. This will guarantee that each leaf node gets a
                                // truly unique ID that it can use as its identifier.
                                levelNode.m_folderName = v;
                                v = uniquePath + v;
                            }
                            levelNode.m_params.put(str, v);
                        }
                        levelNode.m_id = levelNode.getNamedParam(Constants.XMLTags.ID);
                        //System.out.println("****************************************************");
                    }
                    if (levelNode.m_folderName != null)
                        parentLevel.m_states.put(levelNode.m_folderName, levelNode);
                    // ***********************************************************

                    // ***** Setting the image description in the leaf node ***********
                    ImageDescriptor idesc = new ImageDescriptor(n, m_xmlVersion, uniquePath);
                    // After having processed the child elements child elements,
                    // that contain the tile size, image size and pixel dimensions
                    // we shall process the Attributes of the tag.

                    if (n.hasAttributes()) {
                        NamedNodeMap m = n.getAttributes();
                        //System.out.println("------- Image Node Attributes " + n.getNodeName() + " -------");
                        for (int j = 0; j < m.getLength(); j++) {
                            // Get all the attributes of the tag
                            Node a = m.item(j);
                            String str = a.getNodeName();
                            String v = a.getNodeValue();
                            //System.out.println("Attribute : " + str + "=" + v);
                            if (str.equals(Constants.XMLTags.FOLDER_NAME)) {
                                idesc.m_id = uniquePath + v;
                            }
                            idesc.m_params.put(str, v);
                        }
                        //System.out.println("-----------------------------------------------------");
                    }
                    // Setting the image descriptors to the utility hashmap. This is a convenient place
                    // to bunch up all the leaf nodes together.
                    imageDescriptorMap.put(idesc.getId(), idesc);
                    // The image descriptor will only be set in the leaf "Control" nodes. For all the
                    // proceding "Control" nodes this value will be null.
                    levelNode.m_imageDescriptor = idesc;
                    //**************************************************************/
                } else if (n.hasChildNodes() && n.getNodeName().equals(Constants.XMLTags.CONTROL)) { // Non-Leaf <Control> node
                    //System.out.println(n.getNodeName());
                    // This node is a <Control> node. For each <Control> node we shall create a
                    // ControlStates Object. This object will store all the <Control> node attributes
                    // in its m_param hashmap, and will also hold link to its immediate child <Control> node, by
                    // storing the child controlstates object in m_children hashmap. The key of the children
                    // hashmap will be the value of the FolderName attribute of the child "Control" node.
                    levelNode = new ControlStates();
                    levelNode.m_parent = parentLevel;
                    levelNode.m_depth = treeDepth;
                    if (n.hasAttributes()) {
                        // First we read in all the attributes of this "Control" node.
                        NamedNodeMap m = n.getAttributes();
                        //System.out.println("**************** Attributes of " + n.getNodeName() + " *************");
                        for (int j = 0; j < m.getLength(); j++) {
                            // Get all the attributes of the tag
                            Node a = m.item(j);
                            String str = a.getNodeName();
                            String v = a.getNodeValue();
                            //System.out.println("Attribute : " + str + "=" + v);
                            if (str.equals(Constants.XMLTags.FOLDER_NAME)) {
                                // Concatenating the foldername with the parent folder name, and then
                                // passing on to the child. This will guarantee that each leaf node gets a
                                // truly unique ID that it can use as its identifier.
                                //uniquePath = uniquePath + v + Constants.PATH_SEPARATOR;
                                strPath = uniquePath + v + Constants.PATH_SEPARATOR;
                                levelNode.m_folderName = v;
                            }
                            levelNode.m_params.put(str, v);
                        }
                        levelNode.m_id = levelNode.getNamedParam(Constants.XMLTags.ID);
                        //System.out.println("****************************************************");
                    }
                    if (levelNode.m_folderName != null)
                        parentLevel.m_states.put(levelNode.m_folderName, levelNode);
                    // If this was the child node (other than the magnification node) we then recurse to get
                    // all its children.
                    processChild(n, imageDescriptorMap, levelNode, strPath, treeDepth);
                } else {
                    /**
                     * This will be reached when we come across a child tag that is not a <Control> tag.
                     * The EDS related <Element> tag is one such example. Reaching this 'else'
                     * indicates that the parent <Control> tag has some specific parameters
                     * associated with it which will be required in some specific processing of
                     * this <Control>
                     */
                    //System.out.println("Specific Parameters found for this tag.");
                    if (n.hasAttributes()) {
                        if (n.getNodeName().equals(Constants.XMLTags.ELEMENT)) {
                            /**
                             * It is an <ELEMENT> tag. This tag needs to be handled differently.
                             * This tag will appear under tags that incorporate the Image Map overlay
                             * type of functionality. As of now the "EDS" is one such tag. In future we
                             * may have others. The <ELEMENT> tags appearing as a child of the EDS <Control id="EDS" ...>
                             * tag indicate various maps that can be overlaid ontop of the base image.
                             * For each map that can be overlaid we will have a corresponding <ELEMENT>
                             * child tag.
                             * Unfortunately due to the specificity of the EDS overlay requirement there
                             * is not elegant and generic way to handle such cases. We shall trick the system
                             * by inserting these elements into the m_states map. This way the rest of the
                             * loading and control rendering logic will primarily remain unchanged.
                             */
                            OverlayElement oe = new OverlayElement();
                            NamedNodeMap m = n.getAttributes();
                            for (int j = 0; j < m.getLength(); j++) {
                                // Get all the attributes of the tag
                                Node a = m.item(j);
                                // Set the control Descriptor ID, since this is how it will be uniquely
                                // identified from the rest of the Controls.
                                if (a.getNodeName().equalsIgnoreCase(Constants.XMLTags.ID)) {
                                    oe.m_id = a.getNodeValue();
                                } else if (a.getNodeName().equalsIgnoreCase(Constants.XMLTags.PATH)) {
                                    oe.m_mapFilePath = a.getNodeValue();
                                } else if (a.getNodeName().equalsIgnoreCase(Constants.XMLTags.PXSIZE)) {
                                    oe.m_pixelSize = Float.parseFloat(a.getNodeValue());
                                } else if (a.getNodeName().equalsIgnoreCase(Constants.XMLTags.SCALE)) {
                                    oe.m_scale = Float.parseFloat(a.getNodeValue());
                                }
                            }
                            m_isEds = true;
                            //loadElementGraphdata(oe);
                            validateOverlayImage(oe);
                            parentLevel.m_overlayElementMap.put(oe.m_id, oe);

                        } else if (n.getNodeName().equals(Constants.XMLTags.BREMSSTRAHLUNG)) {
                            /**
                             * The noise information is enclosed in this tag.
                             */
                            OverlayElement oe = new OverlayElement();
                            NamedNodeMap m = n.getAttributes();
                            for (int j = 0; j < m.getLength(); j++) {
                                // Instead of creating a new data structure to hold the noise data, we
                                // shall use the OverlayElement object to represent the noise
                                // information. We shall piggybacki the noise information on the
                                // OverlayElement object's data variables as appropriate.
                                Node a = m.item(j);
                                oe.m_id = Constants.NOISE_DATA_FILE_NAME;
                                //oe.setDefaultHSValues();
                                oe.m_longName = Constants.NOISE_DATA_FILE_NAME;
                                if (a.getNodeName().equalsIgnoreCase(Constants.XMLTags.SCALE)) {
                                    oe.m_scale = Float.parseFloat(a.getNodeValue());
                                }
                            }

                            m_edsNoise = oe;

                        } else {
                            ControlDescriptor desc = new ControlDescriptor();
                            NamedNodeMap m = n.getAttributes();
                            //System.out.println("//////////////////////////////////////////////////////");
                            for (int j = 0; j < m.getLength(); j++) {
                                // Get all the attributes of the tag
                                Node a = m.item(j);

                                // Set the control Descriptor ID, since this is how it will be uniquely
                                // identified from the rest of the Controls.
                                if (a.getNodeName().equals(Constants.XMLTags.ID)) {
                                    desc.m_id = a.getNodeValue();
                                } else {
                                    desc.m_params.put(a.getNodeName(), a.getNodeValue());
                                }
                            }
                            // Finally add the new Control to the HashMap.
//						   controlDescriptorMap.put(desc.getId(), desc);
                        }
                    } // non-leaf non <Control> node.
                }
            }// for

            /**
             * If this is the EDS node and it has atleast one DETECTOR child then we need to add
             * another child named "None". This artifically injecting the "None" as a child will
             * allow us to Incorporate the "BaseImage None" functionality which is required for EDS module.
             */
            if (levelNode.m_id.equalsIgnoreCase(Constants.Controls.EDS) && levelNode.m_states != null &&
                    levelNode.m_states.size() > 0) {
                ControlStates sibling = (ControlStates) levelNode.m_states.get("0");
                if (sibling.m_id.equals(Constants.Controls.DETECTOR)) {
                    // Found a DETECTOR child, let the "None" game begin !
                    ControlStates noneNode = new ControlStates();
                    noneNode.setNamedParam(Constants.XMLTags.DISPLAY_STRING, Constants.Controls.NONE_DISPLAY_STRING);
                    noneNode.m_parent = sibling.m_parent;
                    noneNode.m_depth = sibling.m_depth;
                    // This will indicate that the "None" node will not be able to load any image.
                    // Since there would be no folder named "-1" in the folder hierarchy.
                    noneNode.m_folderName = "-1";
                    noneNode.m_id = sibling.m_id;
                    levelNode.m_states.put(noneNode.m_folderName, noneNode);
                }
            }
        }// if hasChildNodes()
    }

    /**
     * This method starts the loading process. It frst retrieves the main root tag "Specimen".
     * Then it gets all its children and goes into the loop to traverse and load each of the child.
     * Some child elements are for informational purpose e.g. Display string, Author,
     * image collection date etc. Others are more serious tags which themselves have children
     * and they encapsulates the image data location, the controls and the image tiles sizes etc.
     * This method handles the loading for XML revisions >1.0. For XMLrevisions <= 1.0 the
     * method loadElements() is called.
     *
     * @param doc
     * @param nl  The NodeList pointing to the "specimen" tag's children.
     * @throws SpecimenException
     * @see virtuallab.Specimen.loadElements
     */
    private void loadElementsNewFormat(Document doc, NodeList nl) throws SpecimenException {
        // To reset the default color pointer for the EDS tools
        Constants.OverlayElementColor.m_pointer = 0;

        // TODO: Figure out if we really need the following call to loadElement().
        // Load the parent <Specimen> tag.
        loadElement(nl.item(0));
        // ******* Load SampleInfo Parameters **********
        nl = doc.getElementsByTagName(Constants.XMLTags.SAMPLE_INFO);
        if (nl.getLength() != 1) {
            throw new SpecimenException("Invalid specimen configuration file: The XML Revision " + m_xmlVersion + " allows exactly one " +
                    Constants.XMLTags.SAMPLE_INFO + " tag to be present in the specimen XML file. The specimen can not be loaded");
        }
        m_sampleInfo = new Information(nl.item(0));

        //**** Load the Instrument information from the XML *******
        nl = doc.getElementsByTagName(Constants.XMLTags.INSTRUMENT);
        if (nl.getLength() != 1) {
            throw new SpecimenException("Invalid specimen configuration file: The XML Revision " + m_xmlVersion + " allows exactly one " +
                    Constants.XMLTags.INSTRUMENT + " tag to be present in the specimen XML file. The specimen can not be loaded");
        }
        m_instrumentInfo = new Information(nl.item(0));
        // *****************************************************//

        // Putting mandatory controls like navigation thumbnail,brightness, contrast.
        if (getNavigationThumbnail() != null) {
            // Navigation thumbnail will only show up if there is a valid thumbnail tag.
            ControlDescriptor c0 = new ControlDescriptor();
            c0.m_id = Constants.Controls.NAVIGATOR;
            c0.m_params.put(Constants.XMLTags.DISPLAY_STRING, Constants.Controls.NAVIGATOR_DISPLAY_STRING);
            m_controlDescriptors.put(Constants.Controls.NAVIGATOR, c0);
        }

        ControlDescriptor c1 = new ControlDescriptor();
        c1.m_id = Constants.Controls.BRIGHTNESS;
        c1.m_params.put(Constants.XMLTags.DISPLAY_STRING, Constants.Controls.BRIGHTNESS_DISPLAY_STRING);
        m_controlDescriptors.put(Constants.Controls.BRIGHTNESS, c1);

        ControlDescriptor c2 = new ControlDescriptor();
        c2.m_id = Constants.Controls.CONTRAST;
        c2.m_params.put(Constants.XMLTags.DISPLAY_STRING, Constants.Controls.CONTRAST_DISPLAY_STRING);
        m_controlDescriptors.put(Constants.Controls.CONTRAST, c2);

        // Now we gather the parameters that will help us load the images and
        // various control descriptors e.g. focus slider, magnification dropdown etc.

        // The root node for the imaging information tags is titled "ControlDescriptors"
        // we shall get this node and will then dive into it to get the remaining
        // children and eventually the leaves.
        nl = doc.getElementsByTagName(Constants.XMLTags.CONTROL_DESCRIPTORS);
        // there should only be one ControlDescriptor element.
        if (nl.getLength() != 1) {
            throw new SpecimenException("Malformed XML!.There should be exactly one <ControlDescriptor> in the XML.");
        }
        Node nn = nl.item(0);
        // Now we dive into the <ControlDescriptors> to get all its children nodes, during which
        // we will have the appropriate HashMaps populated with the parsed data. Note that this logic
        // is different from the old XML loading logic used in the loadElements(Node) function, which
        // loads the Controls and ImageDescriptors during separate passes.
        short level = 0;
        ControlStates rootState = new ControlStates();
        processChild(nn, m_imageDescriptors, rootState, "", level);
        m_root = new Root(rootState);

        // the new XML does not explicitly specify the controls, therefore we shall
        // extract this information from the nodes hierarchy.
        ControlStates cs = m_root.m_states;
        int iSize = cs.m_states.size();
        for (int i = 0; i < iSize; i++) {
            ControlStates csss = (ControlStates) cs.m_states.get(Integer.toString(i));
            figureOutApplicableControls(csss);
        }
        m_isAfm = containsControl(Constants.Controls.AFM);
        // Putting mandatory Annotation control at the very end.
        ControlDescriptor c3 = new ControlDescriptor();
        c3.m_id = Constants.Controls.ANNOTATION;
        c3.m_params.put(Constants.XMLTags.DISPLAY_STRING, Constants.Controls.ANNOTATION_DISPLAY_STRING);
        m_controlDescriptors.put(Constants.Controls.ANNOTATION, c3);
        // ***************************************************************
    }

    /**
     * Returns true is the given control is contained/applicable for this
     * specimen,false otherwise. This method is useful in some particular
     * cases e.g. when deciding whether to show the HeightMeasure control
     * (which is only applicable to AFM specimens).
     *
     * @param controlName
     * @return true is the given control is contained/applicable for this
     * specimen,false otherwise.
     */
    public boolean containsControl(String controlName) {
        return (m_controlDescriptors.get(controlName) != null);
    }

    /**
     * This method traverses through the Tree and sets the control descriptor
     * of each node. e.g all the "Control" nodes that have id=FOCUS will have their control
     * descriptors set to point to the FOCUS control. It also adds these control descriptors to
     * the m_controlDescriptors HashMap - which acts as a placeholder for ALL the controls that are
     * applicable to the loaded specimen.
     *
     * @param parent - the parent control state/Node.
     */
    private void figureOutApplicableControls(ControlStates parent) throws SpecimenException {
        // ************* Process the parent ***********************/
        // Set the control descriptor for the parent.
	  /*if (parent.m_overlayElementMap != null && parent.m_overlayElementMap.size()!=0){
		  Iterator it = parent.m_overlayElementMap.keySet().iterator();
		  //System.out.println(parent.m_id);
		  while (it.hasNext()){
			  String key = (String) it.next();
			  System.out.println("[" + key + " = " + ((OverlayElement)parent.m_overlayElementMap.get(key)).m_name + "]");
		  }
	  }*/
        parent.m_controlDescriptor = (ControlDescriptor) m_controlDescriptors.get(parent.getId());
        if (parent.m_controlDescriptor == null) {
            // The first occurence of this particular control.
            ControlDescriptor cntrl = new ControlDescriptor();
            cntrl.m_id = parent.getId();
            // The display/label of the control will be its Title Case ID
            String label = "" + parent.getId().charAt(0);
            label = label.toUpperCase();
            label += parent.getId().substring(1).toLowerCase();
            cntrl.m_params.put(Constants.XMLTags.DISPLAY_STRING, label);
            cntrl.setPath(parent.m_folderName);
            parent.m_controlDescriptor = cntrl;
            m_controlDescriptors.put(cntrl.m_id, cntrl);
        }
        //************ Process the immediate children ******************************/
        Iterator it = parent.m_states.keySet().iterator();
        while (it.hasNext()) {
            String key = (String) it.next();
            ControlStates c = (ControlStates) parent.m_states.get(key);
            if (m_controlDescriptors.get(c.getId()) == null) {
                // Set the control descriptor of this child.
                ControlDescriptor cntrl = new ControlDescriptor();
                cntrl.m_id = c.getId();
                cntrl.setPath(c.m_folderName);
                c.m_controlDescriptor = cntrl;
            } else {
                // a control descriptor corresponding to this child already exists, we shall resue it.
                c.m_controlDescriptor = (ControlDescriptor) m_controlDescriptors.get(c.getId());
            }
            if (c == null)
                continue;
            // *************** Process the grandchildren recursively. *******************/
            figureOutApplicableControls(c);
        }
    }

    /**
     * This method sees if the node is a generic informational node (e.g. Author, Copyright etc.)
     * - in which case it returns false, indicating that this is a general info purpose
     * node and does not need any specific handling.
     *
     * @return true if the node is specific node or false if it is a generic info purpose node.
     */
    protected boolean gutsOfLoadElement(Node child) {
        return isHandledIndividually(child.getNodeName());
    }

    /**
     * This method returns the appropriate ControlStates (the "Control" nodes) in the order. Basically
     * this method traverses the tree starting at the root and puts all the
     * visited nodes along the required path (which is determined by the selected
     * values of the controls) and puts these nodes in order of traversal
     * into the ArrayList and returns it back.
     * <p>
     * The return Array List will have the root node (depth = 1) as its first element, the
     * immediate child of the root (depth=2) as its second element and so on. The last element
     * will be the leaf node (the MAGNIFICATION). The calling function can then read the arraylist
     * in order and concatenate the path part (the folderName) of each to get the final image path.
     *
     * @param states
     * @param version the XML Version
     * @return
     */
    private Collection sortControlStates(Collection states, double version) {
        if (version <= 1.0)
            return sortControlStates(states);

        if (states == null) {
            // This should not happen. Even it does the new sorting logic handles this
            // gracefully.
            //throw new IllegalArgumentException("No Control Status passed to sortControlStates().");

            states = new ArrayList();
        }

        ArrayList sorted = new ArrayList();
        //TODO: May be this loop and the one that follows can be compressed into a single loop.

        // Start with the node next to the root. The root node is just a container
        // that may contain several "Level" node (usually for the DETECTOR control.
        // We need some point to strat traversing from, so we take the first child.
        ControlStates node = (ControlStates) m_root.m_states.m_states.get("0");
        Iterator iter = states.iterator();
        // now we shall see if our choice was correct. If there is a control with the
        //same id then our node then we should go to the path that is determined
        // by the control's selected value. e.g. if we started off with the
        // first node titled "DETECTOR" and there was a GUI control DETECTOR
        // then we should start with the node that corresponds to the selected value
        // of the DETECTOR node.
        while (iter.hasNext()) {
            ControlStates cs = (ControlStates) iter.next();
            if (cs != null && cs.m_id.equals(node.m_id) && !node.m_folderName.equals(cs.m_folderName)) {
                node = (ControlStates) m_root.m_states.m_states.get(cs.m_folderName);
                break;
            }
        }
        sorted.add(node);

        // Having determined our starting point we then follow more or less similar logic to traverse
        // the rest of the tree.
        while (node != null) {
            iter = states.iterator();
            boolean found = false;
            while (iter.hasNext()) {
                ControlStates cs = (ControlStates) iter.next();
                // We shall see if the control corresponds to one of the child nodes
                // if yes then we shall follow the path of the selected value.
                if (cs != null && node.m_imageDescriptor == null && cs.m_id.equals(((ControlStates) node.m_states.get("0")).m_id)) {
                    node = (ControlStates) node.m_states.get(cs.m_folderName);
                    sorted.add(node);
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (node.m_imageDescriptor == null) {
                    // this will happen when a controll is not shown on the GUI but it does occurr in
                    // the directory structure i.e. it is in the "Control" nodes in XML but has no peer.
                    // In this case we will have only one peer and hence one path to traverse.
                    //System.out.println("Not found " + node.m_id + " " + node.m_folderName);
                    node = (ControlStates) node.m_states.get("0");
                    sorted.add(node);
                } else {
                    // we have parsed all the nodes and have reached the leaf.
                    break;
                }
            }
        }
        return sorted;
    }

    /**
     * This method forms the hierarchy between the controlstate. It takes in all the
     * selected controlstate objects (based on the selected values of the controls)
     * and it then arranges them in oreder of hierarchy. Later on this order
     * will be used to generate image paths to upload the appropriate image.
     *
     * @param states
     * @return
     */
    private Collection sortControlStates(Collection states) {
        // Return a list sorted in depth-first order of the input states.
        if (states == null) {
            throw new IllegalArgumentException("Null passed to sortControlStates()");
        }

        ArrayList sorted = new ArrayList(states.size());
        ControlStates css = m_root.m_states;

        for (int i = 0; i < states.size() && css != null; i++) {
            Iterator iter = states.iterator();

            while (iter.hasNext()) {
                ControlState cs = (ControlState) iter.next();

                if (cs != null && css.m_states.containsValue(cs)) {
                    sorted.add(cs);
                    css = cs.m_controlStates;
                    break;
                }
            }
        }

        return sorted;
    }

    /**
     * The getter for the sample info.
     *
     * @return
     */
    public Information getSampleInfo() {
        return m_sampleInfo;
    }

    /**
     * This method initializes the sample information under conditions when this
     * is not loaded from the specimen.xml file e.g. when creating specimen objects out
     * of the update RSS feed.
     *
     * @see virtuallab.update.UpdateRetriever
     */
    public void initializeSampleInfo() {
        m_sampleInfo = new Information();
    }

    public void initializeInstrumentInfo() {
        m_instrumentInfo = new Information();
    }

    /**
     * The getter for the Instrument info.
     *
     * @return
     */
    public Information getInstrumentInfo() {
        return m_instrumentInfo;
    }

    /**
     * This class holds information pertaining to any tag.
     * This will be used when we have tags like "Instrument" and
     * "SampleInfo", those that do not require any specific proessing
     * but are there just to group related information together.
     */
    public class Information extends SpecimenElement {
        Information(Node n) {
            loadInfo(n);
        }

        /**
         * Default constructor used to initialize this object under conditions when this
         * is not loaded from the specimen.xml file e.g. when creating specimen objects out
         * of the update RSS feed.
         *
         * @see virtuallab.update.UpdateRetriever
         */
        Information() {

        }

        /**
         * This method formats all the inforefs as a string. Returns an empty
         * Collection if there is no info ref.
         *
         * @return inforefs formatted as a collection of strings. The key of the collection are the inforref
         * textual value and the value is the href URL. Note that the URL may be null if there is no
         * URL specified in the specimen file.
         */
        public LinkedHashMap getInfoRefStrings() {
            LinkedHashMap retValue = new LinkedHashMap();
            for (int i = 0; i < m_infoRefParams.size(); i++) {
                String text = ((InfoRef) m_infoRefParams.get(i)).m_text;
                String href = ((InfoRef) m_infoRefParams.get(i)).m_href;
                retValue.put(text, href);
            }
            return retValue;
        }

        /**
         * This method loads the SampleInfo tag.
         *
         * @param n Node poiting to the SampleInfo tag in the XML
         */
        private void loadInfo(Node n) {
            try {
                // The node should be pointing to the Specimen Collection tag.
                if (n.hasChildNodes()) {
                    NodeList nl = n.getChildNodes();
                    for (int i = 0; i < nl.getLength(); i++) {
                        Node nn = nl.item(i);
                        String name = nn.getNodeName();

                        String value = "";

                        if (nn.getNodeType() == Node.ELEMENT_NODE) {
                            if (name.equals(Constants.XMLTags.INFO_REF)) {
                                m_infoRefParams.add(new Specimen.InfoRef(nn));
                            } else {
                                if (nn.hasAttributes()) {
                                    for (int attribCount = 0; attribCount < nn.getAttributes().getLength(); attribCount++) {
                                        Node nnn = nn.getAttributes().item(attribCount);
                                        // we shall only process the nodes of the type
                                        // <ABA>value</ABC>
                                        String attributeName = nnn.getNodeName();
                                        value = nnn.getNodeValue();
                                        //System.out.println("Putting (" + name + ", " + value + ")");
                                        // The first textual input into the tag is the
                                        // one we are going to consider.
                                        m_params.put(name + "." + attributeName, value);
                                    }
                                }
                                for (int j = 0; j < nn.getChildNodes().getLength(); j++) {
                                    Node nnn = nn.getChildNodes().item(j);
                                    if (nnn.getNodeType() == Node.TEXT_NODE) {
                                        // we shall only process the nodes of the type
                                        // <ABA>value</ABC>
                                        value = nnn.getNodeValue();
                                        //System.out.println("Putting (" + name + ", " + value + ")");
                                        // The first textual input into the tag is the
                                        // one we are going to consider.
                                        m_params.put(name, value);
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                log.error("Failed to retreive specimen info!");
                log.error(ex.getMessage());
            }
        }
    }

    /**
     * Encapsulates ImageDescriptor element.
     * This class contains the hashmap m_params in which all the
     * Image related information gets loaded
     */
    public class ImageDescriptor extends SpecimenElement {
        private Dimension m_tileSize;
        //    ImageDescriptor()
//    {
//    	m_pixelSize = new Point2D.Double();
//    	m_imageAspectRatio = (double)(m_imageSize.getWidth()/m_imageSize.getHeight());
//    }
        private Dimension m_imageSize;
        /**
         * The value that comunicates the scaling factor for the height value.
         * This only applies to AFM. AFM specimens convey the specimen height
         * information as brightness of a pixel. The brightness value range
         * from [0-1] these need to be converted into the appropriate scale.
         * This field provide us with the required multiplying factor.
         */
        private Double m_pixelHeightRange;
        /**
         * communicates how much the width and height of a pixel represents i.e.
         * a one pixel width/height is equal to how much width/height of the
         * specimen.
         */
        private Point2D.Double m_pixelSize;

        ImageDescriptor(Node n) {
            loadElement(n);
            if (m_firstReadImageSize == null) {
                //make sure we only store the image dimensions of the lowest mag image, which will be
                // the first one that we read out of the specimen.
                m_firstReadImageSize = new Point2D.Double(m_imageSize.getWidth(), m_imageSize.getHeight());
            }
        }

        /**
         * This constructor is called for the nex XML format processing.
         *
         * @param n
         * @param version XML revision number
         */
        ImageDescriptor(Node n, double version, String uniqueID) {
            loadImageDescriptor(n, m_xmlVersion);
            if (m_firstReadImageSize == null) {
                //make sure we only store the image dimensions of the lowest mag image, which will be
                // the first one that we read out of the specimen.
                m_firstReadImageSize = new Point2D.Double(m_imageSize.getWidth(), m_imageSize.getHeight());
            }
        }

        public String toString() {
            return ("ID:" + m_id + " Pixel:" + m_pixelSize + " Image:" + m_imageSize + " Tile:" + m_tileSize);
        }

        public Dimension getTileSize() {
            return m_tileSize;
        }

        public Dimension getImageSize() {
            return m_imageSize;
        }

        public Point2D.Double getPixelSize() {
            return m_pixelSize;
        }

        /**
         * Returns the value that comunicates the scaling factor for the height value.
         * This only applies to AFM. AFM specimens convey the specimen height
         * information as brightness of a pixel. The brightness value range
         * from [0-1] these need to be converted into the appropriate scale.
         * This method returns the required multiplying factor.
         *
         * @see #m_pixelHeightRange
         * @deprecated Check with Chas. He does not set the "range" attribute under the magnification
         * control anymore. Instead it is set as an inforref inside the sample info tag.
         */

        public Double getPixelHeightRange() {
            return m_pixelHeightRange;
        }

        /**
         * This method does all the loading work for the image descriptor tag.
         * The "ImageDescriptor" has three children "TileSize", "PixelSize","ImageSize".
         * The method iterates through all the children and only processes these three of them.
         * <p>
         * This method is generic enough to service both the old XML version and the
         * New XML version.
         */
        protected boolean gutsOfLoadElement(Node child) {
            boolean retStatus = false;

            if (child.getNodeType() == Node.ELEMENT_NODE) {
                if (child.getNodeName().equals(Constants.XMLTags.TILE_SIZE)) {
                    m_tileSize = loadDimensionAttribute(child);
                    retStatus = true;
                } else if (child.getNodeName().equals(Constants.XMLTags.PIXEL_SIZE)) {
                    m_pixelSize = loadDoubleDimensionAttribute(child);
                    NamedNodeMap attrs = child.getAttributes();
                    Node rangeNode = attrs.getNamedItem("range");
                    if (rangeNode != null) {
                        m_pixelHeightRange = new Double(rangeNode.getNodeValue());
                    }
                    retStatus = true;
                } else if (child.getNodeName().equals(Constants.XMLTags.IMAGE_SIZE)) {
                    m_imageSize = loadDimensionAttribute(child);
                    retStatus = true;
                }
            }
            return retStatus;
        }
    }

    /**
     * Encapsulates InstrumentDescriptor element.
     */
    public class InstrumentDescriptor extends SpecimenElement {
        private ArrayList m_infoRefs = new ArrayList();

        /**
         * The constructor that supports the loading of the specimen described using the
         * old XML revision. It takes in the InstrumentDescriptor node and loads all its
         * child elements into the m_params hasmap either as (ElementName, Value) pairs (if the
         * element does not have a childnode) or (ElementName.ChildName, Value) if the element has
         * a child.
         *
         * @param n Node of the "InstrumentDescriptor" tag.
         */
        InstrumentDescriptor(Node n) {
            loadElement(n);
        }

        /**
         * This constructor takes in the Node pointing to the "METADATA" tag and
         * loads in all the instrument specific information contained in this tag into
         * the m_params HashMap. The method also gets the specimen XMLrevision number.
         * This revision number helps the constructor decide how to extract the instrument
         * information from the Node n.
         *
         * @param n Node     pointing to the METADATA tag
         * @param revision
         */
        InstrumentDescriptor(Node n, double revision) {
            loadInstrument(n, revision, Constants.XMLTags.TYPE);
            // System.out.println("Instantiated " + getId());
        }

        protected boolean gutsOfLoadElement(Node child) {
            if (child.getNodeName().equals(Constants.XMLTags.INFO_REF)) {
                InfoRef ir = new InfoRef(child);
                m_infoRefs.add(ir);
            } else {
                return false;
            }

            return true;
        }

        /**
         * This method is used to insert a new InfoRef object. This method is used by the
         * new XML version loading logic. The only reason for loading InfoRef through
         * this method is for backward compatibility and consistentecy.
         *
         * @param child
         * @param revision         *
         */
        protected boolean gutsOfLoadElement(Node child, double revision) {
            if (revision > 1) {
                if (child.getNodeName().equals(Constants.XMLTags.INFO_REF)) {
                    InfoRef ir = new InfoRef(child);
                    m_infoRefs.add(ir);
                } else {
                    return false;
                }
            }
            return true;
        }
    }

    /**
     * This class is used to store the info ref tags. These tags can be added to the
     * main tags to provide information like <Inforef href="http://www.itg.uiuc.edu">Prep Video</Inforef>.
     */
    public class InfoRef {
        String m_href = "";
        String m_text = "";

        InfoRef(Node node) {
            NamedNodeMap attrs = node.getAttributes();
            Node hrefNode = attrs.getNamedItem(Constants.XMLTags.HREF);

            if (hrefNode != null) {
                m_href = hrefNode.getNodeValue();
            }
            m_text = getFirstTextChild(node);
        }
    }

    /**
     * Encapsulates ControlDescriptor element.
     */
    public class ControlDescriptor extends SpecimenElement {
        /**
         * This variable keeps the path information for this control.
         * Given the control, this information is used to find which
         * Data Node in the Specimen does this control descriptor
         * refer to.
         */
        private String m_path;

        ControlDescriptor(Node n) {
            loadElement(n);
        }

        /**
         * This constructor is used by the latest XML format loading logic. The constructor does
         * not do anything. All the loading is done outside the constructor.
         */
        ControlDescriptor() {
        }

        /**
         * The getter for the m_path variable
         *
         * @return m_path
         */
        public String getPath() {
            return m_path;
        }

        /**
         * The setter for the m_path variable
         *
         * @param path to set
         */
        public void setPath(String path) {
            m_path = path;
        }

    }

    /**
     * Encapsulates ControlState element.
     */
    public class ControlState extends SpecimenElement {
        private ControlDescriptor m_controlDescriptor;
        private ControlStates m_controlStates;
        private ImageDescriptor m_imageDescriptor;

        ControlState(Node n) {
            loadElement(n);
        }

        public ControlDescriptor getControlDescriptor() {
            return m_controlDescriptor;
        }

        protected boolean gutsOfLoadElement(Node node) throws SpecimenException {
            if (node.getNodeName().equals("ControlDescriptor")) {
                if (!isReference(node)) {
                    m_controlDescriptor = new ControlDescriptor(node);
                } else {
                    // We have encountered a tag that refers to another tag.
                    // This will happen when we encounter a <ControlDescriptor idref="">
                    // type of a tag. This indictaes that the parent node is infact something that
                    // links with a GUI control e.g. MAGNIFICATION or FOCUS.
                    // Our line of action is simply to set the m_constolrDescriptor variable
                    // to point to that GUI control.
                    String cdescid = getElementRefId(node);
                    m_controlDescriptor = (ControlDescriptor) Specimen.this.m_controlDescriptors.get(cdescid);
                }
            } else {
                return false;
            }

            return true;
        }

        private void resolveReferences() {
            String cssid = (String) m_params.get("ControlStates.idref");

            if (cssid != null) {
                m_controlStates = (ControlStates) Specimen.this.m_controlStates.get(cssid);
            }

            String imgDescId = (String) m_params.get("ImageDescriptor.idref");

            if (imgDescId != null) {
                m_imageDescriptor = (ImageDescriptor) Specimen.this.m_imageDescriptors.get(imgDescId);
            }
        }

        private String getPathPart() {
            String pp = (String) getNamedParam("pathPart");
            return pp == null ? "" : pp;
        }
    }

    /**
     * Encapsulates ControlStates element.
     */
    public class ControlStates extends SpecimenElement {
        /**
         * This linked hashmap contains all the overlay elements contained in this <Control> node.
         */
        private LinkedHashMap m_overlayElementMap = new LinkedHashMap();

        /**
         * The pointer to the parent of this node.
         */
        private ControlStates m_parent = null;

        /**
         * This hashmap contains one ControlState object for each "ControlState"
         * tag that is placed under the "ControlStates" tag.
         * However if the XML is of new format then this hashmap will contain one
         * ControlStates Object for each of the child "Control" node. The key for
         * the hashmap will be the FolderName for the particular child "Level" node.
         */
        private LinkedHashMap m_states = new LinkedHashMap();
        /**
         * This attribute holds the FolderName of the "Level" node. In old version
         * of the XML the FolderName was called PathPart and it was stored in the m_params.
         * With the nex XML we shall store the FolderName as a separate variable in this Class.
         * The reason for this is ease of accesibility.
         */
        private String m_folderName = "";
        /**
         * This holds the depth of the tree at which this node is placed. This information
         * is kept for ease of sorting and comparing the level.
         */
        private short m_depth = 0;
        /**
         * This is used by the nex XML format. This points to the control that controls this particular node.
         */
        private ControlDescriptor m_controlDescriptor = null;
        private ImageDescriptor m_imageDescriptor = null;

        ControlStates() {
        }

        ControlStates(Node n) {
            loadElement(n);
        }

        /**
         * Returns the pixel size of the image. null if this control is not the leaf node.
         *
         * @return
         */
        public Point2D.Double getImagePixelSize() {
            if (m_imageDescriptor != null)
                return (m_imageDescriptor.m_pixelSize);
            else
                return null;
        }

        /**
         * The getter for the m_states, which contains all the immediate
         * children of this node.
         *
         * @return LinkedHashMap containing the controlstates for all the immediate children.
         */
        public LinkedHashMap getStates() {
            return m_states;
        }

        /**
         * returns the image descriptor for this controlstates. This will only have a
         * NON-NULL value if this controlstates represent a lead node (usually MAGNIFICATION node),
         * it will be null in all other cases.
         *
         * @return
         */
        public ImageDescriptor getImageDescriptor() {
            return m_imageDescriptor;
        }

        public ControlStates getParent() {
            return m_parent;
        }

        public LinkedHashMap getOverlayMap() {
            return m_overlayElementMap;
        }

        /**
         * The getter for m_folderName
         *
         * @return the folderName for the controlDescriptor
         */
        public String getFolderName() {
            return m_folderName;
        }

        /**
         * The setter for the folder name
         *
         * @param folderName
         */
        public void setFolderName(String folderName) {
            m_folderName = folderName;
        }

        protected boolean gutsOfLoadElement(Node node) throws SpecimenException {
            if (node.getNodeName().equals("ControlState")) {
                if (!isReference(node)) {
                    ControlState cs = new ControlState(node);
                    m_states.put(cs.getId(), cs);
                    Specimen.this.m_controlStateDescriptors.put(cs.getId(), cs);
                } else {
                    String csid = getElementRefId(node);
                    ControlState cs = (ControlState) Specimen.this.m_controlStateDescriptors.get(csid);

                    if (cs != null) {
                        m_states.put(csid, cs);
                    }
                }
            } else {
                return false;
            }

            return true;
        }

        private ControlState getFirstState() {
            if (m_states.size() < 1) {
                return null;
            }

            Iterator iter = m_states.values().iterator();
            return (ControlState) iter.next();
        }

        /**
         * This method gets the first stored controlstate and returns its ControlDescriptor.
         * It need not check other than the first states since with in each ControlStates all the
         * stored ControlState will have the same controldescriptor.
         *
         * @return the controldescriptor of that controls this node.
         */

        public ControlDescriptor getControlDescriptor() {
            ControlState cs = getFirstState();
            return cs != null ? cs.getControlDescriptor() : null;
        }

        /**
         * This method calls the getControlDescriptor() method if it is an old XML
         * else it simply returns the controldescriptor stored in the object. The old
         * version of the XML did not store the controldescriptors against the controlstates
         * object therefore to get the control it had to dive into the children controlstate and get
         * the descriptor of the first child. But in the new XML logic we store the control
         * state of each node within the object.
         *
         * @return the controldescriptor of that controls this node.
         */

        public ControlDescriptor getControlDescriptor(double version) {
            if (version <= 1.0)
                return getControlDescriptor();
            else
                return m_controlDescriptor;
        }

        public int getDepth() {
            return m_depth;
        }
    }


    /**
     * Encapsulates Root element.
     */
    class Root extends SpecimenElement {
        private ControlStates m_states;

        Root(Node n) {
            loadElement(n);
        }

        /**
         * This constructor takes in a ControlStates Object which contains the completely
         * parsed and fully loaded "Level" tree nodes. This ControlStates object is itself
         * the root of the parsed tree. The only reason why we are making another root on top
         * of the Controlstates object is for backward compatibility. The old XML format loading logic
         * used the Root object to contain the first level ControlStates Object, and we are following the
         * same logic here.
         *
         * @param rootState
         */
        Root(ControlStates rootState) {
            m_states = rootState;
        }

        protected boolean gutsOfLoadElement(Node node) throws SpecimenException {
            if (node.getNodeName().equals("ControlStates")) {
                if (!isReference(node)) {
                    ControlStates css = new ControlStates(node);
                    m_states = css;
                    Specimen.this.m_controlStates.put(css.getId(), css);
                } else {
                    String cssid = getElementRefId(node);
                    ControlStates css = (ControlStates) Specimen.this.m_controlStates.get(cssid);
                    m_states = css;
                }
            } else {
                return false;
            }

            return true;
        }

        private String getPathPart() {
            String pp = (String) getNamedParam("pathPart");
            return pp == null ? "" : pp;
        }
    }

    class ParseErrorHandler extends DefaultHandler {
        //Receive notification of a recoverable error.
        public void error(SAXParseException se) throws SAXParseException {
            throw se;
        }

        //Receive notification of a non-recoverable error.
        public void fatalError(SAXParseException se) throws SAXParseException {
            throw se;
        }

        //Receive notification of a warning.
        public void warning(SAXParseException se) throws SAXParseException {
            throw se;
        }
    }
}