/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import virtuallab.util.Locale;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

//$Id: ConfigEditor.java,v 1.6 2006/09/05 01:08:47 manzoor2 Exp $
/**
 * Configuration editor.
 */
public class ConfigEditor extends JDialog {

    private static final Log log = new Log(SpecimenSet.class.getName());

    /**
     * Title of the dialog.
     */
    protected static final String TITLE = Locale.EditConfiguration(Configuration.getApplicationLanguage());
    /**
     * Size of the text field (in chars).
     */
    protected static final int TXT_FIELD_SIZE = 15;
    /**
     * Padding arond components of the dialog.
     */
    protected static final int PADDING = 10;
    private static final long serialVersionUID = 14215933756722993L;
    /**
     * Configuration object.
     */
    protected Configuration m_config;
    /**
     * Apply button.
     */
    protected JButton m_bttnApply;
    /**
     * Specimen directory text field.
     */
    JTextField m_textSpecimenDir;
    /**
     * JDK check Options.
     */
    JComboBox m_textJdkCheck;
    /**
     * The speed selection combo.
     */
    JComboBox m_navigationSpeed;
    /**
     * Annotation directory text field.
     */
    JTextField m_textAnnotDir;

    JComboBox m_languageOptions;

    /**
     * @param owner  owner frame of this dialog
     * @param config non-null Configuration object.
     */
    public ConfigEditor(Frame owner, Configuration config) {
        super(owner, TITLE, true);

        if (config == null) {
            throw new IllegalArgumentException("No Configuration object specified");
        }

        m_config = config;

        getContentPane().add(createButtonPanel(), BorderLayout.SOUTH);
        getContentPane().add(createEditPanel(), BorderLayout.NORTH);

        pack();
        setLocationRelativeTo(owner);

        setResizable(false);
    }

    /**
     * Creates button panel for this dialog.
     *
     * @return JPanel populated with buttons.
     */
    protected JPanel createButtonPanel() {
        JPanel buttonPanel = new JPanel();

        m_bttnApply = new JButton(Locale.Apply(Configuration.getApplicationLanguage()));
        JButton bttnCancel = new JButton(Locale.Cancel(Configuration.getApplicationLanguage()));

        bttnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        m_bttnApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (updateAndValidateConfig()) {
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(ConfigEditor.this,
                            Locale.InvalidConfiguration(Configuration.getApplicationLanguage())+".",
                            Locale.Error(Configuration.getApplicationLanguage()),
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        buttonPanel.add(m_bttnApply);
        buttonPanel.add(bttnCancel);

        return buttonPanel;
    }

    /**
     * Creates edit panel for this dialog.
     *
     * @return JPanel populated with configuration info.
     */
    protected JPanel createEditPanel() {
        SpringLayout layout = new SpringLayout();
        JPanel editPanel = new JPanel(layout);

        editPanel.setBorder(new CompoundBorder(new EtchedBorder(EtchedBorder.RAISED), new EmptyBorder(PADDING, PADDING, PADDING, PADDING)));

        m_textSpecimenDir = new JTextField(m_config.getSpecimenDir(), TXT_FIELD_SIZE);
        m_textAnnotDir = new JTextField(m_config.getAnnotDir(), TXT_FIELD_SIZE);
        m_textJdkCheck = new JComboBox();
        m_textJdkCheck.addItem(Configuration.OptionYes(Configuration.getApplicationLanguage()));
        m_textJdkCheck.addItem(Configuration.OptionNo(Configuration.getApplicationLanguage()));
        if (m_config.shouldCheckJdk())
            m_textJdkCheck.setSelectedIndex(0);
        else
            m_textJdkCheck.setSelectedIndex(1);

        m_navigationSpeed = new JComboBox();
        m_navigationSpeed.addItem("1");
        m_navigationSpeed.addItem("2");
        m_navigationSpeed.addItem("3");
        m_navigationSpeed.addItem("4");
        m_navigationSpeed.addItem("5");
        m_navigationSpeed.addItem("6");
        m_navigationSpeed.addItem("7");
        m_navigationSpeed.addItem("8");
        m_navigationSpeed.addItem("9");
        m_navigationSpeed.addItem("10");
        m_navigationSpeed.setSelectedIndex(m_config.getNavigationSpeedAsInt() - 1);


        JLabel speciemnDirLabel = new JLabel(Locale.SpecimenDirectory(Configuration.getApplicationLanguage())+": ", JLabel.TRAILING);
        speciemnDirLabel.setToolTipText("<html>"+Locale.TheSpecimensDirectory(Configuration.getApplicationLanguage())+".</html>");
        speciemnDirLabel.setBorder(new EmptyBorder(1,0,0,0));
        editPanel.add(speciemnDirLabel);
        speciemnDirLabel.setLabelFor(m_textSpecimenDir);
        editPanel.add(m_textSpecimenDir);

        layout.putConstraint(SpringLayout.NORTH, speciemnDirLabel, 0, SpringLayout.NORTH, editPanel);
        layout.putConstraint(SpringLayout.WEST, speciemnDirLabel, 0, SpringLayout.WEST, editPanel);
        layout.putConstraint(SpringLayout.WEST, m_textSpecimenDir, PADDING, SpringLayout.EAST, speciemnDirLabel );
        layout.putConstraint(SpringLayout.EAST, m_textSpecimenDir, 0, SpringLayout.EAST, editPanel );

        JLabel annotationDirLabel = new JLabel(Locale.AnnotationsDirectory(Configuration.getApplicationLanguage())+": ", JLabel.TRAILING);
        annotationDirLabel.setToolTipText("<html>"+Locale.TheAnnotationDirectory(Configuration.getApplicationLanguage())+".</html>");
        annotationDirLabel.setBorder(new EmptyBorder(1,0,0,0));
        editPanel.add(annotationDirLabel);
        annotationDirLabel.setLabelFor(m_textAnnotDir);
        editPanel.add(m_textAnnotDir);

        layout.putConstraint(SpringLayout.NORTH, annotationDirLabel, PADDING, SpringLayout.SOUTH, m_textSpecimenDir);
        layout.putConstraint(SpringLayout.WEST, annotationDirLabel, 0, SpringLayout.WEST, editPanel);
        layout.putConstraint(SpringLayout.NORTH, m_textAnnotDir, PADDING, SpringLayout.SOUTH, m_textSpecimenDir );
        layout.putConstraint(SpringLayout.WEST, m_textAnnotDir, PADDING, SpringLayout.EAST, annotationDirLabel );
        layout.putConstraint(SpringLayout.EAST, m_textAnnotDir, 0, SpringLayout.EAST, editPanel );

        JLabel jdkCheckLabel = new JLabel(Locale.JDKcheck(Configuration.getApplicationLanguage())+": ", JLabel.TRAILING);
        jdkCheckLabel.setToolTipText("<html>"+Locale.JDKcheckBox(Configuration.getApplicationLanguage())+".<html>");
        jdkCheckLabel.setBorder(new EmptyBorder(4,0,0,0));
        editPanel.add(jdkCheckLabel);
        jdkCheckLabel.setLabelFor(m_textJdkCheck);
        editPanel.add(m_textJdkCheck);

        layout.putConstraint(SpringLayout.NORTH, jdkCheckLabel, PADDING, SpringLayout.SOUTH, m_textAnnotDir);
        layout.putConstraint(SpringLayout.WEST, jdkCheckLabel, 0, SpringLayout.WEST, editPanel);
        layout.putConstraint(SpringLayout.NORTH, m_textJdkCheck, PADDING, SpringLayout.SOUTH, m_textAnnotDir );
        layout.putConstraint(SpringLayout.WEST, m_textJdkCheck, PADDING, SpringLayout.EAST, jdkCheckLabel );

        JLabel navSpeedLabel = new JLabel(Locale.SpecimenScrollSpeed(Configuration.getApplicationLanguage())+": ", JLabel.TRAILING);
        navSpeedLabel.setToolTipText("<html>"+Locale.SpecimenScrollSpeedTip(Configuration.getApplicationLanguage())+".<html>");
        navSpeedLabel.setBorder(new EmptyBorder(4,0,0,0));
        editPanel.add(navSpeedLabel);
        navSpeedLabel.setLabelFor(m_navigationSpeed);
        editPanel.add(m_navigationSpeed);

        layout.putConstraint(SpringLayout.NORTH, navSpeedLabel, PADDING, SpringLayout.SOUTH, m_textJdkCheck);
        layout.putConstraint(SpringLayout.WEST, navSpeedLabel, 0, SpringLayout.WEST, editPanel);
        layout.putConstraint(SpringLayout.NORTH, m_navigationSpeed, PADDING, SpringLayout.SOUTH, m_textJdkCheck );
        layout.putConstraint(SpringLayout.WEST, m_navigationSpeed, PADDING, SpringLayout.EAST, navSpeedLabel );

        JLabel languageLabel = new JLabel(Locale.InterfaceLanguage(Configuration.getApplicationLanguage())+": ", JLabel.TRAILING);
        languageLabel.setToolTipText("<html>"+Locale.InterfaceLanguage(Configuration.getApplicationLanguage())+".<html>");
        languageLabel.setBorder(new EmptyBorder(4,0,0,0));
        editPanel.add(languageLabel);
        m_languageOptions = new JComboBox(Locale.LANGUAGE.values());
        m_languageOptions.setSelectedItem(m_config.getApplicationLanguage());
        languageLabel.setLabelFor(m_languageOptions);
        editPanel.add(m_languageOptions);

        layout.putConstraint(SpringLayout.NORTH, languageLabel, PADDING, SpringLayout.SOUTH, m_navigationSpeed);
        layout.putConstraint(SpringLayout.WEST, languageLabel, 0, SpringLayout.WEST, editPanel);
        layout.putConstraint(SpringLayout.NORTH, m_languageOptions, PADDING, SpringLayout.SOUTH, m_navigationSpeed );
        layout.putConstraint(SpringLayout.WEST, m_languageOptions, PADDING, SpringLayout.EAST, languageLabel );

        int width = 480;
        int height = 200;
        editPanel.setPreferredSize(new Dimension(width,height));

        return editPanel;
    }

    /**
     * Updates config with cuttent values.
     *
     * @return true iff the new configuration is valid
     */
    protected boolean updateAndValidateConfig() {
        m_config.setSpecimenDir(m_textSpecimenDir.getText().trim());
        m_config.setAnnotDir(m_textAnnotDir.getText().trim());
        m_config.setCheckJdk(m_textJdkCheck.getSelectedItem().toString());
        m_config.setNavigationSpeed(m_navigationSpeed.getSelectedItem().toString());

        Locale.LANGUAGE oldLanguage = m_config.getApplicationLanguage();
        m_config.setApplicationLanguage((Locale.LANGUAGE)m_languageOptions.getSelectedItem());
        if (oldLanguage!=m_config.getApplicationLanguage()) {
            restartApplication();
        }

        return m_config.validateConfig();
    }

    private void restartApplication() {
        m_config.writeConfig();
        try {
            String application = new File(VirtualMicroscope.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getPath();
            Runtime.getRuntime().exec("java -jar "+application);
        }
        catch (Exception exception) {
            log.error("Location of the Virtuallab.jar file could not be established. This was required for restart after language change!");
        }
        finally {
            System.exit(0);
        }
    }
}
