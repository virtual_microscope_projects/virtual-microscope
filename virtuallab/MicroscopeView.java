/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseWheelListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ByteLookupTable;
import java.awt.image.LookupOp;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.Vector;
import javax.imageio.ImageIO;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.event.MouseInputAdapter;

import virtuallab.gui.AdjustmentControlBase;
import virtuallab.gui.Control;
import virtuallab.gui.Microscope;
import virtuallab.gui.NavigationThumbnail;
import virtuallab.gui.TextMenu;
import virtuallab.gui.ViewControl;
import virtuallab.gui.controls.annotation.AnnotationControl;
import virtuallab.gui.controls.eds.EDSMenu;
import virtuallab.gui.controls.measure.DistanceMeasure;
import virtuallab.gui.controls.measure.MeasureControlBase;
import virtuallab.util.Constants;

// $Id: MicroscopeView.java,v 1.34 2006/10/25 19:36:33 manzoor2 Exp $
/**
 * This class represents the View for the Currently loaded Specimen; whereas the
 * Specimen holds the model that this view represents.
 * This class represents the area of the window where the specimen is shown. The
 * screen is divided into two portions 1). the specimen viewing area (left of
 * the screen) 2). The side panel with all the controls in it. This class
 * represents the former. This class embeds all the logic for loading the
 * specimen and unloading it. It uses other helper classes to acheive this task.
 * Basically any thing that is to do with the loaded specimen is handled by this
 * class.
 */
public class MicroscopeView extends JComponent implements Printable {

    private static final Log log = new Log(MicroscopeView.class.getName());

    private static final long serialVersionUID = -4297666948263431477L;

    /**
     * Name of the brighness control.
     */
    private static final String CONTROL_NAME_BRIGHTNESS = "BRIGHTNESS";

    /**
     * Name of the contrast control.
     */
    private static final String CONTROL_NAME_CONTRAST = "CONTRAST";

    /**
     * Default value for brightness.
     */
    private static final double DEF_BRIGHTNESS = 0.5;

    /**
     * Default value for contrast.
     */
    private static final double DEF_CONTRAST = 0.5;

    private static HashMap cursors;

    static {
        cursors = new HashMap();
        cursors.put(new Integer(Cursor.DEFAULT_CURSOR), new Cursor(
                Cursor.DEFAULT_CURSOR));
        cursors.put(new Integer(Cursor.MOVE_CURSOR), new Cursor(
                Cursor.MOVE_CURSOR));
        cursors.put(new Integer(Cursor.TEXT_CURSOR), new Cursor(
                Cursor.TEXT_CURSOR));
        cursors.put(new Integer(Cursor.NE_RESIZE_CURSOR), new Cursor(
                Cursor.NE_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.NW_RESIZE_CURSOR), new Cursor(
                Cursor.NW_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.SE_RESIZE_CURSOR), new Cursor(
                Cursor.SE_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.SW_RESIZE_CURSOR), new Cursor(
                Cursor.SW_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.N_RESIZE_CURSOR), new Cursor(
                Cursor.N_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.S_RESIZE_CURSOR), new Cursor(
                Cursor.S_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.E_RESIZE_CURSOR), new Cursor(
                Cursor.E_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.W_RESIZE_CURSOR), new Cursor(
                Cursor.W_RESIZE_CURSOR));
        cursors.put(new Integer(Cursor.HAND_CURSOR), new Cursor(
                Cursor.HAND_CURSOR));
        cursors.put(new Integer(Cursor.CROSSHAIR_CURSOR), new Cursor(
                Cursor.CROSSHAIR_CURSOR));

    }

    /**
     * Holds the previous top coordinates for the overlay image.
     */
    Point2D.Double m_oldTop;
    /**
     * Holds the previous top coordinates for the overlay image.
     */
    Point2D.Double m_oldBottom;
    private short m_scrollDirection = Constants.NONE;
    private boolean m_shouldResetCursor = false;
    private boolean horizontalBarStillVisible;
    private boolean verticalBarsVisible;
    /**
     * This variable holds the dimensions of the viewable are of the specimen.
     * In other words this is the area of the widnow minus the sidepanel.
     */
    private Dimension m_beamSize;
    /**
     * Stage Position holds the starting coordinates of the first tile i.e. the
     * tile corresponding to row 0 column 0 with respect to the base (0,0) i.e.
     * top left of the viewable screen. A minus value indicates that the
     * begining of the tile (top left) is not visible. Positive values
     * indicates that the begining of the tile is towards right/below the
     * top-left of the screen.
     */
    private Point2D.Double m_stagePosition;
    /**
     * The reference to the specimen that is currently loaded and is being
     * viewed.
     */
    private Specimen m_currentSpecimen;
    /**
     * The cache that holds the tiles for the currently loaded base image.
     */
    private ImageCache m_imageCache;
    /**
     * The class that handles all the events that are fired from with in the
     * viewport.
     */
    private MicroscopeView.EventListener m_eventListener;
    /**
     * The mouse position in the view port.
     */
    private Point2D.Double m_mousePosition;
    /**
     * Holds the reference to the EDS Control. This will be null if the specimen
     * does not have an EDS control. This attribute is set by the EDSMenu itself
     * when it gets instantiated. Beawarr that microscopeview is application
     * specific i.e. there would be one instance of MicroscopeView throughout
     * the application and EDSMenu is specimen specific. So we need to make sure
     * we reinitialize all EDSMenu specific parameters when a new specimen is
     * loaded. TODO: Change the design so that the EDS menu specific options are
     * put in the specimen itself.
     */
    private EDSMenu m_edsControl = null;
    /**
     * The reference to the navigator control. Used to pass on events through
     * which the navigator can keep itself insync with the base image.
     */
    private NavigationThumbnail m_navigator = null;
    /**
     * The reverse reference to the class which contains this instance of
     * MicroscopeView. This is needed to facilitate the Accessibility features,
     * since the Accessbility complaince requires some interesting chalenges on
     * keyboard navigation. The easiest (albeit quick and dirty) solution is to
     * keey the references of the contained classes so that we can perform
     * virtually anything from where ever a key is pressed.
     */
    private Microscope m_microscope;
    /**
     * Reference to self, used by the container classes for some redispatching
     * events to the MicroscopeView.
     */
    private MicroscopeView m_microscopeView;
    /**
     * The listener for the key board events.
     */
    private MicroscopeView.KeyboardListener m_keyboardListener;
    /**
     * The variable holds the key board event listener class that is used to
     * register the pre-listening of key board event functionality.
     */
    private MicroscopeView.KeyEventPreListener m_keyboardPreListener;
    /**
     * The variable holds the KeyboardFocusManager that is used to register the
     * pre-listening of key board event functionality.
     */
    private KeyboardFocusManager m_keyboardFocusManager;
    /**
     * Controls displayed in the view (e.g. distance measure).
     */
    private LinkedHashSet m_viewControls;
    /**
     * Side image controls (e.g. brightness).
     */
    private Collection m_imageControls;
    private LinkedList m_controlsToAdd;
    private LinkedList m_controlsToDelete;
    private ImageSet m_currentImageSet;
    private Collection m_specimenControlStates;
    private double m_brightness;
    private double m_contrast;
    private BufferedImageOp m_bcOp;
    private boolean m_brightnessIsAdjusting;

    //	private boolean m_firstTime = true;
    private boolean m_contrastIsAdjusting;

    public MicroscopeView(Microscope containerClass) {
        m_microscope = containerClass;
        m_currentSpecimen = null;
        m_imageCache = new ImageCache();
        m_eventListener = new MicroscopeView.EventListener();
        m_keyboardListener = new MicroscopeView.KeyboardListener();
        m_keyboardPreListener = new MicroscopeView.KeyEventPreListener();
        m_mousePosition = new Point2D.Double();
        m_viewControls = null;
        m_imageControls = null;
        m_controlsToAdd = new LinkedList();
        m_controlsToDelete = new LinkedList();
        m_currentImageSet = null;
        m_specimenControlStates = null;
        m_brightness = DEF_BRIGHTNESS; // [0, 1]
        m_contrast = DEF_CONTRAST; // [0, 1]
        m_bcOp = null;
        m_brightnessIsAdjusting = false;
        m_contrastIsAdjusting = false;
        m_microscopeView = this;
        m_oldTop = new Point2D.Double(-1000, -1000); // used by EDS overlay
        // drawing logic.
        m_oldBottom = new Point2D.Double(-1000, -1000); // used by EDS overlay
        // drawing logic.

        setOpaque(true);
        setFocusable(true);
        setBackground(Color.black);

        addComponentListener(new MicroscopeView.DisplayListener());
        addMouseListener(m_eventListener);
        addMouseWheelListener(m_eventListener);
        addMouseMotionListener(m_eventListener);
        addKeyListener(m_keyboardListener);

        m_beamSize = new Dimension(0, 0);
        m_stagePosition = new Point2D.Double(0, 0);

        // This will let us get ALL the keyboard events irrespetive of where
        // they occur in the
        // application window.
        m_keyboardFocusManager = KeyboardFocusManager
                .getCurrentKeyboardFocusManager();
        m_keyboardFocusManager.addKeyEventDispatcher(m_keyboardPreListener);
        requestFocusInWindow();
        updateLookupOp();
    }

    public Microscope getMicroscope() {
        return m_microscope;
    }

    /**
     * The getter method for the m_scrollDirection.
     *
     * @return the direction in which the user is scrolling the image.
     */
    public short getScrollDirection() {
        return m_scrollDirection;
    }

    /**
     * The setter method for the m_scrollDirection. This stores the direction in
     * which the user is currently scrolling the image.
     *
     * @param direction
     */
    public void setScrollDirection(short direction) {
        m_scrollDirection = direction;
    }

    /**
     * Returns the area in which the speicmen is be shown i.e. the area of the
     * application window minus the side panel.
     *
     * @return beamsize.
     */
    public Dimension getBeamSize() {
        return m_beamSize;
    }

    /**
     * This method sets the cursor for the display.
     *
     * @param type the cursor that is to be set. This value is matched with the
     *             initialized cursors stored at the load time.
     */
    public void setCursor(int type) {
        Cursor cursor = (Cursor) cursors.get(new Integer(type));

        if (cursor != null && !cursor.equals(getCursor())) {
            setCursor(cursor);
        }
    }

    /**
     * Stage Position holds the starting coordinates of the first tile i.e. the
     * tile corresponding to row 0 column 0 with respect to the screen coordinates i.e.
     * w.r.t the top left of the viewable screen. A minus value indicates that the
     * begining of the tile (top left) is not visible. Positive values
     * indicates that the begining of the tile is towards right/below the
     * top-left of the screen.
     */
    public Point2D.Double getStagePosition() {
        return m_stagePosition;
    }

    /**
     * Through this setter we can realign the base image.
     *
     * @param x The top left X-axis of the base image relative to the center of
     *          the viewport
     * @param y The top left Y-axis of the base image relative to the center of
     *          the viewport
     */
    public void setStagePosition(double x, double y) {
        m_stagePosition.setLocation(x, y);
    }

    public Specimen getCurrentSpecimen() {
        return m_currentSpecimen;
    }

    public void setCurrentSpecimen(Specimen newSpecimen) {
//		m_firstTime = true;
        if (newSpecimen == null) {
            return;
        }
        m_imageCache.clear();
        m_currentImageSet = null;
        m_edsControl = null;
        m_currentSpecimen = null;
        m_currentSpecimen = newSpecimen;
        // Readjust the image cache size since now we have a different
        // specimen which may not have EDS control.
        m_imageCache.setCacheSizeToDefault();
        m_stagePosition.setLocation(0, 0);
        setBrightnessAndContrast(DEF_BRIGHTNESS, DEF_CONTRAST);
        m_microscope.getSidePanel().setCursor(
                Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        repaint();
    }

    public Collection getSpecimenControlStates() {
        return m_specimenControlStates;
    }

    public void setSpecimenControlStates(Collection specimenControlStates) {
        m_specimenControlStates = specimenControlStates;
        repaint();
    }

    /**
     * This method takes in a new side control and adds it to the existing
     * imagecontrols.
     *
     * @param controls
     */
    public void addToImageControls(Control control) {
        m_imageControls.add(control);
        updateImageControls();
    }

    public void setImageControls(Collection controls) {
        m_imageControls = controls;
        updateImageControls();
    }

    /**
     * retrieves the requested control.
     *
     * @param controlName
     * @return
     */
    public Control getViewControl(String controlName) {
        Control requestedControl = null;
        Iterator it = m_viewControls.iterator();
        while (it.hasNext()) {
            Control cntrl = (Control) it.next();
            if (cntrl.getControlName().equals(controlName)) {
                requestedControl = cntrl;
                break;
            }
        }
        return requestedControl;
    }

    /**
     * This method returns the view controls.
     *
     * @return LinkedHashSet containing the controls
     */
    public LinkedHashSet getViewControls() {
        return m_viewControls;
    }

    /**
     * This method sets the view controls.
     *
     * @param controls
     */
    public void setViewControls(LinkedHashSet controls) {
        m_viewControls = controls;
    }

    /**
     * This method returns the AnnotationControl.
     */
    public AnnotationControl getAnnotationControl() {
        AnnotationControl ret = null;
        Iterator controlsIter = MicroscopeView.this.m_viewControls.iterator();
        while (controlsIter.hasNext()) {
            Object o = controlsIter.next();
            if (o instanceof AnnotationControl) {
                ret = (AnnotationControl) o;
                break;
            }
        }
        return ret;
    }

    public void updateImageControls() {
        // Update brightness and contrast
        double newBri = m_brightness;
        double newCon = m_contrast;
        boolean briWasAdjusting = m_brightnessIsAdjusting;
        boolean conWasAdjusting = m_contrastIsAdjusting;

        Iterator controlsIter = m_imageControls.iterator();
        while (controlsIter.hasNext()) {
            Control aControl = (Control) controlsIter.next();

            if ((aControl != null)
                    && (aControl instanceof AdjustmentControlBase)) {
                if (aControl.getControlName().equalsIgnoreCase(
                        CONTROL_NAME_BRIGHTNESS)) {
                    newBri = ((AdjustmentControlBase) aControl).getValue();
                    m_brightnessIsAdjusting = ((AdjustmentControlBase) aControl)
                            .valueIsAdjusting();
                } else if (aControl.getControlName().equalsIgnoreCase(
                        CONTROL_NAME_CONTRAST)) {
                    newCon = ((AdjustmentControlBase) aControl).getValue();
                    m_contrastIsAdjusting = ((AdjustmentControlBase) aControl)
                            .valueIsAdjusting();
                }
            }
        }

        if ((m_brightnessIsAdjusting || m_contrastIsAdjusting)
                && !(briWasAdjusting || conWasAdjusting)) { // If we just
            // started making
            // adjustments
            m_imageCache.setImageOp(null);
        } else if (!(m_brightnessIsAdjusting || m_contrastIsAdjusting)
                && (briWasAdjusting || conWasAdjusting)) { // If we just
            // finished making
            // adjustments
            m_bcOp = null;
        }
        setBrightnessAndContrast(newBri, newCon);
    }

    /**
     * This method gets called when the user presses File->Print option. The
     * method passes on the Printer device context to the custom
     * paintComponent() method. The paintComponent() is normally used to update
     * the screen but this time around it will spitout the specimen image onto
     * the printer. We only print a single page. We get the printer page
     * dimensions and see if the the visible portion of the image can fit into
     * the printer page. If it does we do do nothing extra. If it does not, we
     * shrink the image to fit on the page.
     *
     * @param pg Graphics   <i>pg</i> the Printer Device Context passed on to this method
     * @param pageFormat PageFormat <i>pg</i> The page format.
     * @param pageIndex int        <i>pageIndex</i> This is the page number that we need to print.
     *                   Since we will only print one page (i.e. the current stuff shown on
     *                   the screen) we shall return out of the method if the pageIndex is
     *                   greater than one (i.e. if we get asked to pring page number 2 we
     *                   simply won't.)
     */
    public int print(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex >= 1)
            return NO_SUCH_PAGE;

        // Make sure no annotation is selected, since a selected annotation
        // distorts the print.
        // If the distance control did not consume the event then dispatch it to
        // others. This may
        // be redundant since the events may only be dispatched if they haven't
        // been consumed.

        Iterator controlsIter = MicroscopeView.this.m_viewControls.iterator();

        while (controlsIter.hasNext()) {
            Object o = controlsIter.next();
            if (o instanceof AnnotationControl) {
                ((AnnotationControl) o).unselectAnnotations();
                break;
            }
        }
        Graphics2D pg2d = (Graphics2D) pg;

        double visibleX = getBeamSize().getWidth(); // screen image visible area
        // width.
        double visibleY = getBeamSize().getHeight(); // screen image visible
        // area height.
        double ix = visibleX;
        double iy = visibleY;

        // If the visible image is larger than what can
        // be fitted on the single page then shrink the image to fit.
        double pageX = pageFormat.getWidth();
        double pageY = pageFormat.getHeight();
        if (ix <= pageX && iy <= pageY) {
            // The visible area can be easily printed on
            // the page. No resizing/clipping required.
            // Clip the graphics region so that we only print the
            // region of the image that is visible to the user.
            pg2d.setClip(0, 0, (int) visibleX, (int) visibleY);
        } else if (ix > pageX && iy <= pageY) {
            // the image width is larger than what we can fit on
            // the page. Need to resize. set the width to be the same
            // as the page's width and adjust height proportionaly.
            ix = pageX;
            iy = ix * (visibleY / visibleX);
            // scale to fit on the single page.
            pg2d.scale(ix / visibleX, iy / visibleY);
            // Clip the graphics region so that we only print the
            // region of the image that is visible to the user.
            pg2d.setClip(0, 0, (int) visibleX, (int) visibleY);
        } else if (iy > pageY && ix <= pageX) {
            // visibleY = visibleY - 5;
            // the image height is larger than what we can fit on
            // the page. Need to resize. set the height to be the same
            // as the page's height and adjust width proportionaly.
            iy = pageY;
            ix = iy * (visibleX / visibleY);
            // scale to fit on the single page.
            pg2d.scale(ix / visibleX, iy / visibleY);
            // Clip the graphics region so that we only print the
            // region of the image that is visible to the user.
            pg2d.setClip(0, 0, (int) visibleX, (int) visibleY);
        }
        paintComponent(pg);
        // Call GC since the print method gets called multiple time even for
        // a single page, so we don't want to run into memory issues.
        System.gc();
        // Runtime.getRuntime().gc();
        return PAGE_EXISTS;
    }

    public ImageSet getCurrentImageSet() {
        return m_currentImageSet;
    }

    public void setCurrentImageSet(ImageSet newImageSet) {
        // This code assumes that unique image sets have reference identity.

        if (newImageSet == null || newImageSet == m_currentImageSet) {
            return; // Don't execute if the new image set is the same as the
            // currently displayed one
        }

        if (newImageSet.getImageSize().width == 0
                || newImageSet.getImageSize().height == 0) {
            return;
        }

        if (m_currentImageSet != null) {
            // Keep current center-of-display location with zoom in and out.
            m_stagePosition.x = (m_beamSize.width / 2.0)
                    - ((double) newImageSet.getImageSize().width / (double) m_currentImageSet
                    .getImageSize().width)
                    * (m_beamSize.width / 2 - m_stagePosition.x);

            m_stagePosition.y = (m_beamSize.height / 2.0)
                    - ((double) newImageSet.getImageSize().height / (double) m_currentImageSet
                    .getImageSize().height)
                    * (m_beamSize.height / 2.0 - m_stagePosition.y);
        } else {
            centerImageInBeam(newImageSet);
        }

        m_currentImageSet = newImageSet;
    }

    private void centerImageInBeam(ImageSet imageSet) {
        if (imageSet == null) {
            return;
        }

        m_stagePosition.x = (m_beamSize.width - imageSet.getImageSize().width) / 2;
        m_stagePosition.y = (m_beamSize.height - imageSet.getImageSize().height) / 2;
    }

    /**
     * This method sets the viewable of the window in which the specimen is
     * shown. the viewable are is the window area minus the side panel i.e. the
     * area in which the specimen is shown.
     *
     * @param display The JComponent in which the specimen
     */
    private void updateBeamSize(JComponent display) {
        m_beamSize = display.getSize();
    }

    private void updateStageOffset() {
        if (m_currentImageSet == null) {
            return;
        }

        centerImageInBeam(m_currentImageSet);
    }

    /**
     * This method displays the specimen to the user. It gets called by the
     * container when repainting is invovked or when other implicit
     * redrawing/repainintg is required.
     *
     * @param Graphics the graphics device context on which to paint.
     */
    public void paintComponent(Graphics g) {
        // super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        try {
            // g2d = (Graphics2D)g.create();
            drawBackground(g2d);
            if (m_currentSpecimen == null) {
                return;
            }
            Collection specimenControlStates = getSpecimenControlStates();
            ImageSet imageSet = m_currentSpecimen.chooseImageSet(
                    specimenControlStates, m_currentSpecimen.getXMLVersion());
            if (imageSet == null) {
                return;
            }
            setCurrentImageSet(imageSet);
            drawImageSet(g2d, imageSet);
            if (m_edsControl != null) {
                // EDS related Overlay functionality.
                drawOverlayImage(g2d, m_edsControl.isBaseImageNone());
            }
            drawPositionReadout(g2d, imageSet);
            updateTransientControls();
            // Draw view controls
            Iterator iter = m_viewControls.iterator();
            while (iter.hasNext()) {
                ((ViewControl) iter.next()).draw(g2d);
            }
            // invoking the navigator thumbnail update functionality so that it
            // remains insync with
            // the portion of the base image that is viewable in the vieport.
            if (m_navigator != null)
                m_navigator.updateStatus();
        } catch (Throwable t) {
            log.error("Error painting Component");
            log.error(t.getMessage());
        }
        reorientSidePanel();
    }

    /**
     * This method makes sure that we never show a horizontal scroll bar and yet all
     * the contents of the side panel are always completely visible. It does so by
     * finding out whenever a horizontal scroll bar becomes necessary and increases
     * the width of the side panel pixel by pixel until the horizontal scroll bar
     * goes away. It also readjusts the splitpane so that the new width of the
     * sidepanel is respected.
     */
    private void reorientSidePanel() {
        if (m_microscope.getSidePanel().getScrollPane().getHorizontalScrollBar().isVisible()) {
            // must increase the width to get rid of the horizontal scroll bar.
            int height = (int) m_microscope.getSplitPane().getMinimumSize().getHeight();
            int width = (int) m_microscope.getSidePanel().getMinimumSize().getWidth();
            if (horizontalBarStillVisible) {
                // keep increasing the width pixel by pixel until the horizontal
                // bar goes away.
                width += 1;
            }
            int dividerLocation = m_microscope.getWidth() - width - 11;
//			System.out.println("width " + width + " divider " + dividerLocation);
            if (dividerLocation < 0) {
                // user has reduced the screen size to something that is less
                // than the width required to show all the side controls. In such a
                // case we have no option but to give up
                return;
            }
            Dimension newSize = new Dimension(width, height);
            m_microscope.getSidePanel().setMinimumSize(newSize);
            m_microscope.getSplitPane().setDividerLocation(dividerLocation);
            horizontalBarStillVisible = true;
        } else if (verticalBarsVisible
                && !m_microscope.getSidePanel().getScrollPane().getVerticalScrollBar().isVisible()) {
            // vertical bars are not visible anymore so we should reduce the
            // width.
            int height = (int) m_microscope.getSplitPane().getMinimumSize().getHeight();
            Dimension newSize = new Dimension(Microscope.SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS, height);
            m_microscope.getSidePanel().setMinimumSize(newSize);
            m_microscope.getSplitPane().setDividerLocation(m_microscope.getWidth()
                    - Microscope.SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS
                    - 11);
            horizontalBarStillVisible = false;
            verticalBarsVisible = false;
        } else if (m_microscope.getSidePanel().getScrollPane().getVerticalScrollBar().isVisible()) {
            // vertical scroll bars just became visible.
            verticalBarsVisible = true;
        }
    }

    /**
     * The method sets the flag which determines if the cursor should be reset
     * to DEFAULT when the mouse moves inside the viewport. There are multiple
     * components in the application that change the cursor. These modules have
     * the tendency to step onto each other's feet. By setting this variable we
     * ensure that every module that wants to reset the cursor first checks this
     * flag and then decides whether to reset the cursor or not.
     *
     * @param flag
     */
    public void setResetCursorFlag(boolean flag) {
        m_shouldResetCursor = flag;
    }

    /**
     * The method returns the flag which tells the calling method whether it can
     * safely reset the curso to DEFAULT or not.
     *
     * @return true if it is safe to reset the cursor to DEFAULT value.
     */
    public boolean canResetCursor() {
        return m_shouldResetCursor;
    }

    private void drawBackground(Graphics2D g2d) {
        g2d.setColor(getBackground());
        g2d.fillRect(0, 0, getWidth(), getHeight());
    }

    /**
     * This method performs the actual drawing of the image. It receives the
     * ImageSet. An ImageSet depicts the folder under which the tile rows and
     * columns are located. This method loads any tiles included in the image
     * set that have not been already loaded into the cache.
     *
     * @param Graphics2D the graphics device context on which to draw
     * @param imageSet   The information pertaining to the tiled images.
     */
    private void drawImageSet(Graphics2D g, ImageSet imageSet) {
        if (imageSet == null) {
            // This indicates the specimen jar problem, i.e. the specimen.xml
            // and the image
            // locations are not insynch with each other !!!
            return;
        }
        Tile[] tiles = imageSet.chooseDisplayTiles(this, m_imageCache
                .getCacheSize(), getScrollDirection());
        String baseFolder = imageSet.getPath();
        BufferedImage tmpImage = null;
        for (int i = 0; i < tiles.length; i++) {
            String tilePath = baseFolder + tiles[i].getFilePath();
            BufferedImage image;
            try {
                image = getImageTile(tilePath);
            } catch (Exception e) {
                continue;
            }

            if (image != null) {
                // These lines were added to get rid of the color problems
                // *while* adjusting brightness on
                // color images. This is a temporary solution, creates a new
                // image (tmpImage) and pre-filters
                // before drawing it. The problem might have something to do
                // with incorrectly set ColorMap
                // while the tile image was read out of the file.
                // TODO: Think of a bettre fix.
                if (m_bcOp != null) {
                    if ((tmpImage == null)
                            || (tmpImage.getHeight() != image.getHeight())
                            || (tmpImage.getWidth() != image.getWidth())) {
                        tmpImage = new BufferedImage(image.getWidth(), image
                                .getHeight(), image.getType());
                    }
                    m_bcOp.filter(image, tmpImage);
                } else {
                    tmpImage = image;
                }
                if (m_edsControl == null || !m_edsControl.isBaseImageNone()) {
                    // We only draw if ther user has NOT selected "None" base
                    // image.
                    g.drawImage(tmpImage, null, tiles[i].getPosition().x,
                            tiles[i].getPosition().y);
                }
            }
        }
    }

    /**
     * This method either retrieves the requested Tile from the image cache or
     * loads it from the disk. If the tile had to be loaded from the disk
     * this method will add the loaded tile to the cache as well if the
     * updateCache is set to true. Additionally if the forceDiskLoad is set to
     * true then this mehtod will always load the tile from the disk irrespective
     * of whether it is present in the cache or not.
     *
     * @param tilePath
     * @return the requested image tile.
     */
    public BufferedImage getImageTile(String tilePath) throws Exception {

        BufferedImage imageTile = null;
        imageTile = m_imageCache.getImage(tilePath);
        if (imageTile == null) {
            // the tile is not in our cache, let's load it now.
            InputStream is = m_currentSpecimen.getInputStream(tilePath);
            imageTile = ImageIO.read(is);
            is.close();
            m_imageCache.add(tilePath, imageTile);
            imageTile = m_imageCache.getImage(tilePath);
        }
        return imageTile;
    }

    /**
     * This method takes in the controldepth and its corresponding folder name
     * and generates the base path for all the tiles contained in the given
     * folder that corresponds to the control whose depth is controlDepth.
     * For example if our control hierarchy is
     * <ul><li>AFM</li>
     * <ul><li>COLORMAP</li>
     * <ul><li>FOCUS</li>
     * <ul><li>MAGNIFICATION</li>
     * </ul></ul></ul></ul>
     * <p>
     * then the general format of the base path would be
     * a/c/f/m. Where 'a' is the folder corresponding to the
     * selected AFM option, 'c' is the folder corresponding to the
     * selected COLORMAP option so on...
     * If the currentTilePath is 0/0/1/2/ and the controlDepth is '2' (representing
     * COLORMAP) and the folderName is 3 then this method will return 0/3/1/2/
     * <p>
     * The main usage of this method is to find out the tilepath for tiles that are
     * not currently loaded.
     *
     * @param currentTilePath the base tile path corresponding to the currently loaded image.
     * @param controlDepth    the depth for the control. This will help us locate the position in the
     *                        tilepath which will be updated.
     * @param folderName
     * @return the tile path for the specified control and folder name.
     */
    public String getTilePath(String currentTilePath, int controlDepth, String folderName) {

        String tilePath = currentTilePath;
        int startIndex = 0;
        int endIndex = 0;
        int depth = 0;
        for (int j = 0; j < tilePath.length(); j++) {
            if (tilePath.charAt(j) == Constants.PATH_SEPARATOR.charAt(0)) {
                depth++;
                if (depth == (controlDepth - 1)) {
                    startIndex = j;
                    endIndex = tilePath.indexOf(Constants.PATH_SEPARATOR, startIndex + 1);
                    break;
                }
            }
        }
        if (startIndex < 0)
            throw new IllegalArgumentException("Invalid Tile Path. Could not locate any file separator");
        if (endIndex < 0) {
            throw new IllegalArgumentException("Invalid Tile Path. Could not locate the trailing " + Constants.PATH_SEPARATOR);
        }
        tilePath = currentTilePath.substring(0, startIndex + 1) + folderName + currentTilePath.substring(endIndex);
        return tilePath;
    }

    /**
     * This method returns the base image tile size of the tile specified by the
     * path.
     *
     * @param path
     * @return
     */
    public Point2D.Double getTileSize(String path) {
        if (m_imageCache.getImage(path) == null) {
            return new Point2D.Double(m_currentImageSet.getTileSize()
                    .getWidth(), m_currentImageSet.getTileSize().getWidth());
        } else
            return new Point2D.Double(m_imageCache.getImage(path).getWidth(),
                    m_imageCache.getImage(path).getHeight());

    }

    /**
     * This method returns the EDS control if available null otherwise. This is
     * primarily used while unloading the specimen, since we need to close all
     * the graph windows at that point.
     *
     * @return EDSMenu
     */
    public EDSMenu getEDSControl() {
        return m_edsControl;
    }

    /**
     * This method sets the eds control. This method should be called from
     * within the EDSMenu initViewcontrol() method. Through this method the
     * EDSMenu will register itself with the MicroscopeView which will be the
     * starting point of trigerring the EDS specific functionality throughout
     * the application.
     *
     * @param cntrl
     */
    public void setEDSControl(EDSMenu cntrl) {
        m_edsControl = cntrl;
        m_imageCache.readjustCacheSize(cntrl != null);
        // half the cache size to make room for the Overlay chache.
    }

    public void setNavigatorControl(NavigationThumbnail cntrl) {
        m_navigator = cntrl;
    }

    /**
     * This method draws the overlay image. This functionality is specific to
     * EDS.
     *
     * @param g Graphics2D
     */
    public void drawOverlayImage(Graphics2D g2d, boolean isBaseImageNone) {
        // Opacity should be disabled if the base image is set to "None".
        m_edsControl.enableOpacity(!isBaseImageNone);
        if (m_edsControl.getOpacity() > 0 || isBaseImageNone) {
            // Only draw if the user has selected a non-zero opacity vallue,
            // unless the base image
            // is "None" in which case the opacity value is irrelevant.
            Vector vt = m_edsControl.getSelectedOverlayElements();
            Composite origCom = null;
            if (vt.size() == 0) {
                // All the overlay elements have been de-selected. No need to
                // draw
                // the overlay image at all.
                m_edsControl.resetDirtyFlag();
                return;
            }
            // Its going to be a while for the images to be repainted so we show
            // the wait cursor.
            // Since the images are repainted usually when the user does some
            // action in the side panel
            // we shall change the side panel cursor.
            Cursor oldCursor = m_microscope.getSidePanel().getCursor();
            m_microscope.getSidePanel().setCursor(
                    Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            if (!isBaseImageNone) {
                // A valid base image is selected. if we apply opacity for a
                // "None" base image
                // under such conditions the whole image gets darker since havig
                // no base image
                // means having an ALL black base image so under such a case we
                // shall
                // ignore opacity and simply draw the overlay images. Otherwise
                // we shall apply
                // opacity.
                origCom = g2d.getComposite();
                g2d.setComposite(AlphaComposite.getInstance(
                        AlphaComposite.SRC_OVER, m_edsControl.getOpacity()));
            }
            m_edsControl.m_overlayImageHandler.drawOverlayImageSet(
                    m_currentImageSet, m_currentSpecimen, g2d);
            m_microscope.getSidePanel().setCursor(oldCursor);
            // reverting back the original opacity level so that the rest of the
            // drawn objects are not
            // affected by the EDS opacity.
            if (!isBaseImageNone)
                g2d.setComposite(origCom);
        }
    }

    public void setBrightnessAndContrast(double newBright, double newCon) {
        m_brightness = newBright;
        m_contrast = newCon;
        updateLookupOp();
    }

    /**
     * returns the tile pointed to by the tilePath. Null if the tile is not
     * available in the cache.
     *
     * @param tilePath the unique ID for the tile e.g. 0/0/0/1_0.jpg
     * @return the required tile or null if the tile is not available in the cache.
     */
    public BufferedImage getTileFromImageCache(String tilePath) {
        return (m_imageCache.getImage(tilePath));
    }

    private void updateLookupOp() {
        if (m_brightnessIsAdjusting || m_contrastIsAdjusting) { // don't refetch
            // images from
            // the cache;
            // just adjust
            // locally
            m_bcOp = createLookupOp(m_brightness, m_contrast);
            repaint();
        } else { // adjust images in cache so we can move around quickly
            m_imageCache.setImageOp(createLookupOp(m_brightness, m_contrast));
        }
    }

    /**
     * Makes changing brightness and contrast fast by using a lookup table (op)
     *
     * @param brightness Must be [0, 1]
     * @param contrast   Must be [0, 1]
     * @return The lookup op we create.
     */
    private LookupOp createLookupOp(double brightness, double contrast) {
        if (brightness == 0.5 && contrast == 0.5) {
            return null;
        }

        int win = (int) ((2.0 * (1.0 - contrast)) * 255.0);
        int lev = (int) ((1.0 - brightness) * 255.0);

        int windowStart = lev - win / 2;
        int windowEnd = lev + (win + 1) / 2;

        byte lut[] = new byte[256];
        double windowMappingRatio = 256.0 / (double) win;

        int windowStartI = Math.max(windowStart, 0);
        int windowEndI = Math.min(windowEnd, 256);

        for (int i = 0; i < windowStartI; ++i) {
            lut[i] = (byte) 0;
        }
        for (int i = windowStartI; i < windowEndI; ++i) {
            lut[i] = (byte) ((i - windowStart) * windowMappingRatio);
        }
        for (int i = windowEndI; i < 256; ++i) {
            lut[i] = (byte) 255;
        }

        return new LookupOp(new ByteLookupTable(0, lut), null);
    }

    /**
     * This methos calculates the normlized coordinated of the current mouse
     * position. The coordinates are normalized according to the specimen
     * distance measurements. These readouts are then shown at the bottom left
     * of the screen. If the specimen.xml does not specify the specimen distance
     * we shall show 0 as the readout.
     *
     * @param g
     * @param imageSet
     */
    private void drawPositionReadout(Graphics2D g, ImageSet imageSet) {
        if (imageSet == null) {
            return;
        }

        double x = m_mousePosition.x - m_stagePosition.x;
        double y = m_mousePosition.y - m_stagePosition.y;
        String xs = MeasureControlBase.computeReadout(x, m_currentImageSet
                .getPixelSize().getX());
        String ys = MeasureControlBase.computeReadout(y, m_currentImageSet
                .getPixelSize().getY());

        String readout = "X: " + xs + "  Y: " + ys;
        g.setColor(Color.WHITE);
        g.drawString(readout, 5, getHeight() - 5);
    }

    public void addControl(Control c) {
        if (c == null) {
            return;
        }

        m_controlsToAdd.add(c);
    }

    public void removeControl(Control c) {
        if (c == null) {
            return;
        }

        m_controlsToDelete.add(c);
    }

    /**
     * This method clears the image cache.
     */
    public void clearBaseImageCache() {
        m_imageCache.clear();
    }

    private void updateTransientControls() {
        Iterator controlsIter = m_controlsToAdd.iterator();
        while (controlsIter.hasNext()) {
            m_viewControls.add(controlsIter.next());
        }
        m_controlsToAdd.clear();

        controlsIter = m_controlsToDelete.iterator();
        while (controlsIter.hasNext()) {
            m_viewControls.remove(controlsIter.next());
        }
        m_controlsToDelete.clear();
    }

    /**
     * This class acts as the KeyEventListener for the container MouseView
     * class. The primary reason for implementing this class was to Pre-Listen
     * to key events. There was a strong need to pre-listen for key events, in
     * case we want to map certain key events to specimen changes e.g. pressing +
     * should zoom in and pressing - should zoom out. Such events, in the
     * absence of pre-listener, could only be handled if the focus is on the
     * specimen window. Once the user focuses any control in the side panel
     * these key events never got passed to the specimen window. By implementing
     * this Pre-Listening logic the specimen window becomes the first to receive
     * these events and once it does it then redispatches it to other controls.
     *
     * @extents MouseInputAdapter
     * @implements ActionListener, MouseWheelListener
     */
    class KeyEventPreListener implements java.awt.KeyEventDispatcher {
        public boolean dispatchKeyEvent(KeyEvent e) {

            // Microscope View has appropriate key event handlers
            // so we shall not bother about handling the key events
            // generated by the Microscope View. Our goal is to capture
            // some specific events that gets generated in the side panel
            // and don't get passed on to the MicroscopeView. We have
            // to be very specific with our if condition, we can
            // effectivly sink all the key events otherwise.
            // The FileChooser pops up when loading/saving Annotations
            // if we donot put this additional check then pressing +/- on
            // the chooser may initiaite the zoom in/ zoom out functionality.
            // The escape event is used by tools that have the concept of
            // persistent mode. Upon receiving the Escape event they should switch
            // off their persistent mode.
            if (!(e.getSource() instanceof MicroscopeView)
                    && (e.getSource().getClass().getName().indexOf("FileChooser") < 0)
                    && (e.getKeyChar() == '+' || e.getKeyChar() == '-') || e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                MicroscopeView.this.m_keyboardFocusManager.redispatchEvent(
                        MicroscopeView.this.m_microscopeView, e);
                if (e.isConsumed()) {
                    // no one else should consume this event.
                    return true;
                }
            }
            // Other controls should consume this event.
            return false;
        }
    }

    /**
     * This class acts as the event listener for the container MouseView class.
     * This class embeds all the message listening logic for the mouseview
     * class.
     *
     * @extents MouseInputAdapter
     * @implements ActionListener, MouseWheelListener
     */
    class EventListener extends MouseInputAdapter implements ActionListener, MouseWheelListener {
        private boolean isScrollInProgress = false;
        private Point m_pressPoint;
        private Point2D.Double m_velocity;
        private Date m_timeOfPreviousUpdate;
        private Timer m_timer;

        public EventListener() {
            m_velocity = new Point2D.Double(0, 0);
            m_timeOfPreviousUpdate = new Date();
            m_timer = new Timer(10, this);
        }

        public void stopScroll() {
            if (isScrollInProgress) {
                m_timer.stop();
                isScrollInProgress = false;
            }
        }

        /**
         * This method handles the mousewheel events. The method gets called
         * everytime a mousewheel is moved on the image. This method dispatches
         * the mousehweel event to all the controls contained in the MouseView
         * class. This way any control that is interested in handling the event
         * can easily do as it likes. Once the done the control would probably
         * issue a consumed() to indicate that the mousewheel event has been
         * used up and further processing is not required by anyother control.
         *
         * @param MouseWheelEvent
         */
        public void mouseWheelMoved(MouseWheelEvent e) {
            Iterator controlsIter = MicroscopeView.this.m_viewControls.iterator();

            while (controlsIter.hasNext()) { // Dispatch mouse wheel moved
                // event to the appropriate
                // image controls
                ((ViewControl) controlsIter.next()).mouseWheelMoved(e);
            }
        }

        public void simulateMousePressed(KeyEvent e) {
            int key = e.getKeyCode();
            m_pressPoint = new Point(MicroscopeView.this.getSize().width / 2,
                    MicroscopeView.this.getSize().height / 2);
            if (key == KeyEvent.VK_RIGHT)
                m_pressPoint.x += MicroscopeView.this.m_microscope.getConfig()
                        .getNavigationSpeedAsInt() * 50;
            else if (key == KeyEvent.VK_LEFT)
                m_pressPoint.x -= MicroscopeView.this.m_microscope.getConfig()
                        .getNavigationSpeedAsInt() * 50;
            else if (key == KeyEvent.VK_UP)
                m_pressPoint.y -= MicroscopeView.this.m_microscope.getConfig()
                        .getNavigationSpeedAsInt() * 50;
            else if (key == KeyEvent.VK_DOWN)
                m_pressPoint.y += MicroscopeView.this.m_microscope.getConfig()
                        .getNavigationSpeedAsInt() * 50;
            if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT
                    || key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
                isScrollInProgress = true;
                m_velocity.x = m_pressPoint.x - 0.5
                        * MicroscopeView.this.getSize().width;
                m_velocity.y = m_pressPoint.y - 0.5
                        * MicroscopeView.this.getSize().height;
                setScrollDirection(calculateScrollDirection());
                m_timeOfPreviousUpdate = new Date();
                m_timer.restart();
            } else {
                m_timer.stop();
            }
            e.consume();
        }

        public void mousePressed(MouseEvent e) {
            // If alt is not pressed pass events to view controls
            if (!e.isAltDown()) {
                Iterator controlsIter = MicroscopeView.this.m_viewControls
                        .iterator();

                // We shall first dispatch the events to the distance control,
                // since we
                // want them to consume the events before the annotation
                // controls take over.
                // This allows the distance control to be dragged around even
                // when they
                // overlap with an annotation control.
                while (controlsIter.hasNext()) {
                    Object o = controlsIter.next();
                    if (o instanceof DistanceMeasure) {
                        ((DistanceMeasure) o).mousePressed(e);
                    }
                }

                // If the distance control did not consume the event then
                // dispatch it to others.
                if (!e.isConsumed()) {
                    controlsIter = MicroscopeView.this.m_viewControls
                            .iterator();
                    while (controlsIter.hasNext()) { // Dispatch mouse
                        // pressed event to the
                        // appropriate image
                        // controls
                        ((ViewControl) controlsIter.next()).mousePressed(e);
                    }
                }
            }

            if (!e.isConsumed() && SwingUtilities.isLeftMouseButton(e)) {
                e.consume();
                m_pressPoint = e.getPoint();
                // System.out.println(m_pressPoint);
                computeVelocity(e);
                setScrollDirection(calculateScrollDirection());
                m_timeOfPreviousUpdate = new Date();
                m_timer.restart();
            }
        }

        public void mouseReleased(MouseEvent e) {
            // Ask for focus for MicroscopeView
            requestFocusInWindow();

            // If alt is not pressed pass events to view controls
            if (!e.isAltDown()) {
                Iterator controlsIter = MicroscopeView.this.m_viewControls
                        .iterator();

                while (controlsIter.hasNext()) {
                    // Dispatch mouse released event to the appropriate image
                    // controls
                    ((ViewControl) controlsIter.next()).mouseReleased(e);
                }
            }

            if (!e.isConsumed() && SwingUtilities.isLeftMouseButton(e)) {
                m_pressPoint = null;
                m_timer.stop();
            }
        }

        public void mouseDragged(MouseEvent e) {
            m_mousePosition.setLocation(e.getX(), e.getY());
            // repaint() here gives "show while dragging affect" for
            // annotations.
            repaint();

            // If alt is not pressed pass events to view controls
            if (!e.isAltDown()) {
                Iterator controlsIter = MicroscopeView.this.m_viewControls
                        .iterator();
                while (controlsIter.hasNext() && !e.isConsumed()) { // Dispatch mouse dragged
                    // event to the appropriate
                    // image controls
                    ((ViewControl) controlsIter.next()).mouseDragged(e);
                }
            }

            if (!e.isConsumed() && SwingUtilities.isLeftMouseButton(e)) {
                if (m_pressPoint != null) {
                    e.consume();
                    computeVelocity(e);
                }
            }
        }

        public void mouseClicked(MouseEvent e) {
            // Ask for focus for MicroscopeView
            requestFocusInWindow();

            // If alt is not pressed pass events to view controls
            if (!e.isAltDown()) {
                Iterator controlsIter = MicroscopeView.this.m_viewControls
                        .iterator();
                while (controlsIter.hasNext()) { // Dispatch mouse clicked
                    // event to the appropriate
                    // image controls
                    ((ViewControl) controlsIter.next()).mouseClicked(e);
                }
            }
        }

        private void computeVelocity(MouseEvent e) {
            m_velocity.x = e.getX() - 0.5 * MicroscopeView.this.getSize().width;
            m_velocity.y = e.getY() - 0.5
                    * MicroscopeView.this.getSize().height;
        }

        /**
         * This method checks the direction in which the specimen is being
         * scrolled. We figure out the scroll direction by seeing the position
         * of the mouse click w.r.t. the screen center.
         *
         * @return the direction of scroll (north, east, west, south,
         * northeast....)
         */
        public short calculateScrollDirection() {
            int centerX = getBeamSize().width / 2;
            int centerY = getBeamSize().height / 2;
            short retValue = Constants.NONE;
            // Mouse clicked towards the right of the screen center.
            if (m_pressPoint.x > centerX)
                if (m_pressPoint.y > centerY)
                    retValue = Constants.SOUTHEAST;
                else if (centerY > m_pressPoint.y)
                    retValue = Constants.NORTHEAST;
                else
                    retValue = Constants.EAST;
                // User clicked to the left of the screen center
            else if (m_pressPoint.x < centerX)
                if (m_pressPoint.y > centerY)
                    retValue = Constants.SOUTHWEST;
                else if (centerY > m_pressPoint.y)
                    retValue = Constants.NORTHWEST;
                else
                    retValue = Constants.WEST;
                // User clicked right on the Y-Axis.
            else if (m_pressPoint.y > centerY)
                retValue = Constants.NORTH;
            else if (centerY > m_pressPoint.y)
                retValue = Constants.SOUTH;
            // System.out.println("Scroll diretion " + retValue);
            return retValue;
        }

        public void mouseMoved(MouseEvent e) {
            m_mousePosition.setLocation(e.getX(), e.getY());
            repaint();

            // If alt is not pressed pass events to view controls
            if (!e.isAltDown()) {
                Iterator controlsIter = MicroscopeView.this.m_viewControls
                        .iterator();
                while (controlsIter.hasNext()) {
                    ((ViewControl) controlsIter.next()).mouseMoved(e);
                }
            }

            if (!e.isConsumed()) {
                super.mouseMoved(e);
            }
        }

        private boolean isImageVisibleAtX(double x) {
            ImageSet imageSet = MicroscopeView.this.m_currentImageSet;
            if (imageSet == null)
                return false;

            return !(x >= 0.5 * MicroscopeView.this.m_beamSize.width || (x
                    + imageSet.getImageSize().width <= 0.5 * MicroscopeView.this.m_beamSize.width));
        }

        private boolean isImageVisibleAtY(double y) {
            ImageSet imageSet = MicroscopeView.this.m_currentImageSet;
            if (imageSet == null)
                return false;

            return !(y >= 0.5 * MicroscopeView.this.m_beamSize.height || (y
                    + imageSet.getImageSize().height <= 0.5 * MicroscopeView.this.m_beamSize.height));
        }

        public void actionPerformed(ActionEvent e) {
            if (e.getSource().equals(m_timer)) {
                performTimerAction(e);
            }
        }

        /**
         * This method acts as a call back for the timer. We start the timer
         * when the base image needs to be scrolled upon mouse pressed. This is
         * the method which gets called everytime the timer event is fired. The
         * frequency of the timer expiry depends on the scroll speed. This
         * method updates the stage position for the base image which then
         * results in redrawing of the image (done in the paintComponent
         * method).
         *
         * @param e
         */
        private void performTimerAction(ActionEvent e) {
            if (MicroscopeView.this.m_currentImageSet == null)
                return;

            Date now = new Date();
            double deltaTime = 0.001 * (now.getTime() - m_timeOfPreviousUpdate
                    .getTime());
            m_timeOfPreviousUpdate = now;

            double scale = 0.8;
            Point2D.Double newPosition = new Point2D.Double(
                    MicroscopeView.this.m_stagePosition.x
                            - (scale * m_velocity.x * deltaTime),
                    MicroscopeView.this.m_stagePosition.y
                            - (scale * m_velocity.y * deltaTime));
            if (isImageVisibleAtX(newPosition.x)) {
                MicroscopeView.this.m_stagePosition.x = newPosition.x;
                MicroscopeView.this.repaint();
            }
            if (isImageVisibleAtY(newPosition.y)) {
                MicroscopeView.this.m_stagePosition.y = newPosition.y;
                MicroscopeView.this.repaint();
            }
        }
    }

    class DisplayListener extends ComponentAdapter {
        public void componentShown(ComponentEvent e) {
            MicroscopeView.this.updateBeamSize((JComponent) e.getComponent());
            MicroscopeView.this.updateStageOffset();
            MicroscopeView.this.repaint();
        }

        public void componentResized(ComponentEvent e) {
            MicroscopeView.this.updateBeamSize((JComponent) e.getComponent());
            MicroscopeView.this.updateStageOffset();
            MicroscopeView.this.repaint();
        }
    }

    /**
     * Is registered with MicroscopeView. Dispatches KeyEvents to specimen
     * controls.
     */
    class KeyboardListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            // Attempt to pass the event onto specimen controls
            Iterator controlsIter = MicroscopeView.this.m_viewControls.iterator();

            if (e.isAltDown()) {
                e.consume();
            }
            if (e.isControlDown() && e.getKeyCode() == KeyEvent.VK_F4) {
                // Unload the specimen.
                e.consume();
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        m_microscope.m_coreApplication.unloadSpecimen();
                    }
                });
            }

            // We shall first dispatch the events to the distance control, since
            // we
            // want them to consume the events before the annotation controls
            // take over.
            // This allows the distance control to be manipulated even when they
            // overlap with an annotation control.
            while (controlsIter.hasNext()) {
                Object o = controlsIter.next();
                if (o instanceof DistanceMeasure) {
                    ((DistanceMeasure) o).keyPressed(e);
                }
            }

            // If the distance control did not consume the event then dispatch
            // it to others.
            controlsIter = MicroscopeView.this.m_viewControls.iterator();
            while (controlsIter.hasNext() && !e.isConsumed()) {
                ((ViewControl) controlsIter.next()).keyPressed(e);
            }
            if (!e.isConsumed()) {
                // May be navigate the specimen using the arrow keys now.
                MicroscopeView.this.m_eventListener.simulateMousePressed(e);
            }
        }

        public void keyReleased(KeyEvent e) {
            // If we were scrolling the speimen view using the
            // arrow keys then stop doing it.
            MicroscopeView.this.m_eventListener.stopScroll();
        }

        /**
         * Handles the key typed events. We handle the printable character in
         * the KeyTyped event handler and others in the KeyPressed()
         *
         * @param e KeyEvent
         */
        public void keyTyped(KeyEvent e) {
            // Attempt to pass the event onto specimen controls
            Iterator controlsIter = MicroscopeView.this.m_viewControls
                    .iterator();
            while (controlsIter.hasNext() && !e.isConsumed()) {
                Object obj = controlsIter.next();
                if (!(obj instanceof TextMenu)) {
                    // this is done to allow the Annotations to consume the
                    // key types before the TextMenu. If the user presses '+'
                    // '-' then we want it to be consumed by the annotation text
                    // fields. If we don't do this the +/- will invoke the
                    // zoom in/out functionality
                    ((ViewControl) obj).keyTyped(e);
                }
            }
            if (!e.isConsumed() && (e.getKeyChar() == '+' || e.getKeyChar() == '-')) {
                Iterator textIt = MicroscopeView.this.m_viewControls.iterator();
                while (textIt.hasNext() && !e.isConsumed()) {
                    Object obj = textIt.next();
                    if (obj instanceof TextMenu) {
                        // this is done to allow the Annotations to consume the
                        // key types before the TextMenu. If the user presses
                        // '+'
                        // '-' then we want it to be consumed by the annotation
                        // text
                        // fields. If we don't do this the +/- will invoke the
                        // zoom in/out functionality
                        ((ViewControl) obj).keyTyped(e);
                    }
                }

            }
        }
    }
}