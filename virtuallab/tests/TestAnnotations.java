/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
* The project uses the libraries jogl, jfreechart, jcommon and w3c. You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.tests;

import java.io.File;
import java.io.FileWriter;

import virtuallab.gui.controls.annotation.AnnotationFileComboBox;
import virtuallab.gui.controls.annotation.SpecimenAnnotationFile;
import junit.framework.TestCase;

/**
 * Test cases for Annotation related functionality
 *
 */
public class TestAnnotations extends TestCase {

    /**
     * This method tests the proper loading of the short description and
     * specimen id from annotation xml file.
     *
     * @throws Exception
     */
    public void testAnnotationShortDescriptionLoading() throws Exception {

        String annotationXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Annotations><SpecimenUniqueName Name=\"20060801132945\"/>" +
                "<ShortDescription><Text>annotations done by Chas</Text></ShortDescription>" +
                "<Annotation><Color RGB=\"-1\"/>" +
                "<TextColor RGB=\"-1\"/>" +
                "<Font Name=\"Serif\"/><Line Width=\"1.0\"/>" +
                "<Shape><Segment Type=\"SEG_MOVETO\">" +
                "<Location X=\"455.0\" Y=\"312.0\"/>" +
                "</Segment><Segment Type=\"SEG_LINETO\">" +
                "<Location X=\"500.0\" Y=\"346.0\"/></Segment>" +
                "</Shape><ImageDimensions Height=\"667\" Width=\"740\"/>" +
                "<TextOffset X=\"0.0\" Y=\"0.0\"/></Annotation>" +
                "</Annotations>";
        File output = new File("dummy.xml");
        FileWriter writer = new FileWriter(output);
        writer.write(annotationXML);
        writer.close();
        SpecimenAnnotationFile annotationFile = new SpecimenAnnotationFile(output);
        assertEquals("annotations done by Chas", annotationFile.getShortDescription());
        assertEquals("20060801132945", annotationFile.getSpecimenID());
        output.delete();
    }

    /**
     * This method tests whether all the annotation files specific to a specimen are
     * properly loaded or not
     *
     * @throws Exception
     */
    public void testSpecimenAnnotationFilesLoading() throws Exception {
        String annotationXML1 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Annotations><SpecimenUniqueName Name=\"1\"/>" +
                "<ShortDescription><Text>annotations Number 1</Text></ShortDescription>" +
                "<Annotation><Color RGB=\"-1\"/>" +
                "<TextColor RGB=\"-1\"/>" +
                "<Font Name=\"Serif\"/><Line Width=\"1.0\"/>" +
                "<Shape><Segment Type=\"SEG_MOVETO\">" +
                "<Location X=\"455.0\" Y=\"312.0\"/>" +
                "</Segment><Segment Type=\"SEG_LINETO\">" +
                "<Location X=\"500.0\" Y=\"346.0\"/></Segment>" +
                "</Shape><ImageDimensions Height=\"667\" Width=\"740\"/>" +
                "<TextOffset X=\"0.0\" Y=\"0.0\"/></Annotation>" +
                "</Annotations>";
        File output1 = new File("c:/temp/dummy1.xml");
        FileWriter writer = new FileWriter(output1);
        writer.write(annotationXML1);
        writer.close();
        String annotationXML2 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Annotations><SpecimenUniqueName Name=\"1\"/>" +
                "<ShortDescription><Text>annotations Number 2</Text></ShortDescription>" +
                "<Annotation><Color RGB=\"-1\"/>" +
                "<TextColor RGB=\"-1\"/>" +
                "<Font Name=\"Serif\"/><Line Width=\"1.0\"/>" +
                "<Shape><Segment Type=\"SEG_MOVETO\">" +
                "<Location X=\"455.0\" Y=\"312.0\"/>" +
                "</Segment><Segment Type=\"SEG_LINETO\">" +
                "<Location X=\"500.0\" Y=\"346.0\"/></Segment>" +
                "</Shape><ImageDimensions Height=\"667\" Width=\"740\"/>" +
                "<TextOffset X=\"0.0\" Y=\"0.0\"/></Annotation>" +
                "</Annotations>";
        File output2 = new File("c:/temp/dummy2.xml");
        writer = new FileWriter(output2);
        writer.write(annotationXML2);
        writer.close();
        String annotationXML3 = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<Annotations><SpecimenUniqueName Name=\"2\"/>" +
                "<ShortDescription><Text>annotations Number 3</Text></ShortDescription>" +
                "<Annotation><Color RGB=\"-1\"/>" +
                "<TextColor RGB=\"-1\"/>" +
                "<Font Name=\"Serif\"/><Line Width=\"1.0\"/>" +
                "<Shape><Segment Type=\"SEG_MOVETO\">" +
                "<Location X=\"455.0\" Y=\"312.0\"/>" +
                "</Segment><Segment Type=\"SEG_LINETO\">" +
                "<Location X=\"500.0\" Y=\"346.0\"/></Segment>" +
                "</Shape><ImageDimensions Height=\"667\" Width=\"740\"/>" +
                "<TextOffset X=\"0.0\" Y=\"0.0\"/></Annotation>" +
                "</Annotations>";
        File output3 = new File("c:/temp/dummy3.xml");
        writer = new FileWriter(output3);
        writer.write(annotationXML3);
        writer.close();
        AnnotationFileComboBox annotations = new AnnotationFileComboBox(null, "c:/temp", "1");
        SpecimenAnnotationFile[] specimenAnnotationFiles = annotations.getSpecimenAnnotationFiles();
        assertEquals(specimenAnnotationFiles.length, 2);
        SpecimenAnnotationFile annotation = specimenAnnotationFiles[0];
        assertEquals(annotation.getSpecimenID(), "1");
        assertEquals(annotation.getShortDescription(), "annotations Number 1");
        annotation = specimenAnnotationFiles[1];
        assertEquals(annotation.getSpecimenID(), "1");
        assertEquals(annotation.getShortDescription(), "annotations Number 2");

        annotations = new AnnotationFileComboBox(null, "c:/temp", "2");
        specimenAnnotationFiles = annotations.getSpecimenAnnotationFiles();
        assertEquals(specimenAnnotationFiles.length, 1);
        annotation = specimenAnnotationFiles[0];
        assertEquals(annotation.getSpecimenID(), "2");
        assertEquals(annotation.getShortDescription(), "annotations Number 3");
        output1.delete();
        output2.delete();
        output3.delete();
    }
}