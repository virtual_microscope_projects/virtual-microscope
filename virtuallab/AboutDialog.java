/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URL;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

/**
 * The Application About dialog box.
 */
class AboutDialog extends JFrame implements MouseListener {

    private static final Log log = new Log(AboutDialog.class.getName());
    /**
     *
     */
    private static final long serialVersionUID = 3958571740413940913L;
    /**
     * Holds the original cursor before the mouse entered the label. Used to
     * revert back to it when the mouse leaves the label
     */
    private Cursor m_oldCur = null;

    public AboutDialog(JFrame owner) {
        setTitle( (Locale.About(Configuration.getApplicationLanguage())) + " Virtual Microscope");
        try {
            URL url = getClass().getResource(Constants.APPLICATION_ICON);
            Image image = ImageIO.read(url);
            this.setIconImage(image);
        } catch (Exception e) {
            log.error("Could not find the application icon:" + Constants.APPLICATION_ICON);
            log.error(e.getMessage());
        }

        //Close button
        JPanel bttnPanel = new JPanel();
        bttnPanel.setBackground(Color.WHITE);
        JButton bttnClose = new JButton(Locale.Close(Configuration.getApplicationLanguage()));

        bttnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        bttnPanel.add(bttnClose);
        Font labelFont = new Font("Verdana", Font.PLAIN, 14);

        //Credits
        JLabel credits = new JLabel();
        credits.setFont(labelFont);
        credits.setText("<html><b>"+Locale.CreditsToOriginalDevelopers(Configuration.getApplicationLanguage())+":</b><br> " +
                Locale.ThisIsAFork(Configuration.getApplicationLanguage())+".<br> " +
                Locale.SupporitingMaterials(Configuration.getApplicationLanguage())+".<br> " +
                Locale.MoreInformation(Configuration.getApplicationLanguage()));

        JLabel contributorCredits = new JLabel();
        contributorCredits.setFont(labelFont);
        contributorCredits.setText("<html><b>"+Locale.CreditsToContributors(Configuration.getApplicationLanguage())+":</b><br> " +
                Locale.CreditForLogo(Configuration.getApplicationLanguage()) + "<br> " + Constants.ACADEMIX_HOME_PAGE);

        // The panel containing the credits
        JPanel creditsPanel = new JPanel();
        creditsPanel.setLayout(new BoxLayout(creditsPanel, BoxLayout.PAGE_AXIS));
        creditsPanel.setBackground(Color.WHITE);
        creditsPanel.add(Box.createRigidArea(new Dimension(14, 0)));
        creditsPanel.add(credits);
        creditsPanel.add(Box.createRigidArea(new Dimension(14, 14)));
        creditsPanel.add(contributorCredits);
        creditsPanel.add(Box.createRigidArea(new Dimension(0, 20)));

        // The Version information
        JLabel versionLabel = new JLabel();
        versionLabel.setFont(labelFont);
        versionLabel.setText("<html><b>"+Locale.Version(Configuration.getApplicationLanguage())+":</b> " + Version.getVersionNumber() + ", "+Locale.BuildOn(Configuration.getApplicationLanguage())+ Constants.SPACE+ Version.getBuiltDate());

        // The panel containing the version and the hyperlink.
        JPanel versionPanel = new JPanel();
        versionPanel.setLayout(new BoxLayout(versionPanel, BoxLayout.PAGE_AXIS));
        versionPanel.setBackground(Color.WHITE);
        versionPanel.add(Box.createRigidArea(new Dimension(14, 0)));
        versionPanel.add(versionLabel);
        versionPanel.add(Box.createRigidArea(new Dimension(0, 10)));

        //Stuffing all into the About dialog box.
        getContentPane().add(versionPanel, BorderLayout.NORTH);
        getContentPane().add(bttnPanel, BorderLayout.SOUTH);
        getContentPane().add(creditsPanel, BorderLayout.CENTER);

        int windowWidth = 650;
        int windowHeight = 300;
        getContentPane().setPreferredSize(new Dimension(windowWidth, windowHeight));

        setResizable(true);
        pack();
        setLocationRelativeTo(owner);
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        m_oldCur = getCursor();
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    public void mouseExited(MouseEvent e) {
        setCursor(m_oldCur);
    }
}
