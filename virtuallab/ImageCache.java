/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.awt.image.BufferedImage;
import java.awt.image.LookupOp;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import virtuallab.util.Constants;

// $Id: ImageCache.java,v 1.9 2006/09/05 01:08:47 manzoor2 Exp $

public class ImageCache {


    /**
     * The size of the cache
     */
    private int m_hiWater;
    /**
     * Whenever we max out the cache we shall shed off tiles from the cache until
     * the cache size reaches the m_loWater.
     */
    private int m_loWater;
    /**
     * The hashmap which acts as our cache.
     */
    private HashMap m_entries;
    private LookupOp m_imageOp = null;

    /**
     * The constructor that takes in the cache size as the input value. The resulting
     * cache size will be set as per the input value. The loWater will be set as half
     * the cache size.
     *
     * @param cacheSize
     */
    public ImageCache(int cacheSize) {
        m_hiWater = cacheSize;
        m_loWater = m_hiWater / 2;
        m_entries = new HashMap(m_hiWater);
    }

    /**
     * The constructor that automatically sets a reasonable cache size for the base image.
     * Since we have two imag sources (1) the base image which is always present (2) the
     * overlay image which may or may not be present. Presence of EDS overlay image functionality
     * will require us to leave some memory for EDS - hence in such a case we reduce the
     * base image cache size.
     *
     * @param edsPresent
     */
    public ImageCache() {
        m_hiWater = getDefaultCacheSize();
//	  System.out.println("Cache size set to: " + m_hiWater);
        m_loWater = m_hiWater / 2;
        m_entries = new HashMap(m_hiWater);
    }

    /**
     * This method readjusts the image cache size if true is passed to it, else
     * it sets the cache size to default.
     *
     * @param scaleFactor the float value with which the cache size willbe multiplied.
     */
    public void readjustCacheSize(boolean isEdsPresent) {
        // Note that these settings will take affect when the next tile is
        // added to the cache.
        m_hiWater = getDefaultCacheSize();
        if (isEdsPresent) {
            m_hiWater = (int) (m_hiWater * 0.25);
        }
        m_loWater = m_hiWater / 2;
//	  System.out.println("Cache size set to " + m_hiWater);
    }

    /**
     * Sets the cache size to default value. the default value is calculated
     * using an empirical formulae that we think is suitable even in the meanest
     * of memory conditions.
     */
    public void setCacheSizeToDefault() {
        readjustCacheSize(false);
    }

    /**
     * This parameter specifies how many image tiles do we cache
     * If this is set too low then we will have lot of Disk I/O
     * if it is set too high then we may run out of Heap and get
     * OutofMemory errors. Note that this variable specifies the
     * "image tile counts" for tiles that have large size the actual
     * memory consumption could still be fairly large. However a tile will
     * usually not be larger than 512. The formulae below has been verified to
     * work well with 512 tile size. With 256 tile size the cache size can be set
     * even higher.
     * The following formula is based on the line equation with the following
     * two points (66,40) (256,100) i.e. when the max heap size is
     * 66MB (the default value) we shall set the hiWater to 40 and when the
     * max heap size is 256 we shall set it to 100. If eds tool is present we
     * should reduce the cahche size to 1/4th  of what the above formulae gives us.
     */
    public int getDefaultCacheSize() {
        return (int) ((Runtime.getRuntime().maxMemory() / 1000000) * 0.3158 + 19.16);
    }

    public void setImageOp(LookupOp newOp) {
        if (newOp == m_imageOp) {
            return;
        }

        m_imageOp = newOp;
        clear();
    }

    public boolean contains(String filePath) {
        return m_entries.containsKey(filePath);
    }

    public void add(String path, BufferedImage image) {
        if (path == null || image == null) {
            return;
        }

        if (m_entries.size() >= m_hiWater) {
            clean();
        }

        if (m_imageOp != null) {
            m_imageOp.filter(image, image); // apply m_imageOp before storing
        }

        ImageCacheEntry entry = new ImageCacheEntry(path, image);
        m_entries.put(entry.m_path, entry);
        // DEBUG CODE
        //System.out.println("Image cache size is: " + m_entries.size());
    }

    /**
     * This method is called when a side control changes its value e.g. when Mag level
     * is changed or focus is changed etc. The idea is that when the user changes the value
     * our cache mostly contains tiles that were relevant to the oldsettings.Hence what will
     * happen is that everytime the user scrolls image we will have an IO to get the new tile.
     * Ofcourse after navigating through the specimen the cache will get stable and will
     * be mostly populated with the image set pertaining to the current settings of the control.
     * This results in lot of jitters (that is when the IO is taking place). We can
     * get rid of this by preloading the image set (as much as our cache size would allow) pertaining
     * to the current controls settings.
     *
     * @param path
     */
    public void preloadImageSet(String path) {
        // Step 1: Find out how many rows of image tiles do we have.
        String folders = path.substring(0, path.lastIndexOf(Constants.PATH_SEPARATOR));
        folders = folders.substring(0, folders.lastIndexOf(Constants.PATH_SEPARATOR));
        // Now we have all the path leading up to the images.


        // Step 2: For each row of the image preload the column tiles. Keep
        // doing this until either we have preloaded everything or our
        // cache is full.

    }

    public void remove(String path) {
        if (path == null) {
            return;
        }

        ImageCacheEntry entry = (ImageCacheEntry) m_entries.get(path);
        if (entry != null) {
            m_entries.remove(path);
        }
    }

    public BufferedImage getImage(String path) {
        if (path == null) {
            return null;
        }

        ImageCacheEntry entry = (ImageCacheEntry) m_entries.get(path);
        if (entry == null) {
            return null;
        }

        entry.m_lastUsed.setTime(Calendar.getInstance().getTimeInMillis());
        return entry.m_image;
    }

    /**
     * This method empties the cache and explicitly sets the cached
     * images to null so that they can be Garbage Collected.
     */
    public void clear() {
        Iterator iter = m_entries.keySet().iterator();
        while (iter.hasNext()) {
            ImageCacheEntry etry = (ImageCacheEntry) m_entries.get(iter.next());
            // It is important to exlicitly set the cache entry to null to dereference it,
            // otherwise the garbage collector will not collect this object and soon
            // we shall run into infamous out of memory errors.
            etry.m_image = null;
            etry.m_path = null;
            etry.m_lastUsed = null;
            etry = null;
        }
        // Forcing GC everytime the cache is cleared. This will slow down the performance
        // but will make sure that we do not run out of memory.
//	  Runtime.getRuntime().gc();
        //System.out.println("Running GC ");
        //System.out.println(Runtime.getRuntime().freeMemory());
        m_entries.clear();
    }

    /**
     * This method is invoked everytime the image cache is full. We apply a
     * Least Recently used used algorithm to choose image tiles that will be
     * evicted. in other words we look at the last usage time of all the tiles
     * and the tiles for whom this time is the oldest get evicted.
     * Everytime this method is called it gets rid of m_lowWater tiles from
     * the cache.
     * TODO: This method uses the tile count to decide when to evict tiles.
     * A better way should be to use the memory usage since a smaller tile count
     * that has high resolution tiles (100KB each) would consume more memory than a higher
     * tile count with each tile being 1 KB of size.
     */
    private void clean() {
        TreeMap timeOrderedEntries = new TreeMap();
        Iterator iter = m_entries.entrySet().iterator();

        while (iter.hasNext()) {
            Map.Entry entry = (Map.Entry) iter.next();
            timeOrderedEntries.put(((ImageCacheEntry) entry.getValue()).m_lastUsed, entry.getKey());
        }

        iter = timeOrderedEntries.entrySet().iterator();
        boolean bCacheMaxed = false;
        for (int i = 0; i < (m_hiWater - m_loWater); i++) {
            if (iter.hasNext()) {
                bCacheMaxed = true;
                Map.Entry entry = (Map.Entry) iter.next();
                ImageCacheEntry etry = (ImageCacheEntry) m_entries.get(entry.getValue());
                m_entries.remove(entry.getValue());
                // It is important to exlicitly set the cache entry to null to dereference it,
                // othrwise the garbage collector will not collect this object and soon
                // we shall run into infamous out of memory errors.
                etry.m_image = null;
                etry.m_path = null;
                etry.m_lastUsed = null;
                etry = null;
            }
        }
        if (bCacheMaxed) {
            // Forcing GC everytime the cache is cleared. This will slow down the performance
            // but will make sure that we do not run out of memory.
            //Runtime.getRuntime().gc();
            //System.out.println("Running GC ");
            //System.out.println(Runtime.getRuntime().freeMemory());
        }
    }

    /**
     * This method returns the cache size - which indicates how much
     * place (in terms of number of slides) do we have to store the image
     * tiles.
     *
     * @return the image cache.
     */
    public int getCacheSize() {
        return m_hiWater;
    }

    class ImageCacheEntry {
        /**
         * The tiles image that is being cached.
         */
        BufferedImage m_image;
        /**
         * The last used Timestamp. This information is used to implement
         * the cache replacement algorithm.
         */
        Date m_lastUsed;
        /**
         * The path where the image tile is loaded from. Used as the key into
         * the cache hashmap.
         */
        String m_path;

        ImageCacheEntry(String path, BufferedImage image) {
            m_path = path;
            m_image = image;
            m_lastUsed = Calendar.getInstance().getTime();
        }
    }
}