/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;

import virtuallab.MicroscopeView;
import virtuallab.Specimen;

/**
 * This class handles the detector menu drop down. The detector control appears much like any other
 * dropdown based "TextMenu" control except that it has specific logic to add a hardcoded "None" behavior.
 * Additionally it is unique in the way it interacts with the "EDSMenu" control. Both the detector
 * and the EDS controls have specific inter control dependency which necessitated creating new controls
 * for EDS and DETECTOR. However if the specimen does not have an EDS control then this DETECTOR control
 * behaves just like any other TextMenu control.
 *
 */

public class DetectorMenu extends MenuControlBase implements ItemListener, ActionListener, ViewControl, KeyListener {
    private static final long serialVersionUID = -8389034693952791915L;
    /**
     * This variable contains the reference to the MicroscopeView object - in which this control
     * is contained.
     */
    protected MicroscopeView m_display = null;
    protected JComboBox m_comboBox;
    /**
     * This parameter determines the condition at which the "None" option will be added to the dropdown.
     * For DETECTOR control this will happen when we find another control in the specimen named "EDS".
     */
    private String m_noneTrigger = "EDS";
    /**
     * This parameter determines whether the "None" functionality is enabled or not.
     */
    private boolean m_noneEnabled = false;

    /**
     * This method updates the GUI controls contained in the Detector Menu. It
     * removes ALL the current gui controls contained in this DetectorMenu and then recreates
     * them with the new values.
     *
     * @param version the XML Version.
     */
    public void updateGUI(double version) {
        if (m_validControlStates == null) {
            return;
        }
        // Remove the action listeners if set. This MUST be done
        // othewise even after removing these components they will
        // keep on getting these events and it would be a whole
        // mess.
        if (m_comboBox != null)
            m_comboBox.removeActionListener(this);
        if (m_plusButton != null)
            m_plusButton.removeActionListener(this);
        if (m_minusButton != null)
            m_minusButton.removeActionListener(this);
        removeAll();
        setMaximumSize(new Dimension(1024, getMinimumSize().height));
        setPreferredSize(new Dimension(150, 80));

        m_comboBox = new JComboBox();
        Iterator iter = m_validControlStates.iterator();

        while (iter.hasNext()) {
            String s = "";
            if (version <= 1.0) {
                Specimen.ControlState cs = (Specimen.ControlState) iter.next();
                s = cs.getDisplayString();
            } else {
                Specimen.ControlStates cs = (Specimen.ControlStates) iter.next();
                s = cs.getDisplayString();
            }
            m_comboBox.addItem(s);
        }
        updateButtonStates();
        // Add listeners
        m_comboBox.addItemListener(this);
        m_plusButton.addActionListener(this);
        m_minusButton.addActionListener(this);
        addKeyListener(this);

        // Laying out the components.
        m_minusButton.putClientProperty("JButton.buttonType", "toolbar");
        m_plusButton.putClientProperty("JButton.buttonType", "toolbar");
        setLayout(new GridBagLayout());
        setBorder(BorderFactory.createTitledBorder(m_title));
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 2);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        //put some gap between the dropdown and the buttons.
        add(m_comboBox, c);
        add(m_minusButton);
        add(m_plusButton);
    }

    /**
     * This method selects the selected value to the value passed in as the parameter
     *
     * @param value the value that is to be selected.
     */
    public void setSelectedValue(String value) {
        m_comboBox.setSelectedItem(value);
        //m_comboBox.revalidate();
    }

    /**
     * This method cleans up the Detector object. Soon after this method is called
     * the calling method should remove this Detector from the sidePanel as well.
     */
    public void cleanUp(double xmlVersion) {
        // Remove the action listeners if set. This MUST be done
        // othewise even after removing these components they will
        // keep on getting these events and it would be a whole
        // mess.
        if (m_comboBox != null)
            m_comboBox.removeActionListener(this);
        if (m_plusButton != null)
            m_plusButton.removeActionListener(this);
        if (m_minusButton != null)
            m_minusButton.removeActionListener(this);
        removeAll();
        setVisible(false);
    }

    /**
     * Called when different menu item is selected.
     */
    public void itemStateChanged(ItemEvent e) {
        int i = m_comboBox.getSelectedIndex();
        m_currentValue = m_comboBox.getItemAt(i).toString();
        m_microscope.reCreateControls(this, m_display.getCurrentSpecimen().getXMLVersion());
        if (!m_isButtonPressed && m_comboBox.hasFocus())
            m_comboBox.grabFocus();
        m_microscope.updateSpecimenControlStates();
        m_isButtonPressed = false;
        updateButtonStates();
    }

    /**
     * Called when +/- button is pressed.
     */
    public void actionPerformed(ActionEvent e) {
        //DefaultKeyboardFocusManager.getCurrentKeyboardFocusManager().
        JButton thisButton = (JButton) e.getSource();
        int i = m_comboBox.getSelectedIndex();
        if (thisButton.equals(m_minusButton)) {
            m_isButtonPressed = true;
            m_comboBox.setSelectedIndex(i - 1);
            if (i - 1 != 0)
                thisButton.grabFocus();
            else
                // if the minus button is to be disabled we shift focus to the plus button.
                m_plusButton.grabFocus();
        } else if (thisButton.equals(m_plusButton)) {
            m_isButtonPressed = true;
            m_comboBox.setSelectedIndex(i + 1);
            if (i + 2 != m_comboBox.getItemCount())
                thisButton.grabFocus();
        }
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    /**
     * Draws the control.
     *
     * @param g2d Graphics context on which to draw the control.
     */
    public void draw(Graphics2D g2d) {
    }

    /**
     * Handles mouseEntered() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Handles mouseExited() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Handles mouseMoved() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseMoved(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    /**
     * Handles keyTyped() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyPressed(KeyEvent e) {
    }

    /**
     * Handles keyReleased() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    protected void updateButtonStates() {
        int i = m_comboBox.getSelectedIndex();

        m_minusButton.setEnabled(i != 0);
        m_plusButton.setEnabled(i != (m_comboBox.getItemCount() - 1));
    }

    /**
     * Initializes this control as a view control.
     *
     * @param controlName unique name/type of this control
     * @param display     MicroscopeView object
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        if (display == null || display.getCurrentSpecimen() == null || controlName == null) {
            throw new IllegalArgumentException("Null microscope view, specimen, or name");
        }
        m_display = display;
        if (m_controlName != null) {
            m_controlName = controlName;
        }
    }
}