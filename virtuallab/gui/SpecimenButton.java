/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.io.File;
import java.io.InputStream;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import virtuallab.Configuration;
import virtuallab.OfflineSpecimen;
import virtuallab.Specimen;
import virtuallab.util.Constants;
import virtuallab.util.Locale;
import virtuallab.util.VWrappingLabel;
/**
 * Specimen selection panel containing its thumbnail, name, and description.
 */
public class SpecimenButton extends JPanel {
    /**
     * Width of thumbnail.
     */
    protected final static int THUMB_SIZE_X = 100;
    /**
     * Height of thumbnail.
     */
    protected final static int THUMB_SIZE_Y = 100;
    /**
     * X-gap in button's layout.
     */
    protected final static int GAP_X = 10;
    /**
     * Y-gap in button's layout.
     */
    protected final static int GAP_Y = 10;
    /**
     * Width of the button.
     */
    protected final static int SIZE_X = THUMB_SIZE_X + 2 * GAP_X;
    /**
     * Height of the button.
     */
    protected final static int SIZE_Y = THUMB_SIZE_Y + 6 * GAP_Y;
    private static final long serialVersionUID = 2034366384144389770L;
    /**
     * The dimensions of the download button that shows on the offline specimen thumbnails
     */
    private final static Dimension DOWNLOAD_BUTTON_DIMENSION = new Dimension(73, 26);
    /**
     * Specimen's display string.
     */
    String m_specimenName;
    /**
     * Specimen's short description.
     */
    String m_descriptionString;
    /**
     * Button containing specimen's thumbnail.
     */
    JButton m_button;
    JLabel m_thumbnailLabel;
    JButton m_downloadButton;

    /**
     * The constructor that may be used by external classes to make use of some utility functions of
     * this class.
     *
     * @see virtuallab.update.SpecimenDownloadManagerView
     */
    public SpecimenButton(String specimenID) {
        setName(specimenID);
    }

    /**
     * Constructs a selection panel for the given specimen.
     *
     * @param aSpecimen
     */
    SpecimenButton(Specimen aSpecimen) {
        this(aSpecimen, true);
    }

    SpecimenButton(OfflineSpecimen aSpecimen) {
        this(aSpecimen, true);
    }

    SpecimenButton(OfflineSpecimen aSpecimen, boolean requireNameLabel) {
        setName(aSpecimen.getUniqueName());
        createThumbnailLabel(aSpecimen);
        if (!requireNameLabel)
            // OFFLINE Panel image thumbnails require a 1 pt. border around them.
            m_thumbnailLabel.setBorder(BorderFactory.createLineBorder(new Color(169, 169, 169)));
        displaySpecimeThumbnail(aSpecimen, requireNameLabel);
    }

    SpecimenButton(Specimen aSpecimen, boolean requireNameLabel) {
        setName(aSpecimen.getUniqueName());
        createThumbnailButton(aSpecimen);
        displaySpecimeButton(aSpecimen, requireNameLabel);
    }

    /**
     * Gets the dimensions of the Specimen Thumbnail. This may be
     * required by others that want to align themselves with the button e.g.
     * the DetailViewChooser.
     *
     * @return the dimension of the specimen Thumbnail button.
     */
    public static Dimension getThumbnailDimensions() {
        return new Dimension(THUMB_SIZE_X, THUMB_SIZE_Y);
    }

    /**
     * Gets the dimensions of the Specimen Thumbnail button. This may be
     * required by others that want to align themselves with the button e.g.
     * the DetailViewChooser. Note that this size would be greater than the
     * size of the thumbnail since we leave some margin around the thumbnail.
     *
     * @return the dimension of the specimen Thumbnail button.
     */

    public static Dimension getThumbnailButtonDimensions() {
        return new Dimension(SIZE_X, SIZE_Y + 20);
    }

    /**
     * This method creates the label for the online specimen panel.
     * This label is shown at the top left of the panel.
     *
     * @return The online specimen panel label
     */
    public JPanel createOnlineSpecimenPanelLabel(Color panelColor, String labelToShow) {
        String labelText = "<HTML><P style=\"text-align:center;font-size:15pt; font-family:Tahoma,sans-serif;\">" +
                Locale.MySpecimenCollection(Configuration.getApplicationLanguage())+"</P>" +
                "<BR><BR><P style=\"text-align:center;font-size:11pt; font-family:Tahoma,sans-serif;\">" + labelToShow +
                "</P></HTML>";
        return createPanelLabel(labelText, panelColor);
    }

    /**
     * This method creates the label for the offline specimen panel.
     * This label is shown at the top left of the panel.
     *
     * @return The offline specimen panel label
     */
    public JPanel createOfflineSpecimenPanelLabel(Color panelColor, String labelToShow) {
        String labelText = "<HTML><P style=\"color:white;text-align:center;font-size:15pt; font-family:Tahoma,sans-serif;\">" +
                Locale.GetMoreSpecimens(Configuration.getApplicationLanguage())+"</P>" +
                "<BR><BR><P style=\"color:white;text-align:center;font-size:11pt; font-family:Tahoma,sans-serif;\">" + labelToShow + "</FONT>" +
                "</P></HTML>";
        return createPanelLabel(labelText, panelColor);
    }

    /**
     * Creates the panel label for either the online or the offline panels.
     *
     * @param labelText  the text that will appear in the label
     * @param panelColor the background color of the label. This would usually match the color of the
     *                   online/offline panels.
     * @return
     */
    private JPanel createPanelLabel(String labelText, Color panelColor) {
        JLabel specimenCollectionLabel = new JLabel(labelText);
        specimenCollectionLabel.setHorizontalAlignment(JLabel.CENTER);
        specimenCollectionLabel.setVerticalAlignment(JLabel.CENTER);
        specimenCollectionLabel.setPreferredSize(new Dimension(THUMB_SIZE_X, SIZE_Y));

        JPanel labelPanel = new JPanel();
        labelPanel.setPreferredSize(new Dimension(SIZE_X, SIZE_Y));
        labelPanel.add(specimenCollectionLabel);
        labelPanel.setBackground(panelColor);
        return labelPanel;
    }

    /**
     * Displays the offline specimen thumbnail along with the name label and the
     * download button.
     *
     * @param aSpecimen
     */
    private void displaySpecimeThumbnail(OfflineSpecimen anOffLineSpecimen, boolean requireSpecimenNameLabel) {
        // Gather display strilng and description
        m_specimenName = anOffLineSpecimen.getDisplayString();
        String tooltip = "";
        double version = anOffLineSpecimen.getXMLVersion();
        if (version <= 1.0) {
            m_descriptionString = anOffLineSpecimen.getNamedParam("Description");
            tooltip = m_descriptionString;
        } else {
            tooltip = "<html>"
                    + anOffLineSpecimen.getNamedSampleInfoParam(Constants.XMLTags.NAME)
                    + "<br>Using: "
                    + anOffLineSpecimen
                    .getNamedInstrumetParam(Constants.XMLTags.LONG_NAME)
                    + "</html>";
            m_descriptionString = "";// aSpecimen.getNamedSampleInfoParam(Constants.XMLTags.NAME);
        }
        // Configure the button
        m_thumbnailLabel.setToolTipText(tooltip);

        // Create panel with display string and description
        JPanel textPanel = new JPanel();
        textPanel.setLayout(new GridLayout(2, 1));

        JLabel specimenNameLabel = new JLabel(m_specimenName);
        specimenNameLabel.setHorizontalAlignment(JLabel.CENTER);
        specimenNameLabel.setToolTipText(m_specimenName);
        specimenNameLabel.setPreferredSize(new Dimension(THUMB_SIZE_X, 15));

        textPanel.add(specimenNameLabel);

        if (version <= 1.0) {
            JLabel labelDescr = new JLabel(m_descriptionString);
            labelDescr.setToolTipText(m_descriptionString);
            textPanel.add(labelDescr);
        }

        // Put together the specimen panel
        setBorder(BorderFactory.createRaisedBevelBorder());
        setLayout(new GridBagLayout());
        //add(textPanel,BorderLayout.CENTER);
        m_downloadButton = new JButton(Locale.Download(Configuration.getApplicationLanguage()));
        m_downloadButton.setMargin(new Insets(2, 2, 2, 2));
        Font downloadButtonFont = new Font("Verdana", Font.BOLD, 11);
        m_downloadButton.setFont(downloadButtonFont);
        m_downloadButton.setActionCommand(anOffLineSpecimen.getUniqueName());
        m_downloadButton.setPreferredSize(DOWNLOAD_BUTTON_DIMENSION);


        GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.insets = new Insets(3, 3, 4, 3);
        add(m_thumbnailLabel, constraints);
        int col = 1;
        int height = SIZE_Y;
        if (requireSpecimenNameLabel) {
            constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = col++;
            constraints.insets = new Insets(0, 0, 0, 0);
            add(specimenNameLabel, constraints);
            height = SIZE_Y + 10;
        }
        constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = col;
        constraints.insets = new Insets(5, 0, 5, 0);
        add(m_downloadButton, constraints);
        setPreferredSize(new Dimension(SIZE_X, height));
    }

    /**
     * @param aSpecimen
     */
    private void displaySpecimeButton(Specimen anOnlineSpecimen, boolean requireSpecimenNameLabel) {
        // Gather display strilng and description
        m_specimenName = anOnlineSpecimen.getDisplayString();
        String tooltip = "";
        double version = anOnlineSpecimen.getXMLVersion();
        if (version <= 1.0) {
            m_descriptionString = anOnlineSpecimen.getNamedParam("Description");
            tooltip = m_descriptionString;
        } else {
            tooltip = "<html>"
                    + anOnlineSpecimen.getNamedSampleInfoParam(Constants.XMLTags.NAME)
                    + "<br>Using: "
                    + anOnlineSpecimen.getNamedInstrumetParam(Constants.XMLTags.LONG_NAME)
                    + "</html>";
            m_descriptionString = "";// aSpecimen.getNamedSampleInfoParam(Constants.XMLTags.NAME);
        }
        // Configure the button
        m_button.setToolTipText(tooltip);
        m_button.setActionCommand(anOnlineSpecimen.getUniqueName());
        setLayout(new BorderLayout(GAP_X, GAP_Y));
        setBorder(BorderFactory.createRaisedBevelBorder());

        add(m_button, BorderLayout.CENTER);
        int height = THUMB_SIZE_Y + (2 * GAP_Y);

        if (requireSpecimenNameLabel) {
            JPanel textPanel = new JPanel();
            textPanel.setLayout(new GridLayout(2, 1));

            VWrappingLabel labelDisp = new VWrappingLabel();
            labelDisp.setText(m_specimenName);
            labelDisp.setVAlignStyle(JLabel.TOP_ALIGNMENT);
            labelDisp.setHAlignStyle(JLabel.CENTER_ALIGNMENT);
            labelDisp.setPreferredSize(new Dimension(THUMB_SIZE_X, 35));
            labelDisp.setMaximumSize(new Dimension(THUMB_SIZE_X, 35));
            //Font labelFont = new Font("Verdana", Font.BOLD, 11);
            //labelDisp.setFont(labelFont);
            //labelDisp.setToolTipText(m_specimenName);
            textPanel.add(labelDisp);

            if (version <= 1.0) {
                JLabel labelDescr = new JLabel(m_descriptionString);
                labelDescr.setToolTipText(m_descriptionString);
                textPanel.add(labelDescr);
            }
            height = SIZE_Y;
            add(labelDisp, BorderLayout.SOUTH);
        }
        // Put together the specimen panel
        setPreferredSize(new Dimension(SIZE_X, height));
    }

    /**
     * gets the thumbnail image for the offline specimen. This is obtained by reading the image
     * from a specific location on the harddisk.
     *
     * @param anOfflineSpecimen
     * @return
     * @throws Exception
     */
    public Image getThumbnailImage(OfflineSpecimen anOfflineSpecimen, Dimension thumbnailSize) throws Exception {
        Image image = null;
        int x, y, minDim;
        File imageFile = new File(anOfflineSpecimen.getNamedSampleInfoParam(Constants.XMLTags.THUMBNAIL));
        // Create the thumbnail button
        // Get the thumbnail
        try {
            image = ImageIO.read(imageFile);
            // Crop and scale
            int imgHeight = image.getHeight(null);
            int imgWidth = image.getWidth(null);
            minDim = (imgHeight < imgWidth) ? imgHeight : imgWidth;
            x = (imgWidth - minDim) / 2;
            y = (imgHeight - minDim) / 2;

        } catch (Exception ex) {
            imageFile.deleteOnExit();
            throw ex;
        }
        image = createImage( new FilteredImageSource(image.getSource(), new CropImageFilter(x, y, minDim, minDim))).getScaledInstance(thumbnailSize.width, thumbnailSize.height, java.awt.image.BufferedImage.SCALE_SMOOTH);
        return image;
    }

    /**
     * @param aSpecimen TODO: This method should be moved into the Specimen class this will simplify the
     *                  rest of the application code tremendously. Very important Refactring this
     *                  MUST be done at the earliest.
     * @return
     */
    private Image getThumbnailImage(Specimen aSpecimen) throws Exception {
        // Create the thumbnail button
        // Get the thumbnail
        Image image = null;
        try {
            InputStream is = aSpecimen.getInputStream(aSpecimen
                    .getNamedParam(Constants.XMLTags.THUMBNAIL));
            image = ImageIO.read(is);
            is.close();
            aSpecimen.close();// must do this else OutOfMemoryError may
            // show up especially in Mac OS
        } catch (Exception ex) {
            InputStream is = aSpecimen.getInputStream(aSpecimen.getDefaultSampleThumbnail());
            image = ImageIO.read(is);
            is.close();
            aSpecimen.close();
        }

        // Crop and scale
        int imgHeight = image.getHeight(null);
        int imgWidth = image.getWidth(null);
        int minDim = (imgHeight < imgWidth) ? imgHeight : imgWidth;
        int x = (imgWidth - minDim) / 2;
        int y = (imgHeight - minDim) / 2;

        image = createImage(
                new FilteredImageSource(image.getSource(),
                        new CropImageFilter(x, y, minDim, minDim)))
                .getScaledInstance(THUMB_SIZE_X, THUMB_SIZE_Y,
                        java.awt.image.BufferedImage.SCALE_SMOOTH);
        return image;
    }

    /**
     * @param aSpecimen
     */
    private void createThumbnailLabel(OfflineSpecimen aSpecimen) {
        try {
            ImageIcon thumbIcon = new ImageIcon(getThumbnailImage(aSpecimen, new Dimension(THUMB_SIZE_X, THUMB_SIZE_Y)));
            m_thumbnailLabel = (thumbIcon.getIconHeight() > 0) ? new JLabel(thumbIcon) : new JLabel(Locale.NoThumbnail(Configuration.getApplicationLanguage()));
        } catch (Exception e) {
            m_thumbnailLabel = new JLabel(Locale.NoThumbnail(Configuration.getApplicationLanguage()));
        }
    }

    /**
     * @param aSpecimen
     */
    private void createThumbnailButton(Specimen aSpecimen) {
        try {
            ImageIcon thumbIcon = new ImageIcon(getThumbnailImage(aSpecimen));
            m_button = (thumbIcon.getIconHeight() > 0) ? new JButton(thumbIcon) : new JButton(Locale.NoThumbnail(Configuration.getApplicationLanguage()));
        } catch (Exception e) {
            m_button = new JButton(Locale.NoThumbnail(Configuration.getApplicationLanguage()));
        }
    }

    /**
     * Returns the specimen button (if it is an online specimen) or
     * download button) if it is an offline specimen.
     *
     * @return the thumbnail button.
     */
    JButton getSpecimenButtonOrDownloadButton() {
        if (m_button != null)
            return m_button;
        else
            return m_downloadButton;
    }
}
