/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.measure;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.font.TextLayout;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D.Double;
import java.awt.image.BufferedImage;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import virtuallab.MicroscopeView;
import virtuallab.Tile;
import virtuallab.Specimen.ControlStates;
import virtuallab.Specimen.ImageDescriptor;
import virtuallab.gui.TextMenu;
import virtuallab.gui.ViewControlAdapter;
import virtuallab.gui.controls.afm.ColormapMenu;
import virtuallab.util.Constants;

import virtuallab.Log;
import virtuallab.util.Locale;

/**
 * This control applies to AFM specimens. Given two end points of the control
 * it gives difference between the height at those two end points.
 *
 */
public class HeightMeasure extends ViewControlAdapter {

    private static final Log log = new Log(HeightMeasure.class.getName());

    /**
     * The Title of the control. This will be shown in the pop-up menu and in
     * any other places as needed
     */
    protected final static String TITLE = Locale.HeightMeasure(virtuallab.Configuration.getApplicationLanguage());
    private static final short NONE = 0;
    private static final short LEFT_ITEM = 1;
    private static final short CENTER_ITEM = 2;
    private static final short RIGHT_ITEM = 3;
    private static final BasicStroke HIGHLIGHT_STROKE = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    private static final BasicStroke REGULAR_STROKE = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    /**
     * The start of the height measure line in the specimen coordinates.
     *
     * @see #m_endPointInSpecimenCoordinates
     */
    private Point m_startPointInSpecimenCoordinates;
    /**
     * The end point of the height measure line in the specimen coordinates.
     *
     * @see #m_startPointInSpecimenCoordinates
     */
    private Point m_endPointInSpecimenCoordinates;
    private Point m_previousMousePosition;
    private TextLayout m_layout;
    private Point2D m_startOfTextLocation;
    private int m_leadingAndTrailingSpaces;
    private Double m_endOfLeadingLine;
    private Double m_startOfTrailingLineLocation;
    private String m_heightValue;
    /**
     * The start of the height measure line in the view port coordinates.
     *
     * @see #m_endPointInViewportCoordinates
     */
    private Point m_startPointInViewportCoordinates;
    /**
     * The end of the height measure line in the view port coordinates.
     *
     * @see #m_startPointInViewportCoordinates
     */
    private Point m_endPointInViewportCoordinates;

    /**
     * Represents the current pixel size. The pixel size changes with the magnification. This
     * information is used to sclae the height measure up or down based on the current magnification
     * level.
     */
    private Point2D.Double m_pixelSize;
    private short m_selection;
    private Rectangle m_labelBoundingBox;
    /**
     * The angle between the height measure control line and the X-axis.
     */
    private double m_angle;
    private JPopupMenu m_popupMenu;

    public HeightMeasure(Point mouseClickPoint, MicroscopeView display) {
        setSelected(CENTER_ITEM);
        m_previousMousePosition = new Point(mouseClickPoint);
        m_heightValue = "height";
        m_display = display;
        m_startPointInSpecimenCoordinates = getSpecimenCoordinates(new Point(HeightMeasureFactory.LINE_START_X, HeightMeasureFactory.LINE_START_Y));
        m_endPointInSpecimenCoordinates = getSpecimenCoordinates(new Point(HeightMeasureFactory.LINE_END_X, HeightMeasureFactory.LINE_END_Y));
        m_pixelSize = new Point2D.Double();
        m_pixelSize.setLocation(m_display.getCurrentImageSet().getPixelSize());
    }

    protected JPopupMenu getPopupMenu() {
        if (m_popupMenu == null) {
            m_popupMenu = buildPopupMenu();
        }
        return m_popupMenu;
    }


    /**
     * This method builds up the pop-up menu for the Height measure
     * control. As of now this pop-up menu contains only one item which
     * allows the user to delete the control.
     *
     * @return The pop-up menu with "Delete" option.
     */
    protected JPopupMenu buildPopupMenu() {
        JPopupMenu popupMenu = new JPopupMenu();

        JMenuItem item = new JMenuItem();
        item.setText(Locale.DeleteHeightMeasure(virtuallab.Configuration.getApplicationLanguage()));
        item.setActionCommand("Delete");
        popupMenu.add(item);
        item.addActionListener(new PopupMenuEventListener());
        return popupMenu;
    }

    /**
     * Given a Point in Specimen coordinates returns the equivaent point in the
     * viewport coordinates.
     *
     * @param specimenCoordinatePoint
     * @return the equivalent point in view port coordinates.
     */
    private Point getViewPortCoordinates(Point specimenCoordinatePoint) {
        int x = (int) (m_display.getStagePosition().x + specimenCoordinatePoint.getX());
        int y = (int) (m_display.getStagePosition().y + specimenCoordinatePoint.getY());
        return new Point(x, y);
    }

    /**
     * Given a Point in view port coordinates returns the equivalent point in the
     * specimen coordinates.
     *
     * @param viewPortCoordinate
     * @return the equivalent point in specimen coordinatess.
     */
    private Point getSpecimenCoordinates(Point viewPortCoordinate) {
        int x = (int) (viewPortCoordinate.getX() - m_display.getStagePosition().x);
        int y = (int) (viewPortCoordinate.getY() - m_display.getStagePosition().y);
        return new Point(x, y);
    }

    public void draw(Graphics2D g) {
        calculateCoordinates(g);
        drawHeightMeasureLine(g);
        drawLeadingAndTrailingCircles(g);
        drawHeightValueLabel(g);
    }

    /**
     * Draws the height measure control line. First we draw the line and then we use clipping
     * to remove the portion of line that would otherwise pass through the height label. This gives us
     * a clean nice look in which the height value shows through right in the middle of the line without
     * the line striking it through.
     * Normal clipping clips removes everything outside the clip area. So we do have to do some
     * area manipulation to get the reverse affect.
     * bounding box and we use
     *
     * @param g
     */
    private void drawHeightMeasureLine(Graphics2D g) {

        Shape oldClip = g.getClip();

        Rectangle2D labelRectangle = new Rectangle2D.Double(m_labelBoundingBox.x - 3, m_labelBoundingBox.y - 3,
                m_labelBoundingBox.width + 6, m_labelBoundingBox.height + 6);

        Line2D line = new Line2D.Double(m_startPointInViewportCoordinates, m_endPointInViewportCoordinates);
        Rectangle2D boundingRectangle = new Rectangle2D.Double(line.getBounds2D().getX(), line.getBounds2D().getY(),
                line.getBounds2D().getWidth() + 5, line.getBounds2D().getHeight() + 5);
        Area boundingArea = new Area(boundingRectangle);
        boundingArea.subtract(new Area(labelRectangle));
        g.setClip(boundingArea);
        g.drawLine((int) m_startPointInViewportCoordinates.getX(), (int) m_startPointInViewportCoordinates.getY(),
                (int) m_endPointInViewportCoordinates.getX(), (int) m_endPointInViewportCoordinates.getY());
        g.setClip(oldClip);
    }

    /**
     * Draws the leading line - from the start of the control to the start of the text, and the
     * trailing line - from the end of the text to the end of the control.
     *
     * @param g
     */
    private void drawLeadingAndTrailingCircles(Graphics2D g) {

        Color oldColor = g.getColor();
        if (isLeftItemSelected()) {
            g.setColor(Color.YELLOW);
            g.fillOval((int) m_startPointInViewportCoordinates.getX() - 5, (int) m_startPointInViewportCoordinates.getY() - 5, 10, 10);
            g.setColor(oldColor);
        } else {
            g.fillOval((int) m_startPointInViewportCoordinates.getX() - 5, (int) m_startPointInViewportCoordinates.getY() - 5, 10, 10);
        }

        if (isRightItemSelected()) {
            g.setColor(Color.YELLOW);
            g.fillOval((int) m_endPointInViewportCoordinates.getX() - 5, (int) m_endPointInViewportCoordinates.getY() - 5, 10, 10);
            g.setColor(oldColor);
        } else {
            g.fillOval((int) m_endPointInViewportCoordinates.getX() - 5, (int) m_endPointInViewportCoordinates.getY() - 5, 10, 10);
        }
    }

    /**
     * Draws the height value that appears in the center of the control.
     *
     * @param g
     */
    private void drawHeightValueLabel(Graphics2D g) {
        m_heightValue = calculateHeightMeasure();
        if (isCenterItemSelected()) {
            Color oldColor = g.getColor();
            g.setColor(Color.DARK_GRAY);
            g.fill(m_labelBoundingBox);
            g.setColor(oldColor);
        }
        m_layout.draw(g, (float) m_startOfTextLocation.getX(), (float) m_startOfTextLocation.getY());
    }

    /**
     * Calculates the start and end points for the factory control and the
     * label text that is shown in the middle.
     *
     * @param g
     */
    private void calculateCoordinates(Graphics2D g) {

        if (hasMagnificationChanged()) {
            updateStartAndEndSpecimenCoordinates();
        }
        m_startPointInViewportCoordinates = getViewPortCoordinates(m_startPointInSpecimenCoordinates);
        m_endPointInViewportCoordinates = getViewPortCoordinates(m_endPointInSpecimenCoordinates);

        m_layout = new TextLayout(m_heightValue, g.getFont(), g.getFontRenderContext());
        m_startOfTextLocation = getTextStartLocation(m_layout);
        double leadingLineLength = m_startOfTextLocation.getX() - (int) m_startPointInViewportCoordinates.getX();
        m_leadingAndTrailingSpaces = 6;

        m_endOfLeadingLine = new Point2D.Double(m_startOfTextLocation.getX() - m_leadingAndTrailingSpaces, (int) m_startOfTextLocation.getY() - 4);
        m_startOfTrailingLineLocation = new Point2D.Double(m_endPointInViewportCoordinates.getX() - leadingLineLength + m_leadingAndTrailingSpaces,
                (int) m_startOfTextLocation.getY() - 4);
        Rectangle backgroundRect = m_layout.getLogicalHighlightShape(0, m_heightValue.length()).getBounds();
        backgroundRect.x = (int) (backgroundRect.x + m_endOfLeadingLine.getX() + m_leadingAndTrailingSpaces);
        backgroundRect.y = (int) (m_endOfLeadingLine.getY() - backgroundRect.getHeight() / 2);
        m_labelBoundingBox = new Rectangle(backgroundRect);
    }

    /**
     * Updates the start and end coordinates in response to a magnification change
     */
    private void updateStartAndEndSpecimenCoordinates() {
        double xScalingFactor = m_pixelSize.getX() / m_display.getCurrentImageSet().getPixelSize().getX();
        double yScalingFactor = m_pixelSize.getY() / m_display.getCurrentImageSet().getPixelSize().getY();

        m_startPointInSpecimenCoordinates.x *= xScalingFactor;
        m_startPointInSpecimenCoordinates.y *= yScalingFactor;

        m_endPointInSpecimenCoordinates.x *= xScalingFactor;
        m_endPointInSpecimenCoordinates.y *= yScalingFactor;

        m_pixelSize.setLocation(m_display.getCurrentImageSet().getPixelSize());

    }

    /**
     * Returns true if the magnification has been changed since the last time this control was
     * drawn, false otherwise. This information will be used to update the control start and end
     * points.
     *
     * @return true if the magnification has been changed since the last time this control was
     * drawn, false otherwise
     */
    private boolean hasMagnificationChanged() {
        return (m_display.getCurrentImageSet().getPixelSize().getX() != m_pixelSize.getX() ||
                m_display.getCurrentImageSet().getPixelSize().getY() != m_pixelSize.getY());
    }

    /**
     * Calculate the start location for the text. The start location is
     * calculated such that the center of the text is aligned with the
     * center if the Height Control Line.
     *
     * @param layout
     */
    private Point2D getTextStartLocation(TextLayout layout) {
        Rectangle2D textBounds = layout.getBounds();
        textBounds.getCenterX();
        double centerOfLineX = (m_startPointInViewportCoordinates.getX() + m_endPointInViewportCoordinates.getX()) / 2;
        double centerOfLineY = (m_startPointInViewportCoordinates.getY() + m_endPointInViewportCoordinates.getY()) / 2;
        double centerOfTextX = textBounds.getCenterX();
        double centerOfTextY = textBounds.getCenterY();
        double deltaX = centerOfLineX - centerOfTextX;
        double deltaY = centerOfLineY - centerOfTextY;
        return (new Point2D.Double(deltaX, deltaY));
    }

    public boolean isSelected() {
        return (m_selection == CENTER_ITEM ||
                m_selection == LEFT_ITEM ||
                m_selection == RIGHT_ITEM);
    }

    public void setSelected(short selection) {
        m_selection = selection;
    }

    public boolean isInTheVicinity(Point point) {
        Rectangle2D boundingRectangle = new Rectangle2D.Double(m_startPointInViewportCoordinates.x - 5,
                m_startPointInViewportCoordinates.y - 5, m_endPointInViewportCoordinates.x - m_startPointInViewportCoordinates.x + 10, 10);
        return boundingRectangle.contains(point);
    }

    /**
     * If the height measure is being drawn or dragged around then the firing of this
     * event will indicate that the manipulation has finished, so we need to show the
     * height difference now.
     */
    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            handlePopupTrigger(e);
        } else if (!e.isConsumed() && isSelected()) {
            m_heightValue = calculateHeightMeasure();
            e.consume();
        }
    }

    /**
     * The control has a visual representation (a line segment with circles at the end) and it also has a
     * logical representation in the specimen coordinates. This method looks at the visual representation and
     * works out the equivalent position of the control in the specimen coordinates. Once it has figured out
     * the end points of the control in the specimen coordinates it then gets the height of both these end points
     * (represented as brightness values of the pixels) and calculates the difference between the height.
     * It returns the height difference (a numerical value) along with the appropriate units.
     *
     * @return
     */
    private String calculateHeightMeasure() {
        TextMenu magControl = (TextMenu) m_display.getViewControl(Constants.Controls.MAGNIFICATION);
        ImageDescriptor desc = magControl.getCurrentControlState(m_display.getCurrentSpecimen().getXMLVersion()).getImageDescriptor();

        double height1 = retrieveRawHeightValue(m_startPointInSpecimenCoordinates, magControl) * 255;
        double height2 = retrieveRawHeightValue(m_endPointInSpecimenCoordinates, magControl) * 255;
        if (height1 < 0 || height2 < 0)
            return "invalid";
        return MeasureControlBase.computeReadout(Math.abs(height1 - height2) * getHeightScalingFactor(desc), 1000000);
    }

    /**
     * Given a point in Specimen coordinates, this method retrieves the brightness of the pixel on that
     * particular location - which qualatitively represent the height of the specimen at that point.
     * The height value is picked up from the gray scale base image only. this is because it is only the
     * grayscale image that encodes the height as brightness, the rest of the color base images have their
     * brightness values changed due to the coloring in those images.
     *
     * @param pointInSpecimenCoordinates
     * @param magnificationControl       Reference to the Magnification Side control.
     * @return The brightness of the pixel at the given point - which qualitatively represents the height
     * of the specimen at that point.
     * @throws Exception
     */
    private double retrieveRawHeightValue(Point2D pointInSpecimenCoordinates, TextMenu magnificationControl) {
        try {
            ControlStates grayscaleImageState = ((ColormapMenu) m_display.getViewControl(Constants.Controls.COLORMAP)).getFirstValue();
            ControlStates controlState = magnificationControl.getCurrentControlState(m_display.getCurrentSpecimen().getXMLVersion());
            ImageDescriptor desc = controlState.getImageDescriptor();
            if (desc == null)
                return -1;
            double tileRowSize = desc.getTileSize().getHeight();
            double tileColumnSize = desc.getTileSize().getWidth();

            double specimenCoordinatesX = pointInSpecimenCoordinates.getX();
            double specimenCoordinatesY = pointInSpecimenCoordinates.getY();

            int rowThatContainsThePixel = (int) (specimenCoordinatesY / tileRowSize);
            int columnThatContainsThePixel = (int) (specimenCoordinatesX / tileColumnSize);
            Tile tileContainingThePixel = new Tile(rowThatContainsThePixel, columnThatContainsThePixel, (int) (columnThatContainsThePixel * tileColumnSize + m_display.getStagePosition().x), (int) (rowThatContainsThePixel * tileRowSize + m_display.getStagePosition().y));
            String tilePath = m_display.getTilePath(m_display.getCurrentImageSet().getPath(), grayscaleImageState.getDepth(), grayscaleImageState.getFolderName());
            BufferedImage imageTile = m_display.getImageTile(tilePath + tileContainingThePixel.getFilePath());
            int tileCoordinateX = (int) (specimenCoordinatesX % tileColumnSize);
            int tileCoordinateY = (int) (specimenCoordinatesY % tileRowSize);

            if (tileCoordinateX < 0 || tileCoordinateY < 0 || tileCoordinateX >= imageTile.getWidth() || tileCoordinateY >= imageTile.getHeight())
                // user has moved the control beyond the specimen boundaries.
                return -1;
            int rgb = imageTile.getRGB(tileCoordinateX, tileCoordinateY);
            Color clr = new Color(rgb);
            float[] hsb = new float[3];
            Color.RGBtoHSB(clr.getRed(), clr.getGreen(), clr.getBlue(), hsb);
            return (hsb[2]);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return -1;
        }
    }

    private double getHeightScalingFactor(ImageDescriptor desc) {
        try {
            if (desc.getPixelHeightRange() != null)
                return desc.getPixelHeightRange().doubleValue();
            LinkedHashMap infoRefStrings = m_display.getCurrentSpecimen().getSampleInfo().getInfoRefStrings();
            Iterator it = infoRefStrings.keySet().iterator();
            while (it.hasNext()) {
                String key = (String) it.next();
                if (key.indexOf("Range: ") == 0) {
                    String height = key.substring("Range: ".length());
                    String units = height.substring(height.length() - 2);
                    double powerOfTen = 0.0;
                    if (units.equalsIgnoreCase("nm")) {
                        powerOfTen = -9.0;
                    } else if (units.equalsIgnoreCase("pm")) {
                        powerOfTen = -12.0;
                    } else if (units.equalsIgnoreCase("um") || units.equalsIgnoreCase("\u00b5m")) {
                        powerOfTen = -6.0;
                    } else if (units.equalsIgnoreCase("mm")) {
                        powerOfTen = -3.0;
                    } else if (units.equalsIgnoreCase("m")) {
                        powerOfTen = 0.0;
                    } else {
                        log.error("Unexpected/Unhandled height units found:" + units);
                    }
                    height = height.trim();
                    height = height.substring(0, height.length() - 2);
                    double scalingFactor = java.lang.Double.parseDouble(height);
                    scalingFactor *= Math.pow(10, powerOfTen);
                    return scalingFactor;
                }
            }
            throw new Exception("No Height Scaling information found in specimen file");
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return 1;
        }
    }

    public void mousePressed(MouseEvent e) {
        detectSelection(e.getPoint());
        if (e.isPopupTrigger() || SwingUtilities.isRightMouseButton(e)) {
            handlePopupTrigger(e);
        } else if (isSelected()) {
            m_previousMousePosition.setLocation(e.getPoint());
            e.consume();
        }
    }

    /**
     * This method detects if the specified point is near the control and sets the
     * selection item appropriately.
     *
     * @param point
     * @return true if the point is near the control false otherwise.
     */
    private boolean detectSelection(Point point) {
        int topLeftX = m_startPointInViewportCoordinates.x;
        int topLeftY = m_startPointInViewportCoordinates.y - 5;
        int width = (int) m_startOfTextLocation.getX() - topLeftX;
        int height = 10;


        Ellipse2D leftCircle = new Ellipse2D.Double((int) m_startPointInViewportCoordinates.getX() - 5, (int) m_startPointInViewportCoordinates.getY() - 5, 10, 10);
        Ellipse2D rightCircle = new Ellipse2D.Double((int) m_endPointInViewportCoordinates.getX() - 5, (int) m_endPointInViewportCoordinates.getY() - 5, 10, 10);

        if (m_labelBoundingBox.contains(point)) {
            setSelected(CENTER_ITEM);
        } else if (leftCircle.contains(point)) {
            setSelected(LEFT_ITEM);
        } else if (rightCircle.contains(point)) {
            setSelected(RIGHT_ITEM);
        } else
            setSelected(NONE);
        return isSelected();
    }

    public void mouseDragged(MouseEvent e) {
        if (isCenterItemSelected()) {
            m_heightValue = calculateHeightMeasure();
            int deltaX = (int) (e.getX() - m_previousMousePosition.getX());
            int deltaY = (int) (e.getY() - m_previousMousePosition.getY());
            m_startPointInSpecimenCoordinates.setLocation(m_startPointInSpecimenCoordinates.x + deltaX,
                    m_startPointInSpecimenCoordinates.y + deltaY);

            m_endPointInSpecimenCoordinates.setLocation(m_endPointInSpecimenCoordinates.x + deltaX,
                    m_endPointInSpecimenCoordinates.y + deltaY);
            m_previousMousePosition.setLocation(e.getPoint());
            e.consume();
            m_display.repaint();
        } else if (isLeftItemSelected()) {
            m_heightValue = calculateHeightMeasure();
            int deltaX = (int) (e.getX() - m_previousMousePosition.getX());
            int deltaY = (int) (e.getY() - m_previousMousePosition.getY());
            m_startPointInSpecimenCoordinates.setLocation(m_startPointInSpecimenCoordinates.x + deltaX,
                    m_startPointInSpecimenCoordinates.y + deltaY);
            m_previousMousePosition.setLocation(e.getPoint());
            e.consume();
            m_display.repaint();
        } else if (isRightItemSelected()) {
            m_heightValue = calculateHeightMeasure();
            int deltaX = (int) (e.getX() - m_previousMousePosition.getX());
            int deltaY = (int) (e.getY() - m_previousMousePosition.getY());
            m_endPointInSpecimenCoordinates.setLocation(m_endPointInSpecimenCoordinates.x + deltaX,
                    m_endPointInSpecimenCoordinates.y + deltaY);
            m_previousMousePosition.setLocation(e.getPoint());
            e.consume();
            m_display.repaint();
        }
    }

    public void mouseMoved(MouseEvent e) {
        detectSelection(e.getPoint());
    }

    /**
     * This method processes the key pressed while the distance control is
     * selected. Some of the keypressed events handled in this method include:
     * <ol>
     * <li> Delete Key: which is used to delete the distance control (same
     * affect as pressing right click and selecting "Delete" from the pop-up
     * menu.</li>
     * <li>Arrow Keys: Used to move the distance control with arrows. Same
     * affect as moving the distance control with mouse. </li>
     * </ol>
     *
     * @param <b>KeyEvent </b> The key event passed on to the method.
     */
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (isSelected()) {
            if (keyCode == KeyEvent.VK_DELETE
                    || keyCode == KeyEvent.VK_BACK_SPACE) {
                e.consume();
                m_display.removeControl(this);
                e.consume();
                m_display.repaint();
            } else if (isCenterItemSelected()) {
                moveHeightControl(m_startPointInSpecimenCoordinates, m_endPointInSpecimenCoordinates, e);
                e.consume();
                m_display.repaint();
            } else if (isLeftItemSelected()) {
                moveHeightControl(m_startPointInSpecimenCoordinates, new Point(0, 0), e);
                e.consume();
                m_display.repaint();
            } else if (isRightItemSelected()) {
                moveHeightControl(new Point(0, 0), m_endPointInSpecimenCoordinates, e);
                e.consume();
                m_display.repaint();
            }
        }
    }

    /**
     * Depending on the arrow key that was pressed, increments/decrements the
     * X or Y coordinates of the startPoint and endPoint.
     *
     * @param startPoint
     * @param endPoint
     * @param e
     */
    private void moveHeightControl(Point startPoint, Point endPoint, KeyEvent e) {
        int keyCode = e.getKeyCode();
        if (keyCode == KeyEvent.VK_RIGHT) {
            startPoint.x += 1;
            endPoint.x += 1;
        } else if (keyCode == KeyEvent.VK_LEFT) {
            startPoint.x -= 1;
            endPoint.x -= 1;
        } else if (keyCode == KeyEvent.VK_UP) {
            startPoint.y -= 1;
            endPoint.y -= 1;
        } else if (keyCode == KeyEvent.VK_DOWN) {
            startPoint.y += 1;
            endPoint.y += 1;
        }
    }

    private boolean isCenterItemSelected() {
        return (m_selection == CENTER_ITEM);
    }

    private boolean isLeftItemSelected() {
        return (m_selection == LEFT_ITEM);
    }

    private boolean isRightItemSelected() {
        return (m_selection == RIGHT_ITEM);
    }

    public void mouseWheelMoved(MouseWheelEvent arg0) {
    }

    public void unSelect() {
        m_selection = NONE;
    }

    protected void handlePopupTrigger(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }
        if (isSelected()) {
            JPopupMenu popup = getPopupMenu();
            if (popup != null) {
                e.consume();
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }


    /**
     * The class that handles the actions performed on the Pop-up Menu.
     *
     * @author Kashif Manzoor
     */
    class PopupMenuEventListener implements java.awt.event.ActionListener {
        public void actionPerformed(ActionEvent e) {
            performPopupMenuAction(e);
        }

        private void performPopupMenuAction(ActionEvent e) {
            JMenuItem item = (JMenuItem) e.getSource();
            if (item.getActionCommand().equals("Delete")) {
                m_display.removeControl(HeightMeasure.this);
                m_display.repaint();
            }
        }
    }


}