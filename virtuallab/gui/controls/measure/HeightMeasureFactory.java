/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.measure;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.font.TextLayout;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D.Double;

import javax.swing.SwingUtilities;

import virtuallab.MicroscopeView;
import virtuallab.gui.ViewControlAdapter;
import virtuallab.util.Locale;

/**
 * This is the factory for creating height measures. The factory creates a new
 * Height measure everytime the user drags the height measure control (located
 * at the top left) onto the specimen. Although the Height Measure behaves
 * similar to the Distance Measure but there are enough differences to warrant a
 * separate Class.
 *
 */
public class HeightMeasureFactory extends ViewControlAdapter {
    public static final int LINE_START_X = 20;

    public static final int LINE_START_Y = 40;

    public static final int LINE_END_X = LINE_START_X + 100;

    public static final int LINE_END_Y = LINE_START_Y;
    private static final BasicStroke HIGHLIGHT_STROKE = new BasicStroke(3,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    private static final BasicStroke REGULAR_STROKE = new BasicStroke(2,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    private static HeightMeasureFactory m_instance;
    /**
     * Should the Factory show itself or not. The Factory will show itself at
     * the top left corner of the screen if the loaded specimen is an AFM
     * specimen.
     */
    private boolean m_isVisible;

    private HeightMeasure m_heightMeasureBeingDrawn;

    /**
     * indicates if the factory control is selected
     */
    private boolean m_selected;
    /**
     * The bounding box of the label of the height measure factory control.
     * Used to determine if the Factory control is selected or not.
     */
    private Rectangle2D m_labelBoundingBox;

    private TextLayout m_layout;

    private int m_leadingAndTrailingSpaces;

    private Point2D m_startOfTextLocation;

    private Double m_startOfTrailingLineLocation;

    private Double m_endOfLeadingLine;

    /**
     * Default constructor. This class is a Singleton.
     */
    private HeightMeasureFactory() {
    }

    public static HeightMeasureFactory getInstance() {
        if (m_instance == null) {
            m_instance = new HeightMeasureFactory();
        }
        return m_instance;
    }

    /**
     * Framework method. Gets called when the specimen is about to be loaded.
     * Gives the factory a chance to properly initialize itself.
     *
     * @param controlName unique name/type of this control
     * @param display     MicroscopeView object
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        m_isVisible = display.getCurrentSpecimen().isAfm();
        m_display = display;
        m_controlName = controlName;
    }

    /**
     * Determines if the specified point is on or near the Height Control
     * Label.
     *
     * @param mouseClick
     * @return
     */
    private boolean isInTheVicinityOfTheLabel(Point2D mouseClick) {
//		Rectangle2D boundingRectangle = new Rectangle(LINE_START_X - 5,
//				LINE_START_Y - 5, LINE_END_X - LINE_START_X + 10, 10);
        if (m_labelBoundingBox == null)
            return false;
        return (m_labelBoundingBox.contains(mouseClick));
    }


    /**
     * Framework method. Gets called when a redraw is needed.
     */
    public void draw(Graphics2D g) {
        if (m_isVisible) {
            if (m_labelBoundingBox == null) {
                calculateCoordinates(g);
            }
            Stroke oldStroke = g.getStroke();
            if (m_selected) {
                g.setStroke(HIGHLIGHT_STROKE);
                Color oldColor = g.getColor();
                g.setColor(Color.DARK_GRAY);

                g.fill(m_labelBoundingBox);
                g.setColor(oldColor);
            } else {
                g.setStroke(REGULAR_STROKE);
            }
            m_layout.draw(g, (float) m_startOfTextLocation.getX(), (float) m_startOfTextLocation.getY());
            drawLeaderLines(g);
            drawLeadingAndTrailingCircles(g);
            g.setStroke(oldStroke);
            if (isHeightMeasureBeingDrawn()) {
                m_heightMeasureBeingDrawn.draw(g);
            }
        }
    }

    /**
     * Draws two circles at both the end points of the factory control
     *
     * @param g
     */
    private void drawLeaderLines(Graphics2D g) {
        g.drawLine(LINE_START_X, LINE_START_Y, (int) m_endOfLeadingLine.getX(), (int) m_endOfLeadingLine.getY());
        g.drawLine((int) m_startOfTrailingLineLocation.getX(), (int) m_startOfTrailingLineLocation.getY(), LINE_END_X, LINE_END_Y);
    }

    /**
     * Draws the leading line - from the start of the control to the start of the text, and the
     * trailing line - from the end of the text to the end of the control.
     *
     * @param g
     */
    private void drawLeadingAndTrailingCircles(Graphics2D g) {
        g.fillOval(LINE_START_X - 5, LINE_START_Y - 5, 10, 10);
        g.fillOval(LINE_END_X - 5, LINE_END_Y - 5, 10, 10);
    }

    /**
     * Calculates the start and end points for the factory control and the
     * label text that is shown in the middle.
     *
     * @param g
     */
    private void calculateCoordinates(Graphics2D g) {
        String label = Locale.Height(virtuallab.Configuration.getApplicationLanguage());
        m_layout = new TextLayout(label, g.getFont(), g.getFontRenderContext());
        m_startOfTextLocation = getTextStartLocation(m_layout);
        double leadingLineLength = m_startOfTextLocation.getX() - LINE_START_X;
        m_leadingAndTrailingSpaces = 6;

        m_endOfLeadingLine = new Point2D.Double(m_startOfTextLocation.getX() - m_leadingAndTrailingSpaces, LINE_START_Y);
        m_startOfTrailingLineLocation = new Point2D.Double(LINE_END_X - leadingLineLength + m_leadingAndTrailingSpaces, LINE_END_Y);
        Rectangle backgroundRect = m_layout.getLogicalHighlightShape(0, label.length()).getBounds();
        backgroundRect.x = (int) (backgroundRect.x + m_endOfLeadingLine.getX() + m_leadingAndTrailingSpaces);
        backgroundRect.y = (int) (m_endOfLeadingLine.getY() - backgroundRect.getHeight() / 2);
        m_labelBoundingBox = new Rectangle(backgroundRect);
    }

    private boolean isHeightMeasureBeingDrawn() {
        return (m_heightMeasureBeingDrawn != null);
    }

    /**
     * Calculate the start location for the text. The start location is
     * calculated such that the center of the text is aligned with the
     * center if the Height Control Line.
     *
     * @param layout
     */
    private Point2D getTextStartLocation(TextLayout layout) {
        Rectangle2D textBounds = layout.getBounds();
        textBounds.getCenterX();
        double centerOfLineX = (LINE_START_X + LINE_END_X) / 2;
        double centerOfLineY = (LINE_START_Y + LINE_END_Y) / 2;
        double centerOfTextX = textBounds.getCenterX();
        double centerOfTextY = textBounds.getCenterY();
        double deltaX = centerOfLineX - centerOfTextX;
        double deltaY = centerOfLineY - centerOfTextY;
        return (new Point2D.Double(deltaX, deltaY));
    }

    public void mousePressed(MouseEvent e) {
        if (m_selected) {
            // this will stop the specimen movement when the mouse is pressed.
            e.consume();
        }
    }

    public void mouseClicked(MouseEvent arg0) {
    }

    public void mouseMoved(MouseEvent mouseEvent) {
        if (isInTheVicinityOfTheLabel(mouseEvent.getPoint())) {
            m_selected = true;
            mouseEvent.consume();
        } else
            m_selected = false;
    }

    /**
     * When the user drags the factory control we need to create a new Height Measure.
     * The user will set drag the newly created height measure to the desired location
     * and when the dragging stops we will then complete the creation of the Height Measure.
     *
     * @see #mouseReleased(MouseEvent)
     */
    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.isConsumed()) {
                return;
            }
            if (m_selected) {
                m_heightMeasureBeingDrawn = new HeightMeasure(e.getPoint(), m_display);
                m_selected = false;
                e.consume();
            }
            if (isHeightMeasureBeingDrawn()) {
                m_heightMeasureBeingDrawn.mouseDragged(e);
            }
        }
    }

    public void mouseReleased(MouseEvent e) {
        if (isHeightMeasureBeingDrawn()) {
            // complete the height measure creation
            m_heightMeasureBeingDrawn.unSelect();
            m_display.addControl(m_heightMeasureBeingDrawn);
            m_heightMeasureBeingDrawn = null;
        }
    }

    protected boolean isFactoryControl() {
        return true;
    }

    protected Object clone() {
        // TODO Auto-generated method stub
        return null;
    }

    public void mouseWheelMoved(MouseWheelEvent arg0) {
    }
}
