/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.measure;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.Stroke;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;

import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;

import java.util.HashMap;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import virtuallab.gui.Control;
import virtuallab.util.Locale;

// $Id: DistanceMeasure.java,v 1.5 2006/09/05 01:08:47 manzoor2 Exp $

public class DistanceMeasure extends MeasureControlBase {
    /**
     * Title.
     */
    protected final static String TITLE = Locale.DistanceMeasure(virtuallab.Configuration.getApplicationLanguage());

    // Interaction and highlight state identifiers.
    protected static final double MARGIN = 5;
    /**
     * indicates that no item has been selected so no need to draw either a bold
     * line or a shadowed backgrond. The bold line or the shadowed background
     * indicate one of the two forms of highlighting that the distance measure
     * can have.
     */
    protected static final int NO_ITEM = 0;
    /**
     * Indicates that the mouse is on the center of the distance contrl. So we
     * should show the centered text with shadowed background.
     */
    protected static final int CENTER_ITEM = 1;
    /**
     * Indicates that the mouse is on left part of the distance control, So we
     * should show the distance line as bold.
     */
    protected static final int LEFT_ITEM = 2;
    /**
     * Indicates that the mouse is on right part of the distance control, So we
     * should show the distance line as bold.
     */
    protected static final int RIGHT_ITEM = 3;
    private static final BasicStroke REGULAR_STROKE = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    private static final BasicStroke HIGHLIGHT_STROKE = new BasicStroke(3, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    protected static Control factoryControl = null;
    protected String m_title;
    protected Rectangle2D.Double m_bounds = new Rectangle2D.Double();
    protected double m_angle = Math.toRadians(0);
    protected double m_width = 100;
    protected Point2D.Double m_location = new Point2D.Double(20, 20);
    protected AffineTransform m_currentTransform = new AffineTransform();
    protected Shape m_leftBound = new Rectangle2D.Double();
    protected Shape m_centerBound = new Rectangle2D.Double();
    protected Shape m_rightBound = new Rectangle2D.Double();
    protected Point2D.Double m_pressPosition = null;
    protected HashMap m_textAttributes = new HashMap();
    protected Color m_textColor = Color.WHITE;
    protected Color m_lineColor = Color.WHITE;
    protected int m_highlightItem = NO_ITEM;
    protected int m_interactionState = NO_ITEM;
    /**
     * This field will indicate if the control is selected or not. This
     * information is currently being used to handle keypressed and key typed
     * events. e.g. moving the selected distance control when arrow keys are
     * pressed or deleting it when delete key is pressed
     */
    private boolean m_selected = false;

    public DistanceMeasure() {
        if (factoryControl == null) {
            factoryControl = this;
        }
        m_title = TITLE;
        m_popupMenuItemText = Locale.DeleteDistanceMeasure(virtuallab.Configuration.getApplicationLanguage());
        initializeFeedbackAttributes();
    }

    public static void releaseFactory() {
        factoryControl = null;
    }

    protected boolean isFactoryControl() {
        return factoryControl == this;
    }

    protected Object clone() {
        DistanceMeasure newOne = new DistanceMeasure();

        newOne.m_bounds.setRect(m_bounds);
        newOne.m_angle = m_angle;
        newOne.m_width = m_width;
        newOne.m_display = m_display;
        newOne.m_title = m_title;
        newOne.m_location.setLocation(m_location);
        newOne.m_currentTransform.setTransform(m_currentTransform);

        newOne.m_leftBound = new Rectangle2D.Double(m_leftBound.getBounds2D()
                .getX(), m_leftBound.getBounds2D().getY(), m_leftBound
                .getBounds2D().getWidth(), m_leftBound.getBounds2D()
                .getHeight());

        newOne.m_centerBound = new Rectangle2D.Double(m_centerBound
                .getBounds2D().getX(), m_centerBound.getBounds2D().getY(),
                m_centerBound.getBounds2D().getWidth(), m_centerBound
                .getBounds2D().getHeight());

        newOne.m_rightBound = new Rectangle2D.Double(m_rightBound.getBounds2D()
                .getX(), m_rightBound.getBounds2D().getY(), m_rightBound
                .getBounds2D().getWidth(), m_rightBound.getBounds2D()
                .getHeight());

        if (m_pressPosition != null) {
            newOne.m_pressPosition = new Point2D.Double(m_pressPosition.x,
                    m_pressPosition.y);
        }

        newOne.m_textAttributes = m_textAttributes;
        newOne.m_textColor = m_textColor;
        newOne.m_lineColor = m_lineColor;

        return newOne;
    }

    private void initializeFeedbackAttributes() {
        m_textAttributes.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_REGULAR);
    }

    public Point2D.Double getLocation() {
        return m_location;
    }

    public void setLocation(Point2D.Double location) {
        m_location = location;
    }

    public Rectangle2D.Double getControlBounds() {
        return m_bounds;
    }

    public String getCurrentValue() {
        return Double.toString(m_width);
    }

    public String getTitle() {
        return m_title;
    }

    public void setTitle(String title) {
        m_title = title;
    }

    public void draw(Graphics2D g) {
        // Create the text layout for the measurement microscope.
        String readoutString = MeasureControlBase.computeReadout(m_width,
                m_display.getCurrentImageSet().getPixelSize().getX());

        TextLayout layout = new TextLayout(readoutString, g.getFont()
                .deriveFont(m_textAttributes), g.getFontRenderContext());

        // Compute and concatenate the position and rotation transforms.
        m_currentTransform.setToTranslation(m_location.x, m_location.y);
        m_currentTransform.rotate(m_angle);

        // Compute key points.
        double lineLength = Math.max(0, 0.4 * (m_width - layout.getBounds()
                .getWidth()));
        Point2D.Double p[] = new Point2D.Double[5];

        p[0] = new Point2D.Double(0, 0);
        p[1] = new Point2D.Double(p[0].x + lineLength, p[0].y);
        p[2] = new Point2D.Double(p[0].x + 0.5 * m_width, p[0].y);
        p[3] = new Point2D.Double(p[0].x + m_width - lineLength, p[0].y);
        p[4] = new Point2D.Double(p[0].x + m_width, p[0].y);

        // Compute line and text extents.
        double h = layout.getBounds().getHeight();
        Rectangle2D.Double bound = new Rectangle2D.Double();

        bound.setRect(p[0].x, p[0].y - 0.5 * h, lineLength + MARGIN, h);
        m_leftBound = m_currentTransform.createTransformedShape(bound)
                .getBounds2D();

        bound.setRect(p[3].x, p[3].y - 0.5 * h, lineLength + MARGIN, h);
        m_rightBound = m_currentTransform.createTransformedShape(bound)
                .getBounds2D();

        Point2D.Double textPos = new Point2D.Double();
        m_currentTransform.transform(p[2], textPos);

        // Shift the text position to account for centering.
        textPos.x -= 0.5 * layout.getAdvance();
        textPos.y += 0.5 * layout.getBounds().getHeight();
        if (layout.getBounds().getWidth() > 0.7 * m_width) {
            textPos.y -= layout.getBounds().getHeight();
        }
        AffineTransform at = AffineTransform.getTranslateInstance(textPos.x,
                textPos.y);
        Shape bgs = layout.getLogicalHighlightShape(0, readoutString.length());
        m_centerBound = at.createTransformedShape(bgs);

        m_bounds.setRect(m_leftBound.getBounds2D());
        m_bounds.add(m_rightBound.getBounds2D());
        m_bounds.add(m_centerBound.getBounds2D());

        drawReadout(g, layout, textPos);
        drawLeaderLines(g, p);
    }

    protected void drawReadout(Graphics2D g, TextLayout layout,
                               Point2D.Double textPos) {
        // Draw the measurement readout.
        if (m_highlightItem == 1) {
            g.setColor(java.awt.Color.DARK_GRAY);
            g.fill(m_centerBound);
        }

        g.setColor(m_textColor);
        layout.draw(g, (float) textPos.x, (float) textPos.y);
    }

    protected void drawLeaderLines(Graphics2D g, Point2D.Double p[]) {
        Line2D.Double line = new Line2D.Double();
        AffineTransform existingTransform = g.getTransform();
        Stroke existingStroke = g.getStroke();

        g.transform(m_currentTransform);
        g.setColor(m_lineColor);

        if (m_highlightItem == LEFT_ITEM) {
            g.setStroke(HIGHLIGHT_STROKE);
        } else {
            g.setStroke(REGULAR_STROKE);
        }
        line.setLine(p[0], p[1]);
        g.draw(line);

        if (m_highlightItem == RIGHT_ITEM) {
            g.setStroke(HIGHLIGHT_STROKE);
        } else {
            g.setStroke(REGULAR_STROKE);
        }
        line.setLine(p[3], p[4]);
        g.draw(line);

        g.setStroke(REGULAR_STROKE);
        line.setLine(p[0].x, p[0].y - MARGIN, p[0].x, p[0].y + MARGIN);
        g.draw(line);
        line.setLine(p[4].x, p[4].y - MARGIN, p[4].x, p[4].y + MARGIN);
        g.draw(line);

        g.setStroke(existingStroke);
        g.setTransform(existingTransform);
    }

    /**
     * This method handles the mouse pressed events. The method checks if the
     * mouse is pressed with in the distance control, if not it simply ignores
     * the event. If the event is infact destined for the distance control, then
     * the method sees if the mouse is pressed in the middle or at the sides of
     * the control. If the mouse is pressed in the middle then the method
     * initiates the 'move control' logic, else it initiates the 'resize or
     * redraw' logic. The method also marks the distance control as selected so
     * that further processing can be done.
     *
     * @param MouseEvent the mouse event containing various important information,
     *                   passed on to the method.
     */
    public void mousePressed(MouseEvent e) {

        if (e.isPopupTrigger()) {
            handlePopupTrigger(e);
        } else if (SwingUtilities.isLeftMouseButton(e)) {

            // in the conditions below we see if the user mouse is in extreme
            // vicinity
            // of the distance line. Only if it is, do we consume the event.
            // The m_bounds.contains() function above
            // will return true even if the mouse is pressed with in the
            // rectangle
            // whose diagnoal is this distance control.

            if (m_centerBound.contains(e.getX(), e.getY()) || e.isAltDown()) {
                m_pressPosition = new Point2D.Double(e.getX(), e.getY());
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
                e.consume();
                m_interactionState = CENTER_ITEM;
                // Mark the component as selected. This information will be used
                // when the user presses Delete key to delete the distance
                // control.
                m_selected = true;
            } else if (m_leftBound.contains(e.getX(), e.getY())) {
                m_pressPosition = new Point2D.Double(e.getX(), e.getY());
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
                e.consume();
                m_interactionState = LEFT_ITEM;
                // Mark the component as selected. This information will be used
                // when the user presses Delete key to delete the distance
                // control.
                m_selected = true;
            } else if (m_rightBound.contains(e.getX(), e.getY())) {
                m_pressPosition = new Point2D.Double(e.getX(), e.getY());
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
                e.consume();
                m_interactionState = RIGHT_ITEM;
                // Mark the component as selected. This information will be used
                // when the user presses Delete key to delete the distance
                // control.
                m_selected = true;
            } else {
                m_selected = false;
            }
        }
    }

    /**
     * The mandatory method that must be implemented as per the requirements of
     * the parent interface. However the Distance control is not interested in
     * any mousewheel events so the method is just an empty stub and does not do
     * anything.
     *
     * @param MouseWheelEvent e
     */

    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            handlePopupTrigger(e);
        } else if (javax.swing.SwingUtilities.isLeftMouseButton(e)) {
            if (m_pressPosition != null) {
                m_pressPosition = null;
                m_interactionState = 0;
            }

            // // if the release is near me then consume the event. so that an
            // annotation control
            // // that overlaps with me does not get selected.
            // if (m_bounds.contains(e.getX(), e.getY()))
            // {
            // e.consume();
            // }
        }
    }

    protected void handlePopupTrigger(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        if (m_bounds.contains(e.getX(), e.getY())) {
            JPopupMenu popup = getPopupMenu();

            if (popup != null) {
                e.consume();
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.isConsumed()) {
                return;
            }

            if (m_pressPosition != null) {
                Point2D.Double delta = new Point2D.Double(e.getX()
                        - m_pressPosition.x, e.getY() - m_pressPosition.y);

                if (isFactoryControl()) {
                    if (m_interactionState == CENTER_ITEM) {
                        DistanceMeasure newOne = (DistanceMeasure) clone();
                        newOne.m_interactionState = CENTER_ITEM;
                        m_display.addControl(newOne);
                        m_pressPosition = null;
                        newOne.mouseDragged(e);
                    }
                } else if (m_interactionState == CENTER_ITEM) { // Move the
                    // whole
                    // measurement
                    // tool
                    m_location.x += delta.x;
                    m_location.y += delta.y;
                } else if (m_interactionState == LEFT_ITEM
                        || m_interactionState == RIGHT_ITEM) { // Move an
                    // endpoint
                    Point2D.Double endPoint = new Point2D.Double(m_location.x
                            + m_width * java.lang.Math.cos(m_angle),
                            m_location.y + m_width
                                    * java.lang.Math.sin(m_angle));
                    Point2D.Double p = (m_interactionState == LEFT_ITEM) ? m_location
                            : endPoint;
                    p.x += delta.x;
                    p.y += delta.y;
                    m_width = m_location.distance(endPoint);
                    m_angle = Math.atan2(endPoint.y - m_location.y, endPoint.x
                            - m_location.x);
                } else {
                    return;
                }

                e.consume();
                if (m_pressPosition != null) {
                    m_pressPosition.x = e.getX();
                    m_pressPosition.y = e.getY();
                }
                m_display.repaint();
            }
        }
    }

    public void mouseMoved(MouseEvent e) {
        if (m_centerBound.contains(e.getX(), e.getY())) {
            // cursor is set to default, because if the distance control
            // over laps with an annotation the annotation would have
            // set its own cursor based on its internal state (e.g. resize)
            // and if the cursor is moved to the distance control the user
            // needs to see a visual indication that he can manipulate the
            // distance
            // control.
            m_display.setCursor(Cursor.DEFAULT_CURSOR);

            m_highlightItem = CENTER_ITEM;
            m_display.repaint();
            e.consume();
        } else if (m_leftBound.contains(e.getX(), e.getY())
                && !isFactoryControl()) {
            // cursor is set to default, because if the distance control
            // over laps with an annotation the annotation would have
            // set its own cursor based on its internal state (e.g. resize)
            // and if the cursor is moved to the distance control the user
            // needs to see a visual indication that he can manipulate the
            // distance
            // control.
            m_display.setCursor(Cursor.DEFAULT_CURSOR);
            m_highlightItem = LEFT_ITEM;
            m_display.repaint();
            e.consume();
        } else if (m_rightBound.contains(e.getX(), e.getY())
                && !isFactoryControl()) {
            // cursor is set to default, because if the distance control
            // over laps with an annotation the annotation would have
            // set its own cursor based on its internal state (e.g. resize)
            // and if the cursor is moved to the distance control the user
            // needs to see a visual indication that he can manipulate the
            // distance
            // control.
            m_display.setCursor(Cursor.DEFAULT_CURSOR);
            m_highlightItem = RIGHT_ITEM;
            m_display.repaint();
            e.consume();
        } else if (m_highlightItem != NO_ITEM) {
            m_highlightItem = NO_ITEM;
            m_display.repaint();
        }
    }

    /**
     * <b>Description:</b> <br>
     * This method processes the key pressed while the distance control is
     * selected. Some of the keypressed events handled in this method include:
     * <ol>
     * <li> Delete Key: which is used to delete the distance control (same
     * affect as pressing right click and selecting "Delete" from the pop-up
     * menu.</li>
     * <li>Arrow Keys: Used to move the distance control with arrows. Same
     * affect as moving the distance control with mouse. </li>
     * </ol>
     * <i>Last Modified: 09/06/2005</i>
     *
     * @param <b>KeyEvent </b> The key event passed on to the method.
     */
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        // We shall only process the keys if the distance tool is selected and
        // if it
        // is not the Distance control located in the top left
        // corner (since the top left distance control is treated as immutable
        // and is
        // referred to as the FactoryControl).
        if (m_selected && !isFactoryControl()) {
            if (keyCode == KeyEvent.VK_DELETE
                    || keyCode == KeyEvent.VK_BACK_SPACE) {
                e.consume();
                DistanceMeasure.this.m_display
                        .removeControl(DistanceMeasure.this);
                DistanceMeasure.this.m_display.repaint();
            }
            // Handling the right/left/up/down arrow keys to move the
            // distance control.
            else if (keyCode == KeyEvent.VK_RIGHT) {
                m_location.x += 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_LEFT) {
                // if (m_location.x > 0)
                m_location.x -= 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_UP) {
                // if (m_location.y > 0)
                m_location.y -= 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_DOWN) {
                m_location.y += 1;
                e.consume();
                m_display.repaint();
            }
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    protected void setPopupMenuItemEventListener(JMenuItem item) {
        item.addActionListener(new EventListener());
        // TODO Auto-generated method stub
    }

    class EventListener implements java.awt.event.ActionListener {

        private boolean menuContainsItem(JPopupMenu menu, Object object) {
            if (menu == null || object == null) {
                return false;
            }

            for (int i = 0; i < menu.getComponentCount(); i++) {
                if (object.equals(menu.getComponent(i))) {
                    return true;
                }
            }
            return false;
        }

        public void actionPerformed(ActionEvent e) {
            if (menuContainsItem(DistanceMeasure.this.m_popupMenu, e
                    .getSource())) {
                performPopupMenuAction(e);
            }
        }

        private void performPopupMenuAction(ActionEvent e) {
            JMenuItem item = (JMenuItem) e.getSource();
            if (item.getActionCommand().equals("Delete")) {
                DistanceMeasure.this.m_display
                        .removeControl(DistanceMeasure.this);
                DistanceMeasure.this.m_display.repaint();
            }
        }
    }

}