/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.measure;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.font.TextLayout;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.Point2D.Double;
import java.text.NumberFormat;
import java.util.HashMap;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import virtuallab.MicroscopeView;
import virtuallab.gui.ViewControlAdapter;

// $Id: MeasureControlBase.java,v 1.5 2006/09/05 01:08:47 manzoor2 Exp $**
/* The base class for all the Measure Controls. This class provides
 * lot of useful implementation. The subclass only need to override
 * a handful of methods.
 *
 */
public abstract class MeasureControlBase extends ViewControlAdapter {
    /**
     *
     */
    protected static final double MARGIN = 5;
    /**
     * indicates that no item has been selected so no need to draw either a bold
     * line or a shadowed backgrond. The bold line or the shadowed background
     * indicate one of the two forms of highlighting that the distance measure
     * can have.
     */
    protected static final int NO_ITEM = 0;
    /**
     * Indicates that the mouse is on the center of the distance contrl. So we
     * should show the centered text with shadowed background.
     */
    protected static final int CENTER_ITEM = 1;
    /**
     * Indicates that the mouse is on left part of the distance control, So we
     * should show the distance line as bold.
     */
    protected static final int LEFT_ITEM = 2;
    /**
     * Indicates that the mouse is on right part of the distance control, So we
     * should show the distance line as bold.
     */
    protected static final int RIGHT_ITEM = 3;
    protected static final BasicStroke HIGHLIGHT_STROKE = new BasicStroke(3,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    protected static final BasicStroke REGULAR_STROKE = new BasicStroke(2,
            BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL);
    /**
     * This field will indicate if the control is selected or not. This
     * information is currently being used to handle keypressed and key typed
     * events. e.g. moving the selected distance control when arrow keys are
     * pressed or deleting it when delete key is pressed
     */
    protected boolean m_selected = false;
    protected Point2D.Double m_pressPosition = null;
    protected int m_interactionState = NO_ITEM;
    protected double FACTORY_CONTROL_X_COORDINATE;
    protected double FACTORY_CONTROL_Y_COORDINATE;
    /**
     * The location of the end point of the measure control
     */
    protected Point2D.Double m_endLocation;
    /**
     * Holds the image size at which this control was drawn. This information is
     * used to properly scale the control up/down when the magnification is changed.
     * At the time of creation of this control this variable willbe set to
     * MicroscopeView.m_currentImageSet.m_imageSize
     *
     * @see MicroscopeView#m_currentImageSet#m_imageSize
     */
    protected Dimension m_originalImageSize;
    protected JPopupMenu m_popupMenu;
    protected String m_popupMenuItemText;
    /**
     * The title of the measure control e.g. "Distance Measure", "Height Measure"
     */
    protected String m_title;
    protected HashMap m_textAttributes = new HashMap();
    /**
     * The width of the measure. This is the distance (in pixels) between the
     * two end points of the measure control.
     */
    protected double m_width = 100;
    protected AffineTransform m_currentTransform = new AffineTransform();
    protected Shape m_leftBound = new Rectangle2D.Double();
    protected Shape m_centerBound = new Rectangle2D.Double();
    protected Shape m_rightBound = new Rectangle2D.Double();
    protected Color m_textColor = Color.YELLOW;
    protected Color m_lineColor = Color.WHITE;
    protected Rectangle2D.Double m_bounds = new Rectangle2D.Double();
    /**
     * The start location of the measure control.
     */
    protected Point2D.Double m_location;
    /**
     * The angle of the measurecontrol (in radians). Used to draw the control at the
     * appropriate angle.
     */
    protected double m_angle = Math.toRadians(0);
    protected int m_highlightItem = NO_ITEM;
    protected Double m_originalStagePosition;

    /**
     * This method formats a string that will be shown on the measure e.g.
     * "444 um".
     *
     * @param numPixels the width of the control.
     * @param pixelSize the size that each pixel represents.
     * @return The String that will be displayed on the measure control.
     */
    public static String computeReadout(double numPixels, double pixelSize) {
        double m = numPixels * pixelSize;
        int powerOfTen = -6;

        if (m <= 1.0e-12) {
            return "0";
        }

        while (m >= 1000.0) {
            m /= 1000.0;
            powerOfTen += 3;
        }

        while (m < 1.0) {
            m *= 1000.0;
            powerOfTen -= 3;
        }
        String units = null;
        switch (powerOfTen) {
            case -12:
                units = " pm";
                break;
            case -9:
                units = " nm";
                break;
            case -6:
                units = " \u00b5m";
                break; // the Greek mu
            case -3:
                units = " mm";
                break;
            case 0:
                units = " m";
                break;
            case 3:
                units = " km";
                break;
            case 6:
                units = " Mm";
                break;
            case 9:
                units = " Gm";
                break;
            case 12:
                units = " Tm";
                break;
            default:
                units = " units?";
                break;
        }

        NumberFormat formatter = java.text.NumberFormat.getInstance();
        formatter.setMinimumFractionDigits(0);
        formatter.setMinimumIntegerDigits(1);

        if (m >= 100) {
            formatter.setMaximumFractionDigits(0);
        } else if (m >= 10) {
            formatter.setMaximumFractionDigits(1);
        } else {
            formatter.setMaximumFractionDigits(2);
        }

        return formatter.format(m) + units;
    }

    abstract public void draw(Graphics2D g);

    public MicroscopeView getDisplay() {
        return m_display;
    }

    public void setDisplay(MicroscopeView display) {
        m_display = display;
    }

    /**
     * This method builds up the pop-up menu for a particular measure
     * control. As of now this pop-up menu contains only one item which
     * allows the user to delete the control.
     * "Delete"
     *
     * @return
     */
    protected JPopupMenu buildPopupMenu() {
        JPopupMenu retVal = new JPopupMenu();

        JMenuItem item = new JMenuItem();
        item.setText(m_popupMenuItemText);
        item.setActionCommand("Delete");
        retVal.add(item);
        setPopupMenuItemEventListener(item);
        return retVal;
    }

    protected JPopupMenu getPopupMenu() {
        if (m_popupMenu == null) {
            m_popupMenu = buildPopupMenu();
        }

        Component items[] = m_popupMenu.getComponents();

        for (int i = 0; i < items.length; i++) {
            JMenuItem item = (JMenuItem) items[i];
            if (item.getActionCommand().equals("Delete")) {
                item.setEnabled(!isFactoryControl());
                break;
            }
        }

        return m_popupMenu;
    }

    /**
     * Sets the event listener for the pop-up menu. The eventlistener
     * will handle the pop-up menus events like "Delete" etc.
     *
     * @param item The JMenuItem for which the event listener will
     *             be set.
     */
    protected void setPopupMenuItemEventListener(JMenuItem item) {
        item.addActionListener(new MeasureEventListener(this));
    }

    /**
     * This method draws the lines that are shown before and after
     * the centre label of the measure control.
     *
     * @param g
     * @param lineEndPoints the end points which will be used to drawn the lines.
     */
    abstract protected void drawLeaderLines(Graphics2D g, Point2D.Double lineEndPoints[]);

    protected void drawReadout(Graphics2D g, TextLayout layout, Point2D.Double textPos) {
        // Draw the measurement readout.
        if (m_highlightItem == 1) {
            g.setColor(java.awt.Color.DARK_GRAY);
            g.fill(m_centerBound);
        }

        g.setColor(m_textColor);
        layout.draw(g, (float) textPos.x, (float) textPos.y);
    }

    protected void handlePopupTrigger(MouseEvent e) {
        if (e.isConsumed()) {
            return;
        }

        if (m_bounds.contains(e.getX(), e.getY())) {
            JPopupMenu popup = getPopupMenu();

            if (popup != null) {
                e.consume();
                popup.show(e.getComponent(), e.getX(), e.getY());
            }
        }
    }

    /**
     * This method handles the mouse pressed events. The method checks if the
     * mouse is pressed with in the measure control, if not it simply ignores
     * the event. If the event is infact destined for this measure control, then
     * the method sees if the mouse is pressed in the middle or at the sides of
     * the control. If the mouse is pressed in the middle then the method
     * initiates the 'move control' logic, else it initiates the 'resize or
     * redraw' logic. The method also marks the measure control as selected so
     * that further processing can be done.
     *
     * @param MouseEvent the mouse event containing various important information,
     *                   passed on to the method.
     */
    public void mousePressed(MouseEvent e) {

        if (e.isPopupTrigger()) {
            handlePopupTrigger(e);
        } else if (SwingUtilities.isLeftMouseButton(e)) {

            // in the conditions below we see if the user mouse is in extreme
            // vicinity
            // of the distance line. Only if it is, do we consume the event.
            // The m_bounds.contains() function above
            // will return true even if the mouse is pressed with in the
            // rectangle
            // whose diagnoal is this distance control.

            if (m_centerBound.contains(e.getX(), e.getY()) || e.isAltDown()) {
                m_pressPosition = new Point2D.Double(e.getX(), e.getY());
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
                e.consume();
                m_interactionState = CENTER_ITEM;
                // Mark the component as selected. This information will be used
                // when the user presses Delete key to delete the distance
                // control.
                m_selected = true;
            } else if (m_leftBound.contains(e.getX(), e.getY())) {
				/* FIXME: disabled until i fix the height measure logic
				m_pressPosition = new Point2D.Double(e.getX(), e.getY());
				m_display.setCursor(Cursor.DEFAULT_CURSOR);
				e.consume();
				m_interactionState = LEFT_ITEM;
				// Mark the component as selected. This information will be used
				// when the user presses Delete key to delete the distance
				// control.
				m_selected = true; */
            } else if (m_rightBound.contains(e.getX(), e.getY())) {
                m_pressPosition = new Point2D.Double(e.getX(), e.getY());
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
                e.consume();
                m_interactionState = RIGHT_ITEM;
                // Mark the component as selected. This information will be used
                // when the user presses Delete key to delete the distance
                // control.
                m_selected = true;
            } else {
                m_selected = false;
            }
        }
    }

    public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
            handlePopupTrigger(e);
        } else if (javax.swing.SwingUtilities.isLeftMouseButton(e)) {
            if (m_pressPosition != null) {
                m_pressPosition = null;
                m_interactionState = 0;
            }
        }
    }

    /**
     * This variable holds the previousStageposition which
     * corresponds to the stage position before changing. We need this to make sure
     * that the measurecontrols are moved along when the base image is moved/scrolled.
     * Keeping track of the previousstageposition and the new stageposition will
     * tell us how much do we need to translate the measurecontrol such that it
     * moves along with the base image everytime the baseimage is scrolled.
     *
     * @see MicroscopeView#m_stagePosition
     */
//	protected Point2D.Double m_previousStagePosition;
    abstract protected boolean isFactoryControl();

    abstract protected Object clone();

    /**
     * This metohd initializes the fields common to all the subclasses.
     *
     * @param newOne The subclass that is in the process of being cloned
     * @returnc The class with the fields initialized
     */
    protected MeasureControlBase initializeFields(MeasureControlBase newOne) {
        newOne.m_bounds.setRect(m_bounds);
        newOne.m_angle = m_angle;
        newOne.m_width = m_width;
        newOne.m_display = m_display;
        newOne.m_title = m_title;
        newOne.m_location.setLocation(m_location);
        newOne.m_endLocation = new Point2D.Double();
        newOne.m_endLocation.setLocation(m_location.x + m_width, m_location.y);
        newOne.m_currentTransform.setTransform(m_currentTransform);
        newOne.m_originalStagePosition = new Point2D.Double();
        newOne.m_originalStagePosition.setLocation(m_display.getStagePosition());

        newOne.m_originalImageSize = new Dimension(m_display.getCurrentImageSet()
                .getImageSize());

        newOne.m_leftBound = new Rectangle2D.Double(m_leftBound.getBounds2D()
                .getX(), m_leftBound.getBounds2D().getY(), m_leftBound
                .getBounds2D().getWidth(), m_leftBound.getBounds2D()
                .getHeight());

        newOne.m_centerBound = new Rectangle2D.Double(m_centerBound
                .getBounds2D().getX(), m_centerBound.getBounds2D().getY(),
                m_centerBound.getBounds2D().getWidth(), m_centerBound
                .getBounds2D().getHeight());

        newOne.m_rightBound = new Rectangle2D.Double(m_rightBound.getBounds2D()
                .getX(), m_rightBound.getBounds2D().getY(), m_rightBound
                .getBounds2D().getWidth(), m_rightBound.getBounds2D()
                .getHeight());

        if (m_pressPosition != null) {
            newOne.m_pressPosition = new Point2D.Double(m_pressPosition.x,
                    m_pressPosition.y);
        }
        newOne.m_textAttributes = m_textAttributes;
        newOne.m_textColor = m_textColor;
        newOne.m_lineColor = m_lineColor;
        return newOne;
    }

    public void mouseDragged(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.isConsumed()) {
                return;
            }

            if (m_pressPosition != null) {
                Point2D.Double delta = new Point2D.Double(e.getX() - m_pressPosition.x, e.getY() - m_pressPosition.y);

                if (isFactoryControl()) {
                    if (m_interactionState == CENTER_ITEM) {
                        MeasureControlBase newOne = (MeasureControlBase) clone();
                        newOne.m_interactionState = CENTER_ITEM;
                        m_display.addControl(newOne);
                        m_pressPosition = null;
                        newOne.mouseDragged(e);
                    }
                } else if (m_interactionState == CENTER_ITEM) {
                    // Move the whole measurement tool
                    m_location.x += delta.x;
                    m_location.y += delta.y;
                    m_endLocation.x += delta.x;
                    m_endLocation.y += delta.y;
                } else if (m_interactionState == LEFT_ITEM || m_interactionState == RIGHT_ITEM) { // Move an
                    // endpoint
                    m_endLocation.setLocation(e.getX(), e.getY());
                    Point2D.Double endPoint = new Point2D.Double(m_location.x + m_width * java.lang.Math.cos(m_angle),m_location.y + m_width * java.lang.Math.sin(m_angle));
                    if (m_interactionState == LEFT_ITEM) {
                        m_location.x += delta.x;
                        m_location.y += delta.y;
                    } else {
                        endPoint.x += delta.x;
                        endPoint.y += delta.y;
                    }
                    m_width = m_location.distance(endPoint);
                    m_angle = Math.atan2(endPoint.y - m_location.y, endPoint.x - m_location.x);
                } else {
                    return;
                }

                e.consume();
                if (m_pressPosition != null) {
                    m_pressPosition.x = e.getX();
                    m_pressPosition.y = e.getY();
                }
                m_display.repaint();
            }
        }
    }

    public void mouseMoved(MouseEvent e) {
        if (m_centerBound.contains(e.getX(), e.getY())) {
            // cursor is set to default, because if the distance control
            // over laps with an annotation the annotation would have
            // set its own cursor based on its internal state (e.g. resize)
            // and if the cursor is moved to the distance control the user
            // needs to see a visual indication that he can manipulate the
            // distance
            // control.
            m_display.setCursor(Cursor.DEFAULT_CURSOR);

            m_highlightItem = CENTER_ITEM;
            m_display.repaint();
            e.consume();
        } else if (m_leftBound.contains(e.getX(), e.getY())
                && !isFactoryControl()) {
            // FIXME: Disabled until i fix the height measure tool
			/*
			m_display.setCursor(Cursor.DEFAULT_CURSOR);
			m_highlightItem = LEFT_ITEM;
			m_display.repaint();
			e.consume();
			*/
        } else if (m_rightBound.contains(e.getX(), e.getY())
                && !isFactoryControl()) {
            m_display.setCursor(Cursor.DEFAULT_CURSOR);
            m_highlightItem = RIGHT_ITEM;
            m_display.repaint();
            e.consume();
        } else if (m_highlightItem != NO_ITEM) {
            m_highlightItem = NO_ITEM;
            m_display.repaint();
        }
    }

    /**
     * <b>Description:</b> <br>
     * This method processes the key pressed while the distance control is
     * selected. Some of the keypressed events handled in this method include:
     * <ol>
     * <li> Delete Key: which is used to delete the distance control (same
     * affect as pressing right click and selecting "Delete" from the pop-up
     * menu.</li>
     * <li>Arrow Keys: Used to move the distance control with arrows. Same
     * affect as moving the distance control with mouse. </li>
     * </ol>
     *
     * @param KeyEvent. The key event passed on to the method.
     */
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();
        // We shall only process the keys if the distance tool is selected and
        // if it is not the FactoryControl (the Distance control located in
        // the top left corner is treated as immutable and is referred to
        // as the FactoryControl).
        if (m_selected && !isFactoryControl()) {
            if (keyCode == KeyEvent.VK_DELETE
                    || keyCode == KeyEvent.VK_BACK_SPACE) {
                e.consume();
                m_display.removeControl(this);
                m_display.repaint();
            }
            // Handling the right/left/up/down arrow keys to move the
            // distance control.
            else if (keyCode == KeyEvent.VK_RIGHT) {
                m_location.x += 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_LEFT) {
                // if (m_location.x > 0)
                m_location.x -= 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_UP) {
                // if (m_location.y > 0)
                m_location.y -= 1;
                e.consume();
                m_display.repaint();
            } else if (keyCode == KeyEvent.VK_DOWN) {
                m_location.y += 1;
                e.consume();
                m_display.repaint();
            }
        }
    }

    public void mouseWheelMoved(MouseWheelEvent e) {

    }

    /**
     * This method calculates the factor by which the line's width has
     * grown or shrunk. At the point this method is called all that is known
     * is the line start position in the original coordinates and the width
     * of the line. We use some trivial maths to find out the new width for
     * the line.
     *
     * @return the ratio between the (the new width/old width).
     */
    protected float calclateWidthScaling() {
        Dimension currentImageSize = m_display.getCurrentImageSet().getImageSize();
        double curScaleX = currentImageSize.getWidth() / m_originalImageSize.getWidth();
        double curScaleY = currentImageSize.getHeight() / m_originalImageSize.getHeight();

        // Sin(theta) * hypotenuse = perpendicular.
        double hypotenuse = m_width;
        double perpendicular = hypotenuse * Math.sin(m_angle);
        double baseYCoordinate = m_location.getY() - perpendicular;
        // c = y - mx
        double yIntercept = m_location.getY() - m_angle * m_location.getX();
        double baseRightXCoordinate = m_location.x + m_width;
        if (m_angle != 0)
            // x = (y - c)/m
            baseRightXCoordinate = (baseYCoordinate - yIntercept) / m_angle;

        // transferring all the coordinates into the current mag level.
        Point2D.Double newLocationStart = new Point2D.Double(curScaleX * m_location.x,
                curScaleY * m_location.y);
        Point2D.Double newLocationEnd = new Point2D.Double(curScaleX * baseRightXCoordinate,
                curScaleY * baseYCoordinate);
        return (float) (newLocationStart.distance(newLocationEnd) / m_width);
    }

    /**
     * This method applies the necessary transformations to the controlmeasure.
     * We have to deal with the following three different coordinate systems:
     * <ol>
     * <li><b>The physical screen/viewport coordinate system:</b><br>
     * This is the screen coordinates on which the controlmeasure will be drawn.</li>
     * <li><b>The original image coordinate system:</b><br>
     * This is the coordinate system that relates to the image at the time the
     * controlmeasure was drawn. This coordinate system has its origin at the
     * value of MicroscopeView.m_stagePosition at the time when this control
     * measure was drawn.</li>
     * <li><b>The current image coordinate system:</b><br>
     * This is the system that relates to the image tiles that are currently being
     * viewed. This coordinate system has its origin at the current value of the
     * MicroscopeView.m_stagePosition.</li>
     * </ol>
     * <b>Example calculations:</b>
     * <ul><li>
     * The user creates the new controlmeasure and drops it at coordinate (10,10) relative to the
     * screen. This controlmeasure will show at pixel (10,10) on the screen i.e. at the point
     * 10 pixels to the right and 10 pixels down. At this point we will look at the current
     * stage position and the image size to find out the position of this point in the image
     * coordinates. Assume that the current image size is (512x512) and that the stage position
     * (i.e. the position of the origin of the image coordinates w.r.t the screen coordinates)
     * is (-100, -100). What this means is that the first pixel of the image is at
     * (-100,-100) from the screen coordinates origin(0,0).
     * Now if the user drags the control to the new position (20,20) this would mean that
     * the new position of the control in the image coordinates becomes (120,120).
     * Now the user changes the magnification level and the image size becomes (1024x1024).
     * This means that what was (120,120) in the previous image coordinates now becomes
     * (240,240) in the current image coordinates.
     * The following formulae is used to find the appropriate translation for the control:<br>
     * <code>Let<br>
     * P    = original coordinates of the controlmeasure in the screen coordinates.<br>
     * P''  = coordinates of the controlmeasure in the current image coordinates.<br>
     * SP'  = stage position of the original image coordinates.<br>
     * SF   = the ratio of the current image size/the original image size.<br>
     * SP'' = the stage position of the current image.<br>
     * <br>The formula used to calculate the translation is:<b>
     * P'' = [ (P - SP') * SF] + SP''</b>
     * </code><br>
     * All this translation is required because the controlmeasure is drawn starting at
     * (0,0) we then need to translate the controlmeasure so that is shows at the correct
     * position on the image. The situation is further complicated by the fact that
     * we have two additional requirements that we need to satisfy:<ol><li>
     * the controlmeasure should move along with the base image i.e. when the image
     * is scrolled the controlmeasure should scroll with it.</li><li>
     * when the magnification is changed the controlmeasure should stick to the
     * same position on the new image as it did in the old mag level.</li>
     * </li></ol>
     * Both the above requirements mandate the convoluated translation
     * equation.
     */
    protected void transformCoordinates() {
        if (isFactoryControl()) {
            // Factory control will Always be drawn at the top left of the visible
            // portion of the screen.
            m_currentTransform.setToTranslation(m_location.x, m_location.y);
        } else {
            Dimension currentImageSize = m_display.getCurrentImageSet().getImageSize();
            double scalingFactorX = currentImageSize.getWidth() / m_originalImageSize.getWidth();
            double scalingFactorY = currentImageSize.getHeight() / m_originalImageSize.getHeight();
            Point2D.Double currentStagePosition = m_display.getStagePosition();

            Point2D.Double newPosition = new Point2D.Double((m_location.x - m_originalStagePosition.x) * scalingFactorX + currentStagePosition.x,
                    (m_location.y - m_originalStagePosition.y) * scalingFactorY + currentStagePosition.y);

//			System.out.println("Absolute location " + m_location);
//			System.out.println("[("+ m_location.x + " - " + m_originalStagePosition.x + ") * " + scalingFactorX + "] + " +m_display.getStagePosition().x );
//			System.out.println("Relative " + newPosition);
//			System.out.println("Previous Stage position " + m_originalStagePosition);
//			System.out.println("Curr Stage position " + m_display.getStagePosition());
            m_currentTransform.setToTranslation(newPosition.x,
                    newPosition.y);
        }
        m_currentTransform.rotate(m_angle);
    }

    class MeasureEventListener implements java.awt.event.ActionListener {
        /**
         * Holds the reference to the control to which the
         * popup menu belongs to.This information is needed
         * to manipulate the control based on the pop-up menu
         * action e.g. if the Delete was pressed in the
         * popup menu then we would use this variable to
         * delete the control.
         */
        MeasureControlBase m_associatedControl;

        MeasureEventListener(MeasureControlBase associatedControl) {
            m_associatedControl = associatedControl;
        }

        private boolean menuContainsItem(JPopupMenu menu, Object object) {
            if (menu == null || object == null) {
                return false;
            }

            for (int i = 0; i < menu.getComponentCount(); i++) {
                if (object.equals(menu.getComponent(i))) {
                    return true;
                }
            }
            return false;
        }

        public void actionPerformed(ActionEvent e) {
            if (menuContainsItem(m_associatedControl.m_popupMenu, e.getSource())) {
                performPopupMenuAction(e);
            }
        }

        private void performPopupMenuAction(ActionEvent e) {
            JMenuItem item = (JMenuItem) e.getSource();
            if (item.getActionCommand().equals("Delete")) {
                m_associatedControl.getDisplay().removeControl(m_associatedControl);
                m_associatedControl.getDisplay().repaint();
            }
        }
    }


}