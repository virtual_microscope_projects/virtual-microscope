/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.afm;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBufferInt;
import java.awt.image.PixelGrabber;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import javax.swing.JFrame;

import virtuallab.gui.controls.annotation.Annotation;
import virtuallab.gui.controls.annotation.AnnotationControl;
import virtuallab.util.Utility;

import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;

import com.sun.opengl.util.BufferUtil;

import virtuallab.Log;


public class AFM3DRenderer extends WindowAdapter implements GLEventListener, KeyListener, MouseListener, MouseMotionListener {

    private static final Log log = new Log(AFM3DRenderer.class.getName());

    /**
     * Used to store the surface trangles creation, so that we do not issue
     * these commands everytime the display() is called.
     */
    private final int DISPLAY_LIST = 1;
    private GLU glu = new GLU();
    /**
     * by how much should we zoom in when the user presses Page-Up/Down.
     */
    private float m_zoomInSensitivity = 0.1f;
    /**
     * by how much should we zoom in when the user presses Page-Up/Down.
     */
    private float m_scrollSpeed = 0.1f;
    /**
     * Shows how much detail do we wish to show in the surface. If set to 1 then
     * ALL the 2D pixels will be shown in the 3D image, if set to say 3 then
     * every third pixel in the row and every third pixel in the column will be
     * shown i.e. the resulting 3D image will be created using total_pixels/9
     * pixels and hence surface details will be compromised but the rendering
     * will be much faster.
     */
    private int m_surfaceDetailsResolution = 1;
    /**
     * Switch the light on/off.
     */
    private boolean m_enableLight;
    /**
     * Holds the angle by which the image will be rotated around the X-Axis
     */
    private float m_xrot = 0;// -45f;

    /**
     * Holds the angle by which the image will be rotated around the Z-Axis
     */
    private float m_zrot = 0;// 45f;

    /**
     * Holds the fraction by which the xrot will be increased.
     */
    private double m_xspeed;

    /**
     * Holds the fraction by which the zrot will be increased.
     */
    private double m_zspeed;

    /**
     * The amount by which the image will be translated in the z direction. This
     * must be done else the image will not show up, since we draw the image on
     * a scale of 0 to +1 in the z-direction and the camera by default is placed
     * at 0. So we should translate the image to make sure it is far away from
     * the camera so that it becomes visible.
     */
    private float m_zTranslation = -2.00f;

    /**
     * The ambient light which will lighten up all the image uniformly
     */
    private float[] m_lightAmbient = {1.0f, 1.0f, 1.0f, 1.0f};

    /**
     * The diffuse light which will create the 3-D look for the surface.
     */
    private float[] m_lightDiffuse = {1.f, 1.0f, 1.0f, 1.0f};

    /**
     * The position of the light.
     */
    private float[] m_lightPosition = {0.0f, 0.0f, -1.0f, 1.0f};

    /**
     * Contains the vertices for each point in the 3D image. These vertices are
     * then used to create traingles that represent the 3D surface.
     */
    private float[][] m_vertices;

    /**
     * Contains the vertex normal for each of the vertices contained in
     * m_vertices. This array is used to average the vertex normals (a.k.a
     * surface normals) which is then used to set the vertex normal for each of
     * the three vertices of the triangle. The array has the dimensions [m_rows *
     * m_cols][4]. The array element [0..3] represent: [0]=normalX, [1]=normalY,
     * [2]=normalZ and [3]= the number of contributing triangles.
     *
     * @see m_vertices
     * @see createDisplayList()
     */
    private float[][] m_verticesNormals;

    /**
     * The width of the window that contains the 3D image
     */
    private double m_windowWidth;

    /**
     * The height of the window that contains the 3D image
     */
    private double m_windowHeight;

    /**
     * Image columns. m_rows x m_cols equals the image width.
     */
    private int m_cols;

    /**
     * Image rows. m_rows x m_cols equals the image width.
     */
    private int m_rows;

    /**
     * The two dimensional base image which needs to be converted into 3D.
     */
    private BufferedImage m_image2D;

    /**
     * Used to handle the image scrolling through mouse.
     */
    private Point m_previousMousePosition;

    /**
     * The annotation associated with this 3Dimage.
     */
    private Annotation m_associatedAnnotation;

    /**
     * reference to the annotation manager. Required so that we can delete the
     * associated annotation.
     */
    private AnnotationControl m_annotationManager;

    /**
     * The width to which the m_image2D and m_image2DTexture will be resized.
     * Irrespective of the dimensions of the selected area we would resize
     * it to a standard dimension. This will allow us to treat all the
     * images uniformly.
     * Caution: a value greater than 256 will require textur creation logic
     * to be revisited, since OpenGL can not handle textures greater than
     * 256 x 256 (there is a workaround but the current implementation of
     * AFM3DRenderer does not implement the workaround). Also openGL
     * requires that each dimension height/width be
     * a multiple of 2.
     *
     * @see IMAGE_HEIGHT
     */
    private double IMAGE_WIDTH = 256;

    /**
     * The height to which the m_image2D will be resized. Irrespective of the
     * dimensions of the selected area we would resize it to a standard
     * dimension. This will allow us to treat all the images uniformly.
     * Caution: a value greater than 256 will require textur creation logic
     * to be revisited, since OpenGL can not handle textures greater than
     * 256 x 256 (there is a workaround but the current implementation of
     * AFM3DRenderer does not implement the workaround). Also openGL
     * requires that each dimension height/width be
     * a multiple of 2.
     *
     * @see IMAGE_WIDTH
     */
    private double IMAGE_HEIGHT = 256;

    /**
     * Determines how many degrees rotation does a single pixel mouse movement
     * (in horizontal direction) translate into.
     */
    private float m_leftRightScrollSensitivity;

    /**
     * Determines how many degrees rotation does a single pixel mouse movement
     * (in vertical direction) translate into.
     */
    private float m_upDownScrollSensitivity;

    /**
     * Holds the reference to the texture
     */
    private int[] m_texture = new int[1];

    /**
     * Enable/disable texture.
     */
    private boolean m_enableTexture = true;

    /**
     * Holds the texture image.
     */
    private BufferedImage m_image2DTexture;


    /**
     * The one and only Constructor.
     *
     * @param image2D              the image which will be visualized in 3D.
     * @param annotationManager    The annotationcontrol required to delete the associated annotations
     *                             when the window is closed
     * @param associatedAnnotation The associated annotation. This reference is required
     *                             so that we can delete this when the window is closed
     * @param colorMap         the file containing the color map to be applied to the texture
     */
    //TODO colorMap is never used
    public AFM3DRenderer(BufferedImage image2D, BufferedImage texture2DImage, AnnotationControl annotationManager, Annotation associatedAnnotation, String colorMap) {
        if (Math.abs(image2D.getWidth() - image2D.getHeight()) > 10) {
            String errorMessage = "The selected area should atleast be an approximate square. " +
                    "The AFM3Renderer resizes the selected area to " + IMAGE_WIDTH +
                    " x " + IMAGE_HEIGHT + " \nThe 3D visualization may not work properly " +
                    "if the selection is not a perfect square.\nThe selection was found " +
                    " to be " + image2D.getWidth() + " x " + image2D.getHeight();
            log.error(errorMessage);
        }
        m_image2D = Utility.resizeImage(image2D, (double) IMAGE_WIDTH / image2D.getWidth(), (double) IMAGE_HEIGHT / image2D.getHeight(), AffineTransformOp.TYPE_BILINEAR);
        texture2DImage = Utility.resizeImage(texture2DImage, (double) IMAGE_WIDTH / texture2DImage.getWidth(), (double) IMAGE_HEIGHT / texture2DImage.getHeight(), AffineTransformOp.TYPE_BILINEAR);

        m_annotationManager = annotationManager;
        m_associatedAnnotation = associatedAnnotation;
        m_image2DTexture = alignTextureWithBaseImage(texture2DImage);
    }

    /**
     * This method is the heart of the 3D image creation. It identifies the
     * vertices of each pixel in the 3D image, it joins the vertices into
     * triangles and uses averaged normals for each of the vertex, thus
     * resulting in a smooth 3D surface image.
     *
     * @param gl
     */
    private void createDisplayListWithVertexNormals(GL gl) {
        int index = 0;
        int rows = 0;
        int cols = 0;
        float minX = 1;
        float maxX = -1;
        float minY = 1;
        float maxY = -1;
        float minZ = 1;
        float maxZ = -1;
        float maxDimension = (m_image2D.getHeight() > m_image2D.getWidth() ? m_image2D.getHeight() : m_image2D.getWidth());

        m_rows = m_image2D.getHeight();
        m_cols = m_image2D.getWidth();
        m_vertices = new float[m_rows * m_cols][3];
        m_verticesNormals = new float[m_rows * m_cols][4];
        for (int i = 0; i < m_rows; i += m_surfaceDetailsResolution) {
            rows++;
            cols = 0;
            for (int j = 0; j < m_cols; j += m_surfaceDetailsResolution) {
                cols++;
                float[] coordinates = new float[3];
                int rgb = 0;
                try {
                    rgb = m_image2D.getRGB(j, i);
                } catch (Exception ex) {
                    String errorInfo = " i " + i + " j " + j;
                    log.error(errorInfo);
                    log.error(ex.getMessage());
                }
                // convert to HSB values.
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = rgb & 0xFF;
                float[] inputHSB = new float[3];
                inputHSB = Color.RGBtoHSB(red, green, blue, inputHSB);

                coordinates[0] = ((float) i / maxDimension);
                coordinates[1] = ((float) j / maxDimension);
                coordinates[2] = inputHSB[2];
                m_vertices[index++] = coordinates;

                // figuring out the bounding cube for this surface.
                if (coordinates[0] < minX)
                    minX = coordinates[0]; // new minimum found
                if (coordinates[0] > maxX)
                    maxX = coordinates[0]; // new maximum found

                if (coordinates[1] < minY)
                    minY = coordinates[1]; // new minimum found
                if (coordinates[1] > maxY)
                    maxY = coordinates[1]; // new maximum found

                if (coordinates[2] < minZ)
                    minZ = coordinates[2]; // new minimum found
                if (coordinates[2] > maxZ)
                    maxZ = coordinates[2]; // new maximum found
            }
        }
        m_rows = rows;
        m_cols = cols;
        calculateSurfaceNormals(rows, cols);
        gl.glNewList(DISPLAY_LIST, GL.GL_COMPILE);
        // float[] mat = {1f,0f,0f};//new float[3];
        // gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE,
        // mat);

        gl.glRotatef(-90f, 0.0f, 0.0f, 1.0f);
        gl.glTranslatef(-(minX + (maxX - minX) / 2),
                -(minY + (maxY - minY) / 2), -(minZ + (maxZ - minZ) / 2));
        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_FILL);
        gl.glBegin(GL.GL_TRIANGLES);
        for (int i = 0; i < rows - 1; i++) {
            int contributingTriangles = 4;
            for (int j = 0; j < cols - 1; j++) {
                // drawing triangles anti-clockwise.
                int indexV1 = j + (i * cols);

                contributingTriangles = (int) m_verticesNormals[indexV1 + 1][3];
                gl.glNormal3f(m_verticesNormals[indexV1 + 1][0]
                                / contributingTriangles,
                        m_verticesNormals[indexV1 + 1][1]
                                / contributingTriangles,
                        m_verticesNormals[indexV1 + 1][2]
                                / contributingTriangles);

                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1]));

                contributingTriangles = (int) m_verticesNormals[indexV1][3];
                gl.glNormal3f(m_verticesNormals[indexV1][0]
                        / contributingTriangles, m_verticesNormals[indexV1][1]
                        / contributingTriangles, m_verticesNormals[indexV1][2]
                        / contributingTriangles);
                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1]));

                contributingTriangles = (int) m_verticesNormals[indexV1 + 1
                        + cols][3];
                gl.glNormal3f(m_verticesNormals[indexV1 + 1 + cols][0]
                        / contributingTriangles, m_verticesNormals[indexV1 + 1
                        + cols][1]
                        / contributingTriangles, m_verticesNormals[indexV1 + 1
                        + cols][2]
                        / contributingTriangles);
                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1 + cols]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1 + cols]));

                contributingTriangles = (int) m_verticesNormals[indexV1][3];
                gl.glNormal3f(m_verticesNormals[indexV1][0]
                        / contributingTriangles, m_verticesNormals[indexV1][1]
                        / contributingTriangles, m_verticesNormals[indexV1][2]
                        / contributingTriangles);
                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1]));

                contributingTriangles = (int) m_verticesNormals[indexV1 + cols][3];
                gl.glNormal3f(m_verticesNormals[indexV1 + cols][0]
                        / contributingTriangles, m_verticesNormals[indexV1
                        + cols][1]
                        / contributingTriangles, m_verticesNormals[indexV1
                        + cols][2]
                        / contributingTriangles);
                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1 + cols]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1 + cols]));

                contributingTriangles = (int) m_verticesNormals[indexV1 + 1
                        + cols][3];
                gl.glNormal3f(m_verticesNormals[indexV1 + 1 + cols][0]
                        / contributingTriangles, m_verticesNormals[indexV1 + 1
                        + cols][1]
                        / contributingTriangles, m_verticesNormals[indexV1 + 1
                        + cols][2]
                        / contributingTriangles);
                gl.glTexCoord3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1 + cols]));
                gl.glVertex3fv(FloatBuffer.wrap(m_vertices[indexV1 + 1 + cols]));
            }
        }
        gl.glEnd();
        // TODO: Test code to show the bounding box. Remove later.
        // drawBoundingBox(gl, minX, maxX, minY, maxY, minZ, maxZ);

        gl.glEndList();
    }

    /**
     * This method calculates the normal corresponding to each vertex. Each
     * vertex participates in four traingles (except of vertices on the edge
     * which contribute towards two trianglesonly), this means that each vertex
     * can have four normals we take all these normal contributions and set them
     * in an array against each vertex. This method sets the consolidate normal
     * for each of the vertices. Later while drawing the triangles we shall
     * divide this consolidated normal to get its average (by dividing it by
     * four (non-edge vertex) or two (edge vertex)
     *
     * @param rows
     * @param cols
     */
    private void calculateSurfaceNormals(int rows, int cols) {
        for (int i = 0; i < rows - 1; i++) {
            for (int j = 0; j < cols - 1; j++) {
                // each iteration of this loop draws two triangles.
                int indexV1 = j + (i * cols);

                float vertexNormal1[] = calculateVertexNormal(
                        m_vertices[indexV1 + 1], m_vertices[indexV1],
                        m_vertices[indexV1 + 1 + cols]);
                float vertexNormal2[] = calculateVertexNormal(
                        m_vertices[indexV1], m_vertices[indexV1 + cols],
                        m_vertices[indexV1 + 1 + cols]);

                // Setting the normal contribution for each vertex of the first
                // triangle
                m_verticesNormals[indexV1 + 1][0] += vertexNormal1[0];
                m_verticesNormals[indexV1 + 1][1] += vertexNormal1[1];
                m_verticesNormals[indexV1 + 1][2] += vertexNormal1[2];
                m_verticesNormals[indexV1 + 1][3]++;

                m_verticesNormals[indexV1][0] += vertexNormal1[0];
                m_verticesNormals[indexV1][1] += vertexNormal1[1];
                m_verticesNormals[indexV1][2] += vertexNormal1[2];
                m_verticesNormals[indexV1][3]++;

                m_verticesNormals[indexV1 + 1 + cols][0] += vertexNormal1[0];
                m_verticesNormals[indexV1 + 1 + cols][1] += vertexNormal1[1];
                m_verticesNormals[indexV1 + 1 + cols][2] += vertexNormal1[2];
                m_verticesNormals[indexV1 + 1 + cols][3]++;

                // Setting the normal contribution for each vertex of the second
                // triangle
                m_verticesNormals[indexV1][0] += vertexNormal2[0];
                m_verticesNormals[indexV1][1] += vertexNormal2[1];
                m_verticesNormals[indexV1][2] += vertexNormal2[2];
                m_verticesNormals[indexV1][3]++;

                m_verticesNormals[indexV1 + cols][0] += vertexNormal2[0];
                m_verticesNormals[indexV1 + cols][1] += vertexNormal2[1];
                m_verticesNormals[indexV1 + cols][2] += vertexNormal2[2];
                m_verticesNormals[indexV1 + cols][3]++;

                m_verticesNormals[indexV1 + 1 + cols][0] += vertexNormal2[0];
                m_verticesNormals[indexV1 + 1 + cols][1] += vertexNormal2[1];
                m_verticesNormals[indexV1 + 1 + cols][2] += vertexNormal2[2];
                m_verticesNormals[indexV1 + 1 + cols][3]++;
            }
        }

    }

    /**
     * Draws the image bounding rectangle box and the coordinate system lines
     * these gives the user an idea of orientation of the image.
     *
     * @param gl
     * @param minX the top left corner's X coordinate
     * @param maxX the top right corner's X coordinate
     * @param minY the bottom left corner's Y coordinate
     * @param maxY the bottom right corner's Y coordinate
     * @param minZ the Z-coordinates of the back face of the bounding box.
     * @param maxZ the Z-coordinates of the front face of the bounding box.
     */
    private void drawBoundingBox(GL gl, float minX, float maxX, float minY,
                                 float maxY, float minZ, float maxZ) {
        gl.glPolygonMode(GL.GL_FRONT_AND_BACK, GL.GL_LINE);
        // float[] blue = {0, 0, 1};
        // gl.glMaterialfv(GL.GL_FRONT_AND_BACK, GL.GL_AMBIENT_AND_DIFFUSE,
        // blue);
        gl.glBegin(GL.GL_QUADS);
        gl.glNormal3f(0, 0, -1);
        gl.glVertex3f(maxX, minY, minZ);
        gl.glVertex3f(maxX, maxY, minZ);
        gl.glVertex3f(minX, maxY, minZ);
        gl.glVertex3f(minX, minY, minZ);

        gl.glNormal3f(0, 0, 1);
        gl.glVertex3f(maxX, minY, maxZ);
        gl.glVertex3f(maxX, maxY, maxZ);
        gl.glVertex3f(minX, maxY, maxZ);
        gl.glVertex3f(minX, minY, maxZ);

        gl.glNormal3f(1, 0, 0);
        gl.glVertex3f(maxX, minY, minZ);
        gl.glVertex3f(maxX, maxY, minZ);
        gl.glVertex3f(maxX, maxY, maxZ);
        gl.glVertex3f(maxX, minY, maxZ);

        gl.glNormal3f(-1, 0, 0);
        gl.glVertex3f(minX, minY, minZ);
        gl.glVertex3f(minX, maxY, minZ);
        gl.glVertex3f(minX, maxY, maxZ);
        gl.glVertex3f(minX, minY, maxZ);

        gl.glNormal3f(0, -1, 0);
        gl.glVertex3f(maxX, minY, maxZ);
        gl.glVertex3f(minX, minY, maxZ);
        gl.glVertex3f(minX, minY, minZ);
        gl.glVertex3f(maxX, minY, minZ);

        gl.glNormal3f(0, 1, 0);
        gl.glVertex3f(maxX, maxY, maxZ);
        gl.glVertex3f(minX, maxY, maxZ);
        gl.glVertex3f(minX, maxY, minZ);
        gl.glVertex3f(maxX, maxY, minZ);
        gl.glEnd();

        gl.glBegin(GL.GL_LINES);
        gl.glVertex3f(minX, 0, 0);
        gl.glVertex3f(maxX, 0, 0);

        gl.glVertex3f(0, minY, 0);
        gl.glVertex3f(0, maxY, 0);

        gl.glVertex3f(0, 0, minZ);
        gl.glVertex3f(0, 0, maxZ);

        gl.glEnd();
    }

    /**
     * Applies Vector cross product to obtain the vertex normal for the given
     * vertices.
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @param vertex3 the third vertex
     * @return the vertex normal for the three vertices calculated as (vertex1 -
     * vertex 2) CROSS_PRODUCT (vertex2 - vertex3)
     */
    private float[] calculateVertexNormal(float[] vertex1, float[] vertex2,
                                          float[] vertex3) {
        float normal[] = new float[3];
        float v1[] = new float[3];
        float v2[] = new float[3];
        // v1-v2
        v1[0] = vertex1[0] - vertex2[0];
        v1[1] = vertex1[1] - vertex2[1];
        v1[2] = vertex1[2] - vertex2[2];
        // v2-v3
        v2[0] = vertex2[0] - vertex3[0];
        v2[1] = vertex2[1] - vertex3[1];
        v2[2] = vertex2[2] - vertex3[2];

        // Cross Product: (v1-v2) X (v2-v3)
        normal[0] = v1[1] * v2[2] - v1[2] * v2[1];
        normal[1] = v1[2] * v2[0] - v1[0] * v2[2];
        normal[2] = v1[0] * v2[1] - v1[1] * v2[0];

        // normalize
        float d = (float) Math.sqrt(normal[0] * normal[0] + normal[1]
                * normal[1] + normal[2] * normal[2]);
        if (d != 0) {
            normal[0] /= d;
            normal[1] /= d;
            normal[2] /= d;
        }
        return normal;
    }

    /**
     * Called by the drawable to initiate OpenGL rendering by the client. After
     * all GLEventListeners have been notified of a display event, the drawable
     * will swap its buffers if necessary.
     *
     * @param gLDrawable The GLAutoDrawable object.
     */
    public void display(GLAutoDrawable gLDrawable) {
        final GL gl = gLDrawable.getGL();
        m_xrot += m_xspeed;
        m_zrot += m_zspeed;
        if (m_xrot > 0)
            m_xrot = 0;
        else if (m_xrot < -120)
            m_xrot = -120;

        gl.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity(); // Reset The View
        gl.glTranslatef(0.0f, 0.0f, m_zTranslation);
        gl.glRotatef(m_xrot, 1.0f, 0.0f, 0.0f); // rotate aorund x-axis
        gl.glRotatef(m_zrot, 0.0f, 0.0f, 1.0f);// rotate around z-axis

        if (m_enableTexture) {
            gl.glEnable(GL.GL_TEXTURE_2D);
            gl.glBindTexture(GL.GL_TEXTURE_2D, m_texture[0]);
        } else
            gl.glDisable(GL.GL_TEXTURE_2D);

        if (m_enableLight) {
            gl.glEnable(GL.GL_LIGHTING);
        } else {
            gl.glDisable(GL.GL_LIGHTING);
        }

        gl.glCallList(DISPLAY_LIST);
        gl.glFlush();

    }

    /**
     * Called when the display mode has been changed. <B>!! CURRENTLY
     * UNIMPLEMENTED IN JOGL !!</B>
     *
     * @param gLDrawable    The GLDrawable object.
     * @param modeChanged   Indicates if the video mode has changed.
     * @param deviceChanged Indicates if the video device has changed.
     */
    public void displayChanged(GLAutoDrawable gLDrawable, boolean modeChanged,
                               boolean deviceChanged) {
    }

    /**
     * Called by the drawable immediately after the OpenGL context is
     * initialized for the first time. Perform one-time OpenGL initialization
     * such as setup of lights and display lists.
     *
     * @param gLDrawable The GLAutoDrawable object.
     */
    public void init(GLAutoDrawable gLDrawable) {
        final GL gl = gLDrawable.getGL();

        /**
         * GL_SMOOTH works well with vertex normals i.e. use this when using
         * createDisplayListWithVertexNormals()
         */
        gl.glShadeModel(GL.GL_SMOOTH);
        /**
         * GL_FLAT works with surface normals i.e. use this when using
         * createDisplayListWithSurfaceNormals()
         */
        // gl.glShadeModel(GL.GL_FLAT); // Works with vertex normals
        gl.glClearColor(1.0f, 1.0f, 1.0f, 1.0f); // white Background
        gl.glClearDepth(1.0f); // Depth Buffer Setup
        /**
         * Enables Depth Testing i.e. ignore the objects that will be partially
         * or completely hidden.
         */
        gl.glEnable(GL.GL_DEPTH_TEST);
        gl.glDepthFunc(GL.GL_LEQUAL); // The Type Of Depth Testing To Do
        gl.glHint(GL.GL_PERSPECTIVE_CORRECTION_HINT, GL.GL_NICEST);
        gl.glEnable(GL.GL_TEXTURE_2D);

        // Creating the texture.
        gl.glGenTextures(1, m_texture, 0);
        gl.glBindTexture(GL.GL_TEXTURE_2D, m_texture[0]);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, GL.GL_NEAREST);
        gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, GL.GL_NEAREST);
        makeRGBTexture(gl, glu, m_image2DTexture, GL.GL_TEXTURE_2D, false);
        // Set up lighting
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_AMBIENT, FloatBuffer.wrap(m_lightAmbient));
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_DIFFUSE, FloatBuffer.wrap(m_lightDiffuse));
        gl.glLightfv(GL.GL_LIGHT1, GL.GL_POSITION, FloatBuffer.wrap(m_lightPosition));
        gl.glEnable(GL.GL_LIGHT1);
        gl.glEnable(GL.GL_LIGHTING);
        m_enableLight = true;
        gLDrawable.addKeyListener(this);
        createDisplayListWithVertexNormals(gl);
    }

    /**
     * This method takes in the raw texture image and flips it vertically and then rotates it
     * by 90 degrees around the center of the image. This reorientation is required so that
     * the texture should align properly with the base image.
     *
     * @param originalImage The raw image from which the texture will be formed, this is also
     *                      the base image out of which the 3D surface will be created.
     * @return The flipped (vertically) and rotated (90 degrees) texture image.
     */
    private BufferedImage alignTextureWithBaseImage(BufferedImage originalImage) {
        AffineTransform tx = AffineTransform.getScaleInstance(1, -1);
        tx.translate(0, -originalImage.getHeight(null));
        AffineTransformOp op = new AffineTransformOp(tx,
                AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        return rotateImageBy90DegreesAndApplyColorMap(op.filter(originalImage, null));
    }

    /**
     * This method takes in an image and rotates it by 90 degrees. We could have
     * used AffineTransform for rotation but for some strange reason the rotation
     * operation would change the image type to BufferedImage.TYPE_3BYTE_BGR. Instead
     * of fighting with the peculiarities of AffineTransform we perform the rotation
     * ourselves (90 degrees rotation is tirival to perform). This method may be slower
     * than the AffineTransform but we are only dealing with IMAGE_WIDTH x IMAGE_HEIGHT sized
     * image which is small enough not to hurt the performance too much.
     * This method also applies the color map. Both the colormapapplication and the
     * image rotation is done in a single loop due to which the performance hit is not that severe.
     *
     * @param originalImage
     * @return
     */
    private BufferedImage rotateImageBy90DegreesAndApplyColorMap(BufferedImage originalImage) {
        BufferedImage rotatedImage = new BufferedImage(originalImage
                .getHeight(), originalImage.getWidth(), originalImage.getType());
        int jj = 0;
        int ii = 0;
        int i = 0;
        int j = 0;
        try {
/*
			BufferedImage colormap = null;
			if (m_colorMapFile.getColorMapFile() != null){
				if (!m_colorMapFile.getColorMapFile().exists()){
					// case when the file was deleted after loading the specimen.
					JOptionPane.showMessageDialog(
							null,
							"The selected colormap '" + m_colorMapFile.getColorMapFile().getName() + "' was not found.\nPlease reload the colormap list by restarting the application.\n",
							"Information", JOptionPane.INFORMATION_MESSAGE);
				}else{
					colormap = ImageIO.read(m_colorMapFile.getColorMapFile());
				}
			}
			*/
            // rotate by 90 degrees and then translate by -height to
            // achieve the affect of rotation around the center of the
            // image.
            for (i = 0; i < originalImage.getHeight(); i++) {
                for (j = 0; j < originalImage.getWidth(); j++) {
                    ii = j;
                    jj = -i + (originalImage.getHeight() - 1);
//					rotatedImage.setRGB(jj, ii, originalImage.getRGB(j, i));
                    int inputRGB = originalImage.getRGB(j, i);
                    int red = (inputRGB >> 16) & 0xFF;
                    int green = (inputRGB >> 8) & 0xFF;
                    int blue = inputRGB & 0xFF;
//					if (colormap != null){
//						red = (colormap.getRGB(red,0) >> 16) & 0xFF;
//						green = (colormap.getRGB(green,0) >> 8) & 0xFF;
//						blue = (colormap.getRGB(blue,0) & 0xFF);
//					}
//					rotatedImage.setRGB(jj, ii, new Color(red, green, blue).getRGB());
                    // TODO: quick fix. Somehow the red and blues are all messed up i.e.
                    // blue shows up as red and viceversa. figure out why that is and
                    // then remove this hack. Seems like the image type is BGR instead of
                    // RGB.
                    rotatedImage.setRGB(jj, ii, new Color(blue, green, red).getRGB());
                }
            }
        } catch (Exception ex) {
            String errorInfo = " (i,j)=(" + i + ", " + j + ") and (ii, jj) = (" + ii + ", " + jj + ")";
            log.error(errorInfo);
            log.error(ex.getMessage());
        }
        return rotatedImage;


    }


    /**
     * This method transforms the texture in BufferedImage to a format
     * that OpenGL APIs expect.
     *
     * @param gl
     * @param glu
     * @param img
     * @param target
     * @param mipmapped
     */
    private void makeRGBTexture(GL gl, GLU glu, BufferedImage img, int target, boolean mipmapped) {
        if (false) {
            int[] packedPixels = new int[img.getWidth() * img.getHeight()];

            PixelGrabber pixelgrabber = new PixelGrabber(img, 0, 0, img.getWidth(), img.getHeight(), packedPixels, 0, img.getWidth());
            try {
                pixelgrabber.grabPixels();
            } catch (InterruptedException e) {
                throw new RuntimeException();
            }
            boolean storeAlphaChannel = true;
            int bytesPerPixel = storeAlphaChannel ? 4 : 3;
            ByteBuffer unpackedPixels = BufferUtil.newByteBuffer(packedPixels.length * bytesPerPixel);

            for (int row = img.getHeight() - 1; row >= 0; row--) {
                for (int col = 0; col < img.getWidth(); col++) {
                    int packedPixel = packedPixels[row * img.getWidth() + col];
                    unpackedPixels.put((byte) ((packedPixel >> 16) & 0xFF));
                    unpackedPixels.put((byte) ((packedPixel >> 8) & 0xFF));
                    unpackedPixels.put((byte) ((packedPixel >> 0) & 0xFF));
                    if (storeAlphaChannel) {
                        unpackedPixels.put((byte) ((packedPixel >> 24) & 0xFF));
                    }
                }
            }

            unpackedPixels.flip();

            if (mipmapped) {
                glu.gluBuild2DMipmaps(target, GL.GL_RGB8, img.getWidth(), img.getHeight(), GL.GL_RGB, GL.GL_UNSIGNED_BYTE, unpackedPixels);
            } else {
                gl.glTexImage2D(target, 0, GL.GL_RGB, img.getWidth(), img.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, unpackedPixels);
            }
            return;
        }


        ByteBuffer dest = null;
        switch (img.getType()) {
            case BufferedImage.TYPE_3BYTE_BGR:
            case BufferedImage.TYPE_CUSTOM: {
                String errorMessage = "Is TYPE_3BYTE_BGR or TYPE_CUSTOM";
                log.error(errorMessage);
                byte[] data = ((DataBufferByte) img.getRaster().getDataBuffer()).getData();
                dest = ByteBuffer.allocateDirect(data.length);
                dest.order(ByteOrder.nativeOrder());
                dest.put(data, 0, data.length);
                dest.rewind();
                break;
            }
            case BufferedImage.TYPE_INT_RGB: {
                String errorMessage = "Is TYPE_INT_RGB";
                log.error(errorMessage);
                int[] data = ((DataBufferInt) img.getRaster().getDataBuffer()).getData();
                dest = ByteBuffer.allocateDirect(data.length * BufferUtil.SIZEOF_INT);
                dest.order(ByteOrder.nativeOrder());
                dest.asIntBuffer().put(data, 0, data.length);
                dest.rewind();
                break;
            }
            default:
                throw new RuntimeException("Unsupported image type " + img.getType());
        }

        if (mipmapped) {
            glu.gluBuild2DMipmaps(target, GL.GL_RGB8, img.getWidth(), img.getHeight(), GL.GL_RGB, GL.GL_UNSIGNED_BYTE, dest);
        } else {
            gl.glTexImage2D(target, 0, GL.GL_RGB, img.getWidth(), img.getHeight(), 0, GL.GL_RGB, GL.GL_UNSIGNED_BYTE, dest);
        }
    }

    /**
     * Called by the drawable during the first repaint after the component has
     * been resized. The client can update the viewport and view volume of the
     * window appropriately, for example by a call to GL.glViewport(int, int,
     * int, int); note that for convenience the component has already called
     * GL.glViewport(int, int, int, int)(x, y, width, height) when this method
     * is called, so the client may not have to do anything in this method.
     *
     * @param gLDrawable The GLDrawable object.
     * @param x          The X Coordinate of the viewport rectangle.
     * @param y          The Y coordinate of the viewport rectanble.
     * @param width      The new width of the window.
     * @param height     The new height of the window.
     */
    public void reshape(GLAutoDrawable gLDrawable, int x, int y, int width,
                        int height) {
        m_windowWidth = width;
        m_windowHeight = height;
        final GL gl = gLDrawable.getGL();

        if (height <= 0) // avoid a divide by zero error!
            height = 1;
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL.GL_PROJECTION);
        gl.glLoadIdentity();
//		gl.gluPerspective(45.0f, h, 1.0, 20.0);
        glu.gluPerspective(45.0f, h, 1.0, 20.0);
        gl.glMatrixMode(GL.GL_MODELVIEW);
        gl.glLoadIdentity();
        // a mouse movement from pixel 0 to (width-1) should give one
        // complete rotations of the image.
        m_leftRightScrollSensitivity = (float) 360f / width;
        // a mouse movement from pixel 0 to (height-1) should give one
        // complete rotations of the image.
        m_upDownScrollSensitivity = (float) 360f / height;
//		gLDrawable.display();
    }

    /**
     * Handles image scrolling with keys
     *
     * @param e The KeyEvent.
     */
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
//		case KeyEvent.VK_L:
//			m_zspeed = 0;
//			m_xspeed = 0;
//			m_enableLight = !m_enableLight;
//			break;
            case KeyEvent.VK_PAGE_UP:
                m_zspeed = 0;
                m_xspeed = 0;
                m_zTranslation -= m_zoomInSensitivity;
                break;
            case KeyEvent.VK_PAGE_DOWN:
                m_zspeed = 0;
                m_xspeed = 0;
                m_zTranslation += m_zoomInSensitivity;
                break;
            case KeyEvent.VK_UP:
                m_zspeed = 0;
                if (m_xspeed > 0)
                    m_xspeed = 0;
                m_xspeed -= m_scrollSpeed;
                break;
            case KeyEvent.VK_DOWN:
                m_zspeed = 0;
                if (m_xspeed < 0)
                    m_xspeed = 0;
                m_xspeed += m_scrollSpeed;
                break;
            case KeyEvent.VK_RIGHT:
                m_xspeed = 0;
                if (m_zspeed < 0)
                    m_zspeed = 0;
                m_zspeed += m_scrollSpeed;
                break;
            case KeyEvent.VK_LEFT:
                m_xspeed = 0;
                if (m_zspeed > 0)
                    m_zspeed = 0;
                m_zspeed -= m_scrollSpeed;
                break;
//		case KeyEvent.VK_ESCAPE:
//			System.out.println("xrot " + m_xrot + " zrot " + m_zrot + " z "
//					+ m_zTranslation);
//			m_enableTexture = !m_enableTexture;
//			// animator.stop();
//			// System.exit(0);
//			break;
            default:
                m_xspeed = 0;
                m_zspeed = 0;
        }
        ((GLCanvas) e.getSource()).repaint();
    }

    /**
     * Handles the image scrolling with mouse movement.
     */
    public void mouseDragged(MouseEvent e) {
        Point currentPosition = e.getPoint();
        Rectangle2D screen = new Rectangle2D.Double(0, 0, m_windowWidth,
                m_windowHeight);
        if (screen.contains(e.getX(), e.getY())) {
            // we keep getting mousedragged events even when the user has
            // dragged out of the window, through this if() we shall
            // ignore if the user has stepped out of the window.
            double horizontalDisplacement = currentPosition.getX()
                    - m_previousMousePosition.getX();
            double verticalDisplacement = currentPosition.getY()
                    - m_previousMousePosition.getY();

            m_xspeed = verticalDisplacement * m_upDownScrollSensitivity;
            m_zspeed = horizontalDisplacement * m_leftRightScrollSensitivity;
            ((GLCanvas) e.getSource()).repaint();
        }
        m_previousMousePosition = currentPosition;
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void mouseMoved(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mousePressed(MouseEvent e) {
        m_previousMousePosition = e.getPoint();
    }

    public void mouseClicked(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseReleased(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void windowClosing(WindowEvent e) {
        m_annotationManager.removeAnnotation(m_associatedAnnotation);
        ((JFrame) e.getSource()).dispose();
    }
}
