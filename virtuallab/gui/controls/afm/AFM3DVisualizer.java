/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.afm;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import virtuallab.Configuration;
import virtuallab.MicroscopeView;
import virtuallab.Tile;
import virtuallab.Specimen.ControlStates;
import virtuallab.gui.MenuControlBase;
import virtuallab.gui.controls.annotation.Annotation;
import virtuallab.gui.controls.annotation.AnnotationControl;
import virtuallab.util.Constants;

import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;

import virtuallab.Log;
import virtuallab.util.Locale;


public class AFM3DVisualizer {

    private static final Log log = new Log(AFM3DVisualizer.class.getName());
    /**
     * The image that corresponds to the user selection. This image is based on
     * the highest magnification gray scale tiles. This forms the base for
     * constructing the 3D image.
     */
    BufferedImage m_selected2DGrayScaleImage = null;
    /**
     * The image that corresponds to the user selection. This image is based on
     * the highest magnification currently loaded/viewable tiles. This forms the
     * texture for the 3D image.
     */
    BufferedImage m_selected2DLoadedImage = null;
    private MicroscopeView m_display;
    private String m_basePathForHighestMagLoadedTiles;
    private ColormapMenu m_colorMapMenu;
    private String m_basePathForHighestMagGrayScaleTiles;

    public AFM3DVisualizer(MicroscopeView display, ColormapMenu colormapMenu) {
        m_display = display;
        m_colorMapMenu = colormapMenu;
    }

    /**
     * Gets the requested tile.
     *
     * @param tileId
     * @return
     * @throws Exception
     */
    private BufferedImage getImageTile(String tileId) throws Exception {
        return m_display.getImageTile(tileId);
    }

    public void cleanUp() {
    }

    /**
     * This method shows the selected 2D image in 3D.
     *
     * @param selectedArea
     */
    protected void show3DImage(Rectangle2D selectedArea, Cursor oldCursor, AnnotationControl annotationManager, Annotation associatedAnnotation, String colorMap) {
        try {
            m_display.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            try {
                if (selectedArea.getWidth() <= 0 || selectedArea.getHeight() <= 0) {
                    JOptionPane.showMessageDialog(null, Locale.InvalidSelection(Configuration.getApplicationLanguage())+".\n",
                            Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                    annotationManager.removeAnnotation(associatedAnnotation);
                    m_display.setCursor(oldCursor);
                    return;
                }
                createBaseAndTextureImage(selectedArea);
            } catch (IllegalArgumentException ie) {
                JOptionPane.showMessageDialog(null,
                        Locale.SelectionBoundaryError(Configuration.getApplicationLanguage())
                        +". "
                        +Locale.SelectionBoundaryErrorSupport(Configuration.getApplicationLanguage())
                        +".",
                        "\n"+Locale.Information(Configuration.getApplicationLanguage())+Constants.SPACE,
                        JOptionPane.INFORMATION_MESSAGE);
                log.error(Locale.SelectionBoundaryError(Locale.LANGUAGE.EN));
                log.error(ie.getMessage());

                annotationManager.removeAnnotation(associatedAnnotation);
                m_display.setCursor(oldCursor);
                return;
            }
            JFrame frame = new JFrame(Locale.VizualizationOfArea(Configuration.getApplicationLanguage())+Constants.SPACE+associatedAnnotation.getAnnotationText());

            GLCanvas canvas = new GLCanvas(new GLCapabilities());
            associatedAnnotation.setAssociatedWindow(frame);
            AFM3DRenderer renderer = new AFM3DRenderer(m_selected2DGrayScaleImage, m_selected2DLoadedImage, annotationManager, associatedAnnotation, colorMap);
            canvas.addGLEventListener(renderer);
            canvas.addMouseListener(renderer);
            canvas.addMouseMotionListener(renderer);
            frame.add(canvas);
            frame.setSize(400, 400);
            frame.addWindowListener(renderer);
            frame.setVisible(true);
            canvas.requestFocus();
            m_display.setCursor(oldCursor);
        } catch (Exception ex) {
            m_display.setCursor(oldCursor);
            JOptionPane.showMessageDialog(null,
                    Locale.ErrorDisplaying3DImmage(Configuration.getApplicationLanguage())
                    +". "
                    +Locale.TryAgain(Configuration.getApplicationLanguage())
                    +". \n"
                    +Locale.IfErrorPersists(Configuration.getApplicationLanguage())
                    +" "
                    +Constants.SUPPORT_EMAIL_ADDRESS,
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorDisplaying3DImmage(Locale.LANGUAGE.EN));
            annotationManager.removeAnnotation(associatedAnnotation);
            m_display.setCursor(oldCursor);
        }
    }

    /**
     * Takes in the selected area and figures out how the selection maps to the
     * AFM image tiles. After the tiles have been identified it reads in the
     * selected tiles and forms a single image that corresponds to the user
     * selection. Irrespective of which magnification level did the user choose
     * the selection area, we shall always dig into the highest magnification
     * tiles to form the selected area image. This helps us in getting rid of
     * data quality problems since the data at highest mag contains the
     * best available resolution.
     * A single image is returned to the caller. If the user
     * selection extends the boundary of the base image then the user selection
     * is clipped such that only the portion that overlaps with the base image
     * is returned.
     *
     * @param selectedArea the user selection
     * @throws Exception
     */
    private void createBaseAndTextureImage(Rectangle2D selectedArea) throws Exception {
        Rectangle2D areaToVisualize = clipSelectedArea(selectedArea.getBounds2D());
        if (areaToVisualize.getHeight() <= 0 || areaToVisualize.getWidth() <= 0)
            throw new IllegalArgumentException("Invalid selection");
        ControlStates highestMagControlState = ((MenuControlBase) m_display.getViewControl(Constants.Controls.MAGNIFICATION)).getLastValue();
        int standardTileWidth = highestMagControlState.getImageDescriptor().getTileSize().width;
        int standardTileHeight = highestMagControlState.getImageDescriptor().getTileSize().height;

        if (m_display.getCurrentImageSet().getPixelSize() != highestMagControlState.getImagePixelSize()) {
            // perform this only if the current magnification level is not the maximum mag.
            Point2D topLeft = new Point2D.Double(areaToVisualize.getMinX(), areaToVisualize.getMinY());
            Point2D bottomRight = new Point2D.Double(topLeft.getX() + areaToVisualize.getWidth(),topLeft.getY() + areaToVisualize.getHeight());
            topLeft = getPixelPositionInNewViewPort(topLeft, m_display.getCurrentImageSet().getPixelSize(), highestMagControlState.getImagePixelSize());
            bottomRight = getPixelPositionInNewViewPort(bottomRight, m_display.getCurrentImageSet().getPixelSize(), highestMagControlState.getImagePixelSize());
            areaToVisualize = new Rectangle2D.Double(topLeft.getX(), topLeft.getY(),bottomRight.getX() - topLeft.getX(),bottomRight.getY() - topLeft.getY());
        }
        m_selected2DGrayScaleImage = null;
        m_selected2DLoadedImage = null;

        int tileCoordinateX = -1;
        int tileCoordinateY = -1;
        Tile tileContainingThePixel = null;
        BufferedImage grayscaleTileImage = null;
        BufferedImage loadedTileImage = null;
        int calls = 0;
        int tileHeightDelta = 99999999;
        int tileWidthDelta = 99999999;

        for (int i = 0; i < (int) areaToVisualize.getHeight(); i++) {
            for (int j = 0; j < (int) areaToVisualize.getWidth(); j++) {
                tileCoordinateX = (int) (areaToVisualize.getY() + i) - tileHeightDelta;
                tileCoordinateY = (int) (areaToVisualize.getX() + j) - tileWidthDelta;
                if (tileCoordinateX < 0 || tileCoordinateY < 0 || tileCoordinateX >= grayscaleTileImage.getWidth() || tileCoordinateY >= grayscaleTileImage.getHeight()) {
                    // this loop ensures that we do the lengthy calculation only if needed.
                    calls++;
                    tileContainingThePixel = getHighestMagnificationTileThatContainsThePixel(new Point2D.Double(areaToVisualize.getX() + j, areaToVisualize.getY() + i));
                    grayscaleTileImage = getImageTile(m_basePathForHighestMagGrayScaleTiles + tileContainingThePixel.getFilePath());
                    loadedTileImage = getImageTile(m_basePathForHighestMagLoadedTiles + tileContainingThePixel.getFilePath());
                    tileHeightDelta = tileContainingThePixel.getRow() * standardTileHeight;//grayscaleTileImage.getHeight();//FIXME: this is wrong for the last row
                    tileWidthDelta = tileContainingThePixel.getCol() * standardTileWidth;//grayscaleTileImage.getWidth();//FIXME: this is wrong for the last column.
                    tileCoordinateX = (int) (areaToVisualize.getY() + i) - tileHeightDelta;
                    tileCoordinateY = (int) (areaToVisualize.getX() + j) - tileWidthDelta;

                }
                try {
                    if (m_selected2DGrayScaleImage == null) {
                        m_selected2DLoadedImage = new BufferedImage(
                                (int) areaToVisualize.getWidth(),
                                (int) areaToVisualize.getHeight(),
                                loadedTileImage.getType());
                        m_selected2DGrayScaleImage = new BufferedImage(
                                (int) areaToVisualize.getWidth(),
                                (int) areaToVisualize.getHeight(),
                                grayscaleTileImage.getType());
                    }
                    if (tileCoordinateX >= grayscaleTileImage.getHeight()
                            || tileCoordinateY >= grayscaleTileImage.getWidth()) {
                        /** This check is probably not needed anymore. Due to an earlier
                         * programming error we would sometime go pass the edges. After the
                         * fix this should not happen anymore.
                         * FIXME: Make sure the error is fixed and remove this condition.
                         */
                        m_selected2DGrayScaleImage.setRGB(j, i, 0);
                        m_selected2DLoadedImage.setRGB(j, i, 0);
                    } else {
                        int grayscaleImageRGB = grayscaleTileImage.getRGB(tileCoordinateY, tileCoordinateX);
                        int loadedImageRGB = loadedTileImage.getRGB(tileCoordinateY, tileCoordinateX);
                        m_selected2DGrayScaleImage.setRGB(j, i, grayscaleImageRGB);
                        m_selected2DLoadedImage.setRGB(j, i, loadedImageRGB);
                    }
                } catch (Exception ex) {
                    String errorInfo = j + ", " + i + " == " + tileCoordinateY + ", " + tileCoordinateX;
                    log.error(errorInfo);
                    log.error(ex.getMessage());
                }
            }
        }
    }

    /**
     * This method takes in the user selected area and figures out how much of
     * it corresponds to the base image. This clipping is required because the
     * user may have drawn the selection such that it extends the boundary of
     * the base image. In such a case we should clip the selection only to the
     * portion that corresponds to the base image.
     *
     * @param selectedArea The user selected Area
     * @return The selected area that corresponds to the base image.
     */
    private Rectangle2D clipSelectedArea(Rectangle2D selectedArea) {
        double baseImageWidth = m_display.getCurrentImageSet().getImageSize().getWidth();
        double baseImageHeight = m_display.getCurrentImageSet().getImageSize().getHeight();
        Rectangle2D baseImageRectangle = new Rectangle2D.Double(0, 0, baseImageWidth, baseImageHeight);
        return baseImageRectangle.createIntersection(selectedArea);
    }

    /**
     * Retreives the tile which contains the given point. The pixelPosition
     * corresponds to the pixel in the highest magnification viewport (which may
     * not be the currently loaded viewport) and the returned tile is the
     * Image Tile that contains that pixel.
     *
     * @param pixelPositionInHighestMagTile the position of the pixel in the highest magnification tile. To convert
     *                                      the current pixel position to the highest mag tile position use method getPixelPositionInNewViewPort()
     * @return
     * @see getPixelPositionInNewViewPort()
     */
    public Tile getHighestMagnificationTileThatContainsThePixel(Point2D pixelPositionInHighestMagTile) {
        MenuControlBase magControl = ((MenuControlBase) m_display.getViewControl(Constants.Controls.MAGNIFICATION));
        ControlStates highestMagControlState = magControl.getLastValue();
        Dimension highestMagTileSize = highestMagControlState.getImageDescriptor().getTileSize();
        ControlStates grayscaleImageState = m_colorMapMenu.getFirstValue();

        double highestMagTileRowSize = highestMagTileSize.getHeight();
        double highestMagTileColumnSize = highestMagTileSize.getWidth();
        int rowThatContainsThePixel = (int) (pixelPositionInHighestMagTile.getY() / highestMagTileRowSize);
        int columnThatContainsThePixel = (int) (pixelPositionInHighestMagTile.getX() / highestMagTileColumnSize);
        Tile targetTile = new Tile(rowThatContainsThePixel,
                columnThatContainsThePixel,
                (int) (columnThatContainsThePixel * highestMagTileColumnSize + m_display.getStagePosition().x),
                (int) (rowThatContainsThePixel * highestMagTileRowSize + m_display.getStagePosition().y));

        m_basePathForHighestMagLoadedTiles = m_display.getTilePath(m_display.getCurrentImageSet().getPath(), highestMagControlState.getDepth(), highestMagControlState.getFolderName());
        // we should base our 3D only on the highest mag tile corresponding to the gray scale image.
        m_basePathForHighestMagGrayScaleTiles = m_display.getTilePath(m_basePathForHighestMagLoadedTiles, grayscaleImageState.getDepth(), grayscaleImageState.getFolderName());
        return targetTile;
    }


    /**
     * This method takes in a pixel position in the current viewport (pixelSizeOfOldViewPort) and
     * returns the pixel position of this point in the new viewport (pixelSizeOfNewViewPort).
     *
     * @param pixelPosition
     * @param pixelSizeOfOldViewPort The pixel size of the tiles in the original viewport
     * @param pixelSizeOfNewViewPort The pixel size of the tiles in the highest mag viewport.
     * @return
     */
    public Point2D getPixelPositionInNewViewPort(Point2D pixelPosition, Point2D pixelSizeOfOldViewPort, Point2D.Double pixelSizeOfNewViewPort) {
        double magnificationFactorX = pixelSizeOfOldViewPort.getX() / pixelSizeOfNewViewPort.x;
        double magnificationFactorY = pixelSizeOfOldViewPort.getY() / pixelSizeOfNewViewPort.y;

        Point2D pixelPositionInHighestMagTile = new Point2D.Double(magnificationFactorX * pixelPosition.getX(),
                magnificationFactorY * pixelPosition.getY());
        return pixelPositionInHighestMagTile;
    }


    public String getBasePathForHighestMagLoadedTiles() {
        return m_basePathForHighestMagLoadedTiles;
    }

    public String getBasePathForHighestMagGrayScaleTiles() {
        return m_basePathForHighestMagGrayScaleTiles;
    }
}
