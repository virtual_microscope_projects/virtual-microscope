/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.afm;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;

import virtuallab.Configuration;
import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.Specimen.ControlStates;
import virtuallab.gui.MenuControlBase;
import virtuallab.gui.ViewControl;
import virtuallab.gui.controls.annotation.Annotation;
import virtuallab.gui.controls.annotation.AnnotationControlChangeListener;
import virtuallab.util.Constants;

import virtuallab.Log;
import virtuallab.util.Locale;

/**
 * This class represents the side control corresponding to AFM images.
 * TODO: Refactor the packages of ALL the controls' menus
 * to belong to virtuallab.gui.controls package. Currently they belong
 * to virtuallab.gui package.
 */
public class ColormapMenu extends MenuControlBase implements ViewControl, ItemListener, AnnotationControlChangeListener {

    private static final Log log = new Log(ColormapMenu.class.getName());
    /**
     * This variable holds the label number that will be appended to each
     * selection area. Everytime the user draws an area we shall label it and
     * use the same label as the title of the 3D image pop-up window. This
     * variable will be appended as postfix to the label.
     */
    int m_selectedAreaLabelNumber = 1;
    /**
     * The reference to the MicroscopeView. This is used to access the currently
     * loaded specimen and to set/reset the cursor etc.
     */
    private MicroscopeView m_display;
    /**
     * The togglebutton that is pressed to invoke the area selection on the base
     * AFM specimen.
     */
    private JToggleButton m_bttnArea;
    /**
     * Holds whether the area selection mode should be remain active even after
     * an area has been selected. This is invoked by pressing the area button
     * with right mouse click.
     */
    private boolean m_persistentMode;
    /**
     * is set to true when the user is drawing the area selection square. This
     * is used to manage the mouse event handling.
     */
    private boolean m_areaSelectionInProgress;
    /**
     * The annotation's original color before we start manipulating the
     * annotation controls under the hood
     */
    private Color m_oldColor;
    /**
     * The annotation's original line width before we start manipulating the
     * annotation controls under the hood
     */
    private float m_oldLineWidth;
    /**
     * Once the user has selected the base AFM image area this class is
     * delegated the rest of the 3D image drawing task.
     */
    private AFM3DVisualizer m_3dVisualizer;
    /**
     * the original cursor before we started changing it to our liking.
     */
    private Cursor m_oldCursor;
    /**
     * The combo box that contains the available colormaps.
     */
    private JComboBox m_colorModelCombo = null;

    private String m_oldSelection;


    /**
     * Gets called by the framework when this control is being created - much
     * like a constructor. We initialize the variables accordingly.
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        try {
            if (display == null || display.getCurrentSpecimen() == null || controlName == null) {
                throw new IllegalArgumentException("Null microscope view, specimen, or name");
            }
            m_display = display;
            m_3dVisualizer = new AFM3DVisualizer(m_display, this);
            if (m_controlName != null) {
                m_controlName = controlName;
            }
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    /**
     * This is where the detailed control gets created. We shall set the
     * internals of our AFM control overe here.
     */
    public void updateGUI(double version) {
        if (m_validControlStates == null) {
            return;
        }
        if (m_colorModelCombo != null)
            m_colorModelCombo.removeItemListener(this);
        removeAll();

        // The default application behavior is to use the "Control" Id
        // and change its case to title case and show that as the control title
        // on the GUI. this would mean that this control will show as "Colormap",
        // however we prefer to call this as "AFM". Note that in the rare case
        // when there are multiple AFM options then there would be another dropdown
        // dropdown control titled "AFM" as well. So if this happens we need
        // to resolve it and perhaps call one of these controls something
        // else
        m_title = Constants.Controls.AFM;
//		setLayout(new FlowLayout(FlowLayout.CENTER,1,0));
        setBorder(BorderFactory.createTitledBorder(m_title));
        setMaximumSize(new Dimension(1024, 60));

        setLayout(new GridBagLayout());

        JPanel afmPanel = createColormapPanel();
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 0);
        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 1.0;
        c.gridwidth = GridBagConstraints.REMAINDER;
        add(afmPanel, c);

    }

    /**
     * This method creates the required AFM specific tools and
     * inserts them inside the AFM side panel.
     */
    private JPanel createColormapPanel() {
        Insets bttnMargins = new Insets(1, 1, 1, 1);
        JPanel afmPanel = new JPanel();
        afmPanel.setLayout(new GridBagLayout());

        m_bttnArea = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_RECTANGLE)));
        m_bttnArea.setSelectedIcon(new ImageIcon(getClass().getResource(Constants.Resources.SELECTED_RECTANGLE)));
        m_bttnArea.setToolTipText("<html>" +
                Locale.Select3DArea(Configuration.getApplicationLanguage())
                +".<br><i>"
                +Locale.RightClick(Configuration.getApplicationLanguage())
                +"</i> "
                +Locale.ForPersistentMode(Configuration.getApplicationLanguage()).toLowerCase()
                +"<br><i>"
                +Locale.LeftClick(Configuration.getApplicationLanguage())
                +"</i> "
                +Locale.ForVolatileMode(Configuration.getApplicationLanguage()).toLowerCase()
                +"</html>");
        m_bttnArea.setName(Constants.AREA_TOOL);
        m_bttnArea.setBorderPainted(false);
        m_bttnArea.setContentAreaFilled(false);
        m_bttnArea.setMargin(bttnMargins);
        m_bttnArea.addMouseListener(this);
        m_colorModelCombo = new JComboBox();
        Iterator iter = m_validControlStates.iterator();
        while (iter.hasNext()) {
            String colorMapOption = "";
            Specimen.ControlStates cs = (Specimen.ControlStates) iter
                    .next();
            colorMapOption = cs.getDisplayString();
            m_colorModelCombo.addItem(colorMapOption);
        }

        m_currentValue = m_colorModelCombo.getItemAt(0).toString();
        m_colorModelCombo.addItemListener(this);
        m_colorModelCombo.setToolTipText(Locale.AvailableColormaps(Configuration.getApplicationLanguage()));

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(1, 1, 1, 0);
        c.gridx = 0;
        c.gridy = 0;
        afmPanel.add(m_bttnArea, c);

        c = new GridBagConstraints();
        c.insets = new Insets(1, 1, 1, 1);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.5;
        c.gridwidth = GridBagConstraints.REMAINDER;
        afmPanel.add(m_colorModelCombo, c);
        return afmPanel;
    }
    /**
     * This method looks into the colormaps folder and gets the names
     * of each of the image file located in that folder. It then makes
     * one entry for each of the files in the drop down. By doing this we
     * are able to load new colormaps dynamically just by putting the
     * colormap image file in the dsignated folder.
     * @param modelCombo
     * @deprecated
     *
    private void loadColorMaps() {
    m_colorModelCombo = new JComboBox();
    m_colorModelCombo.addItem(new ColorMapFile(null));

    File dir = new File(Constants.COLORMAPS_LOCATION);
    FilenameFilter filter = new FilenameFilter() {
    public boolean accept(File dir, String name) {
    return (name.endsWith(".bmp"));
    }
    };
    File[] files = dir.listFiles(filter);
    String path = dir.getAbsolutePath();
    if (files == null) {
    JOptionPane.showMessageDialog(
    null,
    "Could not locate '"
    + path
    + "' folder. "
    + Constants.NEW_LINE
    + "Make sure the colormaps sub-folder is present and that it contains colormap '.bmp' image files.",
    "Error", JOptionPane.ERROR_MESSAGE);
    } else {
    // sort the color maps by name.
    Arrays.sort(files, new Comparator() {
    public int compare(Object o1, Object o2) {
    File f1 = (File) o1; File f2 = (File) o2;
    return (f1.getName().compareTo(f2.getName()));
    }
    });
    for (int i = 0; i < files.length; i++) {
    m_colorModelCombo.addItem(new ColorMapFile(files[i]));
    }
    }
    }*/

    /**
     * This method unselects any selected tools. This is useful incases when
     * any other controls want to ensure that no AFM tool is active before they
     * can start their work.
     */
    public void unselectAllTools() {
        m_persistentMode = false;
        m_bttnArea.setSelected(false);
    }

    /**
     * Returns the first valid control state value. This should point to
     * the 'gray' (a.k.a the base image). This is used to generate the 3D image.
     * Since the 3D image must be generated from the base image tiles - as this
     * is the image without any colormaps applied to it hence its pixel brightness
     * vlaues are unadultered and generate an accurate 3D image.
     *
     * @return
     */
    public ControlStates getFirstValue() {
        try {
            Iterator it = m_validControlStates.iterator();
            ControlStates lastControlState = null;
            if (it.hasNext())
                lastControlState = (ControlStates) it.next();
            return lastControlState;
        } catch (Exception ex) {
            return null;
        }
    }


    /**
     * Called when the combo box selection is changed.
     */
    public void itemStateChanged(ItemEvent ie) {
        m_currentValue = (String) ie.getItem();
        m_microscope.reCreateControls(this, m_display.getCurrentSpecimen()
                .getXMLVersion());
        m_microscope.updateSpecimenControlStates();
        updateButtonStates();
    }

    /**
     * cleans up after the control is unloaded.
     * TODO: right now the cleanUp does not get called implicitly.
     * Put the cleanUp in the class hierarchy so that it gets called for
     * each controlautomatically from the microscopeview.
     */
    public void cleanUp(double version) {
        // TODO Auto-generated method stub
        if (m_colorModelCombo != null)
            m_colorModelCombo.removeItemListener(this);
        m_3dVisualizer.cleanUp();
    }

    public void setSelectedValue(String value) {
        // TODO Auto-generated method stub

    }

    protected void updateButtonStates() {
        // TODO Auto-generated method stub

    }

    public void draw(Graphics2D g2d) {
        // TODO Auto-generated method stub

    }

    public void mouseClicked(MouseEvent e) {
        boolean rightButtonClicked = SwingUtilities.isRightMouseButton(e);
        if (e.getSource() instanceof JToggleButton) {
            JToggleButton btn = (JToggleButton) e.getSource();
            if (btn.getName().equals(Constants.AREA_TOOL)) {
                if (rightButtonClicked)
                    // have to do this since on left button press the toggle
                    // button's default
                    // event handler would already have changed its state but on
                    // right click
                    // the change in state would not have happened.
                    btn.setSelected(!btn.isSelected());
                if (!btn.isSelected()) {
                    // Now other modules can do whatever they wish with the
                    // cursor.
                    m_display.setResetCursorFlag(true);
                    m_persistentMode = false;
//					m_display.getAnnotationControl().removeAnnotationControlChangeListener(this);
                } else {
                    if (rightButtonClicked) {
                        m_persistentMode = true;
                        m_display.getAnnotationControl().addAnnotationControlChangeListener(this);
                    }
                }
            }
        }
    }

    public void mousePressed(MouseEvent e) {
        if (m_bttnArea.isSelected() && e.getSource() instanceof MicroscopeView) {
            m_areaSelectionInProgress = true;
            // start the area drawing.
            m_oldColor = m_display.getAnnotationControl().getCurrentColor();
            m_oldLineWidth = m_display.getAnnotationControl()
                    .getCurrentLineWidth();
            // We fool the annotation control into thinking that
            // it is being activated with the area tool selected. We
            // make sure we do not consume this event, hence the annotation
            // control will receive the event and it will take over.
            m_display.getAnnotationControl().shouldDrawSymmetricalShape(true);
            m_display.getAnnotationControl().shouldDrawReadonlyAnnotation(true);
            m_display.getAnnotationControl().setCurrentColor(Color.YELLOW);
            m_display.getAnnotationControl().setCurrentLineWidth(2.0f);
            m_oldSelection = m_display.getAnnotationControl().selectTool(Constants.ANNOTATION_RECTANGLE_TOOL);
            m_oldCursor = m_display.getCursor();
            m_display.setCursor(Cursor
                    .getPredefinedCursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    public void mouseReleased(MouseEvent arg0) {
        if (m_bttnArea.isSelected()) {
            if (m_areaSelectionInProgress) {
                // finished selecting the area.
                // The Annotation Control would have drawn the area
                // for us. We only need to close the path for the drawn
                // shape and undo some changes and our area shape will be ready
                // for us.
                // The annotation control wouldn't even have noticed what hit
                // it.
                Annotation drawnAnnotation = m_display.getAnnotationControl()
                        .getDrawnAnnotation();
                if (drawnAnnotation != null) {
                    drawnAnnotation.setTextOffset(-9, +5);
                    drawnAnnotation
                            .setAnnotationText(Constants.AREA_LABEL_PREFIX
                                    + m_selectedAreaLabelNumber);
                    // ((GeneralPath)drawnAnnotation.getOrigShape()).closePath();
                    m_display.getAnnotationControl()
                            .setCurrentColor(m_oldColor);
                    m_display.getAnnotationControl().setCurrentLineWidth(
                            m_oldLineWidth);
                    drawnAnnotation.makeUneditable();// m_isEditable = false;
                    drawnAnnotation.makeUnpersistable();// m_isPersistable =
                    // false;
                    drawnAnnotation.unselectAnnotation();
                    m_display.getAnnotationControl().finishDrawAnnot();
                    m_display.getAnnotationControl().selectTool(m_oldSelection);

                    if (!m_persistentMode) {
                        m_bttnArea.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        m_areaSelectionInProgress = false;
                    }
                    m_3dVisualizer.show3DImage(drawnAnnotation.getOrigShape()
                                    .getBounds2D(), m_display.getCursor(), m_display
                                    .getAnnotationControl(), drawnAnnotation,
                            m_currentValue);
                    m_selectedAreaLabelNumber++;
                }
            }
        }
    }

    public void mouseEntered(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseExited(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseDragged(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void mouseMoved(MouseEvent arg0) {
        // TODO Auto-generated method stub

    }

    public void keyTyped(KeyEvent ke) {
        keyPressed(ke);

    }

    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                if (m_persistentMode) {
                    // pressinf escape makes any persistable
                    // tool to unselect itself.
                    m_persistentMode = false;
                    m_bttnArea.setSelected(false);
//				m_display.getAnnotationControl().removeAnnotationControlChangeListener(this);
                }
                break;
            default:
                break;

        }

    }

    public void keyReleased(KeyEvent arg0) {
    }

    public void mouseWheelMoved(MouseWheelEvent arg0) {
    }

    public void annotationToolChanged(JComponent newSelection) {
        if (m_persistentMode) {
            m_persistentMode = false;
            m_bttnArea.setSelected(false);
        }
    }

    public void annotationSelectionChanged(Object newSelection) {

    }

    public void annotationLineWidthChanged(Object newSelection) {

    }

    public void annotationColorChanged(Object newSelection) {

    }

    public void annotationSaved(String annotationFilePath) {

    }

    public void annotationClosed(String annotationFilePath) {

    }


}
