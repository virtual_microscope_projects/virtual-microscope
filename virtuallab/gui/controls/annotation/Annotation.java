/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Shape;
import java.awt.Dimension;
import java.awt.BasicStroke;
import java.awt.Stroke;
import java.awt.Font;
import java.awt.font.TextLayout;
import java.awt.font.TextHitInfo;
import java.awt.geom.AffineTransform;

import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.PathIterator;

import java.awt.event.MouseEvent;
import java.awt.event.KeyEvent;

import javax.swing.JFrame;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import virtuallab.Log;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

// $Id: Annotation.java,v 1.3 2006/09/05 01:08:47 manzoor2 Exp $**
/* A class encapsulating an annotation. TODO: Annotation class is being used not
 * only to annotate specimens but also during EDS graph and AFM 3D visualization
 * features. For EDS and AFM usage we should subclass this Annotation class and
 * move all the EDS/AFM specific logic to the subclasses (e.g. isEditable,
 * isPersistable etc. all only apply to EDS/AFM usage)
 */
public class Annotation {

    private static final Log log = new Log(Annotation.class.getName());

    public static final String XML_ATTRIB_NAME = "Name";
    /**
     * Annotation's default color.
     */
    protected static final Color DEFAULT_COLOR = Color.WHITE;
    /**
     * Annotation's default line width.
     */
    protected static final float DEFAULT_LINE_WIDTH = 1.0f;
    /**
     * Default font size.
     */
    protected static final int DEFAULT_FONT_SIZE = 14;
    /**
     * Default font style.
     */
    protected static final int DEFAULT_FONT_STYLE = Font.BOLD;
    /**
     * Default font name.
     */
    protected static final String DEFAULT_FONT_NAME = "sansserif";
    /**
     * Default caret line width.
     */
    protected static final float DEFAULT_CARET_WIDTH = 2.0f;
    /**
     * Colors to use for the strong caret.
     */
    private static final Color STRONG_CARET_COLOR = Color.blue;
    /**
     * Colors to use for the weak caret.
     */
    private static final Color WEAK_CARET_COLOR = Color.black;
    /**
     * Color to use for highlighting.
     */
    private static final Color HIGHLIGHT_COLOR = Color.darkGray;
    /**
     * Selection color.
     */
    private static final Color SELECTION_COLOR = new Color(0, 0, 255, 200);
    /**
     * Text to appear in the text box if no text was entered.
     */
    private static final String EMPTY_TEXT = " ";
    /**
     * Default text location (relative to the center of the shape).
     */
    private static final Point2D.Double DEFAULT_TEXT_OFFSET = new Point2D.Double(
            0, 0);
    /**
     * Size of the selection squares.
     */
    private static final double SELECTION_SQUARE_SIZE = 6.0;
    /**
     * Size of the sqare area around mouse position for intersection testing.
     */
    private static final double FEELER_SQUARE_SIZE = 10.0;
    /**
     * Step size when shape is moved.
     */
    private static final double SHAPE_SHIFT_STEP = 1.0;
    /**
     * Tag for the Annotation XML element.
     */
    private static final String XML_TAG_ANNOTATION = "Annotation";
    private static final String XML_TAG_COL_SHAPE = "Color";
    private static final String XML_TAG_COL_TEXT = "TextColor";
    private static final String XML_ATTRIB_COL_RGB = "RGB";
    private static final String XML_TAG_FONT = "Font";
    private static final String XML_ATTRIB_FONT_NAME = "Name";
    private static final String XML_TAG_LINE = "Line";
    private static final String XML_ATTRIB_LINE_WIDTH = "Width";
    private static final String XML_TAG_TEXT = "Text";
    private static final String XML_TAG_SHAPE = "Shape";
    private static final String XML_TAG_SEG = "Segment";
    private static final String XML_ATTRIB_SEG_TYPE = "Type";
    private static final String XML_ATTRIB_SEG_VAL_MOVETO = "SEG_MOVETO";
    private static final String XML_ATTRIB_SEG_VAL_LINETO = "SEG_LINETO";
    private static final String XML_ATTRIB_SEG_VAL_QUADTO = "SEG_QUADTO";
    private static final String XML_ATTRIB_SEG_VAL_CUBICTO = "SEG_CUBICTO";
    private static final String XML_ATTRIB_SEG_VAL_CLOSE = "SEG_CLOSE";
    private static final String XML_TAG_TXT_OFFSET = "TextOffset";
    private static final String XML_TAG_LOC = "Location";
    private static final String XML_ATTRIB_LOC_X = "X";
    private static final String XML_ATTRIB_LOC_Y = "Y";
    private static final String XML_TAG_IMG_DIM = "ImageDimensions";
    private static final String XML_ATTRIB_DIM_WIDTH = "Width";
    private static final String XML_ATTRIB_DIM_HEIGHT = "Height";
    /**
     * Used to translate/scale annotations.
     */
    protected static AffineTransform transform;
    /**
     * Selected annotation stroke
     */
    protected static Stroke selectionStroke;
    protected static Rectangle2D feelerRectangle;

    /** Static initializer. */
    static {
        feelerRectangle = new Rectangle2D.Double();
        transform = new AffineTransform();
        float dash[] = {10.0f, 5.0f};
        selectionStroke = new BasicStroke(1.5f, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);
    }

    /**
     * Unique Name of the associated specimen.
     */
    protected String m_specimenName;
    /**
     * Shape color.
     */
    protected Color m_shapeColor;
    /**
     * Text color.
     */
    protected Color m_textColor;
    /**
     * Annotation's font.
     */
    protected Font m_font;
    /**
     * Shape's line width.
     */
    protected float m_lineWidth;
    /**
     * Original shape location, relative to the stagePosition.
     */
    protected Shape m_origShape;
    /**
     * Text layout's offset, relative to the center of shape.
     */
    protected Point2D m_origTextOffset;
    /**
     * Dimensions of the image at the time when this shape was created. This
     * variable is used for relative scaling. This would normally be the same as
     * the m_transImgDim, except when the image is zoomed in or out - in which
     * case m_transImgDim will hold the new current image size whereas this
     * variable will still hold the original image size.
     */
    protected Dimension m_origImgDim;
    /**
     * True if shape is currently selected.
     */
    protected boolean m_shapeSelected;
    /**
     * True if shape's text is currently selected.
     */
    protected boolean m_textSelected;
    /**
     * True if shape's text is currently selected and editable.
     */
    protected boolean m_textEditable;
    /**
     * Shape selection square clicked.
     */
    protected int m_shapeSelSquare;
    /**
     * Dimensions of the current image; to be used for relative scaling. This
     * would normally be the same as the m_origImgDim, except when the image is
     * zoomed in or out - in which case this variable will hold the current
     * image size whereas the m_origImgDim will still hold the original image
     * size at the time when this shape was created.
     */
    protected Dimension m_transImgDim;
    /**
     * Current transformed shape.
     */
    protected Shape m_transShape;
    /**
     * Shape's current transformed text layout.
     */
    protected TextLayout m_transLayout;
    /**
     * Shape's current transformed text offset.
     */
    protected Point2D m_transTextOffset;
    /**
     * Last mouse position when dragging or resizing the shape.
     */
    protected Point2D m_shapeDragResizePos;
    /**
     * Last mouse position when dragging the shape's text.
     */
    protected Point2D m_textDragPos;
    /**
     * Current TextLayout's insertion index.
     */
    protected int m_insertionIdx;
    /**
     * The insertion index of the initial mouse click; one end of the selection
     * range. During a mouse drag this end of the selection is constant.
     */
    protected int m_anchorEnd;
    /**
     * The insertion index of the current mouse location; the other end of the
     * selection range. This changes during a mouse drag.
     */
    protected int m_activeEnd;
    /**
     * Indicates whether the annotation is editable or not. Used primarily from
     * with in the EDS Graph mode to make sure that any annotations that are
     * created by EDS module are not selectable and changeable.
     */
    private boolean m_isEditable = true;
    /**
     * Indicates if this annotation should be persisted or not. Some annotations
     * (e.g. the onese created during the EDS graph mode) are transitory and
     * should not be persisted.
     */
    private boolean m_isPersistable = true;
    /**
     * Applies only when the Annotation is used to represent EDS graph selection
     * area or AFM 3D area selection. Upon selection of this Annotation the
     * m_associatedWindow will be brought to front.
     */
    private JFrame m_associatedWindow;
    /**
     * Shape's text.
     */
    private String m_text;
    private AnnotationControl m_annotationControl;
    private JPopupMenu m_popupMenu;

    /**
     * Constructs an annotation.
     *
     * @param isReadonly    indicates if this annotation should be readonly or not.
     * @param specimenName  unique specimen name
     * @param shape         Shape (position relative to stagePosition)
     * @param textOffset    Text offset (position relative to shape center); assumed zero if null is passed in
     * @param shapeColor    Shape color
     * @param textColor     Text color
     * @param fontName      Text font name
     * @param lineWidth     Spape's line width
     * @param origImgDim    Dimensions of the image with which this annotation is associated
     * @param container     the container which contains all the annotations including this one.
     */
    public Annotation(boolean isReadonly, String specimenName, Shape shape,
                      Point2D textOffset, Color shapeColor, Color textColor,
                      String fontName, float lineWidth, Dimension origImgDim,
                      AnnotationControl container) {
        if (shape == null) {
            throw new IllegalArgumentException("Shape cannot be null");
        }

        m_specimenName = specimenName;
        m_transShape = m_origShape = shape;
        m_shapeColor = (shapeColor == null) ? DEFAULT_COLOR : shapeColor;
        m_textColor = (textColor == null) ? m_shapeColor : textColor;
        m_lineWidth = (lineWidth <= 0) ? DEFAULT_LINE_WIDTH : lineWidth;
        m_font = (fontName == null) ? new Font(DEFAULT_FONT_NAME, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE) : new Font(fontName, DEFAULT_FONT_STYLE, DEFAULT_FONT_SIZE);
        m_origImgDim = origImgDim;
        m_text = "";
        m_insertionIdx = m_activeEnd = m_anchorEnd = 0;
        m_transLayout = null;
        m_shapeDragResizePos = null;
        m_textDragPos = null;
        m_shapeSelected = false;
        m_textSelected = !isReadonly;
        m_textEditable = !isReadonly;
        m_isEditable = !isReadonly;
        m_shapeSelSquare = Square.NONE;
        m_origTextOffset = new Point2D.Double();
        m_transTextOffset = new Point2D.Double();
        m_origTextOffset.setLocation((textOffset == null) ? DEFAULT_TEXT_OFFSET : textOffset);
        m_transTextOffset.setLocation(m_origTextOffset);
        m_annotationControl = container;
        if (container != null)
            m_popupMenu = buildPopupMenu(container);
    }

    /**
     * Constructs an annotation with default color.
     *
     * @param isReadonly        indicates if this annotation should be readonly.
     * @param specimenName      Unique specimen name
     * @param origShape         Shape (position relative to stagePosition)
     * @param origTextOffset    Text offset (position relative to shape center); assumed zero
     *                          if null is passed in
     * @param origImgDim        Dimensions of the image with which this annotation is
     *                          associated
     * @param AnnotationControl the container which contains all the annotations including
     *                          this one.
     */
    public Annotation(boolean isReadonly, String specimenName, Shape origShape,
                      Point2D textOffset, Dimension origImgDim,
                      AnnotationControl container) {
        this(isReadonly, specimenName, origShape, textOffset, null, null, null,
                0, origImgDim, container);
    }

    /**
     * Constructs an annotation with default color.
     *
     * @param isReadonly        indicates if this annotation should be readonly.
     * @param specimenName      Unique specimen name
     * @param origShape         Shape (position relative to stagePosition)
     * @param shapeColor        Shape color
     * @param textColor         Text color
     * @param fontName          Text font name
     * @param lineWidth         Spape's line width
     * @param origImgDim        Dimensions of the image with which this annotation is
     *                          associated
     * @param AnnotationControl the container which contains all the annotations including
     *                          this one.
     */
    public Annotation(boolean isReadonly, String specimenName, Shape origShape,
                      Color shapeColor, Color textColor, String fontName,
                      float lineWidth, Dimension origImgDim, AnnotationControl container) {
        this(isReadonly, specimenName, origShape, null, shapeColor, textColor,
                fontName, lineWidth, origImgDim, container);
    }

    /**
     * Unpersists Annotation from a properly constructed XML node. Its contents:
     * Color is optional: if none specified - default is assumed. Text is
     * optional: no text is assumed. Shape is optional: no shape is assumed.
     * Original Image Dimension: mandatory. Text offset is mandatory if text is
     * present, otherwise, optional.
     *
     * @param node Node to unpersist the Annotation from
     * @return unpersisted annotation or null if failed.
     */
    public static Annotation unpersistFromXMLNode(Node node, AnnotationControl container) {
        Annotation annot = null;

        // Quick sanity check
        if ((node.getNodeName().equals(XML_TAG_ANNOTATION))
                && (node.getNodeType() == Node.ELEMENT_NODE)) {
            NodeList list = null;
            NodeList innerList = null;
            Element elem = null;
            Element innerElem = null;
            Color shapeColor = null;
            Color textColor = null;
            String fontName = null;
            float lineWidth = 0;
            String text = null;
            String specimenName = null;
            Dimension dimension = null;
            Point2D textOffset = null;
            GeneralPath path = null;

            // // Get the specimen's unique name
            // list =
            // ((Element)node).getElementsByTagName(XML_TAG_SPECIMEN_NAME);
            //
            // if (list != null && list.getLength() == 1 &&
            // list.item(0).getNodeType() == Node.ELEMENT_NODE)
            // {
            // elem = (Element)list.item(0);
            // specimenName = elem.getAttribute(XML_ATTRIB_NAME);
            // }

            // Get shape color element
            list = ((Element) node).getElementsByTagName(XML_TAG_COL_SHAPE);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    shapeColor = new Color(Integer.parseInt(elem.getAttribute(XML_ATTRIB_COL_RGB)));
                } catch (Exception e) { //ignore error getting value of XML_ATTRIB_COL_RGB for annotation shape
                }
            }

            // Get text color element
            list = ((Element) node).getElementsByTagName(XML_TAG_COL_TEXT);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    textColor = new Color(Integer.parseInt(elem.getAttribute(XML_ATTRIB_COL_RGB)));
                } catch (Exception e) { //ignore error getting value of XML_ATTRIB_COL_RGB for annotation text
                }
            }

            // Get font element
            list = ((Element) node).getElementsByTagName(XML_TAG_FONT);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    fontName = elem.getAttribute(XML_ATTRIB_FONT_NAME);
                } catch (Exception e) { //ignore error getting value of XML_ATTRIB_FONT_NAME for annotation text
                }
            }

            // Get line element
            list = ((Element) node).getElementsByTagName(XML_TAG_LINE);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    lineWidth = Float.parseFloat(elem.getAttribute(XML_ATTRIB_LINE_WIDTH));
                } catch (Exception e) { //ignore error getting value of XML_ATTRIB_LINE_WIDTH for annotation shape
                }
            }

            // Get the text
            list = ((Element) node).getElementsByTagName(XML_TAG_TEXT);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                // Get the only child (text) node
                list = list.item(0).getChildNodes();

                if (list != null && list.getLength() == 1
                        && list.item(0).getNodeType() == Node.TEXT_NODE) {
                    text = ((Text) list.item(0)).getData();
                }
            }

            // Get the original shape
            list = ((Element) node).getElementsByTagName(XML_TAG_SHAPE);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                float[] coords = new float[6];
                int numPoints = 0;
                int coordNum = 0;
                String segType = null;

                elem = (Element) list.item(0);

                path = new GeneralPath();

                // Get the list of segment nodes
                list = elem.getElementsByTagName(XML_TAG_SEG);

                for (int segNum = 0; segNum < list.getLength(); segNum++) {
                    if (list.item(segNum).getNodeType() == Node.ELEMENT_NODE) {
                        // Get the segment node
                        elem = (Element) list.item(segNum);

                        // Get its associated points
                        innerList = elem.getElementsByTagName(XML_TAG_LOC);
                        numPoints = innerList.getLength();
                        coordNum = 0;

                        for (int ptNum = 0; ptNum < numPoints; ptNum++) {
                            if (innerList.item(ptNum).getNodeType() == Node.ELEMENT_NODE) {
                                try {
                                    innerElem = (Element) innerList.item(ptNum);
                                    coords[coordNum++] = Float.parseFloat(innerElem.getAttribute(XML_ATTRIB_LOC_X));
                                    coords[coordNum++] = Float.parseFloat(innerElem.getAttribute(XML_ATTRIB_LOC_Y));
                                } catch (NumberFormatException e) {
                                    log.error("Unable to establish location of annotation.");
                                    log.error(e.getMessage());
                                }
                            }
                        }

                        // Now, figure out the type of the segment
                        segType = elem.getAttribute(XML_ATTRIB_SEG_TYPE);

                        if (segType.equals(XML_ATTRIB_SEG_VAL_MOVETO) && (numPoints == 1)) {
                            path.moveTo(coords[0], coords[1]);
                        } else if (segType.equals(XML_ATTRIB_SEG_VAL_LINETO) && (numPoints == 1)) {
                            path.lineTo(coords[0], coords[1]);
                        } else if (segType.equals(XML_ATTRIB_SEG_VAL_QUADTO) && (numPoints == 2)) {
                            path.quadTo(coords[0], coords[1], coords[2], coords[3]);
                        } else if (segType.equals(XML_ATTRIB_SEG_VAL_CUBICTO) && (numPoints == 3)) {
                            path.curveTo(coords[0], coords[1], coords[2], coords[3], coords[4], coords[5]);
                        } else if (segType.equals(XML_ATTRIB_SEG_VAL_CLOSE) && (numPoints == 0)) {
                            path.closePath();
                        }
                    }
                }
            }

            // Get the original image dimension
            list = ((Element) node).getElementsByTagName(XML_TAG_IMG_DIM);

            if (list != null && list.getLength() == 1
                    && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    int w = Integer.parseInt(elem.getAttribute(XML_ATTRIB_DIM_WIDTH));
                    int h = Integer.parseInt(elem.getAttribute(XML_ATTRIB_DIM_HEIGHT));
                    dimension = new Dimension(w, h);
                } catch (NumberFormatException e) { //ignore error getting value of XML_ATTRIB_DIM_WIDTH or XML_ATTRIB_DIM_HEIGHT for annotation shape
                }
            }

            // Get the text offset
            list = ((Element) node).getElementsByTagName(XML_TAG_TXT_OFFSET);

            if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                elem = (Element) list.item(0);

                try {
                    double x = Double.parseDouble(elem.getAttribute(XML_ATTRIB_LOC_X));
                    double y = Double.parseDouble(elem.getAttribute(XML_ATTRIB_LOC_Y));
                    textOffset = new Point2D.Double(x, y);
                } catch (NumberFormatException e) {
                    log.error("Unable to establish location of annotation.");
                    log.error(e.getMessage());
                }
            }

            // Now, gather what we know
            if ((dimension != null) && (textOffset != null)) {
                annot = new Annotation(false, specimenName, path, shapeColor, textColor, fontName, lineWidth, dimension, container);
                annot.m_origTextOffset = textOffset;
                annot.m_text = text;
                annot.m_shapeSelected = annot.m_textSelected = annot.m_textEditable = false;
            }
        }

        return annot;
    }

    protected JPopupMenu buildPopupMenu(AnnotationControl popupMenuActionListener) {
        JPopupMenu retVal = new JPopupMenu();

        JMenuItem item = new JMenuItem();
        item.setText(Locale.Delete(virtuallab.Configuration.getApplicationLanguage())+ Constants.SPACE +Locale.Annotation(virtuallab.Configuration.getApplicationLanguage()).toLowerCase());
        item.setActionCommand("Delete");
        item.addActionListener(popupMenuActionListener.getPopupMenuActionListener());
        retVal.add(item);

        return retVal;
    }

    /**
     * (De-)selects Annotation's text.
     *
     * @param select true to select.
     */
    public void selectText(boolean select) {
        m_textSelected = select;

        // If being deselected - disable the editable property
        if (!select) {
            m_textEditable = select;
        }
    }

    /**
     * (De-)selects Annotation's shape.
     *
     * @param select true to select.
     */
    public void selectShape(boolean select) {
        m_shapeSelected = select;
    }

    /**
     * Sets Annotation's text to be (un-)editable.
     *
     * @param select true to set editable.
     */
    public void setTextEditable(boolean select) {
        m_textEditable = select;
    }

    /**
     * Changes shape's color (iff shape is selected) or/and text color (iff text
     * is selected).
     *
     * @param color new color.
     * @return true iff either color was changed.
     */
    public boolean setColor(Color color) {
        boolean changed = false;

        if (color != null) {
            if (m_shapeSelected && !m_shapeColor.equals(color)) {
                m_shapeColor = color;
                changed = true;
            }

            if (m_textSelected && !m_textColor.equals(color)) {
                m_textColor = color;
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Changes shape's line width (iff shape is selected).
     *
     * @param lineWidth new line width.
     * @return true iff line width was changed.
     */
    public boolean setLineWidth(float lineWidth) {
        boolean changed = false;

        if (lineWidth > 0) {
            if (m_shapeSelected && (m_lineWidth != lineWidth)) {
                m_lineWidth = lineWidth;
                changed = true;
            }
        }

        return changed;
    }

    /**
     * Changes shape's font (iff text is selected).
     *
     * @param fontName new font name.
     * @return true iff font was changed.
     */
    public boolean setFontName(String fontName) {
        Font font = null;
        boolean changed = false;

        if ((fontName != null)
                && ((font = new Font(fontName, DEFAULT_FONT_STYLE,
                DEFAULT_FONT_SIZE)) != null) && !m_font.equals(font)) {
            m_font = font;
            changed = true;
        }

        return changed;
    }

    /**
     * @return true iff Annotation's text is selected.
     */
    public boolean textSelected() {
        return m_textSelected;
    }

    /**
     * @return true iff Annotation's shape is selected or the text is selected
     * (in case of the Text Annotation).
     */
    public boolean shapeSelected() {
        return (m_shapeSelected || m_textSelected);
    }

    /**
     * @return true iff Annotation's text is editable.
     */
    public boolean textEditable() {
        return m_textEditable;
    }

    /**
     * Transforms (scales and shifts) and draws the shape on the current stage.
     *
     * @param g2d       Graphics context on which to draw the annotation
     * @param curImgDim Current dimensions of the image on the stage (for scaling)
     * @param stagePos  Position of the stage (for shifting)
     */
    public void draw(Graphics2D g2d, Dimension curImgDim, Point2D.Double stagePos) {
        // Set up a transform (to scale and shift) for the original shape
        double curScaleX = curImgDim.getWidth() / m_origImgDim.getWidth();
        double curScaleY = curImgDim.getHeight() / m_origImgDim.getHeight();
        Color oldColor = g2d.getColor();
        Stroke oldStroke = g2d.getStroke();

        // Remember the current image dimensions (used for scaling when
        // dragging)
        m_transImgDim = curImgDim;

        // Create shape transform
        transform.setTransform(curScaleX, 0, 0, curScaleY, stagePos.x,
                stagePos.y);

        // Transform the shape
        m_transShape = transform.createTransformedShape(m_origShape);

        // Draw shape
        if ((m_origShape.getBounds().getWidth() != 0)
                || (m_origShape.getBounds().getHeight() != 0)) {
            // Set the annotation's color
            g2d.setColor(m_shapeColor);
            // Set stroke
            g2d.setStroke(new BasicStroke((float) (m_lineWidth * curScaleX)));
            // Draw the transformed shape
            g2d.draw(m_transShape);
        }

        // Compute and scale the text layout location
        m_transTextOffset.setLocation(m_origTextOffset.getX()
                + m_origShape.getBounds().getCenterX(), m_origTextOffset.getY()
                + m_origShape.getBounds().getCenterY());
        transform.transform(m_transTextOffset, m_transTextOffset);
        // Set up a transform for text
        transform.setTransform(curScaleX, 0, 0, curScaleY, 0, 0);
        // Make text layout, if empty - draw default text
        m_transLayout = new TextLayout(
                ((m_text != null) && (m_text.length() > 0)) ? m_text
                        : EMPTY_TEXT, m_font.deriveFont(transform), g2d
                .getFontRenderContext());

        // Retrieve caret Shapes for insertionIndex
        Shape[] carets = m_transLayout.getCaretShapes(m_insertionIdx);
        boolean haveCaret = (m_activeEnd == m_anchorEnd);
        transform.setTransform(1, 0, 0, 1, m_transTextOffset.getX(),
                m_transTextOffset.getY());

        // If text is selected - draw highlighted section
        if (!haveCaret && m_textEditable) { // Retrieve selection's highlited
            // region and draw it
            g2d.setColor(HIGHLIGHT_COLOR);
            g2d.fill(transform.createTransformedShape(m_transLayout
                    .getLogicalHighlightShape(m_anchorEnd, m_activeEnd)));
        }

        // Draw the text (on top of the highlighted section)
        g2d.setColor(m_textColor);
        m_transLayout.draw(g2d, (float) m_transTextOffset.getX(),
                (float) m_transTextOffset.getY());
        // Draw selection boxes around text and shape
        drawSelections(g2d);

        // Draw carets on top of the text
        if (haveCaret && m_textEditable) {
            g2d.setStroke(new BasicStroke(
                    (float) (DEFAULT_CARET_WIDTH * curScaleX)));

            // Draw the carets. carets[0] is the strong caret, and is never
            // null.
            g2d.setColor(STRONG_CARET_COLOR);
            g2d.draw(transform.createTransformedShape(carets[0]));

            // carets[1], if it is not null, is the weak caret.
            if (carets[1] != null) {
                g2d.setColor(WEAK_CARET_COLOR);
                g2d.draw(transform.createTransformedShape(carets[1]));
            }
        }

        // Restore the original color and stroke
        g2d.setColor(oldColor);
        g2d.setStroke(oldStroke);
    }

    /**
     * Creates an Document node and persists itself into it.
     *
     * @param doc Document
     * @return newly created node
     */
    public Node persistToXMLNode(Document doc) {
        Node node = null;
        Element elem = null;
        Element innerElem = null;
        Element locElem = null;
        Text text = null;

        // Create the Annotation element
        node = doc.createElement(XML_TAG_ANNOTATION);

        // Create and add the annotation's shape color
        elem = doc.createElement(XML_TAG_COL_SHAPE);
        elem.setAttribute(XML_ATTRIB_COL_RGB, String.valueOf(m_shapeColor
                .getRGB()));
        node.appendChild(elem);

        // Create and add the annotation's text color
        elem = doc.createElement(XML_TAG_COL_TEXT);
        elem.setAttribute(XML_ATTRIB_COL_RGB, String.valueOf(m_textColor
                .getRGB()));
        node.appendChild(elem);

        // Create and add the annotation's font
        elem = doc.createElement(XML_TAG_FONT);
        elem.setAttribute(XML_ATTRIB_FONT_NAME, m_font.getName());
        node.appendChild(elem);

        // Create and add the annotation's line width
        elem = doc.createElement(XML_TAG_LINE);
        elem.setAttribute(XML_ATTRIB_LINE_WIDTH, String.valueOf(m_lineWidth));
        node.appendChild(elem);

        // Persist the text
        if ((m_text != null) && (m_text.length() > 0)) {
            elem = doc.createElement(XML_TAG_TEXT);
            text = doc.createTextNode(m_text);
            elem.appendChild(text);
            node.appendChild(elem);
        }

        // Persist the original shape
        if (m_origShape != null) {
            elem = doc.createElement(XML_TAG_SHAPE);

            GeneralPath path = new GeneralPath(m_origShape);

            PathIterator piter = path.getPathIterator(null);
            float[] coords = new float[6];
            int numPoints;
            int idx;

            while (!piter.isDone()) {
                idx = 0;
                numPoints = -1;
                innerElem = doc.createElement(XML_TAG_SEG);

                switch (piter.currentSegment(coords)) {
                    case PathIterator.SEG_MOVETO: {
                        innerElem.setAttribute(XML_ATTRIB_SEG_TYPE,
                                XML_ATTRIB_SEG_VAL_MOVETO);
                        numPoints = 1;
                        break;
                    }
                    case PathIterator.SEG_LINETO: {
                        innerElem.setAttribute(XML_ATTRIB_SEG_TYPE,
                                XML_ATTRIB_SEG_VAL_LINETO);
                        numPoints = 1;
                        break;
                    }
                    case PathIterator.SEG_QUADTO: {
                        innerElem.setAttribute(XML_ATTRIB_SEG_TYPE,
                                XML_ATTRIB_SEG_VAL_QUADTO);
                        numPoints = 2;
                        break;
                    }
                    case PathIterator.SEG_CUBICTO: {
                        innerElem.setAttribute(XML_ATTRIB_SEG_TYPE,
                                XML_ATTRIB_SEG_VAL_CUBICTO);
                        numPoints = 3;
                        break;
                    }
                    case PathIterator.SEG_CLOSE: {
                        innerElem.setAttribute(XML_ATTRIB_SEG_TYPE,
                                XML_ATTRIB_SEG_VAL_CLOSE);
                        numPoints = 0;
                    }
                }

                // Add actual points if any
                while (numPoints > 0) {
                    locElem = doc.createElement(XML_TAG_LOC);
                    locElem.setAttribute(XML_ATTRIB_LOC_X, String
                            .valueOf(coords[idx++]));
                    locElem.setAttribute(XML_ATTRIB_LOC_Y, String
                            .valueOf(coords[idx++]));
                    innerElem.appendChild(locElem);

                    numPoints--;
                }

                // Save the complete segment
                if (numPoints >= 0) {
                    elem.appendChild(innerElem);
                }

                piter.next();
            }

            node.appendChild(elem);
        }

        // Persist the original image dimension
        if (m_origImgDim != null) {
            elem = doc.createElement(XML_TAG_IMG_DIM);
            elem.setAttribute(XML_ATTRIB_DIM_WIDTH, String
                    .valueOf((int) m_origImgDim.getWidth()));
            elem.setAttribute(XML_ATTRIB_DIM_HEIGHT, String
                    .valueOf((int) m_origImgDim.getHeight()));
            node.appendChild(elem);
        }

        // Persist the text layout offset
        if (m_origTextOffset != null) {
            elem = doc.createElement(XML_TAG_TXT_OFFSET);
            elem.setAttribute(XML_ATTRIB_LOC_X, String.valueOf(m_origTextOffset
                    .getX()));
            elem.setAttribute(XML_ATTRIB_LOC_Y, String.valueOf(m_origTextOffset
                    .getY()));
            node.appendChild(elem);
        }

        return node;
    }

    /**
     * Handles mouseClicked() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
        Point2D point = e.getPoint();
        if (!m_isEditable) {
            // TODO: change the logic/code below and implement this as
            // an observer pattern i.e. other objects can register themselves
            // to receive events when an annotation is selected.
            if (isInShape(point, true) && m_associatedWindow != null) {
                // may be an EDS/AFM specific Annotation. We should set the
                // focus to
                // the EDS/AFM window if possible.
                m_associatedWindow.toFront();// bringToFront();
            }
            return;
        }
        // Check to see if it is in text
        if (isInText(point)) {
            if (m_textSelected) {
                if (m_textEditable) { // Figure out where in the text it was
                    // clicked
                    Point2D origin = computeLayoutOrigin();
                    float clickX = (float) (e.getX() - origin.getX());
                    float clickY = (float) (e.getY() - origin.getY());

                    m_activeEnd = m_anchorEnd = m_insertionIdx = m_transLayout
                            .hitTestChar(clickX, clickY).getInsertionIndex();
                } else {
                    m_textEditable = true;
                }
            } else {
                m_textSelected = true;
            }
            e.consume();
        } else if (m_shapeSelected) {
            // the shape was already selected it should be unselected now
            unselectAnnotation();
            m_annotationControl.m_display.setCursor(Cursor.DEFAULT_CURSOR);
        }
        // Otherwise, see if it is within the shape boundaries
        else if (isInShape(point, true)) {
            if (!m_shapeSelected) {
                m_shapeSelected = true;
                // If text is empty - select the empty box, so text doesn't
                // disappear
                if ((m_text == null) || (m_text.length() == 0)) {
                    m_textSelected = true;
                    m_textEditable = true;
                }
            } else {
                unselectAnnotation();
                m_annotationControl.m_display.setCursor(Cursor.DEFAULT_CURSOR);
            }
            e.consume();
        }
    }

    /**
     * Handles mousePressed() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mousePressed(MouseEvent e) {
        if (!m_isEditable)
            return;
        Point2D point = e.getPoint();

        // Check to see if it is in text
        if (isInText(point)) {
            if (m_textEditable) {
                Point2D origin = computeLayoutOrigin();

                // Compute the mouse location relative to textLayout's origin.
                float clickX = (float) (e.getX() - origin.getX());
                float clickY = (float) (e.getY() - origin.getY());

                // Set the anchor and active ends of the selection
                // to the character position of the mouse location.
                m_activeEnd = m_anchorEnd = m_insertionIdx = m_transLayout
                        .hitTestChar(clickX, clickY).getInsertionIndex();
                e.consume();
            } else if (m_textSelected) { // Starting to drag the text
                m_textDragPos = new Point2D.Double(e.getX(), e.getY());
                e.consume();
            }
            /**
             * Fixing the defect: Selecting the text annotation is very
             * difficult and behaves funny. Fix: We consume the event to avoid
             * the base image to scroll. The actual selection will be made when
             * the mouse has been Released. Without this statement for Text
             * Annotations when the user presses the mouse on the annotation the
             * base image would start to move and when he releases the mouse the
             * text annotation may not be selected since it may have scrolled
             * pass the mouse position; thus giving an erratic behavior.
             */
            e.consume();
        }
        // Else, see if shape is selected and the point is within any of the 8
        // squares
        else if (m_shapeSelected
                && (m_shapeSelSquare = isInShapeSelectionSquare(point)) != Square.NONE) {
            // Starting to resize the annotation
            m_shapeDragResizePos = new Point2D.Double(e.getX(), e.getY());
            e.consume();
        }
        // Else, see if it is in shape
        else if (m_shapeSelected && isInShape(point, false)) {
            // Starting to drag the annotation around.
            m_shapeDragResizePos = new Point2D.Double(e.getX(), e.getY());
            e.consume();
        } else if (isInShape(point, true)) {
            // to make selecting this shape easier. The actual selection happens inside
            // mouseClicked(), we consume this here so that the base image does not
            // scroll under us.
            e.consume();
        }

    }

    /**
     * Handles mouseDragged() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseDragged(MouseEvent e) {
        if (!m_isEditable)
            return;
        if (isInText(e.getPoint()) && m_textEditable) { // Editing text
            Point2D origin = computeLayoutOrigin();

            // Compute the mouse location relative to textLayout's origin.
            float clickX = (float) (e.getX() - origin.getX());
            float clickY = (float) (e.getY() - origin.getY());

            // Get the character position of the mouse location.
            m_activeEnd = m_transLayout.hitTestChar(clickX, clickY)
                    .getInsertionIndex();
            e.consume();
        } else if (m_textDragPos != null) {
            // Dragging text
            dragText(e.getPoint());
            e.consume();
        } else if (m_shapeDragResizePos != null & !e.isShiftDown()) {
            // Dragging/resizing the annotation
            dragScaleShape(e.getPoint());
            e.consume();
        } else if (m_shapeDragResizePos != null & e.isShiftDown()) {
            // Resize the shape keeping the aspect ratio intact.
            dragSymmetricalShape(e.getPoint());
            e.consume();
        }
    }

    /**
     * This method is called when the user drags the shape with shift down. Note
     * that this is different than resizing. Resizing will follow the mouse drag
     * position without any regard to the aspect ratio. Here we must maintain
     * the aspect ratio the same.
     *
     * @param point mouse location
     */
    protected void dragSymmetricalShape(Point2D point) {
        // Use to compensate for the maginifcation change.
        double curScaleX = m_origImgDim.getWidth() / m_transImgDim.getWidth();
        double curScaleY = m_origImgDim.getHeight() / m_transImgDim.getHeight();

        // Scaling
        double resizeScaleX = 1.0;
        double resizeScaleY = 1.0;

        // compensatory shifting to make sure that the opposite coordinate is
        // pinned
        // down while we scale.
        double shiftX = 0.0;
        double shiftY = 0.0;

        Rectangle2D br = m_origShape.getBounds2D();
        // Scaled mouse drag
        // deltaX represents the change in width. deltaY represent the change in
        // height.
        double deltaX = curScaleX
                * (point.getX() - m_shapeDragResizePos.getX());
        double deltaY = curScaleY
                * (point.getY() - m_shapeDragResizePos.getY());
        boolean tooFar = false;

        if (m_shapeSelSquare != Square.NONE) { // Proceede only if one of the
            // shape boxes is selected.

            if (m_shapeSelSquare == Square.TOP_LEFT
                    || m_shapeSelSquare == Square.TOP) {
                if (!(tooFar = deltaX >= br.getWidth())
                        && (!(tooFar = deltaY >= br.getHeight()))) {

                    deltaY = (br.getHeight() / br.getWidth() * (br.getWidth() - deltaX));
                    deltaY = br.getHeight() - deltaY;

                    resizeScaleX = (br.getWidth() - deltaX) / br.getWidth();
                    shiftX = deltaX + br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() - deltaY) / br.getHeight();
                    shiftY = deltaY + br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.TOP_RIGHT
                    || m_shapeSelSquare == Square.RIGHT) {
                if (!(tooFar = deltaX <= -br.getWidth())
                        && (!(tooFar = deltaY >= br.getHeight()))) {
                    deltaY = (br.getHeight() / br.getWidth() * (br.getWidth() + deltaX));
                    deltaY = br.getHeight() - deltaY;

                    resizeScaleX = (br.getWidth() + deltaX) / br.getWidth();
                    shiftX = br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() - deltaY) / br.getHeight();
                    shiftY = deltaY + br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.BOTTOM_LEFT
                    || m_shapeSelSquare == Square.LEFT) {
                if (!(tooFar = deltaX >= br.getWidth())
                        && (!(tooFar = deltaY <= -br.getHeight()))) {
                    deltaY = (br.getHeight() / br.getWidth() * (br.getWidth() - deltaX));
                    deltaY = deltaY - br.getHeight();

                    resizeScaleX = (br.getWidth() - deltaX) / br.getWidth();
                    shiftX = deltaX + br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() + deltaY) / br.getHeight();
                    shiftY = br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.BOTTOM_RIGHT
                    || m_shapeSelSquare == Square.BOTTOM) {
                if (!(tooFar = deltaX <= -br.getWidth())
                        && (!(tooFar = deltaY <= -br.getHeight()))) {
                    deltaY = (br.getHeight() / br.getWidth() * (br.getWidth() + deltaX));
                    deltaY = deltaY - br.getHeight();

                    resizeScaleX = (br.getWidth() + deltaX) / br.getWidth();
                    shiftX = br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() + deltaY) / br.getHeight();
                    shiftY = br.getY() * (1 - resizeScaleY);
                }
            }
        }
        if (!tooFar) {
            // Set up transform
            // setTransform(double m00, double m10, double m01, double m11,
            // double m02, double m12)
            transform.setTransform(resizeScaleX, 0, 0, resizeScaleY, shiftX,
                    shiftY);
            // Drag / scale the shape
            m_origShape = transform.createTransformedShape(m_origShape);
            // Remember the last mouse position
            m_shapeDragResizePos.setLocation(point);
        }
    }

    /**
     * Given the new mouse position, translates or scales the annotation shape.
     *
     * @param point mouse location
     */
    protected void dragScaleShape(Point2D point) {
        // Scale back to the original
        double curScaleX = m_origImgDim.getWidth() / m_transImgDim.getWidth();
        double curScaleY = m_origImgDim.getHeight() / m_transImgDim.getHeight();

        // Scale if resizing
        double resizeScaleX = 1.0;
        double resizeScaleY = 1.0;

        // Shift if dragging
        double shiftX = 0.0;
        double shiftY = 0.0;

        // Scaled mouse drag
        double deltaX = curScaleX
                * (point.getX() - m_shapeDragResizePos.getX());
        double deltaY = curScaleY
                * (point.getY() - m_shapeDragResizePos.getY());

        boolean tooFar = false;

        if (m_shapeSelSquare != Square.NONE) { // Resizing
            Rectangle2D br = m_origShape.getBounds2D();

            // TODO: Fix "going too far" problem
            if (m_shapeSelSquare == Square.LEFT) {
                if (!(tooFar = deltaX >= br.getWidth())) {
                    resizeScaleX = (br.getWidth() - deltaX) / br.getWidth();
                    shiftX = deltaX + br.getX() * (1 - resizeScaleX);
                }
            } else if (m_shapeSelSquare == Square.TOP) {
                if (!(tooFar = deltaY >= br.getHeight())) {
                    resizeScaleY = (br.getHeight() - deltaY) / br.getHeight();
                    shiftY = deltaY + br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.RIGHT) {
                if (!(tooFar = deltaX <= -br.getWidth())) {
                    resizeScaleX = (br.getWidth() + deltaX) / br.getWidth();
                    shiftX = br.getX() * (1 - resizeScaleX);
                }
            } else if (m_shapeSelSquare == Square.BOTTOM) {
                if (!(tooFar = deltaY <= -br.getHeight())) {
                    resizeScaleY = (br.getHeight() + deltaY) / br.getHeight();
                    shiftY = br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.TOP_LEFT) {
                if (!(tooFar = deltaX >= br.getWidth())
                        && (!(tooFar = deltaY >= br.getHeight()))) {
                    resizeScaleX = (br.getWidth() - deltaX) / br.getWidth();
                    shiftX = deltaX + br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() - deltaY) / br.getHeight();
                    shiftY = deltaY + br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.TOP_RIGHT) {
                if (!(tooFar = deltaX <= -br.getWidth())
                        && (!(tooFar = deltaY >= br.getHeight()))) {
                    resizeScaleX = (br.getWidth() + deltaX) / br.getWidth();
                    shiftX = br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() - deltaY) / br.getHeight();
                    shiftY = deltaY + br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.BOTTOM_LEFT) {
                if (!(tooFar = deltaX >= br.getWidth())
                        && (!(tooFar = deltaY <= -br.getHeight()))) {
                    resizeScaleX = (br.getWidth() - deltaX) / br.getWidth();
                    shiftX = deltaX + br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() + deltaY) / br.getHeight();
                    shiftY = br.getY() * (1 - resizeScaleY);
                }
            } else if (m_shapeSelSquare == Square.BOTTOM_RIGHT) {
                if (!(tooFar = deltaX <= -br.getWidth())
                        && (!(tooFar = deltaY <= -br.getHeight()))) {
                    resizeScaleX = (br.getWidth() + deltaX) / br.getWidth();
                    shiftX = br.getX() * (1 - resizeScaleX);
                    resizeScaleY = (br.getHeight() + deltaY) / br.getHeight();
                    shiftY = br.getY() * (1 - resizeScaleY);
                }
            }
        } else { // Dragging
            shiftX = curScaleX * (point.getX() - m_shapeDragResizePos.getX());
            shiftY = curScaleY * (point.getY() - m_shapeDragResizePos.getY());
        }

        if (!tooFar) {
            // Set up transform
            transform.setTransform(resizeScaleX, 0, 0, resizeScaleY, shiftX,
                    shiftY);

            // Drag / scale the shape
            m_origShape = transform.createTransformedShape(m_origShape);

            // Remember the last mouse position
            m_shapeDragResizePos.setLocation(point);
        }
    }

    /**
     * Given the new mouse position, translates the annotation text.
     *
     * @param point mouse location
     */
    protected void dragText(Point2D point) {
        // calculate shift and scale
        double curScaleX = m_origImgDim.getWidth() / m_transImgDim.getWidth();
        double curScaleY = m_origImgDim.getHeight() / m_transImgDim.getHeight();
        double shiftX = curScaleX * (point.getX() - m_textDragPos.getX());
        double shiftY = curScaleY * (point.getY() - m_textDragPos.getY());

        // Drag the text
        m_origTextOffset.setLocation(m_origTextOffset.getX() + shiftX,
                m_origTextOffset.getY() + shiftY);

        // Remember the last mouse position
        m_textDragPos.setLocation(point);
    }

    /**
     * Handles mouseReleased() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseReleased(MouseEvent e) {
        if (!m_isEditable)
            return;
        if (m_shapeDragResizePos != null) {
            // Done dragging/resizing the annotation's shape
            // Reset shape selection square
            m_shapeSelSquare = Square.NONE;
            m_shapeDragResizePos = null;
            e.consume();
        } else if (m_textDragPos != null) {
            // Done dragging annotation's text
            m_textDragPos = null;
            e.consume();
        } else if (SwingUtilities.isRightMouseButton(e))
            if (m_shapeSelected) {
                if (m_popupMenu != null) {
                    m_popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            } else if (m_textSelected) {
                if (m_popupMenu != null) {
                    m_popupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
    }

    /**
     * Handles keyPressed() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyPressed(KeyEvent e) {
        int keyCode = e.getKeyCode();

        // If text is selected, edit it
        if (m_textEditable) {
            // If text was selected right to left, fix indices
            if (m_anchorEnd > m_activeEnd) {
                m_insertionIdx = m_activeEnd;
                m_activeEnd = m_anchorEnd;
                m_anchorEnd = m_insertionIdx;
            }

            if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT) {
                if (handleArrowKey(keyCode == KeyEvent.VK_RIGHT)) {
                    e.consume();
                }
            } else if (keyCode == KeyEvent.VK_BACK_SPACE) {
                if ((m_insertionIdx >= 0) && (m_activeEnd > 0)) {
                    m_text = (m_anchorEnd != m_activeEnd) ? m_text.substring(0,
                            m_anchorEnd)
                            + m_text.substring(m_activeEnd) : m_text.substring(
                            0, --m_insertionIdx)
                            + m_text.substring(m_insertionIdx + 1);
                    m_activeEnd = m_anchorEnd = m_insertionIdx;
                    e.consume();
                }
            } else if (keyCode == KeyEvent.VK_DELETE) {
                if (m_insertionIdx < m_text.length()) {
                    m_text = (m_anchorEnd != m_activeEnd) ? m_text.substring(0,
                            m_anchorEnd)
                            + m_text.substring(m_activeEnd) : m_text.substring(
                            0, m_insertionIdx)
                            + m_text.substring(m_insertionIdx + 1);
                    m_activeEnd = m_anchorEnd = m_insertionIdx;
                    e.consume();
                }
            }
        }

        // If Shape or text is selected - move it
        if ((!m_textEditable) && (!e.isConsumed())) {
            if ((keyCode == KeyEvent.VK_RIGHT) || (keyCode == KeyEvent.VK_LEFT)
                    || (keyCode == KeyEvent.VK_UP)
                    || (keyCode == KeyEvent.VK_DOWN)) {
                double shiftX = (keyCode == KeyEvent.VK_RIGHT) ? SHAPE_SHIFT_STEP
                        : ((keyCode == KeyEvent.VK_LEFT) ? -SHAPE_SHIFT_STEP
                        : 0);
                double shiftY = (keyCode == KeyEvent.VK_DOWN) ? SHAPE_SHIFT_STEP
                        : ((keyCode == KeyEvent.VK_UP) ? -SHAPE_SHIFT_STEP : 0);
                ;

                if (m_shapeSelected) {
                    // Set up transform
                    transform.setTransform(1, 0, 0, 1, shiftX, shiftY);

                    // Shift the shape
                    m_origShape = transform.createTransformedShape(m_origShape);
                    e.consume();
                } else if (m_textSelected) {
                    // Shift the text
                    m_origTextOffset.setLocation(m_origTextOffset.getX()
                            + shiftX, m_origTextOffset.getY() + shiftY);
                    e.consume();
                }
            }
        }
    }

    /**
     * Handles keyTyped() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyTyped(KeyEvent e) {
        // Only care about keyboard events if text is selected
        if (m_textEditable) {
            char c = e.getKeyChar();

            if (!e.isActionKey() && (Character.getType(c) != Character.CONTROL)) {
                if (m_anchorEnd > m_activeEnd) { // If selected right to
                    // left, fix indices
                    m_insertionIdx = m_activeEnd;
                    m_activeEnd = m_anchorEnd;
                    m_anchorEnd = m_insertionIdx;
                }
                if (m_text == null)
                    m_text = "";
                m_text = m_text.substring(0, m_anchorEnd) + c
                        + m_text.substring(m_activeEnd);
                m_activeEnd = m_anchorEnd = ++m_insertionIdx;
                e.consume();
            }
        }
    }

    /**
     * Update the insertion index in response to an arrow key.
     *
     * @return True iff the key hit is handled successfully.
     */
    protected boolean handleArrowKey(boolean rightArrow) {
        boolean success = false;

        if ((m_transLayout != null) && !isTextEmpty()) {
            TextHitInfo newPosition = rightArrow ? m_transLayout
                    .getNextRightHit(m_insertionIdx) : m_transLayout
                    .getNextLeftHit(m_insertionIdx);

            // getNextRightHit() / getNextLeftHit() will return null if
            // there is not a caret position to the right (left) of the
            // current position.
            if (newPosition != null) { // Update insertionIndex.
                m_activeEnd = m_anchorEnd = m_insertionIdx = newPosition
                        .getInsertionIndex();
                success = true;
            }
        }

        return success;
    }

    /**
     * Compute a location within this Component for textLayout's origin, such
     * that textLayout is centered horizontally and vertically.
     */
    protected Point2D computeLayoutOrigin() {
        Rectangle2D bounds = m_transLayout.getBounds();
        Point2D.Float origin = new Point2D.Float();

        origin.x = (float) (bounds.getX() + m_transTextOffset.getX() + (bounds
                .getWidth() - m_transLayout.getAdvance()) / 2);
        origin.y = (float) (bounds.getY() + m_transTextOffset.getY() + (bounds
                .getHeight()
                - m_transLayout.getDescent() + m_transLayout.getAscent()) / 2);

        return origin;
    }

    /**
     * Returns true is the supplied location is near the shape boundary.
     *
     * @param point                       Location to be checked
     * @param selectOnlyIfClickedNearEdge if true this method will return true only if the point lies
     *                                    near the boundary of the figure, else this method will check
     *                                    to see if the point was clicked anywhere inside the shape.
     * @return true iff a small square around the point is near the shape
     * boundary.
     */
    public boolean isInShape(Point2D point, boolean selectOnlyIfClickedNearEdge) {
        boolean insideShape = false;
        if (m_transShape != null) {
            Rectangle2D bounds = m_transShape.getBounds();
            // Consider point if shape is size zero (special case for mouse
            // clicks)
            if ((bounds.getHeight() == 0) && (bounds.getWidth() == 0)) {
                insideShape = (bounds.getX() == point.getX())
                        && (bounds.getY() == point.getY());
            } else { // Set the feeler rectangle around the point
                feelerRectangle.setRect(point.getX() - FEELER_SQUARE_SIZE / 2,
                        point.getY() - FEELER_SQUARE_SIZE / 2,
                        FEELER_SQUARE_SIZE, FEELER_SQUARE_SIZE);
                insideShape = m_transShape.intersects(feelerRectangle);
                if (selectOnlyIfClickedNearEdge) {
                    if (insideShape) {
                        // If the click point feeler square intersects but is
                        // not
                        // contained with in the shape then this indicates that
                        // the
                        // user clicked near the boundary of the shape.
                        return (!m_transShape.contains(feelerRectangle));
                    }
                }
            }
        }
        return insideShape;
    }

    /**
     * Returns true if the supplied location is contained in the shape's text.
     *
     * @param point Location to be checked
     * @return true iff the point is contained within the shape's text.
     */
    protected boolean isInText(Point2D point) {
        boolean bInside = false;

        if (m_transLayout != null && !isTextEmpty()) {
            Shape shape = m_transLayout.getLogicalHighlightShape(0, m_text
                    .length());
            transform.setTransform(1, 0, 0, 1, m_transTextOffset.getX(),
                    m_transTextOffset.getY());

            // Transform and check
            bInside = transform.createTransformedShape(shape).contains(point);
        }

        return bInside;
    }

    /**
     * This method unselects the annotation.
     */
    public void unselectAnnotation() {
        // This method unselects the annotation.
        // This is called during the print function to
        // unselect any selected annotation, since
        // selected annotations distort the print out.
        m_shapeSelSquare = isInShapeSelectionSquare(new Point2D.Float(-1000,
                -1000));
        selectShape(false);
        selectText(false);
    }

    /**
     * Checks if the point is in any of the 8 selection squares.
     *
     * @param point Point to be checked
     * @return a value of the enumerated type Square.
     */
    public int isInShapeSelectionSquare(Point2D point) {
        if (m_transShape != null) {
            double x = m_transShape.getBounds().getX();
            double y = m_transShape.getBounds().getY();
            double w = m_transShape.getBounds().getWidth();
            double h = m_transShape.getBounds().getHeight();
            double size = SELECTION_SQUARE_SIZE;
            double halfsize = SELECTION_SQUARE_SIZE * 0.5;
            Rectangle2D square = new Rectangle2D.Double(x - halfsize, y
                    - halfsize, size, size);

            if (square.contains(point)) {
                return Square.TOP_LEFT;
            }

            square.setRect(x + w * 0.5 - halfsize, y - halfsize, size, size);
            if (square.contains(point)) {
                return Square.TOP;
            }

            square.setRect(x + w - halfsize, y - halfsize, size, size);
            if (square.contains(point)) {
                return Square.TOP_RIGHT;
            }

            square.setRect(x - halfsize, y + h * 0.5 - halfsize, size, size);
            if (square.contains(point)) {
                return Square.LEFT;
            }

            square
                    .setRect(x + w - halfsize, y + h * 0.5 - halfsize, size,
                            size);
            if (square.contains(point)) {
                return Square.RIGHT;
            }

            square.setRect(x - halfsize, y + h - halfsize, size, size);
            if (square.contains(point)) {
                return Square.BOTTOM_LEFT;
            }

            square
                    .setRect(x + w * 0.5 - halfsize, y + h - halfsize, size,
                            size);
            if (square.contains(point)) {
                return Square.BOTTOM;
            }

            square.setRect(x + w - halfsize, y + h - halfsize, size, size);
            if (square.contains(point)) {
                return Square.BOTTOM_RIGHT;
            }
        }

        return Square.NONE;
    }

    /**
     * Draws grey selection boxes around text and/or shape if either is
     * selected.
     *
     * @param g2d Graphics context
     */
    protected void drawSelections(Graphics2D g2d) {
        Color oldColor = g2d.getColor();
        Stroke oldStroke = g2d.getStroke();

        // Change color and stroke temporarily
        g2d.setColor(SELECTION_COLOR);
        g2d.setStroke(selectionStroke);

        if (m_shapeSelected && (m_transShape != null)) {
            drawShapeHighlight(g2d);
        }

        if (m_textSelected) {
            drawTextHighlight(g2d);
        }

        // Restore the original color and stroke
        g2d.setColor(oldColor);
        g2d.setStroke(oldStroke);
    }

    /**
     * Draws 8 rectangles around a selection along with the selection box.
     */
    protected void drawShapeHighlight(Graphics2D g2d) {
        double x = m_transShape.getBounds().getX();
        double y = m_transShape.getBounds().getY();
        double w = m_transShape.getBounds().getWidth();
        double h = m_transShape.getBounds().getHeight();

        double size = SELECTION_SQUARE_SIZE;
        double halfsize = SELECTION_SQUARE_SIZE * 0.5;

        // Draw selection and the 8 squares
        g2d.draw(m_transShape.getBounds());

        Rectangle2D square = new Rectangle2D.Double(x - halfsize, y - halfsize,
                size, size);
        g2d.fill(square);
        square.setRect(x + w * 0.5 - halfsize, y - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x + w - halfsize, y - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x - halfsize, y + h * 0.5 - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x + w - halfsize, y + h * 0.5 - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x - halfsize, y + h - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x + w * 0.5 - halfsize, y + h - halfsize, size, size);
        g2d.fill(square);
        square.setRect(x + w - halfsize, y + h - halfsize, size, size);
        g2d.fill(square);
    }

    /**
     * Draws text highlight box along with the shape connector (if text is being
     * dragged)
     */
    protected void drawTextHighlight(Graphics2D g2d) {
        // Get the text outline
        Shape shape = m_transLayout.getLogicalHighlightShape(0,
                ((m_text != null) && (m_text.length() > 0)) ? m_text.length()
                        : EMPTY_TEXT.length());

        // Shift the shape
        transform.setTransform(1, 0, 0, 1, m_transTextOffset.getX(),
                m_transTextOffset.getY());
        shape = transform.createTransformedShape(shape);

        // Draw it
        g2d.draw(shape);

        // If the text is being dragged, show shape connector
        if (m_textDragPos != null && !isShapeEmpty()) {
            int fromX = (int) m_transShape.getBounds2D().getCenterX();
            int fromY = (int) m_transShape.getBounds2D().getCenterY();
            int toX = (int) shape.getBounds2D().getCenterX();
            int toY = (int) shape.getBounds2D().getCenterY();

            g2d.drawLine(fromX, fromY, toX, toY);
        }
    }

    /**
     * Thie mathos provides the implementation of the signum function. This
     * method takes in a double value and returns 0f is the value is 0, -1.0f if
     * the value if less than zero and +1.0f if the value is greater than zero.
     * This method is similar to Math.signum() (which is included in jdk1.5).
     * However we did not want to use very specific jdk 1.5 code to be backward
     * compatible. So this method is used so that jdk 1.5 specific Math.signum
     * method could be avoided.
     *
     * @param input double value whose signum is to be returned.
     * @return float 0 if input is 0, -1.0 if input is less than 0 and 1.0 if
     * input is greater than 0.
     */

    private float getSignum(double input) {
        if (input < 0)
            return -1.0f;
        else if (input > 0)
            return +1.0f;
        else
            return 0.0f;
    }

    /**
     * @return true iff Annotation's text is not empty and not null.
     */
    protected boolean isTextEmpty() {
        return !((m_text != null) && m_text.length() > 0);
    }

    /**
     * Returns the original shape. Used by AnnotationControl to adjust the shape
     * while it is being drawn, so that drawing is more efficient.
     *
     * @return Original Shape.
     */
    public Shape getOrigShape() {
        return m_origShape;
    }

    public boolean isPersistable() {
        return m_isPersistable;
    }

    public boolean isEditable() {
        return m_isEditable;
    }

    public void makeUneditable() {
        m_isEditable = false;
    }

    public void makeUnpersistable() {
        m_isPersistable = false;
    }

    /**
     * @return true if the shape is empty (e.g. shape associated with text).
     */
    protected boolean isShapeEmpty() {
        return ((m_origShape == null) || ((m_origShape.getBounds().getWidth() == 0) && (m_origShape
                .getBounds().getHeight() == 0)));
    }

    /**
     * This method sets the text offset. Through this method we can choose at
     * which location, inside a shape, does the corresponding text appear.
     *
     * @param x the x-axis offset from the center of the shape
     * @param y the y-axis offset from the top of the shape.
     */
    public void setTextOffset(int x, int y) {
        if (m_origTextOffset != null)
            m_origTextOffset.setLocation(x, y);
    }

    /**
     * Returns the text associated with this annotation.
     *
     * @return The text associated with this annotation.
     */
    public String getAnnotationText() {
        return m_text;
    }

    /**
     * Sets the text that will appear against this annotation
     *
     * @param annotationText The text to associate with this annotation.
     */
    public void setAnnotationText(String annotationText) {
        this.m_text = annotationText;
    }

    /**
     * Sets the associated Window. Applies only when the Annotation is used to
     * represent EDS graph selection area or AFM 3D area selection. Upon
     * selection of this Annotation the m_associatedWindow will be brought to
     * front.
     *
     * @param m_associatedWindow
     */
    public void setAssociatedWindow(JFrame associatedWindow) {
        m_associatedWindow = associatedWindow;
    }

    /**
     * Simple enumerated type for identifying the selection's squares.
     */
    public interface Square {
        public static final int NONE = -1;

        public static final int LEFT = 0;

        public static final int TOP = 1;

        public static final int RIGHT = 2;

        public static final int BOTTOM = 3;

        public static final int TOP_LEFT = 4;

        public static final int TOP_RIGHT = 5;

        public static final int BOTTOM_LEFT = 6;

        public static final int BOTTOM_RIGHT = 7;
    }
}
