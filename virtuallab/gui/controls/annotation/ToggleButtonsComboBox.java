/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import virtuallab.Log;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.ListCellRenderer;

public class ToggleButtonsComboBox extends JComboBox implements ListCellRenderer, ActionListener {

    private static final Log log = new Log(Annotation.class.getName());

    private static final long serialVersionUID = 5086208704100943619L;
    /**
     * This is a convenient place holder for the currently selected buttons.
     * By keeping refernce to the currently selected button we avoid unnecessary
     * iteraiton over all the buttons to identify the selected button.
     *
     * @see ToolComboBox#updateSelection(JToggleButton)
     */
    protected JToggleButton m_selectedButton = null;
    /**
     * Used to inform the annotation control of some interesting
     * events that occurred on me e.g. itemStateChaanged.
     */
    protected AnnotationControl m_annotationControl;
    /**
     * The map that contains all the items to be displayed in the drop down.
     * Note that the values of the items in the map will be added as
     * items in the drop down. We maintain a map so that we can easily lookup
     * items if needed, instead of having to iterate over each item which
     * would be the case if we had used an ArrayList.
     */
    protected LinkedHashMap m_comboBoxItems = new LinkedHashMap();

    /**
     * indicates whether the observers/listeneres should be informed that an item state change has
     * occurred. Through this flag we make sure that we only report the events that
     * no one has any objection to. This is required since the EDS/AFM tends to manipulate
     * the annotationcontrols programmatically - when they do so they would set this flag to
     * false so that the item state change happens silently and nobody is made aware of it.
     * Note that this is a one time flag when set to false it will only ignore the next
     * item state event any event after that will NOT be ignored unless someone else
     * explicitly sets this flag to false again. Callers who wish to set this to false
     * must expect that only the next itemstate event will be ignored.
     */


    /**
     * Must be explicitly called by ALL the subclasses. This sets up
     * the dropdown with the items.
     *
     * @param annotationControl
     */
    public ToggleButtonsComboBox(AnnotationControl annotationControl) {
        m_annotationControl = annotationControl;
        setRenderer(this);
        addActionListener(this);
        createAndPopulateComboBoxItems();
        Iterator iterator = m_comboBoxItems.values().iterator();
        while (iterator.hasNext()) {
            addItem(iterator.next());
        }

        setPreferredSize(new Dimension(45, 26));
    }

    public void actionPerformed(ActionEvent e) {
        JComboBox cb = (JComboBox) e.getSource();
        updateSelection((JToggleButton) cb.getSelectedItem());
    }

    /**
     * This method allows the caller to programmatically select an item from the
     * drop down.
     *
     * @param toBeSelectedItemName
     */
    public void setSelection(String toBeSelectedItemName) {
        Iterator iterator = m_comboBoxItems.keySet().iterator();
        while (iterator.hasNext()) {
            JToggleButton item = (JToggleButton) iterator.next();
            if (item.getName().equals(toBeSelectedItemName)) {
                updateSelection(item);
                return;
            }
        }
    }

    public void doNotReportNextEvent() {
        /**DO NOT DELETE
         * sets the internal variables such that the next item state change event is consumed
         * and is not communicated back to any listener/observer. This helps in cases when we
         * want to perform the item state change silently.
         */
    }

    public void setSelection(String toBeSelectedOrUnselectedItemName, boolean shouldPress) {
        if (shouldPress) {
            setSelection(toBeSelectedOrUnselectedItemName);
        } else if (toBeSelectedOrUnselectedItemName.equals(m_selectedButton.getName())) {
            // the button was selected so we unselect it, and then show the default
            // option (the first item in the dropdown) as selected.
            updateSelection((JToggleButton) m_comboBoxItems.keySet().iterator().next());
        }
    }

    /**
     * Creates all the items that will be populated in the combo box.
     * Should add these items in the m_comboBoxItems in the order in
     * which they should appear in the dropdown combo.
     */
    protected void createAndPopulateComboBoxItems() {
    }

    /**
     * Gets called everytime a combobox item needs to be rendered.
     */
    public Component getListCellRendererComponent(JList list, Object itemValue, int index, boolean isSelected, boolean cellHasFocus) {
        JToggleButton buttonToBeRendered = (JToggleButton) m_comboBoxItems.get(itemValue);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        buttonPanel.add((JToggleButton) m_comboBoxItems.get(buttonToBeRendered));
        return buttonPanel;
    }

    /**
     * This method takes in a toggle button that is to be shown selected.
     *
     * @param selectedButton
     */
    protected void updateSelection(JToggleButton selectedButton) {
        if (selectedButton == m_selectedButton) {
            m_selectedButton.setSelected(true);
            setSelectedItem(m_selectedButton);
            return;
        }
        m_selectedButton.setSelected(false);
        selectedButton.setSelected(true);
        m_selectedButton = selectedButton;
        setSelectedItem(selectedButton);
    }
}
