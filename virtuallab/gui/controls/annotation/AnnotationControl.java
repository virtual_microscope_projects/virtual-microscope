/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import org.w3c.dom.*;
import virtuallab.Configuration;
import virtuallab.Log;
import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.gui.Microscope;
import virtuallab.gui.SideControl;
import virtuallab.gui.ViewControl;
import virtuallab.util.Constants;
import virtuallab.util.ControlBase;
import virtuallab.util.Locale;
import virtuallab.util.TranslatedOptionPane;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

/**
 * This class implements the Annotation Control. The Annotation Control provides
 * access to specimen annotation functionality.
 */
public class AnnotationControl extends ControlBase implements SideControl, ViewControl {

    private static final Log log = new Log(AnnotationControl.class.getName());

    /**
     * Tag that will hold specimen unique id
     */
    public static final String XML_TAG_SPECIMEN_NAME = "SpecimenUniqueName";
    /**
     * The name attribute
     */
    public static final String XML_ATTRIB_NAME = "Name";
    public static final String XML_TAG_SHORT_DESCRIPTION = "ShortDescription";
    public static final String XML_TAG_TEXT = "Text";
    /**
     * Tag for the Annotation XML element.
     */
    protected static final String XML_TAG_ANNOTATIONS = "Annotations";
    /**
     * Default annotation line/text color.
     */
    protected final static Color DEF_COLOR = Color.WHITE;
    /**
     * Default annotation line width.
     */
    protected final static float DEF_LINE_WIDTH = 1.0f;
    /**
     * Choice of line widths presented in line width selection combo box.
     */
    protected final static String[] LINE_WIDTH_SELECTIONS = {"1.0", "2.0", "3.0", "4.0", "5.0", "6.0", "7.0", "8.0", "9.0", "10.0"};
    /**
     * Choice of fonts presented in font selection combo box.
     */
    protected final static String[] FONT_SELECTIONS = {"Serif", "SansSerif", "Monospaced", "Dialog", "DialogInput"};
    private static final long serialVersionUID = -975785258403563222L;
    protected Microscope m_microscope;
    protected MicroscopeView m_display;
    protected Specimen.ControlDescriptor m_controlDescriptor;
    protected ButtonGroup m_bttnGroup;
    /**
     * Last mouse press position, relative to the stagePosition
     */
    protected Point2D m_pressPosition;
    /**
     * Chosen color
     */
    protected Color m_curColor;
    /**
     * Chosen line thickness
     */
    protected float m_curLineWidth;
    /**
     * Chosen font name.
     */
    protected String m_curFontName;
    /**
     * Line width selection combo box.
     */
    protected JComboBox m_lineWidthCombo;
    /**
     * Font selection combo box.
     */
    protected JComboBox m_fontCombo;
    /**
     * Currently active Annotation
     */
    protected Annotation m_actAnnot;
    /**
     * Current specimen's unique name
     */
    protected String m_curSpecimenName;
    /**
     * Ignore mouse click flag.
     */
    protected boolean m_ignoreClick;
    /**
     * Unique name/type of this control.
     */
    protected String m_controlName;
    /**
     * List of all of the currently displayed annotations.
     */
    protected LinkedList m_annotations;

    /**
     * Holds the reference to the currently loaded annotation xml file.
     * Used to track of which file to persist changes to if the user
     * does an action which can result in lost changes.
     */
    private SpecimenAnnotationFile m_currentlyLoadedAnnotations;
    /**
     * Indicates if an annotation(s) have been modified and hence need to be saved.
     * The following actions will set this flag to true:<br>
     * <ol>
     * <li>annotation creation</li>
     * <li>annotation deletion.</li>
     * <li>annotation resize.</li>
     * <li>annotation move.</li>
     * <li>annotation text modification.</li>
     * <li>annotation color change.</li>
     * <li>annotation line width change.</li></ol>
     */
    private boolean m_isDirty = false;
    /**
     * should the shape being drawn be symmetrical ? i.e. draw a square, circle and
     * lines at 0,45,90,135,180,225,270,315,360 angles. This is used primarily
     * when the caller wants to use the annotation under the hood for something
     * other than annotating the image (e.g. ColormapMenu)
     *
     * @see ColormapMenu.java
     */
    private boolean m_drawSymmetricalShape = false;
    /**
     * If set to true any new annotations drawn this point forward will be
     * readonly. This is used by the EDS/AFM controls to draw the
     * selection areas as readonly.
     */
    private boolean m_drawReadonlyAnnotations = false;
    private ArrayList m_listeners = new ArrayList();
    /**
     * The color tool button that is currently selected.
     * <p>
     * private JToggleButton m_selectedToolButton;
     * <p>
     * private ToolComboBox m_toolComboBox;
     */
    private LineWidthComboBox m_lineWidthComboBox;
    private ColorComboBox m_colorComboBox;
    private ToolsComboBox m_toolsMenu;

    private AnnotationFileComboBox m_annotationFileComboBox;
    /**
     * Current annotation that is being drawn
     */
    private Annotation m_drawnAnnot;
    private PopupMenuActionListener m_popuMenuActionListener = new PopupMenuActionListener();

    public AnnotationControl() {
        m_drawnAnnot = null;
        m_pressPosition = null;
        m_curColor = DEF_COLOR;
        m_curLineWidth = DEF_LINE_WIDTH;
        m_curFontName = FONT_SELECTIONS[1];//sans-serif
        m_actAnnot = null;
        m_ignoreClick = false;
        m_controlName = null;
        m_annotations = new LinkedList();
        getAccessibleContext().setAccessibleName(Locale.AnnotationControlPanel(Configuration.getApplicationLanguage()));
        getAccessibleContext().setAccessibleDescription(Locale.AnnotationControlDescription(Configuration.getApplicationLanguage()));
    }

    public SpecimenAnnotationFile getCurrentlyLoadedAnnotationFile() {
        return m_currentlyLoadedAnnotations;
    }

    public void setCurrentlyLoadedAnnotationFile(SpecimenAnnotationFile loadedFile) {
        m_currentlyLoadedAnnotations = loadedFile;
    }

    /**
     * Initializes this control s a view control.
     *
     * @param controlName unique name/type of this control
     * @param display     MicroscopeView object
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        if (display == null || display.getCurrentSpecimen() == null || controlName == null) {
            throw new IllegalArgumentException("Null microscope view, specimen, or name");
        }

        m_display = display;
        if (m_controlName != null) {
            m_controlName = controlName;
        }
        m_curSpecimenName = display.getCurrentSpecimen().getUniqueName();
        updateGUI();
    }

    /**
     * Initializes this control as a side control.
     *
     * @param controlName unique name/type of this control
     * @param controlDesc control descriptor
     * @param scope       Microscope object
     */
    public void initSideControl(String controlName, Specimen.ControlDescriptor controlDesc, Microscope scope) {
        if (controlDesc == null || scope == null || controlName == null) {
            throw new IllegalArgumentException("Null control descriptor, microscope, or name");
        }

        m_microscope = scope;
        {
            m_controlName = controlName;
        }
        m_controlDescriptor = controlDesc;
        m_title = Locale.translateSideMenuTitles(controlDesc.getNamedParam("DisplayString"));

        if (m_title == null || m_title.equals("")) {
            m_title = Locale.Annotation(Configuration.getApplicationLanguage());
        }

        updateGUI();
    }

    /**
     * The mandatory method that must be implemented as per the requirements of
     * the implemented interface. However the this control is not interested in
     * any mousewheel events so the method is just an empty stub and does not do
     * anything.
     *
     * @param e - MouseWheelEvent
     */
    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    /**
     * @return name/type of this control
     */
    public String getControlName() {
        return m_controlName;
    }

    /**
     * This method draws the complete Annotation Control panel with all its
     * children panels and drop downs.
     */
    private void updateGUI() {
            // need to wait for all the needed variables to be available before
            // we can start laying out this control.
        if (m_display == null)
            return;

        removeAll();
        setMaximumSize(new Dimension(1024, getMinimumSize().height));
        setBorder(BorderFactory.createTitledBorder(m_title));
        setLayout(new GridBagLayout());
        JPanel annotationSelectorPanel = createAnnotationFileChooserPanel();
        JPanel toolPanel = createAnnotationToolChooserPanel();
        JPanel buttonPanel = createAnnotationFileOperationPanel();
        layoutAllPanels(annotationSelectorPanel, toolPanel, buttonPanel);
    }

    /**
     * Lays out all the children panels in the main Annotation control panel.
     *
     * @param annotationSelectorPanel
     * @param toolPanel
     * @param buttonPanel
     */
    private void layoutAllPanels(JPanel annotationSelectorPanel, JPanel toolPanel, JPanel buttonPanel) {
        /** Adding all panels to the Annotation control Panel **/
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 1, 5, 1);
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.5;
        add(annotationSelectorPanel, c);

        c = new GridBagConstraints();
        c.insets = new Insets(5, 1, 5, 1);
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.5;
        add(toolPanel, c);

        c = new GridBagConstraints();
        c.insets = new Insets(5, 1, 5, 1);
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0;
        add(buttonPanel, c);
    }

    /**
     * Creates the panel that contains the Save As..., Save and Close
     * buttons.
     *
     * @return
     */
    private JPanel createAnnotationFileOperationPanel() {
        /******** Draw (Save, Save As..., Close) buttons ********/
        JPanel buttonPanel = new JPanel();
        Font buttonFont = new Font("Verdana", Font.BOLD, 11);
        Insets insets = new Insets(0, 0, 0, 0);
        JButton m_bttnSave = new JButton(Locale.Save(Configuration.getApplicationLanguage()));
        JButton m_bttnSaveAs = new JButton(Locale.SaveAs(Configuration.getApplicationLanguage()));
        JButton m_bttnClose = new JButton(Locale.Close(Configuration.getApplicationLanguage()));
        buttonPanel.setLayout(new GridBagLayout());

        m_bttnSaveAs.setMargin(insets);
        m_bttnSaveAs.setFont(buttonFont);
        m_bttnSaveAs.setActionCommand("Save As");

        m_bttnSave.setMargin(insets);
        m_bttnSave.setFont(buttonFont);
        m_bttnSave.setActionCommand("Save");

        m_bttnClose.setMargin(insets);
        m_bttnClose.setFont(buttonFont);
        m_bttnClose.setActionCommand("Close");

        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 2);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        buttonPanel.add(m_bttnSaveAs, c);
        c.insets = new Insets(0, 2, 0, 2);
        c.gridx = 1;
        buttonPanel.add(m_bttnSave, c);
        c.insets = new Insets(0, 2, 0, 0);
        c.gridx = 2;
        buttonPanel.add(m_bttnClose, c);
        FileOperationListener listener = new FileOperationListener();
        m_bttnSave.addActionListener(listener);
        m_bttnSaveAs.addActionListener(listener);
        m_bttnClose.addActionListener(listener);
        return buttonPanel;
    }

    /**
     * Creates the Panel which will contain the Annotation Tools drop down
     *
     * @return
     */
    private JPanel createAnnotationToolChooserPanel() {
        /******* Draw Tool, color, line width dropdowns ********/
        m_toolsMenu = new ToolsComboBox(this);

        m_lineWidthComboBox = new LineWidthComboBox(this);
        m_colorComboBox = new ColorComboBox(this);
        JPanel toolPanel = new JPanel();
        toolPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(0, 0, 0, 8);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        toolPanel.add(m_toolsMenu, c);
        c.insets = new Insets(0, 8, 0, 8);
        c.gridx = 1;
        toolPanel.add(m_colorComboBox, c);
        c.insets = new Insets(0, 8, 0, 0);
        c.gridx = 2;
        toolPanel.add(m_lineWidthComboBox, c);
        return toolPanel;
    }

    /**
     * Creates the panel which will contain the Annotation XML Files drop down.
     *
     * @return
     */
    private JPanel createAnnotationFileChooserPanel() {
        /******* Draw annotation chooser combo ********/
        m_annotationFileComboBox = new AnnotationFileComboBox(this,
                m_display.getMicroscope().getConfig().getAnnotDir(),
                m_display.getCurrentSpecimen().getUniqueName());
        m_annotationFileComboBox.setPreferredSize(new Dimension(160, 25));
        JPanel annotationSelectorPanel = new JPanel();
        annotationSelectorPanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        annotationSelectorPanel.add(m_annotationFileComboBox, c);
        return annotationSelectorPanel;
    }

    /**
     * Creates and initializes the tool panel.
     *
     * @return tool panel.
     */
    protected JPanel createToolPanel() {
        return new JPanel();
    }

    /**
     * This method allows various observers to register themselves so that they
     * will get notified when a change occurs in AnnotationControl.
     *
     * @param newListener
     */
    public void addAnnotationControlChangeListener(AnnotationControlChangeListener newListener) {
        if (!m_listeners.contains(newListener))
            m_listeners.add(newListener);
    }

    /**
     * This method allows various observers to unregisters themselves so that
     * they are nolonger notified when a change occurs in AnnotationControl.
     *
     * @param oldListener
     */
    public void removeAnnotationControlChangeListener(AnnotationControlChangeListener oldListener) {
        m_listeners.remove(oldListener);
    }

    /**
     * Removes all annotations from the collection and repaints.
     */
    public void clearAnnotations() {
        LinkedList readonlyAnnotations = new LinkedList();
        readonlyAnnotations.addAll(m_annotations);
        if (!m_annotations.isEmpty()) {
            for (Iterator iter = m_annotations.iterator(); iter.hasNext(); ) {
                Annotation annotation = (Annotation) iter.next();
                if (annotation.isEditable())
                    readonlyAnnotations.remove(annotation);
            }
            m_annotations.clear();
            m_annotations = readonlyAnnotations;
            m_display.repaint();
        }
    }

    /**
     * Unpersists Annotations from a file.
     *
     * @param annotationFile File containing persisted annotations.
     * @throws Exception TODO handle better maybe?
     */
    public void unPersistAnnotations(SpecimenAnnotationFile annotationFile) throws Exception {
        // Clean up
        clearAnnotations();
        m_currentlyLoadedAnnotations = annotationFile;
        File file = annotationFile.getAnnotationFile();

        // Read annotations from a file
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(file);
        NodeList list = doc.getElementsByTagName(XML_TAG_ANNOTATIONS);
        Annotation curAnnot;

        if (list != null && list.getLength() == 1 && list.item(0).getNodeType() == Node.ELEMENT_NODE) {
            list = list.item(0).getChildNodes();

            for (int idx = 0; idx < list.getLength(); idx++) {
                if ((curAnnot = Annotation.unpersistFromXMLNode(list.item(idx),this)) != null) {
                    m_annotations.add(curAnnot);
                    m_colorComboBox.addCustomColor(curAnnot.m_shapeColor);
                    m_colorComboBox.addCustomColor(curAnnot.m_textColor);
                }
            }

            m_display.repaint();
        }
    }

    protected void persistAnnotations(SpecimenAnnotationFile annotationFile) throws Exception {
        persistAnnotations(annotationFile.getAnnotationFile(), annotationFile.getShortDescription());
    }

    /**
     * Persists all annotations into a given file.
     *
     * @param file             File to be saved.
     * @param shortDescription
     * @throws Exception TODO handle better maybe?
     */
    protected void persistAnnotations(File file, String shortDescription) throws Exception {
        if (true || !m_annotations.isEmpty()) {
            // Create the document
            DocumentBuilder builder = (DocumentBuilderFactory.newInstance())
                    .newDocumentBuilder();
            Document doc = builder.newDocument();
            Node root = doc.createElement(XML_TAG_ANNOTATIONS);
            Node node;

            /** unique specimen id */
            Element specimenIDElement = doc
                    .createElement(XML_TAG_SPECIMEN_NAME);
            specimenIDElement.setAttribute(XML_ATTRIB_NAME, m_display
                    .getCurrentSpecimen().getUniqueName());
            root.appendChild(specimenIDElement);

            /** short description */
            node = doc.createElement(XML_TAG_SHORT_DESCRIPTION);
            Element textElem = doc.createElement(XML_TAG_TEXT);
            Text text = doc.createTextNode(shortDescription);
            textElem.appendChild(text);
            node.appendChild(textElem);
            root.appendChild(node);

            Iterator iter = m_annotations.iterator();
            while (iter.hasNext()) {
                Annotation an = (Annotation) iter.next();
                if (!an.isPersistable())
                    continue;
                node = an.persistToXMLNode(doc);
                if (node != null) {
                    root.appendChild(node);
                }
            }
            doc.appendChild(root);

            // Now, persist in into file with a given name
            Transformer transformer = TransformerFactory.newInstance()
                    .newTransformer();
            StreamResult result = new StreamResult(new FileOutputStream(file));
            transformer.transform(new DOMSource(doc), result);
        }
    }

    /**
     * Draws the control.
     *
     * @param g2d Graphics context on which to draw the control.
     */
    public void draw(Graphics2D g2d) {
        Iterator iter = m_annotations.iterator();

        Dimension imageDim = m_display.getCurrentImageSet().getImageSize();
        Point2D.Double stagePos = m_display.getStagePosition();

        while (iter.hasNext()) { // Get next annotation and draw it
            ((Annotation) iter.next()).draw(g2d, imageDim, stagePos);
        }
    }

    /**
     * This method unselects all the contained annotations.
     */
    public void unselectAnnotations() {
        // This method unselects the annotation.
        // This is called during the ptin function to
        // unselect any selected annotation, since
        // selected annotations distort the print out.
        // Look through annotations to see if any of them was selected
        Iterator iter = m_annotations.iterator();

        while (iter.hasNext()) { // Pass the event onto annotations to
            // consume
            ((Annotation) iter.next()).unselectAnnotation();
        }
    }

    /**
     * Handles mousePressed() from MicroscopeView. Starts drawing a new
     * annotation or passes the event to an existing one if arrow tool is
     * selected.
     *
     * @param e MouseEvent
     */
    public void mousePressed(MouseEvent e) {
        // Figure out which tool is selected
        String selTool = getSelectedToolName();

        // If left mouse button pressed, tool is selected
        if (SwingUtilities.isLeftMouseButton(e) && (selTool != null)) {
            // Start drawing a new annotation
            if (!selTool.equals(Constants.NO_ACTIVE_TOOL)) {
                e.consume();
                // Get stage position from MicroscopeView
                Point2D stagePos = m_display.getStagePosition();
                // Compute mouse press position relative to the stage
                m_pressPosition = new Point2D.Double(
                        e.getX() - stagePos.getX(), e.getY() - stagePos.getY());
                // Start drawing - create initial annotation passing it cur
                // imageSet size
                startDrawAnnot(m_pressPosition, m_display.getCurrentImageSet()
                                .getImageSize(), m_curColor, m_curFontName,
                        m_curLineWidth);
            }
            // Or select an existing one
            else {

                // Look through annotations to see if any of them was selected
                Iterator iter = m_annotations.iterator();
                boolean somethingSelected = false;
                if (m_actAnnot != null)
                    somethingSelected = true;
                while (iter.hasNext() && !e.isConsumed()) { // Pass the event
                    // onto annotations
                    // to consume
                    ((Annotation) iter.next()).mousePressed(e);
                }

                // BUG Fix: If a shape is selected and user clicks outside the shape then
                // the base image should not scroll instead the shape should be deselected.
                if (somethingSelected && !e.isConsumed()) {
                    e.consume();
                }
            }
        }
    }

    /**
     * Handles mouseDragged() from MicroscopeView. Continues drawing an
     * annotation or passes the event to an appropriate annotation.
     *
     * @param e MouseEvent
     */
    public void mouseDragged(MouseEvent e) {
        // Figure out which tool is selected
        String selToolName = getSelectedToolName();

        // If left mouse button pressed, and tool is selected
        if (SwingUtilities.isLeftMouseButton(e) && (selToolName != null)) {
            // If tool selected is not an arrow
            if ((m_pressPosition != null) && (!selToolName.equals(Constants.NO_ACTIVE_TOOL))) {
                e.consume();
                // Get stage position from MicroscopeView
                Point2D stagePos = m_display.getStagePosition();
                // Figure out current mouse position
                Point2D curPos = new Point2D.Double(e.getX() - stagePos.getX(),
                        e.getY() - stagePos.getY());
                // Continue drawing started annotation
                continueDrawAnnot(m_pressPosition, curPos, e.isShiftDown() || m_drawSymmetricalShape);
            } else if (selToolName.equals(Constants.NO_ACTIVE_TOOL)) {
                // Look through annotation to see if any of them is being edited
                Iterator iter = m_annotations.iterator();

                while (iter.hasNext() && !e.isConsumed()) { // Pass the event
                    // onto annotations
                    // to consume
                    ((Annotation) iter.next()).mouseDragged(e);
                }
                if (e.isConsumed()) {
                    // an annotation was resized or moved.
                    setDirty(true);

                }
            }
        }
    }

    /**
     * Handles mouseReleased() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseReleased(MouseEvent e) {

        String selToolName = getSelectedToolName();

        // If left mouse button pressed, tool is selected and it's not arrow
        if (selToolName != null) {
            if (SwingUtilities.isLeftMouseButton(e)) {
                if ((m_pressPosition != null) && (!selToolName.equals(Constants.NO_ACTIVE_TOOL))) {
                    e.consume();

                    Point2D stagePos = m_display.getStagePosition();
                    Point2D curPos = new Point2D.Double(e.getX() - stagePos.getX(), e.getY() - stagePos.getY());

                    // If clicked was drawing annotations - ignore the click
                    if (m_pressPosition.equals(curPos)) {
                        m_ignoreClick = true;
                    }
                    // Finish drawing annotation
                    finishDrawAnnot();

                    m_toolsMenu.unselectActiveTool();

                    // Repaint
                    m_display.repaint();
                } else if (selToolName.equals(Constants.NO_ACTIVE_TOOL)) {
                    // Look through annotations to see if any of them was selected
                    Iterator iter = m_annotations.iterator();

                    while (iter.hasNext() && !e.isConsumed()) {
                        // Pass the event onto annotations to consume
                        ((Annotation) iter.next()).mouseReleased(e);
                    }

                    if (e.isConsumed()) {
                        m_display.repaint();
                    }
                }
            } else if (SwingUtilities.isRightMouseButton(e)) {
                // need to show the pop-up menu with related options e.g. "Delete annotation"
                if (selToolName.equals(Constants.NO_ACTIVE_TOOL)) {
                    // Look through annotations to see if any of them was selected
                    Iterator iter = m_annotations.iterator();
                    while (iter.hasNext() && !e.isConsumed()) {
                        // Pass the event onto annotations to consume
                        ((Annotation) iter.next()).mouseReleased(e);
                    }

                    if (e.isConsumed()) {
                        m_display.repaint();
                    }
                }
            }
        }
    }

    /**
     * Handles mouseClicked() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseClicked(MouseEvent e) {
        String selToolName = getSelectedToolName();

        if (m_ignoreClick) {
            m_ignoreClick = false;
            e.consume();
        } else if ((SwingUtilities.isLeftMouseButton(e)) && (selToolName != null)
                && (selToolName.equals(Constants.NO_ACTIVE_TOOL))) {
            // Look through annotation to see if any of them was selected
            Iterator iter = m_annotations.iterator();
            Annotation curAnnot = null;

            while (iter.hasNext() && !e.isConsumed()) { // Pass the event onto
                // annotations to
                // consume
                curAnnot = (Annotation) iter.next();
                curAnnot.mouseClicked(e);
            }
            // If we selected a new annotation as acive - remember it
            setActiveAnnot(e.isConsumed() ? curAnnot : null);
            m_display.repaint();
        }
    }

    /**
     * Handles mouseEntered() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Handles mouseExited() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Handles mouseMoved() from MicroscopeView.
     *
     * @param e MouseEvent
     */
    public void mouseMoved(MouseEvent e) {

        // Give cursor hints about the active annotation
        if (m_actAnnot != null) {
            int square = m_actAnnot.isInShapeSelectionSquare(e.getPoint());

            if (m_actAnnot.textEditable() && m_actAnnot.isInText(e.getPoint())) {
                // Over editable text
                m_display.setCursor(Cursor.TEXT_CURSOR);
                e.consume();
            } else if (m_actAnnot.textSelected()
                    && m_actAnnot.isInText(e.getPoint())) {
                // Over selected text
                m_display.setCursor(Cursor.MOVE_CURSOR);
                e.consume();
            } else if (m_actAnnot.shapeSelected()
                    && (square != Annotation.Square.NONE)) {
                // Over one of the resize squares
                if (square == Annotation.Square.TOP) {
                    m_display.setCursor(Cursor.N_RESIZE_CURSOR);
                } else if (square == Annotation.Square.BOTTOM) {
                    m_display.setCursor(Cursor.S_RESIZE_CURSOR);
                } else if (square == Annotation.Square.LEFT) {
                    m_display.setCursor(Cursor.W_RESIZE_CURSOR);
                } else if (square == Annotation.Square.RIGHT) {
                    m_display.setCursor(Cursor.E_RESIZE_CURSOR);
                } else if (square == Annotation.Square.TOP_LEFT) {
                    m_display.setCursor(Cursor.NW_RESIZE_CURSOR);
                } else if (square == Annotation.Square.TOP_RIGHT) {
                    m_display.setCursor(Cursor.NE_RESIZE_CURSOR);
                } else if (square == Annotation.Square.BOTTOM_LEFT) {
                    m_display.setCursor(Cursor.SW_RESIZE_CURSOR);
                } else if (square == Annotation.Square.BOTTOM_RIGHT) {
                    m_display.setCursor(Cursor.SE_RESIZE_CURSOR);
                }
                e.consume();
            } else if (m_actAnnot.shapeSelected()
                    && m_actAnnot.isInShape(e.getPoint(), false)) { // Over shape
                m_display.setCursor(Cursor.MOVE_CURSOR);
                e.consume();
            } else {
                // Not over any part of active annotation.
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
            }
        } else { // No active annotation
            if (m_display.canResetCursor())
                m_display.setCursor(Cursor.DEFAULT_CURSOR);
        }
    }

    /**
     * Handles keyPressed() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyPressed(KeyEvent e) {
        if (m_actAnnot != null) {
            // First, pass on the event to the active annotation if its text is
            // selected
            m_actAnnot.keyPressed(e);

            if (m_actAnnot.shapeSelected() && !e.isConsumed()) {
                // Otherwise, someone may be trying to delete the annotation
                int keyCode = e.getKeyCode();

                if ((keyCode == KeyEvent.VK_BACK_SPACE)
                        || (keyCode == KeyEvent.VK_DELETE)) {
                    removeAnnotation(m_actAnnot);
                    e.consume();
                }
            }
            if (e.isConsumed()) {
                setDirty(true);
                m_display.repaint();
            }
        }
    }

    /**
     * Indicates whether there are some unsaved annotations.
     *
     * @return true if the current annotations are not saved.
     * @see AnnotationControl#markAnnotationChangesSaved(boolean)
     */
    public boolean containsUnsavedChanges() {
        return m_isDirty;
    }

    /**
     * indicates that the unsavedchanges have been saved.
     *
     * @param saved
     * @see AnnotationControl#containsUnsavedChanges()
     */
    public void markAnnotationChangesSaved(boolean saved) {
        setDirty(!saved);
    }

    /**
     * Handles keyReleased() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyReleased(KeyEvent e) {
    }

    /**
     * Handles keyTyped() from MicroscopeView.
     *
     * @param e KeyEvent
     */
    public void keyTyped(KeyEvent e) {
        // Pass on the event to the active annotation.
        if (m_actAnnot != null) {
            m_actAnnot.keyTyped(e);
            if (e.isConsumed()) {
                setDirty(true);
                m_display.repaint();
            }
        }
    }

    /**
     * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
     */
    public void actionPerformed(ActionEvent e) {
    }

    /**
     * Returns button selected in the ButtonGroup or null is none selected.
     *
     * @return Reference to a selected JToggleButton or null.
     */
    protected String getSelectedToolName() {
        return m_toolsMenu.getSelectedToolName();
/*		JToggleButton selBttn = null;
		JToggleButton curBttn;

		// Look through all buttons and find the one that is selected
		Enumeration enumBttn = m_bttnGroup.getElements();

		while (enumBttn.hasMoreElements() && (selBttn == null)) {
			curBttn = (JToggleButton) enumBttn.nextElement();

			if (curBttn.isSelected()) {
				selBttn = curBttn;
			}
		}

		return selBttn;*/
    }

    /**
     * De-selects the shape/text of the old active annotation and remembers the
     * new one as acive.
     *
     * @param annot new annotation to be active (may be null)
     * @return true iff active annotation changed
     */
    protected boolean setActiveAnnot(Annotation annot) {
        boolean bChange = false;
        if (annot != m_actAnnot) {
            // De-select old annotation
            if (m_actAnnot != null) {
                m_actAnnot.selectShape(false);
                m_actAnnot.selectText(false);
            }
            // Assign new annotation to m_actAnnot
            m_actAnnot = annot;
            bChange = true;
        }
        return bChange;
    }

    /**
     * Creates an appropriate annotation. Should be called by mousePressed().
     *
     * @param curPos    position to start drawing from
     * @param dim       dimension of the image on which the current annotation is being
     *                  drawn
     * @param col       color of the new annotation (both shape and text)
     * @param fontName  font name of the new annotation
     * @param lineWidth line width of the new annotation
     */
    protected void startDrawAnnot(Point2D curPos, Dimension dim, Color col,
                                  String fontName, float lineWidth) {
        // Figure out which tool is selected
        String selToolName = getSelectedToolName();

        // Create a new annotation
        if (selToolName.equals(Constants.ANNOTATION_ROUNDEDRECTANGLE_TOOL)) {
            m_drawnAnnot = new Annotation(m_drawReadonlyAnnotations, m_curSpecimenName,
                    new RoundRectangle2D.Double(curPos.getX(), curPos.getY(),
                            0, 0, 20, 20), col, col, fontName, lineWidth, dim,
                    this);
        } else if (selToolName.equals(Constants.ANNOTATION_RECTANGLE_TOOL)) {
            m_drawnAnnot = new Annotation(m_drawReadonlyAnnotations, m_curSpecimenName,
                    new Rectangle2D.Double(curPos.getX(), curPos.getY(), 0, 0),
                    col, col, fontName, lineWidth, dim, this);
        } else if (selToolName.equals(Constants.ANNOTATION_ELLIPSE_TOOL)) {
            m_drawnAnnot = new Annotation(m_drawReadonlyAnnotations, m_curSpecimenName,
                    new Ellipse2D.Double(curPos.getX(), curPos.getY(), 0, 0),
                    col, col, fontName, lineWidth, dim, this);
        } else if (selToolName.equals(Constants.ANNOTATION_LINE_TOOL)) {
            m_drawnAnnot = new Annotation(m_drawReadonlyAnnotations, m_curSpecimenName, new Line2D.Double(
                    curPos, curPos), col, col, fontName, lineWidth, dim, this);
        } else if ((selToolName.equals(Constants.ANNOTATION_PEN_TOOL)) || (selToolName.equals(Constants.ANNOTATION_TEXT_TOOL))) {
            GeneralPath path = new GeneralPath();
            path.moveTo((float) curPos.getX(), (float) curPos.getY());
            m_drawnAnnot = new Annotation(m_drawReadonlyAnnotations, m_curSpecimenName, path, col, col,
                    fontName, lineWidth, dim, this);
        }
        if (m_drawnAnnot != null) {
            // Deselect current active annotation
            setActiveAnnot(null);
            // Add new annotation to collection
            addAnnotation(m_drawnAnnot);
        }
    }

    /**
     * Thie mathos provides the implementation of the signum function. This
     * method takes in a double value and returns 0f is the value is 0, -1.0f if
     * the value if less than zero and +1.0f if the value is greater than zero.
     * This method is similar to Math.signum() (which is included in jdk1.5).
     * However we did not want to use very specific jdk 1.5 code to be backward
     * compatible. So this method is used so that jdk 1.5 specific Math.signum
     * method could be avoided.
     *
     * @param input double value whose signum is to be returned.
     * @return float 0 if input is 0, -1.0 if input is less than 0 and 1.0 if
     * input is greater than 0.
     */

    private float getSignum(double input) {
        if (input < 0)
            return -1.0f;
        else if (input > 0)
            return +1.0f;
        else
            return 0.0f;
    }

    /**
     * Continuous drawing a started annotation. Assumes startDrawAnnot() has
     * been called and thus m_drawnAnnot is not null. Should be called by
     * mouseDragged().
     *
     * @param startPos position where drawing started
     * @param curPos   current position of the mouse
     * @param keepEven keeps annotation width and height the same. Has no effect if the
     *                 pen tool is selected.
     */
    protected void continueDrawAnnot(Point2D startPos, Point2D curPos,
                                     boolean keepEven) {
        if (m_drawnAnnot != null) {
            // Figure out which tool is selected
            String selToolName = getSelectedToolName();
            Point2D endPos = (Point2D) curPos.clone();

            // If pen tool is not selected and want to keep shape even
            if (keepEven && (!selToolName.equals(Constants.ANNOTATION_PEN_TOOL))) {
                double maxDelta = Math.max(Math.abs(curPos.getX()
                        - startPos.getX()), Math.abs(curPos.getY()
                        - startPos.getY()));
                endPos.setLocation(
                        startPos.getX()
                                + getSignum(curPos.getX() - startPos.getX())
                                * maxDelta, startPos.getY()
                                + getSignum(curPos.getY() - startPos.getY())
                                * maxDelta);
            }

            double width = Math.abs(startPos.getX() - endPos.getX());
            double height = Math.abs(startPos.getY() - endPos.getY());
            double x = Math.min(startPos.getX(), endPos.getX());
            double y = Math.min(startPos.getY(), endPos.getY());

            if (selToolName.equals(Constants.ANNOTATION_ROUNDEDRECTANGLE_TOOL)) {
                ((RoundRectangle2D.Double) m_drawnAnnot.getOrigShape())
                        .setFrame(x, y, width, height);
            } else if (selToolName.equals(Constants.ANNOTATION_RECTANGLE_TOOL)) {
                ((Rectangle2D.Double) m_drawnAnnot.getOrigShape()).setFrame(x,
                        y, width, height);
            } else if (selToolName.equals(Constants.ANNOTATION_ELLIPSE_TOOL)) {
                ((Ellipse2D.Double) m_drawnAnnot.getOrigShape()).setFrame(x, y,
                        width, height);
            } else if (selToolName.equals(Constants.ANNOTATION_LINE_TOOL)) {
                if (keepEven) {
                    /**
                     * The existing logic will only draw lines at 45, 135, 225,
                     * 315 degree angles. we also need to draw lines to 0, 90,
                     * 180, 270 degrees. Since the above logic is used for
                     * circles, squares and lines ( as per this logic the lines
                     * are drawn as diagonal of the squares and hence only 45,
                     * 135, 225, 315 degrees can be drawn) We shall not change
                     * that logic; instead we shall further qualify this logic
                     * for the 0,90,180,270 degree lines. We divide each
                     * quadrant into two parts one below 45 degrees and one
                     * above. This effectively divides the X-Y plane into 8
                     * quadrants. The existing logic can border four of these
                     * quadrants we will provide the bordering for the rest of
                     * the four.
                     */

                    double deltaX = (curPos.getX() - endPos.getX());
                    double deltaY = (curPos.getY() - endPos.getY());
                    double deltaXX = (curPos.getX() - startPos.getX());
                    double deltaYY = (curPos.getY() - startPos.getY());

                    if (deltaXX > 0) {
                        if (deltaYY < 0 && deltaY > 0) {
                            // we are in the quadrant between 0 and 45 degrees.
                            // We shall draw the line along 0 degrees.
                            endPos.setLocation(curPos.getX(), startPos.getY());
                        } else if (deltaYY > 0 && deltaX < 0) {
                            // we are in the quadrant between 270 and 360
                            // degrees. We shall draw the line along 270
                            // degrees.
                            endPos.setLocation(startPos.getX(), curPos.getY());
                        }
                    } else {
                        if (deltaYY > 0 && deltaY < 0) {
                            // we are in the quadrant between 180 and 270
                            // degrees. We shall draw the line along 180
                            // degrees.
                            endPos.setLocation(curPos.getX(), startPos.getY());
                        } else if (deltaYY < 0 && deltaX > 0) {
                            // we are in the quadrant between 90 and 135
                            // degrees. We shall draw the line along 90 degrees.
                            endPos.setLocation(startPos.getX(), curPos.getY());
                        }
                    }
                }
                // finally we actually do the drawing.
                ((Line2D.Double) m_drawnAnnot.getOrigShape()).setLine(startPos,
                        endPos);
            } else if (selToolName.equals(Constants.ANNOTATION_PEN_TOOL)) {
                ((GeneralPath) m_drawnAnnot.getOrigShape()).lineTo(
                        (float) endPos.getX(), (float) endPos.getY());
            }
        }
    }

    /**
     * Cleans up when the drawing is done. Should be called by mouseReleased();
     */
    public void finishDrawAnnot() {
        m_drawSymmetricalShape = false;
        m_drawReadonlyAnnotations = false;
        // Set active selection to the annotation that was just drawn
        setActiveAnnot(m_drawnAnnot);

        // And reset misc variables
        if (m_drawnAnnot.isPersistable())
            setDirty(true);
        m_drawnAnnot = null;
        m_pressPosition = null;
    }

    /**
     * Removes an annotation from the collection and repaints.
     *
     * @param a Annotation to be removed.
     */
    public void removeAnnotation(Annotation a) {
        if (a != null) {
            boolean found = false;

            Iterator iter = m_annotations.iterator();
            while (iter.hasNext() && !found) {
                found = (a == (Annotation) iter.next());
            }

            if (found) {
                m_annotations.remove(a);
                m_display.repaint();
                if (a.isPersistable())
                    setDirty(true);
            }
            m_actAnnot = null;
            // if the user was moving the annotation and then deletes it
            // the cursor
            // would remain 'resize cursor' so we reset the cursor to
            // default.
            m_display.setCursor(Cursor.DEFAULT_CURSOR);
        }
    }

    public PopupMenuActionListener getPopupMenuActionListener() {
        return m_popuMenuActionListener;
    }

    /**
     * This method returns the current color.
     *
     * @return the selected color
     */
    public Color getCurrentColor() {
        return m_curColor;
    }

    /**
     * This method sets the current color.
     */
    public void setCurrentColor(Color newColor) {
        m_curColor = newColor;
    }


    /**
     * This method returns the current font name.
     *
     * @return The selected font name.
     */
    public String getCurrentFontName() {
        return m_curFontName;
    }

    /**
     * This method returns the current line width.
     *
     * @return the current line width.
     */
    public float getCurrentLineWidth() {
        return m_curLineWidth;
    }

    /**
     * This method sets the current line width.
     */
    public void setCurrentLineWidth(float newWidth) {
        m_curLineWidth = newWidth;
    }

    ;

    /**
     * Ads an annotation to the collection and repaints.
     *
     * @param a Annotation to be added to the collection.
     */
    public void addAnnotation(Annotation a) {
        if (a != null) {
            m_annotations.add(a);
            m_display.repaint();
        }
    }

    public String selectTool(String toolName) {
        String selectedOption = m_toolsMenu.getSelectedOption().getName();
        m_toolsMenu.setSelected(toolName);
        return selectedOption;
    }

    public boolean isRectangleButtonPressed() {
        return m_toolsMenu.getSelectedToolName().equals(Constants.ANNOTATION_RECTANGLE_TOOL);
    }

    /**
     * This method saves the annotations. This is invoked in response to user's pressing of
     * the Save button in the Annotation side Panel. This method determines if the
     * "Save" or the "Save As..." logic should be invoked and takes the appropriate action.
     */
    private void performSave() {
        Object selection = m_annotationFileComboBox.getSelectedItem();
        try {
            if (selection == null ||
                    selection.toString().equals(AnnotationFileComboBox.SELECT_ANNOTATION) ||
                    selection.toString().equals(AnnotationFileComboBox.NONE_AVAILABLE)) {
                performSaveAs();
            } else {
                SpecimenAnnotationFile annotationFile = (SpecimenAnnotationFile) selection;
                persistAnnotations(annotationFile);
                markAnnotationChangesSaved(true);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.ErrorSaving(Configuration.getApplicationLanguage())
                    +Constants.SPACE
                    +Locale.AnnotationFile(Configuration.getApplicationLanguage()).toLowerCase(),
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorSaving(Locale.LANGUAGE.EN)+Constants.SPACE+Locale.AnnotationFile(Locale.LANGUAGE.EN).toLowerCase());
            log.error(ex.getMessage());
        }
    }

    /**
     * This method saves the annotations with a new short description. This is invoked in response
     * to user's pressing of the "Save As..." button in the Annotation side Panel
     */
    public void performSaveAs() {
        try {
            File file = new File(m_display.getMicroscope().getConfig().getAnnotDir() + Constants.PATH_SEPARATOR + System.currentTimeMillis() + ".xml");
            String shortDescription = new TranslatedOptionPane().input(this,
                    Locale.PleaseEnter(Configuration.getApplicationLanguage())+Constants.SPACE+Locale.ShortDescription(Configuration.getApplicationLanguage()).toLowerCase(),
                    Locale.SaveAs(Configuration.getApplicationLanguage()) );

            if (shortDescription == null) {
                // cancel pressed.
                return;
            } else if (shortDescription.trim().length() == 0) {
                JOptionPane.showMessageDialog(m_microscope,
                        Locale.InvalidShortDescription(Configuration.getApplicationLanguage())
                        +". "
                        +Locale.Annotation(Configuration.getApplicationLanguage()).toLowerCase()
                        +Constants.SPACE
                        +Locale.NotSaved(Configuration.getApplicationLanguage()).toLowerCase(),
                        Locale.InvalidShortDescription(Configuration.getApplicationLanguage()),
                        JOptionPane.INFORMATION_MESSAGE);
                return;
            }
            if (shortDescription.equals(AnnotationFileComboBox.SELECT_ANNOTATION) ||
                    shortDescription.equals(AnnotationFileComboBox.NONE_AVAILABLE)) {
                // making sure user does not enter a short description that collides with
                // our special 'keyword' else funny things may happen.
                shortDescription += " ";
            }
            persistAnnotations(file, shortDescription);
            markAnnotationChangesSaved(true);
            SpecimenAnnotationFile annotationFile = new SpecimenAnnotationFile(file);
            if (m_annotationFileComboBox.isNoneAvailable()) {
                m_annotationFileComboBox.setEnabled(true);
                m_annotationFileComboBox.removeItem(AnnotationFileComboBox.NONE_AVAILABLE);
            }
            m_annotationFileComboBox.addItem(annotationFile);
            m_annotationFileComboBox.setSelectedItem(annotationFile);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.ErrorSaving(Configuration.getApplicationLanguage())
                    +Constants.SPACE
                    +Locale.AnnotationFile(Configuration.getApplicationLanguage()).toLowerCase(),
                    Locale.Error(Configuration.getApplicationLanguage()).toLowerCase(),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorSaving(Locale.LANGUAGE.EN)
                            +Constants.SPACE
                            +Locale.AnnotationFile(Locale.LANGUAGE.EN).toLowerCase());
            log.error(ex.getMessage());
        }
    }

    /**
     * This method closes any open annotation giving the user a chance to
     * save any unsaved changes before closing.
     *
     * @return true if the close was performed, false if it was cancelled.
     */
    public boolean closeAnnotation() {
        if (containsUnsavedChanges()) {
            int reply = new TranslatedOptionPane().yesNoCancel(m_microscope,
                    Locale.ConfirmSavingAnnotation(Configuration.getApplicationLanguage()) + "?",
                    Locale.Save(Configuration.getApplicationLanguage())+Constants.SPACE+
                            Locale.Annotation(Configuration.getApplicationLanguage()).toLowerCase());

            if (reply == JOptionPane.CANCEL_OPTION) {
                return false;
            } else if (reply == JOptionPane.NO_OPTION) {
                markAnnotationChangesSaved(true);
            } else {
                // user wishes to save the changes
                // a valid annotation was already selected
                performSave();
            }
        }
        clearAnnotations();
        m_annotationFileComboBox.unselectItem(false);
        m_currentlyLoadedAnnotations = null;
        return true;
    }

    public Annotation getDrawnAnnotation() {
        return m_drawnAnnot;
    }

    public void shouldDrawSymmetricalShape(boolean drawSymmetricalShape) {
        this.m_drawSymmetricalShape = drawSymmetricalShape;
    }

    public void shouldDrawReadonlyAnnotation(boolean isReadonly) {
        m_drawReadonlyAnnotations = isReadonly;
    }

    public boolean drawSymmetricalShape() {
        return m_drawSymmetricalShape;
    }

    public Color getSelectedColor() {
        return m_curColor;
    }

    public void toolComboBoxSelectionMade(String name) {
        // inactivate EDS and AFM tools as they may interfere with us.
        for (int i = 0; i < m_listeners.size(); i++) {
            ((AnnotationControlChangeListener) m_listeners.get(i)).unselectAllTools();
        }
    }

    public void linwWidthComboBoxSelectionChanged(float f) {
        m_curLineWidth = f;
        if ((m_actAnnot != null)
                && m_actAnnot.setLineWidth(m_curLineWidth)) {
            m_display.repaint();
            setDirty(true);
        }
    }

    public void colorComboBoxSelectionChanged(Color selectedColor) {
        m_curColor = selectedColor;
        if ((m_actAnnot != null)
                && m_actAnnot.setColor(m_curColor)) {
            m_display.repaint();
            setDirty(true);
        }
    }

    /**
     * This method imports the selected file. This entails copying the
     * source file to annotation directory, then loading the imported file
     * and finally setting it as the topmost option in thr annotation file dropdow
     */
    public void importAnnotations() {
        if (!closeAnnotation())
            return;//cancelled.

        String defaultDir = System.getProperty("user.home");
        if (defaultDir == null)
            defaultDir = ".";
        JFileChooser fc = new JFileChooser(defaultDir);
        fc.addChoosableFileFilter(new AnnotationXMLFileFilter(m_curSpecimenName));
        fc.showOpenDialog(m_microscope);
        File file = fc.getSelectedFile();
        try {
            if (file != null) {
                File tempFile = new File(m_display.getMicroscope().getConfig().getAnnotDir() +
                        Constants.PATH_SEPARATOR + file.getName());
                if (tempFile.getCanonicalPath().equals(file.getCanonicalPath())) {
                    // user is trying to import a file from the annotation directory.This would result
                    //in multiple copies of the same file in annotation directory. We shall
                    // not import this file but will only load the file.
                    int count = m_annotationFileComboBox.getItemCount();
                    for (int i = 0; i < count; i++) {
                        Object itemAt = m_annotationFileComboBox.getItemAt(i);
                        if (itemAt instanceof SpecimenAnnotationFile) {
                            SpecimenAnnotationFile annot = (SpecimenAnnotationFile) itemAt;
                            if (annot.getAnnotationFile().getCanonicalPath().equals(file.getCanonicalPath())) {
                                m_annotationFileComboBox.setSelectedIndex(i);
                                return;
                            }
                        }
                    }
                }
                // User may have selected an invalid specimen file. This will happen if he changes the file filter
                // so that now all files are available and then he can choose whatever pleases him.
                try {
                    if (!m_curSpecimenName.equals(new SpecimenAnnotationFile(file).getSpecimenID())) {
                        // invalid file. Shouldn't allow import.
                        JOptionPane.showMessageDialog(m_microscope,
                                Locale.Error(Configuration.getApplicationLanguage())
                                +Constants.SPACE
                                +Locale.Importing(Configuration.getApplicationLanguage()).toLowerCase()
                                +Constants.SPACE
                                + file.getName()
                                +". "
                                +Locale.InvalidAnnotationFile(Configuration.getApplicationLanguage()),
                                Locale.Error(Configuration.getApplicationLanguage()),
                                JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(m_microscope,
                            Locale.Error(Configuration.getApplicationLanguage())
                            +Constants.SPACE
                            +Locale.Importing(Configuration.getApplicationLanguage()).toLowerCase()
                            +Constants.SPACE
                            +file.getName()
                            +". "
                            +Locale.InvalidAnnotationFile(Configuration.getApplicationLanguage()).toLowerCase()
                            +".",
                            Locale.Error(Configuration.getApplicationLanguage()),
                            JOptionPane.ERROR_MESSAGE);
                    log.error(Locale.Error(Locale.LANGUAGE.EN)
                            +Constants.SPACE
                            +Locale.Importing(Locale.LANGUAGE.EN).toLowerCase()
                            +Constants.SPACE
                            +file.getName()
                            +". "
                            +Locale.InvalidAnnotationFile(Locale.LANGUAGE.EN).toLowerCase()
                            +".");
                    log.error(ex.getMessage());
                    return;
                }
                File importedFile = copyToAnnotationDirectory(file);
                SpecimenAnnotationFile annotationFile = new SpecimenAnnotationFile(importedFile);
                AnnotationControl.this.unPersistAnnotations(annotationFile);
                if (m_annotationFileComboBox.getItemAt(0).toString().equals(AnnotationFileComboBox.NONE_AVAILABLE)) {
                    m_annotationFileComboBox.removeItem(AnnotationFileComboBox.NONE_AVAILABLE);
                    m_annotationFileComboBox.addItem(annotationFile);
                    m_annotationFileComboBox.setEnabled(true);
                } else {
                    // newly imported anotation will be selected and loaded automatically.
                    m_annotationFileComboBox.insertItemAt(annotationFile, 1);
                    m_annotationFileComboBox.setSelectedIndex(1);
                    m_annotationFileComboBox.setEnabled(true);
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.Error(Configuration.getApplicationLanguage())
                    +Constants.SPACE
                    +Locale.Opening(Configuration.getApplicationLanguage()).toLowerCase()
                    +Constants.SPACE
                    +file.getName(),
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.Error(Locale.LANGUAGE.EN)
                    +Constants.SPACE
                    +Locale.Opening(Locale.LANGUAGE.EN).toLowerCase()
                    +Constants.SPACE
                    +file.getName());
            log.error(ex.getMessage());
        }
    }

    /**
     * This method copies the given file to the annotation directory and returns
     * the newly copied file.
     *
     * @param sourceFile
     * @return
     */
    private File copyToAnnotationDirectory(File sourceFile) {

        FileInputStream from = null;  // Stream to read from source
        FileOutputStream to = null;   // Stream to write to destination
        String destinationFileName = sourceFile.getName();
        File destinationFile = new File(m_display.getMicroscope().getConfig().getAnnotDir() + Constants.PATH_SEPARATOR + destinationFileName);
        while (destinationFile.exists()) {
            // making sure there is no name conflict.
            destinationFileName = "Copy of " + destinationFileName;
            destinationFile = new File(m_display.getMicroscope().getConfig().getAnnotDir() + Constants.PATH_SEPARATOR + destinationFileName);
        }
        try {
            from = new FileInputStream(sourceFile);
            to = new FileOutputStream(destinationFile);
            byte[] buffer = new byte[4096];
            int bytes_read;
            while ((bytes_read = from.read(buffer)) != -1) // Read bytes until EOF
                to.write(buffer, 0, bytes_read);
            return destinationFile;
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.Error(Configuration.getApplicationLanguage())
                    +Constants.SPACE
                    +Locale.Importing(Configuration.getApplicationLanguage()).toLowerCase()
                    +Constants.SPACE
                    +sourceFile.getPath()
                    +Constants.SPACE
                    +Locale.To(Configuration.getApplicationLanguage()).toLowerCase()
                    +Constants.SPACE
                    +destinationFile.getPath(),
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.Error(Locale.LANGUAGE.EN)
                    +Constants.SPACE
                    +Locale.Importing(Locale.LANGUAGE.EN).toLowerCase()
                    +Constants.SPACE
                    +sourceFile.getPath()
                    +Constants.SPACE
                    +Locale.To(Locale.LANGUAGE.EN).toLowerCase()
                    +Constants.SPACE
                    +destinationFile.getPath());
            log.error(ex.getMessage());
        } finally {
            if (from != null) try {
                from.close();
            } catch (IOException e) {
                log.error("Unable to close file input stream for file " + sourceFile.getPath() + sourceFile.getName());
                log.error(e.getMessage());
            }
            if (to != null) try {
                to.close();
            } catch (IOException e) {
                log.error("Unable to close file output stream for file " + destinationFile.getPath() + destinationFile.getName());
                log.error(e.getMessage());
            }
        }
        return null;
    }

    /**
     * This method allows the user to export the currently loaded annotation
     * to an xml file.
     */
    public void exportAnnotations() {
        String shortDescription = "";
        if (!containsAnyPersistableAnnotation()) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.NoAnnotationsToExport(Configuration.getApplicationLanguage())
                    +". "
                    +Locale.LoadOrDrawAnnotations(Configuration.getApplicationLanguage())
                    +".",
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
        }
        String selectedOption = m_annotationFileComboBox.getSelectedItem().toString();
        boolean saveAsRequired = (selectedOption.equals(AnnotationFileComboBox.SELECT_ANNOTATION) ||
                selectedOption.equals(AnnotationFileComboBox.NONE_AVAILABLE));
        if (!saveAsRequired)
            shortDescription = ((SpecimenAnnotationFile) m_annotationFileComboBox.getSelectedItem()).getShortDescription();
        shortDescription = new TranslatedOptionPane().input (this,
                Locale.PleaseEnter(Configuration.getApplicationLanguage())+
                Constants.SPACE+
                Locale.ShortDescription(Configuration.getApplicationLanguage()).toLowerCase(),
                shortDescription );
        if (shortDescription == null) {
            return;
        }

        String defaultDir = System.getProperty("user.home");
        if (defaultDir == null)
            defaultDir = ".";
        JFileChooser fc = new JFileChooser(defaultDir);
        fc.addChoosableFileFilter(new AnnotationXMLFileFilter(null));
        fc.showSaveDialog(m_microscope);
        File file = fc.getSelectedFile();
        try {
            if (file != null) {
                if (file.getName().indexOf(".xml") != file.getName()
                        .length() - 4) {
                    String str = file.getPath();
                    str = str + ".xml";
                    file = new File(str);
                }
                AnnotationControl.this.persistAnnotations(file, shortDescription);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(m_microscope,
                    Locale.ErrorSaving(Configuration.getApplicationLanguage())
                    +Constants.SPACE
                    +file.getName(),
                    Locale.Error(Configuration.getApplicationLanguage()),
                    JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorSaving(Locale.LANGUAGE.EN)
                    +Constants.SPACE
                    +file.getName());
            log.error(ex.getMessage());
        }
    }

    /**
     * returns true if there is atleast one persistable annotation false otherwise.
     *
     * @return
     */
    private boolean containsAnyPersistableAnnotation() {
        if (m_annotations == null || m_annotations.size() == 0)
            return false;
        for (int i = 0; i < m_annotations.size(); i++) {
            if (((Annotation) m_annotations.get(i)).isPersistable())
                return true;
        }
        return false;
    }

    /**
     * This method deletes the currently loaded annotation file both from the
     * harddisk and from the drop down.
     */
    public void deleteAnnotations() {
        Object selectedItem = m_annotationFileComboBox.getSelectedItem();
        if (!selectedItem.toString().equals(AnnotationFileComboBox.SELECT_ANNOTATION)) {

            SpecimenAnnotationFile selectedAnnotation = (SpecimenAnnotationFile) selectedItem;
            if (!selectedAnnotation.getAnnotationFile().canWrite()) {
                JOptionPane.showMessageDialog(m_microscope,
                        Locale.Error(Configuration.getApplicationLanguage())
                        +Constants.SPACE
                        +Locale.Deleting(Configuration.getApplicationLanguage()).toLowerCase()
                        +Constants.SPACE
                        +selectedAnnotation.getAnnotationFile().getName()
                        +". "
                        +Locale.TheAnnotationFile(Configuration.getApplicationLanguage())
                        +Constants.SPACE
                        +Locale.IsWriteProtected(Configuration.getApplicationLanguage()).toLowerCase(),
                        Locale.Error(Configuration.getApplicationLanguage()),
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            m_annotationFileComboBox.unselectItem(true);
            m_annotationFileComboBox.removeItem(selectedAnnotation);

            clearAnnotations();
            boolean isDeleted = selectedAnnotation.getAnnotationFile().delete();

            if (!isDeleted) {
                selectedAnnotation.getAnnotationFile().deleteOnExit();
            }
            setDirty(false);
        }
    }

    private void setDirty(boolean makeDirty) {
        if (m_isDirty) {
            // was dirty before
            if (!makeDirty) {
                // marking it clean now i.e. get rid of * at the end of the title.
                String title = m_display.getMicroscope().m_coreApplication.getTitle();
                if (title.charAt(title.length() - 1) == '*') {
                    title = title.substring(0, title.length() - 1);
                    m_display.getMicroscope().m_coreApplication.setTitle(title);
                }
                // else title must already contain a *.
            }
        } else {
            // was clean before and marking it dirty now.
            if (makeDirty) {
                String title = m_display.getMicroscope().m_coreApplication.getTitle();
                m_display.getMicroscope().m_coreApplication.setTitle(title + "*");
            }
        }
        this.m_isDirty = makeDirty;
    }

    class PopupMenuActionListener implements java.awt.event.ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (AnnotationControl.this.m_actAnnot.shapeSelected()) {
                // Otherwise, someone may be trying to delete the annotation
                removeAnnotation(m_actAnnot);
            }
        }
    }

    /**
     * Handles the Save, Save As..., Close operations.
     *
     * @author Kashif Manzoor
     */
    class FileOperationListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            String actionCommand = e.getActionCommand();
            if (actionCommand.equals("Save")) {
                performSave();
            } else if (actionCommand.equals("Save As")) {
                performSaveAs();
            } else if (actionCommand.equals("Close")) {
                closeAnnotation();
            }
        }
    }
}