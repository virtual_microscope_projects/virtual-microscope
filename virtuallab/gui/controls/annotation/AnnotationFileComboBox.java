/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListCellRenderer;

import virtuallab.Configuration;
import virtuallab.Log;
import virtuallab.util.Constants;
import virtuallab.util.Locale;
import virtuallab.util.TranslatedOptionPane;

/**
 * This Class implements the Annotations Drop down, from which the user can select
 * an annotation file to load.
 *
 */
public class AnnotationFileComboBox extends JComboBox implements ListCellRenderer, ItemListener {

    private static final Log log = new Log(AnnotationFileComboBox.class.getName());

    public final static String SELECT_ANNOTATION = " Select Annotation";
    public final static String NONE_AVAILABLE = " None Available";
    private static final long serialVersionUID = 3547136592531462636L;
    /**
     * Reference to the annotation manager that is incharge of all the annotations.
     */
    private AnnotationControl m_annotationControl;
    /**
     * The files that will form the contents of the drop down.
     */
    private SpecimenAnnotationFile[] m_specimenAnnotations;

    public AnnotationFileComboBox(AnnotationControl annotationControl, String annotationDirectory, String specimenID) {
        setToolTipText(Locale.LoadSpecimenAnnotations(Configuration.getApplicationLanguage()));
        m_annotationControl = annotationControl;
        loadSpecimenSpecificAnnotations(annotationDirectory, specimenID);
        addItemListener(this);
        setRenderer(this);
        m_annotationControl.m_display.getMicroscope().m_coreApplication.enableAnnotationDeleteItem(false);
    }

    /**
     * Gets called everytime a combobox item needs to be rendered.
     */
    public Component getListCellRendererComponent(JList list, Object itemValue, int index, boolean isSelected, boolean cellHasFocus) {
        if (itemValue instanceof String) {
            // rendering the SELECT_ANNOTATION or NONE_AVAILABLE options.
            return new JLabel(itemValue.toString());
        } else {
            SpecimenAnnotationFile annotationFile = (SpecimenAnnotationFile) (itemValue);
            String text = annotationFile.getShortDescription();
            if (text.length() > 25) {
                text = text.substring(0, 25);
                text += "...";
            }
            JLabel label = new JLabel(" " + text);
//			label.setSize(new Dimension(20,10));
            label.setToolTipText(annotationFile.getLongDescriptionAsHtml());
            if (isSelected) {
                label.setForeground(Color.BLUE);
            }
            return label;
        }
    }

    /**
     * This method loads all the annotation xml files that are specific to the specified
     * specimen.
     */
    private void loadSpecimenSpecificAnnotations(String annotationDirectory, String specimenID) {
        File dir = new File(annotationDirectory);

        File[] files = dir.listFiles(new AnnotationDropDownFilter(specimenID));
        m_specimenAnnotations = new SpecimenAnnotationFile[files.length];
        if (m_specimenAnnotations.length > 0)
            addItem(SELECT_ANNOTATION);
        else {
            addItem(NONE_AVAILABLE);
            setEnabled(false);
            return;
        }
        for (int i = 0; i < files.length; i++) {
            SpecimenAnnotationFile annotationFile = new SpecimenAnnotationFile(files[i]);
            m_specimenAnnotations[i] = annotationFile;
            addItem(annotationFile);
        }
    }

    public SpecimenAnnotationFile[] getSpecimenAnnotationFiles() {
        return m_specimenAnnotations;
    }

    /**
     * Gets called when the user changes the selection in the combo box.
     */
    public void itemStateChanged(ItemEvent e) {
        int i = getSelectedIndex();

        if (getSelectedItem() == null)
            return;
        if (!getSelectedItem().toString().equals(SELECT_ANNOTATION) &&
                !getSelectedItem().toString().equals(NONE_AVAILABLE)) {
            //file->Annotation->Delete should be enabled.
            m_annotationControl.m_display.getMicroscope().m_coreApplication.enableAnnotationDeleteItem(true);

            if (m_annotationControl.containsUnsavedChanges()) {
                int reply = new TranslatedOptionPane().yesNo(this,
                        Locale.ConfirmSavingAnnotation(Configuration.getApplicationLanguage()) +"?",
                        Locale.Save(Configuration.getApplicationLanguage())+Constants.SPACE+Locale.Annotation(Configuration.getApplicationLanguage()).toLowerCase());
                if (reply == JOptionPane.NO_OPTION) {
                    m_annotationControl.markAnnotationChangesSaved(true);
                } else {
                    // user wishes to save the changes
                    // a valid annotation was already selected
                    try {
                        SpecimenAnnotationFile currentlyLoadedAnnotationFile = m_annotationControl.getCurrentlyLoadedAnnotationFile();
                        if (currentlyLoadedAnnotationFile == null) {
                            m_annotationControl.performSaveAs();
                        } else
                            m_annotationControl.persistAnnotations(currentlyLoadedAnnotationFile);
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(this,
                                Locale.ErrorSaving(Configuration.getApplicationLanguage())
                                + Constants.SPACE
                                +Locale.AnnotationFile(Configuration.getApplicationLanguage()).toLowerCase(),
                                Locale.Error(Configuration.getApplicationLanguage()),
                                JOptionPane.ERROR_MESSAGE);
                        log.error(Locale.ErrorSaving(Locale.LANGUAGE.EN)
                                + Constants.SPACE
                                +Locale.AnnotationFile(Locale.LANGUAGE.EN).toLowerCase());
                        log.error(ex.getMessage());
                    }
                    m_annotationControl.markAnnotationChangesSaved(true);
                }
            }
            // remove the 'select annotation' option.
            if (getItemAt(0).toString().equals(SELECT_ANNOTATION)) {
                removeItemAt(0);
                i--;
            }
            // load annotations corresponding to the selected item.
            SpecimenAnnotationFile annotationFile = ((SpecimenAnnotationFile) getItemAt(i));
            try {
                m_annotationControl.unPersistAnnotations(annotationFile);
            } catch (Exception exception) {
                JOptionPane.showMessageDialog(null,
                        Locale.AnnotationsFrom(Configuration.getApplicationLanguage())
                        +Constants.SPACE
                        + annotationFile.getAnnotationFilePath()
                        +Constants.SPACE
                        +Locale.CouldntBeLoaded(Configuration.getApplicationLanguage()).toLowerCase(),
                        Locale.Error(Configuration.getApplicationLanguage()),
                        JOptionPane.ERROR_MESSAGE);
                log.error(Locale.AnnotationsFrom(Locale.LANGUAGE.EN)
                        +Constants.SPACE
                        + annotationFile.getAnnotationFilePath()
                        +Constants.SPACE
                        +Locale.CouldntBeLoaded(Locale.LANGUAGE.EN).toLowerCase());
                log.error(exception.getMessage());
            }
        } else {
            //file->Annotation->Delete should be disabled.
            m_annotationControl.m_display.getMicroscope().m_coreApplication.enableAnnotationDeleteItem(false);
        }
    }

    /**
     * This method unselects any selected item and makes the string
     * " Select Annotation" or " None Available" as the current selection.
     *
     * @param willDeleteLater if true this indicates that the item which is being unselected will
     *                        be removed soon after. This information will help us to determine if we should show
     *                        NONE_AVAILABLE or SELECT_ANNOTATION in the dropdown.
     */
    public void unselectItem(boolean willDeleteLater) {
        if (getItemAt(0).toString().equals(NONE_AVAILABLE) && !willDeleteLater) {
            // e.g. case when the user is importing a file when there is
            // no annotations available.

        } else if (getItemAt(0).toString().equals(SELECT_ANNOTATION)) {
            if (getItemCount() == 1) {
                // no more annotations available.
                removeItem(SELECT_ANNOTATION);
                addItem(NONE_AVAILABLE);
                setEnabled(false);
            } else {
                setSelectedIndex(0);
                setEnabled(true);
            }
        } else if (willDeleteLater && getItemCount() == 1) {
            addItem(AnnotationFileComboBox.NONE_AVAILABLE);
            setSelectedIndex(0);
            setEnabled(false);
        }
        // safeguard against the case when the user pressed 'close' when
        // there was no annotation loaded.
        else if (!getItemAt(0).toString().equals(NONE_AVAILABLE) &&
                !getItemAt(0).toString().equals(SELECT_ANNOTATION)) {
            insertItemAt(AnnotationFileComboBox.SELECT_ANNOTATION, 0);
            setSelectedIndex(0);
            setEnabled(true);
        }
    }

    /**
     * Returns if there isn't any annotation available, false otherwise
     *
     * @return
     */
    public boolean isNoneAvailable() {
        return (getItemCount() == 1 && getItemAt(0).toString().equals(NONE_AVAILABLE));
    }
}
