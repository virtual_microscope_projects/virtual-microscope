/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.*;

import virtuallab.Log;
import virtuallab.util.ColorChooserDialog;
import virtuallab.util.Locale;


/**
 * This class implements the annotations color dropdown functionality.
 *
 */
public class ColorComboBox extends ToggleButtonsComboBox {

    private static final Log log = new Log(Annotation.class.getName());

    private static final long serialVersionUID = -3425137764219391086L;
    private static final String ADD_CUSTOM = "ADD_CUSTOM";
    private boolean m_customColorAdditioninProgress = false;
    private boolean m_cancelPressed = false;

    public ColorComboBox(AnnotationControl annotationControl) {
        super(annotationControl);
        setToolTipText(Locale.SetAnnotationColor(virtuallab.Configuration.getApplicationLanguage()));
    }

    protected void createAndPopulateComboBoxItems() {
        Color white = new Color(255, 255, 255);
        Color red = new Color(255, 0, 0);
        Color green = new Color(0, 255, 0);
        Color blue = new Color(0, 0, 255);
        Color black = new Color(0, 0, 0);
        Color yellow = new Color(255, 255, 0);
        Color pink = new Color(255, 128, 255);
        Color purple = new Color(128, 0, 128);
        Color brown = new Color(128, 64, 0);
        m_comboBoxItems.put("255,255,255", white);
        m_comboBoxItems.put("255,0,0", red);
        m_comboBoxItems.put("0,255,0", green);
        m_comboBoxItems.put("0,0,255", blue);
        m_comboBoxItems.put("0,0,0", black);
        m_comboBoxItems.put("255,255,0", yellow);
        m_comboBoxItems.put("255,128,255", pink);
        m_comboBoxItems.put("128,0,128", purple);
        m_comboBoxItems.put("128,64,0", brown);
        m_comboBoxItems.put(ADD_CUSTOM, ADD_CUSTOM);
    }

    public void actionPerformed(ActionEvent e) {
        if (m_customColorAdditioninProgress)
            return;
        Object selectedObject = ((JComboBox) e.getSource()).getSelectedItem();
        if (selectedObject.toString().equals(m_comboBoxItems.get(ADD_CUSTOM).toString())) {
            if (m_cancelPressed) {
                m_cancelPressed = false;
                setSelectedItem(m_annotationControl.getCurrentColor());
                return;
            } else {
                m_customColorAdditioninProgress = true;
                Color customColor = addCustomColor();
                selectedObject = customColor;
                setSelectedItem(customColor);
                m_customColorAdditioninProgress = false;
            }
        }
        updateSelection((Color) selectedObject);
    }

    /**
     * Gets called everytime a combobox item needs to be rendered.
     */
    public Component getListCellRendererComponent(JList list, Object itemValue, int index, boolean isSelected, boolean cellHasFocus) {
        JPanel outerPanel = new JPanel();
        outerPanel.setPreferredSize(new Dimension(20, 20));
        JPanel innerPanel = new JPanel();
        innerPanel.setPreferredSize(new Dimension(16, 16));
        outerPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 1, 1));
        if (itemValue instanceof String) {
            // the last add custom entry needs to be rendered.
            innerPanel.setBackground(getBackground());
            JLabel label = new JLabel("...");
            label.setForeground(Color.BLACK);
            innerPanel.add(label);
            outerPanel.add(innerPanel);
        } else {
            // a normal color entry
            Color colorOptionToRender = (Color) itemValue;
            innerPanel.setBackground(colorOptionToRender);
            outerPanel.add(innerPanel);
        }
        return outerPanel;
    }

    /**
     * This method prompts the user to choose a color and then adds, if not already added,
     * the chosen color to the drop down and to color collection.
     *
     * @return the user selected color.
     */
    public Color addCustomColor() {
        Color customColor = m_annotationControl.getSelectedColor();
        ColorChooserDialog dlg = new ColorChooserDialog(m_annotationControl.m_display.getMicroscope().m_coreApplication,
                Locale.Annotations(virtuallab.Configuration.getApplicationLanguage()),
                customColor);
        dlg.setVisible(true);
        if (dlg.m_chosenColor != null && (customColor.getRGB() != dlg.m_chosenColor.getRGB())) {
            customColor = dlg.m_chosenColor;
            addCustomColor(customColor);
        } else
            m_cancelPressed = true;
        return customColor;
    }

    /**
     * This method takes in a color and adds it,iff not already present,
     * to the drop down (at the bottom) and also to the internal color collection,
     *
     * @param colorToAdd
     * @return The color that was added or null if the color was already present.
     */
    public Color addCustomColor(Color colorToAdd) {
        String colorLookupKey = colorToAdd.getRed() + "," +
                colorToAdd.getGreen() + "," + colorToAdd.getBlue();
        if (!m_comboBoxItems.containsKey(colorLookupKey)) {
            insertItemAt(colorToAdd, 0);
            setSelectedItem(colorToAdd);
            m_comboBoxItems.put(colorLookupKey, colorToAdd);
            return colorToAdd;
        } else
            return null;
    }

    /**
     * informs the annotation control that a color has been selected.
     *
     * @param selectedColor
     */
    protected void updateSelection(Color selectedColor) {
        m_annotationControl.colorComboBoxSelectionChanged(selectedColor);
    }
}
