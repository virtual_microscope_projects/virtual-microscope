/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */

package virtuallab.gui.controls.annotation;

import virtuallab.Configuration;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;


public class ToolsComboBox extends ToggleButtonsComboBox implements MouseListener {

    private static final long serialVersionUID = 1146079028547751410L;

    public ToolsComboBox(AnnotationControl annotationControl) {
        super(annotationControl);
        addMouseListener(this);
        setToolTipText(Locale.SelectAnnotationTool(Configuration.getApplicationLanguage()));
        m_selectedButton.setSelected(false);
    }

    /**
     * Gets called everytime a combobox item needs to be rendered.
     */
    public Component getListCellRendererComponent(JList list, Object itemValue, int index, boolean isSelected, boolean cellHasFocus) {
        JToggleButton buttonToBeRendered = (JToggleButton) m_comboBoxItems.get(itemValue);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(20, 20));
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        buttonPanel.add((JToggleButton) m_comboBoxItems.get(buttonToBeRendered));
        return buttonPanel;
    }


    protected void createAndPopulateComboBoxItems() {
        Insets bttnMargins = new Insets(0, 0, 0, 0);

        JToggleButton rectangleOption = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_RECTANGLE)));
        rectangleOption.setPreferredSize(new Dimension(20, 20));
        rectangleOption.setName(Constants.ANNOTATION_RECTANGLE_TOOL);
        rectangleOption.setToolTipText(Locale.Rectangle(Configuration.getApplicationLanguage()));
        rectangleOption.getAccessibleContext().setAccessibleParent(this);
        rectangleOption.getAccessibleContext().setAccessibleName(Locale.Rectangle(Configuration.getApplicationLanguage()));
        rectangleOption.getAccessibleContext().setAccessibleDescription(Locale.Rectangle(Configuration.getApplicationLanguage()));
        rectangleOption.setBorderPainted(false);
        rectangleOption.setContentAreaFilled(false);
        rectangleOption.setMargin(bttnMargins);
        m_selectedButton = rectangleOption;
        m_comboBoxItems.put(rectangleOption, rectangleOption);

        JToggleButton lineOption = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_LINE)));
        lineOption.setPreferredSize(new Dimension(20, 20));
        lineOption.setName(Constants.ANNOTATION_LINE_TOOL);
        lineOption.setToolTipText(Locale.Line(Configuration.getApplicationLanguage()));
        lineOption.getAccessibleContext().setAccessibleParent(this);
        lineOption.getAccessibleContext().setAccessibleName(Locale.Line(Configuration.getApplicationLanguage()));
        lineOption.getAccessibleContext().setAccessibleDescription(Locale.Line(Configuration.getApplicationLanguage()));
        lineOption.setBorderPainted(false);
        lineOption.setContentAreaFilled(false);
        rectangleOption.setMargin(bttnMargins);
        m_comboBoxItems.put(lineOption, lineOption);

        JToggleButton elipseOption = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_ELIPSE)));
        elipseOption.setPreferredSize(new Dimension(20, 20));
        elipseOption.setName(Constants.ANNOTATION_ELLIPSE_TOOL);
        elipseOption.setToolTipText(Locale.Ellipse(Configuration.getApplicationLanguage()));
        elipseOption.getAccessibleContext().setAccessibleParent(this);
        elipseOption.getAccessibleContext().setAccessibleName(Locale.Ellipse(Configuration.getApplicationLanguage()));
        elipseOption.getAccessibleContext().setAccessibleDescription(Locale.Ellipse(Configuration.getApplicationLanguage()));
        elipseOption.setBorderPainted(false);
        elipseOption.setContentAreaFilled(false);
        elipseOption.setMargin(bttnMargins);
        m_comboBoxItems.put(elipseOption, elipseOption);

        JToggleButton penOption = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_PEN)));
        penOption.setPreferredSize(new Dimension(20, 20));
        penOption.setName(Constants.ANNOTATION_PEN_TOOL);
        penOption.setToolTipText(Locale.Freeform(Configuration.getApplicationLanguage()));
        penOption.getAccessibleContext().setAccessibleParent(this);
        penOption.getAccessibleContext().setAccessibleName(Locale.Freeform(Configuration.getApplicationLanguage()));
        penOption.getAccessibleContext().setAccessibleDescription(Locale.Freeform(Configuration.getApplicationLanguage()));
        penOption.setBorderPainted(false);
        penOption.setContentAreaFilled(false);
        penOption.setMargin(bttnMargins);
        m_comboBoxItems.put(penOption, penOption);

        JToggleButton textOption = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_TEXT)));
        textOption.setPreferredSize(new Dimension(20, 20));
        textOption.setName(Constants.ANNOTATION_TEXT_TOOL);
        textOption.setToolTipText(Locale.Text(Configuration.getApplicationLanguage()));
        textOption.getAccessibleContext().setAccessibleParent(this);
        textOption.getAccessibleContext().setAccessibleName(Locale.Text(Configuration.getApplicationLanguage()));
        textOption.getAccessibleContext().setAccessibleDescription(Locale.Text(Configuration.getApplicationLanguage()));
        textOption.setBorderPainted(false);
        textOption.setContentAreaFilled(false);
        textOption.setMargin(bttnMargins);
        m_comboBoxItems.put(textOption, textOption);
    }

    public String getSelectedToolName() {
        if (m_selectedButton.isSelected())
            return m_selectedButton.getName();
        else
            return Constants.NO_ACTIVE_TOOL;
    }

    public JToggleButton getSelectedOption() {
        return m_selectedButton;
    }

    public void setSelected(String toolToSelect) {
        setSelection(toolToSelect);
    }

    public void unselectActiveTool() {
        m_selectedButton.setSelected(false);
    }

    public void mouseExited(MouseEvent event) { }
    public void mouseEntered(MouseEvent event) { }
    public void mousePressed(MouseEvent event) {
        this.setPopupVisible(false);
    }
    public void mouseClicked(MouseEvent event) { }
    public void mouseReleased(MouseEvent event) {
        if (!m_selectedButton.isSelected())
            m_selectedButton.setSelected(true);
    }
}
