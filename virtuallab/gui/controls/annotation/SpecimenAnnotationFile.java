/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import virtuallab.util.Locale;

/**
 * This class encapsulates a specimen annotation xml file.
 *
 */
public class SpecimenAnnotationFile {
    /**
     * auto generated id.
     */
    private static final long serialVersionUID = -8458739207596578612L;
    /**
     * The File that this class encapsulates.
     */
    private File m_annotationFile;
    /**
     * The short description as read from the annotation XM file's
     * short description tag.
     */
    private String m_shortDescription;
    private String m_specimenID;


    public SpecimenAnnotationFile(File annotationFile) {
        m_annotationFile = annotationFile;
        extractSpecimenIDAndShortDescription();
    }

    public String getShortDescription() {
        return m_shortDescription;
    }

    public String getSpecimenID() {
        return m_specimenID;
    }

    public String getAnnotationFileName() {
        return m_annotationFile.getName();
    }

    public String getAnnotationFilePath() {
        return m_annotationFile.getAbsolutePath();
    }

    public String getLongDescriptionAsHtml() {
        String longDescription = "<html><b>"+ Locale.ShortDescription(virtuallab.Configuration.getApplicationLanguage())+":</b>" + m_shortDescription +
                "<br><b>"+Locale.FileName(virtuallab.Configuration.getApplicationLanguage())+":</b>" + getAnnotationFileName() + "</html>";
        return longDescription;
    }

    public String toString() {
        return m_shortDescription;
    }

    /**
     * Extracts the associated specimen id and short description from the
     * annotation xml file.
     *
     * @return specimenName or null if no specimen name was found
     */
    public void extractSpecimenIDAndShortDescription() {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(m_annotationFile);
            Node root = doc.getElementsByTagName(AnnotationControl.XML_TAG_ANNOTATIONS).item(0);
            Element elem = (Element) ((Element) root).getElementsByTagName(AnnotationControl.XML_TAG_SPECIMEN_NAME).item(0);
            m_specimenID = elem.getAttribute(AnnotationControl.XML_ATTRIB_NAME);

            elem = (Element) ((Element) root).getElementsByTagName(AnnotationControl.XML_TAG_SHORT_DESCRIPTION).item(0);

            NodeList list = ((Element) elem).getElementsByTagName(AnnotationControl.XML_TAG_TEXT);

            if (list != null && list.getLength() == 1 &&
                    list.item(0).getNodeType() == Node.ELEMENT_NODE) {
                // Get the only child (text) node
                list = list.item(0).getChildNodes();

                if (list != null && list.getLength() == 1 &&
                        list.item(0).getNodeType() == Node.TEXT_NODE) {
                    m_shortDescription = ((Text) list.item(0)).getData();
                }
            }
        } catch (Exception e) {
            m_shortDescription = m_annotationFile.getName();
        }
    }

    public File getAnnotationFile() {
        return m_annotationFile;
    }


    /**
     * Extracts the short description.
     * @param file Annotation file
     * @return specimenName or null if no specimen name was found
     *
    public String getShortDescription(File file)
    {
    String shortDescription = null;
    try
    {
    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
    Document doc = builder.parse(file);
    Node root = doc.getElementsByTagName(AnnotationControl.XML_TAG_ANNOTATIONS).item(0);
    Element elem = (Element)((Element)root).getElementsByTagName(AnnotationControl.XML_TAG_SHORT_DESCRIPTION).item(0);

    NodeList list = ((Element)elem).getElementsByTagName(AnnotationControl.XML_TAG_TEXT);

    if (list != null && list.getLength() == 1 &&
    list.item(0).getNodeType() == Node.ELEMENT_NODE)
    {
    // Get the only child (text) node
    list = list.item(0).getChildNodes();

    if (list != null && list.getLength() == 1 &&
    list.item(0).getNodeType() == Node.TEXT_NODE)
    {
    shortDescription = ((Text)list.item(0)).getData();
    }
    }
    }
    catch (Exception e)
    {
    e.printStackTrace();
    }
    return shortDescription;
    }*/
}