/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.annotation;

import virtuallab.Configuration;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
/**
 * This Class implements the Annotation LineWidth Selector Dropdown.
 *
 */
public class LineWidthComboBox extends ToggleButtonsComboBox {
    /**
     * Auto generated serial version id
     */
    private static final long serialVersionUID = -8202069535820991915L;

    public LineWidthComboBox(AnnotationControl annotationControl) {
        super(annotationControl);
        setToolTipText(Locale.SetLineWidth(Configuration.getApplicationLanguage()));
    }

    /**
     * Gets called everytime a combobox item needs to be rendered.
     */
    public Component getListCellRendererComponent(JList list, Object itemValue, int index, boolean isSelected, boolean cellHasFocus) {
        JToggleButton buttonToBeRendered = (JToggleButton) m_comboBoxItems.get(itemValue);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setPreferredSize(new Dimension(20, 20));
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        buttonPanel.add((JToggleButton) m_comboBoxItems.get(buttonToBeRendered));
        return buttonPanel;
    }


    /**
     * Creates all the toggle buttons corresponding to each line width and puts
     * them in the collection in the order in which they should appear in the
     * dropdown.
     */
    protected void createAndPopulateComboBoxItems() {
        Insets bttnMargins = new Insets(0, 0, 0, 0);

        JToggleButton onePixelLineButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.ONE_PIXEL_LINE)));
        onePixelLineButton.setName("1.0");
        onePixelLineButton.setToolTipText(Locale.OnePixelWideLine(Configuration.getApplicationLanguage()));
        onePixelLineButton.getAccessibleContext().setAccessibleParent(this);
        onePixelLineButton.getAccessibleContext().setAccessibleName(Locale.OnePixelLineWidthSelection(Configuration.getApplicationLanguage()));
        onePixelLineButton.getAccessibleContext().setAccessibleDescription(Locale.UsedForOnePixelLineWidthSelection(Configuration.getApplicationLanguage()));
        onePixelLineButton.setBorderPainted(false);
        onePixelLineButton.setContentAreaFilled(false);
        onePixelLineButton.setMargin(bttnMargins);
        onePixelLineButton.setSelected(true);
        m_selectedButton = onePixelLineButton;
        m_comboBoxItems.put(onePixelLineButton, onePixelLineButton);

        JToggleButton twoPixelLineButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.TWO_PIXEL_LINE)));
        twoPixelLineButton.setName("2.0");
        twoPixelLineButton.setToolTipText(Locale.TwoPixelWideLine(Configuration.getApplicationLanguage()));
        twoPixelLineButton.getAccessibleContext().setAccessibleParent(this);
        twoPixelLineButton.getAccessibleContext().setAccessibleName(Locale.TwoPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        twoPixelLineButton.getAccessibleContext().setAccessibleDescription(Locale.UsedForTwoPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        twoPixelLineButton.setBorderPainted(false);
        twoPixelLineButton.setContentAreaFilled(false);
        twoPixelLineButton.setMargin(bttnMargins);
        m_comboBoxItems.put(twoPixelLineButton, twoPixelLineButton);

        JToggleButton fourPixelLineButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.FOUR_PIXEL_LINE)));
        fourPixelLineButton.setName("4.0");
        fourPixelLineButton.setToolTipText(Locale.FourPixelWideLine(Configuration.getApplicationLanguage()));
        fourPixelLineButton.getAccessibleContext().setAccessibleParent(this);
        fourPixelLineButton.getAccessibleContext().setAccessibleName(Locale.FourPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        fourPixelLineButton.getAccessibleContext().setAccessibleDescription(Locale.UsedForFourPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        fourPixelLineButton.setBorderPainted(false);
        fourPixelLineButton.setContentAreaFilled(false);
        fourPixelLineButton.setMargin(bttnMargins);
        m_comboBoxItems.put(fourPixelLineButton, fourPixelLineButton);

        JToggleButton sixPixelLineButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.SIX_PIXEL_LINE)));
        sixPixelLineButton.setName("6.0");
        sixPixelLineButton.setToolTipText(Locale.SixPixelWideLine(Configuration.getApplicationLanguage()));
        sixPixelLineButton.getAccessibleContext().setAccessibleParent(this);
        sixPixelLineButton.getAccessibleContext().setAccessibleName(Locale.SixPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        sixPixelLineButton.getAccessibleContext().setAccessibleDescription(Locale.UsedForSixPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        sixPixelLineButton.setBorderPainted(false);
        sixPixelLineButton.setContentAreaFilled(false);
        sixPixelLineButton.setMargin(bttnMargins);
        m_comboBoxItems.put(sixPixelLineButton, sixPixelLineButton);

        JToggleButton eightPixelLineButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.EIGHT_PIXEL_LINE)));
        eightPixelLineButton.setName("8.0");
        eightPixelLineButton.setToolTipText(Locale.EightPixelWideLine(Configuration.getApplicationLanguage()));
        eightPixelLineButton.getAccessibleContext().setAccessibleParent(this);
        eightPixelLineButton.getAccessibleContext().setAccessibleName(Locale.EightPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        eightPixelLineButton.getAccessibleContext().setAccessibleDescription(Locale.UsedForEightPixelLineWidthSelection(Configuration.getApplicationLanguage()));
        eightPixelLineButton.setBorderPainted(false);
        eightPixelLineButton.setContentAreaFilled(false);
        eightPixelLineButton.setMargin(bttnMargins);
        m_comboBoxItems.put(eightPixelLineButton, eightPixelLineButton);


    }

    /**
     * This method takes in a toggle button that is to be shown selected.
     *
     * @param selectedButton
     */
    protected void updateSelection(JToggleButton selectedButton) {
        super.updateSelection(selectedButton);
        m_annotationControl.linwWidthComboBoxSelectionChanged(Float.parseFloat(selectedButton.getName()));
    }
}
