/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.eds;

import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Vector;

import virtuallab.ImageCache;
import virtuallab.ImageSet;
import virtuallab.OverlayElement;
import virtuallab.Specimen;
import virtuallab.Tile;
import virtuallab.util.Constants;
import virtuallab.util.Utility;
/**
 * This class handles the image related processing for the EDS overlay. The
 * EDSMenu uses this class to handle all the overlay image handling from image
 * creation to image caching and other nitty gritties of image handling.
 *
 */

public class OverlayImageHandler {
    /**
     * Holds the overlay tiled image cache. Used to implement image caching for
     * the overlay images.
     */
    ImageCache m_overlayImageCache;

    /**
     * Holds the index of the brightest pixel element against each pixel value.
     * e.g. if the brightest pixel at slot [0][0] was that of Calcium (Ca) whose
     * index is 1 then the m_cachedBrightestPixels[0][0] will be set to 1. As
     * long as the selected element set remains the same the cache values will
     * be used and we will not need to recalculate the brightest value at each
     * pixel. If the user checks/unchecks one or more elements then the cache
     * will be cleared and we will recalculate the brightest values and then
     * populate the cache with these values. TODO: implement this logic in the
     * code. This logic has not been implemented yet.
     */
    private short m_cachedBrightestPixels[][];

    /**
     * Holds information that lets us know the elements for which the
     * m_cachedBrightestPixels values were calculated. e.g. if the selected
     * elements were Ca, Ba, Na and the m_cachedBrightestPixels contains the
     * brightest pixel information for these three elements then this variable
     * will hold the index numbers (separated by a delimiting space between each
     * index number) pertaining to these three elements. This way when the user
     * selets an additional element 'Al' we would know that the cache needs to
     * be cleared. TODO: implement this logic in the code. This logic has not
     * been implemented yet.
     */
    private String m_cachedBrightestPixelsKey;

    /**
     * The reference to the EDSMenu that instantiated this OverlayImageHandler
     * object.
     */
    private EDSMenu m_edsMenu;

    public OverlayImageHandler(EDSMenu parent) {
        // TODO: Come up with a more researched cache value based on the
        // available system memory and the tile size etc.
        int cacheSize = (int) (((Runtime.getRuntime().maxMemory() / 1000000) * 0.3158 + 19.16) * 0.25);
        // System.out.println("EDS Cache :" + cacheSize);
        m_overlayImageCache = new ImageCache(cacheSize); // the cache that
        // will hold the
        // tiled overlay
        // images.
        m_edsMenu = parent;
    }

    /**
     * This method selects the Overlay tiles that need to be loaded/created
     * based on the input information. We shall keep the overlay tiles insynch
     * with the base image tiles. This will make the approach consistent and
     * easy to follow.
     *
     * @return Array of Tile Objects that needs to be loaded/created for
     * immediate showing.
     */
    public Tile[] chooseOverlayTiles(ImageSet imageSet, Tile[] baseTiles) {
//		long mills = System.currentTimeMillis();
        // Step 1: First figure out which of the base image tiles are being
        // shown to the user.
        Tile[] overlayTiles = new Tile[baseTiles.length];
        double pixelW = imageSet.getPixelSize().getX();
        double pixelH = imageSet.getPixelSize().getY();
        String overlayElements = "";
        Vector vt = m_edsMenu.getSelectedOverlayElements();
        // forming the postfix for the tile path.
        for (int j = 0; j < vt.size(); j++) {
            overlayElements += ((OverlayElement) vt.get(j)).m_shortName
                    + Constants.PATH_SEPARATOR;
        }
        for (int i = 0; i < baseTiles.length; i++) {
            Tile bTile = baseTiles[i];
            /**
             * The path will act as a key (and hence a unique identifier) The
             * overlay tiles is composed of drawing position row/col the tile
             * pixel width and height (which will indicate the
             * magnification/resize/tile size to be used) and the selected
             * overlay elements (which will tell us which overlay images to use
             * to compose the consolidated image). The combination of these
             * parameters will form the unique key for each overlay tile.
             */
            String path = bTile.getRow() + Constants.PATH_SEPARATOR
                    + bTile.getCol() + Constants.PATH_SEPARATOR + pixelW
                    + Constants.PATH_SEPARATOR + pixelH
                    + Constants.PATH_SEPARATOR + overlayElements;
            Tile oTile = new Tile(bTile.getRow(), bTile.getCol(), bTile
                    .getPosition().x, bTile.getPosition().y);
            oTile.setPath(path);
            overlayTiles[i] = oTile;
        }
//		mills = System.currentTimeMillis() - mills;
//		System.out.println("Tiles chosen in " + (mills/1000L) + " second(s)");
        return overlayTiles;
    }

    /**
     * returns the image from the cache. Null if the image was not found in the
     * cache.
     *
     * @param key
     * @return
     */
    public BufferedImage getImage(String key) {
        return (BufferedImage) m_overlayImageCache.getImage(key);
    }


    /**
     * This method prepares AND draws the overlay tiled image. This method is
     * similar to prepareOverlayImageSet() except that it also draws the overlay
     * tiles.
     *
     * @param specimen the loaded specimen
     * @param ImageSet the base image set, used to determine the overlay tiles and
     *                 resizing.
     * @param g2d      The graphics device context used for drawing the tiles.
     * @see prepareOverlayImageSet
     */
    public void drawOverlayImageSet(ImageSet imageSet, Specimen specimen, Graphics2D g2d) {
        Tile[] baseTiles = imageSet.getChosenTiles();
        if (baseTiles == null) {
            return;
        }
        Tile[] tiles = chooseOverlayTiles(imageSet, baseTiles);
        for (int i = 0; i < tiles.length; i++) {
            BufferedImage image = m_overlayImageCache.getImage(tiles[i]
                    .getFilePath());
            if (image == null) {
                image = getImageTileFromDisk(tiles[i].getFilePath(), specimen.getUniqueName());
                if (image == null) {
                    // The required tile is not in our cache, we need to create the
                    // tile and
                    // add it to our cache.
                    long millis = System.currentTimeMillis();
                    image = createOverlayTile(tiles[i], imageSet, baseTiles[i].getFilePath());
                    millis = System.currentTimeMillis() - millis;
                    m_overlayImageCache.add(tiles[i].getFilePath(), image);
                    addToDiskCache(image, specimen);
                }
            }
            g2d.drawImage(image, null, tiles[i].getPosition().x, tiles[i].getPosition().y);
        }
    }


    /**
     * This method stores the given tile to disk.
     *
     * @param image
     * @param specimen TODO: This is just a stub. This is just an idea to improve the EDS performance.
     *                 Time permitting this method should be implemented and tests should be performed
     *                 there is some improvement
     */
    private void addToDiskCache(BufferedImage image, Specimen specimen) {
    }

    /**
     * This method looks up the disk cache to see if some tile is available on the disk
     *
     * @param filePath
     * @param uniqueName
     * @return the bufferedimage loaded from the disk or null if none was found.
     * TODO: This is just a stub. This is just an idea to improve the EDS performance.
     * Time permitting this method should be implemented and tests should be performed
     * there is some improvement
     */
    private BufferedImage getImageTileFromDisk(String filePath, String uniqueName) {
        return null;
    }

    /**
     * This method creates a single consolidated image tile from ALL the overlay
     * images. Additionally the method also resizes the tiled image to make it
     * insynch with the baseimage tile.
     *
     * @param tile         to create
     * @param ImageSet     the base iamage set on top of which this tile will be overlayed
     * @param baseTilePath the unique key that indentifies a tile in the tile collection/tile
     *                     cache.
     */
    public BufferedImage createOverlayTile(Tile tile, ImageSet baseImageSet,
                                           String baseTilePath) {
        /**
         * Given the base image tile row and column number find out the
         * corresponding X-index and Y-index into the overlay image data 2-D
         * array from where we should start creating the corresponding overlay
         * tile.
         */

        // finding out the starting x and y indices into the overlay image data
        // array. Note that we
        // are implicitly using 'nearest neighbour' approach here by rounding
        // off the indices.
        int startX = (int) Math.round(tile.getCol()
                * baseImageSet.getTileSize().getWidth()
                * baseImageSet.getPixelSize().getX()
                / m_edsMenu.getPixelSize().getX());
        int startY = (int) Math.round(tile.getRow()
                * baseImageSet.getTileSize().getHeight()
                * baseImageSet.getPixelSize().getY()
                / m_edsMenu.getPixelSize().getY());
        int endX = startX
                + (int) Math.round(baseImageSet.getTileSize().getWidth()
                * baseImageSet.getPixelSize().getX()
                / m_edsMenu.getPixelSize().getX());
        int endY = startY
                + (int) Math.round(baseImageSet.getTileSize().getHeight()
                * baseImageSet.getPixelSize().getY()
                / m_edsMenu.getPixelSize().getY());
        Point2D.Double pt = m_edsMenu.getDisplay().getTileSize(baseImageSet
                .getPath()
                + baseTilePath);
        // Making sure we do not go pass the edges.
        if (endX > (int) m_edsMenu.getOverlayElementDataArraySize().getX())
            endX = (int) m_edsMenu.getOverlayElementDataArraySize().getX();
        if (endY > (int) m_edsMenu.getOverlayElementDataArraySize().getY())
            endY = (int) m_edsMenu.getOverlayElementDataArraySize().getY();

        Vector vt = m_edsMenu.getSelectedOverlayElements();
        // Look at the pixel value for each of the overlay image the overlay
        // with the
        // highest brighntess at a specific pixel is the winner and we shall use
        // this
        // value for the destination image.
        GraphicsConfiguration gc = Utility.getDefaultConfiguration();
        BufferedImage overlayImageTile = gc.createCompatibleImage(
                (endX - startX), (endY - startY));
        int ii = 0;
        int jj = 0;
//		int[] pixel = new int[4];
//		WritableRaster raster = overlayImageTile.getRaster();
        for (int i = startX; i < endX; i++) {
            for (int j = startY; j < endY; j++) {
                int index = getBrightestPixel(vt, i, j);
                OverlayElement oe = (OverlayElement) vt.get(index);
                int rgb = oe.getImageRGB(i, j);
//				Color newColor = new Color(rgb);
//				pixel[0] = newColor.getRed();
//				pixel[1] = newColor.getGreen();
//				pixel[2] = newColor.getBlue();
//				pixel[3] = (rgb & 0xff000000) >> 24;
//				raster.setPixel(ii,jj,pixel);
                overlayImageTile.setRGB(ii, jj, rgb);
                // see if we can create a HSV image so that we do not need to do
                // the RGB/HSV conversio.
                jj++;
            }
            ii++;
            jj = 0;
        }
        // return overlayImage;
        // return resizeImage(overlayImage, scaleX, scaleY);
        return Utility.getScaledInstance(overlayImageTile, (int) pt.getX(),
                (int) pt.getY());
        // return resizeImage(overlayImage, (int)pt.getX(),(int)pt.getY());
    }


    /**
     * This method clears up the image cache.
     */
    public void clearCache() {
        m_overlayImageCache.clear();
    }

    /**
     * This method takes in a pixel and finds out the overlay image that has the
     * maximum brightness at this pixel. The index of this winner overlay is the
     * returned value.
     *
     * @param vt the vector containing all the selected overlay elements.
     * @param x  the pixel x location
     * @param y  the pixel y location.
     * @return the index of the overlay element that has the brightest pixel
     * value at the x,y location.
     */
    private int getBrightestPixel(Vector vt, int x, int y) {
        int winnerIndex = 0;
        float maxBrightness = -1;
        for (int k = 0; k < vt.size(); k++) {
            OverlayElement oe = (OverlayElement) vt.get(k);
            if (maxBrightness < oe.m_pixelBrightnessValues[x][y]) {
                maxBrightness = (float) oe.m_pixelBrightnessValues[x][y];
                winnerIndex = k;
            }
        }
        return winnerIndex;
    }

}
