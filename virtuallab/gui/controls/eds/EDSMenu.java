/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui.controls.eds;


import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.geom.Ellipse2D;
import java.awt.geom.GeneralPath;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import virtuallab.Configuration;
import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.Specimen.ControlStates;
import virtuallab.OverlayElement;
import virtuallab.gui.MenuControlBase;
import virtuallab.gui.ViewControl;
import virtuallab.gui.controls.annotation.Annotation;
import virtuallab.gui.controls.annotation.AnnotationControlChangeListener;
import virtuallab.util.*;

/**
 * This class implements the side controls applicable to EDS. This control is considerably
 * more complicated than simple slider menu and text menu controls. Amongst others this control
 * embodies a drop down (containing the detectors and a default 'None' option), a slider control
 * for opacity, a check box group with a checkbox for each Overlay Element (e.g. Al, Si etc.).
 * It also contains its own selection tools which are used to show
 * X-Ray count graphs (much like the annotation control groups). In short
 * this class is a juxtaposition of several controls that link to each other in different ways to
 * implement the overall EDS functionality.
 * <p>
 * In the context of this class a 'base image' is the image without any X-Ray count information. this
 * is the usual raw specimen image that we are used to seeing in the viewport. For these base
 * images there can me one or several Overlay elements' images that represent the X-Ray count of the
 * respective elements. This class handles the task of overlaying these X-Ray images ontop of this
 * base image with varying (user selected) opacity using (user selected) colors.
 * <p>
 * The selection tools allow the user to click at a desired spot or to select an area or draw a line - all
 * indicating the region of the image whose X-ray counts the user wants to view in graphical form.
 * <p>
 * This class encompasses several helper inner classes. The inner classes make this Class self-sufficient.
 *
 */

public class EDSMenu extends MenuControlBase implements AnnotationControlChangeListener, ItemListener, ActionListener, ViewControl, KeyListener, ChangeListener {
    private static final long serialVersionUID = 7258282691709372589L;
    /**
     * Holds the overlay image handler object which is used for all overlay image
     * processing.
     */
    public OverlayImageHandler m_overlayImageHandler;
    /**
     * The Array that contains the checkboxes corresponding to
     * each overlay element.
     */
    protected OverlayElementCheckbox m_elementsArray[];
    /**
     * This variable contains the reference to the MicroscopeView object - in which this control
     * is contained.
     */
    protected MicroscopeView m_display = null;
    /**
     * The drop down that contains the detectors e.g. back scatter
     */
    protected JComboBox m_comboBox;
    /**
     * The opacity slider.
     */
    protected JSlider m_slider;
    /**
     * The inner class that contains the represent and handles the Overlay panel.
     */
    Overlay m_overlayPanel = null;
    /**
     * Holds the last value of the BaseImage dropdown prior to the current selected value.
     */
    private String m_oldBaseImageValue = "";
    /**
     * Holds the status of whether the base image dropdown is set to "None" or not.
     */
    private boolean m_isBaseImageNone;
    /**
     * The class that manages the graph windows and their associated annotations.
     */
    private GraphManager m_graphManager = new GraphManager();
    //private LinkedHashMap m_overlapElements;
    private GraphHandler m_graphHandler = new GraphHandler();

    /**
     * Retrns the pixel size of the overlay image. We assume the pixel size
     * for ALL the overlay images to be the same so returning the first overlayelement's
     * pixel size is enough. Also the pixel sizeis assumed to be a perfect square, so only
     * the width need to be stored in the specimen.xml file. This method forms a Point2D.Double
     * using the same width and height and returns that as the pixel size.
     *
     * @return the pixel size of the first overlayelement. Since all overlay elements are assumed to
     * have the same pixel size returning any one of the pixel size is enough.
     */
    public Point2D.Double getPixelSize() {
        OverlayElement oe = m_elementsArray[0].m_overlayElement;
        Point2D.Double pixel = new Point2D.Double(oe.m_pixelSize, oe.m_pixelSize);
        return pixel;
    }

    /**
     * This method unselects any selected tools. This is useful incases when
     * any other controls want to ensure that no AFM tool is active before they
     * can start their work.
     */
    public void unselectAllTools() {
        m_graphHandler.m_persistentGraphMode = false;
        m_graphHandler.m_bttnArea.setSelected(false);
        m_graphHandler.m_bttnLine.setSelected(false);
        m_graphHandler.m_bttnPoint.setSelected(false);
    }

    /**
     * This method returns the size of the 2-D array that contains the overlay elements's pixel
     * brightness information. This information is used while creating the consolidated overlay image
     *
     * @return the size of the 2-D array. The X value is the size of the first dimensions of the array
     * and the Y value is the size of the second dimension.
     */
    public Point2D.Double getOverlayElementDataArraySize() {
        OverlayElement oe = m_elementsArray[0].m_overlayElement;
        int x = oe.m_pixelBrightnessValues.length;
        int y = oe.m_pixelBrightnessValues[0].length;
        // TODO: check to see if this change is behavior preserving.
//		int x = oe.getImageHeight();//m_pixelBrightnessValues.length;
//		int y = oe.getImageWidth();//m_pixelBrightnessValues[0].length;
        return new Point2D.Double(x, y);
    }

    /**
     * This method updates the GUI controls contained in the DetectorMenu. It
     * removes ALL the current gui controls contained in this DetectorMenu and then recreates
     * them with the new values.
     *
     * @param version the XML Version.
     */
    public void updateGUI(double version) {
        if (m_validControlStates == null) {
            return;
        }
        setMaximumSize(new Dimension(1024, getMinimumSize().height));
        //setPreferredSize(new Dimension(150, 330));

        // Remove the action listeners if set. This MUST be done
        // othewise even after removing these components they will
        // keep on getting these events and it would be a whole
        // mess.
        if (m_comboBox != null)
            m_comboBox.removeActionListener(this);
        removeAll();

        /**
         * The layout is designed such that all related controls are
         * contained in a panel. This encompassing panel is then
         * added into the main EDSMenu side panel.
         */
        add(Box.createVerticalGlue());
        // Base image drop down.
        add(updateBaseImageGUI(version));
        // Overlay elements checkboxes
        m_overlayPanel.updateGUI(version);
        add(m_overlayPanel);
        // Opacity slider
        add(updateOpacityGUI(version));
        // Graph Selection Tools
        add(m_graphHandler.updateToolGUI(version));
        add(Box.createVerticalGlue());
        ////// Add listeners ///////////////
        m_comboBox.addItemListener(this);
        addKeyListener(this);
    }

    /**
     * This method draws the drop down that contains the "Base Image" options.
     */
    private JPanel updateBaseImageGUI(double version) {
        m_comboBox = new JComboBox();
        //m_comboBox.setPreferredSize(new Dimension(50,3));
        Iterator iter = m_validControlStates.iterator();
        while (iter.hasNext()) {
            String s = "";
            if (version <= 1.0) {
                Specimen.ControlState cs = (Specimen.ControlState) iter.next();
                s = cs.getDisplayString();
            } else {
                Specimen.ControlStates cs = (Specimen.ControlStates) iter.next();
                s = cs.getDisplayString();
            }
            m_comboBox.addItem(s);
        }
        updateButtonStates();

        JPanel detectorPanel = new JPanel();
        detectorPanel.setLayout(new BoxLayout(detectorPanel, BoxLayout.Y_AXIS));
        detectorPanel.setBorder(BorderFactory.createTitledBorder(Locale.BaseImage(Configuration.getApplicationLanguage())));

        detectorPanel.add(m_comboBox);
        detectorPanel.add(Box.createRigidArea(new Dimension(0, 5)));
        m_comboBox.setToolTipText(Locale.SelectBaseImage(Configuration.getApplicationLanguage()));
        return detectorPanel;
    }

    /**
     * This method draws the Opacity slider.
     *
     * @param version
     * @return the JPanel containing the opacity slider.
     * @throws Exception
     */
    private JPanel updateOpacityGUI(double version) {
        if (version <= 1.0) {
            return new JPanel();
        }
        // The slider control.
        JPanel opacityPanel = new JPanel();
        opacityPanel.setLayout(new BoxLayout(opacityPanel, BoxLayout.PAGE_AXIS));
        opacityPanel.setBorder(BorderFactory.createTitledBorder(Locale.Opacity(Configuration.getApplicationLanguage())));
        m_slider = new JSlider();
        m_slider.setMajorTickSpacing(25);
        m_slider.setMinorTickSpacing(5);
        m_slider.setPaintTicks(true);
        m_slider.setPaintLabels(true);
        m_slider.setValue(50);
        m_slider.setPreferredSize(new Dimension(100, 50));
        m_slider.setToolTipText(Locale.OpacitySlider(Configuration.getApplicationLanguage()));
        opacityPanel.add(m_slider);
        m_slider.addChangeListener(this);
        return opacityPanel;
    }

    /**
     * Handles the events fired by the opacity slider control.
     *
     * @param version
     * @return
     */
    public void stateChanged(ChangeEvent e) {
        m_display.validate();
        m_display.repaint();
    }

    public float getOpacity() {
        return ((float) m_slider.getValue() / 100);
    }

    /**
     * This method selects the selected value to the value passed in as the parameter
     *
     * @param value the value that is to be selected.
     */
    public void setSelectedValue(String value) {
        m_comboBox.setSelectedItem(value);
        //m_comboBox.revalidate();
    }

    /**
     * This method cleans up the EDS controls and frees up the memory. This will usually be called
     * when the specimen is being unloaded. Not doing this upon unloading will usually result
     * in an out of memory error when performing "Tools->Refresh".
     */
    public void cleanUp(double xmlVersion) {
	  /*
	// Remove the action listeners if set. This MUST be done
	// othewise even after removing these components they will
	// keep on getting these events and it would be a whole
	// mess.
	if (m_comboBox != null)
		m_comboBox.removeActionListener(this);
	if (m_plusButton != null)
		m_plusButton.removeActionListener(this);
	if (m_minusButton != null)
		m_minusButton.removeActionListener(this);
	*/

        // cleaning up the overlay elements data
        Vector vt = getOverlayElements();
        for (int i = 0; i < vt.size(); i++) {
            OverlayElement oe = (OverlayElement) vt.get(i);
            oe.cleanUp();
            oe = null;
        }
        vt.removeAllElements();
        vt = null;
        // explicitly setting up things that matter to null, so that GC can collect them.
        m_elementsArray = null;
        m_graphHandler = null;
        m_graphManager.cleanUp();
        m_graphManager = null;
        m_overlayImageHandler.clearCache();
        m_overlayImageHandler = null;
        removeAll();
        setVisible(false);
    }

    /**
     * This method closes all graph windows and does the necessary clean up.
     */
    public void closeAllGraphs() {
        if (m_graphManager != null)
            m_graphManager.cleanUp();
    }

    /**
     * Called when a different Base image menu item is selected.
     * This method handles the special case of when the Base image is
     * set to "None". In such a case this method retains the baseimage's internal
     * value to the last non-none selected value. This will allow the rest of the
     * system logic to work correctly. However it sets the isBaseimageNone to 'true'
     * indicating to the Microscope View's base image drawing logic that the base image
     * should not be drawn.
     * This trick helps us achieve the peculiar 'None' functionality cleanly and with minimum
     * code change.
     */
    public void itemStateChanged(ItemEvent e) {
        int i = m_comboBox.getSelectedIndex();
        m_oldBaseImageValue = m_currentValue;
        m_currentValue = m_comboBox.getItemAt(i).toString();
        if (m_currentValue.equals(Constants.Controls.NONE_DISPLAY_STRING)) {
            m_currentValue = m_oldBaseImageValue;
            m_isBaseImageNone = true;

        } else
            m_isBaseImageNone = false;
        m_microscope.reCreateControls(this, m_display.getCurrentSpecimen().getXMLVersion());
        if (!m_isButtonPressed && m_comboBox.hasFocus())
            m_comboBox.grabFocus();
        m_microscope.updateSpecimenControlStates();
        m_isButtonPressed = false;
        updateButtonStates();
    }

    public boolean isBaseImageNone() {
        return m_isBaseImageNone;
    }

    public void actionPerformed(ActionEvent e) {
    }

    public void draw(Graphics2D g2d) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    public void keyPressed(KeyEvent ke) {
        switch (ke.getKeyCode()) {
            case KeyEvent.VK_ESCAPE:
                if (m_graphHandler.m_persistentGraphMode) {
                    // pressinf escape makes any persistable
                    // tool to unselect itself.
                    m_graphHandler.m_persistentGraphMode = false;
                    m_graphHandler.m_bttnArea.setSelected(false);
                    m_graphHandler.m_bttnLine.setSelected(false);
                    m_graphHandler.m_bttnPoint.setSelected(false);
                }
                break;
            default:
                break;
        }
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void mouseClicked(MouseEvent e) {

        if (m_graphHandler.isAnyToolSelected() || m_graphHandler.m_graphModeJustFinished) {
            // We shall consume the clicked event as well, since this creates some
            // conflict with annotations e.g. if we do not do this then the label annotation
            // is always shown selected no matter what we do, because of the fact that the
            // mouse click is near the labelling line and hence when the mouseclicked event
            // gets called the annotationcontrol thinks it needs to select the line.
            //
            e.consume();
            // This variable is needed because in the volatile mode the graph window
            // pops-up only once per click. After the click the button gets disabled. Hence
            // the m_graphHandler.isAnyToolSelected() check will fail, resulting in the annotation
            // being shown as selected. Through this variable we find a workaround this problem.
            m_graphHandler.m_graphModeJustFinished = false;
            // Making sure the graph window retains the focus and is at top.
            m_graphManager.m_lastGraphFrame.requestFocusInWindow();
        }
    }

    /**
     * This method handles the case when the user clicks the image with one of the
     * tools (point, area, line) selected.
     */
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            m_graphHandler.handleMousePressed(e);
        }
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        m_graphHandler.handleMouseReleased(e);
    }

    /**
     * This method returns a vector containing the OverlayElements corresponding to
     * the selected elements.
     *
     * @return
     */
    public Vector getSelectedOverlayElements() {
        Vector sel = new Vector();
        for (int i = 0; i < m_elementsArray.length; i++) {
            if (m_elementsArray[i].isSelected())
                sel.add(m_elementsArray[i].m_overlayElement);
        }
        return sel;
    }

    /**
     * This method returns a vector containing all the OverlayElements.
     *
     * @return
     */
    public Vector getOverlayElements() {
        Vector sel = new Vector();
        for (int i = 0; i < m_elementsArray.length; i++) {
            if (m_elementsArray[i].isSelected())
                sel.add(m_elementsArray[i].m_overlayElement);
        }
        return sel;
    }

    /**
     * This method takes in a vector containing the OverlayElements and returns a sorted vector
     * ordered in ascending Energy Level order
     *
     * @return sorted overlayelements vector w.r.t. increasing energy levels.
     */
    public Vector sortOverlayElements(Vector vt) {
        Vector values = new Vector(vt.size());
        Object obj[] = new Object[vt.size()];
        for (int i = 0; i < obj.length; i++)
            obj[i] = vt.get(i);
        Arrays.sort(obj);
        for (int j = 0; j < obj.length; j++) {
            values.add(obj[j]);
        }
        return values;

    }

    /**
     * This method goes through each of the elements and returns true if atleast one of them is
     * dirty.
     *
     * @return true if atleast one of the elements is dirty false otherwise.
     */
    public boolean areElementsDirty() {
        boolean isDirty = false;
        for (int i = 0; i < m_elementsArray.length; i++) {
            OverlayElement oe = m_elementsArray[i].m_overlayElement;
            if (oe.isDirty())
                return true;

        }
        return isDirty;
    }

    /**
     * This method resets the dirty flags of all the elements to false. Additionally this
     * method also returns true if atleast one of the elements is dirty. This is a convenient
     * way for the calling method to find out if a redraw is required and to mark the dirty
     * elements clean at the same time.
     *
     * @return true if atleast one element was dirty, false otherwise.
     */
    public boolean resetDirtyFlag() {
        boolean isDirty = false;
        for (int i = 0; i < m_elementsArray.length; i++) {
            OverlayElement oe = m_elementsArray[i].m_overlayElement;
            if (oe.isDirty()) {
                oe.makeClean();
                isDirty = true;
            }
        }
        return isDirty;
    }

    /**
     * This method marks one of the elements dirty.
     */
    public void markAnElementDirty() {
        if (m_elementsArray.length > 0)
            m_elementsArray[0].m_overlayElement.makeDirty();
    }

    protected void updateButtonStates() {
        int i = m_comboBox.getSelectedIndex();

        m_minusButton.setEnabled(i != 0);
        m_plusButton.setEnabled(i != (m_comboBox.getItemCount() - 1));
    }

    /**
     * Initializes this control as a view control.
     *
     * @param controlName unique name/type of this control
     * @param display     MicroscopeView object
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        try {
            if (display == null || display.getCurrentSpecimen() == null || controlName == null) {
                throw new IllegalArgumentException("Null microscope view, specimen, or name");
            }
            m_display = display;
            m_display.setEDSControl(this);// register yourself with the microscopeview.
            if (m_controlName != null) {
                m_controlName = controlName;
            }
            //setLayout(new GridBagLayout());
            setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
            //TODO: The title should come from the specimen. The default application
            // behavior is to use the "Control" Id and change its case to title case and
            // show that as the control title on the GUI. this would mean that the EDS
            // shows as "Eds", which does not convey that EDS is an acronym. Hence we are
            // hardcoding this to "EDS" here.
            m_title = Constants.Controls.EDS;
            // resetting the color pointer so that the new EDS image gets assigned
            // the same colors.
            Constants.OverlayElementColor.m_pointer = 0;
            m_graphHandler.loadEnergySpectrum(m_display.getCurrentSpecimen().getNoiseData());
            m_overlayImageHandler = new OverlayImageHandler(this);
            setBorder(BorderFactory.createTitledBorder(m_title));
            setForeground(Color.BLUE);
            initContainedDetectorControl(display);
            initContainedElementControl(display);
        } catch (Exception ex) {
            throw new IllegalArgumentException(ex.getMessage());
        }
    }

    /**
     * This method initializes the data structures required to draw and handle all the
     * contained controls e.g. the Element checkboxes, etc.
     */
    private void initContainedDetectorControl(MicroscopeView display) {
        /**
         * First we shall get the EDS node and will then get all its DETECTOR children. This
         * information will let us draw the base image control. We shall also get the ELEMENTS
         * contained in each of the DETECTORS, this will help us establish the dependency
         * relationship between the DETECTORS and the ELEMENTs i.e. which ELEMENTS should be
         * shown when a particular DETECTOR is selected.
         */
        ControlStates css = display.getCurrentSpecimen().getRootStates();
        ControlStates cs = null;
        LinkedHashMap sm = css.getStates();
        // First look for the node titled "EDS".
        while (css != null) {
            String key = "0";
            cs = (ControlStates) sm.get(key);
            if (cs.getId().equals(Constants.Controls.EDS)) {
                break;
            } else
                css = cs;
        }
        // We must always find an EDS. if the specimen did not have EDS then this control
        // would never have been created in the first place.
        // We also assume that the immediate child of EDS would always be DETECTOR.
        sm = cs.getStates();
        m_validControlStates = sm.values();
    }

    /**
     * This method initializes the data structures required to draw and handle all the
     * contained controls e.g. the Element checkboxes, etc. This method gets called when the user
     * click the corresponding EDS specimen in the chooser view.
     * Note that the overlay elements are partially loaded during the specimen loading, however
     * unlike other side controls the overlayelements are heavy weight and have associated image
     * data - we use lazy initialization to load such heavy weight data, and only load such data inside
     * this method.
     */
    private void initContainedElementControl(MicroscopeView display) {
        int i = 0;
        Vector elmnts = new Vector();
        Iterator it = m_validControlStates.iterator();
        while (it.hasNext()) {
            Specimen.ControlStates css = (Specimen.ControlStates) it.next();
            LinkedHashMap map = css.getParent().getOverlayMap();
            Iterator it1 = map.values().iterator();
            while (it1.hasNext()) {
                OverlayElement oe = (OverlayElement) it1.next();
                m_graphHandler.loadEnergySpectrum(oe);// loading the csv file data series
                oe.loadOverlayImage(m_display.getCurrentSpecimen());
                OverlayElement level = display.getMicroscope().m_coreApplication.m_config.getOverlayElement(oe.m_id);
                if (level == null) {
                    oe.m_energyLevel = i++;
                    oe.m_shortName = oe.m_id;
                    oe.m_longName = oe.m_id;
                } else {// assign a default value if nothing can be found from the config file.
                    oe.m_shortName = level.m_shortName;
                    oe.m_longName = level.m_longName;
                    oe.m_energyLevel = level.m_energyLevel;
                }
                //System.out.println(oe.m_energyLevel);
                elmnts.add(oe);
            }
            break;
        }
        m_overlayPanel = new Overlay(this.m_microscope.m_coreApplication, elmnts);
    }

    ///////// change this to handle it as per the detectors.
    public void setValidControlStates(Map controlStates, double version) {
        updateGUI(version);
    }

    /**
     * This method is overridden from the base class. This method sets the initial
     * value for the Base Image dropdown. Due to the peculiarities of the "None" base
     * image functionality we had to override the default baseclass implementation.
     */
    public void setCurrentControlState(Collection controlStates, double version) {
        m_currentValue = m_comboBox.getItemAt(0).toString();
    }

    public MicroscopeView getDisplay() {
        return m_display;
    }

    /**
     * This method enables/disables the opacity control. The opacity
     * control should be disabled when the Base Image is "None", since
     * Opacity is meaningless if there is no base image to overlay on.
     *
     * @param enable
     */
    public void enableOpacity(boolean enable) {
        m_slider.setEnabled(enable);
    }

    /**
     * This class manages the graph windows and their associated
     * annotations. Since the graphs and the annotations are linked together
     * we need to make sure that when the annotation is deleted the corresponding
     * graph window also gets deletes and vice versa. This class acheives this
     * linkage. This class handles all three graph modes 1) the new window mode
     * 2) the open grah in same window mode 3) the append graph to same window mode.
     *
     * @author Kashif Manzoor
     */
    class GraphManager implements ActionListener, MouseListener {
        /**
         * This variable holds the label number that will be appended to each label. Everytime
         * the user clicks at a certain point we shall label it and use the same label as the
         * title of the graph. This variable will be appended as postfix to this label.
         */
        int m_pointLabelNumber = 1;
        /**
         * This variable holds the label number that will be appended to each Area label. Everytime
         * the user draws an area we shall label it and use the same label as the
         * title of the Area graph. This variable will be appended as postfix to this label.
         */
        int m_areaLabelNumber = 1;
        /**
         * This variable holds the label number that will be appended to each line label. Everytime
         * the user draws a line we shall label it and use the same label as the
         * title of the Line graph. This variable will be appended as postfix to this label.
         */
        int m_lineLabelNumber = 1;
        /**
         * Holds the list of graph frames and their associated annotations. Keyed
         * with the "Annotation label" - which is guaranteed to be unique.
         */
        private LinkedHashMap m_graphAnnotationMap = new LinkedHashMap();
        /**
         * The latest graph frame corresponding to the most recently popped up
         * graph window.
         */
        private JFrame m_lastGraphFrame;
        /**
         * Determines whether the new graph will be plotted in the same
         * window or will be plotted in a new pop-up window.
         */
        private JCheckBox m_newWindowBox;
        /**
         * Determines whether we shall create new plots in the same window
         * but without wiping off the old plot. This way the new plots will
         * be shown at the bottom of the existing plot.
         * This format may be useful for comparative analysis.
         */
        private JCheckBox m_compareWindowBox;
        /**
         * Holds the JPanel in which holds the plotted graph image.
         */
        private JPanel m_graphPanel;
        /**
         * Holds the JPanel which contains the m_graphPanel.
         */
        private JPanel m_parentGraphPanel;
        /**
         * Holds the controls that appear in the footre of the graph window
         */
        private JPanel m_footerPanel;
        /**
         * The last EDS graph mode. Was it APPEND_MODE/NEW_WINDOW_MODE/EXISTING_WINDOW_MODE ?
         */
        private short m_lastMode;
        /**
         * Stores the value of the 'open in new window' checkbox at the time when the
         * 'append to same window' checkbox was clicked. This is needed so that when
         * the user unchecks the 'append to same window' check box we can restore the
         * original value of 'open in new window'
         */
        private boolean m_wasNewWindowChecked;
        /**
         * The append window information is stored in this variable.
         */
        private LinkedHashMap m_appendWindowMap = new LinkedHashMap();
        /**
         * This variable tells us if the user just pressed the "open in append mode" check box.
         */
        private boolean m_firstUseOfAppend = false;


        /**
         * The default constructor.
         */
        public GraphManager() {
        }


        /**
         * This method draws a point graph in specific window mode.
         *
         * @param dataseries
         */
        public void plotPointGraph(Vector dataseries, Annotation lastAnnotation) {
            if (m_newWindowBox == null || !m_newWindowBox.isSelected()) {
                plotPointInNewWindowMode(dataseries, lastAnnotation);
            } else if (m_newWindowBox.isSelected() && !m_compareWindowBox.isSelected()) {
                plotPointInExistingWindowMode(dataseries, lastAnnotation);
            } else if (m_compareWindowBox.isSelected()) {
                plotPointInAppendMode(dataseries, lastAnnotation);
            }
        }

        /**
         * This method handles the plotting of the Line graph in specific window mode.
         *
         * @param dataseries
         */
        public void plotLineGraph(Vector dataseries, Annotation lastAnnotation) {
            if (m_newWindowBox == null || !m_newWindowBox.isSelected()) {
                plotLineInNewWindowMode(dataseries, lastAnnotation);
            } else if (m_newWindowBox.isSelected() && !m_compareWindowBox.isSelected()) {
                plotLineInExistingWindowMode(dataseries, lastAnnotation);
            } else if (m_compareWindowBox.isSelected()) {
                plotLineInAppendMode(dataseries, lastAnnotation);
            }
        }

        /**
         * This method handles the plotting of the Area graph in specific window mode.
         *
         * @param dataseries
         */
        public void plotAreaGraph(Vector dataseries, Annotation lastAnnotation) {
            // We are finished creating the dataseries that will be plotted. Now we shall move on to
            // actually plotting the data.
            if (m_newWindowBox == null || !m_newWindowBox.isSelected()) {
                plotAreaInNewWindowMode(dataseries, lastAnnotation);
            } else if (m_newWindowBox.isSelected() && !m_compareWindowBox.isSelected()) {
                plotAreaInExistingWindowMode(dataseries, lastAnnotation);
            } else if (m_compareWindowBox.isSelected()) {
                plotAreaInAppendMode(dataseries, lastAnnotation);
            }
        }

        /**
         * This method redraws the graph window. This can be used to refresh
         * the graph window after some changes have been made to it.
         */
        public void refresh(JFrame frame) {
            //frame.removeAll();
            //frame.dispose();
            //frame = null;
            frame.update(frame.getGraphics());
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }

        /**
         * The mouse event handler. We shall delete the graph on which the mouse was pressed from the
         * append window. After deleting the graph we then redraw the append window with adjusted
         * dimensions.
         * This functionality has not been completely developed. It is deisabled for the time being.
         * Time permitting if this is prioritized then we can continue work on the following function.
         *
         * @deprecated
         */
        public void mouseClicked(MouseEvent e) {
            if (true)
                // Disabling the selectively delete graphs in append window feature.
                return;

            // See the source, find out the frame that contains this. Now dive into the
            // append map and find out all the graphs plotted in this frame in order they
            // were plotted. Now dispose off the frame and create a new frame with the
            // windows minus the recently deleted one.

            // TODO: Comeup with a different event and way to delete the graph/panel.
            if (e.getSource() instanceof JPanel && SwingUtilities.isLeftMouseButton(e)) {
                // Delete the graph of the append window at which the user pressed the mouse.
                // Also delete the associated annotation.
                String label = ((JPanel) e.getSource()).getName();
                GraphAppendWindow obj = (GraphAppendWindow) m_appendWindowMap.get(label);
                m_display.getAnnotationControl().removeAnnotation(obj.m_annotation);
                obj.m_parentPanel.remove((JPanel) e.getSource());
                if (obj.m_parentPanel.getComponentCount() <= 1) {
                    // user has deleted all the graphs in the append window.We shall close the window.
                    obj.m_parentFrame.dispose();
                } else {
                    if (obj.m_parentPanel != null)
                        obj.m_parentPanel.update(obj.m_parentPanel.getGraphics());
                    if (obj.m_parentFrame != null)
                        obj.m_parentPanel.update(obj.m_parentFrame.getGraphics());
                }
                e.consume();
            }
        }

        /**
         * This eventhanler handles all the events fired from within the
         * graph window.
         *
         * @param ae the ActionEvent that is fired.
         */
        public void actionPerformed(ActionEvent ae) {
            if (ae.getActionCommand().equals(Locale.AppendToSameWindow(Configuration.getApplicationLanguage()))) {
                // The user wants to append graphs in the same window.
                if (m_compareWindowBox.isSelected()) {
                    m_wasNewWindowChecked = m_newWindowBox.isSelected();
                    m_newWindowBox.setSelected(true);
                    m_newWindowBox.setEnabled(false);
                } else {
                    m_newWindowBox.setEnabled(true);
                    m_newWindowBox.setSelected(m_wasNewWindowChecked);
                }
            }
        }

        /**
         * This method plots the graph in new window mode i.e. each graph
         * will be ploted in a new new window.
         *
         * @param dataseries The dataseries to be plotted
         * @param lastAnnotation
         */
        private void plotPointInNewWindowMode(Vector dataseries, Annotation lastAnnotation) {
            // Graph will be plotted in a new pop-up window.
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries, "Virtual Miscroscope X-ray Spectra (" + Constants.POINT_LABEL_PREFIX + m_pointLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.POINT_LABEL_PREFIX + m_pointLabelNumber);
            drawGraphInNewWindow(Constants.POINT_LABEL_PREFIX + m_pointLabelNumber);
            m_pointLabelNumber++;
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            addGraphAnnotation(m_lastGraphFrame, m_graphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            m_lastMode = Constants.NEW_WINDOW_MODE;
        }

        /**
         * This method plots the graph in existing window mode.
         *
         * @param countVector The dataseries to be plotted
         */
        private void plotPointInExistingWindowMode(Vector dataseries, Annotation lastAnnotation) {
            // Existing window is to be repainted with the new graph data.
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries, "Virtual Miscroscope X-ray Spectra (" + Constants.POINT_LABEL_PREFIX + m_pointLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.POINT_LABEL_PREFIX + m_pointLabelNumber);
            // Passing on the GUI and annotation information so that
            // the deletion of the graph and annotation can be coordinated.
            replaceGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            // physically drawing the graph in the window.
            drawGraphInExistingWindow(Constants.POINT_LABEL_PREFIX + m_pointLabelNumber);
            m_pointLabelNumber++;
            // the last annotation has been removed, now register the new annotation.
            m_graphAnnotationMap.put(lastAnnotation.getAnnotationText(), new GraphAnnotationPair(this, m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox));
            m_lastMode = Constants.EXISTING_WINDOW_MODE;
        }

        /**
         * This method plots the graph in append mode.
         *
         * @param countVector The dataseries to be plotted
         */
        private void plotPointInAppendMode(Vector dataseries, Annotation lastAnnotation) {
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries, "Virtual Miscroscope X-ray Spectra (" + Constants.POINT_LABEL_PREFIX + m_pointLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(lastAnnotation.getAnnotationText());
            m_graphPanel.addMouseListener(this);
            if (m_firstUseOfAppend) {
                ((JPanel) m_parentGraphPanel.getComponent(0)).addMouseListener(this);
            }
            drawGraphInAppendWindow();
            m_pointLabelNumber++;
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            appendGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, m_graphPanel, lastAnnotation, m_newWindowBox);
            m_lastMode = Constants.APPEND_WINDOW_MODE;
        }

        /**
         * This method plots the Area graph in new window mode i.e. each area graph
         * will be ploted in a new new window.
         *
         * @param countVector The dataseries to be plotted
         */
        private void plotAreaInNewWindowMode(Vector dataseries, Annotation lastAnnotation) {
            // Graph will be plotted in a new pop-up window.
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.AREA_LABEL_PREFIX + m_areaLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.AREA_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.AREA_LABEL_PREFIX + m_areaLabelNumber);
            drawGraphInNewWindow(Constants.AREA_LABEL_PREFIX + m_areaLabelNumber);
            m_areaLabelNumber++;
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            addGraphAnnotation(m_lastGraphFrame, m_graphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            m_lastMode = Constants.NEW_WINDOW_MODE;
        }

        /**
         * Plots the new Area graph in the same window.
         *
         * @param dataseries the data to be plotted.
         */
        private void plotAreaInExistingWindowMode(Vector dataseries, Annotation lastAnnotation) {
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.AREA_LABEL_PREFIX + m_areaLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.AREA_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.AREA_LABEL_PREFIX + m_areaLabelNumber);
            replaceGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            drawGraphInExistingWindow(Constants.AREA_LABEL_PREFIX + m_areaLabelNumber);
            m_areaLabelNumber++;
            // done with drawing and replacing the old graph. Now we register the new
            // annotation into our system
            m_graphAnnotationMap.put(lastAnnotation.getAnnotationText(), new GraphAnnotationPair(this, m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox));
            m_lastMode = Constants.EXISTING_WINDOW_MODE;
        }

        /**
         * This method plots the area graph in append mode.
         *
         * @param dataseries The dataseries to be plotted
         */
        private void plotAreaInAppendMode(Vector dataseries, Annotation lastAnnotation) {
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.AREA_LABEL_PREFIX + m_areaLabelNumber + ")",
                    "Energy (KeV)", "X-ray count", Graph.AREA_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(lastAnnotation.getAnnotationText());
            drawGraphInAppendWindow();
            m_areaLabelNumber++;
            appendGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, m_graphPanel, lastAnnotation, m_newWindowBox);
            m_lastMode = Constants.APPEND_WINDOW_MODE;
        }

        /**
         * This method handles the line tool graph drawing in new window.
         *
         * @param dataseries
         */
        private void plotLineInNewWindowMode(Vector dataseries, Annotation lastAnnotation) {
            // Graph will be plotted in a new pop-up window.
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.LINE_LABEL_PREFIX + m_lineLabelNumber + ")",
                    "%Distance", "X-ray count", Graph.MULTI_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.LINE_LABEL_PREFIX + m_lineLabelNumber);
            drawGraphInNewWindow(Constants.LINE_LABEL_PREFIX + m_lineLabelNumber);
            m_lineLabelNumber++;
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            addGraphAnnotation(m_lastGraphFrame, m_graphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            m_lastMode = Constants.NEW_WINDOW_MODE;
        }

        /**
         * This method handles the line tool graph drawing in existing window.
         *
         * @param dataseries
         */
        private void plotLineInExistingWindowMode(Vector dataseries, Annotation lastAnnotation) {
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.LINE_LABEL_PREFIX + m_lineLabelNumber + ")",
                    "%Distance", "X-ray count", Graph.MULTI_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.LINE_LABEL_PREFIX + m_lineLabelNumber);
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            // the last annotation has been removed, now register the new annotation.
            replaceGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox);
            // physically drawing the graph.
            drawGraphInExistingWindow(Constants.LINE_LABEL_PREFIX + m_lineLabelNumber);
            m_lineLabelNumber++;
            // done with drawing and replacing the old graph. Now we register the new
            // annotation into our system
            m_graphAnnotationMap.put(lastAnnotation.getAnnotationText(), new GraphAnnotationPair(this, m_lastGraphFrame, m_parentGraphPanel, lastAnnotation, m_newWindowBox, m_compareWindowBox));
            m_lastMode = Constants.EXISTING_WINDOW_MODE;
        }

        /**
         * This method handles the line tool graph drawing in append mode.
         *
         * @param dataseries
         */
        private void plotLineInAppendMode(Vector dataseries, Annotation lastAnnotation) {
            Graph graph = new Graph(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT, dataseries,
                    "Virtual Miscroscope X-ray Spectra (" + Constants.LINE_LABEL_PREFIX + m_lineLabelNumber + ")",
                    "%Distance", "X-ray count", Graph.MULTI_LINE_GRAPH, m_elementsArray);
            m_graphPanel = graph.m_panel;
            m_graphPanel.setName(Constants.LINE_LABEL_PREFIX + m_lineLabelNumber);
            drawGraphInAppendWindow();
            m_lineLabelNumber++;
            // Passing on the GUI and annotation information to the graph manager so that
            // the deletion of the graph and annotation can be coordinated.
            appendGraphAnnotation(m_lastGraphFrame, m_parentGraphPanel, m_graphPanel, lastAnnotation, m_newWindowBox);
            m_lastMode = Constants.APPEND_WINDOW_MODE;
        }

        /**
         * This method draws the graph in a new window.
         *
         * @param labelNumber The title of the frame.
         */
        private void drawGraphInNewWindow(String frameTitle) {
            int locX = 0;
            int locY = 0;
            if (m_newWindowBox != null) {
                // freezing the controls of the previous window. since only the latest
                // window control values will be entertained.
                m_newWindowBox.setEnabled(false);
                m_compareWindowBox.setEnabled(false);
            }
            if (m_lastGraphFrame != null) {
                locX = (int) m_lastGraphFrame.getLocation().getX() + 10;
                locY = (int) m_lastGraphFrame.getLocation().getY() + 20;
            }
            m_lastGraphFrame = new JFrame(frameTitle);
            m_lastGraphFrame.setSize(new Dimension(Constants.GRAPH_PANEL_WIDTH, Constants.GRAPH_PANEL_HEIGHT));
            m_newWindowBox = new JCheckBox(Locale.OpenInSameWindow(Configuration.getApplicationLanguage()));
            m_compareWindowBox = new JCheckBox(Locale.AppendToSameWindow(Configuration.getApplicationLanguage()));
            m_compareWindowBox.addActionListener(this);
            m_newWindowBox.setVisible(true);
            m_parentGraphPanel = new JPanel();
            m_parentGraphPanel.setBorder(BorderFactory.createEtchedBorder());
            m_parentGraphPanel.setLayout(new BoxLayout(m_parentGraphPanel, BoxLayout.Y_AXIS));
            m_lastGraphFrame.getContentPane().add(m_parentGraphPanel);

            /** Setting up the footer panel */
            m_footerPanel = new JPanel();
            m_footerPanel.setLayout(new BoxLayout(m_footerPanel, BoxLayout.LINE_AXIS));
            m_footerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
            m_footerPanel.add(m_newWindowBox);
            m_footerPanel.add(Box.createRigidArea(new Dimension(10, 0)));
            m_footerPanel.add(m_compareWindowBox);
            m_footerPanel.add(Box.createHorizontalGlue());

            /** Setting up the parent graph panel that holds everything*/
            m_parentGraphPanel.add(m_graphPanel);
            m_parentGraphPanel.add(m_footerPanel);

            /** Make sure the Graph window does not draw beyond screen limits.*/
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if ((locX + m_lastGraphFrame.getPreferredSize().getWidth()) > screenSize.getWidth() ||
                    (locY + m_lastGraphFrame.getPreferredSize().getHeight()) > screenSize.getHeight()) {
                locX = 0;
                locY = 0;
            }
            m_lastGraphFrame.setLocation(locX, locY);
            //m_lastGraphFrame.pack();
            m_lastGraphFrame.setVisible(true);
        }

        /**
         * This method draws the graph in existing window by replacing the old graph.
         *
         * @param frameTitle The title of the frame that contains the graph.
         */
        public void drawGraphInExistingWindow(String frameTitle) {
            // Existing window is to be repainted with the new graph data.
            m_graphPanel.removeAll();
            m_parentGraphPanel.removeAll();
            m_lastGraphFrame.removeAll();
            int locX = 0;
            int locY = 0;
            int originalWidth = Constants.GRAPH_PANEL_WIDTH;
            int originalHeight = Constants.GRAPH_PANEL_HEIGHT;
            if (m_lastGraphFrame != null) {
                originalWidth = m_lastGraphFrame.getWidth();
                originalHeight = m_lastGraphFrame.getHeight();
                locX = (int) m_lastGraphFrame.getLocation().getX();
                locY = (int) m_lastGraphFrame.getLocation().getY();
                m_lastGraphFrame.dispose();
            }
            m_lastGraphFrame = null;

            m_lastGraphFrame = new JFrame(frameTitle);
            m_lastGraphFrame.setSize(new Dimension(originalWidth, originalHeight));

            m_parentGraphPanel.setBorder(BorderFactory.createEtchedBorder());
            m_parentGraphPanel.setLayout(new BoxLayout(m_parentGraphPanel, BoxLayout.Y_AXIS));
            m_lastGraphFrame.getContentPane().add(m_parentGraphPanel);
            m_parentGraphPanel.add(m_graphPanel);
            m_parentGraphPanel.add(m_footerPanel);
            m_lastGraphFrame.setLocation(locX, locY);
//			  m_lastGraphFrame.pack();
            //m_graphPanel.revalidate();
            //m_parentGraphPanel.grabFocus();
            m_lastGraphFrame.setVisible(true);
        }

        /**
         * This method appends the graph to an existing window. The method
         * disposes off the previous frame creates a new frame and appneds the graph
         * to the existing parent panel. The parent Graph panel is the one that
         * is retained from the previous runs and consolidates the previous graphs. The
         * frame gets recreated everytime we append to a given window.
         * TODO: If the user changes the window dimensions then we should respect those dimensions.
         * Right now we always revert back to our defaul values.
         */
        private void drawGraphInAppendWindow() {
            int locX = 0;
            int locY = 0;
            int originalWidth = (int) m_lastGraphFrame.getSize().getWidth();
            if (m_lastGraphFrame != null) {
                locX = (int) m_lastGraphFrame.getLocation().getX();
                locY = (int) m_lastGraphFrame.getLocation().getY();
                m_lastGraphFrame.dispose();
                m_lastGraphFrame = null;
            }

            m_lastGraphFrame = new JFrame("Comparative Analysis");
            if (m_parentGraphPanel.getComponentCount() == 0) {
                m_lastGraphFrame.setSize(new Dimension(originalWidth, Constants.GRAPH_PANEL_HEIGHT));
            } else {
                m_lastGraphFrame.setSize(new Dimension((int) (originalWidth), (int) (Constants.APPEND_WINDOW_HEIGHT_FRACTION * Constants.GRAPH_PANEL_HEIGHT)));
            }
            // Graph will be plotted in a new pop-up window.
            m_parentGraphPanel.setBorder(BorderFactory.createEtchedBorder());
            m_parentGraphPanel.setLayout(new BoxLayout(m_parentGraphPanel, BoxLayout.Y_AXIS));
            m_lastGraphFrame.getContentPane().add(m_parentGraphPanel);

            /** Setting up the footer panel */
            m_parentGraphPanel.remove(m_footerPanel); // clean up the previous footer.
            m_footerPanel = new JPanel();
            m_footerPanel.setLayout(new BoxLayout(m_footerPanel, BoxLayout.LINE_AXIS));
            m_footerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 0, 10));
            m_footerPanel.add(m_newWindowBox);
            m_footerPanel.add(Box.createRigidArea(new Dimension(10, 0)));
            m_footerPanel.add(m_compareWindowBox);
            m_footerPanel.add(Box.createHorizontalGlue());

            /** Setting up the parent panel that holds everything */
            m_parentGraphPanel.add(m_graphPanel);
            m_parentGraphPanel.add(m_footerPanel);
            /** Make sure the Graph window is always visible */
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            if ((locX + m_lastGraphFrame.getPreferredSize().getWidth()) > screenSize.getWidth() ||
                    (locY + m_lastGraphFrame.getPreferredSize().getHeight()) > screenSize.getHeight()) {
                locX = 0;
                locY = 0;
            }
            m_lastGraphFrame.setLocation(locX, locY);
            m_lastGraphFrame.setVisible(true);
        }

        /**
         * This method adds a graph and its annotation to the internally maintained
         * list of graph-annotation pairs.
         *
         * @param frame the pop-up window frame
         * @param annot the associated annotation
         */
        public void addGraphAnnotation(JFrame frame, JPanel panel, Annotation annot, JCheckBox newWindow, JCheckBox appendWindow) {
            m_firstUseOfAppend = true;
            m_graphAnnotationMap.put(annot.getAnnotationText(), new GraphAnnotationPair(this, frame, panel, annot, newWindow, appendWindow));
        }

        /**
         * This method replaces the last graph-annotation pair with the new graph and its annotation.
         * This is called when the user opens a new graph in the same window.
         *
         * @param frame the pop-up window frame
         * @param annot the associated annotation
         */
        public void replaceGraphAnnotation(JFrame frame, JPanel panel, Annotation annot, JCheckBox newWindow, JCheckBox appendWindow) {
            m_firstUseOfAppend = true;
            /**
             * The last frame will be replaced with the new one. So we remove the last frame and
             * add the new frame to our linked hashmap. However we also need to remove any
             * annotations corresponding to the graphs that were contained in the last frame. If the
             * last frame was an append window then it may contain more than one graphs if it was
             * a new window or an existing window frame then it would only contain one graph.
             */
            if (m_lastMode != Constants.APPEND_WINDOW_MODE) {
                Iterator it = m_graphAnnotationMap.keySet().iterator();
                Object o = null;
                String key = "";
                while (it.hasNext()) {
                    key = (String) it.next();
                    o = m_graphAnnotationMap.get(key);
                }
                if (o != null) {
                    m_graphAnnotationMap.remove(key);
                    // Physically remove the annotation.
                    m_display.getAnnotationControl().removeAnnotation(((GraphAnnotationPair) o).m_annotation);
                }
            } else {
                Iterator it = m_appendWindowMap.keySet().iterator();
                Vector toBeDeleted = new Vector();
                // Figure out which graphs and annotations were contained in the
                // frame that was closed by the user.
                while (it.hasNext()) {
                    String key = (String) it.next();
                    GraphAppendWindow obj = (GraphAppendWindow) m_appendWindowMap.get(key);
                    if (obj.m_parentPanel == panel) {
                        toBeDeleted.add(key);
                        // Physically remove all the existing graphs, otherwise they will continue
                        // to show up even when another brand new append window is opened.
                        obj.m_parentPanel.removeAll();
                        //obj.m_parentPanel = null;
                    }
                }
                // Having marked the contained graphs and annotations we now physically
                // delete the annotations.
                for (int i = 0; i < toBeDeleted.size(); i++) {
                    GraphAppendWindow obj = (GraphAppendWindow) m_appendWindowMap.get(toBeDeleted.get(i));
                    m_display.getAnnotationControl().removeAnnotation(obj.m_annotation);
                    m_appendWindowMap.remove(toBeDeleted.get(i));
                }
            }
        }

        /**
         * This method appends the new panel to an existing frame. This is called when the user
         * opens graphs in append mode.
         *
         * @param frame the container JFrame.
         * @param panel the JPanel that will be appended to the frame.
         * @param annot the associated annotation.
         */
        public void appendGraphAnnotation(JFrame frame, JPanel parentPanel, JPanel panel, Annotation annot, JCheckBox close) {
            if (m_firstUseOfAppend) {
                m_firstUseOfAppend = false;
                // if this is the first time we are appending then the last graph would have been
                // added to the m_graphAnnotationMap. As at that time we did not know that the
                // user will switch to append mode. So we need to put the last graph in the correct
                // hash map.
                Iterator it = m_graphAnnotationMap.keySet().iterator();
                GraphAnnotationPair o = null;
                String key = "";
                while (it.hasNext()) {
                    key = (String) it.next();
                    o = (GraphAnnotationPair) m_graphAnnotationMap.get(key);
                }
                // The last graph will be put in the append hashmap.
                m_graphAnnotationMap.remove(key);
                // TODO: Create the new "close" box for the first graph.
                GraphAppendWindow oldWin = new GraphAppendWindow(this, o.m_frame, parentPanel, o.m_graphPanel, o.m_annotation, null);
                m_appendWindowMap.put(o.m_annotation.getAnnotationText(), oldWin);
            }
            GraphAppendWindow append = new GraphAppendWindow(this, frame, parentPanel, panel, annot, close);
            m_appendWindowMap.put(annot.getAnnotationText(), append);
        }

        /**
         * This method deletes the graph annotation pair both from its internal data structure and
         * also physically from the screen
         *
         * @param pair
         */
        public void deleteGraphAnnotationPair(GraphAnnotationPair pair) {
            m_display.getAnnotationControl().removeAnnotation(pair.m_annotation);
            m_graphAnnotationMap.remove(pair.m_annotation.getAnnotationText());
        }

        /**
         * This method takes in a graph that was contained in an append window and
         * deletes ALL the graphs and their associated annotations that were in the
         * append window.
         *
         * @param win
         */
        public void deleteAppnedWindow(GraphAppendWindow win) {
            Iterator it = m_appendWindowMap.keySet().iterator();
            Vector toBeDeleted = new Vector();
            // Figure out which graphs and annotations were contained in the
            // frame that was closed by the user.
            while (it.hasNext()) {
                String key = (String) it.next();
                GraphAppendWindow obj = (GraphAppendWindow) m_appendWindowMap.get(key);
                if (obj.m_parentPanel == win.m_parentPanel) {
                    toBeDeleted.add(key);
                    // Physically remove all the existing graphs, otherwise they will continue
                    // to show up even when another brand new append window is opened.
                    obj.m_parentPanel.removeAll();
                    //obj.m_parentPanel = null;
                }
            }
            // Having marked the contained graphs and annotations we now physically
            // delete the annotations.
            for (int i = 0; i < toBeDeleted.size(); i++) {
                GraphAppendWindow obj = (GraphAppendWindow) m_appendWindowMap.get(toBeDeleted.get(i));
                m_display.getAnnotationControl().removeAnnotation(obj.m_annotation);
                m_appendWindowMap.remove(toBeDeleted.get(i));
            }
        }

        /**
         * This method closes all the graph windows and does other necessary clean up.
         */
        public void cleanUp() {
            Iterator it = m_graphAnnotationMap.keySet().iterator();
            GraphAnnotationPair o = null;
            //String key = "";
            while (it.hasNext()) {
                //key = (String) it.next();
                o = (GraphAnnotationPair) m_graphAnnotationMap.get(it.next());
                o.m_frame.dispose();
            }
            m_display.getAnnotationControl().clearAnnotations();
            m_graphAnnotationMap.clear();

            it = m_appendWindowMap.keySet().iterator();
            GraphAppendWindow o1 = null;
            //String key = "";
            while (it.hasNext()) {
                //key = (String) it.next();
                o1 = (GraphAppendWindow) m_appendWindowMap.get(it.next());
                if (o1.m_parentFrame.isValid())
                    o1.m_parentFrame.dispose();
            }
            m_appendWindowMap.clear();
        }
    }

    /**
     * This class couples the graph frame and its associated annotation. This
     * is a convenient data structure to put all the graph frame and its associated
     * things together.
     *
     * @author Kashif Manzoor.
     */
    class GraphAnnotationPair implements WindowListener {
        /**
         * The frame that contains the graph panel and other GUI controls.
         */
        protected JFrame m_frame;
        /**
         * The annotation associated with the graph.
         */
        protected Annotation m_annotation;
        /**
         * The JPanel that contains the graph.
         */
        private JPanel m_graphPanel;
        /**
         * The graph manager that cotains this frame-annotation pair. This is used
         * to access the manager's method to delete this frame-annotation when the user
         * closes the frame.
         */
        private GraphManager m_parentGraphManager;

        /**
         * The check box that "open in new window" appears at the bottom of the frame
         */
        private JCheckBox m_newWindowCheckBox;
        /**
         * The "append to same window" checkbox that appears at the bottom of the frame.
         */
        private JCheckBox m_appendWindowCheckBox;


        /**
         * The constructor for the class. We shall set the container frame and
         * the associated annotation. We shall also set the window listener so that
         * this class gets the window events e.g. closing (which necessitates a delete
         * of the frame and the annotation).
         *
         * @param frame
         * @param annot
         */
        public GraphAnnotationPair(GraphManager parent, JFrame frame, JPanel graphPanel, Annotation annot, JCheckBox newWindow, JCheckBox appendWindow) {
            m_parentGraphManager = parent;
            m_frame = frame;
            m_graphPanel = graphPanel;
            m_annotation = annot;
            m_newWindowCheckBox = newWindow;
            m_appendWindowCheckBox = appendWindow;
            m_frame.addWindowListener(this);
        }

        public void windowOpened(WindowEvent we) {
        }

        public void windowIconified(WindowEvent we) {
        }

        public void windowDeiconified(WindowEvent we) {
        }

        public void windowActivated(WindowEvent we) {
        }

        public void windowDeactivated(WindowEvent we) {
        }

        public void windowClosed(WindowEvent we) {
        }

        /**
         * This method gets called when the graph window is being closed by pressing the top
         * right "Cross" button. We shall delete the respective Annotation and the JFrame.
         */
        public void windowClosing(WindowEvent we) {
            m_parentGraphManager.deleteGraphAnnotationPair(this);
        }
    }

    /**
     * This class couples the graph Panel and its associated annotation. This
     * is a convenient data structure to put all Panel and the annotation
     * together. This is used to keep track of which graphs are included in the
     * append window. This information is useful while deleting the panels and their
     * associated annotations.
     *
     * @author Kashif Manzoor.
     */
    class GraphAppendWindow implements WindowListener {
        /**
         * The append window frame that contains this panel.
         */
        protected JFrame m_parentFrame;
        /**
         * The append panel that contains all the graphs which appear in the append
         * window.
         */
        protected JPanel m_parentPanel;
        /**
         * The panel that contains the graph.
         */
        protected JPanel m_panel;
        /**
         * The associated annotation.
         */
        protected Annotation m_annotation;
        /**
         * The graph manager that cotains this frame-annotation pair. This is used
         * to access the manager's method to delete this frame-annotation when the user
         * closes the frame.
         */
        private GraphManager m_parentGraphManager;

        /**
         * Holds the close control for the graph
         */

        private JCheckBox m_closeBox;


        /**
         * The constructor for the class. It sets the parent frame, the graph panel and the
         * associated annotation.
         *
         * @param frame parent JFrame
         * @param panel graph JPanel
         * @param annot associated Annotation.
         */
        public GraphAppendWindow(GraphManager parent, JFrame frame, JPanel parentPanel, JPanel panel, Annotation annot, JCheckBox close) {
            m_parentGraphManager = parent;
            m_parentPanel = parentPanel;
            m_parentFrame = frame;
            m_panel = panel;
            m_annotation = annot;
            m_closeBox = close;
            m_parentFrame.addWindowListener(this);
        }

        public void windowOpened(WindowEvent we) {
        }

        public void windowIconified(WindowEvent we) {
        }

        public void windowDeiconified(WindowEvent we) {
        }

        public void windowActivated(WindowEvent we) {
        }

        public void windowDeactivated(WindowEvent we) {
        }

        public void windowClosed(WindowEvent we) {
        }

        /**
         * This method gets called when the graph window is being closed by pressing the top
         * right "Cross" button. We shall delete all the Annotations associated with the
         * graphs drwn in the parent frame. This is because in append mode a single window
         * may contain multiple graphs (and hence associated annotations).
         */
        public void windowClosing(WindowEvent we) {
            m_parentGraphManager.deleteAppnedWindow(this);
        }
    }


    /**
     * This class contains all the logic to handle the overlay elements.
     * It handles the proper drawing of the checkboxes corresponding to
     * these elements in addition to the event based logic (i.e. what happens
     * if the user clicks on a checkbox etc.)
     *
     * @author Kashif Manzoor
     */
    class Overlay extends JPanel {
        /**
         * The constructor
         *
         * @param owner
         * @param elements
         */
        Overlay(JFrame owner, Vector elements) {
            m_elementsArray = new OverlayElementCheckbox[elements.size()];
            for (int i = 0; i < elements.size(); i++) {
                m_elementsArray[i] = new OverlayElementCheckbox(owner, (OverlayElement) elements.get(i), m_display);
            }

            setLayout(new GridBagLayout());
            setBorder(BorderFactory.createTitledBorder(Locale.Elements(Configuration.getApplicationLanguage())));
        }

        /**
         * This method draws the Overlay elements panel and the corresponding checkboxes.
         *
         * @param version
         * @return the JPanel containing the Elements Checkboxes.
         * @throws Exception
         */
        private JPanel updateGUI(double version) {
            if (version <= 1.0) {
                return new JPanel();
            }
            GridBagConstraints c = new GridBagConstraints();
            c.insets = new Insets(0, 0, 0, 0);//(top,left,bottom,right)
            c.anchor = GridBagConstraints.LINE_START;
            c.weightx = 1.0;
            c.weighty = 1.0;
            c.gridheight = 1;
            // Overlay Elements.
            int x = 0, y = 0;
            for (int i = 0; i < m_elementsArray.length; i++) {
                if (i % 3 == 0) {// Draw three checkboxes in a single line.
                    x = 0;
                    y++;
                }
                c.gridx = x;
                c.gridy = y;
                add(m_elementsArray[i], c);
                x++;
            }
            return this;
        }
    }

    /**
     * This class encompasses the logic for drawing the graph from creating the data series to
     * handling the GUI related stuff.
     *
     * @author Kashif Manzoor
     */
    class GraphHandler implements MouseListener {
        /**
         * Has the user right clicked one of the tools buttons ?
         */
        boolean m_persistentGraphMode = false;
        /**
         * Was our last action finishing up the graph mode ?. This is used to ensure that we
         * unselect any annotations. Needed as a work around to ensure that AnnotationControl
         * does not show our annotations as selected.
         */
        boolean m_graphModeJustFinished = false;
        /**
         * The old cursor. We shall revert back to this once we are done with our processong
         */
        private Cursor m_oldCursor;
        /**
         * The Point tool JToggle button
         */
        private JToggleButton m_bttnPoint;
        /**
         * The Line tool JToggle button
         */
        private JToggleButton m_bttnLine;
        /**
         * The Area tool JToggle button
         */
        private JToggleButton m_bttnArea;
        /**
         * Tells us if the Area drwaing is in progress. When this is
         * true we would let the mouse events bypass since now the AnnotationControl
         * is in charge of the events
         */
        private boolean m_areaDrawingInProgress = false;
        private boolean m_pointDrawingInProgress = false;
        /**
         * Tells us if the line is being drawn
         */
        private boolean m_lineDrawingInProgress = false;
        /**
         * Stores the original color so that once we are
         * done drawing in yellow color we can revert back to the
         * original color.
         */
        private Color m_oldColor;
        /**
         * Stores the original Line width so that once we are
         * done drawing in 2.0 line width we can revert back to the
         * original line width.
         */
        private float m_oldLineWidth;

        /**
         * Holds the reference to the last annotation for a graph.
         */
        private Annotation m_lastAnnotation;
        private String m_oldAnnotationSelection;


        public void mouseExited(MouseEvent e) {
        }

        /**
         * Handles the mouse click events generated by the tools buttons.
         *
         * @param MouseEvent
         */
        public void mouseClicked(MouseEvent e) {
            /**
             * This will allow the annotation toolto switchoff the
             * persistent mode when an annotation tool is selected while
             * still in EDS persisten mode
             */
            m_display.getAnnotationControl()
                    .addAnnotationControlChangeListener(EDSMenu.this);
            boolean rightButtonClicked = SwingUtilities.isRightMouseButton(e);
            if (e.getSource() instanceof JToggleButton) {
                JToggleButton btn = (JToggleButton) e.getSource();
                if (btn.getName().equals(Constants.POINT_TOOL)) {
                    if (m_bttnArea.isSelected()) {
                        m_bttnArea.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the
                        // cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (m_bttnLine.isSelected()) {
                        m_bttnLine.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (rightButtonClicked)
                        // have to do this since on left button press the toggle button's default
                        // event handler would already have changed its state but on right click
                        // the change in state would not have happened.
                        btn.setSelected(!btn.isSelected());
                    if (!btn.isSelected()) {
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    } else {
                        m_oldCursor = m_display.getCursor();
                        //m_display.setCursor(Cursor.CROSSHAIR_CURSOR);
                        m_display.setCursor(Utility.createPointCursor());
                        // making sure that no other module resets the cursor while we are doing our stuff.
                        m_display.setResetCursorFlag(false);
                        if (rightButtonClicked)
                            m_persistentGraphMode = true;
                    }
                } else if (btn.getName().equals(Constants.AREA_TOOL)) {
                    if (m_bttnPoint.isSelected()) {
                        m_bttnPoint.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (m_bttnLine.isSelected()) {
                        m_bttnLine.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (rightButtonClicked)
                        // have to do this since on left button press the toggle button's default
                        // event handler would already have changed its state but on right click
                        // the change in state would not have happened.
                        btn.setSelected(!btn.isSelected());
                    if (!btn.isSelected()) {
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    } else {
                        m_oldCursor = m_display.getCursor();
                        m_display.setCursor(Cursor.HAND_CURSOR);
                        //m_display.setCursor(Utility.createPointCursor());
                        // making sure that no other module resets the cursor while we are doing our stuff.
                        m_display.setResetCursorFlag(false);
                        if (rightButtonClicked)
                            m_persistentGraphMode = true;
                    }
                } else if (btn.getName().equals(Constants.LINE_TOOL)) {
                    if (m_bttnPoint.isSelected()) {
                        m_bttnPoint.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (m_bttnArea.isSelected()) {
                        m_bttnArea.setSelected(false);
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    }
                    if (rightButtonClicked)
                        // have to do this since on left button press, the toggle button's default
                        // event handler would already have changed its state but on right click
                        // the change in state would not have happened.
                        btn.setSelected(!btn.isSelected());
                    if (!btn.isSelected()) {
                        m_display.setCursor(m_oldCursor);
                        // Now other modules can do whatever they wish with the cursor.
                        m_display.setResetCursorFlag(true);
                        m_persistentGraphMode = false;
                    } else {
                        m_oldCursor = m_display.getCursor();
                        m_display.setCursor(Cursor.E_RESIZE_CURSOR);
                        //m_display.setCursor(Utility.createPointCursor());
                        // making sure that no other module resets the cursor while we are doing our stuff.
                        m_display.setResetCursorFlag(false);
                        if (rightButtonClicked)
                            m_persistentGraphMode = true;
                    }
                }
            }
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

        /**
         * This method returns true if any of the graph tools is selected.
         *
         * @return iff at least one of the graph tool button is pressed.
         */
        public boolean isAnyToolSelected() {
            return (m_bttnArea.isSelected() | m_bttnLine.isSelected() | m_bttnPoint.isSelected());
        }

        /**
         * This method reads the csv file corresponding to the specified element and loads
         * the energy spectrum of the given element. The enrgy spectrum is stored as a
         * CSV file.
         * The CSV file contains 1000 data points starting from (0KeV to 10KeV)
         * in .01 increments. Majority of these values are zero except of the values
         * in the vacinity of the energy peak of the element. If the file could not
         * be located, the baseGraphdata is set to an empty vector.
         *
         * @param oe The overlay element whose data is to be loaded.
         */
        private void loadEnergySpectrum(OverlayElement oe) {
            try {
                Vector vt = Utility.readRecordFile(",", getClass().getResourceAsStream(Constants.ELEMENT_GRAPH_DATA_FILE_LOCATION + oe.m_id + Constants.ELEMENT_GRAPH_DATA_FILE_EXTENSION));
                if (vt != null) {
                    oe.m_baseGraphData = (Vector) vt.get(0);
                    // Setting the X-Axis values.
                    if (oe.m_baseGraphData != null) {
                        float increment = (float) (Constants.ENERGY_LEVEL_UPPER_LIMIT - Constants.ENERGY_LEVEL_LOWER_LIMIT) / oe.m_baseGraphData.size();
                        float x = Constants.ENERGY_LEVEL_LOWER_LIMIT;
                        for (int i = 0; i < oe.m_baseGraphData.size(); i++) {
                            ((Point2D.Float) oe.m_baseGraphData.get(i)).x = x;
                            x += increment;
                        }
                    }
                } else
                    oe.m_baseGraphData = new Vector();
            } catch (Exception ex) {
                oe.m_baseGraphData = new Vector();
            }
        }

        /**
         * This method handles the graph plot when the "Point" tool is selected.
         * For the point tool we plot the graph based on the xray count at the pixel
         * where the mouse was clicked.
         *
         * @param mouseX the mouse click X-Axis positon
         * @param mouseY the mouse click Y-Axis positon
         */
        public void handlePointPlot(int mouseX, int mouseY) {
            // We are finished creating the dataseries that will be plotted. Now we shall move on to
            // actually plotting the data.
            m_graphManager.plotPointGraph(calculatePointDataSeries(mouseX, mouseY), m_lastAnnotation);
        }

        /**
         * This method handles the Line plot.
         *
         * @param Line2D.Double the drawn line.
         */
        public void handleLinePlot(Line2D.Double line) {
            // We are finished creating the dataseries that will be plotted. Now we shall move on to
            // actually plotting the data.
            m_graphManager.plotLineGraph(calculateLineDataSeries(line), m_lastAnnotation);
            if (!m_persistentGraphMode) {
                m_bttnLine.setSelected(false);
                m_graphModeJustFinished = true;
                m_display.setCursor(m_oldCursor);
            }
        }

        /**
         * This method calculates the dataseries for the line plot. We first
         * convert the line into the equivalent line in the overlay space i.e.
         * the corresponding line in the overlay images. Then we use this new line
         * to find out the pixels in the overlay images that plot on this line.
         * From there we get the XRay count and formulate our dataseries.
         *
         * @param Line2D.Double the user drawn line.
         */
        public Vector calculateLineDataSeries(Line2D.Double line) {
            Vector vt = new Vector();
            double x1 = line.getP1().getX();
            double y1 = line.getP1().getY();
            double x2 = line.getP2().getX();
            double y2 = line.getP2().getY();
            /**
             * We shall try to reduce the error by selecting whether to calculate
             * "x" given "y" or viceversa. The decision will be based on the number
             * or points available to plot. Consider a very long line that starts
             * from P(x1,y1):(1,2) and ends at P(x2,y2):(2,100). If we traverse
             * along X-axis we shall only get two points (corresponding to X=1 and X=2)
             * but if we traverse along y-axis we shall have 98 points to plot. Although
             * their X-axis values will hover between 1 and 2 but since our overlay images
             * are of different size than the base image we may still get better results
             * with this approach. This whole mumbo jumbo is simply aimed at reducing the
             * averaging error as much as conveniently possible.
             * Also note that additionally we have two options here: Either to do all the calculations
             * in the base image dimension and then translate into the overlay image or to first
             * translate the line in overlay image and do the rest of the calculations there.
             * We tried with both and the annotations have been implemented the second approach is
             * better. With the first approach we ended up getting more sample points for base images
             * with high mag even when the line had the same apparent length. i.e. a line in
             * 225x mag and the line in 445x mag despite having the same apparent length will result
             * in twich the number of sample points in the graph.
             */

            // Changing to the overlay image dimensions. i.e. Convert the pixels into
            // the corresponding location in the overlay maps.
            x1 = (int) (m_display.getCurrentImageSet().getPixelSize().getX() * x1 / getPixelSize().getX());
            y1 = (int) (m_display.getCurrentImageSet().getPixelSize().getY() * y1 / getPixelSize().getY());
            x2 = (int) (m_display.getCurrentImageSet().getPixelSize().getX() * x2 / getPixelSize().getX());
            y2 = (int) (m_display.getCurrentImageSet().getPixelSize().getY() * y2 / getPixelSize().getY());

            // horizontal and vertical lines.
            if (x1 == x2) {
                //vertical line. Slope is undefined
                for (double i = Math.min(y1, y2); i < Math.max(y1, y2); i++) {
                    Point2D.Double pt = new Point2D.Double(x1, i);
                    vt.add(pt);
                }
            } else if (y1 == y2) {
                //horizontal line. Slope is zero.
                for (double i = Math.min(x1, x2); i < Math.max(x1, x2); i++) {
                    Point2D.Double pt = new Point2D.Double(i, y1);
                    vt.add(pt);
                }
            } else {
                double m = (y2 - y1) / (x2 - x1);
                double c = y1 - (m * x1);
                if (Math.abs(y2 - y1) < Math.abs(x2 - x1)) {
                    // More X-axis values, increment along X-Axis.
                    for (double i = Math.min(x1, x2); i <= Math.max(x1, x2); i++) {
                        double y = m * i + c;
                        Point2D.Double pt = new Point2D.Double(i, y);
                        vt.add(pt);
                    }
                } else {
                    // more Y-axis values, increment along Y-Axis.
                    for (double i = Math.min(y1, y2); i <= Math.max(y1, y2); i++) {
                        double x = (i - c) / m;
                        Point2D.Double pt = new Point2D.Double(x, i);
                        vt.add(pt);
                    }
                }
            }

            /** So far we have only figured out the pixels that plot on the drawn line.
             * Now we need to get the XRay counts of all these pixels. Additionally
             * we need to do this for all the selected elements. Note that the Line tool
             * is the only graph tool that considers the selected elements. Both the Point
             * and the Area tool plot the graphs for all the elements irrespective of whether
             * their check boxes are selected or not.
             */
            Vector vte = getSelectedOverlayElements();
            vte = sortOverlayElements(vte);
            for (int i = 0; i < vte.size(); i++) {
                OverlayElement oel = (OverlayElement) vte.get(i);
                oel.addToLineGraphPoints(vt);
            }
            return vte;
        }

        /**
         * This method handles the graph plot when the "Area" tool is selected.
         * For the Area tool we plot the graph based on the total xray count
         * from all the overlay pixels that fall inside the drawn area. We are given
         * the area shape and we then determine which pixels in the overlay
         * image fall inside this area. Having done that we then add up the xray counts
         * of ALL the pixels in this area for the respective elements.
         *
         * @param GeneralPath the area shape given as a GeneralPath Object.
         */
        public void handleAreaPlot(GeneralPath area) {
            m_graphManager.plotAreaGraph(calculateAreaDataSeries(area), m_lastAnnotation);
        }


        /**
         * Given the energy level x-ray counts for each of the element, this method
         * creates the complete Dataseries.
         *
         * @param mouseX the X-axisof mouse clicked location
         * @param mouseY the Y-axisof mouse clicked location
         * @return The vector containing the Point2D.Float with X-axis and Y-axis values to be plotted on the graph.
         */
        private Vector calculatePointDataSeries(int mouseX, int mouseY) {
            /**
             * The user clicks on the image at a certain location. Our taks
             * is to find out the xray count at that point. To do this we first
             * find out the micron distance into the base image at which the mouse
             * was clicked.
             * This requires multiplying with the PixelSize in micron with the pixel
             * location at which the mouse was clicked. Once we do that we
             * find out the distance in the base image (in micron) where the mouse
             * was clicked. However what we need is the PIXEL location in the OVERLAY
             * image. So we reconvert this micron distance into pixel by dividing it
             * with PixelSize in micron. But this time we divide it with the PixelSize
             * of the OVERLAY image. This gives us the corresponding pixel in the
             * overlay image at which the mouse is clicked.
             */
            Vector countVector = new Vector();
            double x = mouseX - m_display.getStagePosition().x;
            double y = mouseY - m_display.getStagePosition().y;
            if (x < 0)
                x = 0;
            if (y < 0)
                y = 0;
            // Calculate the distance into the base image.
            x = x * m_display.getCurrentImageSet().getPixelSize().getX();
            y = y * m_display.getCurrentImageSet().getPixelSize().getY();
            //Iterator it = m_overlapElements.keySet().iterator();
            for (int j = 0; j < m_elementsArray.length; j++) {
                boolean bEntered = false;
                OverlayElement oe = m_elementsArray[j].m_overlayElement;
                // Find out the corresponding distance into the overlay image.
                // This will give us the pixel location corresponding to the overlay
                // image where the mouse has been clicked.
                float xx = (float) (x / oe.m_pixelSize);
                float yy = (float) (y / oe.m_pixelSize);
                // Apply limits checks when the user clicks outside
                // the image.
                if (xx >= oe.m_pixelBrightnessValues.length)
                    xx = oe.m_pixelBrightnessValues.length - 1;
                if (yy >= oe.m_pixelBrightnessValues[0].length)
                    yy = oe.m_pixelBrightnessValues[0].length - 1;
                oe.setSelectedIndex(Math.round(xx), Math.round(yy));
                for (int i = 0; i < countVector.size(); i++) {
                    OverlayElement oe1 = (OverlayElement) countVector.get(i);
                    if (oe1.m_energyLevel > oe.m_energyLevel) {
                        countVector.insertElementAt(oe, i);
                        bEntered = true;
                        break;
                    }
                }
                if (!bEntered) {
                    // This is the largest of all the energy levels encountered so far.
                    countVector.add(oe);
                }
            }
            return generateConsolidatedDataSeries(countVector, Constants.POINT_TOOL);
        }


        /**
         * This method creates the data series to be plotted for the Area Plot.
         *
         * @param The path representing the area from which the xray count needs to be plotted.
         * @return the vector containing the Overlayelements along with their consolidated Xray counts
         * form within the selected area. the vector is sorted in ascending order of the elements energy
         * levels.
         */
        private Vector calculateAreaDataSeries(GeneralPath area) {
            Vector values = new Vector();
            OverlayElement obj[] = new OverlayElement[m_elementsArray.length];
            // We could exhaustively test each pixel in the overlay image to see
            // if it is inside the area or not. But an optimization could be done
            // to restrict the search to only the pixels that fall with in the bounding
            // rectangle of the area.
            // Note that the bounding rectangle is already converted into coordinates w.r.t
            // the base image and not w.r.t the current viewport. Therefore we need not
            // convert it anymore.

            float minX = (float) (m_display.getCurrentImageSet().getPixelSize().getX() * area.getBounds2D().getMinX());
            float minY = (float) (m_display.getCurrentImageSet().getPixelSize().getY() * area.getBounds2D().getMinY());
            float maxX = (float) (m_display.getCurrentImageSet().getPixelSize().getX() * area.getBounds2D().getMaxX());
            float maxY = (float) (m_display.getCurrentImageSet().getPixelSize().getY() * area.getBounds2D().getMaxY());
            // Figure out the pixel into the overlay image that corresponds to the
            // area bounds.
            for (int i = 0; i < m_elementsArray.length; i++) {
                OverlayElement oe = m_elementsArray[i].m_overlayElement;
                int lowx = Math.round((float) (minX / oe.m_pixelSize));
                int lowy = Math.round((float) (minY / oe.m_pixelSize));
                int highx = Math.round((float) (maxX / oe.m_pixelSize));
                int highy = Math.round((float) (maxY / oe.m_pixelSize));
                if (lowx < 0)
                    lowx = 0;
                if (lowy < 0)
                    lowy = 0;
                if (highx >= oe.m_pixelBrightnessValues.length)
                    highx = oe.m_pixelBrightnessValues.length - 1;
                if (highy >= oe.m_pixelBrightnessValues[0].length)
                    highy = oe.m_pixelBrightnessValues[0].length - 1;
                float xrayCount = 0;
                for (int j = lowx; j < highx; j++) {
                    for (int k = lowy; k < highy; k++) {
                        // Converting back to the original shape coordinates for inside check.
                        double bigX = j * oe.m_pixelSize / m_display.getCurrentImageSet().getPixelSize().getX();
                        double bigY = k * oe.m_pixelSize / m_display.getCurrentImageSet().getPixelSize().getY();
                        if (area.contains(bigX, bigY)) {
                            xrayCount += (float) oe.m_pixelBrightnessValues[j][k] / (float) OverlayElement.MAX_BRIGHTNESS_VALUE;
                        }
                    }
                }
                oe.m_areaXrayCount = xrayCount;
                obj[i] = oe;
            }
            Arrays.sort(obj);
            for (int jj = 0; jj < obj.length; jj++) {
                values.add(obj[jj]);
            }
            return generateConsolidatedDataSeries(values, Constants.AREA_TOOL);
        }

        /**
         * This method uses appropruate helper methods to calculate the Graph Data for Area
         * and/or Point tools.
         *
         * @param oe
         * @param graphType
         * @return
         */
        private Vector calculateGraphData(OverlayElement oe, String graphType) {
            if (graphType.equals(Constants.AREA_TOOL))
                return calculateAreaGraphData(oe);
            else if (graphType.equals(Constants.POINT_TOOL))
                return calculatePointGraphData(oe);
            else
                return new Vector();
        }

        /**
         * This method considers the selected index and calculates the graph data based on
         * the X-ray count of the selected index and the base graph data that is loaded from the
         * csv file.
         */
        public Vector calculatePointGraphData(OverlayElement oe) {
            Vector vt = new Vector(oe.m_baseGraphData.size());
            /**
             * We shall multiply each of the base graph data value with the X-ray count value
             * to get the actual spectra at the specified point.
             */
            for (int i = 0; i < oe.m_baseGraphData.size(); i++) {
                Point2D.Float pt = (Point2D.Float) oe.m_baseGraphData.get(i);
                Point2D.Float pt1 = new Point2D.Float(pt.x, pt.y);
                pt1.y = (float) oe.m_scale * (float) pt1.getY() * ((float) oe.m_pixelBrightnessValues[oe.m_selectedIndex.x][oe.m_selectedIndex.y] / (float) OverlayElement.MAX_BRIGHTNESS_VALUE);
                vt.add(pt1);
            }
            return vt;
        }

        /**
         * This method considers the area xray count and calculates the graph data based on
         * the total X-ray count of the selected area and the base graph data that is loaded from the
         * csv file.
         */
        public Vector calculateAreaGraphData(OverlayElement oe) {
            Vector vt = new Vector(oe.m_baseGraphData.size());
            /**
             * We shall multiply each of the base graph data value with the X-ray count value
             * to get the actual spectra at the specified point.
             */
            for (int i = 0; i < oe.m_baseGraphData.size(); i++) {
                Point2D.Float pt = (Point2D.Float) oe.m_baseGraphData.get(i);
                Point2D.Float pt1 = new Point2D.Float(pt.x, pt.y);
                pt1.y = (float) oe.m_scale * (float) pt1.getY() * (float) oe.m_areaXrayCount;
                vt.add(pt1);
            }
            return vt;
        }

        /**
         * This method takes in a sorted vector (sorted in ascending order of elements energy level) and
         * creates a consolidated data series. The return value is a vector whose elements are Point2D.Float
         * whose x value indicates the X-axis value and whose 'y' value indicates the Y-Axis value
         * of the graph.
         *
         * @param overlayElements the sorted vector containing the overlay elements that are to be included
         *                        in the graph.
         * @param determines      if the point graph is being drawn or an area graph is being drawn.
         * @return a vector whose elements are Point2D.Float whose x value indicates
         * the X-axis value and whose 'y' value indicates the Y-Axis value of the graph.
         */
        protected Vector generateConsolidatedDataSeries(Vector overlayElements, String strType) {
            Vector dataseries = new Vector();
            float maxPeak = 1.0f;// what is the maximum peak height ?
            /** Add up the X-ray count value of all the elements for a given point. */
            for (int l = 0; l < overlayElements.size(); l++) {
                OverlayElement oe = (OverlayElement) overlayElements.get(l);
                Vector oevt = calculateGraphData(oe, strType);
                for (int i = 0; i < oevt.size(); i++) {
                    if (dataseries.size() < (i + 1)) {
                        // We shall get into this condition only for the first element. After
                        // that we would always get into else.
                        dataseries.add(oevt.get(i));
                    } else {
                        Point2D.Float pt = (Point2D.Float) dataseries.get(i);
                        pt.y += ((Point2D.Float) oevt.get(i)).getY();
                        if (pt.y > maxPeak)
                            maxPeak = pt.y;
                    }
                }
            }
            if (!strType.equals(Constants.AREA_TOOL))
                maxPeak = 1.0f;// only adjust the noise peak for Area tool.
            /************* Adding the noise data. ***************/
            if (m_display.getCurrentSpecimen().getNoiseData() != null &&
                    m_display.getCurrentSpecimen().getNoiseData().m_baseGraphData.size() == dataseries.size()) {
                for (int i = 0; i < dataseries.size(); i++) {
                    OverlayElement oe = m_display.getCurrentSpecimen().getNoiseData();
                    Point2D.Float pt = (Point2D.Float) dataseries.get(i);
                    pt.y = pt.y + (float) ((Point2D.Float) oe.m_baseGraphData.get(i)).getY() * (float) oe.m_scale * maxPeak;
                }
            }
            return dataseries;
        }

        /**
         * This method handles the graph plotting when the user clicks
         * the mouse in the viewport.
         *
         * @param e
         */
        public void handleMousePressed(MouseEvent e) {

            if (m_bttnPoint.isSelected()) {
                e.consume();
                m_pointDrawingInProgress = true;
				/*  drawPointAnnotation(e);
				  e.consume();
				  handlePointPlot(e.getX(), e.getY());

				  if (!m_persistentGraphMode){
					  m_bttnPoint.setSelected(false);
					  m_graphModeJustFinished = true;
					  m_display.setCursor(m_oldCursor);
				  }
			  */
            }
            /*else */
            if (m_bttnArea.isSelected()) {
                // start the area drawing.
                m_areaDrawingInProgress = true;
                m_oldColor = m_display.getAnnotationControl().getCurrentColor();
                m_oldLineWidth = m_display.getAnnotationControl().getCurrentLineWidth();
                // We fool the annotation control into thinking that
                // it is being activated with the area tool selected. We
                // make sure we do not consume this event, hence the annotation
                // control will receive the event and it will take over.
                m_display.getAnnotationControl().setCurrentColor(Color.YELLOW);
                m_display.getAnnotationControl().setCurrentLineWidth(2.0f);
                m_display.getAnnotationControl().shouldDrawReadonlyAnnotation(true);
                m_oldAnnotationSelection = m_display.getAnnotationControl().selectTool(Constants.ANNOTATION_PEN_TOOL);
            } else if (m_bttnLine.isSelected()) {
                // start the Line drawing.
                m_lineDrawingInProgress = true;
                m_oldColor = m_display.getAnnotationControl().getCurrentColor();
                m_oldLineWidth = m_display.getAnnotationControl().getCurrentLineWidth();
                // We fool the annotation control into thinking that
                // it is being activated with the line tool selected. We
                // make sure we do not consume this event, hence the annotation
                // control will receive the event and it will take over.
                m_display.getAnnotationControl().setCurrentColor(Color.YELLOW);
                m_display.getAnnotationControl().setCurrentLineWidth(2.0f);
                m_display.getAnnotationControl().shouldDrawReadonlyAnnotation(true);
                m_oldAnnotationSelection = m_display.getAnnotationControl().selectTool(Constants.ANNOTATION_LINE_TOOL);
            }
        }

        /**
         * This method handles the graph plotting when the user releases
         * the mouse in the viewport.
         *
         * @param e
         */
        public void handleMouseReleased(MouseEvent e) {
            if (m_bttnPoint.isSelected() && m_pointDrawingInProgress) {
                m_pointDrawingInProgress = false;
                drawPointAnnotation(e);
                //e.consume();
                handlePointPlot(e.getX(), e.getY());
                if (!m_persistentGraphMode) {
                    m_bttnPoint.setSelected(false);
                    m_graphModeJustFinished = true;
                    m_display.setCursor(m_oldCursor);
                }
            }
            if (m_bttnArea.isSelected() && m_areaDrawingInProgress) {
                // finished drawing the area.
                m_areaDrawingInProgress = false;
                Cursor oldCur = m_display.getCursor();
                m_display.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                // The Annotation Control would have drawn the area
                // for us. We only need to close the path for the drawn
                // shape and undo some changes and our area shape will be ready for us.
                // The annotation control wouldn't even have noticed what hit it.
                Annotation drawnAnnotation = m_display.getAnnotationControl().getDrawnAnnotation();
                ((GeneralPath) drawnAnnotation.getOrigShape()).closePath();
                m_display.getAnnotationControl().setCurrentColor(m_oldColor);
                m_display.getAnnotationControl().setCurrentLineWidth(m_oldLineWidth);
                drawnAnnotation.setAnnotationText(Constants.AREA_LABEL_PREFIX + m_graphManager.m_areaLabelNumber);
                drawnAnnotation.makeUneditable();
                drawnAnnotation.makeUnpersistable();
                m_lastAnnotation = drawnAnnotation;
                drawnAnnotation.unselectAnnotation();
                m_display.getAnnotationControl().selectTool(m_oldAnnotationSelection);
                handleAreaPlot((GeneralPath) drawnAnnotation.getOrigShape());
                m_display.setCursor(oldCur);
                if (!m_persistentGraphMode) {
                    m_bttnArea.setSelected(false);
                    m_graphModeJustFinished = true;
                    m_display.setCursor(m_oldCursor);
                } else
                    m_display.setCursor(Cursor.HAND_CURSOR);
            } else if (m_bttnLine.isSelected() && m_lineDrawingInProgress) {
                // We are finished drawing the Line.
                m_lineDrawingInProgress = false;
                Cursor oldCur = m_display.getCursor();
                m_display.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                // The Annotation Control would have drawn the line
                // for us. We only need to undo some changes and our line shape
                // will be ready for us.
                Annotation drawnAnnotation = m_display.getAnnotationControl().getDrawnAnnotation();
                m_display.getAnnotationControl().setCurrentColor(m_oldColor);
                m_display.getAnnotationControl().setCurrentLineWidth(m_oldLineWidth);
                drawnAnnotation.setAnnotationText(Constants.LINE_LABEL_PREFIX + m_graphManager.m_lineLabelNumber);
                drawnAnnotation.setTextOffset(0, -5);
                drawnAnnotation.makeUneditable();
                drawnAnnotation.makeUnpersistable();
                m_lastAnnotation = drawnAnnotation;
                m_display.getAnnotationControl().unselectAnnotations();
                m_display.getAnnotationControl().selectTool(m_oldAnnotationSelection);
                handleLinePlot((Line2D.Double) drawnAnnotation.getOrigShape());
                m_display.setCursor(oldCur);
                if (!m_persistentGraphMode) {
                    m_bttnLine.setSelected(false);
                    m_graphModeJustFinished = true;
                    m_display.setCursor(m_oldCursor);
                } else
                    m_display.setCursor(Cursor.E_RESIZE_CURSOR);
            }
        }

        /**
         * This method annotates the viewport. The annotation is made
         * at the point where the mouse was clicked, and it corresponds to the graph
         * pop-up window. The annotation will also be set as the graph window's title
         * which will allow the user to match the label with the graph window
         * telling him exactly which plot corresponds to which point in the viewport.
         */
        private void drawPointAnnotation(MouseEvent e) {
            float x = (float) e.getX() - (float) m_display.getStagePosition().getX();
            float y = (float) e.getY() - (float) m_display.getStagePosition().getY();

            double horDia = m_elementsArray[0].m_overlayElement.m_pixelSize /
                    m_display.getCurrentImageSet().getPixelSize().getX();
            double verDia = m_elementsArray[0].m_overlayElement.m_pixelSize /
                    m_display.getCurrentImageSet().getPixelSize().getY();
            Ellipse2D.Double circle = new Ellipse2D.Double(x, y, horDia, verDia);
            Annotation circ = new Annotation(true, m_display.getCurrentSpecimen().getUniqueName(), circle,
                    Color.YELLOW,
                    Color.YELLOW,
                    m_display.getAnnotationControl().getCurrentFontName(),
                    2.0f,
                    m_display.getCurrentImageSet().getImageSize(), null);

            circ.setAnnotationText(Constants.POINT_LABEL_PREFIX + m_graphManager.m_pointLabelNumber);
            circ.setTextOffset(7, -2);
            circ.makeUneditable();
            circ.makeUnpersistable();
            m_display.getAnnotationControl().addAnnotation(circ);
            m_display.getAnnotationControl().unselectAnnotations();
            m_lastAnnotation = circ;
        }


        /**
         * This method draws the Tool panel.
         *
         * @param version The XML version.
         * @return The JPanel containing all the graph plot tools controls.
         */
        public JPanel updateToolGUI(double version) {
            Insets bttnMargins = new Insets(0, 7, 1, 7);
            JPanel toolPanel = new JPanel();
            toolPanel.setLayout(new BoxLayout(toolPanel, BoxLayout.LINE_AXIS));
            toolPanel.setBorder(BorderFactory.createTitledBorder(Locale.Tools(Configuration.getApplicationLanguage())));

            m_bttnPoint = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_ESD_POINT)));
            m_bttnPoint.setSelectedIcon(new ImageIcon(getClass().getResource(Constants.Resources.SELECTED_ESD_POINT)));
            m_bttnPoint.setToolTipText("<html>"+Locale.PointSelectionTool(Configuration.getApplicationLanguage())+
                    ".<br><i>"+Locale.RightClick(Configuration.getApplicationLanguage())+
                    "</i> "+Locale.ForPersistentMode(Configuration.getApplicationLanguage()).toLowerCase()+
                    "<br><i>"+Locale.LeftClick(Configuration.getApplicationLanguage())+
                    "</i> "+Locale.ForVolatileMode(Configuration.getApplicationLanguage()).toLowerCase()+"</html>");
            m_bttnPoint.setName(Constants.POINT_TOOL);
            m_bttnPoint.setBorderPainted(false);
            m_bttnPoint.setContentAreaFilled(false);
            m_bttnPoint.setMargin(bttnMargins);
            //TODO: Decide which class is more appropriate for handling events. its not good that we have two
            // different classes handling graph relatedevents.
            m_bttnPoint.addActionListener(m_graphManager);
            m_bttnPoint.addMouseListener(m_graphHandler);

            m_bttnArea = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_ESD_AREA)));
            m_bttnArea.setSelectedIcon(new ImageIcon(getClass().getResource(Constants.Resources.SELECTED_ESD_AREA)));
            m_bttnArea.setToolTipText("<html>"+Locale.AreaSelectionTool(Configuration.getApplicationLanguage())+
                    ".<br><i>"+Locale.RightClick(Configuration.getApplicationLanguage())+
                    "</i> "+Locale.ForPersistentMode(Configuration.getApplicationLanguage()).toLowerCase()+
                    "<br><i>"+Locale.LeftClick(Configuration.getApplicationLanguage())+
                    "</i> "+Locale.ForVolatileMode(Configuration.getApplicationLanguage()).toLowerCase()+"</html>");
            m_bttnArea.setName(Constants.AREA_TOOL);
            m_bttnArea.setBorderPainted(false);
            m_bttnArea.setContentAreaFilled(false);
            m_bttnArea.setMargin(bttnMargins);
            m_bttnArea.addActionListener(m_graphManager);
            m_bttnArea.addMouseListener(m_graphHandler);

            m_bttnLine = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_ESD_LINE)));
            m_bttnLine.setSelectedIcon(new ImageIcon(getClass().getResource(Constants.Resources.SELECTED_ESD_LINE)));
            m_bttnLine.setToolTipText("<html>"+Locale.LineSelectionTool(Configuration.getApplicationLanguage())+
                    ".<br><i>"+Locale.RightClick(Configuration.getApplicationLanguage())+
                    "</i> "+Locale.ForPersistentMode(Configuration.getApplicationLanguage()).toLowerCase()+
                    "<br><i>"+Locale.LeftClick(Configuration.getApplicationLanguage())+"" +
                    "</i> "+Locale.ForVolatileMode(Configuration.getApplicationLanguage())+"</html>");
            m_bttnLine.setName(Constants.LINE_TOOL);
            m_bttnLine.setBorderPainted(false);
            m_bttnLine.setContentAreaFilled(false);
            m_bttnLine.setMargin(bttnMargins);
            m_bttnLine.addActionListener(m_graphManager);
            m_bttnLine.addMouseListener(m_graphHandler);

            toolPanel.add(Box.createHorizontalGlue());
            toolPanel.add(m_bttnPoint);
            toolPanel.add(m_bttnArea);
            toolPanel.add(m_bttnLine);
            toolPanel.add(Box.createHorizontalGlue());
            return toolPanel;
        }
    }
}