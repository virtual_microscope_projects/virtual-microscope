/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JPanel;

import virtuallab.Configuration;
/**
 * Chooser views are the views that displays the specimen thumbnails right after
 * the application starts up. Throw these views the user can select the desired
 * specimen and loads it and explore it as desired. The chooser views are
 * implemented using the Composite desig pattern and this class acts as the
 * abstract parent class for all the chooser views in the application.
 *
 */
public abstract class AbstractChooserView extends JPanel implements ComponentListener {
    /**
     * The old Online view is not being used anymore.
     * Should be moved after it is validated that this is no needed anymore.
     * Removing this would also mean that we delete ChooserView class.
     *
     * @deprecated
     */
    public static final short ONLINE_ICON_VIEW = 0;
    public static final short ICON_VIEW = 1;
    public static final short DETAIL_VIEW = 2;
    public static final String ICON = "Icon";
    public static final String DETAIL = "Detail";
    public static final String NONE = "";
    private final static short GAP_Y = 50;
    private final static short GAP_X = 50;
    /**
     * Configuration.
     */
    protected Configuration m_config;
    /**
     * This panel holds all the online specimens.
     */
    protected JPanel m_onlineChooserPanel;
    /**
     * This panel holds all the offline specimens.
     */
    protected JPanel m_offlineChooserPanel;
    /**
     * Specimen button listener.
     */
    protected ActionListener m_specimenButtonListener;

    /**
     * Constructs a chooser given a set of specimens and a specimen button
     * listener.
     *
     * @param specimens set of specimens
     * @param listener  specimen button listener
     */
    public AbstractChooserView(Configuration config,
                               LinkedHashMap onLineSpecimens, LinkedHashMap offLineSpecimens,
                               ActionListener listener) {
    }

    public AbstractChooserView() {
        m_onlineChooserPanel = new JPanel();
        m_onlineChooserPanel.setBackground(new Color(230, 230, 230));
        m_offlineChooserPanel = new JPanel();
        m_offlineChooserPanel.setBackground(new Color(116, 116, 116));
    }

    public abstract short getViewType();

    /**
     * The method returns the minimum size for the top and the bottom online/offline specimens panes.
     *
     * @return
     */
    protected Dimension getMinimumPaneSize() {
        Dimension minimumSize = new Dimension();
        minimumSize.setSize(SpecimenButton.getThumbnailButtonDimensions().getWidth() + GAP_X,
                SpecimenButton.getThumbnailButtonDimensions().getWidth() + GAP_Y + 10);
        return minimumSize;

    }

    public abstract void initialize(Configuration config, LinkedHashMap onlineSpecimens,
                                    LinkedHashMap offlineSpecimens, ActionListener listener);

    public abstract void setSpecimens(LinkedHashMap onlineSpecimenSet, LinkedHashMap offlineSpecimenSet);

    public abstract void refreshSpecimens(LinkedHashMap onlineSpecimenSet, LinkedHashMap offlineSpecimenSet,
                                          LinkedHashMap oldOnlineSpecimens, LinkedHashMap oldOffLineSpecimens);

    /**
     * @return configuration.
     */
    public Configuration getConfig() {
        return m_config;
    }

    public void componentHidden(ComponentEvent e) {
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentResized(ComponentEvent e) {
        for (int i = 0; i < this.getComponentCount(); i++) {
            getComponent(i).dispatchEvent(e);
        }
    }

    public void componentShown(ComponentEvent e) {
    }

    /**
     * Removes all specimen buttons from the chooser panel.
     */
    protected void removeSpecimens() {
        removeSpecimens(m_onlineChooserPanel);
    }

    /**
     * This method iterates over all the components and find the "download" button corresponding
     * to the given specimenId. It then enables/disables it as specified.
     *
     * @param specimenID
     * @param enabled    indictaes if the button should be enabled or disabled.
     */
    public void setSpecimenButtonEnabled(String specimenID, boolean enabled) {
        for (int i = 0; i < m_offlineChooserPanel.getComponentCount(); i++) {
            Component specimenButton = m_offlineChooserPanel.getComponent(i);
            if (specimenButton.getName() != null && specimenButton.getName().equals(specimenID)) {
                // found the download button for the given specimen.
                ((SpecimenButton) specimenButton).getSpecimenButtonOrDownloadButton().setEnabled(enabled);
                return;
            }
        }
    }

    /**
     * This method looks at the new specimens and removes all the buttons that were present
     * in old specimens panel but are not needed in the new specimen panel.
     *
     * @param newSpecimens
     * @param oldSpecimens
     */
    protected void removeObsleteSpecimensFromPanel(LinkedHashMap newSpecimens, LinkedHashMap oldSpecimens, JPanel specimenPanel) {
        Iterator oldSpecimensIterator = oldSpecimens.keySet().iterator();
        while (oldSpecimensIterator.hasNext()) {
            String specimenID = (String) oldSpecimensIterator.next();
            if (!newSpecimens.containsKey(specimenID)) {
                for (int i = 0; i < specimenPanel.getComponentCount(); i++) {
                    Component specimenButton = specimenPanel.getComponent(i);
                    if (specimenButton.getName() != null && specimenButton.getName().equals(specimenID)) {
                        ((SpecimenButton) specimenButton).getSpecimenButtonOrDownloadButton()
                                .removeActionListener(m_specimenButtonListener);
                        specimenPanel.remove(specimenButton);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Removes all specimen buttons from the chooser panel.
     */
    protected void removeSpecimens(JPanel container) {
        // Unregister the listener from old buttons
        Component[] oldButtons = container.getComponents();

        for (int i = 0; (oldButtons != null) && (i < oldButtons.length); i++) {
            if (oldButtons[i] instanceof SpecimenButton) {
                ((SpecimenButton) oldButtons[i]).getSpecimenButtonOrDownloadButton()
                        .removeActionListener(m_specimenButtonListener);
            }
        }
        // Clear panel
        container.removeAll();
    }


}
