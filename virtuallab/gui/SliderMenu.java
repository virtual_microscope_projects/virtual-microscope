/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.Graphics2D;

import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.SpecimenElement;
import virtuallab.util.Constants;

// $Id: SliderMenu.java,v 1.11 2006/09/05 01:08:47 manzoor2 Exp $

public class SliderMenu extends MenuControlBase implements ChangeListener, ActionListener, ViewControl {
    protected MicroscopeView m_display = null;
    protected JSlider m_slider;
    protected HashMap m_stateMap = new HashMap();
    /**
     * Will hold the position where the mouse was when it is dragged. This is used to
     * figure out if the mouse is being dragged to the right or to left.
     */
    protected int m_xCoordinates;
    /**
     * This is used to decide how many pixel movement of the mouse drag
     * will result in one level change of the focu. Decreasing this value
     * will mean that the user can change focue even with the slightest mouse drag
     * and increasing this value will mean that the user will have to drag the
     * mouse more to change the focus.
     */
    private short SENSITIVITY = 10;
    /**
     * Keeps the old cursor before switching to the new horizontal resize cursor. This value
     * is then used to revert back to the original cursor when we are done.
     */
    private Cursor m_oldCursor;

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    public void mouseMoved(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void draw(Graphics2D g) {
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    /**
     * Initializes this control s a view control.
     *
     * @param controlName unique name/type of this control
     * @param display     MicroscopeView object
     */
    public void initViewControl(String controlName, MicroscopeView display) {
        if (display == null || display.getCurrentSpecimen() == null || controlName == null) {
            throw new IllegalArgumentException("Null microscope view, specimen, or name");
        }
        m_display = display;
        if (m_controlName != null) {
            m_controlName = controlName;
        }
    }

    /**
     * This method selects the selected value to the value passed in as the parameter
     *
     * @param value the value that is to be selected.
     */
    public void setSelectedValue(String value) {
        // The value is the display value, we need to find the corresponding
        // tick of the slider against this value.
        Iterator it = m_validControlStates.iterator();
        int i = 0;
        while (it.hasNext()) {
            // Typecasting to the SpecimenElement to take care of both the
            // old XML (which uses ControlState) and new
            // XML formats(which uses ControlStates).
            String v = ((SpecimenElement) it.next()).getDisplayString();
            if (v.equals(value))
                break;
            i++;
        }
        if (i >= m_validControlStates.size())
            i = 0;
        m_slider.setValue(i);
    }

    /**
     * This method cleans up the SliderMenu objetc. Soon after this method is called
     * the calling method should remove this SliderMenu from the sidePanel as well.
     */
    public void cleanUp(double xmlVersion) {
        // Remove the action listeners if set. This MUST be done
        // othewise even after removing these components they will
        // keep on getting these events and it would be a whole
        // mess.
        if (m_slider != null)
            m_slider.removeChangeListener(this);
        if (m_plusButton != null)
            m_plusButton.removeActionListener(this);
        if (m_minusButton != null)
            m_minusButton.removeActionListener(this);
        removeAll();
        setVisible(false);

    }

    /**
     * This method updates the GUI controls contained in the Slider Menu. It
     * removes ALL the current gui controls contained in this Slider Menu and then recreates
     * them with the new values.
     *
     * @param version the XML Version.
     */
    public void updateGUI(double version) {
        if (m_validControlStates == null) {
            return;
        }
        // Remove the action listeners if set. This MUST be done
        // othewise even after removing these components they will
        // keep on getting these events and it would be a whole
        // mess.
        if (m_slider != null)
            m_slider.removeChangeListener(this);
        if (m_plusButton != null)
            m_plusButton.removeActionListener(this);
        if (m_minusButton != null)
            m_minusButton.removeActionListener(this);

        removeAll();
        setMaximumSize(new Dimension(1024, getMinimumSize().height));
        setPreferredSize(new Dimension(150, 70));

        // Init menu
        m_slider = new JSlider(0, m_validControlStates.size() - 1, 0);
        m_slider.setMinorTickSpacing(1);
        m_slider.setPaintTicks(true);
        m_slider.setSnapToTicks(true);
        m_slider.setMinimumSize(new Dimension(25, 0));

        // Init stateMap
        Iterator iter = m_validControlStates.iterator();
        int i = 0;
        while (iter.hasNext()) {
            String s = "";
            if (version <= 1.0) {
                Specimen.ControlState cs = (Specimen.ControlState) iter.next();
                s = cs.getDisplayString();
            } else {
                Specimen.ControlStates cs = (Specimen.ControlStates) iter.next();
                s = cs.getDisplayString();
            }
            m_stateMap.put(new Integer(i), s);
            i++;
        }

        updateButtonStates();

        // Add listeners
        m_slider.addChangeListener(this);
        m_plusButton.addActionListener(this);
        m_minusButton.addActionListener(this);

        // Slider panel
        JPanel sliderPanel = new JPanel(new BorderLayout(0, 0));
        sliderPanel.add(m_slider);

        // Button layout panel
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 5));
        m_minusButton.putClientProperty("JButton.buttonType", "toolbar");
        m_plusButton.putClientProperty("JButton.buttonType", "toolbar");
        buttonPanel.add(m_minusButton);
        buttonPanel.add(m_plusButton);
        setBorder(BorderFactory.createTitledBorder(m_title));
        m_slider.setToolTipText(m_title);
        setLayout(new BorderLayout());
        add(sliderPanel, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.EAST);
    }

    /**
     * Called when button is pressed.
     */
    public void actionPerformed(ActionEvent e) {
        JButton thisButton = (JButton) e.getSource();
        int i = m_slider.getValue();

        if (thisButton == m_minusButton) {
            m_slider.setValue(i - 1);
            if (i - 1 != 0)
                m_minusButton.grabFocus();//Focus should remain on the button.
            else
                // if the minus button is to be disabled we shift focus to the plus button.
                m_plusButton.grabFocus();

        } else if (thisButton == m_plusButton) {
            m_slider.setValue(i + 1);
            if (i + 1 != m_slider.getMaximum())
                m_plusButton.grabFocus();//Focus should remain on the button.
        }
    }

    protected void updateButtonStates() {
        int i = m_slider.getValue();

        m_minusButton.setEnabled(i != 0);
        m_plusButton.setEnabled(i != m_slider.getMaximum());
    }

    public void stateChanged(ChangeEvent e) {
        int i = m_slider.getValue();
        m_currentValue = (String) m_stateMap.get(new Integer(i));
        // See if the change in value has invalidated the subordinate controls
        // e.g. if the new focus value only has 2 magnification levels then the
        // mganification control needs to be redrawn - if there is no magnification
        // value then the magnificaiton control needs to be deleted altogether.
        m_microscope.reCreateControls(this, m_display.getCurrentSpecimen().getXMLVersion());
        m_microscope.updateSpecimenControlStates();
        m_slider.grabFocus();// Focus should remained in the slider.
        updateButtonStates();
    }

    /**
     * This method captures the right mouse button click and changes the
     * cursor to indicate that the user can drag the mouse and chage the focus.
     */
    public void mousePressed(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e) && isVisible() && m_controlName.equals(Constants.Controls.FOCUS)) {
            m_oldCursor = m_display.getCursor();
            m_display.setCursor(Cursor.E_RESIZE_CURSOR);
        }
    }

    /**
     * This method resets the cursor to the default, because while
     * adjusting the focus we had set the cursor to West resize shape.
     */
    public void mouseReleased(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e) && isVisible() && m_controlName.equals(Constants.Controls.FOCUS)) {
            m_display.setCursor(m_oldCursor);
        }
    }

    /**
     * This method gets called when the mouse is dragged. We shall see if the right mouse button
     * is pressed and if it is, then we will increasethe focus when the mouse is moved right and will
     * decrease the focus whhen it is moved left.
     */
    public void mouseDragged(MouseEvent e) {

        // mouse dragging for slider control only applies to
        // FOCUS control. Since our logic creates control inadvance whether they are
        // present in a particular specimen or not, we have the isVisible() check to
        // make sure we only show the horizontal cursor for specimens that have
        // focus control available and visible.
        if (SwingUtilities.isRightMouseButton(e) && isVisible() && m_controlName.equals(Constants.Controls.FOCUS)) {
            if (m_slider == null) return;
            int i = m_slider.getValue();
            // Here we shall only increase/decrease the focus if the user
            // has moved mouse substantially. This will decrease the
            // sensitivity of the mouse move.
            if ((m_xCoordinates - e.getX()) >= SENSITIVITY) {
                // mouse was dragged to the left. So we decrease the focus.
                m_slider.setValue(i - 1);
                m_xCoordinates = e.getX();
            } else if ((e.getX() - m_xCoordinates) >= SENSITIVITY) {
                // mouse was moved to the right so we increase the focus.
                m_slider.setValue(i + 1);
                m_xCoordinates = e.getX();
            }
            // else the mouse was moved too little to the right/left so we
            // ignore the movement.
        }
    }
}