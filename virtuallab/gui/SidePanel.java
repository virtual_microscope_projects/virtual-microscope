/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import virtuallab.util.Constants;
import virtuallab.util.ControlBase;
import virtuallab.util.Locale;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.util.Iterator;
import java.util.LinkedHashSet;

// $Id: SidePanel.java,v 1.13 2006/09/05 01:08:47 manzoor2 Exp $

public class SidePanel extends JPanel {
    private static final long serialVersionUID = 5561231740011037808L;
    protected JPanel m_controlsPanel;
    protected JPanel m_unloadPanel;
    protected JButton m_unloadButton;
    private JScrollPane m_scrollPanel;

    /**
     * Constructs an instance of <code>SidePanel</code>. Sets itself up to be
     * ready to be populated with specimen controls through a call to
     * <code>setSpecimenControls</code>.
     */
    SidePanel() {
        // Configure SidePanel's preferences
        setPreferredSize(new Dimension(Microscope.SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS, 0));
        setMinimumSize(new Dimension(Microscope.SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS, 0));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        // Create and configure the controls panel
        m_controlsPanel = new JPanel();
        m_controlsPanel.setLayout(new BoxLayout(m_controlsPanel, BoxLayout.Y_AXIS));

        m_scrollPanel = new JScrollPane(m_controlsPanel, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        // Create, configure and add the button panel
        m_unloadPanel = new JPanel();
        m_unloadButton = new JButton(Locale.Unload(virtuallab.Configuration.getApplicationLanguage()));

        m_unloadPanel.setBorder(BorderFactory.createEtchedBorder());
        m_unloadPanel.add(m_unloadButton);

        m_unloadPanel.setMaximumSize(new Dimension(1024, getMinimumSize().height));

        // Populate the SidePanel
        add(m_scrollPanel);
        add(m_unloadPanel);

        // Spacer for Mac resize widget
        if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
            add(Box.createVerticalStrut(16));
        }
    }

    public JScrollPane getScrollPane() {
        return m_scrollPanel;
    }

    /**
     * This method adds a control to the side panel.
     *
     * @param control - set of controls.
     */
    public void addToSpecimenControls(Control control) {
        m_controlsPanel.add((JComponent) control);// (JComponent)iter.next());
        m_controlsPanel.revalidate();
    }

    /**
     * Populates the panel with specimen controls.
     *
     * @param specimenControls -
     *                         set of controls.
     */
    public void setSpecimenControls(LinkedHashSet specimenControls) {
        // Clear the controls panel
        m_controlsPanel.removeAll();

        // Read controls and populate the panel
        if (specimenControls != null) {
            Iterator iter = specimenControls.iterator();

            while (iter.hasNext()) {
                JComponent control = (JComponent) iter.next();
                if (control instanceof MenuControlBase) {
                    MenuControlBase comp = (MenuControlBase) control;
                    if (shouldHideControl(comp))
                        control.setVisible(false);
                }

                control.setBorder(BorderFactory.createTitledBorder(BorderFactory.createCompoundBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED),
                        BorderFactory.createBevelBorder(BevelBorder.LOWERED)),((ControlBase)control).m_title));

                m_controlsPanel.add(control);

                if (iter.hasNext()) {
                    m_controlsPanel.add(Box.createRigidArea(new Dimension(0, 5)));
                }
            }
        }

        m_controlsPanel.add(Box.createVerticalGlue());
        m_controlsPanel.revalidate();
    }

    /**
     * See if there is any control with only one possible value, if there is one
     * then we shall hide such a control; unless it is one of special controls
     * (e.g. EDS etc.) that require special handling and need to be shown
     * no matter what.
     *
     * @param comp
     * @return true if the control should be hidden false otherwise.
     */
    private boolean shouldHideControl(MenuControlBase comp) {

        boolean onlyOneValue = comp.m_validControlStates.size() <= 1;
        boolean mustNotHide = comp.m_controlName.equals(Constants.Controls.EDS);
        return (onlyOneValue && !mustNotHide);
    }

    /**
     * This method gets the JPanel that is shown on the side of the Specimen.
     *
     * @return The side JPanel.
     */
    public JPanel getSidePanel() {
        return m_controlsPanel;
    }

    /**
     * @return the unload button.
     */
    public JButton getUnloadButton() {
        return m_unloadButton;
    }
}
