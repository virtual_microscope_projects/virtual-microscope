/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */

/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 **
 * University of Illinois/NCSA Open Source License Copyright (c) 2005, Imaging
 * Technology Group, All rights reserved. Developed by: Imaging Technology Group
 * Beckman Institute for Advanced Science and Technology University of Illinois
 * at Urbana-Champaign http://virtual.itg.uiuc.edu virtual@itg.uiuc.edu
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * with the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions: Redistributions of
 * source code must retain the above copyright notice, this list of conditions
 * and the following disclaimers. Redistributions in binary form must reproduce
 * the above copyright notice, this list of conditions and the following
 * disclaimers in the documentation and/or other materials provided with the
 * distribution. Neither the names of the Imaging Technology Group, Beckman
 * Institute for Advanced Science and Technology, University of Illinois at
 * Urbana-Champaign, nor the names of its contributors may be used to endorse or
 * promote products derived from this Software without specific prior written
 * permission. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE
 * OR OTHER DEALINGS WITH THE SOFTWARE.
 */

package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import virtuallab.Configuration;
import virtuallab.Specimen;

// $Id: ChooserView.java,v 1.4 2006/09/05 01:08:47 manzoor2 Exp $**
/* Implements the specimen chooser view of the VirtualMicroscope. Through this
 * view user can choose the specimen to load (by clicking the specimen thumbnail
 * button).
 */
public class ChooserView extends AbstractChooserView {
    /**
     *
     */
    private static final long serialVersionUID = -7439197375690745740L;
    ScrollableFlowPanel m_flowPanel;

    /**
     * Constructs a chooser given a set of specimens and a specimen button
     * listener.
     *
     * @param specimens set of specimens
     * @param listener  specimen button listener
     */
    public ChooserView(Configuration config, LinkedHashMap onLineSpecimens,
                       LinkedHashMap offLineSpecimens, ActionListener listener) {
        initialize(config, onLineSpecimens, listener);
    }

    public String menuText() {
        return NONE;
    }

    /**
     * This method initializes the Chooser view. As a result of which the view
     * can now be displayed.
     */
    public void initialize(Configuration config, LinkedHashMap onlineSpecimens,
                           LinkedHashMap offlineSpecimens, ActionListener listener) {
        initialize(config, onlineSpecimens, listener);
    }

    private void initialize(Configuration config, LinkedHashMap specimens,
                            ActionListener listener) {

        m_specimenButtonListener = listener;
        m_config = config;
        m_onlineChooserPanel = new JPanel();

        m_flowPanel = new ScrollableFlowPanel(m_onlineChooserPanel);
        setSpecimens(specimens);
        setLayout(new BorderLayout());
        add(m_flowPanel, BorderLayout.CENTER);
        setOpaque(false);
    }

    public void setSpecimens(LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens) {
        setSpecimens(onlineSpecimens);
    }

    public void refreshSpecimens(LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens, LinkedHashMap oldOnlineSpecimens, LinkedHashMap oldOfflineSpecimens) {
        setSpecimens(onlineSpecimens);
    }

    /**
     * Sets chooser's specimen.
     *
     * @param specimens set of specimens
     */
    private void setSpecimens(LinkedHashMap specimens) {
        SpecimenButton curBttn = null;
        Dimension curBttnDim = null;
        int minWidth = 0;
        int minHeight = 0;

        // Remove old specimen buttons from m_onlineChooserPanel
        removeSpecimens();

        // Create buttons for all specimens and add them to chooser.
        Iterator iter = specimens.keySet().iterator();

        while ((iter != null) && (iter.hasNext())) {
            // Put together a specimen button
            curBttn = new SpecimenButton((Specimen) iter.next());
            curBttn.getSpecimenButtonOrDownloadButton().addActionListener(m_specimenButtonListener);

            // Add it to chooser panel
            m_onlineChooserPanel.add(curBttn);

            // Remember current button dimenstions
            curBttnDim = curBttn.getMinimumSize();
            minWidth = Math.max(minWidth, curBttnDim.width);
            minHeight = Math.max(minHeight, curBttnDim.height);
        }

        // Set minimum chooser size to be that of the largest button.
        m_onlineChooserPanel.setMinimumSize(new Dimension(minWidth, minHeight));
    }

    public short getViewType() {
        return AbstractChooserView.ONLINE_ICON_VIEW;
    }

    /**
     * @return configuration.
     */
    public Configuration getConfig() {
        return m_config;
    }

    /**
     * Removes all specimen buttons from the chooser panel.
     */
    protected void removeSpecimens() {
        // Unregister the listener from old buttons
        Component[] oldButtons = m_onlineChooserPanel.getComponents();

        for (int i = 0; (oldButtons != null) && (i < oldButtons.length); i++) {
            if (oldButtons[i] instanceof SpecimenButton) {
                ((SpecimenButton) oldButtons[i]).getSpecimenButtonOrDownloadButton()
                        .removeActionListener(m_specimenButtonListener);
            }
        }
        // Clear panel
        m_onlineChooserPanel.removeAll();
    }
}


class ScrollableFlowPanel extends JScrollPane {
    private static final long serialVersionUID = -4043342364454977665L;
    static JPanel pane;

//	public ScrollableFlowPanel()
//	{
//		new ScrollableFlowPanel(new JPanel(new FlowLayout()));
//	}

    public ScrollableFlowPanel(JPanel view) {
        super(pane = view);
        setOpaque(false);
        setBorder(new EmptyBorder(0, 0, 0, 0));

        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Insets i = c.getInsets();
                pane.setPreferredSize(new Dimension(c.getWidth() - i.left
                        - i.right, c.getHeight()));
                pane.revalidate();
            }
        });

        pane.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Dimension currentPrefSize = c.getPreferredSize();
                if (c.getComponentCount() > 0) {
                    int newHeight = 0, tmpHeight = 0;
                    for (int i = 0; i < c.getComponentCount(); ++i) {
                        Component lastComp = c.getComponent(i);
                        tmpHeight = lastComp.getLocation().y
                                + lastComp.getHeight();
                        newHeight = Math.max(newHeight, tmpHeight);
                    }
                    newHeight += c.getInsets().bottom;

                    currentPrefSize.height = newHeight;
                }
                c.setPreferredSize(currentPrefSize);
                c.revalidate();
            }
        });
    }
}