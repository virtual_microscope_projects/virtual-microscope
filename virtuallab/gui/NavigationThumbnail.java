/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import virtuallab.Log;
import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.util.Constants;
import virtuallab.util.ControlBase;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.InputStream;

/**
 * This class implements the SpecimenThumbnail located at the top right in the sidepanel.
 * The thumbnail is used for quick navigation and presents a much smaller version of the base image
 * which the user can use to jump from one portion of the image to another.
 *
 */

public class NavigationThumbnail extends ControlBase implements SideControl, MouseListener, MouseMotionListener {

    private static final Log log = new Log(NavigationThumbnail.class.getName());

    private final short HORIZONTAL_INSET = 30;
    private final short VERTICAL_INSET = 25;
    /**
     * Reference to the microscopeview that contains the base image.
     */
    private MicroscopeView m_display;
    /**
     * thumbnail dimensions
     */
    private Dimension m_thumbnailDimension;
    /**
     * Stores the current dimension of the rectangular region that corresponds to the
     * view port.
     */
    private Dimension m_viewPortDimension;
    /**
     * Cursor dimension that depicts the size of the viewort
     */
    private Dimension m_cursorDimension;
    /**
     * Holds the coordinates at which the top left of the viewport rectangle will be drawn.
     */
    private Point2D.Double m_viewPortTopCoordinates;
    /**
     * Holds the current mouse position.
     */
    private Point2D.Double m_mousePosition;
    /**
     * TODO: clean up the label thumbnailimage when the specimen is unloaded.
     * TODO: May be we can store the thumbnail image inside the specimen. Initially
     * the specimen will not have it, but once it is loaded we create a thumbnail and
     * store it inside the specimen.
     */
    private BufferedImage m_thumbnailImage;
    /**
     * holds whether the mouse is inside the thumbnail image or not.
     */
    private boolean m_bMouseInside;
    /**
     * The coordinates of the top-left corner of the thumbnail. These are required since the
     * thumbnail is not drawn at (0,0) - we leave some space for the border and the title. By
     * maintaining these coordinates we can trnaslate the mouse coordinates relative to the image
     * instead of the NavigationThumbnail JPanel.
     */
    private Point2D.Double m_ImageTopLeft = new Point2D.Double(8, 17);

    /**
     * This is the initializer method that gets called when the control is first created.
     * All iitialization should go into this mehtod.
     */
    public void initSideControl(String controlName, Specimen.ControlDescriptor controlDesc, Microscope scope) {
        if (controlDesc == null || scope == null || controlName == null) {
            throw new IllegalArgumentException("Null control descriptor, microscope, or name");
        }
        m_thumbnailDimension = new Dimension(0, 0);
        m_viewPortDimension = new Dimension(0, 0);
        m_viewPortTopCoordinates = new Point2D.Double(0, 0);
        m_mousePosition = new Point2D.Double(0, 0);
        m_cursorDimension = new Dimension(0, 0);
        m_display = scope.getMicroscopeView();
        m_title = controlDesc.getNamedParam(Constants.XMLTags.DISPLAY_STRING);
        // TODO: Getting the thumbnail dimension from the actual thumbnail instead of the specimen.
        // make sure Chas is putting the thumbnail in correct aspect ratio. Also since Chas is already
        // scaling the thumbnail do it still need to scale the thumbnails again?
//	    double thumbnailWidth  = Math.round(m_display.getMicroscope().getSidePanel().getPreferredSize().getWidth()) -
//	    						HORIZONTAL_INSET;
//	    double thumbnailHeight = Math.round((m_display.getCurrentSpecimen().getFirstImageDimensions().y/
//	    						m_display.getCurrentSpecimen().getFirstImageDimensions().x)* thumbnailWidth);
//		m_thumbnailDimension.setSize(thumbnailWidth,thumbnailHeight);
        createNavigationThumbnail();
        m_display.setNavigatorControl(this);
        updateGUI();
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyTyped(KeyEvent e) {
    }

    /**
     * returns the name of this control.
     */
    public String getControlName() {
        return Constants.Controls.NAVIGATOR;
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        Rectangle2D rect = new Rectangle2D.Double();
        rect.setRect(m_ImageTopLeft.x, m_ImageTopLeft.y,
                m_thumbnailDimension.getWidth(), m_thumbnailDimension.getHeight());
        if (rect.contains(e.getX(), e.getY()))
            m_bMouseInside = true;
        else
            m_bMouseInside = false;
    }

    public void mouseExited(MouseEvent e) {
        m_bMouseInside = false;
    }

    /**
     * Tracks the mouse movement. We draw a bounding rectangle upon mouse move. This
     * rectangle represents the viewport. If the user were to click the mouse then
     * we would move the base image such that the base image covered by the
     * bounding rectangle will be centered in the viewport. The size of the bounding
     * rectangle depends on the magnification level.
     *
     * @param e
     */
    public void mouseMoved(MouseEvent e) {
        Rectangle2D rect = new Rectangle2D.Double();
        rect.setRect(m_ImageTopLeft.x, m_ImageTopLeft.y,
                m_thumbnailDimension.getWidth(), m_thumbnailDimension.getHeight());
        if (rect.contains(e.getX(), e.getY()))
            m_bMouseInside = true;
        else
            m_bMouseInside = false;
        m_mousePosition.setLocation(e.getX(), e.getY());
        drawContents(this.getGraphics());
    }

    /**
     * Handles the mouse click events on the thumbnail. When this event occurs
     * we shall redraw the base image such that the point at which the mouse was
     * clicked will be positioned at the center of the viewport.
     *
     * @param mouseevent mouse click event.
     */
    public void mouseClicked(MouseEvent e) {
        // Getting the mouse coordinates relative to the thumbnail.
        if (e.getX() > m_ImageTopLeft.x && e.getY() > m_ImageTopLeft.y &&
                e.getX() < m_ImageTopLeft.x + m_thumbnailDimension.width &&
                e.getY() < m_ImageTopLeft.y + m_thumbnailDimension.height) {
            double x = e.getX() - m_ImageTopLeft.x;
            double y = e.getY() - m_ImageTopLeft.y;
            // converting into the equivalent coordinates of the base image.
            x *= (double) m_display.getCurrentImageSet().getImageSize().getWidth() / m_thumbnailDimension.width;
            y *= (double) m_display.getCurrentImageSet().getImageSize().getHeight() / m_thumbnailDimension.height;
            /** How much do we have to translate the click point such that it aligns with the center.*/
            x = ((double) m_display.getBeamSize().getWidth() / 2) - x;
            y = ((double) m_display.getBeamSize().getHeight() / 2) - y;
            m_display.setStagePosition(x, y);
            m_display.validate();
            m_display.repaint();
        }
    }

    /**
     * This method updates the thumbnail with the recent changes in the base view's position. This
     * method gets called everytime the base image needs to be redrawn.
     */
    public void updateStatus() {
        updateViewPortDimensions(m_display.getCurrentImageSet().getImageSize().width, m_display.getCurrentImageSet().getImageSize().height);
        validate();
        repaint();
        //drawContents(this.getGraphics());
    }

    /**
     * This method calclates the dimensions and the positioning of the rectangular region that will represent the
     * viewport in our thumbnail.
     * The method first figures out an intersection between the original base image and the viewport and then
     * translates this ontersection into the thumbnail dimension. This gives us the size of the rectangular
     * region in the thumbnail that corresponds to the viewport. it then figures out the coordinates
     * at which this rectangular region should be drawn.
     *
     * @param baseImageWidth the width of the base image which the thumbnail represents.
     * @param baseImageWidth the width of the base image which the thumbnail represents.
     */
    private void updateViewPortDimensions(int baseImageWidth, int baseImageHeight) {
        m_ImageTopLeft.setLocation(getDrawingCoordinates());
        // First we find out the corresponding points in the thumbnail dimension for each of the
        // base image corners and the base image viewport. Note that the
        // Viewport's top left coordinates always start from (0,0).
        Rectangle2D viewport = new Rectangle2D.Double(0, 0, m_display.getBeamSize().getWidth(), m_display.getBeamSize().getHeight());
        Rectangle2D baseimage = new Rectangle2D.Double(m_display.getStagePosition().x,
                m_display.getStagePosition().y, baseImageWidth, baseImageHeight);
        Rectangle2D thumbnailViewport = baseimage.createIntersection(viewport);
        double ratioX = (double) m_thumbnailDimension.width / m_display.getCurrentImageSet().getImageSize().getWidth();
        double ratioY = (double) m_thumbnailDimension.height / m_display.getCurrentImageSet().getImageSize().getHeight();
        /** Updating the cursor dimensions */
        m_cursorDimension.setSize(viewport.getWidth() * ratioX, viewport.getHeight() * ratioY);

        m_viewPortDimension.setSize(thumbnailViewport.getWidth() * ratioX, thumbnailViewport.getHeight() * ratioY);
        double x = (double) (m_display.getBeamSize().getWidth() * m_thumbnailDimension.getWidth()) / baseImageWidth;
        double y = (double) (m_display.getBeamSize().getHeight() * m_thumbnailDimension.getHeight()) / baseImageHeight;
        // Now we figure out the top left coordinates of the viewport.
        x = (double) -1 * m_display.getStagePosition().x * (double) m_thumbnailDimension.width / baseImageWidth;
        y = (double) -1 * m_display.getStagePosition().y * (double) m_thumbnailDimension.height / baseImageHeight;
        if (x > 0 && y > 0) {
            // Viewport's top left is within the image boundary.
            m_viewPortTopCoordinates.setLocation(Math.round(x + m_ImageTopLeft.x), Math.round(y + m_ImageTopLeft.y));
        } else if (x < 0 && y < 0) {
            // Viewport's top left Y-axis and X-axis are beyond the top left (X,Y) of the image.
            m_viewPortTopCoordinates.setLocation(m_ImageTopLeft.x, m_ImageTopLeft.y);
        } else if (x < 0) {
            // Viewport's top left X-axis is beyond the top left of the image.
            m_viewPortTopCoordinates.setLocation(m_ImageTopLeft.x, Math.round(y + m_ImageTopLeft.y));
        } else if (y < 0) {
            // Viewport's top left Y-axis is beyond the top left of the image.
            m_viewPortTopCoordinates.setLocation(Math.round(x + m_ImageTopLeft.x), m_ImageTopLeft.y);
        }
        // This condition may occur due to some rounding off errors. Although this error is not more than
        // a pixel but this is enough to make the bounding rectangle draw its edge past the
        // thumbnail. So we specifically truncate the bounding rectangle if this is the case.
        x = (m_viewPortTopCoordinates.x + m_viewPortDimension.width) - (m_ImageTopLeft.x + m_thumbnailDimension.width);
        y = (m_viewPortTopCoordinates.y + m_viewPortDimension.height) - (m_ImageTopLeft.y + m_thumbnailDimension.height);
        if (x > 0) {
            m_viewPortDimension.setSize(m_viewPortDimension.getWidth() - x, m_viewPortDimension.getHeight());
        }
        if (y > 0) {
            m_viewPortDimension.setSize(m_viewPortDimension.getWidth(), m_viewPortDimension.getHeight() - y);
        }
    }

    /**
     * This method initializes/resets the controls contained in the thumbnail navigator
     * panel.
     */
    private void updateGUI() {
        setPreferredSize(new Dimension((int) m_thumbnailDimension.getWidth(), (int) m_thumbnailDimension.getHeight() + VERTICAL_INSET));
        setBorder(BorderFactory.createTitledBorder(m_title));
        setMaximumSize(new Dimension(1024, (int) m_thumbnailDimension.getHeight() + VERTICAL_INSET + 20));
        if (getMouseListeners().length == 0) {
            addMouseListener(this);
            addMouseMotionListener(this);
        }
    }

    /**
     * This method creates an icon out of the specimen thumbnail.
     * The thumbnail is a scaled down version of the image in which the
     * the aspect ratio is maintained while still fitting the thumbnail
     * in the side panel.
     *
     * @param currentSpecimen currently loaded specimen.
     * @return The ImageIcon that shows the specimen thumbnail.
     */
    private void createNavigationThumbnail() {
        Specimen currentSpecimen = m_display.getCurrentSpecimen();
        BufferedImage bufferedImage = null;
        //explicitly setting the previous thumbnail to null guarantees it will be garbage collected.
        m_thumbnailImage = null;
        try {
            try {
                InputStream is = m_display.getCurrentSpecimen().getInputStream(m_display.getCurrentSpecimen().getNavigationThumbnail());
                m_thumbnailImage = ImageIO.read(is);
                is.close();

            } catch (Exception ex) {
                log.error("Could not retrieve specimen thumbnail!");
                log.error(ex.getMessage());
                m_thumbnailImage = null;
            }
            m_thumbnailDimension.setSize(m_thumbnailImage.getWidth(), m_thumbnailImage.getHeight());
            if (m_thumbnailDimension.getWidth() > 150 || m_thumbnailDimension.getHeight() > 150) {
                // Crop and scale if the thumbnail is not already scaled properly.

                double thumbnailWidth = Math.round(m_display.getMicroscope().getSidePanel().getPreferredSize().getWidth()) -
                        HORIZONTAL_INSET;
                double thumbnailHeight = Math.round((m_display.getCurrentSpecimen().getFirstImageDimensions().y / m_display.getCurrentSpecimen().getFirstImageDimensions().x) * thumbnailWidth);
                m_thumbnailDimension.setSize(thumbnailWidth, thumbnailHeight);
                bufferedImage = m_thumbnailImage;
                int imgHeight = bufferedImage.getHeight(null);
                int imgWidth = bufferedImage.getWidth(null);
                double scalex = (double) m_thumbnailDimension.getWidth() / imgWidth;
                double scaley = (double) m_thumbnailDimension.getHeight() / imgHeight;
                AffineTransform tx = new AffineTransform();
                tx.scale(scalex, scaley);
                AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
                m_thumbnailImage = op.filter(bufferedImage, null);
                bufferedImage.flush();
                bufferedImage = null;
            }
        } catch (Exception e) {
            log.error("Could not retrieve specimen thumbnail!");
            log.error(e.getMessage());
            m_thumbnailImage = null;
        }
    }

    /**
     * repaints the component and all its encompassing images/shapes.
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawContents(g);
    }

    /**
     * This method gets the top left coordinate of the thumbnail.
     *
     * @return
     */
    private Point2D.Double getDrawingCoordinates() {
        double posX = (getSize().width - m_thumbnailImage.getWidth()) / 2;
        double posY = (getSize().height - m_thumbnailImage.getHeight()) / 2 + 5;
        return new Point2D.Double(posX, posY);
    }

    /**
     * This method refreshes the thumbnail image and the bounding rectangles etc.
     *
     * @param g
     */
    private void drawContents(Graphics g) {
        Graphics2D g2 = (Graphics2D) g.create();
        Color clr = g2.getColor();
        Point2D.Double topeLeftCoordinates = getDrawingCoordinates();
        /** Drawing the navigatin thumbnail in the center of the panel.*/
//	    g2.drawImage(m_thumbnailImage,null,
//		(int)m_ImageTopLeft.x,(int)m_ImageTopLeft.y);
        g2.drawImage(m_thumbnailImage, null, (int) topeLeftCoordinates.x, (int) topeLeftCoordinates.y);
        g2.setColor(Color.YELLOW);
        /** Drawing the rectangle that depicts the base image viewport */
        // -1 is used to overcome some rendering problems that occurred at the edge of
        // the image - the viewport rectangle seems to be spilling over a pixel out of the
        // thumbnail and a trailing line would show on the right and the bottom edges.
        // TODO: Find the root cause of the problem and find a better fix.
        g2.drawRect((int) m_viewPortTopCoordinates.x, (int) m_viewPortTopCoordinates.y,
                (int) m_viewPortDimension.width - 1, (int) m_viewPortDimension.height - 1);

        /** Drawing the mouse move rectangle which simulates the cursor.*/
        if (m_bMouseInside) {
            /**
             * The mouse hotspot is kept at the center of the cursor rectangle. All the complexity below
             * is due to this hotspot restriction and due to the boundary checks to avoid the spilover
             * of the cursor to non-thumbnail area.
             */
            g2.setColor(Color.WHITE);
            double startX = m_mousePosition.x - (double) m_cursorDimension.width / 2;
            double startY = m_mousePosition.y - (double) m_cursorDimension.height / 2;
            double x = (startX + m_cursorDimension.width) - (m_thumbnailDimension.getWidth() + m_ImageTopLeft.x);
            double y = (startY + m_cursorDimension.height) - (m_thumbnailDimension.getHeight() + m_ImageTopLeft.y);
            if (x > 0)
                x = (double) m_cursorDimension.width - x;
            else
                x = m_cursorDimension.width;
            if (y > 0)
                y = (double) m_cursorDimension.height - y;
            else
                y = m_cursorDimension.height;

            /** if the rectangle goes past the left and top edges we readjust the rectangle size */
            if (startX < m_ImageTopLeft.x) {
                x -= (m_ImageTopLeft.x - startX);
                startX = m_ImageTopLeft.x;
            }
            if (startY < m_ImageTopLeft.y) {
                y -= (m_ImageTopLeft.y - startY);
                startY = m_ImageTopLeft.y;
            }
            g2.drawRect((int) startX, (int) startY, (int) x, (int) y);
        }
        g2.setColor(clr);
        if (g2 != null)
            g2.dispose();

    }
}
