/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.border.EmptyBorder;

import virtuallab.Configuration;
import virtuallab.OfflineSpecimen;
import virtuallab.Specimen;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

/**
 * This is the IconChooserView of the specimen chooser functionality. This view
 * shows the specimen thumbnails split into two portions. One represents
 * the online specimens (i.e. the specimens that the user has already
 * downloaded to his machine) the other shows thumbnails corresponding to
 * the offline specimens (the ones that are available on the virtualweb site
 * but hasn't yet been downloaded by the user).
 * TODO: The ScrollableSpecimenFlowPanel approach needs to be changed, there is lot of
 * code duplication and redundancy.
 *
 */
public class IconChooserView extends AbstractChooserView implements ComponentListener {
    private JSplitPane m_splitPane;
    private ScrollableOnlineSpecimenFlowPanel m_onlineSpecimenFlowPanel;
    private ScrollableOfflineSpecimenFlowPanel m_offlineSpecimenFlowPanel;

    public IconChooserView() {
        super();
        m_splitPane = null;
    }

    public String menuText() {
        return ICON;
    }

    public short getViewType() {
        return AbstractChooserView.ICON_VIEW;
    }

    /**
     * Initializes the view such that it is in a displayable state.
     *
     * @param config
     * @param specimens
     * @param offlineSpecimens
     * @param listener
     */
    public void initialize(Configuration config, LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens, ActionListener listener) {
        m_specimenButtonListener = listener;
        m_config = config;
        m_onlineSpecimenFlowPanel = new ScrollableOnlineSpecimenFlowPanel(m_onlineChooserPanel, getMinimumPaneSize());
        m_offlineSpecimenFlowPanel = new ScrollableOfflineSpecimenFlowPanel(m_offlineChooserPanel, getMinimumPaneSize());
        setSpecimens(onlineSpecimens, offlineSpecimens);
        setLayout(new BorderLayout());
        addSplitPaneBetweenPanels(m_onlineSpecimenFlowPanel, m_offlineSpecimenFlowPanel);
        setOpaque(false);
    }

    /**
     * This method sets the icon view split panes. It has lot of hardcoded logic which should be
     * changed and made more flexible. Right now any change in the icon view willthrow this method off
     * track.
     *
     * @param up
     * @param down
     */
    private void addSplitPaneBetweenPanels(ScrollableOnlineSpecimenFlowPanel up, ScrollableOfflineSpecimenFlowPanel down) {
        int onlinePanelSize = 0;
        m_splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, up, down);
        int onlineSpecimenRows = (int) Math.ceil((float) m_onlineChooserPanel.getComponentCount() / 6);

        if (m_onlineChooserPanel.getComponentCount() <= 1) {
            if (m_offlineChooserPanel.getComponentCount() > 1) {
                // no online specimens and atleast one offline specimen available.
                // We shall let offline take up all the space.
                m_splitPane.setDividerLocation(0);
            } else {
                // no online AND offline specimens available. We shall show both the
                // panels, with offline taking up one row and the rest being taken up by
                // online.
                m_splitPane.setResizeWeight(1);
            }
        } else if (onlineSpecimenRows <= 2) {
            onlinePanelSize = onlineSpecimenRows * (SpecimenButton.SIZE_Y + 10);
            m_splitPane.setDividerLocation(onlinePanelSize);
        } else if (m_offlineChooserPanel.getComponentCount() > 1) {
            // online panel has more than 2 rows, which means it will try to eat up the space
            // of offline. We need to make sure that the offline panel has atleast one row visible.
            m_splitPane.setResizeWeight(1);
        } else {
            // no offline specimen. We let online specimen take up all the place
            onlinePanelSize = Constants.VIEWER_PREFERRED_DIM.height;
            m_splitPane.setDividerLocation(onlinePanelSize);
        }
        m_splitPane.setOneTouchExpandable(true);
        add(m_splitPane, BorderLayout.CENTER);
    }

    /**
     * This method refreshes the panel and updates the panel with any new online specimens that may have
     * been downloaded since the last refresh. It also removes offline specimens if they have been
     * successfully downloaded - in which case these removed specimens will actually be moved to online
     * panel.
     * Notice that we clone the specimens. This avoids synchronization problems, since we are only
     * reading out of the specimens Collection, cloning is safe. Not doing this and passing
     * the original specimens collection will most certainly result in Concurrentmodification
     * exception in case when multiple downloads finish at the same time.
     */
    public void refreshSpecimens(LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens, LinkedHashMap oldOnlineSpecimens, LinkedHashMap oldOfflineSpecimens) {
        int onlineSpecimenCount = oldOnlineSpecimens.size();
        int offlineSpecimenCount = oldOfflineSpecimens.size();
        if (onlineSpecimenCount == 0 || offlineSpecimenCount == 0) {
            // if we did not have anything before we must go all the way i.e recreating everything
            // especially the header - since the header text must be changed to reflect
            // that now we do have some specimens available.
            setSpecimens((LinkedHashMap) onlineSpecimens.clone(), (LinkedHashMap) offlineSpecimens.clone());
        } else {
            refreshSpecimensInPanel((LinkedHashMap) onlineSpecimens.clone(), m_onlineChooserPanel, oldOnlineSpecimens);
            refreshSpecimensInPanel((LinkedHashMap) offlineSpecimens.clone(), m_offlineChooserPanel, oldOfflineSpecimens);
        }

        if (onlineSpecimenCount == 0 && onlineSpecimens.size() > 0) {
            // if there was no online specimen and now there is one, then we should
            // show the online specimen panel.
            m_splitPane.setDividerLocation(SpecimenButton.SIZE_Y + 10);
        }

        //this will make sure we have the latest status of the scrollbars later when we need to refresh scroll bars.
        this.revalidate();
        refreshScrollBars(m_onlineSpecimenFlowPanel, 20000);
        refreshScrollBars(m_offlineSpecimenFlowPanel, 0);
        if (offlineSpecimens.size() == 0) {
            // no more offline specimens so we should hide the offline specimens
            // panel.
            m_splitPane.setDividerLocation(2000);
        }
        this.revalidate();
    }

    /**
     * This method revalidates the vertical scroll bars and adjusts the scroll bars to
     * make sure that the newly added specimen is visible.
     * This is especially useful when we download a new specimen, this would require moving the
     * downloaded specimen from offline panel into the online panel.
     * This newly downloaded specimen will appear at the bottom of the online specimen panel.
     * We need to make this newly added specimen visible and this method achieves this by
     * moving the vertical scroll bar all the way down.
     *
     * @param flowPanel
     */
    private void refreshScrollBars(JScrollPane flowPanel, int vertialScrollBarNewPosition) {
        flowPanel.dispatchEvent(new ComponentEvent(this, ComponentEvent.COMPONENT_RESIZED));
        //flowPanel.getVerticalScrollBar().setValue(vertialScrollBarNewPosition);
        flowPanel.getViewport().setViewPosition(new java.awt.Point(0, vertialScrollBarNewPosition));
    }

    /**
     * This method is a brute force approach. It removes ALL the specimens from the panel and then
     * rereads them from the disk. This method has lot of performance over head. It is best to use
     * refreshSpecimens() instead whenever possible. The only time when using this method makes sense
     * is when the View is created the very first time.
     */
    public void setSpecimens(LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens) {
        if (onlineSpecimens.size() > 0)
            displaySpecimensInPanel(onlineSpecimens, m_onlineChooserPanel, new SpecimenButton("").createOnlineSpecimenPanelLabel(m_onlineChooserPanel.getBackground(), Locale.ClickToView(Configuration.getApplicationLanguage())));
        else
            displaySpecimensInPanel(onlineSpecimens, m_onlineChooserPanel, new SpecimenButton("").createOnlineSpecimenPanelLabel(m_onlineChooserPanel.getBackground(), Locale.NoSpecimensDownloaded(Configuration.getApplicationLanguage())));

        if (offlineSpecimens.size() > 0)
            displaySpecimensInPanel(offlineSpecimens, m_offlineChooserPanel, new SpecimenButton("").createOfflineSpecimenPanelLabel(m_offlineChooserPanel.getBackground(), Locale.ClickToDownload(Configuration.getApplicationLanguage())));
        else
            displaySpecimensInPanel(offlineSpecimens, m_offlineChooserPanel, new SpecimenButton("").createOfflineSpecimenPanelLabel(m_offlineChooserPanel.getBackground(), Locale.NoMoreSpecimensAvailable(Configuration.getApplicationLanguage())));
    }

    /**
     * Sets chooser's specimen.
     *
     * @param specimens set of specimens
     * @param container The panel that contains the specimen buttons.
     */
    private void displaySpecimensInPanel(LinkedHashMap specimens, JPanel container, JPanel specimenPanelLabel) {
        SpecimenButton curBttn = null;
        Dimension curBttnDim = null;
        int minWidth = 0;
        int minHeight = 0;

        removeSpecimens(container);
        container.add(specimenPanelLabel);
        Iterator iter = specimens.values().iterator();
        while ((iter != null) && (iter.hasNext())) {
            // Put together a specimen button
            Object obj = iter.next();
            // TODO: Change the code below so that the class types are not hardcoded.
            if (obj instanceof OfflineSpecimen)
                curBttn = new SpecimenButton((OfflineSpecimen) obj);
            else
                curBttn = new SpecimenButton((Specimen) obj);
            curBttn.getSpecimenButtonOrDownloadButton().addActionListener(m_specimenButtonListener);
            // Add it to chooser panel
            container.add(curBttn);
            // Remember current button dimenstions
            curBttnDim = curBttn.getMinimumSize();
            minWidth = Math.max(minWidth, curBttnDim.width);
            minHeight = Math.max(minHeight, curBttnDim.height);
        }
        // Set minimum chooser size to be that of the largest button.
        container.setMinimumSize(new Dimension(minWidth, minHeight));
    }

    /**
     * This method adds new specimen buttons/labels to the panel, and removes obsolete buttons/labels.
     * synchronization issues.
     *
     * @param specimens set of specimens
     * @param container The panel that contains the specimen buttons.
     */
    private void refreshSpecimensInPanel(final LinkedHashMap newSpecimens, JPanel container, LinkedHashMap oldSpecimens) {
        SpecimenButton curBttn = null;
        Dimension curBttnDim = null;
        int minWidth = 0;
        int minHeight = 0;

        //removeSpecimens(container);
        //container.add(specimenPanelLabel);
        Iterator iter = newSpecimens.values().iterator();
        while ((iter != null) && (iter.hasNext())) {
            // Put together a specimen button
            Specimen obj = (Specimen) iter.next();
            if (oldSpecimens.containsKey(obj.getUniqueName()))
                continue;
            // TODO: Change the code below so that the class types are not hardcoded.
            if (obj.getSpecimenType() == Specimen.OFFLINE_SPECIMEN)
                curBttn = new SpecimenButton((OfflineSpecimen) obj);
            else
                curBttn = new SpecimenButton((Specimen) obj);
            curBttn.getSpecimenButtonOrDownloadButton().addActionListener(m_specimenButtonListener);
            // Add it to chooser panel
            container.add(curBttn);
            // Remember current button dimenstions
            curBttnDim = curBttn.getMinimumSize();
            minWidth = Math.max(minWidth, curBttnDim.width);
            minHeight = Math.max(minHeight, curBttnDim.height);
        }
        //by now all the new specimens have been added. Now we need to remove any obsolete specimens.
        removeObsleteSpecimensFromPanel(newSpecimens, oldSpecimens, container);
        // Set minimum chooser size to be that of the largest button.
        container.setMinimumSize(new Dimension(minWidth, minHeight));
        //m_onlineSpecimenFlowPanel.revalidate();
    }
}

/**
 * This class handles the offline specimens display
 *
 */
class ScrollableOfflineSpecimenFlowPanel extends JScrollPane {
    static JPanel pane;

    public ScrollableOfflineSpecimenFlowPanel(JPanel view, Dimension minimumSize) {
        super(pane = view);
        pane.setLayout(new FlowLayout(FlowLayout.LEFT));
        getVerticalScrollBar().setUnitIncrement(20);
        setMinimumSize(minimumSize);
        setOpaque(false);
        setBorder(new EmptyBorder(0, 0, 0, 0));

        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Insets i = c.getInsets();
                pane.setPreferredSize(new Dimension(c.getWidth() - i.left
                        - i.right, c.getHeight()));
                pane.revalidate();
            }
        });

        pane.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Dimension currentPrefSize = c.getPreferredSize();
                if (c.getComponentCount() > 0) {
                    int newHeight = 0, tmpHeight = 0;
                    for (int i = 0; i < c.getComponentCount(); ++i) {
                        Component lastComp = c.getComponent(i);
                        tmpHeight = lastComp.getLocation().y
                                + lastComp.getHeight();
                        newHeight = Math.max(newHeight, tmpHeight);
                    }
                    newHeight += c.getInsets().bottom;

                    // 5 pixels added to height so that we get some trailing
                    // space at the bottom.
                    currentPrefSize.height = newHeight + 5;
                    if (getVerticalScrollBar().isVisible()) {
                        // If the vertical scroll bar has appeared we need to
                        // make sure we wrap to next line considerig the scroll
                        // bar width.
                        currentPrefSize.width = currentPrefSize.width - getVerticalScrollBar().getWidth();
                    }

                }
                c.setPreferredSize(currentPrefSize);
                c.revalidate();
            }
        });
    }
}
/**
 * This class handles the offline specimens display
 *
 */
class ScrollableOnlineSpecimenFlowPanel extends JScrollPane {
    static JPanel pane;

    public ScrollableOnlineSpecimenFlowPanel(JPanel view, Dimension minimumSize) {
        super(pane = view);
        setMinimumSize(minimumSize);
        pane.setLayout(new FlowLayout(FlowLayout.LEFT));
        getVerticalScrollBar().setUnitIncrement(20);
        //setLayout(new FlowLayout(FlowLayout.LEFT));

        setOpaque(false);
        setBorder(new EmptyBorder(0, 0, 0, 0));

        setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Insets i = c.getInsets();
                pane.setPreferredSize(new Dimension(c.getWidth() - i.left
                        - i.right, c.getHeight()));
                pane.setAlignmentX(JPanel.LEFT_ALIGNMENT);
                pane.revalidate();
            }
        });

        pane.addComponentListener(new ComponentAdapter() {
            public void componentResized(ComponentEvent ev) {
                JComponent c = (JComponent) ev.getSource();
                Dimension currentPrefSize = c.getPreferredSize();
                if (c.getComponentCount() > 0) {
                    int newHeight = 0, tmpHeight = 0;
                    for (int i = 0; i < c.getComponentCount(); ++i) {
                        Component lastComp = c.getComponent(i);
                        tmpHeight = lastComp.getLocation().y
                                + lastComp.getHeight();
                        newHeight = Math.max(newHeight, tmpHeight);
                    }
                    newHeight += c.getInsets().bottom;

                    // 5 pixels added to height so that we get some trailing
                    // space at the bottom.
                    currentPrefSize.height = newHeight + 5;
                    if (getVerticalScrollBar().isVisible()) {
                        // If the vertical scroll bar has appeared we need to
                        // make sure we wrap to next line considerig the scroll
                        // bar width.
                        currentPrefSize.width = currentPrefSize.width - getVerticalScrollBar().getWidth();
                    }
                }
                c.setPreferredSize(currentPrefSize);
                c.setAlignmentX(JComponent.LEFT_ALIGNMENT);
                c.revalidate();
            }
        });
    }
}