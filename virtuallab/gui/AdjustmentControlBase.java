/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import virtuallab.Specimen;
import virtuallab.util.Constants;
import virtuallab.util.ControlBase;
import virtuallab.util.Locale;

//$Id: AdjustmentControlBase.java,v 1.15 2006/09/05 01:08:47 manzoor2 Exp $

public class AdjustmentControlBase extends ControlBase implements MouseListener, SideControl, Stateful, ChangeListener, ActionListener {
    private static final long serialVersionUID = -2611161044872040600L;
    protected JSlider m_slider;
    protected Microscope m_microscope;
    protected Specimen.ControlDescriptor m_controlDescriptor;
    protected Collection m_validControlStates;
    protected int m_minValue = 0;
    protected int m_maxValue = 100;
    protected int m_defaultValue = 50;
    protected String m_controlName;

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    /**
     * This method captures the double clicks and upon
     * a double click it sets the value of the control to
     * the default value.
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            // double-click
            m_slider.setValue(m_defaultValue);
        }
    }

    /**
     * Initializes this control as a side control.
     *
     * @param controlName unique name/type of this control
     * @param controlDesc control descriptor
     * @param scope       Microscope object
     */
    public void initSideControl(String controlName, Specimen.ControlDescriptor controlDesc, Microscope scope) {
        if (controlDesc == null || scope == null || controlName == null) {
            throw new IllegalArgumentException("Null control descriptor, microscope, or name");
        }

        m_controlName = controlName;
        m_microscope = scope;
        m_controlDescriptor = controlDesc;
        m_title = Locale.translateSideMenuTitles(controlDesc.getNamedParam(Constants.XMLTags.DISPLAY_STRING));

        if (m_title == null || m_title.equals("")) {
            m_title = Locale.Choices(virtuallab.Configuration.getApplicationLanguage());
        }

        updateGUI();
    }

    /**
     * @return name/type of this control
     */
    public String getControlName() {
        return m_controlName;
    }

    private void updateGUI() {
        removeAll();
        setMaximumSize(new Dimension(1024, getMinimumSize().height));
//    setPreferredSize(new Dimension(150, 110)); // with +/-/Default buttons.
        setPreferredSize(new Dimension(150, 70));

        // Init slider
        m_slider = new JSlider(JSlider.HORIZONTAL, m_minValue, m_maxValue, m_defaultValue);
        m_slider.setMajorTickSpacing(25);
        m_slider.setMinorTickSpacing(5);
        m_slider.setPaintTicks(true);
        m_slider.setPaintLabels(true);
        m_slider.setMinimumSize(new Dimension(100, 0));
        m_slider.setToolTipText(Locale.Adjust(virtuallab.Configuration.getApplicationLanguage())+Constants.SPACE+m_title);

        updateButtonStates();

        // Add listeners
        if (m_slider.getChangeListeners().length == 0) {
            m_slider.addChangeListener(this);
            m_slider.addMouseListener(this);
        }

        // Button layout panel
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 5));


        // Layout
        setBorder(BorderFactory.createTitledBorder(m_title));
        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
        add(m_slider);
    }

    /**
     * TODO: Figure out if this method does anyhting. It appears to be a noop method.
     *
     * @param controlStates The Collection containing all the allowable states for the control.
     * @param version       indicates the version of the XML.
     */


    public void setCurrentControlState(java.util.Collection controlStates, double version) {
        if (version <= 1.0) {
            setCurrentControlState(controlStates);
        } else if (controlStates != null) {        // Find this control in the control-states argument and set its
            // current control state to the control state there.
            Iterator iter = controlStates.iterator();
            while (iter.hasNext()) {
                Specimen.ControlStates cs = (Specimen.ControlStates) iter.next();
                if (cs.getControlDescriptor(version) == m_controlDescriptor) {
//				String s = cs.getDisplayString();
                    //System.out.println("Match found " + s);
                    //m_currentValue = s != null ? s : "";
                    break;
                }
            }
        }
    }

    public String getId() {
        return m_controlDescriptor.getId();
    }

    public int getRawValue() {
        return m_slider.getValue();
    }

    public double getValue() {
        return ((double) m_slider.getValue()) / (double) m_maxValue;
    }

    /**
     * TODO: Figure out if this method is doing anything. It looks like a noop method.
     */
    public Specimen.ControlState getCurrentControlState() {
        if (m_validControlStates == null)
            return null;

        Iterator iter = m_validControlStates.iterator();

        while (iter.hasNext()) {
            Specimen.ControlState cs = (Specimen.ControlState) iter.next();
//      String s = cs.getDisplayString();
            //if (s != null && s.equals(m_currentValue)) {
            //  return cs;
            //}
        }
        return null;
    }

    /**
     * This method sets the allowable control states.
     * TODO: Figure out if this method is doing anything. It appears to be noop method.
     *
     * @param controlStates The Collection containing all the allowable states for the control.
     */
    private void setCurrentControlState(java.util.Collection controlStates) {
        if (controlStates == null) {
            return; // TODO: Should this throw an exception instead?
        }

        // Find this control in the control-states argument and set its
        // current control state to the control state there.
        Iterator iter = controlStates.iterator();
        while (iter.hasNext()) {
            Specimen.ControlState cs = (Specimen.ControlState) iter.next();
            if (cs.getControlDescriptor() == m_controlDescriptor) {
                //String s = cs.getDisplayString();
                //m_currentValue = s != null ? s : "";
                break;
            }
        }
    }

    /**
     * TODO: Figure out if this method is doing anything. It looks like a noop method.
     */
    public Specimen.ControlStates getCurrentControlState(double version) {
        if (m_validControlStates == null)
            return null;

        Iterator iter = m_validControlStates.iterator();

        while (iter.hasNext()) {
            Specimen.ControlStates cs = (Specimen.ControlStates) iter.next();
//	        String s = cs.getDisplayString();
            //if (s != null && s.equals(m_currentValue)) {
            //  return cs;
            //}
        }
        return null;
    }

    /**
     * This method is effectively just a setter for the m_validControlstates.
     * The ControlStates HashMap contains all the valid values that this control can take
     * Each of this valid value is stored as a ControlStates - which represents a node
     * in the Specimen Data tree.
     *
     * @param controlStates the Map containing the valid states.
     * @param version       double the version of the XML.
     */
    public void setValidControlStates(Map controlStates, double version) {
        if (controlStates == null) {
            m_validControlStates = null;
            return;
        }

        m_validControlStates = (Collection) controlStates.get(m_controlDescriptor);

        updateGUI();
    }

    /**
     * Called when slider is adjusted.
     */
    public void stateChanged(ChangeEvent e) {
        updateButtonStates();
        m_microscope.updateSpecimenControlStates();
    }

    /**
     * Called when button is pressed.
     */
    public void actionPerformed(ActionEvent e) {
//    JButton thisButton = (JButton) e.getSource();
//    int i = m_slider.getValue();

//    if (thisButton == m_minusButton)
//    { m_slider.setValue(Math.max(i - 5, m_minValue));
//    }
//    else if (thisButton == m_plusButton)
//    { m_slider.setValue(Math.min(i + 5, m_maxValue));
//    }
//    else if (thisButton == m_defaultButton)
//    if (thisButton == m_defaultButton)
//    { m_slider.setValue(m_defaultValue);
//    }

    }

    /**
     * Enables/disables plus and minus buttons depending on current selection.
     */
    protected void updateButtonStates() {
//    int i = m_slider.getValue();

//    m_minusButton.setEnabled(i != m_minValue);
//    m_plusButton.setEnabled(i != m_maxValue);
    }

    public boolean valueIsAdjusting() {
        return m_slider.getValueIsAdjusting();
    }
}