/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SpringLayout;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;

import virtuallab.Configuration;
import virtuallab.OfflineSpecimen;
import virtuallab.Specimen;
import virtuallab.SpecimenElement;
import virtuallab.util.Constants;
import virtuallab.util.Locale;
import virtuallab.util.Utility;

import virtuallab.Log;

/**
 * This is the DetailChooserView of the specimen chooser functionality. This
 * view shows the specimen thumbnails split into two portions. One represents
 * the online specimens (i.e. the specimens that the user has already downloaded
 * to his machine) the other shows thumbnails corresponding to the offline
 * specimens (the ones that are available on the virtualweb site but hasn't yet
 * been downloaded by the user). This view presents the specimens with their
 * detailed description followed by the thumbnail.
 *
 */
public class DetailChooserView extends AbstractChooserView {

    private static final Log log = new Log(DetailChooserView.class.getName());

    private JSplitPane m_splitPane;

    private JScrollPane m_onlineScrollPane;

    private JScrollPane m_offlineScrollPane;

    public DetailChooserView() {
        super();
        m_splitPane = null;
    }

    public String menuText() {
        return DETAIL;
    }

    public short getViewType() {
        return AbstractChooserView.DETAIL_VIEW;
    }

    /**
     * Initializes the view such that it is in a displayable state.
     *
     * @param config
     * @param specimens
     * @param offlineSpecimens
     * @param listener
     */
    public void initialize(Configuration config, LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens, ActionListener listener) {
        m_specimenButtonListener = listener;
        m_config = config;
        setSpecimens(onlineSpecimens, offlineSpecimens);
        setLayout(new BorderLayout());
        createSpecimenPanels();
        addSplitPaneBetweenPanels(m_onlineScrollPane, m_offlineScrollPane);
        setOpaque(false);
    }

    /**
     * This method creates the online and offline specimen.
     */
    private void createSpecimenPanels() {
        if (m_onlineScrollPane == null) {
            m_onlineScrollPane = new JScrollPane(m_onlineChooserPanel);
            m_onlineScrollPane.getVerticalScrollBar().setUnitIncrement(20);
        }
        if (m_offlineScrollPane == null) {
            m_offlineScrollPane = new JScrollPane(m_offlineChooserPanel);
            m_offlineScrollPane.getVerticalScrollBar().setUnitIncrement(20);
        }

        createSpecimenPanel(m_onlineScrollPane);
        createSpecimenPanel(m_offlineScrollPane);
    }

    private void createSpecimenPanel(JScrollPane scrollPaneToCreate) {
        scrollPaneToCreate.setOpaque(false);
        scrollPaneToCreate.setMinimumSize(getMinimumPaneSize());
        scrollPaneToCreate.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPaneToCreate.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        /**
         * This makes sure that the scroll bar is set to top when the
         * application initializes
         */
        scrollPaneToCreate.addComponentListener(new ResizeListener());
    }

    /**
     * Creates the splitpane that will contains the online specimens on top and
     * offline specimens at the bottom.
     *
     * @param up
     * @param down
     */
    private void addSplitPaneBetweenPanels(JScrollPane topComponent, JScrollPane bottomComponent) {
        m_splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, topComponent,
                bottomComponent);
        m_splitPane.setResizeWeight(1.0);

        if (m_offlineChooserPanel.getComponentCount() <= 1) {
            if (m_onlineChooserPanel.getComponentCount() <= 1) {
                // no online AND offline specimens available.
                m_splitPane.setResizeWeight(1);
            } else {
                // no offline specimens available but online specimens are
                // available.
                m_splitPane
                        .setDividerLocation(Constants.VIEWER_PREFERRED_DIM.height);
            }
        } else if (m_onlineChooserPanel.getComponentCount() <= 1) {
            // offline specimens are available but no online available.
            m_splitPane.setDividerLocation(0);
        } else
            m_splitPane.setResizeWeight(1);
        m_splitPane.setOneTouchExpandable(true);
        add(m_splitPane, BorderLayout.CENTER);
    }

    /**
     * This method is a brute force approach. It removes ALL the specimens from
     * the panel and then rereads them from the disk. This method has lot of
     * performance over head. It is best to use refreshSpecimens() instead
     * whenever possible. The only time when using this method makes sense is
     * when the View is created the very first time.
     */

    public void setSpecimens(LinkedHashMap onlineSpecimens, LinkedHashMap offlineSpecimens) {
        JPanel offLineHeaderPanel;
        JPanel onlineHeaderPanel;
        if (onlineSpecimens.size() > 0)
            onlineHeaderPanel = createPanelHeader(Locale.MySpecimenCollection(Configuration.getApplicationLanguage()),
                    Locale.ClickToView(Configuration.getApplicationLanguage()), Color.BLACK, m_onlineChooserPanel.getBackground());
        else
            onlineHeaderPanel = createPanelHeader(Locale.MySpecimenCollection(Configuration.getApplicationLanguage()),
                    Locale.NoSpecimensDownloaded(Configuration.getApplicationLanguage()), Color.BLACK,
                    m_onlineChooserPanel.getBackground());
        displaySpecimensInPanel(onlineSpecimens, m_onlineChooserPanel, onlineHeaderPanel);

        if (offlineSpecimens.size() > 0)
            offLineHeaderPanel = createPanelHeader(Locale.GetMoreSpecimens(Configuration.getApplicationLanguage()),
                    Locale.ClickToDownload(Configuration.getApplicationLanguage()), Color.WHITE, m_offlineChooserPanel.getBackground());
        else
            offLineHeaderPanel = createPanelHeader(Locale.GetMoreSpecimens(Configuration.getApplicationLanguage()),
                    Locale.NoSpecimensDownloaded(Configuration.getApplicationLanguage()), Color.WHITE, m_offlineChooserPanel.getBackground());
        displaySpecimensInPanel(offlineSpecimens, m_offlineChooserPanel, offLineHeaderPanel);
    }

    /**
     * This method refreshes the panel and updates the panel with any new online
     * specimens that may have been downloaded since the last refresh. It also
     * removes offline specimens if they have been successfully downloaded - in
     * which case these removed specimens will infact be moved to online panel.
     * Notice that we clone the specimens. This avoids synchronization problems,
     * since we are only reading out of the specimens Collection, cloning is
     * safe. Not doing this and passing the original specimens collection will
     * most certainly result in Concurrentmodification exception in case when
     * multiple downloads finish at the same time.
     */
    public void refreshSpecimens(LinkedHashMap onlineSpecimens,
                                 LinkedHashMap offlineSpecimens, LinkedHashMap oldOnlineSpecimens,
                                 LinkedHashMap oldOfflineSpecimens) {
        int onlineSpecimenCount = oldOnlineSpecimens.size();
        int offlineSpecimenCount = oldOfflineSpecimens.size();

        if (onlineSpecimenCount == 0 || offlineSpecimenCount == 0) {
            // if we did not have anything before we must go all the way i.e
            // recreating everything
            // especially the header - since since the header text must be
            // changed to reflect
            // that now we do have some specimens available.
            setSpecimens((LinkedHashMap) onlineSpecimens.clone(),
                    (LinkedHashMap) offlineSpecimens.clone());
        } else {
            refreshSpecimensInPanel((LinkedHashMap) onlineSpecimens.clone(),
                    m_onlineChooserPanel, oldOnlineSpecimens);
            refreshSpecimensInPanel((LinkedHashMap) offlineSpecimens.clone(),
                    m_offlineChooserPanel, oldOfflineSpecimens);
            // this will make sure we have the latest status of the scrollbars
            // later when we need to refresh scroll bars.
        }

        if (onlineSpecimenCount == 0 && onlineSpecimens.size() > 0) {
            // if there was no online specimen and now there is one, then we should
            // show the online specimen panel.
            int onlinePanelHeight = SpecimenButton.SIZE_Y + 20;
//			    if (m_onlineChooserPanel.getComponentCount() > 0 &&
//			    		m_onlineChooserPanel.getComponent(0) instanceof JPanel) {
//			    	onlinePanelHeight = ((JPanel)m_onlineChooserPanel.getComponent(0)).getPreferredSize().height + 10;
//			    }
            m_splitPane.setDividerLocation(onlinePanelHeight);
        }


        this.revalidate();
        // making sure that the newly added specimen is visible.
        m_onlineScrollPane.getViewport().setViewPosition(
                new java.awt.Point(0, 20000));
        if (offlineSpecimens.size() == 0) {
            // no more offline specimens so we should hide the offline specimens
            // panel.
            m_splitPane.setDividerLocation(2000);
        }
        this.revalidate();
    }

    /**
     * This method iterates over all the components and find the "download"
     * button corresponding to the given specimenId. It then enables/disables it
     * as specified.
     *
     * @param specimenID
     * @param enabled    indictaes if the button should be enabled or disabled.
     */
    public void setSpecimenButtonEnabled(String specimenID, boolean enabled) {
        for (int i = 0; i < m_offlineChooserPanel.getComponentCount(); i++) {
            Component specimenDetail = m_offlineChooserPanel.getComponent(i);
            if (specimenDetail.getName() != null
                    && specimenDetail.getName().equals(specimenID)) {
                // Found the outer panel that contains the information related
                // to the given specimen.
                // This panel contains an inner panel (as its first component)
                // which actually
                // holds the specimen description and the thumbnail. The first
                // element of
                // this outer panel should be JPanel which contains all the text
                // and the thumbnail.
                JPanel detailPanel = (JPanel) ((JPanel) specimenDetail)
                        .getComponent(0);
                for (int j = 0; j < detailPanel.getComponentCount(); j++) {
                    Component button = detailPanel.getComponent(j);
                    if (button instanceof SpecimenButton) {
                        // found the download button for the given specimen.
                        ((SpecimenButton) button)
                                .getSpecimenButtonOrDownloadButton()
                                .setEnabled(enabled);
                        return;
                    }
                }
            }
        }
    }

    /**
     * Sets chooser's specimen.
     *
     * @param specimens set of specimens
     * @param container The panel that contains the specimen buttons and their respective
     *                  details.
     */
    private void displaySpecimensInPanel(final LinkedHashMap specimens, JPanel container, JPanel panelHeader) {
        SpecimenButton curBttn = null;
        JPanel detailsPanel = null;

        removeSpecimens(container);
        container.setLayout(new BoxLayout(container, BoxLayout.PAGE_AXIS));
        container.add(panelHeader);
        Iterator iter = specimens.values().iterator();
        while ((iter != null) && (iter.hasNext())) {
            try {
                JPanel specimenContainer = createSpecimenDetailPanel(container, (Specimen) iter.next());
                // JPanel specimenContainer = new JPanel();
                // // Put together a specimen button
                // Specimen obj = (Specimen) iter.next();
                // int r = container.getBackground().getRed();
                // int g = container.getBackground().getGreen();
                // int b = container.getBackground().getBlue();
                // // TODO: Change the code below so that the class types are
                // not hardcoded.
                // if (obj instanceof OfflineSpecimen){
                // curBttn = new SpecimenButton((OfflineSpecimen)obj, false);
                // curBttn.setBackground(container.getBackground());
                // curBttn.setBorder(new EmptyBorder(0,0,0,0));
                // detailsPanel =
                // createOfflineTextDetailsPanel((OfflineSpecimen)obj,curBttn,r,g,b);
                // }else{
                // curBttn = new SpecimenButton(obj, false);
                // curBttn.setBackground(container.getBackground());
                // detailsPanel =
                // createOnlineTextDetailsPanel(obj,curBttn,r,g,b);
                // }
                // //specimenContainer.setBorder(BorderFactory.createTitledBorder(obj.getNamedSampleInfoParam(Constants.XMLTags.NAME)));
                // specimenContainer.setName(obj.getUniqueName());
                // specimenContainer.setBackground(container.getBackground());
                // curBttn.getSpecimenButtonOrDownloadButton().addActionListener(m_specimenButtonListener);
                // specimenContainer.add(detailsPanel);
                // specimenContainer.add(curBttn);
                container.add(specimenContainer);
            } catch (Exception ex) {
                log.error("Could not create specimen container!");
                log.error(ex.getMessage());
            }
        }
        // Set minimum chooser size to be that of the largest button.
        // container.setMinimumSize(new Dimension(minWidth, minHeight));
    }

    /**
     * This method adds new specimen buttons/labels to the panel, and removes
     * obsolete buttons/labels.
     *
     * @param specimens set of specimens
     * @param container The panel that contains the specimen buttons.
     */
    private/* synchronized */void refreshSpecimensInPanel(final LinkedHashMap newSpecimens, JPanel container, LinkedHashMap oldSpecimens) {
        SpecimenButton curBttn = null;
        JPanel detailsPanel = null;

        Iterator iter = newSpecimens.values().iterator();
        while ((iter != null) && (iter.hasNext())) {
            try {
                Specimen specimen = (Specimen) iter.next();
                if (oldSpecimens.containsKey(specimen.getUniqueName()))
                    continue;// already loaded specimen.
                JPanel specimenContainer = createSpecimenDetailPanel(container,
                        (Specimen) specimen);
                // new specimen has been found,must add this to the panel.
                // JPanel specimenContainer = new JPanel();
                // int r = container.getBackground().getRed();
                // int g = container.getBackground().getGreen();
                // int b = container.getBackground().getBlue();
                // // TODO: Change the code below so that the class types are
                // not hardcoded.
                // if (obj instanceof OfflineSpecimen){
                // curBttn = new SpecimenButton((OfflineSpecimen)obj, false);
                // curBttn.setBackground(container.getBackground());
                // curBttn.setBorder(new EmptyBorder(0,0,0,0));
                // detailsPanel =
                // createOfflineTextDetailsPanel((OfflineSpecimen)obj,curBttn,r,g,b);
                // }else{
                // curBttn = new SpecimenButton(obj, false);
                // curBttn.setBackground(container.getBackground());
                // detailsPanel =
                // createOnlineTextDetailsPanel(obj,curBttn,r,g,b);
                // }
                // curBttn.getSpecimenButtonOrDownloadButton().addActionListener(m_specimenButtonListener);
                // specimenContainer.setName(obj.getUniqueName());
                // specimenContainer.setBackground(container.getBackground());
                // specimenContainer.add(detailsPanel);
                // specimenContainer.add(curBttn);
                // Add it to chooser panel
                container.add(specimenContainer);
            } catch (Exception ex) {
                log.error("Could not create specimen container!");
                log.error(ex.getMessage());
            }
        }
        // by now all the new specimens have been added. Now we need to remove
        // any obsolete specimens.
        removeObsleteSpecimensFromPanel(newSpecimens, oldSpecimens, container);
    }

    /**
     * This method takes in a specimen and creates a JPanel that will hold all
     * the specimen details along with its thumbnail.
     *
     * @param container
     * @param specimen
     * @return
     */
    private JPanel createSpecimenDetailPanel(JPanel container,
                                             final Specimen specimen) {
        JPanel specimenContainer = new JPanel();
        SpecimenButton specimenThumbnail = null;
        JPanel detailsPanel = null;

        // Put together a specimen button
        int r = container.getBackground().getRed();
        int g = container.getBackground().getGreen();
        int b = container.getBackground().getBlue();
        // TODO: Change the code below so that the class types are not
        // hardcoded.
        if (specimen.getSpecimenType() == SpecimenElement.OFFLINE_SPECIMEN) {
            specimenThumbnail = new SpecimenButton((OfflineSpecimen) specimen,
                    false);
            specimenThumbnail.setBackground(container.getBackground());
            specimenThumbnail.setBorder(new EmptyBorder(0, 0, 0, 0));
            detailsPanel = createOfflineTextDetailsPanel(
                    (OfflineSpecimen) specimen, specimenThumbnail, r, g, b);
        } else {
            specimenThumbnail = new SpecimenButton(specimen, false);
            specimenThumbnail.setBackground(container.getBackground());
            detailsPanel = createOnlineTextDetailsPanel(specimen,
                    specimenThumbnail, r, g, b);
        }
        specimenContainer.setName(specimen.getUniqueName());
        specimenContainer.setBackground(container.getBackground());
        specimenThumbnail.getSpecimenButtonOrDownloadButton()
                .addActionListener(m_specimenButtonListener);
        specimenContainer.add(detailsPanel);
        return specimenContainer;
    }

    /**
     * This method looks at the new specimens and removes all the detail panels
     * and buttons that were present in old specimens panel but are not needed
     * in the new specimen panel.
     *
     * @param newSpecimens
     * @param oldSpecimens
     */
    protected void removeObsleteSpecimensFromPanel(LinkedHashMap newSpecimens,
                                                   LinkedHashMap oldSpecimens, JPanel specimenPanel) {
        Iterator oldSpecimensIterator = oldSpecimens.keySet().iterator();
        while (oldSpecimensIterator.hasNext()) {
            String specimenID = (String) oldSpecimensIterator.next();
            if (!newSpecimens.containsKey(specimenID)) {
                for (int i = 0; i < specimenPanel.getComponentCount(); i++) {
                    Component specimenDetail = specimenPanel.getComponent(i);
                    if (specimenDetail.getName() != null
                            && specimenDetail.getName().equals(specimenID)) {
                        // ((SpecimenButton)
                        // specimenButton).getSpecimenButtonOrDownloadButton()
                        // .removeActionListener(m_specimenButtonListener);
                        // TODO: remove al the controls contained with in the
                        // detail.
                        // TODO: remove the actionlistener for the button.
                        specimenPanel.remove(specimenDetail);
                        break;
                    }
                }
            }
        }
    }

    /**
     * This method creates a panel that contains the details (e.g. name,
     * description etc.) of the input specimen which is placed left of the input
     * specimen thumbnail. Returns the panel containing both the details and the
     * specimen thumbnail.
     *
     * @param aSpecimen
     * @return JPanel containing the detils of the specimen.
     */
    private JPanel createOfflineTextDetailsPanel(OfflineSpecimen aSpecimen,
                                                 JPanel thumbnail, int r, int g, int b) {
        String specimenStats = "<tr style=\"color:rgb(228,229,246); font-size:12pt; font-family:sans-serif;\"><td>"
                + aSpecimen
                .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_STATS)
                + "</td></tr><tr><td>&nbsp;</td></tr>";
        String prefix = "<html><body bgcolor='rgb(" + r + "," + g + "," + b
                + ")'><table>";
        String postfix = "</table></body></html>";
        String text = aSpecimen
                .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION);

        JPanel textDetailsPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        textDetailsPanel.setLayout(layout);

        textDetailsPanel.setBackground(new Color(r, g, b));
        Font labelFont = new Font("Tahoma", Font.BOLD, 13);
        textDetailsPanel.setBorder(BorderFactory.createTitledBorder(
                new LineBorder(new Color(169, 169, 169)), " "
                        + aSpecimen.getSampleInfo().getNamedParam(
                        Constants.XMLTags.NAME) + " ",
                TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, labelFont, Color.WHITE));
        // JTextPane textDetails = new JTextPane();
        NoWrapTextPane textDetails = new NoWrapTextPane();
        textDetails.setBackground(new Color(r, g, b));
        HTMLDocument document = new HTMLDocument();
        HTMLEditorKit kit = new HTMLEditorKit();
        textDetails.setContentType("text/html");
        textDetails.setEditable(false);
        textDetails.setEditorKit(kit);
        textDetails.setDocument(document);

        if (text == null) {
            text = prefix + "<font color=red>"+Locale.NoDescriptionFound(Configuration.getApplicationLanguage())+".</font>" + postfix;
        } else {
            String textWithLineBreaks = insertLineBreaks(text);
            text = specimenStats
                    + "<tr style=\"color:white; font-size:14pt; font-family:serif;\"><td>"
                    + textWithLineBreaks
                    + "</td></tr><tr><td>&nbsp;</td></tr>"
                    + "<tr style=\"color:rgb(228,229,246); font-size:12pt; font-family:sans-serif;\">"
                    + "<td>"+Locale.DownloadSize(Configuration.getApplicationLanguage())+": "
                    + Utility.formatDouble(
                    (double) aSpecimen.getJarFileSize() / 1048576D, 1)
                    + " MB</td></tr>";
        }
        textDetails.setText(prefix + text + postfix);
        //System.out.print("Specimen " + aSpecimen.getDisplayString() + " => ");
        int footerSpace = 15;
        int extraLines = 5;
        String operatingSystem = System.getProperty("os.name");
        boolean requireExtraPadding = true;
        if (operatingSystem != null &&
                operatingSystem.toLowerCase().indexOf("windows") > -1 ||
                operatingSystem.toLowerCase().indexOf("nt") > -1) {
            requireExtraPadding = false;
//			System.out.println("Windows operating system");
        }
        if (requireExtraPadding) {
            extraLines += 1;// mac rendering is slightly different than Windows, so
            // we need to one extra line to get the height correct.
        }
        int height = computePanelHeight(
                aSpecimen
                        .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION),
                thumbnail, extraLines);
        textDetailsPanel.setPreferredSize(new Dimension(650,
                (height + footerSpace)));

        layout.putConstraint(SpringLayout.WEST, textDetails, 5,
                SpringLayout.WEST, textDetailsPanel);
        layout.putConstraint(SpringLayout.NORTH, textDetails, 5,
                SpringLayout.NORTH, textDetailsPanel);
        textDetailsPanel.add(textDetails);
        layout.putConstraint(SpringLayout.EAST, thumbnail, -5,
                SpringLayout.EAST, textDetailsPanel);
        layout.putConstraint(SpringLayout.NORTH, thumbnail, -10,
                SpringLayout.NORTH, textDetailsPanel);
        textDetailsPanel.add(thumbnail);

        return textDetailsPanel;
    }

    /**
     * This method creates a panel that contains the details (e.g. name,
     * description etc.) for the input specimen.
     *
     * @param aSpecimen
     * @return JPanel containing the detils of the specimen.
     */
    private JPanel createOnlineTextDetailsPanel(Specimen aSpecimen,
                                                JPanel thumbnail, int r, int g, int b) {
        String prefix = "<html><body bgcolor='rgb(" + r + "," + g + "," + b
                + ")'><table>";
        String specimenStats = "<tr style=\"color:gray; font-size:12pt; font-family:sans-serif;\"><td>"
                + aSpecimen
                .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_STATS)
                + "</td></tr><tr><td>&nbsp;</td></tr>";
        String postfix = "</table></body></html>";
        String text = aSpecimen
                .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION);

        JPanel textDetailsPanel = new JPanel();
        SpringLayout layout = new SpringLayout();
        textDetailsPanel.setLayout(layout);

        textDetailsPanel.setBackground(new Color(r, g, b));
        Font labelFont = new Font("Tahoma", Font.BOLD, 13);
        textDetailsPanel.setBorder(BorderFactory.createTitledBorder(
                new LineBorder(new Color(176, 176, 176)), " "
                        + aSpecimen.getDisplayString() + " ",
                TitledBorder.DEFAULT_JUSTIFICATION,
                TitledBorder.DEFAULT_POSITION, labelFont, Color.BLACK));

        NoWrapTextPane textDetails = new NoWrapTextPane();
        textDetails.setBackground(new Color(r, g, b));
        HTMLDocument document = new HTMLDocument();
        HTMLEditorKit kit = new HTMLEditorKit();
        textDetails.setContentType("text/html");
        textDetails.setEditable(false);
        textDetails.setEditorKit(kit);
        textDetails.setDocument(document);
        int height = 0;

        if (text == null) {
            text = prefix + "<font color=red>"+Locale.NoDescriptionFound(Configuration.getApplicationLanguage())+".</font>"
                    + postfix;
        } else {
            String textWithLineBreaks = insertLineBreaks(text);
            text = specimenStats
                    + "<tr style=\"color:rgb(28,28,28); font-size:14pt; font-family:serif;\"><td>"
                    + textWithLineBreaks
                    + "</td></tr>";
        }
        textDetails.setText(prefix + text + postfix);
        //System.out.print("Specimen " + aSpecimen.getDisplayString() + " => ");
        int footerSpace = 28;

        int extraLines = 2;
        String operatingSystem = System.getProperty("os.name");
        boolean requireExtraPadding = true;
        if (operatingSystem != null &&
                operatingSystem.toLowerCase().indexOf("windows") > -1 ||
                operatingSystem.toLowerCase().indexOf("nt") > -1) {
            requireExtraPadding = false;
        }
        if (requireExtraPadding) {
            extraLines += 1;// mac rendering is slightly different than Windows, so
            // we need to one extra line to get the height correct.
        }

        height = computePanelHeight(
                aSpecimen
                        .getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION),
                thumbnail, extraLines);
        if (height > thumbnail.getPreferredSize().getHeight()) {
            // the text detail has the greatest height, it requires a bit more
            // footer spave than thimbnail.
            footerSpace += 8;
        }
        textDetailsPanel.setPreferredSize(new Dimension(650,
                (height + footerSpace)));

        layout.putConstraint(SpringLayout.WEST, textDetails, 5,
                SpringLayout.WEST, textDetailsPanel);
        layout.putConstraint(SpringLayout.NORTH, textDetails, 5,
                SpringLayout.NORTH, textDetailsPanel);
        textDetailsPanel.add(textDetails);

        layout.putConstraint(SpringLayout.EAST, thumbnail, -5,
                SpringLayout.EAST, textDetailsPanel);
        layout.putConstraint(SpringLayout.NORTH, thumbnail, -5,
                SpringLayout.NORTH, textDetailsPanel);
        textDetailsPanel.add(thumbnail);

        return textDetailsPanel;
    }

    /**
     * This method takes in the raw text and puts <BR>
     * in appropriate position. This will let us break the description at the
     * right place so that we can see the description properly formatted
     *
     * @param rawText
     * @return the description text with <BR>
     * inserted at appropriate position.
     */
    private String insertLineBreaks(String rawText) {
        JLabel dummyLabel = new JLabel();
        Font labelFont = new Font("Serif", Font.PLAIN, 14);
        dummyLabel.setFont(labelFont);
        dummyLabel.setText(rawText);
        FontMetrics fm = dummyLabel.getFontMetrics(dummyLabel.getFont());

        Vector lines = Utility.splitTextIntoLines(rawText, 500, fm);
        String textWithLineBreaks = "";
        // FIXME: The following loop has the bug that it will wipe out all HTML formattings.
        // this is because to determine where to out the line breaks,
        // we needed to strip off the HTML tags, and once we strip them off it
        // is nontrivial to insert the <BR> in the original unstripped text.
        for (int i = 0; i < lines.size(); i++) {
            textWithLineBreaks += lines.get(i);
            if (i + 1 < lines.size())
                textWithLineBreaks += "<BR>";
        }
        return textWithLineBreaks;
    }

    /**
     * @param aSpecimen
     * @param thumbnail
     * @return
     */
    private int computePanelHeight(String text, JPanel thumbnail,
                                   int extraLinesToAdd) {
        Font labelFont;
        JLabel dummyLabel = new JLabel();
        labelFont = new Font("Serif", Font.PLAIN, 14);
        dummyLabel.setFont(labelFont);
        dummyLabel.setText(text);
        FontMetrics fm = dummyLabel.getFontMetrics(dummyLabel.getFont());
        Vector lines = Utility.splitTextIntoLines(text, 500, fm);
        int height = lines.size();
        //System.out.println(height);
        height += extraLinesToAdd;// lines added for the stat text.
        height = fm.getHeight() * height;
        if (height < thumbnail.getPreferredSize().getHeight())
            height = (int) thumbnail.getPreferredSize().getHeight();
        return height;
    }

    /**
     * Creates the header for the online/offline specimen. This is shown at the
     * top of the online/offline details panel.
     *
     * @param headerMainText
     * @param headerSecondaryText
     * @param textColor
     * @param backGround
     * @return
     */
    private JPanel createPanelHeader(String headerMainText,
                                     String headerSecondaryText, Color textColor, Color backGround) {
        String headerHTMLText = "<html><body bgcolor='rgb("
                + backGround.getRed()
                + ","
                + backGround.getGreen()
                + ","
                + backGround.getBlue()
                + ")'>"
                + "<table><tr width=680><td width=530 style=\"color:rgb("
                + textColor.getRed()
                + ","
                + textColor.getGreen()
                + ","
                + textColor.getBlue()
                + "); text-align:center;font-weight:bold; font-size:15pt; font-family:Tahoma,sans-serif;\">"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;"
                + headerMainText
                + "</td>"
                + "<td width=130 style=\"color:rgb("
                + textColor.getRed()
                + ","
                + textColor.getGreen()
                + ","
                + textColor.getBlue()
                + "); text-align:center; font-weight:bold; font-size:11pt; font-family:Tahoma,sans-serif;\">"
                + headerSecondaryText + "</td></tr></table>" + "</body></html>";

        JPanel textDetailsPanel = new JPanel();
        textDetailsPanel.setBackground(backGround);
        JTextPane panelHeader = new JTextPane();
        panelHeader.setBackground(backGround);
        HTMLDocument document = new HTMLDocument();
        HTMLEditorKit kit = new HTMLEditorKit();
        panelHeader.setPreferredSize(new Dimension(680, 30));
        panelHeader.setContentType("text/html");
        panelHeader.setEditable(false);
        panelHeader.setEditorKit(kit);
        panelHeader.setDocument(document);
        panelHeader.setText(headerHTMLText);
        textDetailsPanel.add(panelHeader);
        return textDetailsPanel;
    }

    public class NoWrapTextPane extends JTextPane {
        public boolean getScrollableTracksViewportWidth() {
            // should not allow text to be wrapped
            return false;
        }
    }

}
/**
 * This class moves the vertical scroll bar to the top. It only does it once and
 * then unregisters itself as the listener . This is needed because the default
 * JScrollPane behavior is to set the vertical scrollpane in the center or
 * bottom depending on how the components get added to it. When we show the
 * detail view first this behavior puts the vertical scroll bar at the bottom,
 * we would want the scroll bar to be moved to the top. Setting the scroll bar
 * position to the top at the time of creation does not seem to work, so we must
 * set the scroll bar position to the top in this event handler class. Once that
 * is done we have no need for this behavior anymore and hence this class
 * unregisters itself after moving the vertical scroll bar once.
 *
 */
class ResizeListener extends ComponentAdapter {
    public void componentResized(ComponentEvent ev) {
        if (ev.getSource() instanceof JScrollPane) {
            ((JScrollPane) ev.getSource()).getVerticalScrollBar().setValue(0);
            ((JScrollPane) ev.getSource()).removeComponentListener(this);
        }
    }
}