/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.print.Printable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import virtuallab.MicroscopeView;
import virtuallab.Specimen;
import virtuallab.Configuration;
import virtuallab.VirtualMicroscope;
import virtuallab.Specimen.ControlDescriptor;
import virtuallab.Specimen.ControlStates;
import virtuallab.gui.controls.annotation.AnnotationControl;
import virtuallab.gui.controls.measure.DistanceMeasure;
import virtuallab.gui.controls.measure.HeightMeasureFactory;
import virtuallab.util.Constants;

import virtuallab.Log;

//$Id: Microscope.java,v 1.22 2006/09/06 21:08:13 manzoor2 Exp $

/**
 * This class encompasses the MicroscopeView and the SidePanel. This class is the
 * component that gets added into the application's parent JFrame (VirtualMicroscope).
 * It contains the MicroscopeView (which deals with displaying the Specimen) and the
 * SidePanel(which contains all the controls and deals with the control specific logic).
 * Thus this class is delegated with instantiating all the controls and the specimen
 * viewer.
 */
public class Microscope extends JComponent {

    private static final Log log = new Log(Microscope.class.getName());

    public static final int SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS = 175;
    private static final long serialVersionUID = -7179405416568199888L;
    /**
     * Microscope view.
     */
    public VirtualMicroscope m_coreApplication;
    //	public static final int SIDE_PANEL_MINIMUM_WIDTH_WITH_VBARS = 190;
    private JSplitPane m_splitPane;
    /**
     * Microscope view.
     */
    private MicroscopeView m_microscopeView;
    /**
     * Side panel containing controls, etc.
     */
    private SidePanel m_sidePanel;
    /**
     * Stateful controls.
     */
    private LinkedHashSet m_statefulControls;
    /**
     * Configuration.
     */
    private Configuration m_config;

    /**
     * Default constructor.
     */
    public Microscope(Configuration config, VirtualMicroscope coreApplication) {
        super();
        m_coreApplication = coreApplication;
        m_microscopeView = new MicroscopeView(this);
        m_sidePanel = new SidePanel();
        m_sidePanel.setMinimumSize(new Dimension(SIDE_PANEL_MINIMUM_WIDTH_NO_VBARS, 100));
        m_statefulControls = new LinkedHashSet();
        m_config = config;

        setOpaque(true);
        setLayout(new BorderLayout());

        m_splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, m_microscopeView, m_sidePanel);
        m_splitPane.setResizeWeight(1);

        add(m_splitPane, BorderLayout.CENTER);
    }

    public JSplitPane getSplitPane() {
        return m_splitPane;
    }

    /**
     * This method returns the Microscope View
     *
     * @return MicroscopeView
     */
    public MicroscopeView getMicroscopeView() {
        return m_microscopeView;
    }

    /**
     * Sets current specimen in the microscope view.
     *
     * @param newSpecimen chosen specimen.
     */
    public Specimen getCurrentSpecimen() {
        // Pass the new specimen off to MicroscopeView
        return m_microscopeView.getCurrentSpecimen();
    }

    /**
     * Sets current specimen in the microscope view.
     *
     * @param newSpecimen chosen specimen.
     */
    public void setCurrentSpecimen(Specimen newSpecimen) {
        // Pass the new specimen off to MicroscopeView
        m_microscopeView.setCurrentSpecimen(newSpecimen);


        // Create appropriate set of controls in the side panel
        createSpecimenControls();
        updateSpecimenControlStates();
        //We will set the foucs to the loaded specimen so that the
        // mouse wheel can work without having the need to get the user
        // to click the specimen first.
        m_microscopeView.grabFocus();
        // When we started loading the specimen we had switched to hour glass cursor
        // we need to set it back to default now that the specimen is fully loaded.
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        m_coreApplication.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
//	m_sidePanel.adjustMinimumSize();


    }

    private void createSpecimenControls() {
        LinkedHashSet sideControls = new LinkedHashSet();
        LinkedHashSet viewControls = new LinkedHashSet();
        boolean edsPresent = false;
        m_statefulControls.clear();

        // Release the factory DistanceMeasure control, so new one can be created
        DistanceMeasure.releaseFactory();

        // Get specimen from MicroscopeView for which to create controls
        Specimen curSpecimen = m_microscopeView.getCurrentSpecimen();

        // Get specimen's control descriptors, and default and valid states for those controls
        double version = curSpecimen.getXMLVersion();
        Iterator controlDescriptors = curSpecimen.getControlDescriptors();
        //TOOD 76 by here they are translated
        Collection defaultControlStates = curSpecimen.getDefaultControlStates(version);

        // Given the list of all the GUI controls and their default values, get the list of all
        // the possible values that each of these controls can take. Each possbile value is
        // stored as a separate ControlStates Object inside the validControlStates HashMap.
        HashMap validControlStates = curSpecimen.getValidControlStates(defaultControlStates, version);

        if (controlDescriptors == null || defaultControlStates == null) {
            return; // TODO: Analyze the appropriateness of throwing an exception instead.
        }

        // TODO: Should probably be in specimen XML files (for generality)
        viewControls.add(createControl(Constants.Controls.DISTANCE, null));

        /** Adding Height Control explicitly. As Height control has been implemented as a
         * Singleton Factory, which is a more suitable pattern for this type of control.
         * Perhaps Distance Control should be implemented similarly.
         */
        HeightMeasureFactory heightFactory = HeightMeasureFactory.getInstance();
        heightFactory.initViewControl(Constants.Controls.HEIGHT, m_microscopeView);
        viewControls.add(heightFactory);

        Control curControl = null;
        while (controlDescriptors.hasNext()) {  // Create controls and stash them into appropriate lists
            try {
                ControlDescriptor cntrl = (Specimen.ControlDescriptor) controlDescriptors.next();
                curControl = createControl(null, cntrl);

                if (curControl == null)// No control needed or could be created
                    continue;

                if (curControl instanceof SideControl) {
                    sideControls.add(curControl);
                }

                if (curControl instanceof ViewControl) {
                    viewControls.add(curControl);
                }

                if (curControl instanceof Stateful) {
                    if (curControl.getControlName().equals(Constants.Controls.EDS)) {
                        edsPresent = true;
                    }
                    if (curControl.getControlName().equals(Constants.Controls.DETECTOR) && edsPresent) {
                        /**
                         * The DETECTOR control will be created inside the EDSMenu itself, so we need
                         * not create the DetectorMenu at all. Hence we delete the explicitly
                         * created detector control
                         */
                        sideControls.remove(curControl);
                    } else {
                        // However Detector can occur without
                        // EDS in which case Detector will be created as anyother control.
                        m_statefulControls.add(curControl);
                        ((Stateful) curControl).setValidControlStates(validControlStates, version);
                        ((Stateful) curControl).setCurrentControlState(defaultControlStates, version);

                    }
                }
            } catch (Exception e) {
                log.error("Error creating specimen control!");
                log.error(e.getMessage());
                continue;
            }
        }
        m_microscopeView.setImageControls(sideControls);
        m_microscopeView.setViewControls(viewControls);
        m_sidePanel.setSpecimenControls(sideControls);
    }

    /**
     * This method looks at the currently selected values of all the controls
     * and decides on one or more of the following:
     * 1. Does a new control need to be added ? e.g. user selects a detector which has timing control whereas
     * the previously selected detector did not have any timing control.
     * 2. Does existing controls need to be deleted ? e.g. user chooses a
     * Detector that has no focus level whereas the previously selected detector had three focus levels.
     * 3. Does existing controls needs to be updated ? e.g. the previously selected DETECTOR had three focus levels
     * and the newly selected one has two focus levels.
     *
     * @param Control changedControl The control whose value has been changed.
     */
    public void reCreateControls(MenuControlBase targetControl, double xmlVersion) {
        if (xmlVersion <= 1.0)
            // the following logic only applies to new XML Format
            return;
        // We shall see which node in the specimen data tree does this control correspond to.
        // Any thing above that node need not be revisited, only nodes below this node will
        // have to be revisited.
        if (targetControl.getCurrentControlState(xmlVersion).getStates().size() == 0) {
            // the leaf node GUI control was changed. No need to do the rest of the processing
            return;
        }

        // The children of the node should be visited and we need to decide
        // if any GUI control needs to be edited/hidden/added based on the
        // descendants of this node.
        JPanel sidePanel = m_sidePanel.getSidePanel();


        // Get all the side controls in a hashmap along with their selected
        // values comparison during the specimen data tree traversal.
        LinkedHashMap guiControls = new LinkedHashMap();
        for (int i = 0; i < sidePanel.getComponentCount(); i++) {
            Component cmpt = sidePanel.getComponent(i);
            if (cmpt instanceof MenuControlBase) {
                MenuControlBase mcntrl = (MenuControlBase) cmpt;
                guiControls.put(mcntrl.getControlName(), mcntrl);
            }
        }
        ControlStates changedState = targetControl.getCurrentControlState(xmlVersion);
        // We shal traverse the subtree rooted at the changedState in an attempt to find
        // all its children. We only process children because only the child nodes of this
        // subtree need to be considered. Since a state change in a child can not
        // affect the ancestor node we simply ignore the ancestors of changedState.
        LinkedHashMap newControls = new LinkedHashMap();
        LinkedHashMap states = changedState.getStates();
        while (states.size() > 0) {
            String nodeName = ((ControlStates) states.get("0")).getId();
            newControls.put(nodeName, states);
            if (guiControls.get(nodeName) == null) {
                // This is a genuine new control, which was not shown on the
                // specimen side bar before. So we shall set this controls value to
                // zero and use this to go down the tree.
                states = ((ControlStates) states.get("0")).getStates();
            } else {
                // an existing GUI control found against this control. we shall
                // use the selected value of the existing control as default and will use
                // that value to go down the tree.
                MenuControlBase oldControl = (MenuControlBase) guiControls.get(nodeName);
                String traversalNode = oldControl.m_currentValue;
                int defaultNode = 0;
                // Find out if any of the peers has the same "Display String" value as the current value.
                for (; defaultNode < states.size(); defaultNode++) {
                    if (traversalNode.equals(((ControlStates) states.get(Integer.toString(defaultNode))).getDisplayString()))
                        break;
                }
                if (defaultNode == states.size()) {
                    // No match found, set the default to the first value,and traverse down the
                    // tree.
                    defaultNode = 0;
                }
                states = ((ControlStates) states.get(Integer.toString(defaultNode))).getStates();
            }
        }
        // Now we have all the required GUI controls that need to be shown in the
        // side panel for this changedState. We shall compare this newly gathered
        // control list with the one that is currently being shown to the user. This
        // comparison will tell us which controls need to be edited/deleted/added.
        Iterator newIt = newControls.keySet().iterator();
        while (newIt.hasNext()) {
            String newControlName = (String) newIt.next();
            ControlStates cs = (ControlStates) ((HashMap) newControls.get(newControlName)).get("0");
            MenuControlBase cntrl = (MenuControlBase) guiControls.get(newControlName);
            if (cntrl == null) {
                // This control was not in the previous controls so
                // we shall create a brand new GUI Control for this.
                HashMap newValidValues = new HashMap();
                cntrl = (MenuControlBase) createControl(newControlName, cs.getControlDescriptor(xmlVersion));
                cntrl.m_controlDescriptor = m_microscopeView.getCurrentSpecimen().getControlDescriptor(newControlName);
                newValidValues.put(cntrl.m_controlDescriptor, ((LinkedHashMap) newControls.get(newControlName)).values());
                cntrl.setValidControlStates(newValidValues, xmlVersion);
                // Set the first child (at index "0") to be the default value.
                cntrl.m_currentValue = cs.getDisplayString();
                sidePanel.add(cntrl);
            } else {
                // The control already exists. We shall reuse this control and will simply
                // update its existing values.
                cntrl.m_controlDescriptor = m_microscopeView.getCurrentSpecimen().getControlDescriptor(newControlName);
                HashMap newValidValues = new HashMap();
                newValidValues.put(cntrl.m_controlDescriptor, ((LinkedHashMap) newControls.get(newControlName)).values());
                // See if the selected value can be reused. There may be the case that the
                // previous selected value may not be a valid value in the new set of
                // values - in such a case we will reset the selected value to the first
                // value.
                LinkedHashMap possibleValuesMap = (LinkedHashMap) newControls.get(newControlName);
                Iterator it = possibleValuesMap.keySet().iterator();
                String selectedValue = "";
                String firstValue = "";
                short count = 0;
                while (it.hasNext()) {
                    String key = (String) it.next();
                    ControlStates tempCntrl = (ControlStates) possibleValuesMap.get(key);
                    selectedValue = tempCntrl.getDisplayString();
                    if (count == 0)
                        firstValue = selectedValue;
                    if (tempCntrl.getDisplayString().equals(cntrl.m_currentValue)) {
                        // the selected value also exists in the new set of values, we shall
                        // use this as default.
                        selectedValue = cntrl.m_currentValue;
                        break;
                    }
                    count++;
                }
                if (count == possibleValuesMap.size()) {
                    // No match found i.e. the previous selected value for the
                    // control does not exist in the new values. We shall use the first
                    // value as the selected value.
                    selectedValue = firstValue;
                }
                cntrl.m_currentValue = selectedValue;
                cntrl.setValidControlStates(newValidValues, xmlVersion);
                cntrl.setSelectedValue(selectedValue);
                if (!cntrl.isVisible())
                    cntrl.setVisible(true);
                if (possibleValuesMap.size() == 1) {
                    //TODO: See if this is ever satisfied.I think this is redundant and needs to be taken out.
                    // we are performing this logic in the SidePanel.java, so this logic here never gets
                    // executed and should be taken out.
                    if (!cntrl.m_controlName.equals(Constants.Controls.EDS)) {
                        // This new control has only one possible value to select from. As per
                        // our convention such controls need not be shown to the user since
                        // there is nothing to choose from. However if the control is "EDS" then
                        // we make an exception since EDS control is only an indication for invoking
                        // the Overlay functionality.
                        cntrl.setVisible(false);
                    }
                }
            }
        }
        // Now we see which controls need to be deleted.
        Iterator oldIt = guiControls.keySet().iterator();
        while (oldIt.hasNext()) {
            // We shall only hide the control. Removing it from the side panel
            // is risky, since if this control is needed again it will get redrawn
            // at the bottom of the panel under Annotations.
            String key = (String) oldIt.next();
            if (newControls.get(key) == null && !isAncestor(key, changedState)) {
                // The control should be deleted.
                MenuControlBase cntrlDel = (MenuControlBase) guiControls.get(key);
                cntrlDel.cleanUp(xmlVersion);
            }
        }
        ArrayList sortedList = new ArrayList();
        // The Navigation, Brightness and Contrast controls always go at the very top, with the
        // Navigation Control always at the top and the Brightness and Contrast follow order depending
        // on whether it is a new specimen or an old (in old the order of brightness and contrast are
        // specified in specimen.xml whereas in new specimen format the order is Brightness first then
        // Contrast).
        if (sidePanel.getComponent(0) instanceof NavigationThumbnail) {
            // NavigationThumbnail goes at the very top.
            sortedList.add(sidePanel.getComponent(0));
            if (sidePanel.getComponent(1) instanceof AdjustmentControlBase) {
                sortedList.add(sidePanel.getComponent(1));
            }
            if (sidePanel.getComponent(2) instanceof AdjustmentControlBase) {
                sortedList.add(sidePanel.getComponent(2));
            }
        } else {
            // No NavigationThumbnail the competition for the top spot is between
            // Brightness and Contrast.
            if (sidePanel.getComponent(0) instanceof AdjustmentControlBase) {
                sortedList.add(sidePanel.getComponent(0));
            }
            if (sidePanel.getComponent(1) instanceof AdjustmentControlBase) {
                sortedList.add(sidePanel.getComponent(1));
            }
        }
        // The rest follow the order of the tree.
        Specimen spec = m_microscopeView.getCurrentSpecimen();
        ArrayList treeOrder = spec.getTreeOrder(changedState);
        Iterator itNew = newControls.keySet().iterator();
        while (itNew.hasNext())
            treeOrder.add(itNew.next());

        for (int k = 0; k < treeOrder.size(); k++) {
            for (int j = 0; j < sidePanel.getComponentCount(); j++) {
                if (sidePanel.getComponent(j) instanceof MenuControlBase) {
                    // we have found a contender. This must match the outer loop
                    // control for it to be in the right order. If it does not
                    // then we need to reorder.
                    MenuControlBase tempControl = (MenuControlBase) sidePanel.getComponent(j);
                    if (tempControl.m_controlName.equals(treeOrder.get(k))) {
                        // Corresponding Control found !, put it in the sorted listand move on.
                        sortedList.add(sidePanel.getComponent(j));
                        break;
                    }
                }
            }
        }
        // Adding the annotation and the filler as the last components.
        if (sidePanel.getComponent(sidePanel.getComponentCount() - 2) instanceof AnnotationControl)
            sortedList.add(sidePanel.getComponent(sidePanel.getComponentCount() - 2));
        sortedList.add(sidePanel.getComponent(sidePanel.getComponentCount() - 1));
        for (int i = 0; i < sortedList.size(); i++) {
            sidePanel.add((JComponent) sortedList.get(i));
        }
        sidePanel.revalidate();
    }

    /**
     * This method takes in the name of a control and a sub-tree and checks whether the
     * control corresponds to any of the ancestors of the sub-tree. This information is used to
     * decide which controls should not to be deleted during recreate().
     *
     * @param controlName
     * @param subTreeRoot
     * @return
     */
    private boolean isAncestor(String controlName, ControlStates subTreeRoot) {
        boolean ancestor = false;


        ArrayList ancestorlist = m_microscopeView.getCurrentSpecimen().getTreeOrder(subTreeRoot);
        for (int i = 0; i < ancestorlist.size(); i++) {
            if (ancestorlist.get(i).equals(controlName)) {
                ancestor = true;
                break;
            }
        }
        return ancestor;
    }

    /**
     * Given a name and ControlDescriptor, looks up the appropriate control class
     * name in config, instantiates and initializes the control. If name is null,
     * gets one from controlDesc.
     *
     * @param ctrlName    name of the control (can be null)
     * @param controlDesc control descriptor (can't be null if ctrName is null)
     * @return Instantiated and initialized control.
     */
    private Control createControl(String ctrlName, Specimen.ControlDescriptor controlDesc) {
        Control control = null;

        try {
            String controlName = (ctrlName == null) ? controlDesc.getId() : ctrlName;
            String controlClassName = m_config.getControlClassName(controlName);
            if (controlClassName == null) {
                // The requested control does not have an entry in the VirtualMicroscope.xml
                // configuration file. We shall associate a defaut control to it.
                controlClassName = Constants.Controls.DEFAULT_CONTROL;
            }

            // Instantiate new control object
            control = (Control) Class.forName(controlClassName).newInstance();


            // Now, initialize it appropriately
            if (control instanceof SideControl) {
                ((SideControl) control).initSideControl(controlName, controlDesc, this);
            }

            if (control instanceof ViewControl) {
                ((ViewControl) control).initViewControl(controlName, m_microscopeView);
            }
        } catch (Exception e) {
            log.error("Error creating the Control Descriptor!");
            log.error(e.getMessage());
        }

        return control;
    }

    public void updateSpecimenControlStates() {
        m_microscopeView.setSpecimenControlStates(getSpecimenCurrentControlStates());
        m_microscopeView.updateImageControls();
    }

    // TODO: update m_microscopeView's "specimenControlStates" as necessary
    private Collection getSpecimenCurrentControlStates() {
        // TODO: Cache the control-state list and update it only when necessary.
        ArrayList states = new ArrayList(m_statefulControls.size());
        Iterator iter = m_statefulControls.iterator();
        Stateful curControl;
        double version = m_microscopeView.getCurrentSpecimen().getXMLVersion();
        while (iter.hasNext()) {
            curControl = (Stateful) iter.next();
            if (version <= 1.0)
                states.add(curControl.getCurrentControlState());
            else {
                // Call the method specific to the new XML Format.
                states.add(curControl.getCurrentControlState(version));
            }
        }

        return states;
    }

    public Printable getPrintableView() {
        return m_microscopeView;
    }

    /**
     * @return Unload button from the SidePanel.
     */
    public JButton getUnloadButton() {
        return m_sidePanel.getUnloadButton();
    }

    /**
     * @return configuration.
     */
    public Configuration getConfig() {
        return m_config;
    }

    public SidePanel getSidePanel() {
        return m_sidePanel;
    }
}
