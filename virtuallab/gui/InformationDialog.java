/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab.gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.CropImageFilter;
import java.awt.image.FilteredImageSource;
import java.awt.Font;

import java.util.Iterator;
import java.util.LinkedHashMap;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import java.io.InputStream;
import java.net.URL;

import virtuallab.Configuration;
import virtuallab.Specimen;
import virtuallab.WebLauncher;
import virtuallab.util.Constants;

import virtuallab.Log;
import virtuallab.util.Locale;

/**
 * This dialog box is used to show the Specimen and Instrument Information
 * to the user. It gets invokved when the user selects the "Information->Specimen Information"
 * option from the menu. It applies only to the new XML Format, for old XML formats this
 * menu option does not show up.
 *
 */
public class InformationDialog extends JDialog {

    private static final Log log = new Log(InformationDialog.class.getName());

    /**
     * The padding for the text that appears in the text pane.
     */
    private final static String TEXT_PAD = "";
    /**
     * The width of the thumbnail
     */
    private final static short IMAGE_SIZE_WIDTH = 100;
    /**
     * The height of the thumbnail
     */
    private final static short IMAGE_SIZE_HEIGHT = 100;
    /**
     * The space in pixels that will be used as padding around
     * the image. This will give us the affect of a border around
     * the thumbnail on the button. Set it to zero if no border
     * affect is desired
     */
    private final static short IMAGE_INSET_HEIGHT = 10;
    /**
     * The space in pixels that will be used as padding around
     * the image. This will give us the affect of a border around
     * the thumbnail on the button. Set it to zero if no border
     * affect is desired
     */
    private final static short IMAGE_INSET_WIDTH = 10;
    /**
     * The width of the dialog.
     */
    private final static short FRAME_SIZE_WIDTH = 500;
    /**
     * The layout manager for the dialog
     */
    GridBagLayout m_dialogLayout = null;
    /**
     * The configuration read from the VirtualLabConfig.xml file. This
     * is used to open the browser when the user presses the inforef hyperlink.
     */
    private Configuration m_config;
    /**
     * The variable holds the Specimen Text Pane. This is used in the
     * mouse listener while changing the icon when the user hovers over
     * a hyperlink.
     */
    private JTextPane m_specimenInfoTextPane = null;
    /**
     * The variable holds the Instrument Text Pane. This is used in the
     * mouse listener while changing the icon when the user hovers over
     * a hyperlink.
     */
    private JTextPane m_instrumentInfoTextPane = null;

    /**
     * The constructor of the class that intializes the contents of the information dialog box.
     *
     * @param owner
     * @param specimenMap
     * @param instrumentMap
     * @param currentSpecimen
     * @param config
     */
    public InformationDialog(JFrame owner, LinkedHashMap specimenMap, LinkedHashMap instrumentMap, Specimen currentSpecimen, Configuration config) {
        super(owner, Locale.Information(Configuration.getApplicationLanguage())+Constants.SPACE+Locale.About(Configuration.getApplicationLanguage()).toLowerCase()+Constants.SPACE + currentSpecimen.getDisplayString(), true);
        m_config = config;
        setTitle(Locale.Information(Configuration.getApplicationLanguage())+Constants.SPACE+Locale.About(Configuration.getApplicationLanguage()).toLowerCase()+Constants.SPACE + currentSpecimen.getDisplayString());
        m_dialogLayout = new GridBagLayout();
        getContentPane().setLayout(m_dialogLayout);
        showSpecimenInfo(specimenMap, currentSpecimen);
        showInstrumentInfo(instrumentMap, currentSpecimen);
        // Drawing the close button at the bottom.
        JPanel bttnPanel = new JPanel();
        bttnPanel.setBorder(BorderFactory.createEmptyBorder());
        JButton bttnClose = new JButton(Locale.Close(Configuration.getApplicationLanguage()));
        bttnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        bttnPanel.add(bttnClose);

        GridBagConstraints cd = new GridBagConstraints();
        cd.gridx = 0;
        cd.gridy = 2;
        int top = 2;
        int left = 0;
        int bottom = 2;
        int right = 0;
        cd.insets = new Insets(top, left, bottom, right);
        cd.ipadx = 0;
        cd.ipady = 0;
        cd.weightx = 1;
        cd.weighty = 1;
        cd.anchor = GridBagConstraints.CENTER;
        getContentPane().add(bttnPanel, cd);
        pack();
        setResizable(false);
        setLocationRelativeTo(owner);
        bttnClose.grabFocus();
    }

    /**
     * This method shows the specimen information.
     *
     * @param specimenMap  the hashmap containing the label,value pairs of the information to be shown.
     * @param currSpecimen the loaded specimen whose information is to be shown.
     */
    private void showSpecimenInfo(LinkedHashMap specimenMap, Specimen currSpecimen) {
        try {
            // This panel will hold both the left thumbnail and the right info text.
            GridBagLayout parentLayout = new GridBagLayout();
            JPanel specimenPanel = new JPanel(parentLayout);

            //Create the titled border.
            Border specimenBorder = BorderFactory.createTitledBorder(Locale.Specimen(Configuration.getApplicationLanguage()));
            specimenPanel.setBorder(specimenBorder);

            //Create the panel that will hold the information text, along with the
            // thumbnail, along with the necessary layout manager and the constraints
            //for proper placement.
            GridBagLayout outerLayout = new GridBagLayout();
            JPanel infoOuterTextPanel = new JPanel(outerLayout);
            //Create the thumbnail button.
            JComponent button = createThumbnailButton(currSpecimen, true);
            //Now on to creating the Specimen information.
            infoOuterTextPanel.setPreferredSize(new Dimension(FRAME_SIZE_WIDTH - 35, IMAGE_SIZE_HEIGHT + 23));

            m_specimenInfoTextPane = new JTextPane();
            m_specimenInfoTextPane.setEditable(false);
            SimpleAttributeSet plainText = new SimpleAttributeSet();
            StyleConstants.setFontSize(plainText, 11);
            StyleConstants.setForeground(plainText, Color.BLACK);

            SimpleAttributeSet boldText = new SimpleAttributeSet();
            StyleConstants.setBold(boldText, true);
            StyleConstants.setFontSize(boldText, 11);
            // TODO: Fix the defect: the textpane distorts when the vertical/horizontal scrollbars appear on Mac.

            JScrollPane specimenScrollPane = new JScrollPane(m_specimenInfoTextPane, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            int width = FRAME_SIZE_WIDTH - IMAGE_SIZE_WIDTH - 70;
            int height = IMAGE_SIZE_HEIGHT;
            specimenScrollPane.getViewport().setMinimumSize(new Dimension(width, height));
            specimenScrollPane.getViewport().setPreferredSize(new Dimension(width, height));

            SimpleAttributeSet labelAttributes = null;
            SimpleAttributeSet valueAttributes = null;

            //create all the labels and the values
            Iterator it = specimenMap.keySet().iterator();
            while (it.hasNext()) {
                String labelText = (String) it.next();
                String valueText = (String) specimenMap.get(labelText);
                if (valueText.indexOf("http://") == 0) {
                    // a URL found. We shall create a label for it and make it
                    // clickable so that the user can navigate to the site pointed to
                    // by the URL.
                    m_specimenInfoTextPane.getDocument().insertString(m_specimenInfoTextPane.getDocument().getLength(), TEXT_PAD, plainText);
                    JLabel valueField = new JLabel(labelText, JLabel.LEFT);
                    valueField.setVerticalAlignment(JLabel.TOP);
                    Font f1 = new Font("Serif", Font.PLAIN, 11);
                    valueField.setFont(f1);
                    valueField.setForeground(Color.BLUE);
                    valueField.setToolTipText(valueText);
                    valueField.setName(valueText);

                    valueField.addMouseListener(new LabelFieldListener(m_specimenInfoTextPane));
                    m_specimenInfoTextPane.insertComponent(valueField);

                    m_specimenInfoTextPane.getDocument().insertString(m_specimenInfoTextPane.getDocument().getLength(), Constants.NEW_LINE, plainText);
                } else {
                    if (valueText != null && valueText.length() > 0) {
                        // Not an InfoRef. InfoRefs only have one value they do not have
                        // the (key,value) pair like other deterministic tags.
                        labelText = TEXT_PAD + labelText + ": ";
                        labelAttributes = boldText;
                        valueAttributes = plainText;
                    } else {
                        // an info ref.
                        labelText = TEXT_PAD + labelText;
                        if (labelText.indexOf(":") > -1) {
                            valueText = labelText.substring(labelText.indexOf(":"));
                            labelText = labelText.substring(0, labelText.indexOf(":"));
                            labelAttributes = boldText;
                            valueAttributes = plainText;
                        } else {
                            labelAttributes = plainText;
                            valueAttributes = plainText;
                        }
                    }
                    valueText += Constants.NEW_LINE;
                    m_specimenInfoTextPane.getDocument().insertString(m_specimenInfoTextPane.getDocument().getLength(), labelText, labelAttributes);
                    m_specimenInfoTextPane.getDocument().insertString(m_specimenInfoTextPane.getDocument().getLength(), valueText, valueAttributes);

                }
            }
            // scroll to the top.
            m_specimenInfoTextPane.setCaretPosition(0);

            // Aligning the thumbnail and the info such that they both
            // fill up as much space as they can while being as close to each
            // other as possible.
            GridBagConstraints c1 = new GridBagConstraints();
            c1.gridx = 0;
            c1.gridy = 0;
            int top = 0;
            int left = 10;
            int bottom = 10;
            int right = 0;
            c1.insets = new Insets(top, left, bottom, right);
            c1.ipadx = 0;
            c1.ipady = 0;
            c1.weightx = 0;
            c1.weighty = 0;
            c1.anchor = GridBagConstraints.NORTH;// Top aligned
            infoOuterTextPanel.add(button, c1);
            GridBagConstraints c2 = new GridBagConstraints();
            c2.gridx = 1;
            c2.gridy = 0;
            top = 0;
            left = 0;
            bottom = 10;
            right = 10;
            c2.insets = new Insets(top, left, bottom, right);
            c2.ipadx = 0;
            c2.ipady = 0;
            c2.weightx = 1;
            c2.weighty = 1;
            c1.fill = GridBagConstraints.BOTH;
            c2.anchor = GridBagConstraints.NORTHWEST;// Top-Left Align
            infoOuterTextPanel.add(specimenScrollPane, c2);
            // Now aligning the inner panel that contains the thumbnail and the
            // text pane into the parent specimenPanel.
            GridBagConstraints c3 = new GridBagConstraints();
            c3.gridx = 0;
            c3.gridy = 0;
            top = 0;
            left = 0;
            bottom = 0;
            right = 0;
            c3.insets = new Insets(top, left, bottom, right);
            c3.ipadx = 0;
            c3.ipady = 0;
            c3.weightx = 1;
            c3.weighty = 1;
            c3.anchor = GridBagConstraints.CENTER;
            specimenPanel.add(infoOuterTextPanel, c3);

            // Finally putting the specimen panel into the dialog box.
            GridBagConstraints cd = new GridBagConstraints();
            cd.gridx = 0;
            cd.gridy = 0;
            top = 5;
            left = 10;
            bottom = 2;
            right = 10;
            cd.insets = new Insets(top, left, bottom, right);
            cd.ipadx = 0;
            cd.ipady = 0;
            cd.weightx = 0;
            cd.weighty = 0;
            cd.anchor = GridBagConstraints.CENTER;
            getContentPane().add(specimenPanel, cd);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    /**
     * This method shows the instrument information.
     *
     * @param instrumentMap the hashmap containing the label,value pairs of the information to be shown.
     * @param currSpecimen  the loaded specimen whose information is to be shown.
     */
    private void showInstrumentInfo(LinkedHashMap instrumentMap, Specimen currSpecimen) {
        try {
            // This panel willhold both the left thumbnail and the right info text.
            GridBagLayout parentLayout = new GridBagLayout();
            JPanel instrumentPanel = new JPanel(parentLayout);

            //Create the titled border.
            Border specimenBorder = BorderFactory.createTitledBorder(Locale.Instrument(Configuration.getApplicationLanguage()));
            instrumentPanel.setBorder(specimenBorder);

            //Create the panel that will hold the information text, along with the
            // thumbnail, along with the necessary layout manager and the constraints
            //for proper placement.
            GridBagLayout outerLayout = new GridBagLayout();
            JPanel infoOuterTextPanel = new JPanel(outerLayout);
            //Create the thumbnail button.
            JComponent button = createThumbnailButton(currSpecimen, false);
            //Now on to creating the Specimen information.
            //infoOuterTextPanel.setPreferredSize(new Dimension(FRAME_SIZE_WIDTH-45,IMAGE_SIZE_HEIGHT+20));
            m_instrumentInfoTextPane = new JTextPane();
            m_instrumentInfoTextPane.setEditable(false);

            SimpleAttributeSet plainText = new SimpleAttributeSet();
            StyleConstants.setFontSize(plainText, 11);
            StyleConstants.setForeground(plainText, Color.BLACK);

            SimpleAttributeSet boldText = new SimpleAttributeSet();
            StyleConstants.setBold(boldText, true);
            StyleConstants.setFontSize(boldText, 11);

            // TODO: Fix the defect: the textpane distorts when the vertical/horizontal scrollbars appear on Mac.
            m_instrumentInfoTextPane.setPreferredSize(new Dimension(FRAME_SIZE_WIDTH - IMAGE_SIZE_WIDTH - 70, IMAGE_SIZE_HEIGHT + 8));
            JScrollPane scrollPane = new JScrollPane(m_instrumentInfoTextPane, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            SimpleAttributeSet labelAttributes = null;
            SimpleAttributeSet valueAttributes = null;

            Iterator it = instrumentMap.keySet().iterator();
            while (it.hasNext()) {
                String labelText = (String) it.next();
                String valueText = (String) instrumentMap.get(labelText);
                if (valueText.indexOf("http://") == 0) {
                    // a URL found. We shall create a label for it and make it
                    // clickable so that the user can navigate to the site pointed to
                    // by the URL.
                    m_instrumentInfoTextPane.getDocument().insertString(m_instrumentInfoTextPane.getDocument().getLength(), TEXT_PAD, plainText);
                    JLabel valueField = new JLabel(labelText, JLabel.LEFT);
                    valueField.setVerticalAlignment(JLabel.TOP);
                    Font f1 = new Font("Serif", Font.PLAIN, 11);
                    valueField.setFont(f1);

                    valueField.setForeground(Color.BLUE);
                    valueField.setToolTipText(valueText);
                    valueField.setName(valueText);

                    valueField.addMouseListener(new LabelFieldListener(m_instrumentInfoTextPane));
                    m_instrumentInfoTextPane.insertComponent(valueField);
                    m_instrumentInfoTextPane.getDocument().insertString(m_instrumentInfoTextPane.getDocument().getLength(), Constants.NEW_LINE, plainText);
                } else {
                    if (valueText != null && valueText.length() > 0) {
                        // Not an InfoRef. InfoRefs only have one value they do not have
                        // the (key,value) pair like other deterministic tags.
                        labelText = TEXT_PAD + labelText + ": ";
                        labelAttributes = boldText;
                        valueAttributes = plainText;
                    } else {
                        // an info ref.
                        labelText = TEXT_PAD + labelText;
                        if (labelText.indexOf(":") > -1) {
                            valueText = labelText.substring(labelText.indexOf(":"));
                            labelText = labelText.substring(0, labelText.indexOf(":"));
                            labelAttributes = boldText;
                            valueAttributes = plainText;
                        } else {
                            labelAttributes = plainText;
                            valueAttributes = plainText;
                        }
                    }
                    valueText += Constants.NEW_LINE;
                    m_instrumentInfoTextPane.getDocument().insertString(m_instrumentInfoTextPane.getDocument().getLength(), labelText, labelAttributes);
                    m_instrumentInfoTextPane.getDocument().insertString(m_instrumentInfoTextPane.getDocument().getLength(), valueText, valueAttributes);

                }
            }
            // scroll to the top.
            m_instrumentInfoTextPane.setCaretPosition(0);

            // Aligning the thumbnail and the info such that they both
            // fill up as much space as they can while being as close to each
            // other as possible.
            GridBagConstraints c1 = new GridBagConstraints();
            c1.gridx = 0;
            c1.gridy = 0;
            int top = 0;
            int left = 10;
            int bottom = 10;
            int right = 0;
            c1.insets = new Insets(top, left, bottom, right);
            c1.ipadx = 0;
            c1.ipady = 0;
            c1.weightx = 0;
            c1.weighty = 0;
            c1.anchor = GridBagConstraints.NORTH;// Top aligned
            infoOuterTextPanel.add(button, c1);
            GridBagConstraints c2 = new GridBagConstraints();
            c2.gridx = 1;
            c2.gridy = 0;
            top = 0;
            left = 0;
            bottom = 10;
            right = 10;
            c2.insets = new Insets(top, left, bottom, right);
            c2.ipadx = 0;
            c2.ipady = 0;
            c2.weightx = 1;
            c2.weighty = 1;
            c2.anchor = GridBagConstraints.NORTHWEST;// Top-Left Align
            infoOuterTextPanel.add(scrollPane, c2);
            // Now aligning the inner panel that contains the thumbnail and the
            // text pane into the parent specimenPanel.
            GridBagConstraints c3 = new GridBagConstraints();
            c3.gridx = 0;
            c3.gridy = 0;
            top = 0;
            left = 0;
            bottom = 0;
            right = 0;
            c3.insets = new Insets(top, left, bottom, right);
            c3.ipadx = 0;
            c3.ipady = 0;
            c3.weightx = 1;
            c3.weighty = 1;
            c3.anchor = GridBagConstraints.CENTER;
            instrumentPanel.add(infoOuterTextPanel, c3);

            // Finally putting the specimen panel into the dialog box.
            GridBagConstraints cd = new GridBagConstraints();
            cd.gridx = 0;
            cd.gridy = 1;
            top = 0;
            left = 0;
            bottom = 0;
            right = 0;
            cd.insets = new Insets(top, left, bottom, right);
            cd.ipadx = 0;
            cd.ipady = 0;
            cd.weightx = 1;
            cd.weighty = 1;
            cd.anchor = GridBagConstraints.CENTER;// Top-Left align
            getContentPane().add(instrumentPanel, cd);
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
    }

    /**
     * This method creates an icon out of the specimen thumbnail.
     *
     * @param currentSpecimen currently loaded specimen.
     * @return The ImageIcon that shows the specimen thumbnail.
     */
    private ImageIcon createSpecimenIcon(Specimen currentSpecimen) {
        ImageIcon thumbIcon = null;
        try {
            Image image = null;
            try {
                InputStream is = currentSpecimen.getInputStream(currentSpecimen.getNamedParam(Constants.XMLTags.THUMBNAIL));
                image = ImageIO.read(is);
                is.close();
            } catch (Exception ex) {
                // The thumbnail specified in the specimen.xml does not exist, we shall use a default.
                InputStream is = currentSpecimen.getInputStream(currentSpecimen.getDefaultSampleThumbnail());
                image = ImageIO.read(is);
                is.close();
            }
            // Crop and scale
            int imgHeight = image.getHeight(null);
            int imgWidth = image.getWidth(null);
            int minDim = (imgHeight < imgWidth) ? imgHeight : imgWidth;
            int x = (imgWidth - minDim) / 2;
            int y = (imgHeight - minDim) / 2;

            image = createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(x, y, minDim, minDim))).getScaledInstance(IMAGE_SIZE_WIDTH, IMAGE_SIZE_HEIGHT, java.awt.image.BufferedImage.SCALE_SMOOTH);

            thumbIcon = new ImageIcon(image);
        } catch (Exception e) {
            log.error(e.getMessage());
            thumbIcon = null;
        }
        return thumbIcon;
    }

    /**
     * This method creates an icon out of the Instrument thumbnail.
     *
     * @param currentSpecimen currently loaded specimen.
     * @return The ImageIcon that shows the specimen thumbnail.
     */

    private ImageIcon createInstrumentIcon(Specimen currentSpecimen) {
        ImageIcon thumbIcon = null;
        try {
            Image image = null;
            try {
                image = ImageIO.read(currentSpecimen.getInputStream(currentSpecimen.getNamedInstrumetParam(Constants.XMLTags.THUMBNAIL)));
            } catch (Exception ex) {
                // The instrument thumbnail specified in the specimen.xml does not exist, we shall
                // use a default.
                String strPath = currentSpecimen.getDefaultInstrumentThumbnail();
                if (strPath.length() == 0)
                    // could not find a default instrument thumbnail.
                    return null;
                else {
                    URL url = getClass().getResource(strPath);
                    image = ImageIO.read(url);
                }
            }
            // Crop and scale
            int imgHeight = image.getHeight(null);
            int imgWidth = image.getWidth(null);
            int minDim = (imgHeight < imgWidth) ? imgHeight : imgWidth;
            int x = (imgWidth - minDim) / 2;
            int y = (imgHeight - minDim) / 2;
            image = createImage(new FilteredImageSource(image.getSource(), new CropImageFilter(x, y, minDim, minDim))).getScaledInstance(IMAGE_SIZE_WIDTH, IMAGE_SIZE_HEIGHT, java.awt.image.BufferedImage.SCALE_SMOOTH);
            thumbIcon = new ImageIcon(image);
        } catch (Exception e) {
            log.error(e.getMessage());
            thumbIcon = null;
        }
        return thumbIcon;
    }

    /**
     * This mmethod creates a button with the image thumbnail icon. If the thumbnail
     * could not be found it shows the Label with the text "No Thumbnail" on it.
     *
     * @param currentSpecimen     the currently loaded specimen.
     * @param isSpecimenThumbnail true if we require the specimen thumbnail false if instrument thumbnail is
     *                            required
     * @return The JButton with the specimen thumbnail on it or JLabel if no thumbnail was found.
     */
    private JComponent createThumbnailButton(Specimen currentSpecimen, boolean isSpecimenThumbnail) {
        JButton button = null;
        ImageIcon thumbIcon = null;
        if (isSpecimenThumbnail)
            thumbIcon = createSpecimenIcon(currentSpecimen);
        else
            thumbIcon = createInstrumentIcon(currentSpecimen);

        if (thumbIcon == null)
            return new JLabel(Locale.MissingThumbnail(Configuration.getApplicationLanguage()));
        button = new JButton();
        button.setToolTipText(Locale.SpecimenThumbnail(Configuration.getApplicationLanguage()));
        button.setIcon(thumbIcon);
        button.setPreferredSize(new Dimension(thumbIcon.getIconHeight() + IMAGE_INSET_HEIGHT, thumbIcon.getIconWidth() + IMAGE_INSET_WIDTH));
        return button;
    }


    /**
     * This class acts as the mouse listener for the TextPane that contains all the
     * information.
     *
     * @author Kashif Manzoor
     */
    class LabelFieldListener implements MouseListener {
        /**
         * This variable holds the reference to the JTextPane inwhich the
         * mouse currently is.
         */
        private JTextPane m_pane = null;

        LabelFieldListener(JTextPane pane) {
            m_pane = pane;

        }

        public void mouseClicked(MouseEvent e) {
            // Open the link in the browser.
            try {
                WebLauncher.showURL(((JLabel) e.getSource()).getName());
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(InformationDialog.this,
                        Locale.ErrorOpeningBrowser(Configuration.getApplicationLanguage())
                        +", "
                        +Locale.ConfigurationError(Configuration.getApplicationLanguage()).toLowerCase()
                        +".",
                        Locale.Error(Configuration.getApplicationLanguage()),
                        JOptionPane.ERROR_MESSAGE);
                log.error(Locale.ErrorOpeningBrowser(Locale.LANGUAGE.EN));
                log.error(ex.getMessage());
            }
        }

        /**
         * This method sets the cursor to hand shaped cursor when the user moves
         * the mouse over a hyperlink.
         */
        public void mouseEntered(MouseEvent e) {
            m_pane.setCursor(new Cursor(Cursor.HAND_CURSOR));
        }

        /**
         * This method resets the cursor to default when the user moves out of
         * the hyperlink.
         */
        public void mouseExited(MouseEvent e) {
            m_pane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }

        public void mousePressed(MouseEvent e) {
        }

        public void mouseReleased(MouseEvent e) {
        }

    }
}