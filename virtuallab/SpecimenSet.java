/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JWindow;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;

import virtuallab.update.UpdateRetriever;
import virtuallab.util.Constants;
import virtuallab.util.Locale;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.io.FileFilter;

//	$Id: SpecimenSet.java,v 1.12 2006/10/25 19:36:33 manzoor2 Exp $
/**
 * Displays the progress bar while loading the specimens.
 */
public class SpecimenSet extends JWindow implements FileFilter {

    private static final Log log = new Log(SpecimenSet.class.getName());

    private static final long serialVersionUID = -8230635648521923781L;
    public boolean m_isDone = false;
    /**
     * Holds the online Specimens (i.e. the specimens that are already loaded on user
     * computer).
     */
    private LinkedHashMap m_onlineSpecimens = new LinkedHashMap();
    /**
     * Holds the offline Specimens (i.e. the specimens that available on the website
     * but not yet uploaded to the user computer).
     */
    private LinkedHashMap m_offlineSpecimens = new LinkedHashMap();
    /**
     * Contains ALL the specimens that were found in the RSS feed. This informaiton
     * is particularly useful when we want to move a specimen from ONLINE to OFFLINE.
     * This case will occur when the user manually deletes a specimen from the
     * specimens folder  and presses "Refresh". in this case we will remove the specimen
     * from the online panel and will add it to offline panel.
     */
    private LinkedHashMap m_allSpecimens = new LinkedHashMap();
    /**
     * Default specimen in the collection.
     */
    private Specimen m_defSpecimen;

    private Configuration m_config;

    /**
     * TODO: Implement the progress bar during the "Refresh" as well.
     *
     * @param parentFrame
     * @param config
     * @param noThreads
     */
    public SpecimenSet(JFrame parentFrame, Configuration config, boolean noThreads) {
        super(parentFrame);
        m_config = config;
        String directoryName = m_config.getSpecimenDir();
        // Creating the panel that will hold the progress bar.
        JPanel panel = new JPanel();
        panel.setBorder(new BevelBorder(BevelBorder.RAISED));
        final JProgressBar progressBar = new JProgressBar();
        final JLabel fileName = new JLabel();
        JLabel applicationName = new JLabel(Version.getVersionString());
        fileName.setForeground(Color.BLUE);
        fileName.setText(Locale.LoadingSpecimenFiles(Configuration.getApplicationLanguage()));
        //System.out.println("Am i running in an event dispatch thread ? " + SwingUtilities.isEventDispatchThread());

        // Figure out the progress bar's position from its size and the size of the screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        progressBar.setPreferredSize(new Dimension(Constants.PROGRESS_BAR_WIDTH, Constants.PROGRESS_BAR_HEIGHT));
        int locX = screenSize.width / 2 - (Constants.PROGRESS_BAR_WIDTH / 2);
        int locY = screenSize.height / 2 - (Constants.PROGRESS_BAR_HEIGHT / 2);

        locX = Math.max(locX, 0);
        locY = Math.max(locY, 0);

        // Adding the progress bar and the file name string to the panel.
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.insets = new Insets(5, 10, 0, 10);
        c.gridx = 0;
        c.gridy = 0;
        c.ipadx = 5;
        c.ipady = 5;
        c.gridwidth = 2;
        panel.add(progressBar, c);
        GridBagConstraints c1 = new GridBagConstraints();
        c1.insets = new Insets(0, 10, 0, 0);
        c1.gridx = 0;
        c1.gridy = 1;
        c1.ipadx = 0;
        c1.ipady = 0;
        c1.gridwidth = 0;
        c1.anchor = GridBagConstraints.WEST;
        panel.add(fileName, c1);
        GridBagConstraints c2 = new GridBagConstraints();
        c2.insets = new Insets(0, 0, 0, 15);
        c2.gridx = 1;
        c2.gridy = 1;
        c2.ipadx = 0;
        c2.ipady = 0;
        c2.gridwidth = 0;
        c2.anchor = GridBagConstraints.EAST;
        panel.add(applicationName, c2);

        if (directoryName == null || "".equals(directoryName)) {
            throw new IllegalArgumentException("Specimen directory name is null or empty.");
        }

        File directory = new File(directoryName);

        if (!directory.exists()) {
            throw new IllegalArgumentException("The specified specimen directory does not exist.");
        }

        final File files[] = directory.listFiles(this);
        final int max = files.length;
        progressBar.setMinimum(0);
        progressBar.setMaximum(max - 1);
        progressBar.setStringPainted(true);

        getContentPane().add(panel, BorderLayout.CENTER);
        setBounds(locX, locY, 490, 50);
        setVisible(true);

        final Runnable closerRunner = new Runnable() {
            public void run() {
                setVisible(false);
                dispose();
            }
        };

        Runnable waitRunner = new Runnable() {
            public void run() {
                try {
                    for (int i = 0; i < max; i++) {
                        try {
                            final int count = i;
                            Runnable updateAComponent = new Runnable() {
                                public void run() {
                                    progressBar.setValue(count);
                                    fileName.setText(files[count].getName());
                                }
                            };
                            SwingUtilities.invokeLater(updateAComponent);
                            Specimen s = new Specimen(files[i]);
                            // First specimen is the default
                            if (m_defSpecimen == null) {
                                m_defSpecimen = s;
                            }
                            m_onlineSpecimens.put(s.getUniqueName(), s);
                        } catch (Exception e) {
                            log.info(e.getMessage());
                            continue;
                        }
                    }
                    SwingUtilities.invokeLater(closerRunner);
                    m_isDone = true;
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        };
        Thread refreshThread = new Thread(waitRunner, "RefreshThread");
        refreshThread.start();
    }

    /**
     * The constructor.
     * TODO: Implement the progress bar during the "Refresh" as well.
     * @param parentFrame parent frame
     * @param config configuration
     */
    public SpecimenSet(JFrame parentFrame, Configuration config) {
        super(parentFrame);
        m_config = config;
        String directoryName = m_config.getSpecimenDir();

        // Creating the panel that will hold the progress bar.
        JLabel imageLabel = new JLabel(new ImageIcon(getClass().getResource(Constants.APPLICATION_ICON)));
        JPanel panel = new JPanel();
        panel.setBackground(Color.WHITE);
        final JProgressBar progressBar = new JProgressBar();
        final JLabel fileName = new JLabel();
        Font labelFont = new Font("Verdana", Font.PLAIN, 12);
        final JLabel applicationVersion = new JLabel(Locale.Version(Configuration.getApplicationLanguage())+": " + Version.getVersionNumber());
        applicationVersion.setFont(labelFont);
        applicationVersion.setText("<html><b>" + Constants.APPLICATION_NAME + " </b>" + "<html><b><br>"+Locale.Version(Configuration.getApplicationLanguage())+":</b> " + Version.getVersionNumber() + ", "+
                Locale.BuildOn(Configuration.getApplicationLanguage())+" " + Version.getBuiltDate());
        fileName.setForeground(Color.BLACK);
        Font smallFont = new Font("Verdana", Font.PLAIN, 10);
        fileName.setFont(smallFont);
        fileName.setText(Locale.LoadingSpecimenFiles(Configuration.getApplicationLanguage()));

        // Figure out the progress bar's position from its size and the size of the screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        progressBar.setPreferredSize(new Dimension(Constants.PROGRESS_BAR_WIDTH, Constants.PROGRESS_BAR_HEIGHT));

        Dimension labelSize = imageLabel.getPreferredSize();
        int locX = screenSize.width / 2 - (labelSize.width / 2);
        locX = Math.max(locX, 10);
        int locY = screenSize.height / 2 - (labelSize.height / 2);
        locY = Math.max(locY, 10);

        // Adding the progress bar and the file name string to the panel.
        panel.setLayout(new GridBagLayout());
        GridBagConstraints c2 = new GridBagConstraints();
        int top = 0;
        int left = 0;
        int bottom = 5;
        int right = 0;
        c2.insets = new Insets(top, left, bottom, right);

        c2.gridx = 0;
        c2.gridy = 0;
        c2.ipadx = 0;
        c2.ipady = 0;
        c2.gridwidth = 0;
        c2.anchor = GridBagConstraints.WEST;
        panel.add(applicationVersion, c2);
        GridBagConstraints c = new GridBagConstraints();

        top = 0;
        left = 0;
        bottom = 0;
        right = 0;
        c.insets = new Insets(top, left, bottom, right);
        c.gridx = 0;
        c.gridy = 1;
        c.ipadx = 0;
        c.ipady = 0;
        c.gridwidth = 2;
        panel.add(progressBar, c);
        GridBagConstraints c1 = new GridBagConstraints();

        c1.insets = new Insets(0, 0, 0, 0);
        c1.gridx = 0;
        c1.gridy = 2;
        c1.ipadx = 0;
        c1.ipady = 0;
        c1.gridwidth = 0;
        c1.anchor = GridBagConstraints.WEST;
        panel.add(fileName, c1);

        if (directoryName == null || "".equals(directoryName)) {
            throw new IllegalArgumentException("Specimen directory name is null or empty.");
        }

        File directory = new File(directoryName);

        if (!directory.exists()) {
            throw new IllegalArgumentException("The specified specimen directory does not exist.");
        }

        final File files[] = directory.listFiles(this);
        final int max = files.length;

        progressBar.setMinimum(0);
        progressBar.setMaximum(max - 1);
        progressBar.setStringPainted(true);
        getContentPane().add(imageLabel, BorderLayout.NORTH);
        getContentPane().add(panel, BorderLayout.CENTER);

        setBounds(locX, locY, Constants.LOADING_WINDOW_WIDTH, Constants.LOADING_WINDOW_HEIGHT);

        setVisible(true);
        try {
            for (int i = 0; i < max; i++) {
                try {
                    final int count = i;
                    Runnable updateAComponent = new Runnable() {
                        public void run() {
                            progressBar.setValue(count);
                            fileName.setText(files[count].getName());
                        }
                    };
                    SwingUtilities.invokeLater(updateAComponent);
                    Specimen s = new Specimen(files[i]);

                    // First specimen is the default
                    if (m_defSpecimen == null) {
                        m_defSpecimen = s;
                    }
                    m_onlineSpecimens.put(s.getUniqueName(), s);
                } catch (Exception e) {
                    log.info(e.getMessage());
                    continue;
                }
            }
            new Runnable() {
                public void run() {
                    fileName.setText(Locale.LoadingOfflineSpecimens(Configuration.getApplicationLanguage())+"... ");
                }
            };
            // Loading the offline specimen information.
            loadOffLineSpecimens();
            Runnable updateAComponent = new Runnable() {
                public void run() {
                    fileName.setText(Locale.LoadingThumbnails(Configuration.getApplicationLanguage())+"... ");
                    progressBar.setStringPainted(false);
                }
            };
            SwingUtilities.invokeLater(updateAComponent);
        } catch (Exception e) {
            if (isVisible())
                dispose();
        }
    }

    /**
     * The constructor that reads in all the specimen jars in the given directory.
     *
     * @param config
     * @throws SpecimenException if we were unable to read in the specimens.
     */
    public SpecimenSet(Configuration config) throws SpecimenException {
        m_config = config;
        String directoryName = m_config.getSpecimenDir();
        if (directoryName == null || "".equals(directoryName)) {
            throw new IllegalArgumentException("Specimen directory name is null or empty.");
        }

        try {
            File directory = new File(directoryName);

            if (!directory.exists()) {
                throw new IllegalArgumentException("The specified specimen directory does not exist.");
            }

            File files[] = directory.listFiles(this);

            // For each file try to unpersist specimen
            for (int i = 0; i < files.length; i++) {
                try {
                    Specimen s = new Specimen(files[i]);
                    // First specimen is the default
                    if (m_defSpecimen == null) {
                        m_defSpecimen = s;
                    }
                    m_onlineSpecimens.put(s.getUniqueName(), s);
                } catch (Exception e) {
                    log.info(e.getMessage());
                    continue;
                }
            }
        } catch (Exception e) {
            throw new SpecimenException("Unable to create specimen set.", e);
        }
        m_isDone = true;
    }

    public LinkedHashMap getOnlineSpecimens() {
        return m_onlineSpecimens;
    }

    public LinkedHashMap getOfflineSpecimens() {
        return m_offlineSpecimens;
    }

    public void setOfflineSpecimens(LinkedHashMap offlineSpecimens) {
        m_offlineSpecimens = offlineSpecimens;
    }

    /**
     * Gets the offlines specimens.
     *
     * @return LinkedHashMap containing offline specimens, or null if an exception occurred while
     * getting the update information.
     */
    private LinkedHashMap loadOffLineSpecimens() {
        try {
            if (m_offlineSpecimens == null)
                m_offlineSpecimens = new LinkedHashMap();
            else
                m_offlineSpecimens.clear();
            LinkedHashMap originalSpecimens = (new UpdateRetriever()).getAllSpecimens();
            // Cloning to make sure we do no change the original specimens during
            // the course of the application.
            m_allSpecimens = (LinkedHashMap) originalSpecimens.clone();
            Iterator it = originalSpecimens.keySet().iterator();
            // Filter out the specimens that have already been uploaded to the user machine.
            while (it.hasNext()) {
                OfflineSpecimen offlineSpecimen = (OfflineSpecimen) originalSpecimens.get(it.next());
                boolean isAlreadyUploaded = m_onlineSpecimens.containsKey(offlineSpecimen.getUniqueName());
                if (!isAlreadyUploaded) {
                    m_offlineSpecimens.put(offlineSpecimen.getUniqueName(), offlineSpecimen);
                } else {
                    // The long description of online specimens are not stored in their respective
                    // jars, since the DetailChooserView requires such information to be shown we
                    // shall hack this information out of the RSS feed and put it in our
                    // online specimens.
                    Specimen onlineSpecimen = (Specimen) m_onlineSpecimens.get(offlineSpecimen.getUniqueName());
                    setLongDescriptionForOnlineSpecimen(onlineSpecimen, offlineSpecimen);
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage());
        }
        return m_offlineSpecimens;
    }

    /**
     * This method retrieves the long description from All the input sourceSpecimens and sets this
     * to the correspondig onlineSpecimen. This method will be called when the sourceSpecimens contains
     * the proper longdescriptions (loaded from RSS feed) and the new online specimens do not contain
     * such description. In such a case we will copy the long descriptions to the new online specimens.
     *
     * @param sourceSpecimens
     */
    public void setLongDescriptionForAllOnlineSpecimens(LinkedHashMap sourceSpecimens) {
        Iterator it = m_onlineSpecimens.keySet().iterator();
        while (it.hasNext()) {
            Object onlineSpecimenID = it.next();
            String longDescription = ((Specimen) m_onlineSpecimens.get(onlineSpecimenID)).getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION);
            if (longDescription != null && longDescription.length() > 0) {
                Specimen sourceSpecimen = (Specimen) sourceSpecimens.get(onlineSpecimenID);
                if (sourceSpecimen != null) {
                    // set the long description of the online specimen from the sourceSpecimen.
                    setLongDescriptionForOnlineSpecimen(sourceSpecimen, (Specimen) m_onlineSpecimens.get(onlineSpecimenID));
                }
            }
        }
    }

    /**
     * This method retrieves the long description from the source specimen (usually the offline specimen)
     * and sets it into the target specimen (usually the online specimen).
     * The long description of online specimens are not stored in their respective
     * jars, since the DetailChooserView requires such information to be shown we
     * shall hack this information out of the offline specimen (which is loaded from the RSS feed,
     * and hence contains the long description) and put it in the corresponding online specimens.
     *
     * @param targetSpecimen
     * @param sourceSpecimen
     */
    private void setLongDescriptionForOnlineSpecimen(Specimen targetSpecimen, Specimen sourceSpecimen) {
        String longDescription = sourceSpecimen.getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION);
        String specimenStats = sourceSpecimen.getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_STATS);
        targetSpecimen.getSampleInfo().setNamedParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION, longDescription);
        targetSpecimen.getSampleInfo().setNamedParam(Constants.XMLTags.SAMPLE_STATS, specimenStats);
    }

    /**
     * This method loads the new specimens from the local machine. It does NOT  use
     * brute force i.e. instead of clearing ALL the online specimens and then reloading
     * from the disk it only reloads those specimens that aren't already present in the
     * online specimen collection. It also removes any new online specimens from the offline
     * specimens collection indicating that these are now available to the user and are not marked
     * offline anymore.
     */
    public void refreshOnlineSpecimens() {
        try {
            File directory = new File(m_config.getSpecimenDir());
            if (!directory.exists()) {
                throw new IllegalArgumentException("The specified specimen directory does not exist.");
            }

            File files[] = directory.listFiles(this);

            // use the file name to identify the specimens that have already been loaded.
            for (int i = 0; i < files.length; i++) {
                if (isAlreadyLoaded(files[i]))
                    continue;
                try {
                    Specimen newOnlineSpecimen = new Specimen(files[i]);
                    // First specimen is the default
                    if (m_defSpecimen == null) {
                        m_defSpecimen = newOnlineSpecimen;
                    }
                    m_onlineSpecimens.put(newOnlineSpecimen.getUniqueName(), newOnlineSpecimen);
                    if (m_offlineSpecimens.get(newOnlineSpecimen.getUniqueName()) != null)
                        setLongDescriptionForOnlineSpecimen(newOnlineSpecimen, (OfflineSpecimen) m_offlineSpecimens.get(newOnlineSpecimen.getUniqueName()));
                    // Specimen is available online now so we must unmark it as offline.
                    m_offlineSpecimens.remove(newOnlineSpecimen.getUniqueName());
                } catch (Exception e) {
                    log.info(e.getMessage());
                    continue;
                }
            }
            // Now remove any obsolete specimens, i.e. specimens that are not
            // available to us any more.
            Iterator it = m_onlineSpecimens.keySet().iterator();
            ArrayList tobeDeleted = new ArrayList();
            // mark the specimens that are not present anymore.
            while (it.hasNext()) {
                String specimenID = (String) it.next();
                String jarFile = ((Specimen) m_onlineSpecimens.get(specimenID)).getJarFilePath();
                boolean shouldBeDeleted = true;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].getAbsolutePath().equals(jarFile)) {
                        shouldBeDeleted = false;
                        break;
                    }
                }
                if (shouldBeDeleted)
                    tobeDeleted.add(specimenID);
            }
            // Delete the obsolete specimens.
            for (int i = 0; i < tobeDeleted.size(); i++) {
                m_onlineSpecimens.remove(tobeDeleted.get(i));
                // As we remove the online specimen we should see if this
                // needs to be added to the offline specimens. This check is required
                // since the user may have locally creatd specimens that are not
                // a part of the virtuallab official data repository, in such a case
                // these specimens would not have been published through RSS feed and
                // we should not add such specimens to offline panel.
                OfflineSpecimen offlineCounterpart = (OfflineSpecimen) m_allSpecimens.get(tobeDeleted.get(i));
                if (offlineCounterpart != null) {
                    // the online specimen should be move to the offline.
                    offlineCounterpart.changeJarFilePathToDownloadURL();// set the URL download.
                    m_offlineSpecimens.put(tobeDeleted.get(i), offlineCounterpart);
                }
            }
        } catch (Exception e) {
            String errorInfo = "Unable to create specimen set.";
            log.error(errorInfo);
            log.error(e.getMessage());
        }
        m_isDone = true;
    }

    /**
     * returns true if the specified jar file has already been loaded false otherwise.
     * It uses the jar file path to identify loaded specimens.
     *
     * @param file
     * @return
     */
    private boolean isAlreadyLoaded(File file) {
        Iterator it = m_onlineSpecimens.values().iterator();
        while (it.hasNext()) {
            Specimen specimen = (Specimen) it.next();
            if (specimen.getJarFilePath().equals(file.getAbsolutePath())) {
                return true;
            }
        }
        return false;
    }

    /**
     * This method reads in the specified specimen from the local disk and updates its
     * internal data structures to reflect the change. If the specified specimen can not
     * be found this method remians silent and does not report any error.
     *
     * @param specimenToRead
     */
    public void readInSpecimen(OfflineSpecimen specimenToRead) {
        try {
            Specimen newOnlineSpecimen = new Specimen(new File(specimenToRead.getJarFilePath()));
            m_onlineSpecimens.put(newOnlineSpecimen.getUniqueName(), newOnlineSpecimen);
            setLongDescriptionForOnlineSpecimen(newOnlineSpecimen, specimenToRead);
            m_offlineSpecimens.remove(specimenToRead.getUniqueName());
            m_isDone = true;
        } catch (Exception e) {
            String errorInfo = "Unable to create specimen set.";
            log.error(errorInfo);
            log.error(e.getMessage());
        }
    }

    /**
     * @return default specimen.
     */
    public Specimen getDefaultSpecimen() {
        return m_defSpecimen;
    }

    /**
     * @return An iterator over specimens.
     */
    public Iterator getSpecimenIterator() {
        return m_onlineSpecimens.values().iterator();
    }

    /**
     * Gets the specimen. It uses the internal datastructure
     * to look for the specimen. This method does not care if the
     * specimen has been deleted from the disk. If the caller wants
     * a specimen that MUST exist on the local machine then use the
     * getSpecimenIfAvailable() mehtod instead.
     *
     * @param uniqueName
     * @return
     */
    private Specimen getSpecimen(String uniqueName) {
        if (uniqueName == null || m_onlineSpecimens == null)
            return null;
        return (Specimen) m_onlineSpecimens.get(uniqueName);
    }

    /**
     * This method checks to see if the specimen is available in its
     * internal data structures, it also makes sure that the specimen
     * is available on the local disk. These two checks may be redundant
     * most of the times except of when the user manually deletes a loaded
     * specimen from the specimen folder and does NOT use Tools->Refresh to
     * bring the views in synch with whats available on the disk.
     *
     * @param uniqueName
     * @return Online Specimen if available, null otherwise.
     */
    public Specimen getSpecimenIfAvailable(String uniqueName) {
        Specimen requiredSpecimen = getSpecimen(uniqueName);
        if (requiredSpecimen == null)
            return null;
        File specimenFile = new File(requiredSpecimen.getJarFilePath());
        if (specimenFile.exists())
            return requiredSpecimen;
        else
            return null;
    }

    public boolean isOffline(String specimenUniqueName) {
        return (!m_onlineSpecimens.containsKey(specimenUniqueName));
    }

    public OfflineSpecimen getOfflineSpecimen(String specimenUniqueName) {
        return (OfflineSpecimen) m_offlineSpecimens.get(specimenUniqueName);
    }

    public boolean accept(File file) {
        return (file != null && file.getName().endsWith(".jar")) ? true : false;
    }

    public int getSpecimenCount() {
        return m_onlineSpecimens.size();
    }
}
