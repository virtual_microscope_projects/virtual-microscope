/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.awt.Dimension;
import java.awt.geom.Point2D;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

// $Id: SpecimenElement.java,v 1.8 2006/09/05 01:08:47 manzoor2 Exp $

public class SpecimenElement {
    private static final Log log = new Log(SpecimenElement.class.getName());

    public static final short MISCELLANEOUS_SPECIMEN = 0;
    public static final short OFFLINE_SPECIMEN = 1;
    public static final short ONLINE_SPECIMEN = 2;
    static private int idCount = 1;
    protected String m_id;
    protected LinkedHashMap m_params = new LinkedHashMap();
    /**
     * This object holds the information located under the Instrument Tag.
     */
    protected Specimen.InstrumentDescriptor m_instrumentDescriptor;
    /**
     * Stores the XML tags that have a value and a URL e.g. [Inforef href="..."]Howdy[/Information]
     */
    protected ArrayList m_infoRefParams = new ArrayList();

    static private String uniqueId() {
        // Assign an arbitrary but unique id
        return new Date().toString() + " " + Integer.toString(idCount++);
    }

    static protected String getElementId(Node node) {
        String id = null;

        if (node.hasAttributes()) {
            NamedNodeMap attrs = node.getAttributes();
            Node idNode = attrs.getNamedItem("id");

            if (idNode != null && !idNode.getNodeValue().equals("")) {
                id = idNode.getNodeValue();
            }
        }

        return id;
    }

    /**
     * The [ImageDescriptor], tags are referenced in the specimen.xml through the
     * [ControlStates id="MAGNIFICATIONSTATES"] node. This method sniffs out the
     * original [ImageDescriptor] tags from the referenced ones. We only need to
     * load the original tags. The referenced ones are only to link the
     * [ControlStates id="MAGNIFICATIONSTATES"] tags with the corresponding
     * [ImageDescriptor] tags, hence they need not be loaded.
     *
     * @param node
     * @return the "id" attribute of the node
     */
    static protected String getElementRefId(Node node) {
        String id = null;

        if (node.hasAttributes()) {
            NamedNodeMap attrs = node.getAttributes();
            Node idNode = attrs.getNamedItem("idref");

            if (idNode != null && !idNode.getNodeValue().equals("")) {
                id = idNode.getNodeValue();
            }
        }
        return id;
    }

    protected static boolean hasElementId(Node n) {
        return getElementId(n) != null;
    }

    protected static boolean isReference(Node n) {
        return getElementRefId(n) != null;
    }

    public String getId() {
        return m_id;
    }

    public short getSpecimenType() {
        return MISCELLANEOUS_SPECIMEN;
    }

    /**
     * This method adds/overwrites the input value into the parameter map.
     *
     * @param key
     * @param value
     * @return
     */
    public void setNamedParam(String key, String value) {
        if (m_params == null)
            m_params = new LinkedHashMap();
        m_params.put(key, value);
    }

    /**
     * This method returns the requested parameter's value by extracting it from
     * the m_params
     *
     * @param key
     * @return
     */
    public String getNamedParam(String key) {
        return (String) m_params.get(key);
    }

    /**
     * This method returns the diaply string.The Display string will
     * be shown in the title bar when the specimen is loaded. In the old
     * XML format this was stored as "DisplayString" whereas in the nex XML format
     * this is stored as "LongName"
     *
     * @return
     */
    public String getDisplayString() {
        String displayString = getNamedParam("DisplayString");
        return displayString;
    }

    /**
     * This method loads the leaf-node containing the tile size, image size and pixel dimensions
     * into the ImageDescriptor object.
     *
     * @param n        Node pointintg to the leaf node
     * @param revision the indicator of which XML loadinglogic to use
     */
    protected void loadImageDescriptor(Node n, double revision) {
        try {
            if (revision > 1.0) {
                // The node should be pointing to the Leaf Level node.
                if (n.hasChildNodes()) {
                    NodeList nl = n.getChildNodes();
                    for (int i = 0; i < nl.getLength(); i++) {
                        Node nn = nl.item(i);
                        //This gutsOfLoadElement() method loads the
                        // image size, tile size and the pixel dimension
                        // data
                        if (gutsOfLoadElement(nn))
                            continue;
                    }
                }
            }
            // TODO: Other future revision loading code goes here .....
        } catch (Exception ex) {
            log.error("Error loading Image Descriptior!");
            log.error(ex.getMessage());
        }

    }

    /**
     * This method receives the Node pointing to the instrument tag and loads the
     * Instrument information into the m_params HashMap. The revision parameter
     * helps it decide the loading logic according to the revision of the XML.
     * As of now it executes the same loading logic for any revision above 1.0. For revisions
     * less than or equal to 1.0 the loadElement() method should be called.
     *
     * @param Node     poiting to the Instrument tag in the XML
     * @param revision the XMLrevision. based on which the method will execute the appropriate
     * @param id       The tag name whose value will be set as the id for the control.
     *                 loading logic.
     */
    protected void loadInstrument(Node n, double revision, String id) {
        try {
            if (revision > 1.0) {
                // The node should be pointing to the Instrument tag.
                if (n.hasChildNodes()) {
                    NodeList nl = n.getChildNodes();
                    for (int i = 0; i < nl.getLength(); i++) {
                        Node nn = nl.item(i);
                        String name = nn.getNodeName();

                        // If it is an <InfoRef> tag we shall save it
                        // as the InfoRef Object which will hold both the
                        // href and the value for this tag nicely tucked
                        // into the InstrumentDescriptor. This is done in the
                        // gutsOfLoadElement() method.
                        if (gutsOfLoadElement(nn, revision))
                            continue;


                        String value = "";
                        if (nn.getNodeType() == Node.ELEMENT_NODE) {
                            for (int j = 0; j < nn.getChildNodes().getLength(); j++) {
                                Node nnn = nn.getChildNodes().item(j);
                                if (nnn.getNodeType() == Node.TEXT_NODE) {
                                    // we shall only process the nodes of the type
                                    // <ABA>value</ABC>
                                    value = nnn.getNodeValue();
                                    //System.out.println("Putting (" + name + ", " + value + ")");
                                    // The first textual input into the tag is the
                                    // one we are going to consider.
                                    break;
                                }
                            }
                            if (name.equals(id)) {
                                // The value stored in the control's this specific tag
                                // will be used as the id for the control. In previous
                                // version of the XML this was stored as the attribute named "id"
                                // for the respetive tags. Now the id may be stored as a
                                // separate element with a any name.
                                this.m_id = value;
                            }
                            m_params.put(name, value);
                        }
                    }
                }
            }
            // TODO: Other future revision loading code goes here .....
        } catch (Exception ex) {
            log.error("Error loading instrument!");
            log.error(ex.getMessage());
        }

    }

    /**
     * This method tries to load the unique id for a given node. If it exists it would
     * be set as an attribute named "id" in the node. If it does not exist this method
     * generates a timstamp postfixed with a counter and uses this as the unique id for
     * the node
     *
     * @param node
     */

    protected void loadId(Node node) {
        m_id = getElementId(node);

        if (m_id == null) {
            m_id = uniqueId();
        }
    }

    /**
     * This method loads tags with data such as <Node name="match" value="won">
     * Such attributes of the node will be stored in the m_params hash map as
     * ("match","won") name,value pairs. Inother words any node can use "name", "value"
     * attrbutes and no specific logic has to be implemented to handle this, as this method
     * will automatically load all such attributes.
     *
     * @param paramNode
     */
    protected void loadParam(Node paramNode) {
        NamedNodeMap attrs = paramNode.getAttributes();
        Node nameNode = attrs.getNamedItem("name");
        Node valueNode = attrs.getNamedItem("value");

        if (nameNode != null && valueNode != null) {
            m_params.put(nameNode.getNodeValue(), valueNode.getNodeValue());
        }
    }

    /**
     * This method gets a node (which is a generic node with general purpose
     * information e.g. Author, Copyright)and stores it along with all its children as
     * name-value pairs in the m_params HashMap. The method also checks if the node
     * has some Attributes. If it does it also stores them as (NodeName.AttributeName-value) pairs.
     *
     * @param node
     */
    protected void loadElementAsParam(Node node) {
        String name = node.getNodeName();

        if (name == null) {
            return;
        }

        String value = null;

        if (node.hasChildNodes()) {
            NodeList children = node.getChildNodes();

            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);

                if (child.getNodeType() == Node.TEXT_NODE) {
                    value = child.getNodeValue();
                    break; // Use only the first-encountered text node
                }
            }
        }

        m_params.put(name, value);

        // Atributes should be stored as (NodeName.AttributeName, Value) pairs.
        if (node.hasAttributes()) {
            NamedNodeMap attrs = node.getAttributes();

            for (int i = 0; i < attrs.getLength(); i++) {
                String subName = attrs.item(i).getNodeName();
                value = attrs.item(i).getNodeValue();

                if (subName != null) {
                    m_params.put(name + "." + subName, value);
                }
            }
        }
    }

    protected boolean gutsOfLoadElement(Node node) throws SpecimenException {
        return false;
    }

    /**
     * This method is implemented in the Specimen.InstrumentDescriptor class. This provides
     * a new version of the gutsOfLoadElement(Node node) method.
     *
     * @param node     Node pointing to "InfoRef" tag.
     * @param revision the XML revision number
     * @return true if the  Node is "InfoRef" type and new InfoRef object was successfully created.
     * @throws SpecimenException
     */
    protected boolean gutsOfLoadElement(Node node, double revision) throws SpecimenException {
        return false;
    }

    /**
     * This methid loads an individual element. It checks to see if the element has a unique
     * ID. If it does not it assigns it a time stamp as a unique ID. Then the function
     * gets all the children of the node and sees if any of them is a general purpose informational
     * node (e.g. Author, Copyright) if it is it will store these nodes as name-value pairs
     * in a datastructure which can be shared and consumed through out the application. However
     * if it comes across a specific node (e.g. ImageDescriptor) it will not do anything
     * since in this case the calling function will process such specific nodes.
     *
     * @param node
     */
    protected void loadElement(Node node) {
        loadId(node);

        //Get all the children of the node. And iterate on each of them.
        // This allows us to process all the children of the given node
        // in a single call to this function.
        NodeList children = node.getChildNodes();

        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);

            try {

                if (gutsOfLoadElement(child)) {
                    // We have encountered a specific non-informational node and we will
                    // defer its processing for later.
                    ;
                } else {
                    // the node is not a specific node so we shall handle it
                    // here

                    if (child.getNodeType() == Node.ELEMENT_NODE) {
                        // The encountered node is a general purpose informational node
                        // we will store it as name-value pairs. This information can then be
                        // used anywhere in the application by which ever module and in whatever
                        // way deemed suitable.
                        if (child.getNodeName().equals("Param")) {
                            loadParam(child);
                        } else {
                            // Generic element with only text and maybe attributes; save it as a key/value pair.
                            loadElementAsParam(child);
                        }
                    }
                }
            } catch (SpecimenException e) {
                continue; // TODO: rethrow the exception; prepare calling code to receive it.
            }
        }

        if (node.hasAttributes()) {
            NamedNodeMap attrs = node.getAttributes();

            for (int i = 0; i < attrs.getLength(); i++) {
                String name = attrs.item(i).getNodeName();
                String value = attrs.item(i).getNodeValue();

                if (name != null) {
                    m_params.put(name, value);
                }
            }
        }
    }

    protected Dimension loadDimensionAttribute(Node node) {
        NamedNodeMap attrs = node.getAttributes();
        Node widthNode = attrs.getNamedItem("width");
        Node heightNode = attrs.getNamedItem("height");

        Dimension dim = null;

        if (widthNode != null && heightNode != null) {
            int width = Integer.parseInt(widthNode.getNodeValue());
            int height = Integer.parseInt(heightNode.getNodeValue());

            dim = new java.awt.Dimension(width, height);
        }

        return dim;
    }

    protected Point2D.Double loadDoubleDimensionAttribute(Node node) {
        NamedNodeMap attrs = node.getAttributes();
        Node widthNode = attrs.getNamedItem("width");
        Node heightNode = attrs.getNamedItem("height");

        Point2D.Double dim = null;

        if (widthNode != null && heightNode != null) {
            double width = Double.parseDouble(widthNode.getNodeValue());
            double height = Double.parseDouble(heightNode.getNodeValue());
            dim = new Point2D.Double(width, height);
        }

        return dim;
    }

    protected String getFirstTextChild(Node node) {
        String retVal = null;

        if (node.hasChildNodes()) {
            NodeList children = node.getChildNodes();

            for (int i = 0; i < children.getLength(); i++) {
                Node child = children.item(i);

                if (child.getNodeType() == Node.TEXT_NODE) {
                    retVal = child.getNodeValue();
                    break; // Use only the first-encountered text node
                }
            }
        }

        return retVal;
    }
}