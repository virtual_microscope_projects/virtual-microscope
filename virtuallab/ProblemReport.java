/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import virtuallab.util.Constants;
import virtuallab.util.Locale;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.URI;
import java.net.URL;

class ProblemReport extends JFrame implements MouseListener {

    private static final Log log = new Log(ProblemReport.class.getName());

    /**
     * Holds the original cursor before the mouse entered the label. Used to
     * revert back to it when the mouse leaves the label
     */
    private Cursor m_oldCur = null;

    public ProblemReport(JFrame owner) {
        setTitle(Locale.ProblemReport(Configuration.getApplicationLanguage()));
        try {
            URL url = getClass().getResource(Constants.APPLICATION_ICON);
            Image image = ImageIO.read(url);
            this.setIconImage(image);
        } catch (Exception e) {
            log.error("Could not find the application icon:" + Constants.APPLICATION_ICON);
            log.error(e.getMessage());
        }

        JPanel bttnPanel = new JPanel();
        bttnPanel.setBackground(Color.WHITE);

        JButton bttnEmailNow = new JButton(Locale.OpenEmailClient(Configuration.getApplicationLanguage()));
        bttnEmailNow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Desktop.getDesktop().browse(new URI("mailto:" + Constants.SUPPORT_EMAIL_ADDRESS +
                            "?subject="+Locale.IssueTitle(Configuration.getApplicationLanguage()).replace(" ","%20")+
                            "&body="+Locale.IssueDescription(Configuration.getApplicationLanguage()).replace(" ","%20")));
                } catch (Exception exception) {
                    log.error(Locale.ErrorOpeningEmail(Locale.LANGUAGE.EN));
                    log.error(exception.getMessage());
                    JOptionPane.showMessageDialog(ProblemReport.this, Locale.ErrorOpeningEmail(Configuration.getApplicationLanguage())+"!",
                            Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                }
            }
        });


        JButton bttnClose = new JButton(Locale.Cancel(Configuration.getApplicationLanguage()));
        bttnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        bttnPanel.add(bttnEmailNow);
        bttnPanel.add(bttnClose);
        Font labelFont = new Font("Verdana", Font.PLAIN, 14);

        JTextArea supportInstructions = new JTextArea();
        supportInstructions.setEditable(false);
        supportInstructions.setFont(labelFont);
        supportInstructions.setText(Constants.SPACE+Locale.ReportIssues(Configuration.getApplicationLanguage())+":\n  " + Constants.SUPPORT_EMAIL_ADDRESS + Constants.SPACE);

        JPanel supportPanel = new JPanel();
        BoxLayout layout = new BoxLayout(supportPanel, BoxLayout.PAGE_AXIS);
        supportPanel.setLayout(layout);
        supportPanel.setBackground(Color.WHITE);
        supportPanel.add(Box.createRigidArea(new Dimension(10, 10)));
        supportPanel.add(supportInstructions);
        supportPanel.add(Box.createRigidArea(new Dimension(10, 10)));

        int windowWidth = 600;
        int windowHeight = 50;
        supportPanel.setPreferredSize(new Dimension(windowWidth, windowHeight));

        setResizable(false);

        getContentPane().add(supportPanel, BorderLayout.CENTER);
        getContentPane().add(bttnPanel, BorderLayout.SOUTH);

        pack();
        setLocationRelativeTo(owner);
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        m_oldCur = getCursor();
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }

    public void mouseExited(MouseEvent e) {
        setCursor(m_oldCur);
    }
}
