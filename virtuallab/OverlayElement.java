/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.awt.Color;
import java.awt.Point;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.io.InputStream;
import java.util.Vector;

import javax.imageio.ImageIO;

import virtuallab.util.Constants;
import virtuallab.util.OverlayElementCheckbox;
import virtuallab.util.Utility;

/**
 * Each element that appears under the "EDS" control has some attributes
 * associated with it. This utility class encapsulates the associated data
 * of the overlay element.
 *
 */
public class OverlayElement implements Comparable {

    private static final Log log = new Log(OverlayElement.class.getName());

    /**
     * the maximum value that our m_pixelBrightnessValues can store.
     * Used to convert the brightness specified in the scale of [0,32767] to the
     * float value in the range of [0.0, 1.0].
     */
    public final static short MAX_BRIGHTNESS_VALUE = 127;
    /**
     * This field contains the base data for the element spectra graph. This is the base data
     * which determines the shape of the graph spectra. When the user clicks on a particular point on the overlay image
     * we shall use this data to plot the graph (after doing necessary manipulations ofcourse).
     */
    public Vector m_baseGraphData;

    /**
     * The checkbox with which this element is associated with.
     */
    public OverlayElementCheckbox m_parentCheckbox;

    /**
     * Holds the total xray count with in the specified area. Used while
     * plotting grpah through the Area Tool.
     */
    public float m_areaXrayCount = 0;
    /**
     * The energy level for this element. This value correspond to the place on the X-Axis
     * where this element's peak will be plotted.
     */
    public float m_energyLevel;
    /**
     * Holds the index corresponding to the current point in the overlap image
     * for which the Point graph is being plotted.
     */
    public Point m_selectedIndex;
    /**
     * The pixel brightness value that represents the xray count at the particular point in the
     * overlay image. These valus are used both to draw overlay images and to draw the graphs.
     * When drawing the overlay images these values represent the pixel brightness and when
     * drawing the graphs these values represent the X-Ray counts.
     */
    public byte[][] m_pixelBrightnessValues;
    /**
     * The short name of the element that will be displayed against the check box.
     */
    public String m_shortName;
    /**
     * The long name of the element that will be displayed as annotation on the graph.
     */
    public String m_longName;
    /**
     * The id of the element that uniquely identifies it in the config file.
     */
    public String m_id;
    /**
     * The map file path which stores the X-Ray count value as pixel brightness.
     */
    public String m_mapFilePath;
    /**
     * The pixel width of the image map. This information is used while overlaying the
     * image file on top of the base image.
     */
    public double m_scale;
    /**
     * The pixel height of the image map. This information is used while overlaying the
     * image file on top of the base image.
     */
    public double m_pixelSize;
    /**
     * True if the image needs to be recreated false otherwise. This is used to avoid
     * unnecessary image recreation during the scrolling.
     */
    private boolean m_isDirty = true;
    /**
     * Holds the points corresponding to the Line Graph. i.e. all the points lying on the
     * user drawn line are stored in this vector as Point2D.Double
     */
    private Vector m_lineGraphPoints;
    /**
     * The height of the overlay image, which is also the size of one of the
     * dimensions of the m_pixelBrightnessValues
     */
    private int m_imageWidth;
    /**
     * The width of the overlay image, which is also the size of one of the
     * dimensions of the m_pixelBrightnessValues
     */
    private int m_imageHeight;

    /**
     * The hue of the color of the overlay element.
     * Note that we do not store the brighntess since that varies
     * for eacg pixel. The pixel brightness value is atored in the
     * m_pixelBrightnessValues array
     */
    private float m_hue;
    /**
     * The saturation of the color of the overlay element.
     * Note that we do not store the brighntess since that varies
     * for eacg pixel. The pixel brightness value is atored in the
     * m_pixelBrightnessValues array
     */
    private float m_saturation;


    /**
     * Constructor
     *
     * @param id
     * @param shortName
     * @param longName
     * @param energyLevel
     */

    OverlayElement(String id, String shortName, String longName, float energyLevel) {
        m_id = id;
        m_shortName = shortName;
        m_longName = longName;
        m_energyLevel = energyLevel;
        // setDefaultHSValues();
    }

    /**
     * Default conbstructor.
     */
    OverlayElement() {
    }


    /**
     * This method adds the new point to the line graph X-Axis points
     *
     * @param point
     */
    public void addToLineGraphPoints(Point2D.Double point) {
        if (m_lineGraphPoints == null)
            m_lineGraphPoints = new Vector();
        m_lineGraphPoints.add(point);
    }

    /**
     * This method adds the new point to the line graph X-Axis points
     *
     * @param point
     */
    public void addToLineGraphPoints(Vector pointVector) {
        m_lineGraphPoints = pointVector;
    }


    /**
     * This method adds the new point to the line graph X-Axis points
     *
     * @param point
     */
    public void addToLineGraphPoints(double pointX, double pointY) {
        if (m_lineGraphPoints == null)
            m_lineGraphPoints = new Vector();
        m_lineGraphPoints.add(new Point2D.Double(pointX, pointY));
    }

    /**
     * This method adds the new point to the line graph X-Axis points
     *
     * @param point
     */
    public void clearLineGraphPoints(double pointX, double pointY) {
        if (m_lineGraphPoints != null)
            m_lineGraphPoints.clear();
    }

    /**
     * This method retrieves the XRay count values for all the points on the user
     * drawn Line. It already contains the points plotting on the line in the
     * variable m_lineGraphPoints, it uses this information to retrieve the XRay
     * count from the array m_pixelBrightnessValues for these points.
     *
     * @return The vector containing the Double value for the XRay count, in ascending X-Axis order
     * of the drawn line
     */
    public Vector getLineGraphDataSeries() {
        Vector vt = new Vector();
        for (int i = 0; i < m_lineGraphPoints.size(); i++) {
            int x = (int) Math.round(((Point2D.Double) m_lineGraphPoints.get(i)).getX());
            int y = (int) Math.round(((Point2D.Double) m_lineGraphPoints.get(i)).getY());
            //Applying limit checks. This is required because the overlay and the base images may
            // have slightly different dimensions.
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            if (x >= m_pixelBrightnessValues.length) x = m_pixelBrightnessValues.length - 1;
            if (y >= m_pixelBrightnessValues[0].length) y = m_pixelBrightnessValues[0].length - 1;
            // the x-axis values are just the percentage of the line points.
            Point2D.Double pt = new Point2D.Double((double) i / m_lineGraphPoints.size() * 100, (double) m_pixelBrightnessValues[x][y]);

            vt.add(pt);
        }
        return vt;
    }


    public Point getSelectedIndex() {
        return m_selectedIndex;
    }

    public void setSelectedIndex(Point xy) {
        m_selectedIndex = xy;
    }

    public float getSelectedIndexXRayCount() {
        return m_pixelBrightnessValues[m_selectedIndex.x][m_selectedIndex.y];
    }

    public void setSelectedIndex(int x, int y) {
        m_selectedIndex = new Point(x, y);
    }

    public int getImageRGB(int row, int col) {
        return Color.HSBtoRGB(m_hue, m_saturation, (float) m_pixelBrightnessValues[row][col] / (float) MAX_BRIGHTNESS_VALUE);

    }

    /**
     * This method should be deleted, since with the new logic the callers should have no
     * need to call this method.
     *
     * @param row
     * @param col
     * @param rgb public void setImageRGB(int row, int col, int rgb)
     *            {
     *            //Color.RGBtoHSB()
     *            int red = (rgb>>16)&0xFF;
     *            int green = (rgb>>8)&0xFF;
     *            int blue = rgb&0xFF;
     *            float[] hsb = Color.RGBtoHSB(red, green, blue, null);
     *            m_hue = hsb[0];
     *            m_saturation = hsb[1];
     *            }
     */
    public int getImageWidth() {
        return m_imageWidth;
    }

    public int getImageHeight() {
        return m_imageHeight;
    }

    /**
     * This method reads the overlay image and extracts the brightness values for each pixel
     * from the image. The overlayimages technically do not have to be images since all
     * they commnicate to us is the pixel brighntess values. We are only interested in the
     * pixel brightness values and do not care about the hue and saturation. The brightness
     * values represents the X-Ray count at a particular point in the image.
     *
     * @param oe
     * @param overlayImageFileStream
     */
    public void loadOverlayImage(Specimen mySpecimen) {
        try {
            long millis = System.currentTimeMillis();

            BufferedImage image = null;
            InputStream overlayImageFileStream = mySpecimen.getInputStream(m_mapFilePath);
            if (m_mapFilePath.endsWith(".png") || Constants.JDK_VERSION >= 1.5) {
                image = ImageIO.read(overlayImageFileStream);
            } else {
                // Only JDK version 1.5 and above support .bmp format. For older
                // JDK versions we have to use a custom bmp loading script.
                image = Utility.loadbitmap(overlayImageFileStream);
            }
            m_imageWidth = image.getWidth();
            m_imageHeight = image.getHeight();
            loadPixelBrightnessValues(image);
            overlayImageFileStream.close();
            image.flush();
            image = null;
            millis = System.currentTimeMillis() - millis;
        } catch (Exception e) {
            log.error("Error loading specimen Overlay Image");
            log.error(e.getMessage());
            m_pixelBrightnessValues = null;
        }
    }


    /**
     * This method extracts the brightness values of each pixel and stores
     * them in the local datastructure. These values will later be used to
     * draw EDS graphs and to create overlay images.
     *
     * @param overlayImage
     */
    private void loadPixelBrightnessValues(BufferedImage overlayImage) {
        int cols = overlayImage.getWidth();
        int rows = overlayImage.getHeight();
        m_pixelBrightnessValues = new byte[cols][rows];
        for (int i = 0; i < cols; i++) {
            for (int j = 0; j < rows; j++) {
//					m_pixelBrightnessValues[i][j]= (short)MAX_BRIGHTNESS_VALUE;
//					if (true) continue;
                int rgb = overlayImage.getRGB(i, j);
                // convert to HSB values.
                int red = (rgb >> 16) & 0xFF;
                int green = (rgb >> 8) & 0xFF;
                int blue = rgb & 0xFF;
                float[] inputHSB = new float[3];
                inputHSB = Color.RGBtoHSB(red, green, blue, inputHSB);
                float brightness = inputHSB[2];
                m_pixelBrightnessValues[i][j] = (byte) Math.round(brightness * MAX_BRIGHTNESS_VALUE);
//					m_pixelBrightnessValues[i][j]= (short)Math.round((0.212671f * red + 0.715160f * green + 0.072169f * blue) * MAX_BRIGHTNESS_VALUE);
            }
        }
    }

    /**
     * Sets the hue and saturatio value of the overlay element color.
     * Note that we do not set the brighntess value, since brighntess varies
     * for each pixel. The pixel brightness value is atored in the
     * m_pixelBrightnessValues array and it gets loaded only once at the time when the
     * specimen gets loaded, and is invariant. This is because the brightness
     * depicts the X-Ray count at a particular point in the image - which
     * is an invariant property of a specimen. The reason why hue and saturation
     * needs to be changed is because we give the user an option to change the color
     * of the image overlay, but we still need to show the image overlay such that
     * the brightness of a pixel depicts the X-ray count - hence whenever the user
     * changes the color of the overlay we shall only update the hue and saturation
     * values for the image and keep the brightness value unchanged.
     * <p>
     * In short the hue and saturation apply to the whole overlay image (i.e. it
     * is an overlay image property) whereas the brightness applies to the
     * individual pixel (i.e. brightness is a pixel specific property).
     * brightness value applies
     *
     * @param hue
     * @param saturation
     */
    public void setHSValues(float hue, float saturation) {
        m_hue = hue;
        m_saturation = saturation;
    }

    /**
     * This method updates the color of the overlay element.
     */
    public void changeColor(Color newColor) {
        float[] hsb = Color.RGBtoHSB(newColor.getRed(),
                newColor.getGreen(),
                newColor.getBlue(),
                null);
        m_hue = hsb[0];
        m_saturation = hsb[1];
    }

    public void cleanUp() {
        m_baseGraphData.clear();
        m_baseGraphData = null;
        m_pixelBrightnessValues = null;
        m_parentCheckbox = null;
        m_pixelBrightnessValues = null;
    }

    /**
     * Returns if the overlay is dirty i.e. require a redrawing.
     *
     * @return true if the overlay image is dirty false otherwise.
     */
    public boolean isDirty() {
        return m_isDirty;
    }

    /**
     * Sets the image to dirty, indicating that the overlay element should be redrawn.
     *
     * @param isDirty if set to true the image is marked as dirty i.e.
     *                it needs to be redrawn
     */
    public void makeDirty() {
        m_isDirty = true;
    }

    /**
     * Sets the image to clean indicating that the overlay element does not need to be redrawn.
     *
     * @param isDirty if set to true the image is marked as dirty i.e.
     *                it needs to be redrawn
     */
    public void makeClean() {
        m_isDirty = false;
    }

    /**
     * The comparator that helps us in sorting the OverlayElements w.r.t. the energylevels.
     *
     * @param The object with who the comparison is to be performed.
     * @return 0 is equal -1 if this object is less than the one it is compared to +1 otherwise.
     */
    public int compareTo(Object o2) {
        float el1 = m_energyLevel;
        float el2 = ((OverlayElement) o2).m_energyLevel;
        if (el1 == el2)
            return 0;
        else if (el1 > el2)
            return 1;
        else
            return -1;
    }
}
