/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import java.io.File;
import java.io.FilenameFilter;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.JOptionPane;

import virtuallab.util.Constants;
import virtuallab.util.Locale;

/**
 * This class launches the virtual microscope application. The virtual microscope
 * application rquires additional memory to run efficiently. This class is used
 * to run the virtual microscope application with extended heap size to ensure
 * efficient execution of the application.
 *
 */
public class LaunchVirtualMicroscope {

    private static final Log log = new Log(LaunchVirtualMicroscope.class.getName());

    /**
     * @param args
     */
    public static void main(String[] args) {
        try {

            // Get the directory contents and find out the virtual microscope
            // executable. If there are multiple then we pick the one that was
            // modified most recently.
            File dir = new File(Constants.VIRTUAL_EXECUTABLE_LOCATION);
            String latestFile = "";
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return (name.endsWith(".jar") && name.startsWith("Virtual"));
                }
            };
            File[] files = dir.listFiles(filter);
            String path = dir.getAbsolutePath();
            if (files == null) {
                JOptionPane.showMessageDialog(null,
                        Locale.CouldNotLocate(Configuration.getApplicationLanguage())+Constants.SPACE+path+Constants.SPACE+Locale.Folder(Configuration.getApplicationLanguage()).toLowerCase()+
                        Constants.NEW_LINE + Locale.VirtuallabExecutableIsPresent(Configuration.getApplicationLanguage())+".",
                        Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            } else {
                // latest modified file ends up at top.
                Arrays.sort(files, new Comparator() {
                    public int compare(Object o1, Object o2) {
                        File f1 = (File) o1;
                        File f2 = (File) o2;
                        if (f1.lastModified() > f2.lastModified())
                            return -1;
                        else if (f1.lastModified() < f2.lastModified())
                            return 1;
                        else return 0;

                    }
                });
            }
            if (files.length == 0) {
                JOptionPane.showMessageDialog(null,
                        Locale.VirtuallabExecutableNotFound(Configuration.getApplicationLanguage())+"."+
                        Constants.NEW_LINE+Locale.DownloadTheVirtualLabExecutable(Configuration.getApplicationLanguage())+Constants.SPACE+"https://gitlab.com/virtual_microscope_projects/virtual-microscope. " +
                        Constants.NEW_LINE+Locale.IfErrorPersists(Configuration.getApplicationLanguage())+Constants.SUPPORT_EMAIL_ADDRESS,
                        Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                System.exit(1);
            } else {
                latestFile = files[0].getCanonicalPath();
                // Pick up the first one in the array, since this is the latest file.
                String[] commandLine = new String[]{"java",
                        "-Dapple.laf.useScreenMenuBar=true",
                        "",
                        "-Djava.library.path=resources/",
                        "-Xmx1024m",
                        "-jar", latestFile};

                boolean isMacOS = (System.getProperty("mrj.version") != null);
                if (isMacOS) {
                    commandLine[2] = "-Xdock:name=VirtualLab";
                }

                /*Process proc = */
                Runtime.getRuntime().exec(commandLine);

            }
            System.exit(0);
        } catch (Exception e) {
            String errorInfo = Locale.ErrorWhileSearchingForTheVirtuallab(Configuration.getApplicationLanguage())+"."+
            Constants.NEW_LINE + Locale.IfTheProblemPersists(Configuration.getApplicationLanguage())+Constants.SPACE+Constants.SUPPORT_EMAIL_ADDRESS;
            JOptionPane.showMessageDialog(null, errorInfo, Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorWhileSearchingForTheVirtuallab(Locale.LANGUAGE.EN));
            log.error(e.getMessage());
            System.exit(1);
        }
    }
}