/*
 * Copyright (c) 2018-2019 Viorel Bota
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Virtual Microscope Software"),
 * to deal in the Virtual Microscope Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Virtual Microscope Software, and to permit persons to whom the Virtual Microscope Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Virtual Microscope Software.
 *
 * Redistributions in binary form must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * Redistributions of source code must implement the original license conditions
 * as written in the NOTES Section 1 from below.
 *
 * THE Virtual Microscope Software IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE Virtual Microscope Software OR THE USE OR OTHER DEALINGS IN THE
 * Virtual Microscope Software.
 *
 * NOTES:
 * Section 1. This project is a fork. The original license was:
 *  University of Illinois/NCSA Open Source License
 *
 *  Copyright (c) 2005, Imaging Technology Group, All rights reserved.
 *
 *  Developed by:
 *  Imaging Technology Group
 *  Beckman Institute for Advanced Science and Technology
 *  University of Illinois at Urbana-Champaign
 *
 *  http://virtual.itg.uiuc.edu
 *  virtual@itg.uiuc.edu
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the
 *  "Software"), to deal with the Software without restriction, including
 *  without limitation the rights to use, copy, modify, merge, publish,
 *  distribute, sublicense, and/or sell copies of the Software, and to
 *  permit persons to whom the Software is furnished to do so, subject to
 *  the following conditions:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimers.
 *
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimers in the
 *  documentation and/or other materials provided with the distribution.
 *
 *  Neither the names of the Imaging Technology Group, Beckman Institute
 *  for Advanced Science and Technology, University of Illinois at
 *  Urbana-Champaign, nor the names of its contributors may be used to
 *  endorse or promote products derived from this Software without
 *  specific prior written permission.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *  OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *  IN NO EVENT SHALL THE CONTRIBUTORS OR COPYRIGHT HOLDERS BE LIABLE FOR
 *  ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *  SOFTWARE OR THE USE OR OTHER DEALINGS WITH THE SOFTWARE.
 *
 * Section 2. Third party libraries:
 * The project uses libraries from java.base, java.desktop, jogl, jfreechart, jcommon, sax and w3c.
 * You can find the license for them here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * Instructions on how to replace the jfreechart and jcommon with another version can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 *
 * The versions of the source code and binaries
 * for jfreechart and jcommon used to build the application can be found here:
 * https://gitlab.com/virtual_microscope_projects/virtual-microscope/blob/master/Third%20Party%20Licenses
 */


package virtuallab;

import virtuallab.gui.*;
import virtuallab.update.SpecimenDownloadManager;
import virtuallab.update.UpdateRetriever;
import virtuallab.util.Constants;
import virtuallab.util.Locale;
import virtuallab.util.TranslatedOptionPane;
import virtuallab.util.Utility;

import javax.imageio.ImageIO;
import javax.print.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;

// $Id: VirtualMicroscope.java,v 1.36 2006/09/06 21:08:12 manzoor2 Exp $

// TODO: Clean up exception handling.
// TODO: Perimeter measure
// TODO: Area measure
// TODO: Platform-specific installs
/**
 * This class is where the main application strats from. This class has the
 * runnable main method that gets called when the application is launched. This
 * class is the parent JFrame for the application.
 *
 * @implements WindowListener This interface is implemented so that we can
 * handle customized application closing behavior.
 * @extends JFrame the application GUI is a standard JFrame.
 */

public class VirtualMicroscope extends JFrame implements WindowListener {

    private static final Log log = new Log(VirtualMicroscope.class.getName());

    /**
     * First part of the window title.
     */
    protected static final String TITLE_VLAB = "Virtual Miscroscope : ";
    /**
     * Default title suffix.
     */
    private static final long serialVersionUID = -3552076461930245420L;
    /**
     * Configuration.
     */
    public Configuration m_config = null;
    /**
     * The bullet that is shown against the selectd view menu item
     */
    ImageIcon m_selectedViewBulletIcon;
    ImageIcon m_unSelectedViewBulletIcon;
    /**
     * The File->View->menu item representing the Detail View
     */
    private JMenuItem m_itemDetailView;
    private ViewActionListener m_viewActionListener = new ViewActionListener();
    private JToolBar m_toolbar;
    // private JToggleButton m_detailViewToolBarButton;
    private JToggleButton m_viewToolBarButton;
    /**
     * The File->View->menu item representing the Icon View
     */
    private JMenuItem m_itemIconView;
    /**
     * the icon of the specimen button that was most recently loaded. This is
     * used to set reset the icon since when the user presses the button we
     * replace the icon with the text "Loading Please wait..."
     */
    private Icon m_loadedSpecimenIcon;
    private JButton m_loadedSpecimenButton;
    /**
     * The class that manages the downloads of the specimens
     */
    private SpecimenDownloadManager m_downloadManager;
    private JMenuItem m_itemDelete;
    /**
     * Menu bar for microscope.
     */
    private JMenuBar m_scopeMenuBar;
    /**
     * Menu bar for chooser.
     */
    private JMenuBar m_chooserMenuBar;
    /**
     * Microscope panel.
     */
    private Microscope m_microscope = null;
    /**
     * ChooserView panel. This is a reference to the currently active view. This
     * will either be pointing to m_iconChooserView or to m_detailChooserView.
     * The method m_chooser.getViewType() can tell which view m_chooser
     * currently points to
     */
    private AbstractChooserView m_chooser = null;
    /**
     * Holds the reference to the Icon View
     */
    private IconChooserView m_iconChooserView = null;
    /**
     * Holds the referce to Detail View.
     */
    private DetailChooserView m_detailChooserView = null;
    /**
     * Set of loaded specimens.
     */
    private SpecimenSet m_specimens = null;
    /**
     * Currently selected specimen.
     */
    private Specimen m_curSpecimen = null;
    /**
     * The menu item for Specimen Information
     */
    private JMenuItem m_informationItem;

    private VirtualMicroscope() {
        try {
            URL url = getClass().getResource(Constants.APPLICATION_ICON);
            Image image = ImageIO.read(url);
            this.setIconImage(image);
        } catch (Exception e) {
            log.error("Could not find the application icon:" + Constants.APPLICATION_ICON);
            log.error(e.getMessage());
        }

        // Read (and attempt to fix if broken) configuration
        m_config = new Configuration();

        // Validate configuration
        if (!m_config.validateConfig()) {
            JOptionPane.showMessageDialog(this,
                    "Virtual Microscope "+Locale.InvalidConfiguration(Configuration.getApplicationLanguage()).toLowerCase()+", "+
                    Locale.DownloadTheVirtualLabExecutable(Configuration.getApplicationLanguage())+Constants.SPACE+
                    "https://gitlab.com/virtual_microscope_projects/virtual-microscope/tree/master/deployed", Locale.Warning(Configuration.getApplicationLanguage()),
                    JOptionPane.WARNING_MESSAGE);
            // TODO: Pop up Configuration dialog

            // throw new IllegalArgumentException("Invalid configuration.");
        }

        // Figure out the preferred position from its size and the size of the
        // screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        int locX = screenSize.width / 2 - (Constants.VIEWER_PREFERRED_DIM.width / 2);
        int locY = screenSize.height / 2 - (Constants.VIEWER_PREFERRED_DIM.height / 2) - 40;

        locX = Math.max(locX, 0);
        locY = Math.max(locY, 0);

        // Set location
        setLocation(locX, locY);
        addKeyListener(new KeyboardListener());
        // Other initializations
        m_scopeMenuBar = m_chooserMenuBar = null;
    }

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            final VirtualMicroscope app = new VirtualMicroscope();
            Object[] options = {"OK", Locale.InstallationTutorial(Configuration.getApplicationLanguage())};
            int reply = 0;
            float version = 1.5f;
            String jdkVersion = System.getProperty("java.version");
            try {
                version = Float.parseFloat(jdkVersion.substring(0, 3));
            } catch (Exception ex) {
                version = 1.5f;
            }
            Constants.JDK_VERSION = version;
            if (version < 1.5 && app.m_config.shouldCheckJdk())
                reply = JOptionPane.showOptionDialog(
                                null,
                                Locale.RunningJDKversion(Configuration.getApplicationLanguage())+Constants.SPACE+
                                Constants.JDK_VERSION+
                                Locale.JDKrecommandation(Configuration.getApplicationLanguage())+
                                Constants.NEW_LINE+
                                "JDK 1.5 "+Locale.OrAbove(Configuration.getApplicationLanguage())+
                                Locale.JDKlimitations(Configuration.getApplicationLanguage())+Constants.SPACE+ Constants.JDK_VERSION + "." +
                                Constants.NEW_LINE+
                                Locale.BugReportRequest(Configuration.getApplicationLanguage())+
                                Constants.NEW_LINE+
                                Constants.SUPPORT_EMAIL_ADDRESS+
                                Constants.NEW_LINE+
                                Locale.DisableJDKwarning(Configuration.getApplicationLanguage()),
                                Locale.JavaVersionWarning(Configuration.getApplicationLanguage()),
                                JOptionPane.DEFAULT_OPTION,
                                JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            if (reply == 1) {
                System.exit(0);
            }

            app.readOnlineSpecimens();
            // download any new annotations.
            new UpdateRetriever().getAllAnnotations(app.m_config.getAnnotDir());
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    try {

                        IconChooserView iconView = new IconChooserView();
                        DetailChooserView detailView = new DetailChooserView();
                        app.setChooserViews(iconView, detailView);
                        app.initializeAndShowChooser(iconView, detailView);

                        app.pack();
                        // Once done we must dispose off the progress bar that may still be showing.
                        if (app.m_specimens.isVisible())
                            app.m_specimens.dispose();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, Locale.FatalErrorAtLaunch(Configuration.getApplicationLanguage()),
                                Locale.FatalError(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                        log.error(Locale.FatalErrorAtLaunch(Locale.LANGUAGE.EN));
                        log.error(ex.getMessage());
                        System.exit(1);
                    }
                }
            });
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, Locale.ErrorStarting(Configuration.getApplicationLanguage())+Constants.SPACE+Constants.APPLICATION_NAME + ".",
                    Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
            log.error(Locale.ErrorStarting(Locale.LANGUAGE.EN)+Constants.SPACE+Constants.APPLICATION_NAME);
            log.error(e.getMessage());
        }
    }

    /**
     * Here we customize the application behavior when the widow is being
     * closed.
     *
     * @param event WindowEvent
     */
    public void windowClosing(WindowEvent event) {
        if (JOptionPane.YES_OPTION == new TranslatedOptionPane().yesNo(VirtualMicroscope.this, Locale.ExitVirtualMicroscope(Configuration.getApplicationLanguage())+"?",
                Locale.ConfirmExit(Configuration.getApplicationLanguage())) ) {
            if (m_curSpecimen != null) {
                try {
                    if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                        return;
                } catch (Exception ex) {
                    log.error("Error closing annotation!");
                    log.error(ex.getMessage());
                }
            }
            if (m_downloadManager != null)
                // cancel all in-progress downloads.
                m_downloadManager.cancelAllDownloads();
            dispose();
            System.exit(0);
        }
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowGainedFocus(WindowEvent e) {
    }

    public void windowLostFocus(WindowEvent e) {
    }

    public void windowStateChanged(WindowEvent e) {
    }

    /**
     * Creates (if not already created) and shows a ChooserView with an
     * appropriate menu.
     */
    private void createAndShowChooser() {

        setTitle(TITLE_VLAB + Constants.APPLICATION_NAME);

        // De-select current specimen
        if (m_curSpecimen != null) {
            // close the opened jar file handle.
            m_curSpecimen.close();
            m_curSpecimen = null;
        }

        setJMenuBar(makeChooserMenuBar());
        // Create chooser if does not already exist
        if (m_chooser == null) {
            m_chooser = new ChooserView(m_config, m_specimens
                    .getOnlineSpecimens(), m_specimens.getOfflineSpecimens(),
                    new SpecimenChooserListener());
            m_chooser.setPreferredSize(Constants.VIEWER_PREFERRED_DIM);

            // To handle the processing of user pressing the close (X)
            // button from the top of the window. If this is the first chooser
            // window then we shall add the handler else we already have
            // one actionlistener registered and we shall ignore the
            // reloading of choosers.
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            addWindowListener(this);
        } else {
            // upon loading the specimen we had changed the cursor to wait, now
            // is the time to undo the change
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        // Clean up content pane and add chooser
        getContentPane().removeAll();
        setToolBar();
        getContentPane().add(m_chooser);
        // reset the icon for the last loaded specimen button. This is needed
        // because when we loaded the specimen we set the button text to
        // "Loading Please Wait" now we need to undo that change.
        if (m_loadedSpecimenButton != null) {
            m_loadedSpecimenButton.setText("");
            m_loadedSpecimenButton.setIcon(m_loadedSpecimenIcon);
        }
        // Closing any EDS graph windows that may be openend at the time of
        // unloading of the specimen.
        if (m_microscope != null && m_microscope.getMicroscopeView() != null
                && m_microscope.getMicroscopeView().getEDSControl() != null) {
            m_microscope.getMicroscopeView().getEDSControl().closeAllGraphs();
            // the EDSMenu loads lot of image data, we need to clean up all of
            // that.
            m_microscope.getMicroscopeView().getEDSControl().cleanUp(1.2);
        }
        setVisible(true);
        repaint();
    }

    public void setChooserViews(IconChooserView iconView,
                                DetailChooserView detailView) {
        m_iconChooserView = iconView;
        m_detailChooserView = detailView;
    }

    /**
     * Enables/Disables the download buttons for the respective offline
     * specimens in both the icon and detailview.
     *
     * @param specimenID
     * @param enabled
     */
    public void setSpecimenDownloadButtonsEnabled(String specimenID,
                                                  boolean enabled) {
        m_iconChooserView.setSpecimenButtonEnabled(specimenID, enabled);
        m_detailChooserView.setSpecimenButtonEnabled(specimenID, enabled);
    }

    private void setToolBar() {
        // ////////
        // Create a horizontal toolbar
        // JToolBar toolbar = new JToolBar();
        // Create a vertical toolbar
        if (m_toolbar == null) {
            m_toolbar = new JToolBar();
            m_toolbar.setBackground(m_chooserMenuBar.getBackground());
            // m_toolbar.setBorderPainted(true);
            m_toolbar.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            m_toolbar.setFloatable(false);

            m_toolbar.setLayout(new BorderLayout());

            // Get current orientation
            // int orient = toolbar.getOrientation();

            Insets bttnMargins = new Insets(0, 0, 0, 0);

            m_viewToolBarButton = new JToggleButton(new ImageIcon(getClass().getResource(Constants.Resources.ICONVIEW)));
            m_viewToolBarButton.setText(Locale.SwitchToDetailView(Configuration.getApplicationLanguage()));
            m_viewToolBarButton.setSelectedIcon(new ImageIcon(getClass().getResource(Constants.Resources.DETAILSVIEW)));
            m_viewToolBarButton.setToolTipText("<html>"+Locale.ToggleBetweenIconDetailView(Configuration.getApplicationLanguage())+"</html>");
            m_viewToolBarButton.setName(Locale.Icon(Configuration.getApplicationLanguage()));
            m_viewToolBarButton.setBorderPainted(false);
            m_viewToolBarButton.setContentAreaFilled(false);
            m_viewToolBarButton.setMargin(bttnMargins);
            m_viewToolBarButton.setActionCommand(AbstractChooserView.ICON + AbstractChooserView.DETAIL);
            m_viewToolBarButton.addActionListener(m_viewActionListener);
            m_viewToolBarButton.setSelected(true);
            JPanel outerPanel = new JPanel();
            outerPanel.add(m_viewToolBarButton);
            m_viewToolBarButton.setPreferredSize(new Dimension(350, 30));
            m_viewToolBarButton.setHorizontalAlignment(JToggleButton.LEFT);
            m_toolbar.add(outerPanel, BorderLayout.CENTER);
        }
        getContentPane().add(m_toolbar, BorderLayout.NORTH);
    }

    /**
     * Initializes the specified Chooser View and displays it.
     */
    private void initializeAndShowChooser(AbstractChooserView desiredChooserView) {

        setTitle(TITLE_VLAB + Constants.APPLICATION_NAME);

        // De-select current specimen
        if (m_curSpecimen != null) {
            // close the opened jar file handle.
            m_curSpecimen.close();
            m_curSpecimen = null;
        }

        setJMenuBar(makeChooserMenuBar());
        // TODO: Add tool bar overe here from which the view can be changed.
        // Create chooser if does not already exist
        if (desiredChooserView == null) {
            createAndShowChooser();
        } else {
            m_chooser = desiredChooserView;
            m_chooser.initialize(m_config, m_specimens.getOnlineSpecimens(),
                    m_specimens.getOfflineSpecimens(),
                    new SpecimenChooserListener());
            m_chooser.setPreferredSize(Constants.VIEWER_PREFERRED_DIM);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            addWindowListener(this);
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
        // Clean up content pane and add chooser
        getContentPane().removeAll();
        setToolBar();
        getContentPane().add(m_chooser);
        // reset the icon for the last loaded specimen button. This is needed
        // because when we loaded the specimen we set the button text to
        // "Loading Please Wait" now we need to undo that change.
        if (m_loadedSpecimenButton != null) {
            m_loadedSpecimenButton.setText("");
            m_loadedSpecimenButton.setIcon(m_loadedSpecimenIcon);
        }
        setVisible(true);
        repaint();
    }

    /**
     * This method creates both the views but makes only the first one visible.
     * This method is used at startup when we create both the icon and detail
     * view and showthe icon view as visible.
     */
    private void initializeAndShowChooser(AbstractChooserView viewToShow, AbstractChooserView backupView) {

        setTitle(TITLE_VLAB + Constants.APPLICATION_NAME);
        setJMenuBar(makeChooserMenuBar());
        // Create chooser if does not already exist
        backupView.initialize(m_config, m_specimens.getOnlineSpecimens(), m_specimens.getOfflineSpecimens(), new SpecimenChooserListener());
        backupView.setPreferredSize(Constants.VIEWER_PREFERRED_DIM);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        initializeAndShowChooser(viewToShow);
    }

    /**
     * Creates (if not already created) and shows a Microscope with an
     * appropriate menu.
     */
    private void createAndShowScope() {
        String titleSuff = null;
        String specimenName = null;

        if (m_curSpecimen != null) {
            if (m_curSpecimen.getXMLVersion() <= 1.0) {
                // Old XML Format
                // Get instrument names
                if ((titleSuff = getInstrumentNames(m_curSpecimen)).length() != 0) {
                    titleSuff = "(" + titleSuff + ")";
                }
                // Add the specimen name
                specimenName = m_curSpecimen.getDisplayString();
                if ((specimenName != null) && (specimenName.length() > 0)) {
                    titleSuff = specimenName + " " + titleSuff;
                }
            } else {
                // New XML Format
                titleSuff = m_curSpecimen
                        .getNamedSampleInfoParam(Constants.XMLTags.NAME);
                titleSuff += " ("
                        + m_curSpecimen
                        .getNamedInstrumetParam(Constants.XMLTags.LONG_NAME)
                        + ")";
            }
        }

        // Set to default if no info is available from specimen
        if ((titleSuff == null) || (titleSuff.length() == 0)) {
            titleSuff = Constants.APPLICATION_NAME;
        }

        setTitle(TITLE_VLAB + titleSuff);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setJMenuBar(makeScopeMenuBar());

        if (m_microscope == null) {
            m_microscope = new Microscope(m_config, this);
            m_microscope.getUnloadButton().addActionListener(
                    new SpecimenUnloadListener());
        }

        getContentPane().removeAll();
        getContentPane().add(m_microscope);

        m_microscope.setCurrentSpecimen(m_curSpecimen);

        setVisible(true);
        repaint();
    }

    /**
     * Collects instrument descriptions from a given specimen. Used to set the
     * window title in microscope "view".
     *
     * @param specimen specimen to examine
     * @return string containing a list of comma separated instrument names
     */
    private String getInstrumentNames(Specimen specimen) {
        String names = "";
        Iterator iter = specimen.getInstrumentDescriptors();

        while (iter.hasNext()) {
            if (names.length() != 0) {
                names += ", ";
            }

            names += ((Specimen.InstrumentDescriptor) iter.next())
                    .getDisplayString();
        }
        return names;
    }

    public void enableAnnotationDeleteItem(boolean enable) {
        m_itemDelete.setEnabled(enable);
    }

    /**
     * Creates a scope menu bar if not previously created; returns an existing
     * one, otherwise. The scope menu bar is the file menu that appears on top.
     *
     * @return an instance of JMenuBar.
     */
    private JMenuBar makeScopeMenuBar() {
        JMenu menuHelp = null;
        if (m_scopeMenuBar == null) {
            m_scopeMenuBar = new JMenuBar();

            JMenu menuFile = new JMenu(Locale.File(Configuration.getApplicationLanguage()));
            menuHelp = new JMenu(Locale.Help(Configuration.getApplicationLanguage()));

            JMenu itemFileAnnotations = new JMenu(Locale.Annotations(Configuration.getApplicationLanguage()));
            JMenuItem itemImport = new JMenuItem(Locale.Import(Configuration.getApplicationLanguage()));
            JMenuItem itemExport = new JMenuItem(Locale.Export(Configuration.getApplicationLanguage()));
            m_itemDelete = new JMenuItem(Locale.Delete(Configuration.getApplicationLanguage()));

            addEventHandlersForAnnotation(itemImport, itemExport, m_itemDelete);

            itemFileAnnotations.add(itemImport);
            itemFileAnnotations.add(itemExport);
            itemFileAnnotations.add(m_itemDelete);

            JMenuItem itemFilePrint = new JMenuItem(Locale.Print(Configuration.getApplicationLanguage()));
            JMenuItem itemFileExit = new JMenuItem(Locale.Exit(Configuration.getApplicationLanguage()));
            itemFilePrint.setMnemonic(KeyEvent.VK_P);
            itemFileExit.setMnemonic(KeyEvent.VK_X);

            JMenuItem itemHelpWebHelp = new JMenuItem(Locale.ReportIssue(Configuration.getApplicationLanguage()));
            JMenuItem itemHelpAbout = new JMenuItem(Locale.About(Configuration.getApplicationLanguage()));
            JMenuItem itemHelpLicense = new JMenuItem(Locale.License(Configuration.getApplicationLanguage()));
            itemHelpWebHelp.setMnemonic(KeyEvent.VK_W);
            itemHelpAbout.setMnemonic(KeyEvent.VK_A);
            itemHelpLicense.setMnemonic(KeyEvent.VK_A);

            itemFilePrint.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    printSpecimen();
                }
            });

            itemFileExit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (JOptionPane.YES_OPTION == new TranslatedOptionPane().yesNo(VirtualMicroscope.this,
                                    Locale.ExitVirtualMicroscope(Configuration.getApplicationLanguage())+"?", Locale.ConfirmExit(Configuration.getApplicationLanguage())) ) {
                        if (m_curSpecimen != null) {
                            if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                                return;
                        }

                        if (m_downloadManager != null)
                            // cancel all in-progress downloads.
                            m_downloadManager.cancelAllDownloads();
                        dispose();
                        System.exit(0);
                    }
                }
            });

            itemHelpWebHelp.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ProblemReport report = new ProblemReport(VirtualMicroscope.this);
                    report.setVisible(true);
                }
            });

            itemHelpAbout.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AboutDialog about = new AboutDialog(VirtualMicroscope.this);
                    about.setVisible(true);
                }
            });

            itemHelpLicense.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    License license = new License(VirtualMicroscope.this);
                    license.setVisible(true);
                }
            });

            menuFile.add(itemFileAnnotations);
            menuFile.add(itemFilePrint);
            menuFile.add(new JSeparator());
            menuFile.add(itemFileExit);

            menuHelp.add(itemHelpWebHelp);
            menuHelp.add(new JSeparator());
            menuHelp.add(itemHelpAbout);
            menuHelp.add(itemHelpLicense);

            menuFile.setMnemonic(KeyEvent.VK_F);
            menuHelp.setMnemonic(KeyEvent.VK_H);
            m_scopeMenuBar.add(menuFile);
            m_scopeMenuBar.add(menuHelp);
            createInformationMenu(menuHelp);
            if (m_curSpecimen.getXMLVersion() <= 1.0)
                m_informationItem.setVisible(false);
        } else {
            // the methd is being called while unloading a specimen. We shall
            // Hide/show the Information menu item based on the version of the
            // XML
            if (m_curSpecimen.getXMLVersion() > 1.0) {
                // New XML should show information menu items.
                m_informationItem.setVisible(true);
            } else { // Old XML, we need to hide the Information if it is
                m_informationItem.setVisible(false);
            }
        }
        return m_scopeMenuBar;
    }

    private void addEventHandlersForAnnotation(JMenuItem itemImport, JMenuItem itemExport, JMenuItem itemDelete) {
        itemImport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                m_microscope.getMicroscopeView().getAnnotationControl().importAnnotations();
            }
        });
        itemExport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                m_microscope.getMicroscopeView().getAnnotationControl().exportAnnotations();
            }
        });
        itemDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                m_microscope.getMicroscopeView().getAnnotationControl().deleteAnnotations();
            }
        });
    }

    /**
     * This method creates the Informaton Menu. This menu only applies to the
     * new format XML. Through this menu option the user can view the specimen
     * and the instrument information (which is read out of the specimen jar).
     */
    private void createInformationMenu(JMenu parentMenu) {
        JMenu menuInfo = parentMenu;
        m_informationItem = new JMenuItem(Locale.SpecimenInformation(Configuration.getApplicationLanguage()));
        m_informationItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // ////// Loading the specimen information. /////////
                LinkedHashMap specimenMap = new LinkedHashMap();
                String sampleName = m_curSpecimen.getNamedSampleInfoParam(Constants.XMLTags.NAME);
                if (sampleName != null && sampleName.length() > 0)
                    specimenMap.put("Sample Name", sampleName);

                String collectedOn = m_curSpecimen.getNamedSampleInfoParam(Constants.XMLTags.COLLECTION_DATE);
                if (collectedOn != null && collectedOn.length() > 0)
                    specimenMap.put("Collected On", collectedOn);
                // Collating all the InfoRef tags.
                specimenMap.putAll(m_curSpecimen.getSampleInfo().getInfoRefStrings());
                // long description goes at the very end.
                String longDescription = m_curSpecimen.getNamedSampleInfoParam(Constants.XMLTags.SAMPLE_LONG_DESCRIPTION);
                if (longDescription != null) {
                    specimenMap.put("Description", Utility.removeLinks(longDescription));
                }

                // ////// Loading the instrument information. /////////
                LinkedHashMap instrumentMap = new LinkedHashMap();
                String longName = m_curSpecimen.getNamedInstrumetParam(Constants.XMLTags.LONG_NAME);
                if (longName != null && longName.length() > 0)
                    instrumentMap.put("Equipment Name", longName);
                String model = m_curSpecimen.getNamedInstrumetParam(Constants.XMLTags.MODEL);
                if (model != null && model.length() > 0)
                    instrumentMap.put("Model", model);
                // Collating all the InfoRef tags.
                instrumentMap.putAll(m_curSpecimen.getInstrumentInfo().getInfoRefStrings());

                InformationDialog info = new InformationDialog(VirtualMicroscope.this, specimenMap, instrumentMap, m_curSpecimen, m_config);
                info.setVisible(true);
            }
        });
        m_informationItem.setMnemonic(KeyEvent.VK_S);
        menuInfo.insert(m_informationItem, 0);
    }

    /**
     * Creates a chooser menu bar if not previously created; returns an existing
     * one, otherwise.
     *
     * @return an instance of JMenuBar.
     */
    private JMenuBar makeChooserMenuBar() {
        if (m_chooserMenuBar == null) {
            m_chooserMenuBar = new JMenuBar();
            JMenu menuFile = new JMenu(Locale.File(Configuration.getApplicationLanguage()));
            JMenu menuTools = new JMenu(Locale.Tools(Configuration.getApplicationLanguage()));
            JMenu menuHelp = new JMenu(Locale.Help(Configuration.getApplicationLanguage()));
            menuFile.setMnemonic(KeyEvent.VK_F);
            menuTools.setMnemonic(KeyEvent.VK_T);
            menuHelp.setMnemonic(KeyEvent.VK_H);

            JMenuItem itemFileExit = new JMenuItem(Locale.Exit(Configuration.getApplicationLanguage()));
            itemFileExit.setMnemonic(KeyEvent.VK_X);

            m_selectedViewBulletIcon = new ImageIcon(getClass().getResource(Constants.Resources.BULLET));
            m_unSelectedViewBulletIcon = new ImageIcon(getClass().getResource(Constants.Resources.UNSELECTED_BULLET));
            m_itemDetailView = new JMenuItem(AbstractChooserView.DETAIL, m_unSelectedViewBulletIcon);
            m_itemDetailView.setMnemonic(KeyEvent.VK_D);
            m_itemIconView = new JMenuItem(AbstractChooserView.ICON, m_selectedViewBulletIcon);
            m_itemIconView.setMnemonic(KeyEvent.VK_I);

            JMenuItem itemToolsRefresh = new JMenuItem(Locale.Refresh(Configuration.getApplicationLanguage()));
            JMenuItem itemToolsConfig = new JMenuItem(Locale.Configure(Configuration.getApplicationLanguage()));
            itemToolsRefresh.setMnemonic(KeyEvent.VK_R);
            itemToolsRefresh.setMnemonic(KeyEvent.VK_F5);
            itemToolsConfig.setMnemonic(KeyEvent.VK_C);

            JMenuItem itemHelpWebHelp = new JMenuItem(Locale.ReportIssue(Configuration.getApplicationLanguage()));
            JMenuItem itemHelpAbout = new JMenuItem(Locale.About(Configuration.getApplicationLanguage()));
            JMenuItem itemHelpLicense = new JMenuItem(Locale.License(Configuration.getApplicationLanguage()));
            itemHelpAbout.setMnemonic(KeyEvent.VK_A);
            itemHelpLicense.setMnemonic(KeyEvent.VK_A);
            itemHelpWebHelp.setMnemonic(KeyEvent.VK_W);

            m_itemDetailView.addActionListener(m_viewActionListener);
            m_itemIconView.addActionListener(m_viewActionListener);
            itemFileExit.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if (JOptionPane.YES_OPTION == new TranslatedOptionPane().yesNo(VirtualMicroscope.this,
                            Locale.ExitVirtualMicroscope(Configuration.getApplicationLanguage())+"?",
                            Locale.ConfirmExit(Configuration.getApplicationLanguage())) ) {
                        if (m_curSpecimen != null) {
                            try {
                                if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                                    return;
                            } catch (Exception ex) {
                                log.error("Error closing annotation!");
                                log.error(ex.getMessage());
                            }
                        }
                        if (m_downloadManager != null)
                            // cancel all in-progress downloads.
                            m_downloadManager.cancelAllDownloads();
                        dispose();
                        System.exit(0);
                    }
                }
            });

            itemToolsRefresh.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) { // Re-read specimens, pass them on to chooser
                    try {
                        if (m_chooser != null)
                            refreshSpecimensAndChooserViews();
                    } catch (Exception ex) {
                        JOptionPane.showMessageDialog(null, Locale.ErrorReadingSpecimens(Configuration.getApplicationLanguage()),
                                Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
                        log.error(Locale.ErrorReadingSpecimens(Locale.LANGUAGE.EN));
                        log.error(ex.getMessage());
                    }
                }
            });

            itemToolsConfig.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ConfigEditor editConfig = new ConfigEditor(VirtualMicroscope.this, VirtualMicroscope.this.m_config);

                    editConfig.setVisible(true);

                    // Have a valid config, if it is dirty - write it out
                    if (VirtualMicroscope.this.m_config.isDirty()) {
                        VirtualMicroscope.this.m_config.writeConfig();
                    }
                }
            });

            itemHelpWebHelp.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ProblemReport report = new ProblemReport(VirtualMicroscope.this);
                    report.setVisible(true);
                }
            });

            itemHelpAbout.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AboutDialog about = new AboutDialog(VirtualMicroscope.this);
                    about.setVisible(true);
                }
            });

            itemHelpLicense.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    License license = new License(VirtualMicroscope.this);
                    license.setVisible(true);
                }
            });

            menuFile.add(itemFileExit);

            menuTools.add(itemToolsRefresh);
            menuTools.add(itemToolsConfig);

            menuHelp.add(itemHelpWebHelp);
            menuHelp.add(new JSeparator());
            menuHelp.add(itemHelpAbout);
            menuHelp.add(itemHelpLicense);

            m_chooserMenuBar.add(menuFile);
            m_chooserMenuBar.add(menuTools);
            m_chooserMenuBar.add(menuHelp);
        }

        return m_chooserMenuBar;
    }

    /**
     * Reads specimens from the specimen directory.
     *
     * @throws SpecimenException if failed to read speciemns
     */
    private void readOnlineSpecimens() throws SpecimenException {
        m_specimens = new SpecimenSet(this, m_config);
        m_curSpecimen = m_specimens.getDefaultSpecimen();
    }

    /**
     * This method re-reads the specimend located in the local specimens
     * directory. This functionality is required at the system startup and when
     * the specimens need to be re-loaded (e.g. upon downloading the offline
     * specimens to local directory). This method lets us idenitfy new specimens
     * without the need to restart the application. In addition to loading the
     * online specimens this method also reconciles the online and offline
     * specimens what this means is that if a new specimen gets added to the
     * online specimen collection either by manually copying the specimen jar to
     * specimens folder or by downloading it to the specimens folder then this
     * specimen must be removed from the offline specimens collection.
     *
     * @param noThreads
     * @throws SpecimenException
     */
    private void readSpecimens(boolean noThreads) throws SpecimenException {
        if (m_specimens != null) {
            m_specimens.refreshOnlineSpecimens();
        } else
            m_specimens = new SpecimenSet(m_config);
        m_curSpecimen = m_specimens.getDefaultSpecimen();
    }

    /**
     * This method takes in a newly downloaded offline specimen and inserts it
     * into online specimens. The readSpecimens() method re-reads ALL the
     * specimens in the specimens folder but this method only reads the
     * specified specimen. This method should be called when the caller knows in
     * advance which new specimen has been added into the specimens directory.
     *
     * @param newlyDownloadedSpecimen
     */
    public void moveOfflineSpecimenToOnlinePanel(OfflineSpecimen newlyDownloadedSpecimen) {
        if (m_chooser != null) {
            // refreshSpecimensAndChooserViews();
            LinkedHashMap oldOnlineSpecimens = (LinkedHashMap) m_specimens
                    .getOnlineSpecimens().clone();
            LinkedHashMap oldOfflineSpecimens = (LinkedHashMap) m_specimens
                    .getOfflineSpecimens().clone();
            // Cursor cur = getCursor();
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            m_specimens.readInSpecimen(newlyDownloadedSpecimen);
            while (!VirtualMicroscope.this.m_specimens.m_isDone)
                ;
            refreshChooserViews(oldOnlineSpecimens, oldOfflineSpecimens);
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            // being paranoid in conserving memory..
            oldOnlineSpecimens.clear();
            oldOfflineSpecimens.clear();
            oldOnlineSpecimens = null;
            oldOfflineSpecimens = null;
        }
    }

    /**
     * This method refreshes the chooser views.
     */
    private void refreshChooserViews(LinkedHashMap oldOnlineSpecimens, LinkedHashMap oldOfflineSpecimens) {
        try {
            // Refreshing both the views, although only one view will be visible
            // at a given time but we should keep both views in synch with each
            // other.
            m_iconChooserView.refreshSpecimens(m_specimens.getOnlineSpecimens(), m_specimens.getOfflineSpecimens(), oldOnlineSpecimens, oldOfflineSpecimens);
            m_detailChooserView.refreshSpecimens(m_specimens.getOnlineSpecimens(), m_specimens.getOfflineSpecimens(), oldOnlineSpecimens, oldOfflineSpecimens);
            this.repaint();
        } catch (Exception ex) {
            log.error("Error refreshing chooser View");
            log.error(ex.getMessage());
        }
    }

    /**
     * This method refreshes the specimens and the both chooser views.
     */
    private void refreshSpecimensAndChooserViews() {
        try {
            LinkedHashMap oldOnlineSpecimens = (LinkedHashMap) m_specimens.getOnlineSpecimens().clone();
            LinkedHashMap oldOfflineSpecimens = (LinkedHashMap) m_specimens.getOfflineSpecimens().clone();
            Cursor cur = getCursor();
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            readSpecimens(true);
            while (!VirtualMicroscope.this.m_specimens.m_isDone) ;
            refreshChooserViews(oldOnlineSpecimens, oldOfflineSpecimens);

            // being paranoid in conserving memory..
            oldOnlineSpecimens.clear();
            oldOfflineSpecimens.clear();
            oldOnlineSpecimens = null;
            oldOfflineSpecimens = null;

            setCursor(cur);
        } catch (Exception ex) {
            log.error("Error refreshing specimens and chooser views!");
            log.error(ex.getMessage());
        }
    }

    /**
     * Change current specimen and show the microscope view.
     *
     * @param newSpecimen new specimen
     */
    private void changeSpecimen(Specimen newSpecimen) {
        if (newSpecimen != null) {
            if (m_curSpecimen != null) {
                m_curSpecimen.close();
            }

            m_curSpecimen = newSpecimen;
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    VirtualMicroscope.this.createAndShowScope();
                }
            });
        }
    }

    /**
     * Prints the current specimen. If the viewable portion of the specimen fits
     * on a single page we simply print it out, else we shrink it to fit.
     */
    private void printSpecimen() {

        PrintService selectedPrinter = selectPrintService();

        if (null==selectedPrinter)
            return;

        try {
            PrinterJob printJob = PrinterJob.getPrinterJob();
            printJob.setPrintable(m_microscope.getPrintableView());

            printJob.setPrintService(selectedPrinter);

            if (!printJob.printDialog()) {
                return;
            }

            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            printJob.print();
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));

            JOptionPane.showMessageDialog(this, Locale.PrintingCompletedSuccessfully(Configuration.getApplicationLanguage())+".",
                    m_curSpecimen.getDisplayString(), JOptionPane.INFORMATION_MESSAGE);
        } catch (PrinterException e) {
            log.error("Printing " + m_curSpecimen.getDisplayString() + " failed");
            log.error(e.getMessage());
            JOptionPane.showMessageDialog(this, Locale.PrintingFailed(Configuration.getApplicationLanguage()), m_curSpecimen.getDisplayString(), JOptionPane.ERROR_MESSAGE);
        }
    }

    private PrintService selectPrintService() {
        PrintService[] printers = PrinterJob.lookupPrintServices();

        if(0>=printers.length) {
            JOptionPane.showMessageDialog(this, Locale.PrinterNotFound(Configuration.getApplicationLanguage()), m_curSpecimen.getDisplayString(), JOptionPane.ERROR_MESSAGE);
            return null;
        }

        if(1==printers.length)
            return printers[0];

        ArrayList<String> printerNames = new ArrayList<String>();
        for ( PrintService printerInstance : printers ) {
            printerNames.add(printerInstance.getName());
        }

        JComboBox printerSelector = new JComboBox(printerNames.toArray(new String[0]));

        if (JOptionPane.CANCEL_OPTION == new TranslatedOptionPane().cancel(m_microscope, printerSelector, Locale.SelectPrinter(Configuration.getApplicationLanguage())) )
            return null;

        return printers[printerSelector.getSelectedIndex()];
    }

    /**
     * This method unloads the current specimen and takes the user back to the
     * start page. This is usually called when the user presses Ctrl-F4.
     */
    public void unloadSpecimen() {
        if (m_curSpecimen != null) {
            if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                return;
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                VirtualMicroscope.this.createAndShowChooser();
            }
        });
    }

    class ViewActionListener implements ActionListener {
        /**
         * Gets called when either the menu options View->Detail/Icon are
         * pressed or the view toolbar button is pressed. Note that for the view
         * tool bar button the checks seem to be oppostie, this is because when
         * this method gets called the view toolbar button has already changed
         * state so the checks have to be done in restrospect.
         */
        public void actionPerformed(ActionEvent e) {
            try {
                if (e.getActionCommand().equals(AbstractChooserView.ICON)) {
                    changeToIconView(); // View->Icon selected.
                    m_viewToolBarButton.setSelected(true);
                    m_viewToolBarButton.setText(Locale.SwitchToDetailView(Configuration.getApplicationLanguage()));
                } else if (e.getActionCommand().equals(AbstractChooserView.DETAIL)) {
                    changeToDetailView();// View->Detail selected.
                    m_viewToolBarButton.setSelected(false);
                    m_viewToolBarButton.setText(Locale.SwitchToIconView(Configuration.getApplicationLanguage()));
                } else if (e.getActionCommand().equals(AbstractChooserView.ICON + AbstractChooserView.DETAIL)) {
                    // the toolbar button was pressed.
                    if (m_viewToolBarButton.isSelected()) {
                        changeToIconView();
                        m_viewToolBarButton.setText(Locale.SwitchToDetailView(Configuration.getApplicationLanguage()));
                    } else {
                        changeToDetailView();
                        m_viewToolBarButton.setText(Locale.SwitchToIconView(Configuration.getApplicationLanguage()));
                    }
                } else
                    log.error("Unknown action " + e.getActionCommand());

            } catch (Exception ex) {
                log.error(Locale.ErrorDisplayingTheView(Locale.LANGUAGE.EN));
                log.error(ex.getMessage());
                JOptionPane.showMessageDialog(null, Locale.ErrorDisplayingTheView(Configuration.getApplicationLanguage()),
                        Locale.Error(Configuration.getApplicationLanguage()), JOptionPane.ERROR_MESSAGE);
            }
        }

        private void changeToDetailView() throws Exception {
            if (m_chooser != null
                    && m_chooser.getViewType() != AbstractChooserView.DETAIL_VIEW) {
                m_chooser = null;
                m_itemIconView.setIcon(m_unSelectedViewBulletIcon);
                m_itemDetailView.setIcon(m_selectedViewBulletIcon);

                setJMenuBar(makeChooserMenuBar());
                getContentPane().removeAll();
                setToolBar();

                getContentPane().add(m_detailChooserView);
                m_chooser = m_detailChooserView;
                setVisible(true);
                repaint();

            }
        }

        private void changeToIconView() {
            if (m_chooser != null && m_chooser.getViewType() != AbstractChooserView.ICON_VIEW) {
                m_itemDetailView.setIcon(m_unSelectedViewBulletIcon);
                m_itemIconView.setIcon(m_selectedViewBulletIcon);
                setJMenuBar(makeChooserMenuBar());
                getContentPane().removeAll();
                setToolBar();
                getContentPane().add(m_iconChooserView);
                m_chooser = m_iconChooserView;
                setVisible(true);
                repaint();
            }
        }
    }

    class KeyboardListener extends KeyAdapter {
        public void keyPressed(KeyEvent e) {
            if (e.isAltDown()) {
                if (e.getKeyCode() == KeyEvent.VK_F4) {
                    if (JOptionPane.YES_OPTION == new TranslatedOptionPane().yesNo(VirtualMicroscope.this,
                                    Locale.ExitVirtualMicroscope(Configuration.getApplicationLanguage())+"?", Locale.ConfirmExit(Configuration.getApplicationLanguage()) )) {
                        if (m_curSpecimen != null) {
                            try {
                                if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                                    return;
                            } catch (Exception ex) {
                                log.error("Error closing annotation!");
                                log.error(ex.getMessage());
                            }
                        }
                        if (m_downloadManager != null)
                            // cancel all in-progress downloads.
                            m_downloadManager.cancelAllDownloads();
                        dispose();
                        System.exit(0);
                    }
                } else {
                    m_scopeMenuBar.dispatchEvent(e);
                }
            }
            if (!e.isConsumed()) {
                //pass them on to the view to consume them.
                m_microscope.getMicroscopeView().dispatchEvent(e);
            }

        }

        public void keyReleased(KeyEvent e) {
        }

        /**
         * Handles the key typed events. We handle the printable character in
         * the KeyTyped event handler and others in the KeyPressed()
         *
         * @param e KeyEvent
         */
        public void keyTyped(KeyEvent e) {
        }
    }

    /**
     * Listens for events from the Unload button.
     */
    class SpecimenUnloadListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (m_curSpecimen != null) {
                if (!m_microscope.getMicroscopeView().getAnnotationControl().closeAnnotation())
                    return;
            }

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    VirtualMicroscope.this.createAndShowChooser();
                }
            });
        }
    }

    /**
     * Listens for events from ChooserView's specimen buttons.
     */
    class SpecimenChooserListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            /** ActionCommand is specimen's unique name. */
            m_loadedSpecimenButton = (JButton) e.getSource();
            m_loadedSpecimenIcon = ((JButton) e.getSource()).getIcon();
            if (!VirtualMicroscope.this.m_specimens.isOffline(e
                    .getActionCommand())) {
                ((JButton) e.getSource()).removeAll();
                ((JButton) e.getSource()).setIcon(null);
                ((JButton) e.getSource()).setForeground(Color.RED);
                ((JButton) e.getSource()).setText("<html>"+Locale.Loading(Configuration.getApplicationLanguage())+".<br>"+Locale.PleaseWait(Configuration.getApplicationLanguage())+"...</html>");
                Specimen newSpecimen = VirtualMicroscope.this.m_specimens.getSpecimenIfAvailable(e.getActionCommand());
                if (newSpecimen == null) {
                    // The specimen has been manually deleted from the specimen
                    // folder. We need to refresh our specimens to make sure we
                    // keep our view insynch with whats available in the
                    // specimen
                    // folders.
                    JOptionPane.showMessageDialog(
                                    null,
                                    Locale.SpecimenWasManualyRemoved(Configuration.getApplicationLanguage()),
                                    Locale.SpecimenNotFoundError(Configuration.getApplicationLanguage()),
                                    JOptionPane.ERROR_MESSAGE);

                    refreshSpecimensAndChooserViews();

                } else {
                    // setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    VirtualMicroscope.this.changeSpecimen(newSpecimen);
                }

            } else {

                if (m_downloadManager == null)
                    m_downloadManager = new SpecimenDownloadManager(m_config, VirtualMicroscope.this);

                m_downloadManager.downloadSpecimen(m_specimens.getOfflineSpecimen(e.getActionCommand()));
            }
        }
    }
}