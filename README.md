# Virtual Microscope

<dl>
<table>
<tbody>
<tr>
<td width="60%">
<H2> Description: </H2> 

<p>Virtual Microscope is an application with which users can view, analyze 
and record comments on captured images using different microscopes. 
The purpose of the application is to give to the users the possibility to study saved images 
in the same way as they would by studying a sample with the actual microscope.</p>

<p>The application was developed as part of the "[Virtual Laboratory](http://virtual.itg.uiuc.edu/)" 
to be used by researchers and students around the world.</p>

<p>This work is a fork of the original Virtual Microscope, a project of the Imaging Technology Group, 
Beckman Institute for Advanced Science and Technology at the University of Illinois at Urbana-Champaign. 
http://virtual.itg.uiuc.edu </p>

<H2> Installation: </H2> 
<H4> For Academix users: </H4>
<p>
The Virtual Microscope can be installed from "<em>EDU</em>" or 
using the command: "<em>apt install virtual-microscope</em>"</p>

<H4> For users of other linux distributions: </H4>
<p>The latest version of Virtual Microscope is packed in the  <strong>Virtuallab.jar</strong>
and can be downloaded for free from the repository:
[https://gitlab.com/virtual_microscope_projects/virtual-microscope/tree/master/deployed](https://gitlab.com/virtual_microscope_projects/virtual-microscope/tree/master/deployed)
</p>

<p>Running can be done using the "<em>java-jar Virtuallab.jar</em>" command.</p>

<p>The application was originally created for Windows and Mac but I am not currently testing it on those platforms. </p>

</td>
<td>
<table>
<tbody>
<tr>
<td><img src="Screenshots/MainScreen.png" alt="Main screen" width="400" height="256" /></td>
</tr>
<tr>
<td><img src="Screenshots/Penicillium.png" alt="Penicillium" width="400" height="256" /></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</dl>

## Distribution and Usage Conditions:

Because development on the initial application ceased in 2014, the source code was replicated, 
and the development continues as an open source project in this repository.
The application and source code are distributed under MIT license and other additional 2 clauses added as required by the initial license.

The [data set](http://virtual.itg.uiuc.edu/data/) that can be downloaded using the application has been created 
and made available (licensed under the University of Illinois / NCSA Open Source License) by the Imaging Technology Group 
using the following microscopes:Philips Environmental Scanning Electron Microscope (ESEM) )"And"Fluorescence Light Microscope". 
If you use these images it is necessary to mention the source of the information 
as described at: http://virtual.itg.uiuc.edu/credits/acknowledge.shtml

## Issues:
Please report any issue with the application by sending an email to: 

`incoming+virtual_microscope_projects/virtual-microscope@incoming.gitlab.com`

## WebPage:
[https://academixproject.com/virtual-microscop/](https://academixproject.com/virtual-microscop/)

## Wiki Page
[https://gitlab.com/virtual_microscope_projects/virtual-microscope/wikis/home](https://gitlab.com/virtual_microscope_projects/virtual-microscope/wikis/home)

## Credits to contributors
The translation and logo are available thanks to Academix team efforts. Website: https://academixproject.com